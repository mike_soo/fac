package generecite;

import java.util.LinkedList;

public class FileAvecPriorite <A extends IElementAvecPriorite>
{
	
	private LinkedList <A> elements = new LinkedList<A>();
	
	public boolean estVide()
	{
		return elements.size()==0;
	}
	
	public void ajfile (A e)
	{
		elements.addLast(e);
	}
	
	A defile()
	{
		if(elements.size()!=0)
			return elements.pop();
		else
			return null;
	}
	int nbElements()
	{
		return elements.size();
	}
	
	A sommet()
	{
		return elements.getFirst();
	}
}
