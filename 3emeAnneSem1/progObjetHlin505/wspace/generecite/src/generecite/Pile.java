package generecite;

import java.util.LinkedList;

public class Pile <A>
{
	
	private LinkedList <A> elements = new LinkedList<A>();
	
	public boolean estVide()
	{
		return elements.size()==0;
	}
	
	public void empile (A e)
	{
		elements.addFirst(e);
	}
	
	A depile()
	{
		if(elements.size()!=0)
			return elements.pop();
		else
			return null;
	}
	int nbElements()
	{
		return elements.size();
	}
	
	A sommet()
	{
		return elements.getFirst();
	}
}
