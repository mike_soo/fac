package generecite;

import java.lang.reflect.Array;
import java.util.Vector;

public class Tableau <A extends Comparable <A> >{
	
	private Vector <A> tab = new Vector <A> ();
	
	public Tableau(Vector <A> tab)
	{
		this.tab=tab; //recopie 'superficielle'
	}
	
	public void triBulles ()
	{
		int i = tab.size()  - 2;
		boolean ech = true;
		System.out.println("i="+i);
		while(i >=2 && ech)
		{	
			ech = false;
			for(int j=0;j<=i ; j++)
			{	
				System.out.println("resultat Compare to="+ tab.get(j).compareTo(tab.get(j+1)));
				if(  (tab.get(j).compareTo(tab.get(j+1)))>0  )
				{
						A aux = tab.get(j);
						
						tab.set(j,tab.get(j+1));
						tab.set(j+1,aux);
						
						
						ech=true;
				}
			
			}
			i--;
		}
	}
	
	public void affiche()
	{
		for (int i=0;i<tab.size();i++)
		{
			System.out.println(tab.get(i)+ " ");
		}
		System.out.println();
	}
	
	public static void main(String [] args)
	{
		Vector <Integer> vectI = new Vector <Integer> ();
		vectI.add(5);
		vectI.add(10);
		vectI.add(14);
		vectI.add(2);
		vectI.add(3);
		vectI.add(8);
		vectI.add(7);
		
		Vector <String> vectS = new Vector <String> ();
		vectS.add("A");
		vectS.add("C");
		vectS.add("B");
		vectS.add("E");
		vectS.add("G");
		vectS.add("F");
		vectS.add("Z");
		
		Tableau  <Integer> tableauI = new Tableau <Integer> (vectI);
		tableauI.triBulles();
		tableauI.affiche();
		
		Tableau  <String> tableauS = new Tableau <String> (vectS);
		tableauS.triBulles();
		tableauS.affiche();
	}
}
