package generecite;

import java.util.LinkedList;

public class File <A extends IElementAvecPriorite>
{
	
	private LinkedList <A> elements = new LinkedList<A>();
	
	public boolean estVide()
	{
		return elements.size()==0;
	}
	
	public void ajfile (A e)
	{
		elements.addLast(e);
	}
	public A getPremAvecPriorite() //renvois l'element avec le plus de priorité dans la file*
	{
		if(elements.size()==0)
			return null;
		
		
		int ind=0;
		
		for(int i=0;i<elements.size();i++)
		{
			if(elements.get(i).priorite()>elements.get(ind).priorite())
			{
				
				ind=i;
			}
			
		}
		
		return elements.remove(ind);
	}
	A defile()
	{
		if(elements.size()!=0)
			return elements.pop();
		else
			return null;
	}
	int nbElements()
	{
		return elements.size();
	}
	
	A sommet()
	{
		return elements.getFirst();
	}
}
