package generecite;

import java.util.Vector;

public class CEtreVivant <A extends IEtreVivant,B extends IEtreVivant>
{
	A ev1;
	B ev2;
	
	public void ajoutCEtreVivant(A ev1,B ev2)
	{
		if(  
				(ev1.getEspece().equals(ev2.getEspece()))
			&& 
				(ev1.getGenre().equals( ev2.getGenre()))
		  )
		{
			this.ev1=ev1;
			this.ev2=ev2;
		}
	}
}
