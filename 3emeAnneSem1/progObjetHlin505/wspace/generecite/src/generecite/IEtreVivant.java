package generecite;

public interface IEtreVivant 
{
	Espece getEspece();
	Genre getGenre();
}
