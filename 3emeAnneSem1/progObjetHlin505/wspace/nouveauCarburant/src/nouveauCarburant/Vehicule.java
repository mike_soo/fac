package nouveauCarburant;

public abstract class Vehicule {
	private String inmatriculation;
	private int poidaVide;

	public String toString()
	{
		return "inmatriculation: " + inmatriculation +"\n" 
				+ "poid A vide: " + poidaVide + "\n";
	}
	
	public abstract int calculVitesseMax();
	
	public Vehicule (String inmatriculation,int poidaVide)
	{
		this.inmatriculation=inmatriculation;
		this.poidaVide=poidaVide;
	}
}
