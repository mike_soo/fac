package instrospection;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class C {
	private ArrayList<Object> al= new ArrayList<Object>();
	public void add(Object o)
	{
		al.add(o);
	}
	public Method[] getMethodsForIndex(int i)
	{
		Object o = al.get(i);
		Class c = o.getClass();
		return c.getMethods();
	}
}
