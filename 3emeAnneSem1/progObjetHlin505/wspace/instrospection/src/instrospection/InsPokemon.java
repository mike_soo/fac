package instrospection;

import java.lang.reflect.Field;
import java.util.Scanner;

public class InsPokemon 
{	
	private Object np;
	private Class c;
	private Field lAttr[];
	;
	Pokemon creerPokemon(String nomPType)
	{
		
		boolean ex=false;
		try
		{
			c=Class.forName("instrospection."+nomPType);
			
			np=c.newInstance();
		}
		catch(Exception e)
		{
			System.out.println("Pokemon type non existant; exception = " + e.toString());
			return null;
		}
		
		String inser;
		
		
		Scanner s= new Scanner(System.in);
		
		while (!c.getName().equals("java.lang.Object"))
		{
			System.out.println("Dans:" + c.getName());
			lAttr=c.getDeclaredFields();
			
			for ( Field f : lAttr)
			{
				
				f.setAccessible(true);
				ex=false;
				while(!ex)
				{
					System.out.println("choisisé la valeur de "
							+ f.getName() +" ou exit por anuler");
					ex=false;
					try
					{
						inser=s.next();
						
						if (inser.equals("exit"))
							ex=true;
						else
						{
							if(f.getType().getName().equals("Integer") || f.getType().getName().equals("int"))
							{
								f.set(np,Integer.parseInt(inser,10));
								ex=true;
							}
							
							else if(f.getType().getName().equals("java.lang.String"))
							{
								f.set(np,inser);
								ex=true;
							}
							else if(f.getType().getName().equals("double"))
							{
								f.set(np,Double.parseDouble(inser));
								ex=true;
							}
							else
							{
								System.out.println("type imposible de definir");
							}
							
						}
					}
					catch(Exception e)
					{
						System.out.println("erreur d'insertion d'attribut"
								+" avez vous inserez le bon type de donnée?" + e.toString());
						ex=false;
					}
				}
			}
			c=c.getSuperclass();
			
		}
		
		return (Pokemon)np;
		
		
		
	}
}
