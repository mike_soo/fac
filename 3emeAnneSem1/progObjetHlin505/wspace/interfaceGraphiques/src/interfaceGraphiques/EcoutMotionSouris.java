package interfaceGraphiques;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class EcoutMotionSouris implements MouseMotionListener{
	
	
	public void mouseMoved(MouseEvent e)
	{
		System.out.println("Souris bougé au pt" + e.getX()+' ' +
				e.getY() );
	}
	
	public void mouseDragged(MouseEvent e)
	{
		System.out.println("Souris dragué au pt" + e.getX()+' ' +
				e.getY());
	}

}
