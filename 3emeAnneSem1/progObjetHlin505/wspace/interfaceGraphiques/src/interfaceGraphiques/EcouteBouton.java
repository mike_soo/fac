package interfaceGraphiques;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public class EcouteBouton implements ActionListener{

	private static Color[] tCol= {Color.black, Color.blue, Color.yellow};
	private int numCol = -1;
	private JPanel p;
	public EcouteBouton(JPanel p)
	{ this.p = p; }
	public void actionPerformed(ActionEvent e)
	{
		numCol = (numCol + 1) % tCol.length;
		p.setBackground(tCol[numCol]);
	}
		
}
