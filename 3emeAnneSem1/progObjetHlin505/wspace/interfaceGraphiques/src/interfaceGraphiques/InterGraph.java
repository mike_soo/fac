package interfaceGraphiques;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InterGraph extends JFrame
{	
	/**
	 * 
	 */
	
	protected JPanel pRouge, pBleu, pJaune; 
	protected JTextField inputTexte; 
	protected JButton b1,b2,b3; 
	JButton boutons[];
	 // constructeur 
	 public InterGraph() 
	 { 
	     super(); // sinon, serait ajouté par le compilateur 
	     setTitle("Interface Java!"); 
	     setSize(400,400) ;  
	     setLocation(100,100) ;  
	     //setLayout(new FlowLayout());
	     //setLayout(new GridLayout());
	     
	     pRouge = new JPanel(); 
	     pRouge.setBackground(Color.red); // change la couleur de fond 
	     pBleu = new JPanel(); 
	     pBleu.setBackground(Color.blue); 
	     pJaune = new JPanel(); 
	     pJaune.setBackground(Color.yellow); 
	     add(pRouge, "North");  
	     add(pBleu, "Center"); // ou : add(pBleu); 
	     add(pJaune, "East");  
	     pRouge.setPreferredSize(new Dimension(400,100)); 
	     pJaune.setPreferredSize(new Dimension(100,200)); 
	     b1 = new JButton("b1"); pJaune.add(b1); 
	     b2 = new JButton("b2"); pJaune.add(b2); 
	     b3 = new JButton("b3"); pJaune.add(b3); 
	     inputTexte = new JTextField("Saisir Texte",20); 
	     pBleu.add(inputTexte); 
	        
	     pack();    
	     
	     /*
	      boutons = new JButton[5]; 
		  for (int i = 0; i < boutons.length; i ++) 
		  {
			  boutons[i]=new JButton("Bouton "+i); 
			  add(boutons[i]);
		  }
		  */
	     
	     
		 
	 } 
	 
	  
}
	


