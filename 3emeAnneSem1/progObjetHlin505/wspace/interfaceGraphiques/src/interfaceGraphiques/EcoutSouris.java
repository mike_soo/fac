package interfaceGraphiques;

import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.awt.event.MouseAdapter;

public class EcoutSouris extends MouseAdapter
{
	public void mousePressed(MouseEvent e)
	{
		System.out.println("Souris pressée au pt" + e.getX()+' ' +
				e.getY());
	}
	
	public void mouseReleased(MouseEvent e)
	{
		System.out.println("Souris relaché au pt" + e.getX()+' ' +
				e.getY());
	}
	
	public void mouseClicked(MouseEvent e)
	{
		System.out.println("Souris clické au pt" + e.getX()+' ' +
				e.getY() + "numFen=" +   ((FenetreSouris) 
											(SwingUtilities.getWindowAncestor
														(
															(PanneauBouton) e.getSource()
														)
											)
										).getnumFen()
						  ) ;
	}
	
	
	
}
