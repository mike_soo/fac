package interfaceGraphiques;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Calculatrice extends JFrame

{
	
	
	protected JPanel top, mid, bot; 
	protected JTextField inputTexte; 
	protected  Vector <JButton>  buttons ;
	GridBagConstraints c;
	public Calculatrice()
	{
		super(); // sinon, serait ajouté par le compilateur 
	    setTitle("Calculatrice"); 
	    setSize(400,400);  
	    setLocation(100,100) ;  
	    //setLayout(new FlowLayout());
	    //setLayout(new GridLayout(3,1));
	    setLayout(new BorderLayout());
	           
	    top = new JPanel();
	    mid = new JPanel();
	    bot = new JPanel();
	    
	 
	    
	    //top.setPreferredSize (new Dimension (400,50));
	    top.setLayout(new GridLayout(1,1));
	    
	    mid.setPreferredSize(new Dimension (400,200));
	    mid.setLayout(new GridLayout(4,3));
	   
	    //bot.setPreferredSize(new Dimension (400,50));
	    bot.setLayout(new GridLayout(1,1));
	    
	    add(top,"North");
	    add(mid,"Center");
	    add(bot,"South");
	    
	    
	   
	    
	    buttons.get(0).setPreferredSize(new Dimension(400,50));
	    
	    for (int i=0;i<4;i++)
	    {
	    	for(int j=0;j<4;j++)
	    		
	    		buttons.add(new JButton("" + i ));
	    	
	    	
	    }
	  
	   
	    
	    
	    top.add(buttons.get(0));
	    for (int i=1;i<11;i++)
	    {
	    	if(i==10)
	    	{
	    		mid.add(Box.createRigidArea (new Dimension (400,50)));
	    		mid.add(buttons.get(i));
	    	}
	    	else
	    	mid.add(buttons.get(i));
	    	//mid.add(Box.createRigidArea(new Dimension(10,0)));
	    	
	    }
	    bot.add(buttons.get(11));
	    

	    /*
		    pRouge = new JPanel(); 
		    pRouge.setBackground(Color.red); // change la couleur de fond 
		    pBleu = new JPanel(); 
		    pBleu.setBackground(Color.blue); 
		    pJaune = new JPanel(); 
		    pJaune.setBackground(Color.yellow); 
		    add(pRouge, "North");  
		    add(pBleu, "Center"); // ou : add(pBleu); 
		    add(pJaune, "East");  
		    pRouge.setPreferredSize(new Dimension(400,100)); 
		    pJaune.setPreferredSize(new Dimension(100,200)); 
		    b1 = new JButton("b1"); pJaune.add(b1); 
		    b2 = new JButton("b2"); pJaune.add(b2); 
		    b3 = new JButton("b3"); pJaune.add(b3); 
		    inputTexte = new JTextField("Saisir Texte",20); 
		    pBleu.add(inputTexte); 
	    */
	    pack();  
	}

}
