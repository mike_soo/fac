package lecteurFichier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EntierNaturel {
	private int n;
	private int bonneSaisie=0;
	public EntierNaturel() throws NumberFormatException, EntierNaturelException, IOException 
	{	
		System.out.println("rentrez un entier naturel ");
		setN();
	}
	
	public void setN() throws EntierNaturelException, NumberFormatException, IOException
	{
		BufferedReader clavier= new BufferedReader (new InputStreamReader(System.in));
		do
		{
			n=Integer.parseInt(clavier.readLine());
			
			try 
			{
				if (n<0)
				{
					throw new EntierNaturelException();
				}
			}
			catch (EntierNaturelException e)
			{	
				bonneSaisie=0;
				System.out.println("rentrez un entier naturel valide");
			}
		}
		while(bonneSaisie==0);
	}
}
