package lecteurFichier;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LireFichier
{
	
	private int fcorect=0;
	private Scanner sc=new Scanner(System.in);
	
	private String m;
	public LireFichier()
	{	
		System.out.println("Rentrez le nom du fichier a afficher:");
		m = sc.nextLine();
	}
	
	public void Lfichier() throws IOException
	{ 
		do
		{
			try(BufferedReader bufferedReader = new BufferedReader(new FileReader(m)))
			{ 
				
				String line=null;
				while((line=bufferedReader.readLine())!=null)
				{
					System.out.println(line);
				}
				fcorect=1;
				
			}
			
			catch(IOException ioe)
			{
				System.out.println("Rentrez le un fichier a afficher valide:");
				m = sc.nextLine();
			}
		}
		while(fcorect==0);
			
	}
}