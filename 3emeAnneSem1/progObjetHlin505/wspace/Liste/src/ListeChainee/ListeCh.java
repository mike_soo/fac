package ListeChainee;

public class ListeCh {
	private Node node;
		private class Node
		{
			String nom;
			ListeCh succ=null;
			
		}
	public ListeCh(String nom,ListeCh succ)
	{
		this.node.nom=nom;
		this.node.succ=succ;
		
	}
	
	public ListeCh(String nom)
	{
		this.node.nom=nom;
		this.node.succ=null;
		
	}
		
		
	public String getNomTete() 
	{
		return node.nom;
	}
	
	public ListeCh getSucc()
	{
		return node.succ;
	}
	
	public ListeCh reverser(ListeCh lsh)
	{
		if  (node.succ==null)
		{
			return lsh;
		}
		else 
		{
			return new ListeCh(reverser(lsh.getSucc()).getNomTete(),lsh.getSucc());
		}
	}
	
	
}
