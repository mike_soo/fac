package anotation;

import java.lang.reflect.Method;import java.util.function.ToDoubleBiFunction;

public class Anotations {
	private static void afficherTodos(Class[] classes)
	{
		for (Class c : classes)
		{
			Method[] methodes=c.getMethods();
			for(Method m : methodes)
			{
				if(m.isAnnotationPresent(Todo.class))
				{
					System.out.println("- Methode : "+m.getName()+"() de la classe : "+c.getName());
					Todo a=m.getAnnotation(Todo.class);
					System.out.println("\t- Todo: "+ a.kind()+", version="+a.version()+",duration="+a.duration());
					
				}
			}
		}
	}
}
