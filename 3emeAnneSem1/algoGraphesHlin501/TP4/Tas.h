#ifndef TAS_H
#define TAS_H

#include <iostream>
#include <sstream>
#include <iomanip>      // std::setw


using namespace std;

typedef int indDTS; //indiceDansTableauSommet; (racourcie)

class ArbreParfait
{
  public:
  int IndicePremierSommetLibre;
  int hauteur;
  int* tasVsommet; //indique a qu'elle indice des table inter et traite se trouve x
  int* sommetVtas;
  unsigned int* d;
  unsigned int* dfinal;
  bool* traite;
  int n; //taille maximal du tableau qui est calcul� a partir de l'hauteur max donn�e au parametre du constructeur
  void Echanger(indDTS,indDTS);

  public:
  ArbreParfait(int n);
// on passe la hauteur max de l'arbre, un arbre r�duit �sa racine �tant de hauteur 0
  int getContenu(int ind);

 int AjouteSommetArbreParfait(unsigned int);
// renvoie -1 si l'ajout a �chou�
 

bool SommetValide(indDTS);

indDTS Racine();
bool FeuilleP(indDTS);
indDTS Pere(indDTS);
indDTS FilsGauche(indDTS);
indDTS FilsDroit(indDTS);
indDTS min2Ind(indDTS,indDTS); //renvoi l'indice contenant la valeur minimum dans contenue des ind pass�
                                //en parametre. Les deux ind sont supos� frere. Si l'indice droit
                                // n'existe pas alors l'indice gauche est renvoy�.

void SupprimerArbreParfait(indDTS);

};


class Tas : public ArbreParfait {
  public:
  Tas(int);

  void Remonter(indDTS); //remonte une valeur recursivement jusqu'a attendre sa position
                          //corecte dans le tas
  void Descendre(indDTS); //<-descendre recursive et descendre 

  int SupprimerTas(indDTS);//suprime UN element du tas puis rajoute le dernier element
                            //du tas a l'indice indDTS (donc l'indice de l'element suprim�)
                            //puit les fonction Remonter et descendre sont appel� a l'indice
                            //indDTS pour bien remetre a la place l'element qui remplace l'element suprim�

  void AjouterTas(unsigned int ); //ajoute un element a la fin du tas puis avec la fonction
                        //AjouteSommetArbreParfait(int) puis on fait appel a la fonction
                        //Remonter(..) pour metre cette nouvelle valeur dans sa position 
                        //corecte 
  

  int ConsulterDisTas(int ind);
  void ModifDistTas(int ind,unsigned int dis);

  


  


  //int Supmin(); je ne l'utilise pas pour le trie par tas, le trie
  // est quand meme effectu� dans triPartas en suprimant le min (donc la racine)
  // du tas pour ensuite le stock�e dans le premier indice du tableau tri�e

  
  int existeSomNT();
  

};

int* triPartas(Tas &T); //renvois un tableau d'entiers tri� a partir d'un tas
void afficherTas(Tas *T); //affiche un tas avec ces tasVsommet

#endif
