
#include <iomanip>      // std::setw
#include <math.h>       /* pow */

#include "Tas.h"
using namespace std;

ArbreParfait::ArbreParfait(int n)
{  
	this->n=n;


	IndicePremierSommetLibre=0;
	traite=new bool[this->n];
	d=new unsigned int[this->n];
	dfinal=new unsigned int [this->n];
	tasVsommet=new  int [this->n];
	sommetVtas=new int [this->n];
	for(int i=0;i<this->n;i++)
	{
		d[i]=-1;
		dfinal[i]=0;
		traite[i]=false;
		tasVsommet[i]=i;
		sommetVtas[i]=i;
	}
}

void ArbreParfait::Echanger (indDTS ind1,indDTS ind2)
{
	int temp;
	bool tempb;
	temp=d[ind1];
	d[ind1]=d[ind2];
	d[ind2]=temp;

	
	sommetVtas[tasVsommet[ind2]]=ind1;
	sommetVtas[tasVsommet[ind1]]=ind2;


	temp=tasVsommet[ind1];
	tasVsommet[ind1]=tasVsommet[ind2];
	tasVsommet[ind2]=temp;

}

int ArbreParfait::AjouteSommetArbreParfait(unsigned int dist)
{
	if(IndicePremierSommetLibre<n)
	{
		tasVsommet[IndicePremierSommetLibre]=IndicePremierSommetLibre;
		d[IndicePremierSommetLibre]=dist;
		IndicePremierSommetLibre++;
		return 1;
	}

	else
		return -1;
}

bool ArbreParfait::SommetValide(indDTS ind)
{
	if(ind<IndicePremierSommetLibre )
	{
		return true;
	}

	else
	{
		return false;
	}
}

indDTS ArbreParfait::Racine()
{
	return 0;
}



bool ArbreParfait::FeuilleP(indDTS ind)
{
  if((ind<IndicePremierSommetLibre) && (FilsGauche(ind)==-1) && (FilsDroit(ind)==-1))
  	return true;

  else if (ind<IndicePremierSommetLibre)
  	return false;
  else
  {

	  cout<<"ind="<<ind<<endl;
  	cout<<"IndicePremierSommetLibre depacé dans FeuilleP"<<endl;
  	return -1;
  }
}

indDTS ArbreParfait::FilsGauche(indDTS ind)
{
	if(((2*ind)+1)<IndicePremierSommetLibre)
	{
		return ((2*ind)+1);
	}
	else 
	{
		return -1;
	}

}

indDTS ArbreParfait::FilsDroit(indDTS ind)
{
	if(((2*ind)+2)<IndicePremierSommetLibre)
	{
		return ((2*ind)+2);
	}
	else 
	{
		return -1;
	}

}

indDTS ArbreParfait::min2Ind (indDTS FilsG,indDTS FilsD)
{
	if (FilsD==-1)
	{
		return FilsG;
	}

	else if(d[FilsG]>d[FilsD])
	{
		return FilsD;
	}
	
	else
	{
		return FilsG;
	}
}

indDTS ArbreParfait::Pere(indDTS ind)
{
	if(ind==0)
	{
		return -1;
	}

	else if(SommetValide(ind))
	{
		if(((int)((ind-1)/2))>=0)
			return ((int)((ind-1)/2));
		
		else
		{ 
			cout<<"erreur dans AP::Pere"<<endl; 
			return -1;
		}
	}

	else 
	{
		cout<<"depacement de IndicePremierSommetLibre dans Pere"<<endl;
		return -1;
	}
}

void ArbreParfait::SupprimerArbreParfait(indDTS ind)
{
	if(ind>0)
	{
	   Echanger(ind,IndicePremierSommetLibre-1);
	   IndicePremierSommetLibre--;
	}

	else 
		IndicePremierSommetLibre--;
}


Tas::Tas(int h) : ArbreParfait(h)
{
 	
}

void Tas::Remonter (indDTS ind)
{
	if(ind == 0)
	{

	}

	else 
	{
		if(d[ind]<d[Pere(ind)])
		{
			Echanger(ind,Pere(ind));
			Remonter(Pere(ind));
		}

	}

}

void Tas::Descendre(indDTS ind)
{
	if(FeuilleP(ind))
	{

	}
	else 
	{   indDTS FilsMin = min2Ind(FilsGauche(ind),FilsDroit(ind));
		if(d[ind]>d[FilsMin])
		{
			Echanger(ind,FilsMin);
			Descendre(FilsMin);
		}
	}
}

int Tas::SupprimerTas(indDTS ind) //en general  ind=0;
{
	/*
	 * cout<< "ind dant supTas : "<<ind<<endl;
	 * cout<<"tVs dans supTas : "<<tasVsommet[ind]<<endl;
	 */
	if(IndicePremierSommetLibre<=0)
	{	
		cout<<"erreur dans SupprimerTas"<<endl;
		return -1;
	}

	

	dfinal[tasVsommet[ind]]=d[ind];
	traite[tasVsommet[ind]]=true;
	
	
	
	int Sx=tasVsommet[ind];
	
	Echanger(ind,IndicePremierSommetLibre-1);
    IndicePremierSommetLibre--;
    if(IndicePremierSommetLibre!=0)
	    Descendre(ind);

	if(ind!=0)
		Remonter(ind);
	

	
	return Sx;
	
	
}

void Tas::AjouterTas(unsigned int dist)
{
	if(IndicePremierSommetLibre<n)
	{
		tasVsommet[IndicePremierSommetLibre]=IndicePremierSommetLibre;
		d[IndicePremierSommetLibre]=dist;
		IndicePremierSommetLibre++;
		Remonter(IndicePremierSommetLibre-1);
		
	}

	else
		cout<<"Erreur"<<endl;
	
}



int Tas::ConsulterDisTas(int sx)
{
	if (sx>=n || sx<0)
	{
		cout<<"Erreur dans ConsulterDisTas"<<endl;
		return -1;
	}

	else
	{
		return d[sommetVtas[sx]];	
	}
}

void Tas::ModifDistTas(int sx,unsigned int dis)
{
	if (sx>=n || sx<0)
	{
		cout<<"Erreur dans ModifValTas"<<endl;
	}

	else
	{
		//cout<<"je modifie le s "<<sx<<" qui a la pos "<<sommetVtas[sx]<<" dans le tas"<<endl;
		d[sommetVtas[sx]]=dis;
		Remonter(sommetVtas[sx]); //on ne fait que remonter car la distance 
								 //qu'on affecte sera toujour inf a celle existante
		//afficherTas(this);
	}
}

int Tas::existeSomNT() //renvois un sommet non traité dans traite[] avec d minimal
{
	if( IndicePremierSommetLibre!=0 && d[0]!=-1)
	{
		//cout << "on suprime tas[0]" << endl;
		int s = SupprimerTas(0);
		traite[s]=true;
		//afficherTas(this);
		return s;
	}

	return -1;



}



void afficherTas(Tas* T)
{
	cout<<setw(11)<<"i : ";
	for(int i=0;i<T->n;i++)
	{
		if(i>=T->IndicePremierSommetLibre)
			cout<<"\e[31;1m";

		cout<<setw(11)<<i;

	}
	cout<<"\e[0m";
	cout<<endl;
	cout<<setw(11)<<"sVt : ";
	for(int i=0;i<T->n;i++)
	{
		if(i>=T->IndicePremierSommetLibre)
			cout<<"\e[31;1m";
		cout<<setw(11)<<T->sommetVtas[i];
	}
	cout<<"\e[0m";
	cout<<endl;

	cout<<setw(11)<<"tVs : ";
	for(int i=0;i<T->n;i++)
	{
		if(i>=T->IndicePremierSommetLibre)
			cout<<"\e[31;1m";
		cout<<setw(11)<<T->tasVsommet[i];
	}
	cout<<"\e[0m";
	cout<<endl;


	cout<<setw(11)<<"d : ";
	for(int i=0;i<T->n;i++)
	{
		if(i>=T->IndicePremierSommetLibre)
			cout<<"\e[31;1m";
		cout<<setw(11)<<T->d[i];
	}
	cout<<"\e[0m";
	cout<<endl;


}




int* triPartas(Tas &T)
{   int i=0;
	indDTS IndPSL=T.IndicePremierSommetLibre; 
	int * trie=new int [IndPSL];
	

	while (IndPSL>0)
	{   
		trie[i]=T.d[0];
		T.SupprimerTas(0);
		i++;
		IndPSL--;

		
	}
	

	return trie;
}


/*
int getd(int ind)
{
	if (ind <IndicePremierSommetLibre && ind >=0)
	{
		return d[ind];
	}

	else
	{
		return -1;
		cout<<"erreur acces a un element non existant"<<endl;
	}
}*/