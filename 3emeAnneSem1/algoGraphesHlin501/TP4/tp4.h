#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <iomanip>  
#include <ctime>

using namespace std;

typedef struct coord{int x; int y;} coord;

void pointrandom(int n,coord point[]);
void voisins(int n, int dmax, coord point[], vector<int> voisins[]);
void dijsktra (int n, vector <int> voisin[],coord point[], int pere[],unsigned d[]);
unsigned int l(int Sx,int Sy,coord point[]);
int existeSomNT(int n, bool traite[], unsigned int d[]); //renvois l'ind s'il existe d'un sommet non traité dans traite[] sinon renvois -1
void AffichageGraphique(int n, coord point[], vector <int> voisins[]);
int construirearbre(int n,int arbre[][2],int pere[]);
void AffichageGraphique1(int n,int k,coord point[],int arbre[][2]);
void trieTas(int n, bool traite[], unsigned int d[]);
void dijsktraAvTas (int n, vector <int> voisin[],coord point[], int pere[]);