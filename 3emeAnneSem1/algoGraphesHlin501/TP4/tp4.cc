#include "tp4.h"

void pointrandom(int n,coord point[])
{
	for (int i=0;i<n;i++)
	{
		point[i].x=rand()%612;
		point[i].y=rand()%792;
	
	}


	/*
	for(int i=0;i<n;i++)
	{
		cout<<"x"<<i<<"="<<point[i].x<<" y"<<i<<"="<<point[i].y<<endl;
	}*/
}


void voisins(int n, int dmax, coord point[], vector<int> voisins[])
{	

	
	int d;
	for(int i=0;i<n;i++)
	{	
		for(int ii=i+1;ii<n;ii++)
		{
		
			if((d=pow((point[ii].x-point[i].x),2)+pow((point[ii].y-point[i].y),2)) < dmax)
			{
			
				voisins[i].push_back(ii);
				voisins[ii].push_back(i);

			}
			

		}
	}
	/*
	for(int i=0;i<n;i++)
	{
		cout<<"s"<<i<<": ";
		for(int j=0;j<voisins[i].size();j++)
		{
			cout << voisins[i][j]<<" ";

		}
		cout<<endl;
	}*/
	
	
}

void dijsktra (int n, vector <int> voisin[],coord point[], int pere[],unsigned d[])
{
	bool traite[n]; //pour marquer les sommets traités
	int Sx; //Sommet x
	int Sy;

	for (int i=0;i<n;i++)
	{
		d[i]=-1;
		traite[i]=false;
	}
	pere[0]=0;
	d[0]=0;
	
	while((Sx=existeSomNT(n,traite,d))>=0)
	{
		traite[Sx]=true ;
		//cout<<"Sx="<<Sx<<" nb de voisin[Sx]="<<voisin[Sx].size()<<endl;
		
		for(unsigned int i=0;i<voisin[Sx].size();i++)
		{	
			Sy=voisin[Sx][i];
			//traite[Sy]? cout<<"true"<<endl : cout<<"false"<<endl;

			if(!traite[Sy])
			{	
				//cout<<"d[sy]="<<d[Sy]<<endl;
				if(d[Sy] > (d[Sx] + l(Sx,Sy,point)))
				{
					/*cout<<"d[i]"<<endl;
					for(int i=0;i<n;i++)
					{
						cout<<d[i]<<"|"<<setw(12);
					}
					cout<<endl;
					cout<<"traite[i]"<<endl;
					for(int i=0;i<n;i++)
					{
						cout<<traite[i]<<"|"<<setw(12);
					}
					cout<<endl;
					cout<<"je boucle"<<endl;
					*/
					
					d[Sy]=d[Sx]+l(Sx,Sy,point);
					pere[Sy]=Sx;
					
			
				}	
			}

		}

	}
	/*
    cout<<"dijsktraSansTas"<<endl;

    cout<<"i / ";

    for(int i=0;i<n;i++)
	{
		cout<<i<<"|"<<setw(3);
	}
	cout<<endl<<"pi/ ";
	for(int i=0;i<n;i++)
	{
		cout<<pere[i]<<"|"<<setw(3);
	}
	cout<<endl;
	*/


}

unsigned int l(int Sx,int Sy,coord point[])
{
	return (unsigned int)pow((point[Sy].x-point[Sx].x),2)+pow((point[Sy].y-point[Sx].y),2);
}

int existeSomNT(int n, bool traite[], unsigned int d[]) //renvois true s'il existe un sommet non traité dans traite[]
{	
	int indDmin;
	bool existe=false;
	for(int i=0;i<n;i++)
	{
		if(traite[i]==0)
		{
			indDmin=i;
			break;
		}
	}	
	for (int i=0;i<n;i++)
	{
		if((traite[i]==0) && d[i]<=d[indDmin] ) 
		{	
			existe=true;
			indDmin=i;
		}
	}
	if(existe && d[indDmin]!=-1)
		return indDmin;

	return -1;
}



int construirearbre(int n, int arbre[][2],int pere[])
{
	int k=0;
	for (int i=0;i<n;i++)
	{
		if(pere[i]!=-1)
		{	
			arbre[k][0]=i;
			arbre[k][1]=pere[i];
			k++;
		}
	}
	/*
	for(int i=0;i<k;i++)
	{
		cout<<arbre[i][0]<<" | "<<arbre[i][1]<<endl;
	}
	*/
	return  k;
}