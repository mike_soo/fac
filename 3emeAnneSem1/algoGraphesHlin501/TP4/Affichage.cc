
#include "tp4.h"
          
void AffichageGraphique(int n, coord point[], vector <int> voisins[])       // Cree le fichier Exemple.ps qui affiche
                                                                     // les points et l'arbre de Kruskal.
{  
   ofstream output;                           
   output.open("Graphe.ps",ios::out);
   output << "%!PS-Adobe-3.0" << endl;
   output << "%%BoundingBox: 0 0 612 792" << endl;
   output << endl;  
   
   for(int i=0;i<n;i++)
   {  if(i==0)
         output << point[i].x << " " << point[i].y << " 6 0 360 arc" <<endl;
      else
         output << point[i].x << " " << point[i].y << " 4 0 360 arc" <<endl;
      
      output << "0 setgray" <<endl;
      output << "fill" <<endl;
      output << "stroke"<<endl;
      output << endl;
      
   }
   output << endl;

   for(int i=0;i<n;i++) //n*(n-1)/2
   {  
      for(int j=0;j<voisins[i].size();j++)
      {
         output << point[voisins[i][j]].x << " " << point[voisins[i][j]].y 
	      << " moveto" << endl;
         output << point[i].x << " " << point[i].y
	      << " lineto" << endl;
         output << "stroke" << endl;
         output << endl;
      }
   }
   output << "showpage";
   output << endl;
}

void AffichageGraphique1(int n,int k,coord point[],int arbre[][2])
{
    ofstream output;                           
   output.open("Arbre.ps",ios::out);
   output << "%!PS-Adobe-3.0" << endl;
   output << "%%BoundingBox: 0 0 612 792" << endl;
   output << endl;  
   for(int i=0;i<k;i++)
   {  
      output << point[arbre[i][0]].x << " " << point[arbre[i][0]].y<< " 3 0 360 arc" <<endl;
      output << "0 setgray" <<endl;
      output << "fill" <<endl;
      output << "stroke"<<endl;
      output << endl;

      output << point[arbre[i][1]].x << " " << point[arbre[i][1]].y<< " 3 0 360 arc" <<endl;
      output << "0 setgray" <<endl;
      output << "fill" <<endl;
      output << "stroke"<<endl;
      output << endl;
   }
   for(int i=0;i<k;i++)
   {  
      output << point[arbre[i][0]].x << " " << point[arbre[i][0]].y
      << " moveto" << endl;
      output << point[arbre[i][1]].x << " " << point[arbre[i][1]].y
      << " lineto" << endl;
      output << "stroke" << endl;
      output << endl;
   }
   output << "showpage";
   output << endl;
}