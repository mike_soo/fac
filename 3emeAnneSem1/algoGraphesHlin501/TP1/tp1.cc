#include <cstdlib>
#include <iostream>
#include <vector>
#include <ctime>

using namespace std;


void grapherandom(int n,int m,int edge[][2])
{
	
	for (int i=0;i<m;i++)
	{

			edge[i][0]=rand()%n;
			edge[i][1]=rand()%n;
			//cout << " " << edge[i][0] << " "  << edge[i][1] << "\n";
	}

}

void composante (int n,int m,int edge[][2],int comp[])
{
	for (int i=0;i<n;i++)
	{
		comp[i]=i;
	}
	
	for (int i=0;i<m;i++)
	{
		if (comp[edge[i][0]] != comp[edge[i][1]] )
		{
			int compox = comp[edge[i][0]];
			int compoy = comp[edge[i][1]];
			
			for (int j=0;j<n;j++)
			{
				if(comp[j] == compox)
				{
					comp[j] = compoy;
				}
			}

		}
	}
	/*cout<<"comp[]= ";
	
	for (int i=0;i<n;i++) 
		cout << comp[i] << " ";

	cout<<endl;
	*/
}


void arretppaG(int m, int edge[][2]) //dans le tableau edge, on remet tout les valeur des arret les plus petit a gauche
{	int aux;
	for(int i=0;i<m;i++)
	{
		if(edge[i][0]>edge[i][1])
		{
			aux=edge[i][0];
			edge[i][0]=edge[i][1];
			edge[i][1]=aux;
		}
	}
}

void composante2 (int n,int m,int edge[][2],int comp[])
{	
	vector<vector <int> > vect(n);
	
	//cout<<"vect:"<<endl;
	for(int i=0;i<n;i++)
	{
		vect[i]=vector<int> (1);
		vect[i][0]=i;
		//cout<<vect[i][0]<<endl;
	}
	//cout<<endl;
	


	for (int i=0;i<n;i++)
	{ comp[i]=i; }
	

	
	
	for (int i=0;i<m;i++)
	{	

		if (comp[edge[i][0]] != comp[edge[i][1]] )
		{ 	
			int compox = comp[edge[i][0]];
			int compoy = comp[edge[i][1]];
			
			if(vect[compoy].size()>vect[compox].size())
			{
				int inter=compox;
				compox=compoy;
				compoy=inter;
			}

			int tailleComp=vect[compoy].size();
		
			//cout<<"taille="<<tailleComp<<endl;
			//cout<<"i="<<i<<endl;
			
			for (int k=(tailleComp-1);k>=0;k--)
			{	
				//cout<<"k="<<k<<","<<endl;
				
				
				comp[vect[compoy][k]] = compox ;
				
				
				
				
				
				vect[compox].push_back(vect[compoy][k]);

				vect[compoy].pop_back();
				


				//affichage console pour comprendre algo
				/*cout<<"k="<<k<<endl;
				cout<<"comp"<<vect[compoy][k]<<"="<<compox<<endl;
				cout<<"comp[]= ";
	
				for (int i=0;i<n;i++) 
					cout << comp[i] << " ";

				cout<<endl;
				cout<<"vect"<<compox<<".push_back"<<vect[compoy][k]<<endl;;

				cout<<"vect= "<<endl;
			    for (int i=0;i<n;i++) 
					{	
						cout<<"comp"<<i<<" contient: ";
						for(unsigned int j=0; j<vect[i].size(); j++)
							cout << vect[i][j] << " ";
						cout<<endl;
					}
				*/
			}

		}
	}
	
	/*
	cout<<"comp[]= ";
	
	for (int i=0;i<n;i++) 
		cout << comp[i] << " ";

	cout<<endl;
	cout<<"vect= "<<endl;
    for (int i=0;i<n;i++) 
		{	
			cout<<"comp"<<i<<" contient: ";
			for(unsigned int j=0; j<vect[i].size(); j++)
				cout << vect[i][j] << " ";
			cout<<endl;
		}
	
	*/
	
	
}


int ecrituretailles(int n,int m,int comp[]) //renvois le nombre de points isolées
{	
	int taillecompt[n]; // tableau ou chaque indice i correspond a la taille de composante[i]
	int combCompodeTailleX[n+1]; //tableau contenant le nombre de composant par taille 
	
	for (int i=0;i<=n;i++)
	{	if(i<n)
			taillecompt[i]=0;
		combCompodeTailleX[i]=0;
	}
	
	for (int i=0;i<n;i++)
	{
		taillecompt[comp[i]]++;
	}

	/*cout<<"taillecompt[]= ";
	for (int i=0;i<n;i++) 
		cout<<taillecompt[i] << " ";
	cout<<endl;*/

	for(int i=0;i<n;i++)
	{
		combCompodeTailleX[taillecompt[i]]++;
	}
	

	//cout<<"combCompodeTailleX[]= ";
	//for (int i=0;i<n;i++) 
		//cout<<combCompodeTailleX[i] << " ";
	//cout<<endl;

	/*
	for(int i=1;i<=n;i++)  //affiche les frases demander sur le sujet
	{	

		if(i==1)
			cout<<"il y a "<<combCompodeTailleX[i]<<" points isolés "<<endl;
		
		else if(combCompodeTailleX[i]!=0)
		{	
			cout<<"il y a "<<combCompodeTailleX[i]<<" composante de taille "<<i<<endl;
		}
	}
	*/
	
	return combCompodeTailleX[1];
}

void moyennes(int n,int m,int edge[][2],int comp[],int itr)
{
	int nbrTotalPI=0; //nombre total de points isolées
	
	for (int i=0;i<itr;i++)
	{	
		for (int i=0;i<m;i++)
		{

			edge[i][0]=rand()%n;
			edge[i][1]=rand()%n;
			//cout << " " << edge[i][0] << " "  << edge[i][1] << "\n";
		}
	

		composante2(n,m,edge,comp);
		nbrTotalPI+=ecrituretailles(n,m,comp);
	}
	cout<<" n="<<n<<" m="<<m<<" nbrItr="<<itr<<" moy de points isolées="<<(float)nbrTotalPI/(float)itr<<endl;;
}
int main(){ 
   	srand(time(NULL));
    
    int n;     // Nombre de sommets.	
    int m;     // Nombre d'aretes.	
    
    cout << "Entrer le nombre de sommets:";
    cin >> n;
    cout << "Entrer le nombre d'aretes:";
    cin >> m;
    int edge[m][2];    // Tableau des aretes.
    int comp[n];    // comp[i] est le numero de la composante contenant i.
    
   	/*
    int edge[5][2]={{2,1},{2,3},{5,4},{4,0},{3,5}};
    int comp[6];
    n=6;
    m=5;
    */
    
    grapherandom(n,m,edge);
    //composante(n,m,edge,comp); 
    composante2(n,m,edge,comp); //algo composante opitmisée
    ecrituretailles(n,m,comp);
    
    cout<<"composante1Exec"<<endl;
    double t1=clock();

   
   	grapherandom(n,m,edge);
   	composante(n,m,edge,comp);
   	
   	double t2=clock();
  	
  	
  	cout<<"composante2Exec"<<endl;
  	double t3=clock(); 	
   	
   	grapherandom(n,m,edge);
   	composante2(n,m,edge,comp);
   	
   	double t4=clock();

   	cout<<"temps d'execution composante1="<<(float)(t2-t1)<<endl;
   	cout<<"temps d'execution composante2="<<(float)(t4-t3)<<endl;
	
    //moyennes(n,m,edge,comp,50);  //test sur la moyenne des points isolées sur 50 appel de fonction composante.

    return 0;
}