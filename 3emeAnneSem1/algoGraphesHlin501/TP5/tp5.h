#ifndef tp5_H
#define tp5_H
#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include <unordered_map>

using namespace std;
const int n=5;
const int inf=9999;                    //La valeur infinie.
const int n1=6;
const int nx=6;
const int nv=41;
using namespace std;

void floyd_warshall(int longueur[][n], int dist[][n],int chemin[][n]);
void itineraire (int i, int j, int chemin[][n]);
void fermeturetransitive(int arc[][n1],int fermeture[][n1]);
void compfortconnexe( int fermeture[][nx]);
void itineraireVilles(string v1,string v2, unordered_map<string,int> vilPos,int chemin[][41], char villes[41][20]);
void floyd_warshallpvilles(int longueur[][nv],int dist[][nv],int chemin[][nv]);

#endif