#include "tp5.h"


void floyd_warshall(int longueur[][n],int dist[][n],int chemin[][n])
{
    cout<<"AFFICHAGE DE LONGUEUR POUR EXERCICE 2"<<endl;
    cout<<setw(7)<<"ij";
    for(int i=0;i<n;i++)
    {
        cout<<setw(7)<<i;
    }
    cout<<endl;
    for (int i = 0; i < n; i++)
    {
        cout<<setw(7)<<i;
        for (int j = 0; j < n; j++)
        {
            cout<<setw(7)<<longueur[i][j];
        }
        cout<<endl;
    }




    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (longueur[i][j] != inf) //dist[i][j]!=inf <=> ij appartient a A
            {
                dist[i][j] = longueur[i][j];
                chemin[i][j] = j;
            }
            else
            {
                dist[i][j] = inf;
                chemin[i][j] = -1;
            }

        }
    }


    for(int k=0;k<n;k++)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if(dist[i][j]>(dist[i][k]+dist[k][j]))
                {
                    dist[i][j]=dist[i][k]+dist[k][j];
                    chemin[i][j]=chemin[i][k];
                }
            }
        }

    }

    for(int i=0;i<n;i++)
    {
           if(dist[i][i]<0)
           {
                cout<<"Il existe un cycle orienté de pois <0";
           }
    }
    /*
     * Mise en forme d'un affichage de dist!
     */
    cout<<"AFFICHAGE DE DIST POUR EXERCICE 2"<<endl;
    cout<<setw(7)<<"ij";
    for(int i=0;i<n;i++)
    {
        cout<<setw(7)<<i;
    }
    cout<<endl;
    for (int i = 0; i < n; i++)
    {
        cout<<setw(7)<<i;
        for (int j = 0; j < n; j++)
        {
            cout<<setw(7)<<dist[i][j];
        }
        cout<<endl;
    }
}


void itineraire (int i, int j, int chemin[][n])
{

    int s=i;
    string chem="";
    cout<<" le chemin est: ";
    chem+=to_string(s) +" ";
    while(j != s)
    {
        s=chemin[s][j];
        if(s==-1)
        {
            cout<<" le chemin n'existe pas!"<<endl;
            return;
        }
        else
        {
            chem+=std::to_string(s) +" ";
        }
    }
    cout<<chem<<endl;

}


void fermeturetransitive(int arc[][n1],int fermeture[][n1])
{

    cout<<"AFFICHAGE DE ARC POUR EXERCICE 4"<<endl;
    cout<<setw(7)<<"ij";
    for(int i=0;i<n1;i++)
    {
        cout<<setw(7)<<i;
    }
    cout<<endl;
    for (int i = 0; i < n1; i++)
    {
        cout<<setw(7)<<i;
        for (int j = 0; j < n1; j++)
        {
            cout<<setw(7)<<arc[i][j];
            fermeture[i][j]=0;
        }
        cout<<endl;
    }

    for (int i = 0; i < n1; i++)
    {
        for (int j = 0; j < n1; j++)
        {
            if (arc[i][j]) //dist[i][j]!=inf <=> ij appartient a A
            {
                fermeture[i][j] = 1;
            }

        }
    }
    //int x=0;
    for(int k=0;k<n1;k++)
    {
        for (int i = 0; i < n1; i++)
        {
            for (int j = 0; j < n1; j++)
            {
                /*
                cout<<setw(5)<<x++;
                cout<<"\e[31;1m["<<i<<"]"<<"["<<j<<"] ";
                cout<<"\e[36m["<<i<<"]"<<"["<<k<<"] ";
                cout<<"\e[32m["<<k<<"]"<<"["<<j<<"] "<<endl;
                  */
                /*
                if((arc[i][k] && arc[k][j]))
                {
                    int jj=i;
                    fermeture[i][j]=1;
                    for (int ii=0;ii<n;ii++)
                    {
                        if(arc[ii][jj])
                        {
                            arc[ii][j]=1;
                        }
                    }
                }*/

                if(fermeture[i][j] && fermeture[j][k] )
                {
                    fermeture[i][k]=1;
                }

            }
            //cout<<endl;
        }
        //cout<<endl;
        //cout<<endl;

    }


    cout<<"AFFICHAGE DE FERMETURE POUR EXERCICE 4"<<endl;
    cout<<setw(7)<<"ij";
    for(int i=0;i<n1;i++)
    {
        cout<<setw(7)<<i;
    }
    cout<<endl;
    for (int i = 0; i < n1; i++)
    {
        cout<<setw(7)<<i;
        for (int j = 0; j < n1; j++)
        {
            cout<<setw(7)<<fermeture[i][j];
        }
        cout<<endl;
    }

}

void compfortconnexe( int fermeture[][nx])
{
    int comp[nx];
    vector<int> compFortementCon[nx];


    for (int i=0;i<nx;i++)
    {
        comp[i]=i;
        compFortementCon[i].push_back(i);
    }

    for(int i=0;i<nx;i++)
    {
        for (int j = 0; j < nx; j++)
        {
            if (fermeture[i][j] && fermeture[j][i] && comp[i]!=comp[j])
            {
                int compi=comp[i];
                int compj=comp[j];

                if(compFortementCon[compj].size()>compFortementCon[compi].size())
                {
                    int inter =compi;
                    compi=compj;
                    compj=inter;
                }
                int tailleComp=compFortementCon[compj].size();

                for(int k=(tailleComp-1);k>=0;k--)
                {
                    comp[compFortementCon[compj][k]]=compi;

                    compFortementCon[compi].push_back(compFortementCon[compj][k]);

                    compFortementCon[compj].pop_back();

                }
            }
        }
    }

    cout<<"les composantes fortement connexes sont:"<<endl;
    for (int i=0;i<nx;i++)
    {
        cout<<setw(7)<<"comp"<<i<<": ";
        for(int j=0;j<compFortementCon[i].size();j++)
        {
            cout<<setw(3)<<compFortementCon[i][j];
        }
        cout<<endl;
    }
}