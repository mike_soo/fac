#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <stdlib.h> 
#include <ctime>
using namespace std;

typedef struct coord{int x; int y;} coord;

typedef struct edge3{unsigned int p1; unsigned int p2;unsigned int d;}edge3;

void AffichageGraphique(int n, coord point[], unsigned int arbre[][2]);