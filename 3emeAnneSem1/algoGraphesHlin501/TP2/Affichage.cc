
#include "tp2.h"
          
void AffichageGraphique(int n, coord point[],  vector <int> voisins[])      // Cree le fichier Exemple.ps qui affiche
                                                                     // les points et l'arbre de Kruskal.
{  
   ofstream output;                           
   output.open("Exemple.ps",ios::out);
   output << "%!PS-Adobe-3.0" << endl;
   output << "%%BoundingBox: 0 0 612 792" << endl;
   output << endl;  
   
   for(int i=0;i<n;i++)
   {  
      output << point[i].x << " " << point[i].y << " 3 0 360 arc" <<endl;
      output << "0 setgray" <<endl;
      output << "fill" <<endl;
      output << "stroke"<<endl;
      output << endl;
   }
   output << endl;

   for(int i=0;i<n-1;i++) //n*(n-1)/2
   {
      output << point[arbre[i][0]].x << " " << point[arbre[i][0]].y 
	   << " moveto" << endl;
      output << point[arbre[i][1]].x << " " << point[arbre[i][1]].y
	     << " lineto" << endl;
      output << "stroke" << endl;
      output << endl;
   }
   output << "showpage";
   output << endl;
}
