#include "tp5.h"


int main(){
  int longueur[n][n]={
                    {0,2,inf,4,inf}, //Les longueurs des arcs.
                    {inf,0,2,inf,inf},   //long[i][j]=inf si l'arc ij n'existe pas
                    {inf,inf,0,2,inf},
                    {inf,-3,inf,0,2},
                    {2,inf,inf,inf,0}};
  int dist[n][n];                      //Le tableau des distances.
  int chemin[n][n];                    //Le tableau de la premiere etape du chemin de i a j.

  for (int i=0;i<n;i++)
  {
      for (int j = 0; j < n; j++)
      {
        dist[i][j] = longueur[i][j];
      }
  }

/*

    floyd_warshall(longueur,dist,chemin);
    int i,j;
    cout<<"Entrez le depart : ";
    cin>>i;
    cout<<endl;
    cout<<"Entrez la destination : ";
    cin>>j;
    cout<<endl;
    itineraire(i,j,chemin);

*/
/*
    int arc[n1][n1]={{0,0,0,1,0,1},//La matrice d'adjacence du graphe oriente D.
                   {1,0,1,1,0,0},
                   {0,0,0,1,0,0},
                   {0,0,0,0,1,1},
                   {0,0,1,0,0,1},
                   {0,0,1,0,0,0}};

    int arc[n1][n1]={
                        {0,1,0,0,0},
                        {0,0,1,0,0},
                        {0,0,0,1,0},
                        {0,0,0,0,1},
                        {1,0,0,0,0},
                    };*/
    int arc[n1][n1]=
            {{0,1,0,0,0,0},//La matrice d'adjacence du graphe oriente D.
             {0,0,1,0,0,1},
             {0,0,0,0,1,0},
             {0,0,0,1,0,0},
             {0,0,1,0,0,0},
             {0,0,0,0,0,0}};

    int fermeture[n1][n1];         // La matrice de la fermeture transitive de D.
    fermeturetransitive(arc,fermeture);
    compfortconnexe(fermeture);

    return 0;
}


