
#include "tp4.h"

int main(int argc,char **argv)
{
	srand(time(NULL));
	int n;
	cout<<"rentrer n:"<<endl;
	cin>>n;
	int cpt=0;
	int puis10=1;
	int dmax=0;
	        	        //Le nombre de points.
	int k=0;                   //Le nombre d aretes.
	

	             // La distance jusqu'a laquelle on relie deux points.

	vector<int> voisin[n];   // Les listes de voisins.          
	coord point[n];         // Les coordonnees des points.

	unsigned int d[n];                // La distance a la racine.
	int arbre[n-1][2];       // Les aretes de l'arbre de Dijkstra.
	int pere[n];             // La relation de filiation de l'arbre de Dijkstra.
	for (int i=0;i<n;i++)
	{
		pere[i]=-1;
		
	} 
	if (argc >1)
	{
		while (argv[1][cpt]!='\0')
		{
			cpt++;
		}

		cpt--;
		for(int i=cpt;i>=0;i--)
		{
			dmax+=((int)argv[1][i]-48)*puis10;
			puis10*=10;
		}	

		cout<<"dmax="<<dmax<<endl;
	}

	pointrandom(n,point);
	voisins(n,dmax, point, voisin);
	//AffichageGraphique(n, point, voisin);
    /*
	cout<<"graphe:"<<endl;
	for(int i=0;i<n;i++)
	{	
		cout<<i<<": ";
		for(int j=0;j<voisin[i].size();j++)
		{
			cout<<voisin[i][j]<<" ";
		}

		cout<<endl;

	}

	for(int i=0;i<n;i++)
	{
		cout<<i<<" : ("<<point[i].x<<","<<point[i].y<<")"<<endl;
	}
    */
    cout<<"DijsktraAvecTas Execution:"<<endl;

    double t1=clock();
    dijsktraAvTas(n,voisin,point,pere);
    double t2=clock();

    cout<<"DijsktraSansTas Execution:"<<endl;

    k=construirearbre( n,arbre,pere);
	AffichageGraphique1( n, k, point, arbre);

	for (int i=0;i<n;i++)
	{
		pere[i]=-1;

	}
    double t3=clock();
    dijsktra (n, voisin, point, pere, d);
    double t4=clock();

    cout<<"temps d'execution DijsktraAvecTas="<<(float)(t2-t1)<<endl;
    cout<<"temps d'execution DijsktraSansTas="<<(float)(t4-t3)<<endl;


	return 0;
}
