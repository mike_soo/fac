
#include "tp3.h"
using namespace std;



 
void ecritureniveaux(int n, int niveau[])
{	int niv=0;
	int effectifN[n];
	int nbSomHorsZero=0; //nombre de sommet qui ne sont pas dans la composante 0.
	
	for (int i=0; i<n ;i++)
	{
		effectifN[i]=0;
	}

	for (int i=0; i<n ;i++)
	{

		niv=niveau[i];
		if(niv!=-1)
			effectifN[niv]++;
		else
			nbSomHorsZero++;
	}

	for (int i=0;i<n;i++)
	{	
		if(effectifN[i]>0)
			cout<<"il y a "<<effectifN[i]<<" sommets au niveau "<<i<<endl;
	}
	cout<<"il y a "<<nbSomHorsZero<<" sommets qui ne sont pas dans la compostante de 0 "<<endl;
}

ledge ajoutElem(int x, int y , ledge listEl)
{

	ledge eNouv=new edge; 
	eNouv->x=x;
	eNouv->y=y;
	eNouv->prec=NULL;
	eNouv->suiv=NULL;
	
	//cout<<"eNouvx="<<eNouv->x<<" eNouvy"<<eNouv->y<<endl;
	if(listEl==NULL)
	{
		return eNouv;
		//cout<<"listEl==null"<<endl;
	}

	listEl->prec=eNouv;
	eNouv->suiv=listEl;

	listEl=eNouv;

	return listEl;

}

ledge creerlEdgeVide()
{
	ledge eNouv=NULL;
	return eNouv;
}

ledge gGraphComplet(int n, int m , ledge listEl)
{	
	
	
	int nbEl=0;
	
	for(int i=0;i<n;i++)
	{	
		for(int ii=i+1;ii<n;ii++)
		{
			
			listEl=ajoutElem(i,ii,listEl);
			nbEl++;
		}
	}
	
	ledge P=listEl;
	//cout<<"listEH GraphComplet final"<<endl;
	/*for (int i=0;i<nbEl;i++)
	{
		cout<<"i="<<i<<" x="<<P->x<<" y="<<P->y<<endl;
		P=P->suiv;
	}
	P=listEl;
	int i=0;
	while(P!=NULL)
	{
		cout<<"i="<<i<<" x="<<P->x<<" y="<<P->y<<endl;
		i++;
		P=P->suiv;
	}*/

	//cout<<endl<<endl;
	
	return listEl;


	/*
	cout<<"avantsqrt = "<<pow((point[ledge[0][0]].x-point[ledge[0][1]].x),2)+pow((point[ledge[0][0]].y-point[ledge[0][1]].y),2)<<endl;
	cout<<"distance entre ("<<point[ledge[0][0]].x<<","<<point[ledge[0][0]].y<<") et (" <<point[ledge[0][1]].x<<","<<point[ledge[0][1]].y<<")";
	cout<<"="<<ledge[0][2]<<endl; 
	*/

}

void voisinstrandom(int n, int m, ledge listEl, vector<int> voisins[])
{	
	
	int indPairXY;
	ledge P=listEl;
	
	int mtotal=m;
	int enleve=0;
	/*
	ledge Q=listEl;
	cout<<"Dans voisin random"<<endl;
	for (int i=0;i<(n*(n-1)/2);i++)
	{	
		cout<<"i="<<i<<"Qx="<<Q->x<<" Qy="<<Q->y<<endl;

		Q=Q->suiv;
	}*/	
	

	while (m>0)
	{	
		
		//cout<<"m="<<m<<endl;
		indPairXY=rand()%((n*(n-1)/2) - enleve); //on pioche une arrête dans le graphe complet d’après l'indice 
												//généré dans rand();
		/*
		* Puis on cherche cette arrête : 
		*/
		//cout<<"indPairXY"<<indPairXY<<endl;
		for (int i=0;i<indPairXY;i++)
		{	

			//cout<<"i="<<i<<"Px="<<P->x<<" Py="<<P->y<<endl;

			P=P->suiv;
		}
		//cout<<endl<<endl;
		voisins[P->x].push_back(P->y);
		voisins[P->y].push_back(P->x);
		
		if(P==listEl) //si on veut supprimer la première arrête de la listEL 
		{	
			if(P==NULL)
			{
				//erreur car cela veut dire qu'on cherche a utiliser une 
				//arrête en trop qui n'est plus
				// disponible dans le graphe complet stocker dans 
				//listEl ce qui veux dire que le graph complet et maintenant
				//vide et qu'on essaye de lui prendre encore des arrêtes. Ceci
				// normalement ne devrais pas se passer car le graphe
				//représenté par voisins est inclut ou égal a listEL
				cout<<"error"<<endl;
				return ;
			}
			if (listEl->suiv!=NULL)
			{
				listEl=listEl->suiv;
				delete listEl->prec;
				listEl->prec=NULL;
			}
			
			else
			{
				
				delete P;
				
			}
		}
		

		else
		{ 
			if(P->prec!=NULL)
			{
				P->prec->suiv=P->suiv;
				
			}
			if(P->suiv!=NULL)
			{
				P->suiv->prec=P->prec;
				
			}
			delete P;
		}
		
		m--;
		P=listEl;
		enleve++;
	}

	for (int i=0;i<n;i++)
	{	

		cout<<"s"<<i<<":";
		for (int j=0 ; j<voisins[i].size();j++)
		{
			cout<<voisins[i][j]<<" ";
		}
		cout<<endl;
	}


}

int tousVue(int n,bool dejaVue[])
{
	for (int i=0;i<n;i++)
	{
		if(dejaVue[i]==false)
			return i;
	
	}
	return -1;
}

void parcourslargeur(int n, vector <int> voisins [], int niveau[],int ordre[],int pere[])
{
	int r=0 ;	
	int ord=0;
	
	int vois;
	bool dejaVue[n];
	int snonvue;
	queue <int> file;
	file.push(r);

	for(int i=0; i<n;i++)
	{
		dejaVue[i]=false;
	}
	dejaVue[0]=true;
	
	pere[0]=-2;
	
	ordre[0]=0;
	ord++;
	
	niveau[0]=0;
	
	while (!file.empty())
	{
		
		for(int i=0;i<voisins[r].size();i++)
		{	
			
			vois=voisins[r][i];
			if(!dejaVue[vois])
			{
				file.push(vois);
				pere[vois]=r;
				ordre[vois]=ord;
				ord++;
				dejaVue[vois]=true;
				niveau[vois]=niveau[pere[vois]] + 1;

			}

		}

		file.pop();
		
		
		r=file.front();

		/*if( file.empty() && (snonvue=tousVue(n,dejaVue))!=-1 )
		{
			file.push(snonvue);
			dejaVue[snonvue]=true;
			r=snonvue;
			
			pere[snonvue]=-2;
			
			ordre[snonvue]=ord;
			ord++;
			
			niv=0;
			niveau[snonvue]=niv;
			niv++;
		}*/
	}
	
	
	int somNivLarg=0;
	//cout<<"som: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<i<<"|";
	}
	cout<<endl;
	
	//cout<<"niv: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<niveau[i]<<"|";
		somNivLarg+=niveau[i];
	}
	cout<<endl;
	cout<<"somNivLarg="<<somNivLarg;

	//cout<<"per: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<pere[i]<<"|";
	}
	cout<<endl;

	//cout<<"ord: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<ordre[i]<<"|";
	}
	cout<<endl;

}


void parcoursprofondeur(int n, vector <int> voisins [], int niveau[],int ordre[],int pere[])
{
	int r=0 ;	
	int y;
	int ord=0;
	
	int vois;
	bool dejaVue[n];
	int snonvue;
	stack <int> pile;
	pile.push(r);
	int niv=0;

	for(int i=0; i<n;i++)
	{
		dejaVue[i]=false;
	}
	dejaVue[0]=true;
	
	pere[0]=0;
	
	ordre[0]=0;
	ord++;
	
	niveau[0]=0;
	
	while (!pile.empty())
	{
		r=pile.top();
		if (voisins[r].size()==0)
		{
			pile.pop();
			niv--;
		}

		else
		{
			y=voisins[r].back();
			voisins[r].pop_back();
			
			if(!dejaVue[y])
			{
				pile.push(y);

				niv++;
				dejaVue[y]=true;
				pere[y]=r;
				ordre[y]=ord;
				ord++;
				niveau[y]=niv;
			}
		}
	}
	
	int somNivProf=0;

	//cout<<"som: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<i<<"|";
	}
	cout<<endl;
	
	//cout<<"niv: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<niveau[i]<<"|";
		somNivProf+=niveau[i];
	}
	cout<<endl;

	cout<<"somNivProf="<<somNivProf<<endl;

	cout<<endl;

	//cout<<"per: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<pere[i]<<"|";
	}
	cout<<endl;

	//cout<<"ord: ";
	for(int i=0;i<n;i++)
	{
		//cout<<setw(3)<<ordre[i]<<"|";
	}
	cout<<endl;

} 
