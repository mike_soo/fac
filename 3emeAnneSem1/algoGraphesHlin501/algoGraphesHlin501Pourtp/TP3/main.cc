#include "tp3.h"
using namespace std;

int main(int argc,char ** argv)
{
	srand(time(NULL));
	if(argc<=1)  //si on a pas de parametre seuil
	{
		int n;             //Le nombre de sommets.
		int m;             // Le nombre d'aretes.
		cout << "Entrer le nombre de sommets: ";
		cin >> n;
		cout << "Entrer le nombre d'aretes: ";

		cin >> m;
		if(m>(n*(n-1)/2))
		{
			m=(n*(n-1)/2);
		}

		
		vector<int> voisins[n];	// Les listes des voisins. 
		int pere[n];            // L'arbre en largeur.
		int ordre[n];           // L'ordre de parcours.
		int niveau[n];          // Le niveau du point.
		ledge listEl= creerlEdgeVide();
		
		for (int i=0;i<n;i++)
		{
			pere[i]=-1;
			ordre[i]=-1;
			niveau[i]=-1;
		}
		
		listEl=gGraphComplet(n, m , listEl ); //on genere le graphe complet a n sommets
		
		voisinstrandom(n, m,listEl, voisins);
		cout<<"parcourslargeur:"<<endl;
		parcourslargeur(n, voisins, niveau, ordre, pere);

		ecritureniveaux(n,niveau);

		cout<<endl;
		for (int i=0;i<n;i++)
		{
			pere[i]=-1;
			ordre[i]=-1;
			niveau[i]=-1;
		}
		
		cout<<"parcourProfondeur:"<<endl;
		parcoursprofondeur(n, voisins,niveau ,ordre ,pere);
		ecritureniveaux(n,niveau);
	}

	

	else //si l'utilisateur definie un seuil
	{
		int n;
		cout<<"rentrer n:"<<endl;
		cin>>n;
		int cpt=0;
		int puis10=1;
		int seuil=0;

		int pere[n];            // L'arbre en largeur.
		int ordre[n];           // L'ordre de parcours.
		int niveau[n];          // Le niveau du point.
		for (int i=0;i<n;i++)
		{
			pere[i]=-1;
			ordre[i]=-1;
			niveau[i]=-1;
		}
		while (argv[1][cpt]!='\0')
		{
			cpt++;
		}
		
		cpt--;
		for(cpt;cpt>=0;cpt--)
		{
			seuil+=((int)argv[1][cpt]-48)*puis10;
			puis10*=10;
		}	

		cout<<"seuil="<<seuil<<endl;
		coord point[n];
		pointrandom(n,point);

		vector<int> voisins[n];
		vector<int> arbreLargeur[n];
		grapheInfSeuil(n,point,voisins,seuil);

		AffichageGraphique1(n, point, voisins) ;

		for(int i=0;i<n;i++)
		{
			cout<<"s"<<i<<": ";
			for(int j=0;j<voisins[i].size();j++)
			{
				cout << voisins[i][j]<<" ";

			}
			cout<<endl;
		}
		parcourslargeurCreerArbre(n,voisins,arbreLargeur,niveau,ordre,pere);
		AffichageGraphique2(n,point,arbreLargeur);



	}
	return 0;
}
