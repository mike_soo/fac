#ifndef tp3_H
#define tp3_H
#include <math.h>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <stack>
#include <iomanip>      // std::setw
#include <queue>

using namespace std;
typedef struct coord
{
	int x; 
	int y;
} coord;
typedef struct edge
{
		int x;
		int y;
		int d;
		struct edge* suiv;
		struct edge* prec;
}edge;
typedef edge *ledge;


void ecritureniveaux(int n, int niveau[]);
ledge ajoutElem(int x, int y , ledge listEl);
ledge creerlEdgeVide();
ledge gGraphComplet(int n, int m , ledge listEl);
void voisinstrandom(int n, int m, ledge listEl, vector<int> voisins[]);
int tousVue(int n,bool dejaVue[]);
void parcourslargeur(int n, vector <int> voisins [], int niveau[],int ordre[],int pere[]);
void parcoursprofondeur(int n, vector <int> voisins [], int niveau[],int ordre[],int pere[]);
void pointrandom(int n,coord point[]);
void distancesEuc(int n, int m, coord point[],unsigned int** edge);

//les fonctions qui suivent sert a repondre à l'exercice 5:
void pointrandom(int n,coord point[]);
void grapheInfSeuil(int n, coord point[], vector<int> voisins[], int seuil);
void AffichageGraphique1(int n, coord point[],  vector <int> voisins[]); //permet d'afficher un graphe ou les arrêtes sont choisi par rapport a un seuil
void AffichageGraphique2(int n, coord point[],  vector <int> voisins[]); //permet d'afficher un arbre en largeur du graphe si dessus.

void parcourslargeurCreerArbre(int n, vector <int> voisins [],vector <int> arbreLargeur [], int niveau[],int ordre[],int pere[]);
#endif 