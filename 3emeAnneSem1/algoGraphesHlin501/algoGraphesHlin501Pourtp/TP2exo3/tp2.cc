
#include "tp2.h"



void pointrandom(int n,coord point[])
{
	for (int i=0;i<n;i++)
	{
		point[i].x=rand()%612;
		point[i].y=rand()%792;
	
	}
	/*
	for(int i=0;i<n;i++)
	{
		cout<<"x"<<i<<"="<<point[i].x<<" y"<<i<<"="<<point[i].y<<endl;
	}*/
	
}

void distancesEuc(int n, int m, coord point[],unsigned int** edge)
{	
	int io=0;
	for(int i=0;i<n;i++)
	{	for(int ii=i+1;ii<n;ii++)
		{
			edge[io][0]=i;
			edge[io][1]=ii;
			edge[io][2]=(point[edge[io][0]].x-point[edge[io][0]].y)-((point[edge[io][1]].x-point[edge[io][1]].y));
			io++;
		}
	}
	/*
	cout<<"avantsqrt = "<<pow((point[edge[0][0]].x-point[edge[0][1]].x),2)+pow((point[edge[0][0]].y-point[edge[0][1]].y),2)<<endl;
	cout<<"distance entre ("<<point[edge[0][0]].x<<","<<point[edge[0][0]].y<<") et (" <<point[edge[0][1]].x<<","<<point[edge[0][1]].y<<")";
	cout<<"="<<edge[0][2]<<endl; 
	*/

}

void distanceManh(int n, int m, coord point[],unsigned int** edge)
{
	int io=0;
	for(int i=0;i<n;i++)
	{	for(int ii=i+1;ii<n;ii++)
		{
			edge[io][0]=i;
			edge[io][1]=ii;
			//edge[io][2]=abs(pow(point[edge[io][0]].x-point[edge[io][1]].x,2)+pow((point[edge[io][0]].y-point[edge[io][1]].y),2));
			io++;
		}
	}
}

void afficheEdge(int m,unsigned int **edge,coord point[])
{
	for(int i=0;i<m;i++)
	{
		cout<<"p"<<edge[i][0]<<" ("<<point[edge[i][0]].x<<","<<point[edge[i][0]].y<<") ; p"<<edge[i][1]<<" (" <<point[edge[i][1]].x<<","<<point[edge[i][1]].y<<") d="<<edge[i][2]<<" "<<endl;
	}
	
}
void tri(int m,unsigned int **edge)
{	
	int i=0;
	int inter0;
	int inter1;
	int inter2;

	bool permu=false;
	do
	{	
		permu=false;
		if(i<m)
		{
			if(edge[i][2]>edge[i+1][2])
			{	
				permu=true;
				inter0 = edge[i][0];
				inter1 = edge[i][1];
				inter2 = edge[i][2];

				edge[i][0]=edge[i+1][0];
				edge[i][1]=edge[i+1][1];
				edge[i][2]=edge[i+1][2];

				edge[i+1][0]=inter0;
				edge[i+1][1]=inter1;
				edge[i+1][2]=inter2;
			}

		}
		i++;

		if(permu && i==(m-1))
		{
			i=0;
			m--;
		}
	}
	while(permu || i<m);

}

void composanteOpti (int n,int m,unsigned int **edge,int comp[],unsigned int ADM[][2])
{	
	vector<vector <int> > vect(n); //tableau qui contient pour chaque indice en vect[i], tout les sommets appartenant a la composante i
	int ii=0;
	
	//cout<<"vect:"<<endl;
	for(int i=0;i<n;i++)
	{
		vect[i]=vector<int> (1);
		vect[i][0]=i;
		//cout<<vect[i][0]<<endl;
	}
	//cout<<endl;
	


	for (int i=0;i<n;i++)
	{ comp[i]=i; }
	

	
	
	for (int i=0;i<m;i++)
	{	

		if (comp[edge[i][0]] != comp[edge[i][1]] )
		{ 	
			int compox = comp[edge[i][0]];
			int compoy = comp[edge[i][1]];
			

			ADM[ii][0]=edge[i][0];
			ADM[ii][1]=edge[i][1];
			ii++;
			if(vect[compoy].size()>vect[compox].size())
			{
				int inter=compox;
				compox=compoy;
				compoy=inter;
			}

			int tailleComp=vect[compoy].size();
		
			//cout<<"taille="<<tailleComp<<endl;
			//cout<<"i="<<i<<endl;
			
			for (int k=(tailleComp-1);k>=0;k--)
			{	
				//cout<<"k="<<k<<","<<endl;
				
				
				comp[vect[compoy][k]] = compox ;
				
				
				
				
				
				vect[compox].push_back(vect[compoy][k]);

				vect[compoy].pop_back();
				


				//------------------------------>trace algo
				/*cout<<"k="<<k<<endl;
				cout<<"comp"<<vect[compoy][k]<<"="<<compox<<endl;
				cout<<"comp[]= ";
	
				for (int i=0;i<n;i++) 
					cout << comp[i] << " ";

				cout<<endl;
				cout<<"vect"<<compox<<".push_back"<<vect[compoy][k]<<endl;;

				cout<<"vect= "<<endl;
			    for (int i=0;i<n;i++) 
					{	
						cout<<"comp"<<i<<" contient: ";
						for(unsigned int j=0; j<vect[i].size(); j++)
							cout << vect[i][j] << " ";
						cout<<endl;
					}
				*/
			}

		}
	}
	
	/*
	cout<<"comp[]= ";
	
	for (int i=0;i<n;i++) 
		cout << comp[i] << " ";

	cout<<endl;
	cout<<"vect= "<<endl;
    for (int i=0;i<n;i++) 
		{	
			cout<<"comp"<<i<<" contient: ";
			for(unsigned int j=0; j<vect[i].size(); j++)
				cout << vect[i][j] << " ";
			cout<<endl;
		}
	*/
	
	
	
}
void afficheABM(int m, int ADM[][2])
{	
	cout<<"ABM="<<endl;
	for(int i=0;i<m;i++)
	{
		cout<<ADM[i][0]<<" "<<ADM[i][1]<<endl;
	}
}

void fusion(unsigned int **edge,int deb1,int fin1,int fin2)
{
	edge3 *table1;
	int deb2=fin1+1;
	int compt1=deb1;
	int compt2=deb2;
	int i;

	table1=(edge3*)malloc((fin1-deb1+1)*sizeof(edge3));

	//on recopie les éléments du début du edge
	for(i=deb1;i<=fin1;i++)
	    {
	        table1[i-deb1].p1=edge[i][0];
	        table1[i-deb1].p2=edge[i][1];
	        table1[i-deb1].d=edge[i][2];
	    }
	                
	for(i=deb1;i<=fin2;i++)
	    {        
	    if (compt1==deb2) //c'est que tous les éléments du premier edge ont été utilisés
	        {
	       		 break; //tous les éléments ont donc été classés
	        }
	    else if (compt2==(fin2+1)) //c'est que tous les éléments du second edge ont été utilisés
	        {
	            edge[i][0]=table1[compt1-deb1].p1; //on ajoute les éléments restants du premier edge
	            edge[i][1]=table1[compt1-deb1].p2;
	            edge[i][2]=table1[compt1-deb1].d;

	       		compt1++;
	        }
	    else if (table1[compt1-deb1].d<edge[compt2][2])
	        {
	        
	        
	            edge[i][0]=table1[compt1-deb1].p1; 
	            edge[i][1]=table1[compt1-deb1].p2;
	            edge[i][2]=table1[compt1-deb1].d;
	        	compt1++;
	        }
	    else
	        {
	            edge[i][0]=edge[compt2][0]; //on ajoute un élément du second edge
	            edge[i][1]=edge[compt2][1];
	            edge[i][2]=edge[compt2][2];


	            compt2++;
	        }
	    }
	free(table1);
}
        

void tri_fusion_bis(unsigned **edge,int deb,int fin)
        {
        if (deb!=fin)
            {
	            int milieu=(fin+deb)/2;
	            tri_fusion_bis(edge,deb,milieu);
	            tri_fusion_bis(edge,milieu+1,fin);
	            fusion(edge,deb,milieu,fin);
            }
        }

void tri_fusion(unsigned int **edge,int longueur)
     {
     if (longueur>0)
            {
            	tri_fusion_bis(edge,0,longueur-1);
            }
     }


int main(){
  srand(time(NULL));
  int n;             //Le nombre de points.
  cout << "Entrer le nombre de points: ";
  cin >> n;
  cout<<"cin ="<<n<<endl;
  coord point[n];   // Les coordonnees des points dans le plan.


  int m=n*(n-1)/2;   // Le nombre de paires de points.

  unsigned int** edge=(unsigned int**)malloc(m*sizeof(unsigned int*));    // Les paires de points et le carre de leur longueur.
  
  for (int i=0;i<m;i++)
  {
  	edge[i]=(unsigned int*)malloc(3*sizeof(unsigned int));
  }
  
  unsigned int ADM[n-1][2]; //arbre Couvrant de Distance Minimum du graphe complet representé par edge  
  int comp[n];    // comp[i] est le numero de la composante contenant i.
  
 	/*pointrandom(n,point); //algo avec triBulles
	distancesEuc(n,m,point,edge);

	cout<<"algo avec tri Bulles Exec"<<endl;
	double t3=clock(); 	

    tri(m,edge);
    composanteOpti(n,m,edge,comp,ADM);


	double t4=clock();

	cout<<"temps d'execution avec TBulles="<<(float)(t4-t3)<<endl;*/

	pointrandom(n,point);
	distancesEuc(n,m,point,edge);
	
	cout<<"algo avec tri Fusion Exec"<<endl;
	double t3=clock(); 	

	tri_fusion(edge,m);
	composanteOpti(n,m,edge,comp,ADM);


	double t4=clock();

	cout<<"temps d'execution avec TFusion="<<(float)(t4-t3)<<endl;


  /*	// decomenter pour afficher edge avant et apres le tri
  cout<<"Edge"<<endl;
  afficheEdge(m,edge,point);
  tri_fusion(edge,m);
  cout<<"EdgeTriée:"<<endl;
  afficheEdge(m,edge,point);
  composanteOpti(n,m,edge,comp,ADM);
  */
  
  

  
  
  AffichageGraphique(n,point,ADM);
  
  return 0;
}
