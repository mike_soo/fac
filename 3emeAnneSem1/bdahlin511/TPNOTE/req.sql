--PREMIERE ETAQUE: GESTION DE VALEURS

-->1
/*
DROP TABLE AB_NB;
CREATE TABLE AB_NB(
	NUMERO NUMERIC (6,0) PRIMARY KEY,
	NB NUMERIC(3,0),
	CONSTRAINT FK_NUMERO FOREIGN KEY(NUMERO) REFERENCES ABONNE(NUM_AB));

CREATE OR REPLACE FUNCTION assigneNombreEmprunt (PNUM_AB IN INTEGER)
		RETURN INTEGER IS  rnombreEmprunt INTEGER;
		BEGIN
			rnombreEmprunt:=3;
			RETURN rnombreEmprunt;
		END;
		/

		--curseur implicite

DECLARE
	TARIFTOTAL FLOAT;
	BEGIN
		SELECT SUM(tarif) into TARIFTOTAL
		FROM abonne;

	dbms_output.put_line('Gain'||tariftotal);
end;
select object_name from user_procedures;
/

CREATE OR REPLACE FUNCTION assigneNombreEmprunt
		(PNUM_AB IN ABONNE.NUM_AB%TYPE)
		RETURN NUMBER IS  rnombreEmprunt NUMBER(3,0);

		DECLARE
			nombreEmprunt IS
				SELECT COUNT(*)
				FROM EMPRUNT
				WHERE EMPRUNT.NUM_AB=PNUM_AB
				GROUP BY EMPRUNT.NUM_AB;
		BEGIN

			RETURN nombreEmprunt;
		END;/



create or replace
	procedure inserer(num_ab in NUMERIC, nb_emp in NUMERIC)
		is
		cursor c1 is
			select *
			from ab_nb
			where numero = num_ab;
		 abo c1%ROWTYPE;
		begin
			open c1;
				FETCH c1 INTO abo;
				if c1%NOTFOUND THEN
					INSERT INTO ab_nb VALUES(num_ab,nb_emp);
				ELSE
					UPDATE ab_nb set nb = nb_emp WHERE 	numero = num_ab;
				END IF;
			CLOSE c1;
		end;
		/


*/
CREATE OR REPLACE PROCEDURE passigneNombreEmprunt (PNUM_AB IN ABONNE.NUM_AB%TYPE)
		IS
			CURSOR nombreEmprunt IS
				SELECT COUNT(*) as n
				FROM EMPRUNT
				WHERE EMPRUNT.NUM_AB=PNUM_AB
				GROUP BY EMPRUNT.NUM_AB;
         nbEmpFetch nombreEmprunt%ROWTYPE;

     CURSOR existenum_ab IS
        SELECT NUM_AB
        FROM ABONNE;
        testExiste existenum_ab%ROWTYPE;
    exceptionExnum_ab EXCEPTION ;
		BEGIN
		  OPEN existenum_ab;
      FETCH existenum_ab INTO testExiste;
      IF existenum_ab%NOTFOUND  THEN RAISE exceptionExnum_ab;

      END IF;

      OPEN nombreEmprunt;
      fetch nombreEmprunt INTO nbEmpFetch;
      inserer(PNUM_AB,nbEmpFetch.n);
      CLOSE nombreEmprunt;

      EXCEPTION
        WHEN exceptionExnum_ab
          THEN DBMS_OUTPUT.PUT_LINE('Erreur num_ab passé en param non existant!');

		END;
		/


show errors;

--TEST DU FONCTIONNEMENT DES PROCEDURE RECAMENT CRÉÉ
SELECT NUM_AB,COUNT(*) as nbemprunt
				FROM EMPRUNT
				GROUP BY EMPRUNT.NUM_AB;

BEGIN
	passigneNombreEmprunt(911023);
END;
/
SELECT *
  FROM AB_NB;