#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>    /* For O_RDWR */
#include <sys/stat.h>

void tailleParallele(int out[2],char * nomrep)
{
	int in[2];
	pipe(in);

	close(out[0]);
	
	
	struct stat etat;
	lstat(nomrep,&etat);
	if(!S_ISDIR(etat.st_mode))
	{
		//long int temp=(long int)etat.size;
		write(out[1],&etat.st_size,sizeof(off_t));
		printf("%s size %li\n",nomrep,etat.st_size );
	}

	else
	{
		long int tailleTotal=0;
		long int taille;
		DIR *d =opendir(nomrep);
		dirent *de=NULL;
		char dsuiv[1024];
		memset(dsuiv,'\0',sizeof(dsuiv));
		while(de=readdir(d))
		{
			if(!(0==strcmp(".",de->d_name)) &&  !(0==strcmp("..",de->d_name)))
			{
				strcpy(dsuiv,nomrep);
				strcat(dsuiv,"/");
				strcat(dsuiv,de->d_name);
				if(0==fork())
				{
					tailleParallele(in,dsuiv);
					return;
				}


			}
			else
			{
				lstat(de->d_name,&etat);
				write(out[1],&etat.st_size,sizeof(off_t));
			}


		}
		//je suis le père
		close(in[1]);
		while(read(in[0],&taille,sizeof(off_t)))
		{

			tailleTotal+=taille;
			printf("une taille=%li\n",taille);
		}
		write(out[1],&tailleTotal,sizeof(off_t));
		close(out[1]);
		close(in[0]);
	}
}

int main(int argc,char **argv)
{
	if (argc!=2)
	{
		printf("rentrer parametre");
		return -1;
	}
	else
	{
		int in[2];
		if(0>pipe(in))
		{
			printf("erreur de creatin de tube\n");
			exit(-1);
		}

		struct stat etat;
		if(0>lstat(argv[1],&etat))
		{
			printf("erreur d'analyse avec lstat\n");
			exit(-1);
		}	

		if(!S_ISDIR(etat.st_mode))
		{
			printf("taille sing= %li\n",etat.st_size);
		}
		else
		{
			long int taille,tailleTotal;
			tailleTotal=0;
			DIR* d=NULL;
			dirent* de=NULL;
			if(!(d=opendir(argv[1])))
			{
				printf("erreur ouveture de dossier\n");
			}
			char dsuiv[1024];
			memset(dsuiv,'\0',sizeof(dsuiv));

			while(de=readdir(d))
			{
				if(!(0==strcmp(".",de->d_name)) &&  !(0==strcmp("..",de->d_name)))
				{
					strcpy(dsuiv,argv[1]);
					strcat(dsuiv,"/");
					strcat(dsuiv,de->d_name);

					if(0==fork())
					{
						tailleParallele(in,dsuiv);
						return 1;
					}
				}


			}
			close(in[1]);
			while(read(in[0],&taille,sizeof(off_t)))
			{
				tailleTotal+=taille;
				printf("une taille=%li\n",taille);
			}
			close(in[0]);

			printf("taille total %li\n",tailleTotal);
		}

	}
}