#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>    /* For O_RDWR */
#include <sys/stat.h>

FILE* f;

void gst(int sig)
{
	printf("fermeture fichier\n");
	fclose(f);
	exit(1);
}

int main()
{
	f=fopen("grandText.txt","w");

	struct sigaction comp;
	comp.sa_handler=gst;
	sigaction(SIGALRM,&comp,NULL);
	alarm(20);
	while(true)
	{
		fprintf(f,"a");
	}
}