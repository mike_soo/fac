#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main (int argc,char **argv)
{	if(argc<3)
		return -1;
	
	int fileSrc=open(argv[1],O_RDONLY);
	int nbrlu;
	
	
	int fileCp=open(argv[2],O_CREAT | O_WRONLY | O_TRUNC,0640);
	
	if(fileSrc<0 )
	{
		write(2,"erreurSRC",9);
		return -1;
	}


	if(fileCp<0 )
	{
		write(2,"erreurCP",8);
		return -1;
	}

	char buffer[1024];
	
	while(0<(nbrlu=read(fileSrc,buffer,1024)))
	{		
		if(nbrlu != write(fileCp,buffer,nbrlu))
		{	
			write(2,"erreur d'ecriture disque probablement plein",47);
			return 5;
		}
		
		
	}

	write(2,"\n",1);
	close(fileSrc);
	close(fileCp);

	return 0;
}