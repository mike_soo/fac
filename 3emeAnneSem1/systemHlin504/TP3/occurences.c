#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc,char **argv)
{	
	char c;
	int fichier;
	int *occu=(int*)malloc(256*sizeof(int)); 
	char *buffer=(char*)malloc(1024*sizeof(char)); 
	int bitBufLu; //nombre de bit lu du fichier copier ver le buffer
	if (argc<2)
	{
		write(1,"erreur",6);
		return -1;
	}
	for(int i=0;i<256;i++)
	{
		occu[i]=0;
	}
	int ic;
	

	fichier = open(argv[1],O_RDONLY);
	while (0<(bitBufLu=read(fichier,buffer,1024)))
	{
		for (int i=0;i<bitBufLu;i++)
		{	
			c=buffer[i];
			ic=c;
			occu[ic]++;
		}
	}
	close(fichier);
	
	char *ch_nbre=(char*) malloc(4);
	int nb_octet; 

	for (int i=0;i<256;i++)
	{
		if(occu[i]!=0)
		{	nb_octet = sprintf(ch_nbre,"%d",occu[i]);
			write(1,&i,1);
			write(1,":",1);
			write(1,ch_nbre,nb_octet);
			write(1," ",1);
		}
	}
	write(1,"\n",1);

	return 0;
}