#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{	int file;
	if(argv[1]!=NULL)
		file=open(argv[1],O_RDONLY);

	char present[256];
	char c;
	int cptr=0;
	if(file==-1)
	{	
		printf("je me suicide, fichier mal ouvert");
		return -1;
	}

	for(int i=0;i<256;i++)
	{
		present[i]=0;
	}	

	while(0<read(file,&c,1))
	{
		if(present[c]==0)
		{
			cptr++;
			present[c]=1;
		}
			
	}

	close(file);
	char *ch_nbre=(char*) malloc(4);
	int nbOctets = sprintf(ch_nbre,"%d",cptr);


	write (1,ch_nbre,nbOctets);
	write (1," caracteres differentes:",24);
	
	for(int i=0;i<256;i++)
	{
		if(present[i]==1)
		{	char ic=i;
			
			write(1,&ic,1);
			
			write(1,", ",2);
		}
	
	}
	write(1,"\n",1);

	return 0;
}