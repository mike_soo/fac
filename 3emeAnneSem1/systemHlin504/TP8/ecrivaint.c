#include <unistd.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>   

int main(int argc,char ** argv )
{
	if(argc!=2)
	{
		fprintf(stderr,"ecrivain besoin de nom du tube");	
	}
	
	if((mkfifo(argv[1],S_IWUSR | S_IRUSR ) == -1 ) && (errno!=EEXIST))
	{
		fprintf(stderr, "pb creation tube %s",argv[1] ); 

		return 2;
	}

	else  
	{
		chmod(argv[1],0600);
		printf("tube %s crée (ou existant)\n",argv[1]);
	}
	int tubeE = open(argv[1],O_WRONLY);
	
	if(tubeE==-1)
	{
		
		fprintf(stderr,"imp ouvrir tube %s\n",argv[1]);
		return 2; 
	}
	
	char lu[1025];

	while( fgets(lu,1025,stdin)!= NULL   &&   strcmp(lu,"STOP")!=0  )
	{
		printf("C'est le moment d'ecrire'!\n" );
		write (tubeE,lu,strlen(lu));
	}

	close(tubeE);
	return 0; 


	
}
