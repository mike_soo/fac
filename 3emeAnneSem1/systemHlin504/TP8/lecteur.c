#include <unistd.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>   

int main(int argc,char ** argv )
{
	if(argc!=2)
	{
	
		fprintf(stderr,"lecteur besoin de nom du tube");
	}
	if((mkfifo(argv[1],S_IRUSR | S_IWUSR) == -1 ) && (errno!=EEXIST))
	{
		fprintf(stderr, "pb creation tube %s",argv[1] ); 

		return 2;
	}

	else  
	{
		chmod(argv[1],0600);
		printf("tube %s crée (ou existant)\n",argv[1]);
	}
	int tubeL = open(argv[1],O_RDONLY);
	
	if(tubeL==-1)
	{
		fprintf(stderr,"imp ouvrir tube %s",argv[1]);
		return 2; 
	}

	printf("C'est le moment de lire!\n");

	char c;
	while(read(tubeL,&c,1)==1)
	{
		write(1,&c,1);
	}
	close(tubeL);
	//unlink(argv[1]);
	return 0;

}
