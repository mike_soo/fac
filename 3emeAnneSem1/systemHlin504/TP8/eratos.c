#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

void crible(int in[2])
{
    pid_t pid;
    int out[2],intlue,numPremier;
    close(in[1]);
    pipe(out);
    if(read(in[0],&intlue,sizeof(int)))
    {

        numPremier=intlue;
        //printf("mon num premier %i pid=%i",numPremier,getpid());
        if(0==(pid=fork()))
        {
            crible(out);
            return;
        }


        else //procesus pere et fils du procesus main pour tout premier fork du main
        {

            close(out[0]); //le pere ne vas pas lire dans le tube out auquel lui meme il ecrit
            
            while(read(in[0],&intlue,sizeof(int)))
            {
                //printf("entier lue=%i par moi pid=%i\n",intlue,getpid());
            
                if(intlue%numPremier !=0)
                {
                    //printf("mon num premier %i pid=%i et je laisse passer intlue=%i\n",numPremier,getpid(),intlue);
                    write(out[1],&intlue,sizeof(int));
                }
            }
            printf("numPremierFinal%i\n",numPremier);
            close(in[0]);
            close(out[1]);

        }
    }
}

int main(int argc,char **argv)
{
    pid_t pid;
    int max=atoi(argv[1]);
    int out[2];
    pipe(out);

    if(0==(pid=fork()))
    {
        //printf("coucou\n");

        crible(out);

    }
    else
    {
        close(out[0]); //ferme lecture pour le pere car il est ecrivain
        for(int i=2;i<=max;i++)
        {
            if(i%2!=0)
            {
                //printf("i=%i normalement ecrit\n",i);
                write(out[1],&i,sizeof(int));
            }

        }
        close(out[1]);
    }
    return 0;
}