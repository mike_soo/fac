#include <unistd.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>


int main (int argc,char ** argv)
{  
	struct stat info;
	
	if(argc!=2)
	{
		printf("erreur fichier en parametre requis\n");
		return -1;
	}

	lstat(argv[1],&info);

	switch(info.st_mode & S_IFMT)
	{
		case S_IFLNK:
			printf("le fichier est un lien symbolique\n");
			break;
		case S_IFREG:
			printf("le fichier est regulier\n");
			printf("IFRE: %o \n",S_IFREG);
			break;

		case S_IFDIR:
			printf("le fichier est un repertoir \n");
			break;
	}


	printf("droit du fichier\n");
	printf("%s ",argv[1]);
	switch(info.st_mode & S_IFMT)
	{
		case S_IFLNK:
			printf("l");
			break;
		
		case S_IFREG:
			printf("-");
			
			break;

		case S_IFDIR:
			printf("d");
			break;
	}

	(info.st_mode & S_IRUSR) ? printf("r") : printf("-");
    (info.st_mode & S_IWUSR) ? printf("w") : printf("-");    
    (info.st_mode & S_IXUSR) ? printf("x") : printf("-");
    (info.st_mode & S_IRGRP) ? printf("r") : printf("-");
    (info.st_mode & S_IWGRP) ? printf("w") : printf("-");
    (info.st_mode & S_IXGRP) ? printf("x") : printf("-");
    (info.st_mode & S_IROTH) ? printf("r") : printf("-");
    (info.st_mode & S_IWOTH) ? printf("w") : printf("-");
    (info.st_mode & S_IXOTH) ? printf("x") : printf("-");
	// inclue ause droitichier
    printf("\n");
	
	

}