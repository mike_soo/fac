#include <unistd.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

void afficheDroitFich(char *argv)
{
	struct stat info;
	
	

	lstat(argv,&info);

		
	
	switch(info.st_mode & S_IFMT)
	{
		case S_IFLNK:
			printf("l");
			break;
		
		case S_IFREG:
			printf("-");
			
			break;

		case S_IFDIR:
			printf("d");
			break;
	}

	(info.st_mode & S_IRUSR) ? printf("r") : printf("-");
    (info.st_mode & S_IWUSR) ? printf("w") : printf("-");    
    (info.st_mode & S_IXUSR) ? printf("x") : printf("-");
    (info.st_mode & S_IRGRP) ? printf("r") : printf("-");
    (info.st_mode & S_IWGRP) ? printf("w") : printf("-");
    (info.st_mode & S_IXGRP) ? printf("x") : printf("-");
    (info.st_mode & S_IROTH) ? printf("r") : printf("-");
    (info.st_mode & S_IWOTH) ? printf("w") : printf("-");
    (info.st_mode & S_IXOTH) ? printf("x") : printf("-");
	// inclue ause droitichier
	printf(" %-20s ",argv);
    printf("\n");
	
}

int main(int argc, char** argv)
{

	DIR *d;
	if(argc!=2)
	{
		printf("erreur fichier en parametre requis\n");
		return -1;
	}
	struct dirent *de;
	d=opendir (argv[1]);
	while((de=readdir(d))!=NULL)
	{
		printf("%-10li ",de-> d_ino );
		afficheDroitFich(de->d_name);
		
	}
	
	
	int c=closedir(d);
}