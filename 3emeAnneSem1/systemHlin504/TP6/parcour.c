#include <unistd.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

char afficheDroitFich(char *argv)
{
	struct stat info;
	
	

	lstat(argv,&info);

		
	
	switch(info.st_mode & S_IFMT)
	{
		case S_IFLNK:
			return 'l';
			
		
		case S_IFREG:
			return '-';
			
			

		case S_IFDIR:
			return 'd';
			
	}

	return '0';
	
}

int getTailleChar(char *tabc)
{
	int i=0;
	
	while (tabc[i]!='\0')
	{
		i++;
	}

	return i+1; //on return la taille sans d'un char* en comptant \0

}

void parcours(char *rep,char* dec,int nDec)
{
	char type=afficheDroitFich(rep);
	printf ("%s %c %s\n",dec,type,rep);

		struct stat etat;
		lstat(rep,&etat);
		S_ISDIR(etat.st_mode);

		DIR *d;
		struct dirent *de;
		if(!S_ISDIR(etat.st_mode))
		{

			return;
		}
		
		if(0>(d=opendir(rep)))
		{
			printf ("erreur avec fichier %s :",rep);
			return ;
		}

		
		char decplusun[nDec+5];
		
		strcpy(decplusun,dec);
		strcat(decplusun,"     ");
		
		
		


	while ((de=readdir(d))!=NULL)
	{	
		
		int nTaille=strlen(rep)+strlen(de->d_name)+2;
		char nouvRep[nTaille];

		strcpy(nouvRep,rep);
		strcat(nouvRep,"/");
		strcat(nouvRep,de->d_name);
		
		if( !((0==strcmp(de->d_name,".")) || (0==strcmp(de->d_name,".."))))				
			parcours(nouvRep,decplusun,nDec+1);
	}
	closedir(d);
	
		
	
}



int main(int argc,char **argv)
{
	if(argc!=2)
	{
		printf("rentrez 1 param\n" );
		return -1;
		
	}
	char esp[1];
	esp[0]=' ';
	parcours(argv[1],esp,0);
  return 1;
}
