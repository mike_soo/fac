#include "mm.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>    // time()

mm mm_creer()
{
	srand(time(NULL));
	mm jeu= (mm)malloc (sizeof(mm));
	for(int i=0;i<TAILLE;i++)
	{
		jeu->secret[i]=(rand()%9)+48;
	}
	return jeu;
}

void mm_detruire(mm jeu)
{
	free(jeu);
}

int mm_test(mm jeu,char* essai)
{	
	int nLTBP=0; //nombre de lettres trouvée bien placées
	int nLTMP=0; //nombre de lettres trouvée mal placées
	int tabJeu[TAILLE];

	for(int i=0;i<TAILLE;i++)
	{
		tabJeu[i]=-1;
	}

	for(int i=0;i<TAILLE;i++)
	{	printf("%c -- %c \n",essai[i],jeu->secret[i]);
		if (essai[i]==jeu->secret[i])
		{	
			printf("je match \n");
			tabJeu[i]=i;
			nLTBP++;
		}

	}
	for(int i=0;i<TAILLE;i++)
	{	
		for(int j=0;j<TAILLE;j++)
		{
			if((essai[i] == jeu->secret[j] ) && (tabJeu[j]==-1))
			{
				nLTMP++;
				tabJeu[j]=i;
				break;
			}
		}
	}
	
	
	return ((TAILLE + 1)*nLTBP) + nLTMP;
}

int getnbessais()
{
	return jeu->nbessais;
}