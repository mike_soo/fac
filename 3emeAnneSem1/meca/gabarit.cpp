
#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846


using namespace std;
int cptP=0;
Tabpoint gab;

/*axe de rotation*/

float **Ru;
float theta=M_PI/128;
bool mousegAc=false;
float radius = 0.5;

Point mouseRecup; //stocke les coordonnées de la souris récupérées en coordonnées fenêtre;
Point p1;
Point p2;
Point p1p2;
bool mousemAc=false; //vérificateur du bouton du milieu activé
bool deuxiemeCli=false;
Point vUx;
int cptPointSais=0; //compteur de points saisis par la souris
Point vsx(1,0,0);
Point vsy(0,1,0);
Point vsz(0,0,1);
Point Psaisie; //point récupérant les point de saisie en cordonnées espace;
int isaiTabP=0;
bool preDraw=true; // booléen à true indiquant le premier dessin
/*axe de rotation*/
float zoom=0.70;

bool vNormalVis=false;

bool lignesVis=true;
bool polyGVis=false;
bool pointVis=false;
bool vNsurface=false;
bool flatShading=false;
bool hautverlebas=false;
bool lum=false;


void keyboard(unsigned char key, int x, int y);  //Sert à identifier les multiples interventions de l'utilisateur avec le clavier.

void mouseB(int button, int state, int x, int y);   //Sert à identifier les multiples interventions de l'utilisateur avec la souris.

void mouseM(int x, int y); //fonction précisant les tâches à réaliser lors de l'appui continu du bouton milieu de la souris.
void render(void); //fonction désignant tous les dessins 3D à faire lors d'un appel à glutPostRedisplay().
void Rof (float Ux,float Uy,float Uz); //fonction appliquant les calcul de la matrice de rotation 3D appliquée à la base R0 par rapport au vecteur U.
void GetOGLPosRot(int x, int y); //fonction désignant tout les algorithmes à appliquées lors d'une rotation.
void GetOGLPosSai(int x, int y);//fonction désignant tout les algorithmes à appliquer lors de la saisie en 2D.
float normeV(Point v); //renvoi la norme d'un vecteur.
Point normaliseV(Point v); //renvoi le vecteur normalisé du vecteur v.

/*opérateur pour les vecteurs*/

bool operator==(Point p1,Point p2); 
bool operator!=(Point p1,Point p2);
Point operator+(Point p1,Point p2);
Point operator-(Point p1,Point p2);
Point operator*(float scal,Point p1); //produit par un scalaire
Point operator * (Point p1,Point p2); //produit vectoriel

int mod(int m, int n); //fonction remplaçant l’opérateur %, spécialement quand on traite des valeurs négatives.

/*=====================================================================================
  
       
        DEBUT MAIN


=====================================================================================*/


int main(int argc, char** argv,char *envp[])
{
  Ru=new float *[3];
 
  for(int i=0;i<3;i++)
  {
    Ru[i]=new float [3];
  } 

 glutInit(&argc, argv);

 glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
 
 glutInitWindowPosition(200/*+(640*2)*/,100);

 glutInitWindowSize(640,610); 	//Optionnel
 glutCreateWindow("Gabarit 2016");
 glClearColor(0, 0, 0, 0);
 


    
 glDepthMask(GL_TRUE);
 glEnable(GL_DEPTH_TEST);
 glDepthRange(1.0f,0.0f);
 glDepthFunc(GL_LEQUAL); 
 
 glEnable(GL_COLOR_MATERIAL);
 /*
 float mcolor[] = { 0.2f, 0.0f, 1.0f, 1.0f };
 float specReflection[] = { 0.1f, 0.1f, 0.8f, 1.0f };
 
 glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mcolor);
 glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specReflection);
 
 glMateriali(GL_FRONT, GL_SHININESS, 96);*/
 glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
 glShadeModel(GL_SMOOTH); //GL_FLAT GL_SMOOTH
 
 glEnable(GL_MULTISAMPLE);


 
 
 
 
 glutDisplayFunc(render);
 glutMouseFunc(mouseB);
 glutMotionFunc(mouseM);
 glutKeyboardFunc(keyboard) ;
 
 

 glutMainLoop();
 

	return 0;
}

int mod (int m, int n) 
{ return m >= 0 ? m % n : ( n - abs ( m%n ) ) % n; }

bool operator==(Point p1,Point p2)
{
  
  return (p1.getX()==p2.getX()) && (p1.getY()==p2.getY()) &&(p1.getZ()==p2.getZ());

  
}


bool operator!=(Point p1,Point p2)
{

 return !(p1==p2);
}


Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator-(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()-p2.getX());
  presul.setY(p1.getY()-p2.getY());
  presul.setZ(p1.getZ()-p2.getZ());

  return presul;
}


Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}


Point operator * (Point p1,Point p2) //produit vectorielle
{
  Point presul;
  presul.setX( (p1.getY()*p2.getZ())  -  (p1.getZ()* p2.getY()) );

  presul.setY( (p1.getZ()*p2.getX())  -  (p1.getX()* p2.getZ())  );

  presul.setZ( (p1.getX()*p2.getY())  -  (p1.getY()* p2.getX())  );
  return presul;

}

float normeV(Point v)
{
  return (float)sqrt(((float)v.getX()*(float)v.getX())+((float)v.getY()*(float)v.getY())+((float)v.getZ()*(float)v.getZ())); //norme du vecteur normal


}

Point normaliseV(Point v)
{
  Point vNormalise;
  float nV=normeV(v);
  if(nV!=0)
  {
    vNormalise.setX((1.0f/nV)*v.getX());
    vNormalise.setY((1.0f/nV)*v.getY());
    vNormalise.setZ((1.0f/nV)*v.getZ());
    return vNormalise;
  }

  else 
    return v;
  
  
}

void Rof (float Ux,float Uy,float Uz)
{
 Ru[0][0]=(float)cos(theta)+(Ux*Ux*(1-cos(theta)));
 Ru[1][0]=(float)Uy*Ux*(1-cos(theta))+(Uz*sin(theta));
 Ru[2][0]=(float)Uz*Ux*(1-cos(theta))-(Uy*sin(theta));

 Ru[0][1]=(float)Uy*Ux*(1-cos(theta))-(Uz*sin(theta));
 Ru[1][1]=(float)cos(theta)+Uy*Uy*(1-cos(theta));
 Ru[2][1]=(float)Uz*Uy*(1-cos(theta))+Ux*sin(theta);

 Ru[0][2]=(float)Ux*Uz*(1-cos(theta))+Uy*sin(theta);
 Ru[1][2]=(float)Uy*Uz*(1-cos(theta))-Ux*sin(theta);
 Ru[2][2]=(float)cos(theta)+Uz*Uz*(1-cos(theta)); 

 Point tempvsx((float)vsx.getX()*Ru[0][0]+vsx.getY()*Ru[0][1]+vsx.getZ()*Ru[0][2]  , (float)vsx.getX()*Ru[1][0]+vsx.getY()*Ru[1][1]+vsx.getZ()*Ru[1][2]  ,  (float)vsx.getX()*Ru[2][0]+vsx.getY()*Ru[2][1]+vsx.getZ()*Ru[2][2]);
 Point tempvsy((float)vsy.getX()*Ru[0][0]+vsy.getY()*Ru[0][1]+vsy.getZ()*Ru[0][2]  , (float)vsy.getX()*Ru[1][0]+vsy.getY()*Ru[1][1]+vsy.getZ()*Ru[1][2]  ,  (float)vsy.getX()*Ru[2][0]+vsy.getY()*Ru[2][1]+vsy.getZ()*Ru[2][2]);
 Point tempvsz((float)vsz.getX()*Ru[0][0]+vsz.getY()*Ru[0][1]+vsz.getZ()*Ru[0][2]  , (float)vsz.getX()*Ru[1][0]+vsz.getY()*Ru[1][1]+vsz.getZ()*Ru[1][2]  ,  (float)vsz.getX()*Ru[2][0]+vsz.getY()*Ru[2][1]+vsz.getZ()*Ru[2][2]);

 vsx=tempvsx;
 vsy=tempvsy;
 vsz=tempvsz;
}


void render(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

 

 
 if(preDraw)
  {
    glDrawBuffer(GL_BACK);
    preDraw=false;
  }
 
 else 
  glDrawBuffer(GL_BACK);
 
  
 
 

  
  glLoadIdentity();



 

 if(lum)      //Lumières.
 {
   glEnable(GL_LIGHTING);   

     GLfloat globalAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
     GLfloat diffuseLight[] = {1.0, 1.0, 1.0, 1.0};
     GLfloat lightPosition[] = {10.0f, 0.0f, 5.0f, 1.f};
     GLfloat specular[] = {1.0f, 1.0f, 1.0f , 1.0f};

     glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
     glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
     glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
     glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbient);
        
   glEnable(GL_LIGHT0);
  }
 
  
 
 glScalef(zoom, zoom, zoom);
/*  
  cout<<"  zoom:"<<zoom<<endl;

  cout<<"vsx=("<<vsx.getX()<<","<<vsx.getY()<<","<<vsx.getZ()<<")"<<endl;

  cout<<"vsy=("<<vsy.getX()<<","<<vsy.getY()<<","<<vsy.getZ()<<")"<<endl;

  cout<<"vsz=("<<vsz.getX()<<","<<vsz.getY()<<","<<vsz.getZ()<<")"<<endl;
*/
  

glBegin(GL_LINES);
 
 /*base R0 orthonormé fixe*/

  glColor3f(1,1,1);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);

 
glEnd();
 
 
//Draw Circle
    glBegin(GL_POLYGON);
      //Change the 6 to 12 to increase the steps (number of drawn points) for a smoother circle
      //Note that anything above 24 will have little affect on the circles appearance
      //Play with the numbers till you find the result you are looking for
      //Value 1.5 - Draws Triangle
      //Value 2 - Draws Square
      //Value 3 - Draws Hexagon
      //Value 4 - Draws Octagon
      //Value 5 - Draws Decagon
      //Notice the correlation between the value and the number of sides
      //The number of sides is always twice the value given this range
      for(double i = 0; i < 2 * M_PI; i += M_PI / 256) //<-- Change this Value
        glVertex3f(cos(i) * radius, sin(i) * radius, 0.0);
    glEnd();
    //Draw Circle
 



    GetOGLPosRot(mouseRecup.getX(),mouseRecup.getY());
 /*rotation avec mouse */
 Point temp((p1p2.getX()*cos(M_PI/2))-(p1p2.getY()*sin(M_PI/2)),(p1p2.getX()*sin(M_PI/2))+(p1p2.getY()*cos(M_PI/2)),0);
 
 /*glBegin(GL_LINES);   
    glColor3f(0.5,0.2,1);//affichage du vecteur p1p2 de norme 1
        glVertex3f(0,0,0);
        glVertex3f((float)p1p2.getX(),(float)p1p2.getY(),(float)p1p2.getZ());
     //glVertex3f(0,0,0);
        //glVertex3f(p1p2.getX()/100,p1p2.getY()/100,p1p2.getZ()); //affichage correct de verif

   glColor3f(0.45,0.3,0);//affichage de la perpendiculaire du vecteur p1p2 de norme 1
    glVertex3f(0,0,0);
    glVertex3f(temp.getX(),temp.getY(),temp.getZ());
 glEnd();*/
  
  if(mousemAc)
  {
    vUx=temp;
   Rof(vUx.getX(),vUx.getY(),vUx.getZ());
  }

  glBegin(GL_LINES); //base  Rs
    glColor3f(0,0,1);

        glVertex3f(0,0,0);
        glVertex3f(vsx.getX(),vsx.getY(),vsx.getZ());

    glColor3f(0,1,0);

        glVertex3f(0,0,0);
        glVertex3f(vsy.getX(),vsy.getY(),vsy.getZ());

    glColor3f(1,0,0);
        glVertex3f(0,0,0);
        glVertex3f(vsz.getX(),vsz.getY(),vsz.getZ());
   
   glEnd();


 glutSwapBuffers();


 
}



void GetOGLPosSai(int x, int y)
{
  GLint viewport[4];  //Les valeurs de la fenêtre affichées seront stockées ici.
  GLdouble modelview[16];  //Les 16 doubles correspondants à la matrice de vue du modèle seront stockés ici.
  GLdouble projection[16];   //Les 16 doubles correspondants à la matrice de projection seront stockés ici.
  GLfloat winX, winY, winZ;
  GLdouble posX, posY, posZ;
  glGetDoublev( GL_MODELVIEW_MATRIX, modelview ); //Obtient et stocke les valeurs des 16 doubles dans modelview[].
  
  glGetDoublev( GL_PROJECTION_MATRIX, projection ); //Obtient et stocke les valeurs des 16 doubles dans projection[].
  
  glGetIntegerv( GL_VIEWPORT, viewport ); //Obtient les valeurs X, Y, longueur et hauteur de l'écran  puis on les stocke
                                          //dans viewport[] où X et Y correspondent aux coordonnées en haut à gauche de la fenêtre.
  
  winX = (float)x;
  winY = (float)viewport[3] - (float)y; //on inverse l'axe des y car de base, le centre de l'axe des x et y 
                                        //se trouve en haut a gauche, avec un axe y qui est dirigé vers le bas. Or 
                                        //notre axe y en cmv se dirige vers le haut.
  //glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
  gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ); 
               //stocke dans posX et dans posY les coordonnées du monde virtuel des
               //coordonnées fenêtres x et y passées en paramètres de la fonction. 
 
 
 Psaisie.setX(posX); //On stocke posX puis posY dans Psaisie, une variable globale de type Point qu'on pourra utiliser plus tard.
 Psaisie.setY(posY);
 Psaisie.setZ(0);

}




void GetOGLPosRot(int x, int y)
{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;
 
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );
 
    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
 
    
    //indicatif
    cout<<" posX="<<posX<<" posY="<<posY<<" posZ="<<posZ;

    Point verif;
    float thetaPP;
   
    if(deuxiemeCli)
    { 
      p2.setX((float)posX*10000);
      p2.setY((float)posY*10000);
      p2.setZ(0);
      
      
       
      verif=(p2-p1); //vérification de la norme du nouveau vecteur p2-p1;
      float norme=(float)sqrt((verif.getX()*verif.getX())+(verif.getY()*verif.getY())+(verif.getZ()*verif.getZ()));
      // indicatif
       //cout<<" \e[34m NORME="<<norme<<"\e[0m ";
        
      if((p1!=p2) && (0<(float)norme)) //on évite la division par zéro
       {
         thetaPP=(float)acos((float)verif.getX()/(float)norme); //on calcule l'angle de p2-p1
         // indicatif
          //cout<<"\e[32m thP1P2="<<thetaPP*180/M_PI<<"\e[0m"; //on affiche l'angle de p2-p1
       
         if(verif.getY()<0) //on calcule en fonction du signe de Y l'angle thetaPP
          {
           Point tempp((float)cos(-thetaPP),(float)sin(-thetaPP),0); //on calcul le vecteur représentant le vecteur p2-p1 avec une norme de 1
           p1p2=tempp; //p1p2 amplifié avec norme égal 1
          }
           else
          {
            Point tempp((float)cos(thetaPP),(float)sin(thetaPP),0); //on calcul le vecteur représentant le vecteur p2-p1 avec une norme de 1
            p1p2=tempp; //p1p2 amplifié avec norme égal 1
          } 
        }
          //p1p2=verif; //le vecteur p2-p1 avec sa norme sans imposition de norme =1
        
     deuxiemeCli=false;
     cout<<endl;
      
     /*titre indicatif
     cout<<"\e[34m np1p2="<<(float)sqrt((p1p2.getX()*p1p2.getX())+(p1p2.getY()*p1p2.getY())+(p1p2.getZ()*p1p2.getZ()))<<" "; //norme du vecteur p1p2
     cout<<" np1="<<(float )sqrt((p1.getX()*p1.getX())+(p1.getY()*p1.getY())+(p1.getZ()*p1.getZ())); //norme du vecteur p1
     cout<<" np2="<<(float )sqrt((p2.getX()*p2.getX())+(p2.getY()*p2.getY())+(p2.getZ()*p2.getZ()))<<"\e[0m"; //norme du vecteur p2
     
     cout<<"\e[35m p1=("<<p1.getX()<<","<<p1.getY()<<") "; //coordonnées p1
     cout<<" p2=("<<p2.getX()<<","<<p2.getY()<<") "; //coordonnées p2
     cout<<" p1p2=("<<p1p2.getX()<<","<<p1p2.getY()<<") \e[0m"; //coordonnées p1p2*/
     //cout<<endl;
     p2.setX(0);
     p2.setY(0);
     p1.setX(0);
     p1.setY(0);
     
     theta=norme*M_PI/(64*500);
     
    }

    else if(!deuxiemeCli)
    {
      p1.setX((float)posX*10000);
      p1.setY((float)posY*10000);
      p1.setZ(0);
      deuxiemeCli=true;
    }
 

    verif.setX(posX*100);
    verif.setY(posY*100);
    thetaPP=(float)acos((float)verif.getX()/(sqrt((float)(verif.getX()*verif.getX())+(verif.getY()*verif.getY()))));

    //indicatif 
    //cout<<"\e[33m tp2="<<thetaPP*180/M_PI<<"\e[0m";
    
  
}


void mouseB(int button, int state, int x, int y)
{
  switch (button)
    {
      
     case (GLUT_MIDDLE_BUTTON):
       if(state==GLUT_UP)
        {
         mousemAc=false;
         deuxiemeCli=false;

        } 

        else if(state==GLUT_DOWN)
        {
         mousemAc=true;
         deuxiemeCli=false;
        }    
      break;
     
      case (GLUT_LEFT_BUTTON):
       
      if(state==GLUT_DOWN)
        {
         mousegAc=true;
         mouseRecup.setX(x);
         mouseRecup.setY(y);
         glutPostRedisplay();
        }    
      break;
   }
}

void mouseM(int x,int y)
{ if(mousemAc)
  {//indicatif
   //std::cout<<" Souris a:"<<x<<" "<<y;
   mouseRecup.setX(x);
   mouseRecup.setY(y);
   glutPostRedisplay();
  }
}

void keyboard(unsigned char key, int x, int y) 
{


  switch (key) 
  {

   case 'y':
    zoom += 0.025;
        
    glutPostRedisplay();
   break;

  case 'h':
    zoom -=0.025;
      
    glutPostRedisplay() ;
  break ;

  case 't':  //vue dessus
   
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(0);
    vsy.setZ(1);

    vsz.setX(0);
    vsz.setY(-1);
    vsz.setZ(0);

    glutPostRedisplay();
  break;

  case 'g': //vue dessous
  
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(0);
    vsy.setZ(-1);

    vsz.setX(0);
    vsz.setY(1);
    vsz.setZ(0);

    glutPostRedisplay();

  break;
  /*
  case'g':
  theta-=M_PI/16;
  glutPostRedisplay();
  break;*/

  case 'r':

    theta=M_PI/64;;

    zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(0);
    vsz.setY(0);
    vsz.setZ(1);



    glutPostRedisplay() ;
  break;

    case 'f':
   
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(0);
    vsx.setY(0);
    vsx.setZ(1);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(-1);
    vsz.setY(0);
    vsz.setZ(0);



    glutPostRedisplay() ;
  break;

    case 'd':
 
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(0);
    vsx.setY(0);
    vsx.setZ(-1);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(1);
    vsz.setY(0);
    vsz.setZ(0);



    glutPostRedisplay() ;
  break;

  case 'v':
  
    theta=M_PI/64;

    mousegAc=false;
    mousemAc=false;
    deuxiemeCli=false;

    vNormalVis=false;

   lignesVis=true;
   polyGVis=false;
   pointVis=false;
   vNsurface=false;
   hautverlebas=false;

    zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(0);
    vsz.setY(0);
    vsz.setZ(1);


    for (int i=0; i<15;i++)
    {
      for(int j=0;j<37;j++)
      {
        delete [] gab.tabP[i][j];
      
        // delete [] gab.tabP[i];
        gab.tabP[i][j]=NULL;
      }
      
    }



    

 
    isaiTabP=0;

    cptPointSais=0;
    mousegAc=false;
    glutPostRedisplay() ;
    break;
    
    case 'a':

    if (lignesVis)
       lignesVis=false;
     else
       lignesVis=true;
    glutPostRedisplay();

    break;


    case 'q':

    if (polyGVis)
       polyGVis=false;
     else if(flatShading)
     { 
       flatShading=false;
       polyGVis=true;
      }
      else
        polyGVis=true;
    glutPostRedisplay();

    break;

    case 's':
    if(!flatShading && polyGVis)
      {
        flatShading=true;
        polyGVis=false;
      }

    else if(flatShading)
    {
      flatShading=false;
    }

    else
    {
      flatShading=true;
    }
    glutPostRedisplay();
    break;

    case 'x':

    if (vNsurface)
       vNsurface=false;
     else
       vNsurface=true;
    glutPostRedisplay();

    break;


    case 'w':
    
     if (vNormalVis)
       vNormalVis=false;
     else if (polyGVis)
      {
       polyGVis=false;
       vNormalVis=true;
      }
      else 
      {
        vNormalVis=true;
      }
    glutPostRedisplay();

    break;

    case 'z':
    if (pointVis)
      pointVis=false;
    else
      pointVis=true;

    glutPostRedisplay();

    break;


    case 'l':
        
     if(lum)
     {  glDisable ( GL_LIGHTING );
        lum=false;
     }
     else
       lum=true;
   
     glutPostRedisplay();
    break;

    
   
    case 27: /* Esc */
    exit(1) ;

  } 
}