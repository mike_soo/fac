#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"

float normeV(Point v); //renvoi la norme d'un vecteur.
Point normaliseV(Point v); //renvoi le vecteur normalisé du vecteur v.

/*opérateur pour les vecteurs*/

bool operator==(Point p1,Point p2); 
bool operator!=(Point p1,Point p2);
Point operator+(Point p1,Point p2);
Point operator-(Point p1,Point p2);
Point operator*(float scal,Point p1); //produit par un scalaire
Point operator * (Point p1,Point p2); //produit vectoriel

int mod(int m, int n); //fonction remplaçant l’opérateur %, spécialement quand on traite des valeurs négatives.
