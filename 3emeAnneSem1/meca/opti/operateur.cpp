#include "simulation.h"


int mod (int m, int n) 
{ return m >= 0 ? m % n : ( n - abs ( m%n ) ) % n; }

bool operator==(Point p1,Point p2)
{
  
  return (p1.getX()==p2.getX()) && (p1.getY()==p2.getY()) &&(p1.getZ()==p2.getZ());

  
}


bool operator!=(Point p1,Point p2)
{

 return !(p1==p2);
}


Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator-(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()-p2.getX());
  presul.setY(p1.getY()-p2.getY());
  presul.setZ(p1.getZ()-p2.getZ());

  return presul;
}


Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}


Point operator * (Point p1,Point p2) //produit vectorielle
{
  Point presul;
  presul.setX( (p1.getY()*p2.getZ())  -  (p1.getZ()* p2.getY()) );

  presul.setY( (p1.getZ()*p2.getX())  -  (p1.getX()* p2.getZ())  );

  presul.setZ( (p1.getX()*p2.getY())  -  (p1.getY()* p2.getX())  );
  return presul;

}

float normeV(Point v)
{
  return (float)sqrt(((float)v.getX()*(float)v.getX())+((float)v.getY()*(float)v.getY())+((float)v.getZ()*(float)v.getZ())); //norme du vecteur normal


}

Point normaliseV(Point v)
{
  Point vNormalise;
  float nV=normeV(v);
  if(nV!=0)
  {
    vNormalise.setX((1.0f/nV)*v.getX());
    vNormalise.setY((1.0f/nV)*v.getY());
    vNormalise.setZ((1.0f/nV)*v.getZ());
    return vNormalise;
  }

  else 
    return v;
  
  
}