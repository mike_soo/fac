<html>
	<head>
		<meta charset="UTF-8">
		 <link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		
		
		<script src ="../jquery/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="../jquery-ui-1.12.1/jquery-ui.min.js"></script>
		<div id="map"></div>
	    <script type="text/javascript">

		var map;
		function initMap() {
		  map = new google.maps.Map(document.getElementById('map'), {
		    center: {lat: 43.6111, lng: 3.87667},
		    zoom: 10
		  });
		}

	    </script>
	    <script async defer
	      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzJuXO_z5mrt99jAHeKhVGrr_YM6gfxr0&callback=initMap">
	    </script>

		<!--<link rel="stylesheet" href="../jquery-ui-1.12.1/jquery-ui.css"></link>-->
		<script src ="functionJS/chargerMap.js"></script>
	</body>
</html>