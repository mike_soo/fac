-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-11-2016 a las 22:49:15
-- Versión del servidor: 5.5.52-0+deb8u1
-- Versión de PHP: 5.6.26-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `VoleyBallArchiWeb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
  `nomE` varchar(40) NOT NULL,
  `nivE` int(8) NOT NULL,
  `idEntraineur` int(20) NOT NULL,
  `idU` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipe`
--

INSERT INTO `equipe` (`nomE`, `nivE`, `idEntraineur`, `idU`) VALUES
('Test', 20, 14, 1),
('Testl', 20, 14, 1),
('Toto', 20, 14, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `joue`
--

CREATE TABLE IF NOT EXISTS `joue` (
  `idE1` int(20) NOT NULL,
  `idE2` int(20) NOT NULL,
  `date` int(8) NOT NULL,
  `res1` int(4) NOT NULL,
  `res2` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `joueur`
--

CREATE TABLE IF NOT EXISTS `joueur` (
  `idP` int(20) NOT NULL,
  `nomE` varchar(40) NOT NULL,
  `numJ` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `joueur`
--

INSERT INTO `joueur` (`idP`, `nomE`, `numJ`) VALUES
(13, 'Test', 20),
(16, 'Test', 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orgGereT`
--

CREATE TABLE IF NOT EXISTS `orgGereT` (
  `idT` int(20) NOT NULL,
  `idP` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
`idP` int(20) NOT NULL,
  `prenomP` varchar(40) NOT NULL,
  `nomP` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personne`
--

INSERT INTO `personne` (`idP`, `prenomP`, `nomP`) VALUES
(1, 'Michael', 'Oh'),
(14, 'Test', 'Test'),
(15, 'Test', 'Test'),
(16, 'Prenomjtest', 'Nomjtest');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pool`
--

CREATE TABLE IF NOT EXISTS `pool` (
`idpool` int(20) NOT NULL,
  `nomPool` varchar(40) NOT NULL,
  `nbrDeMatch` int(8) NOT NULL COMMENT 'indique le nombre de match a jouer avant de decider les gagnat de la pool',
  `nbrEquipes` int(8) NOT NULL COMMENT 'indque le nombre d''equipes contenue dans la pool',
  `idT` int(8) NOT NULL COMMENT 'indique le tournoi mere de la pool courante'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tournoi`
--

CREATE TABLE IF NOT EXISTS `tournoi` (
`idT` int(20) NOT NULL,
  `nomT` varchar(40) NOT NULL,
  `dateCreation` date NOT NULL,
  `dureeJour` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idP` int(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `motdePasse` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `utilisateur`
--

INSERT INTO `utilisateur` (`idP`, `email`, `motdePasse`) VALUES
(1, 'mike@mike.com', '123'),
(14, 'test@test.com', '123'),
(15, 'testdeu@test.com', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipe`
--
ALTER TABLE `equipe`
 ADD PRIMARY KEY (`nomE`,`idU`);

--
-- Indices de la tabla `joue`
--
ALTER TABLE `joue`
 ADD PRIMARY KEY (`idE1`,`idE2`,`date`);

--
-- Indices de la tabla `joueur`
--
ALTER TABLE `joueur`
 ADD PRIMARY KEY (`nomE`,`numJ`), ADD UNIQUE KEY `idP` (`idP`);

--
-- Indices de la tabla `orgGereT`
--
ALTER TABLE `orgGereT`
 ADD PRIMARY KEY (`idT`,`idP`);

--
-- Indices de la tabla `personne`
--
ALTER TABLE `personne`
 ADD PRIMARY KEY (`idP`);

--
-- Indices de la tabla `pool`
--
ALTER TABLE `pool`
 ADD PRIMARY KEY (`idpool`);

--
-- Indices de la tabla `tournoi`
--
ALTER TABLE `tournoi`
 ADD PRIMARY KEY (`idT`);

--
-- Indices de la tabla `utilisateur`
--
ALTER TABLE `utilisateur`
 ADD PRIMARY KEY (`idP`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `email_2` (`email`), ADD UNIQUE KEY `idP` (`idP`), ADD UNIQUE KEY `email_3` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `personne`
--
ALTER TABLE `personne`
MODIFY `idP` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `pool`
--
ALTER TABLE `pool`
MODIFY `idpool` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tournoi`
--
ALTER TABLE `tournoi`
MODIFY `idT` int(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
