//inspiration : https://github.com/mdn/sw-test/blob/gh-pages/sw.js
//https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API/Using_Service_Workers

this.addEventListener('install', function(event){
	event.waitUntil(
		caches.open('v11').then(function(cache) {
			return cache.addAll([
				'/pArchiWeb/testSW~j/',
				'/pArchiWeb/testSW~j/testConnection~j.html',
				'/pArchiWeb/testSW~j/testConnection~j.js',
				'/pArchiWeb/testSW~j/sw~j.js'
			]);
		})
	);
});

this.addEventListener('activate', function(event){
	var cacheWhiteList = ['v11', 'vv2'];

	event.waitUntil(
		caches.keys().then(function(keyList){
			return Promise.all(keyList.map(function(key){
				if (cacheWhiteList.indexOf(key)==-1){
					return caches.delete(keyList[i]);
				}
			}));
		})
	);
});


this.addEventListener('fetch', function(event) {
	event.respondWith(
		fetch(event.request).then(function(response){
			console.log('fetch ok');
			return caches.match(event.request).then(function(){
				return caches.open('v11').then(function(cache){
					cache.put(event.request, response.clone());
					return response;
				}
				).catch(function(err){
					console.log('Erreur ouverture cache', err);
					return response;
				});	
			}).catch(function(){
				console.log('pas dans le cache');
				return response;
			});
		}).catch(function(){
			console.log('fetch non ok');
			return caches.match(event.request).then(function(response){
				console.log(event.request.url);
				if(typeof(response) != "undefined"){
                        		console.log('cache ok');
                                	return response;
				}
				else{
                        		console.log('cache non ok');
					return new Response('Erreur network');
				}
                        });
		})
	)
});
