//Script affichant si on est connecté à internet ou non
// Inspiration : https://developer.mozilla.org/fr/Apps/Build/Hors-ligne
// Inspiratio: https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers

if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('/pArchiWeb/testSW~j/sw~j.js', { scope: '/pArchiWeb/testSW~j/'}).then( function(reg){
		if (reg.installing){
			console.log('Service worker installing');
		} else if(reg.waiting){
			console.log('Service worker installed');
		}else if (reg.active){
			console.log('Service worker active');
		}
	}).catch( function(error) {
			console.log('Registration failed with '+error);
		}
	);
}
else {
	console.log('Pas de service workers');
}

var onlineFunction = function(){
        console.log('We seem to be online!');
	document.body.appendChild(document.createTextNode('oui'));
};

var offlineFunction = function(){
	console.log('We are likely offline');
	document.body.appendChild(document.createTextNode('non'));
};

var request = new window.XMLHttpRequest();

request.open('HEAD', 'https://niecorp.zzzz.io/pArchiWeb/testSW~j/coucou~j.txt');
request.timeout = 5070;

request.addEventListener('load', function(event){
	if(request.response != "Erreur network"){
		onlineFunction();
	}
	else{
		offlineFunction();
	}
	
});



request.send(null);
