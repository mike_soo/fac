<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<?php

	include('head.php');
	include_once "verifInput.php";

	include_once "functions.php";
    include_once "functionsGet.php";


	$erreur=false;
    $equipes=array();
	$nomEq=$nivE=$nomE=$prenomE=$emailE=$motdePasseE="";
	$nomEqErr=$nivEErr=$nomEErr=$prenomEErr=$emailEErr=$motdePasseErr=$verifdePasseEErr="";
    if(!empty($_SESSION['idU']))
    {

        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=VoleyBallArchiWeb;charset=utf8', 'fac', "livefor7livefor7123");
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
        }
        /*
        * AFFICHAGE DES EQUIPES DE L'UTILISATEUR COURANT
        */

        getEquipes($bdd,$equipes,$_SESSION['idU'],-1);


        if(isset($_POST['cEquipe']) )
        {

            /*
             * AFFICHAGE DES EQUIPES DE L'UTILISATEUR COURANT
             */



            if (empty($_POST["nomEq"]))
            {
                $nomEqErr = "Veuillez saisir le nom de l’équipe.";
                $erreur=true;
            }

            else
            {

                $nomEq = ucfirst(strtolower(test_input($_POST["nomEq"]))); //on convertie tout en minus sauf la premier lettre en mayus

                if (!preg_match("/^[a-zA-Z ]*$/",$nomE) or (40<mb_strlen($nomE, 'UTF-8')))
                {
                     $nomEqErr = "Maximum 40 caractères avec uniquement lettres et espaces autorisés.";
                     $erreur=true;
                }
            }

            if (empty($_POST["nivE"]))
            {
                $nivEErr = "Veuillez un niveau une note comprise entre 0 et 20 à l’équipe en cour de création.";
                $erreur=true;
            }

            else
            {
                $nivE = test_input($_POST["nivE"]);
                if (($nivE>20) or ($nivE<0))
                {
                     $nivEErr = "uniquement une note compris entre 0 et 20.";
                     $erreur=true;
                }
            }


            if (empty($_POST["nomE"]))
            {
                $nomEErr = "Veuillez saisir le prénom de l’entraineur de l’équipe.";
                $erreur=true;
            }

            else
            {
                $nomE = ucfirst(strtolower(test_input($_POST["nomE"]))); //on convertie tout en minus sauf la premier lettre en mayus
                if (!preg_match("/^[a-zA-Z ]*$/",$nomE) or (40<mb_strlen($nomE, 'UTF-8')))
                {
                     $nomEErr = "Maximum 40 caractères avec uniquement lettres et espaces autorisés.";
                     $erreur=true;
                }
            }


            if (empty($_POST["prenomE"]))
            {
                $prenomEErr ="Veuillez le prénom de l’entraineur de l’équipe.";
                $erreur=true;
            }

            else
            {
                $prenomE = ucfirst(strtolower(test_input($_POST["prenomE"]))); //on convertie tout en minus sauf la premier lettre en mayus
                if (!preg_match("/^[a-zA-Z ]*$/",$prenomE) or (40<mb_strlen($prenomE, 'UTF-8')))
                {
                     $prenomEErr = "Maximum 40 caractères avec uniquement lettres et espaces autorisés.";
                     $erreur=true;
                }
            }


            if (empty($_POST["motdePasseE"]))
            {
                $motdePasseErr = "Veuillez saisir un mot de passe.";
                $erreur=true;


            }

            if(empty($_POST["verifdePasseE"]))
            {
                $verifdePasseEErr="Veuillez vérifier votre mot de passe.";
                $erreur=true;
            }

            else
            {
                $motdePasseE = test_input($_POST["motdePasseE"]);
                $verifdePasseE=test_input($_POST["verifdePasseE"]);
                if (!preg_match("/[a-zA-z0-9]*[-|_]*/",$motdePasseE) or (30<mb_strlen($motdePasseE, 'UTF-8')) )
                {
                     $motdePasseErr = "Uniquement 30 caractères d'où lettres, numéros et traits d'unions autorisés.";
                     $erreur=true;

                }

                if($motdePasseE!==$verifdePasseE)
                {
                    $erreur=true;
                    $verifdePasseEErr="Vos mots de passe ne sont pas identique, veuillez vérifier votre saisie";
                }
            }

            if (empty($_POST["emailE"]))
            {
                $emailEErr = "Adresse de courriel manquante.";
                $erreur=true;
            }
            else
            {
                $emailE = test_input($_POST["emailE"]);

                if (!filter_var($emailE, FILTER_VALIDATE_EMAIL))
                {
                    $emailEErr = "Format du courrier saisie invalide.";
                    $erreur=true;
                }
            }


            if($erreur==false)
            {

                try
                {
                    $bdd = new PDO('mysql:host=localhost;dbname=VoleyBallArchiWeb;charset=utf8','fac', "livefor7livefor7123");
                }
                catch(Exception $e)
                {
                    error_log($e->getMessage());
                }



                /*
                    on vérifie  l'entraineur associé a la nouvelle équipe
                    avec VE = vérification entraineur
                */

                /*
                *	on vérifie si l'entraineur associé a l’équipe existe déjà
                */

                if($idEnt=utilisateurExiste($bdd,$emailE))
                {
                    /*
                        si on a déjà l'entraineur enregistré
                        on associe donc cette entraineur existant à la nouvelle équipe en cours
                        de création:
                     */
                    $idUc=-1;
                    if($idEnt>=0)
                    {
                        $scE=0;
                        if(1==($scE=creerEquipe($bdd,$nomEq,$nivE,$idEnt,$_SESSION['idU'])))
                        {

                            print("Équipe ".$nomEq." crée avec entraineur déjà existant:". "</br>\n ");
                        }
                        else if($scE==-1)
                        {
                            $nomEqErr="L’équipe saisie existe déjà.";
                        }

                        else
                        {
                            error_log("erreur dans creerEquipe.php après exécution de creerEquipe() avec status de".
                                "la création de l’équipe =".$scE."\n");
                        }
                    }

                    /*
                      sinon on crée la personne associé à l’entraineur qui sera
                      un utilisateur pour faciliter éventuellement à l'organisateur  la tache de création
                      des joueurs avec idUc = idUtilisateurCrée
                    */

                    else if(-1==$idEnt)
                    {
                        if(0<=($idUc=creerUtilisateur($bdd,$prenomE,$nomE,$emailE,$motdePasseE)))
                        {
                            error_log("J'ai crée un entraineur nEnt=".$nomE."\n ");

                            if(creerEquipe($bdd,$nomE,$nivE,$idUc,$_SESSION['idU']))
                            {

                                error_log("J'ai crée une équipe " . $nomE." après avoir crée entraineur nomE=".$nomE."\n");
                                echo "Équipe ".$nomEq." ajouté correctement</br>\n";
                            }

                            else
                            {
                                error_log("erreur dans créer équipe après creerUtilisateur idUc=".$idUc."\n");
                            }
                        }
                    }

                    else
                    {
                        error_log ("équipe non créeeee idUc=".$idUc."\n");
                    }
                }





            }

            else
            {
                print "erreur veuillez ressayer ultérieurement</br>";
            }
        }
    }
 echo json_encode($equipes);
?>




<table>
<form method="post" action ="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for='Nom Equipe'></label>
		<tr>
			<td>Nom de l’équipe:</td>
			<td><input type ='text'  value='toto' name='nomEq' id='nomEq' pattern='[a-zA-Z_]{2,40}' required>
				<span class="error">* <?php echo $nomEqErr;?></span>
			</td>
		</tr>
        <label for='Niveau Equipe'></label>
		<tr>
			<td>Niveau de l’équipe :</td>
			<td><input type='number' value='<?php isset($_POST['nivE'])? print ($_POST['nivE']) : print("20")?>' name='nivE' id='nivE' pattern='[0-9]{2}' required>
				<span class="error">* <?php echo $nivEErr;?></span>
			</td>
			
		</tr>

    <label for='Prenom Entraineur'></label>
		<tr>
			<td>Prénom de l'entraineur de l'équipe:</td>
			<td><input type ='text' value='<?php isset($_POST['prenomE'])? print ($_POST['prenomE']) : print("test")?>' name='prenomE' id='prenomE' pattern='[a-zA-Z_]{2,40}' required>
				<span class="error">* <?php echo $prenomEErr;?></span>
			</td>
		</tr>
    <label for='Nom Entraineur'></label>
		<tr>
			<td>Nom de l'entraineur de l'équipe:</td>
			<td><input type ='text'value='<?php isset($_POST['nomE'])? print ($_POST['nomE']) : print("test")?>'  name='nomE' id='nomE' pattern='[a-zA-Z_]{2,40}' required>
				<span class="error">* <?php echo $nomEErr;?></span>
			</td>
		</tr>
    <label for='Email Entraineur'></label>
		<tr>
			<td>Courrier électronique de l'entraineur:</td>
			<td><input type='emailE' value='<?php isset($_POST['emailE'])? print ($_POST['emailE']) : print("test@test.com")?>' name='emailE' id='emailE' required>
				<span class="error">* <?php echo $emailEErr;?></span>
			</td></br>
		</tr>
	

    <label for='Entraineur mot de passe'></label>
			<tr>
				<td>Mot de passe de l'entraineur :</td>
				<td><input type='password' value='<?php isset($_POST['motdePasseE'])? print ($_POST['motdePasseE']) : print("123")?>' name='motdePasseE' id='motdePasseE' pattern='[a-zA-Z_0-9]{2,50}' required>
					<span class="error">* <?php echo $motdePasseErr;?></span>
				</td>
				
			</tr>

    <label for='Verification mot de passe'></label>
			<tr>
                <td>Verifier mot de passe</td>
				<td><input type='password' value='<?php isset($_POST['verifdePasseE'])? print ($_POST['verifdePasseE']) : print("123")?>' name='verifdePasseE' id='verifdePasseE' pattern='[a-zA-Z_0-9]{2,50}' required>
					<span class="error">* <?php echo $verifdePasseEErr;?></span>
				</td>
				
			</tr>
	
            <tr>
                <td><input type="submit" name="cEquipe" value="Creer équipe"></td>
            </tr>
</form>
</table>


<script src="js/functions.js"></script>
<script>

    var jsequipes=<?php echo json_encode($equipes);?>;
    console.log(jsequipes);
    console.log("coucou");
    //afficheEquipes(jsequipes);
    //afficheGenerique(jsequipes,oaffEquipes,0);




</script>

<?php include ('footer.php');?>