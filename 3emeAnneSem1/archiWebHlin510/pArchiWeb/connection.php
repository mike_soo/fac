<?php include ('head.php');?>

   
	<!--
		connection.php

		Ce script sert à s'authentifier.
		Il est séparé en deux blocs :
			- Un bloc php vérification des données du formulaire.
			- Un bloc  html affichage du formulaire.

	-->

	<!--
		Premier bloc php de vérification des données du formulaire :
		- Le login et le mot de passe doivent être présents et correspondre
			dans la base de données, si oui alors sauvegarde du login
			dans la variable de session et redirection vers
			la page principale.
	-->
	

	<?php
		
		include_once "verifInput.php";
		
		if(isset($_POST['aCompte']))
		{	
			$email = $motdePasse = "";
			$emailErr = $motdePasseErr = "";
			
			$erreur=false;


			if (empty($_POST['email'])) 
			{
			    $emailErr = "Adresse de courriel manquante.";
			    $erreur=true;
			} 
			else 
			{

			    $email = test_input($_POST["email"]);
			    
			    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
			    {
      				$emailErr = "Format du courrier saisie invalide.";
      				$erreur=true;
    			}
			}



			if (empty($_POST["motdePasse"])) 
			{
			    $motdePasseErr = "Veuillez saisir un mot de passe.";
				$erreur=true;
			} 

			else 
			{
			    $motdePasse = test_input($_POST["motdePasse"]);
			    if (!preg_match("/[a-zA-z0-9]*[-|_]*/",$motdePasse) or (30<mb_strlen($motdePasse, 'UTF-8')) ) 
			    {
     				$motdePasseErr = "Uniquement 30 caractères d'où lettres, numéros et traits d'unions autorisés.";
    				$erreur=true;
    			}
			}

			if(!$erreur)
			{ 

				try 
				{
					$bdd = new PDO('mysql:host=localhost;dbname=VoleyBallArchiWeb;charset=utf8','fac', "livefor7livefor7123");
				}
				catch(Exception $e)
				{
					die($e->getMessage());
				}

				
				$stmtlogin = $bdd->prepare("SELECT utilisateur.idP,nomP,prenomP  
								   FROM utilisateur,personne
								    WHERE utilisateur.email=?
									 and utilisateur.motdePasse=? and 
										utilisateur.idP=personne.idP");
				$stmtlogin->bindParam(1,$_POST['email']);
				$stmtlogin->bindParam(2,$_POST['motdePasse']);

				
				
				if($stmtlogin->execute())
				{	
					
					if(($id=$stmtlogin->fetch())!=null)
					{
						//session_start();
						error_log("\n idddd".$id['idP'] . "\n");  
						
						$_SESSION['idU']=$id['idP'];
						$_SESSION['nomU']=$id['nomP'];
						$_SESSION['prenomU']=$id['prenomP'];
						
						header('location: accueilU.php');
					}
					else
					{
						echo "non reconnue";
						//header('location: accueilU.php');
					}
				}
			}
		}
		
	?>

	<!--
		Deuxième bloc php : formulaire.
	-->

	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
		<table>
			<label for='Email'>
				<tr>
				<td>Saisir votre courrier électronique :</td></label>
				<td><input  type='email' name='email' id='email' required></td></br>
				</tr>
			<label for='Mot de passe'>
				<tr>
				<td>Saisir mot de passe :</td></label>
				<td><input  type='password'  name='motdePasse' id='motdePasse' pattern='[a-zA-Z_0-9]{2,50}' required></td>
				</tr>
		</table>
		<input type="submit" name="aCompte" value="S'authentifier">

		
	
	</form>
	<form method="post" action= "inscription.php">
		<input type="submit" name="cCompte" value="Creer un nouveau compte">
	</form>
<?php include('footer.php');?>