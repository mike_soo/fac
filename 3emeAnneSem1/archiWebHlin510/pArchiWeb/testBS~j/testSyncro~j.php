<!DOCTYPE html>
<html>
<head>
	<meta charset='UTF8'/>
	<title>TEST SYNCRONISATION</title>
</head>
<body>
	<!--
		Sources :
			http://php.net/manual/fr/function.file-get-contents.php
			http://stackoverflow.com/questions/9529112/json-to-php-array-using-file-get-contents
	-->
	<?php
		function afficherTchat(){
			echo "<table id='myTable'>";
			echo "<tr><th>Pseudo</th><th>Comment</th><th>Etat</th></tr>";
			$json = file_get_contents('./data.json', FILE_USE_INCLUDE_PATH);
			//echo "$json";

			$tab = json_decode($json, true);

			//print_r($tab);

			foreach($tab["tchat"] as $com){
				echo "<tr>";
				foreach($com as $key => $val){
					echo "<td>$val</td>";
				}
				echo "<td>Envoyé</td>";
				echo "</tr>";
			}		
			
			echo "</table>";
		}


		function nettoyerFichier(){
			unlink("data.json");
		}

		function entrerValeur($pseudo, $comment){
			$json = file_get_contents('./data.json', FILE_USE_INCLUDE_PATH);
			$tab = json_decode($json, true);

			//unlink ("./data.json"); pas le droit
			
			$tab["tchat"][] = array(
				"pseudo" => $pseudo,
				"comment" => $comment
			);


			nettoyerFichier();

			$fp = fopen("data.json", "a+");
			fputs($fp, json_encode($tab, true));
	
			fclose($fp);
		}

		function supprimerTchat(){
			$json = file_get_contents('./data.json', FILE_USE_INCLUDE_PATH);
			$tab = json_decode($json, true);
			
			$tab["tchat"] = [];

			

			nettoyerFichier();

			$fp = fopen("data.json", "a+");
			fputs($fp, json_encode($tab, true));
	
			fclose($fp);
		}

		echo "Post : ";
		var_dump($_POST);
		echo "<br/>";
		if(isset($_POST['delete'])){
			supprimerTchat();
		}
		if(isset($_POST["pseudo"]) && isset($_POST["comment"])){
			entrerValeur($_POST["pseudo"], $_POST["comment"]);
		}

		afficherTchat();
		
	?>

	<span id="myText"></span>

	<form name="myForm" method="post"> 
		<label for="pseudo">Pseudo</label>
		<input type="text" id="pseudo" name="pseudo">
		<label for="comment">Commentaire</label>
		<input type="text" id="comment" name="comment">
		<input type="submit" value="envoyer">
	</form>

	<form name="deleteForm" method="post">
		<label for="delete">Supprimer tchat ?</label>
		<input type="submit" id="delete" name="delete" value="delete">
	</form>


	<script src="jQuery.js"></script>
	<script src="testSyncroV2~j.js"></script>
</body>
</html>
