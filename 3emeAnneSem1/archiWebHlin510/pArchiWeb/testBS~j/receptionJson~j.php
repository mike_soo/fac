<?php
	$new = null;
	if(isset($_POST["tchat"])){
		$new = $_POST["tchat"];
	}
	$delete = null;
	if(isset($_POST["delete"])){
		$delete = true;
	}

	if($delete != null){
		$json = file_get_contents('./data.json', FILE_USE_INCLUDE_PATH);
		$tab = json_decode($json, true);

		$tab["tchat"]="";

		unlink("data.json");

		$fp = fopen("data.json", "a+");
		fputs($fp, json_encode($tab, true));

		fclose($fp);		
	}

	if($new != null){
		$json = file_get_contents('./data.json', FILE_USE_INCLUDE_PATH);
		$tab = json_decode($json, true);
		
		foreach($new as $elem){
			$tab["tchat"][] = $elem;
		}

		unlink("data.json");

		$fp = fopen("data.json", "a+");
		fputs($fp, json_encode($tab, true));

		fclose($fp);		
 	}
?>
