//Script implémentant le tchat 
//Inspiration : https://developers.google.com/web/updates/2015/12/background-sync
// How to send json data to the server
// http://stackoverflow.com/questions/4342926/how-can-i-send-json-data-to-server
// LocalStorage :
//http://www.alsacreations.com/article/lire/1402-web-storage-localstorage-sessionstorage.html
//Validation d'un formulaire avec javascript :
//http://www.w3schools.com/js/js_validation.asp
//Background synchronisation is not ready for firefox : https://jakearchibald.github.io/isserviceworkerready/

//version 10

if('serviceWorker' in navigator) {
	navigator.serviceWorker.register('/pArchiWeb/testBS~j/sw~j.js', { scope: '/pArchiWeb/testBS~j/'}).then( function(reg){
		if(reg.installing){
			console.log('Service worker installing');
		} else if(reg.waiting){
			console.log('Service worker installed');
		} else if (reg.active){
			console.log('Service worker active');
		}
	}).catch( function(error) {
			console.log('Registration failed with '+error);
		}
	);
}

var ajaxPost = function(url, data, callback){
	var req = new XMLHttpRequest();
	req.open("POST", url, false);
	req.addEventListener("load", function(){
		if(req.status >= 200 && req.status < 400){
			callback(req.responseText);
                }
                else {
                        console.error(req.status + " " + req.statusText + " " + url);
                }
        });
        req.addEventListener("error", function() {
                console.error("Erreur réseau avec l'URL "+url);
        });
        req.send(data);
};


var syncroDonneesServer = function(){
        if(typeof localStorage!='undefined'){
                if("tchat" in localStorage || "delete" in localStorage){
                        var url = "https://niecorp.zzzz.io/pArchiWeb/testBS~j/receptionJson~j.php";
                        var data = {};

                        if("tchat" in localStorage){
                                data = JSON.parse(localStorage.getItem("tchat"));
                        }
                        else{
                                data["tchat"]=[];
                        }
                        if("delete" in localStorage){
                                data["delete"]="true";
                        }
                        else{
                                data["delete"]="false";
                        }

                        localStorage.removeItem("tchat");
                        localStorage.removeItem("delete");

                        ajaxPost(url, "json="+JSON.stringify(data), function(response){
                                console.log("Data envoyée au serveur : ", response);
                        });
                }
        }
};


var nettoyerLocalStorage = function(){
	if(typeof localStorage != 'undefined'){
		if("delete" in localStorage){
			localStorage.removeItem("delete");
		}
		if("tchat" in localStorage){
			localStorage.removeItem("tchat");
		}
	}
};

nettoyerLocalStorage();

var nettoyerFormulaire = function(){
	document.forms["myForm"]["pseudo"].value = "";
	document.forms["myForm"]["comment"].value = "";
};

var afficherDonneesClient = function(){
	if(typeof localStorage != 'undefined'){
		console.log("Affichage données client :");
		if("delete" in localStorage){
			console.log("Bool delete:", localStorage.getItem("delete"));
		}
		else {
			console.log("Pas de valeur delete");
		}
		if("tchat" in localStorage){
			console.log("tchat :", JSON.stringify("tchat"));
		}
		else{
			console.log("Pas de données tchat");
		}
	}
	else{
		alert("localStorage n'est pas supporté");
	}
};

var nettoyerTchat = function(){
	console.log("Nettoyage des données du tchat non présentes dans localStorage");
	var table = document.getElementById("myTable");
	var child = null;
	/*
		L'élément du DOM ayant pour ID : myTable, contient
		- Un child tbody contenant :
			- un child tr contenant les entetes des colonnes
			- plusieurs childs tr provenant du server
		- Plusieurs child tr provenant de localStorage

		On souhaite enlever les childs tr provenant du server
		et les childs provenant de localStorage
	*/
	//suppresions childs provenant de localStorage
	while(table.childNodes.length != 1){
		child = table.childNodes[1];
		console.log("Suppression : ", child.childNodes[0].childNodes[0], child.childNodes[1].childNodes[1]);
		table.removeChild(child);
	}
	//suppression childs provenant du server
	while(table.childNodes[0].childNodes.length != 1){
		child = table.childNodes[0].childNodes[1];
		console.log("Suppression : ", child.childNodes[0].childNodes[0], child.childNodes[1].childNodes[1]);
		table.childNodes[0].removeChild(child);
	}
	console.log("Fin nettoyage");
};


var deleteTchat = function(){
	if(navigator.onLine){
		console.log("Navigator online");
	 	syncroDonneesServer();
		return true;
	}
	else {
		if(typeof localStorage!='undefined'){
			console.log("---------------");
			console.log("Delete du tchat");
			localStorage.setItem("delete", true);
			if("tchat" in localStorage){
				localStorage.removeItem("tchat");
			}		

			nettoyerTchat();
			nettoyerFormulaire();
			afficherDonneesClient();
			console.log('Fin du delete');	
			console.log("---------------");
		}
		else {
			alert("localStorage n'est pas supporté");
		}	
		return false;
	}	
};


var envoyerCommentaire = function(){
	if(navigator.onLine){
		console.log("Navigator online");
		syncroDonneesServer();
		return true;
	} else {
		if(typeof localStorage!='undefined'){
			console.log("------------------");
			console.log("Envoyer commntaire");
			if('tchat' in localStorage){
				var tchat = JSON.parse(localStorage.getItem("tchat"));
			}
			else {
				var tchat = {"tchat": []};
	
			}	
	
			var pseudo = document.forms["myForm"]["pseudo"].value;
			var comment = document.forms["myForm"]["comment"].value;

			tchat["tchat"].push({
				"pseudo": pseudo,
				"comment": comment
			});

			var tr = document.createElement("tr");

			var td1 = document.createElement("td");
			td1.appendChild(document.createTextNode(pseudo));

			var td2 = document.createElement("td");
			td2.appendChild(document.createTextNode(comment));

			var td3 = document.createElement("td");
			td3.appendChild(document.createTextNode("En cours d'envoi"));

			tr.appendChild(td1);
			tr.appendChild(td2);
			tr.appendChild(td3);

			document.getElementById("myTable").appendChild(tr);

			localStorage.setItem("tchat", JSON.stringify(tchat));
			afficherDonneesClient();
			console.log("------------------");
		}	
		else {
			alert("localStorage n'est pas supporté");
		}
		return false;
	}
}
