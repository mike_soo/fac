if('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/pArchiWeb/testBS~j/sw~j.js', { scope: '/pArchiWeb/testBS~j/'}).then( function(reg){
                if(reg.installing){
                        console.log('Service worker installing');
                } else if(reg.waiting){
                        console.log('Service worker installed');
                } else if (reg.active){
                        console.log('Service worker active');
                }
        }).catch( function(error) {
                        console.log('Registration failed with '+error);
                }
        );
}

$(function(){
	$('#pseudo').focus();

	$("#myText").change(function(){
		var str = "Localstorage : ";

		if(typeof(localStorage) != 'undefined'){
			str += JSON.stringify(localStorage);
		}
		else{
			str += 'undefined';
		}

		$("#myText").html(str);
	});

	$("#myText").trigger("change");

	$('[name="myForm"]').submit(function(){
		if(navigator.onLine){
			return true;
		}
		else {
			console.log("Formulaire soumis : ");
			console.log("Pseudo : ", $("#pseudo").val());
			console.log("Comment : ", $("#comment").val());
			if(typeof(localStorage) != 'undefined'){
				var tchat = null;

				if("tchat" in localStorage){
					tchat = JSON.parse(localStorage.getItem("tchat"));
				}
				else{
					tchat = { "tchat": []};
				}

				tchat["tchat"].push({
					"pseudo": $("#pseudo").val(),
					"comment": $("#comment").val()
				});

				localStorage.setItem("tchat", JSON.stringify(tchat));
				var str = '<tr class="removable"><td>'+$("#pseudo").val()+'</td><td>'+$("#comment").val()+'</td><td>En cours</td></tr>';
				$("#myTable").append($(str));
			}
			else{
				alert("Localstorage n'est pas supporté par le navigateur");
			}
			$("#myText").trigger('change');
			return false;
		}
	});

	$('[name="deleteForm"]').submit(function(){
		if(navigator.onLine){
			return true;
		}
		else{
			if(typeof(localStorage) != 'undefined'){
				console.log("Tentative suppression tchat");
				localStorage.setItem("delete", true);
				localStorage.removeItem("tchat");
				$("tr").remove();
			}
			else {
				alert("Localstorage n'est pas supporté par le navigateur");
			}
			$('#myText').trigger('change');
			return false;
		}
	});

	$(document).on('offline', function(){
		alert('You are offline !');
	});

	$(document).on('online', function(){
		alert('You are online !');
		var tchat = null;
		if(typeof(localStorage) != 'undefine'){
			tchat = JSON.parse(localStorage.getItem('tchat'));
		}
		else{
			tchat= {"tchat":[]};
		}
		$.post( "https://niecorp.zzzz.io/pArchiWeb/testBS~j/receptionJson~j.php", tchat)
		.fail(function(){
			alert("Error");
		});

		if(typeof(localStorage) != 'undefined'){
			if("tchat" in localStorage){
				localStorage.removeItem('tchat');
			}
		}

		if(typeof(localStorage) != 'undefined' && "delete" in localStorage){
			
			$.post( "https://niecorp.zzzz.io/pArchiWeb/testBS~j/receptionJson~j.php", {"delete": "true"})
			.fail(function(){
				alert("Error");
			});
			localStorage.removeItem("delete");
		}

		$("td:nth-child(3)").html("Envoyé");
		$('#myText').trigger('change');
	});
});
