<?php

function connectionbd(&$bdd)
{
    try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=VoleyBallArchiWeb;charset=utf8','fac', "livefor7livefor7123");
    }
    catch(Exception $e)
    {
        error_log($e->getMessage());
    }
}
    /*
     * Stoque dans une array tous les équipes de l'utilisateur
     */

    function getEquipes(&$bdd,&$equipes,$idU,$idT)
    {

        $stmtgetEq=$bdd->prepare("
                                    SELECT *
                                    FROM equipe
                                    WHERE equipe.idU=:idU
                                    
                                 ");
        $stmtgetEq->bindParam(':idU',$idU);

        if($stmtgetEq->execute())
        {
            while(null!=($equipe=$stmtgetEq->fetch()))
            {
                $equipes[$equipe['idEq']]=array
                (
                    'nomEq'=>$equipe['nomEq'],
                    'nivE'=>$equipe['nivE'],
                    'idU'=>$equipe['idU']
                );
            }
        }
        else
        {
            error_log("Erreur de requete l. 43 dans getEquipes");
        }
        /*
        foreach($equipes as $idEq=>$eq)
        {
            echo $eq['nomEq']."</br>";
        }
     */
        //return $equipes;
    }


    function getTournois(&$bdd,&$tournois,$idU)
    {

        $stmtgetTournois=$bdd->prepare("
                                    SELECT orgGereT.idT,
                                            tournoi.nomT,
                                            tournoi.dateDeroul,
                                            tournoi.dureeJour
                                    FROM orgGereT,tournoi
                                    WHERE orgGereT.idP=:idU and
                                          orgGereT.idT=tournoi.idT
                                 ");


        $stmtgetTournois->bindParam(':idU',$idU);

        if($stmtgetTournois->execute())
        {
            while(null!=($tournoi=$stmtgetTournois->fetch()))
            {
                $tournois[$tournoi['idT']]=array
                (
                    'nomT'=>$tournoi['nomT'],
                    'dateDeroul'=>$tournoi['dateDeroul'],
                    'dureeJour'=>$tournoi['dureeJour']
                );
            }
        }
        else
        {
            error_log("Erreur de requete l. 43 dans getEquipes");
        }

    }

    function getTournoiContenu(&$bdd,&$Atournoi,$idT,$idU)
    {
        //echo "___".json_encode($Atournoi)."</br>";
        $stmtgetT=$bdd->prepare("
                                    SELECT  
                                            equipe.idEq,
                                            equipe.nomEq,
                                            equipe.nivE,
                                            equipe.idU,
                                            TetEqdonneP.idP,
                                            TetEqdonneP.numPool
                                            
                                    FROM tournoi,TetEqdonneP,equipe
                                    WHERE tournoi.idT=:idT and TetEqdonneP.idT=tournoi.idT 
                                    and TetEqdonneP.idEq=equipe.idEq
                                    ORDER BY equipe.nomEq
                                 ");
        $Atournoi['equipes']=null;

        $stmtgetT->bindParam(':idT',$idT);

        if($stmtgetT->execute())
        {
            while(null!=($equipe=$stmtgetT->fetch()))
            {

                $Atournoi['equipes'][$equipe['idEq']]=
                (
                    array
                    (

                    'nomEq'=>$equipe['nomEq'],
                    'nivE'=>$equipe['nivE'],
                    'idU'=>$equipe['idU'],
                    'idP'=>$equipe['idP'],
                    'numPool'=>$equipe['numPool']
                    )
                );
                //echo "______".json_encode($Atournoi)."</br></br>";
            }


            //print_r($Atournoi);
            //echo ">>>>" .json_encode($Atournoi)."</br></br>";
        }
        else
        {
            error_log("Erreur dans getTournois avec idU= $idU");
            error_log( "\nPDO::errorInfo():\n");
            print_r($bdd->errorInfo());

        }
    }


    function getEquipesSansTournoi(&$bdd,&$Atournoi,$idU)
    {
        /*
             * On inclue aussi les equipes qui n'appartiennent pas
             * a un tournoi
             */

        $stmtgetsansT=$bdd->prepare("
                                    SELECT  equipe.idEq,
                                            equipe.nomEq,
                                            equipe.nivE,
                                            equipe.idU
                                    FROM equipe
                                    WHERE equipe.idU=:idU and equipe.idEq NOT IN 
                                      (SELECT TetEqdonneP.idEq
                                      FROM tournoi,TetEqdonneP
                                      WHERE equipe.idEq=TetEqdonneP.idEq and TetEqdonneP.idT=tournoi.idT)
                                      ORDER BY equipe.nomEq");

        $stmtgetsansT->bindParam(':idU',$idU);
        if ($stmtgetsansT->execute())
        {
            if(empty($Atournoi['-1']))
            {
                $Atournoi['-1'] = array
                (
                    'nomT' => 'aucun',

                    'equipes'=>array()
                );
            }
            while(null!=($eqSansTourn=$stmtgetsansT->fetch()))
            {




                $Atournoi['-1']['equipes'][$eqSansTourn['idEq']]=
                array
                (

                    'nomEq'=>$eqSansTourn['nomEq'],
                    'nivE'=>$eqSansTourn['nivE'],
                    'idU'=>$eqSansTourn['idU']
                );


            }

        }
        else
        {
            error_log("erreur requete stmtgetsansT");
        }
    }
    /*
     * REMPLIS ensTournois:ENSEMBLE DES TOURNOIS AVEC TOUS LES
     * TOURNOIS DE L'UTILISATEUR COURRANT.
     */
    function getEnsTournoiContenu(&$bdd,&$ensTournois,$idU)
    {

        getTournois($bdd,$ensTournois,$idU);

        foreach($ensTournois as $idT => &$tournoi)
        {
            getTournoiContenu($bdd, $tournoi, $idT,$idU);

        }


        getEquipesSansTournoi($bdd,$nonTournoi,$idU);

        $ensTournois["-1"]=$nonTournoi["-1"];
        /*

            $AensTournois=array();
            array_push($AensTournois,$ensTournois);
            $ensTournois=$AensTournois;
        */
        //echo "</br>";
        //echo "\n</br> . ensTournois </br> ". json_encode($ensTournois,JSON_PRETTY_PRINT)."</br>";
	echo json_encode($ensTournois, JSON_PRETTY_PRINT);


    }
    // afaire !!! function getTournoiContenue


?>
