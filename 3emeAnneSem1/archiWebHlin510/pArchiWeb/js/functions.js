
var jsensTournois={};
var jsensEquipes={};
var jsensSynchro={};
var tournoisExec={};
var ensPooldeidTcour={};


/*
 *complemObjet['typeEvens'] : 1 =>GERE LES EVENEMENT DE CHAQUE <td>
 *              D'EQUIPE POUR L'INSERER DANS UN TOURNOI
 *             EN FONCTION DES CLICK DE L'UTILISATEUR
 *
 *          2 =>GERE LES EVENEMENT DE CHAQUE <TD>
 *              CONTENANT TOUS LES TOURNOI, LORS DE LA PROPOSITION
 *              DE LA GESTION DES TOURNOI,APRES UN CLICK SUR UN TOURNOI
 *              ON AFFICHE TOUS LES DETAILS SUR CE MEME TOURNOI.
 */

function afficheGenerique(objets,idObjet,complemObjet)
{
    var tabT;
    var titretr;
    var divT ;
    var tbody;
    var nouvTable=false;
    //console.log(divAnt);
    /*
    divAnt.forEach(function(d)
    {
        console.log("d= "+d);
    });*/

    if(!$("#div"+idObjet).length)
    {
       divT = $("<div></div>").attr("id", "div"+idObjet);


    }
    else
    {
        divT = $("#div" + idObjet);


    }
    $("body").append(divT);

    console.log("idObjet="+idObjet);


    if(!$("#tab"+idObjet).length)
    {
        nouvTable=true;
        tabT = $("<table></table>").attr("id", "tab"+idObjet,"class","sortable");
        tbody=$("<tbody></tbody>").attr("id","tbody"+idObjet);
    }
    else
    {
        tabT=$("#tab"+idObjet);
        tbody=$("#tbody"+idObjet);
    }

    divT.append(tabT);
    tabT.append(tbody);

    if(!$("#titre"+idObjet).length)
    {
        thead=$("<thead></thead>");

        titretr = $("<tr></tr>").attr("id","titre"+idObjet);
        titretr.append($("<th>"+idObjet+"</th>").attr("id","tempheadID"+idObjet));

        for (t in complemObjet["titres"])
        {
           $(
                "<th>" +
                complemObjet["titres"][t] +
                "</th>"
            ).appendTo(titretr);
        }
        thead.append(titretr);
        tabT.append(thead);

    }



    $.each
    (objets, function (idO, objet)

        {
            if(idO==-1  || objet==null)
                return;


            //console.log("idO="+idO);
           // console.log (" dans "+idObjet+" " +objet );


            if(!$("#"+idObjet+"ID"+idO).length)
            {
                caractr = $("<tr></tr>").attr("id",idObjet+"ID"+idO);
                caractr.append("<td>"+idO+"</td>");
               // console.log("idO="+idO +" objet="+objet);
                $.each(objet, function (idEi, caracObjet) {

                    //console.log( "    idEi="+idEi + " " +caracObjet);
                    if(complemObjet["titres"][idEi]!=null)
                    {
                        caractr.append("<td>" + caracObjet +"</td>");
                    }
                });
                tbody.append(caractr);
                if(complemObjet["typeEvens"]['0']===true)
                {

                }
                /*
                * ON CODE PAR LA SUITE LES EVENEMENT DE CHAQUE <td>
                * D'EQUIPE POUR L'INSERER DANS UN TOURNOI
                 */
                if (gereAffiGen.typeAff.affTC.lierNEqTEqT)
                {
                    /*
                    * <tr></tr> suivant contient l'information d'une
                    * equipe qui se trouve ou pas dans un tournoi donnée.
                    * On vient stocké le ptr de ce meme tr dans jsentiteInBody.equipe.trInnIndTptrs
                    * pour faciliter un future accès.
                     */


                    var regexTEq,typeEquip;
                    //let cptr=jsentiteInBody.equipe.cptr;
                    var trEqINNIN=$("#"+idObjet+"ID"+idO);
                    caractr.click
                    (
                        function()
                        {

                            /*
                             * ON FIXE LE POINTEUR SUR LE <tr> QUI CONTIENT
                              * L'INFORMATION QUI NOUS PERMET DE
                             * DETERMINER LE PAR LA SUITE GRACE UNE EXPRESSION REG
                              * SI IL S'AGIT D'UN EQUIPE DANS UN TOURNOI
                             * OU D'UN EQUIPE QUI N'EST PAS DANS CE MEME TOUNROI.
                             * CE PROCEDER NOUS PERMET DE CHANGER L'EMPLACEMENT D'UNE
                             * EQUIPE GERER PAR L'UTILISATEUR. ON PERMET DONC DE RENTRER UNE
                             * EQUIPE OU DE LE SORTIR D'UN TOURNOI
                             * valeur de regexTEq  posible: equiInidT ou equiNonInidT
                             */

                            /*
                            console.log("tratId="+trEqINNIN.attr("id"));
                            console.log("cptr="+jsentiteInBody.equipe.cptr);
                            */
                            regexTEq=new RegExp("(.*?)ID");
                            typeEquip=regexTEq.exec(trEqINNIN.attr("id"));
                            //console.log("typeEq="+typeEquip);
                            //console.log("typeEq[0]="+typeEquip[0]);
                            //console.log("typeEq[1]="+typeEquip[1]);
                            oaffEquipes.typeEvens.lierNEqTEqT(typeEquip[1],idO);


                        }
                    );
                    jsentiteInBody.equipe.cptr++;
                }
                /*
                 *  L'EVENEMENT 2 GERE LA SELECTION D'UN TOURNOI PAR L'UTILISATEUR
                 *  ON PLACE UN EVENEMENT DANS CHAQUE NOM DE TOURNOI QUI VIENDRA AFFICHER
                 *  LES EQUIPES APPARTENANT A CE TOUNROI, ET LES EQUIPE QUI N'APARTIENNENT
                 *  PAS A CE TOURNOI AFIN DE POUVOIR GERER LE CONTENUE D'UN TOUNOI
                 */


                if (gereAffiGen.typeAff.affTC.affTs && gereAffiGen.typeAff.affTC.affEqs)
                {
                    elCour=$("#"+idObjet+"ID"+idO);
                    caractr.click
                    (
                        function()
                        {
                            //console.log('je click sur '+elCour.attr("id"));
                            complemObjet.typeEvens.affGestionEquipes(idO);



                        }
                    );

                }

                /*
                *   ON GERE L'AFFICHAGE DU CONTENUE DE CHAQUE POOL;
                */
                if(gereAffiGen.typeAff.affPC.affPs)
                {
                    var divtr;
                    function a(el)
                    {
                        let tournSelec=jsentiteInBody.tournoi.tournSelec;
                        console.log('affiche equipes dans pool');
                        console.log(JSON.stringify(ensPooldeidTcour));
                        //if(!$("#eqsDansPool").length)
                        gereAffiGen.typeAff.affPC.affPs=false;
                        gereAffiGen.typeAff.affPC.affEqsInPs=true;
                        divtr=afficheGenerique(ensPooldeidTcour[tournSelec][idO],"eqsDansPool"+idO,oaffEquipes);
                        divtr.attr('class','Pscontenue');
                        gereAffiGen.typeAff.affPC.affEqsInPs=false;
                        divtr.toggle(true);
                        console.log("11");
                        console.log(divtr);

                    }

                    function b(el){
                        divtr.toggle(false);
                        console.log("22");
                    }

                    caractr.click(function() {

                        var el = this;
                        return (el.t = !el.t) ? a(el) : b(el);
                    });

                    /*caractr.click
                    (
                        function()
                        {

                            let tournSelec=jsentiteInBody.tournoi.tournSelec;
                            console.log('affiche equipes dans pool');
                            console.log(JSON.stringify(ensPooldeidTcour));

                            afficheGenerique(ensPooldeidTcour[tournSelec][idO],"eqsDansPool"+idO,oaffEquipes);



                        }
                    );*/
                }

            }

        }

    );




    /*
    if(nouvTable)
    {
       // tsorter.create("tab"+idObjet);
    }*/
    return divT;
}

var jsentiteInBody=
    {
        "tournoi":
        {
            /*tournSelec : idT   //contient le tournoi actuellement selectioné par l'utilisateur*/
        },
        "equipe":
            {
                "eqNonInidT" :
                {

                },
                "eqInidT":
                {

                },
                cptr : 0,
                "trInNindTptrs":
                {

                },


                /*
                 *  CACHE TOUS LES DIVS DES EQUIPE QUI N'APARTIENNENT
                 *  PAS A UN TOURNOI idTx <=> DESAFFICHE TOUT LE CONTENU DE eqNonInidT
                 */
                "desafficheENDTs": function()

                {

                    $.each(this.eqNonInidT,
                        function(divendt,estaffiche)
                        {
                            console.log("je desaficheendts" + divendt);
                            if(estaffiche)
                            {
                                $("#"+divendt).toggle();
                                jsentiteInBody.equipe.eqNonInidT[divendt]=false;

                            }
                        });
                },

                /*
                 *   CACHE TOUS LES DIVS DES EQUIPE QUI APARTIENNENT
                 *    A UN TOURNOI idTx <=> DESAFFICHE TOUT LE CONTENU DE eqInidT
                 */

                "desafficheEDTs": function()
                {
                    $.each(this.eqInidT,
                        function(divedt,estaffiche)
                        {
                            console.log("je desaficheedts" + divedt);
                            if(estaffiche)
                            {
                                $("#"+divedt).toggle();
                                jsentiteInBody.equipe.eqInidT[divedt]=false;
                            }
                        });
                },



                "changeAffENDT": function(divendt)
                {
                    if(this.eqNonInidT[divendt]!=null )
                    {
                        $("#"+divendt).toggle();

                        if(this.eqNonInidT[divendt])
                            this.eqNonInidT[divendt]=false;

                        else
                            this.eqNonInidT[divendt]=true;
                    }
                },


                "changeAffEDT": function(divedt)
                {
                    if(this.eqInidT[divedt]!=null )
                    {
                        $("#"+divedt).toggle();

                        if(this.eqInidT[divedt])
                            this.eqInidT[divedt]=false;

                        else
                            this.eqInidT[divedt]=true;
                    }
                }


            },
        "joueur":{}
    };


var gereAffiGen=
{
    "typeAff" :
    {
        "affTC" : //affichage de tournoi contenu
        {
            "affEqs" : false,
            "affTs"  : false,
            /*
                evenenement obligeant affichage generique
                à lier une equipe qui napartient pas a un tournoi avec l'ensemble
                des equipes qui apartiennent a ce tournoi pour rajouter ou enlever
                des equipes d'un tournoi
            */
            "lierNEqTEqT" : false
        },
        "affPC" : //affichage de pool contenu
        {
            "affPs" : false,
            "affEqs" : false,
            "affEqsInps":false //control de pool contenant des equipes
        }
    }
};



/*Objet pour afficher les diferent tournois dispo*/
var oaffTournoi =
{
    "titres":
    {
        "nomT":"Nom tournoi",
        "dateDeroul":"Date de début",
        "dureeJour":"durée en jours"
    },

    "typeEvens":
    {
        '0': false,
        '1': false,
        '2': true,

        "affGestionEquipes" : function(idT)
        {

            //idT clické para l'utilisateur dans gestion des tournois

            if((jsensTournois[idT]!=null) /*&& !($("#equipeInidT"+idT).length) && !($("#equipeNonInidT"+idT).length)*/)
            {
                /*
                * AFFICHE TOUS LES EQUIPES APARTENANT AU TOURNOI idT
                * ET TOUS LES EQUIPES NON INCLUE DANS LE TOURNOI idT
                */
                gereAffiGen.typeAff.affTC.lierNEqTEqT=true;
                oaffEquipes.typeEvens.equipeDispoPT(idT);
                gereAffiGen.typeAff.affTC.lierNEqTEqT=false;
                jsentiteInBody.tournoi["tournSelec"]=idT;
                console.log(JSON.stringify(jsentiteInBody,null,2));
                if($("#lanceTournoi").length)
                {
                    $("#lanceTournoi").remove();
                }

                 buttonLanceT = $("<button type =\"button\">Commmencer Tournoi! </button>").attr('id', 'lanceTournoi');
                $('body').append(buttonLanceT);
                buttonLanceT.click(function ()
                {

                    if(jsentiteInBody.tournoi['tournSelec']==null)
                     {
                        buttonLanceT.html("Selectionez un tournoi!");
                     }
                    else
                    {
                        if(tournoisExec["idTExecute"][idT]==null)
                        {
                            tournoisExec["idTExecute"] = idT;
                            if(tournoisExec.initTournElim(jsentiteInBody.tournoi.tournSelec, 4)>0)
                            {
                                this.remove();
                            }
                        }


                    }

                });
            }

        },
        "test" : function()
        {
            console.log("fonction dasn oafftournoi marche!!!")
        }
    }


};

jsensSynchro=
{
    "pool" :
    {
       "gesTournoi" :
        {
            "nbrPoolTotal" : 0 //a initialiser dans gestionTournoi
            /*
             nbrPoolAjout : n,


             idt5:
             {
                  "idP1":"A",

                  "idP2":"A"
              }*/
        },
        /*
        * Creer une pool avec son statu dans jssynchro
         */
        "creerPoolDansT" : function(idT)
        {
            if(jsensTournois[idT]==null)
            {
                alert("Erreur, creation d'une pool dans tournois non existant");
                return;
            }
            var ptourns=jsensSynchro.pool.gesTournoi;
            var idnouvP=ptourns.nbrPoolTotal;
            ptourns.nbrPoolTotal++;

            if(ptourns[idT]==null)
            {
                ptourns[idT]={};
            }
            ptourns[idT]["A"+idnouvP]={"equipes":{}};

            return "A"+idnouvP;



        },


        "ajoutEquipDansP" : function(idP,idEq,idT)
        {
            if(jsensSynchro.pool.gesTournoi[idT][idP]==null || jsensEquipes[idEq]==null)
            {
                console.log("equipe "+ idEq+ " non ajouté dans" + idP);

                return;
            }
            else
            {
                if(jsensSynchro.pool.gesTournoi[idT][idP]["equipes"][idEq]==null)
                    jsensSynchro.pool.gesTournoi[idT][idP]["equipes"][idEq]="A";

                else
                {
                    console.log("equipeee"+ idEq+" non ajouté dans " + idP);
                }
            }

        }
    },
    
    "match":
    {
        /*
          idM1 : [
                    eq1,
                    eq2,
                    
                 ]
         */
    },

    "equipe" :
    {
        "gesTournoi" :
        {
            /*
            idt5:
            {
               "eq1":"A",
                "eq2":"M"

            }*/

        },


        "ajEqDansT" : function(idEq,idT)
        {
            if(jsensSynchro.equipe.gesTournoi[idT]!=null)
            {
                if(jsensSynchro.equipe.gesTournoi[idT][idEq]=="A")
                {

                }
                if(jsensSynchro.equipe.gesTournoi[idT][idEq]=="E")
                {
                    delete jsensSynchro.equipe.gesTournoi[idT][idEq];
		            		            console.log("Modification du tounoi n°", idT, ", ajout de l'equipe n°", idEq);
                }

                else
                {
                    jsensSynchro.equipe.gesTournoi[idT][idEq]="A";
		            		            console.log("Modification du tounoi n°", idT, ", ajout de l'equipe n°", idEq);
                }
            }
            else
            {
                jsensSynchro.equipe.gesTournoi[idT]={} ;
                jsensSynchro.equipe.gesTournoi[idT][idEq]="A" ;
		        		        console.log("Modification du tounoi n°", idT, ", ajout de l'equipe n°", idEq);

            }

        },
        "enlEqDeT" : function(idEq,idT)
        {
            if(jsensSynchro.equipe.gesTournoi[idT]!=null)
            {
                if(jsensSynchro.equipe.gesTournoi[idT][idEq]=="E")
                {

                }
                if(jsensSynchro.equipe.gesTournoi[idT][idEq]=="A")
                {
                    delete jsensSynchro.equipe.gesTournoi[idT][idEq];
		            		            console.log("Modification du tounoi n°", idT, ", on enleve l'equipe n°", idEq);
                }

                else
                {
                    jsensSynchro.equipe.gesTournoi[idT][idEq]="E";
		            		            console.log("Modification du tounoi n°", idT, ", on enleve l'equipe n°", idEq);
                }
            }
            else
            {
                jsensSynchro.equipe.gesTournoi[idT]={} ;
                jsensSynchro.equipe.gesTournoi[idT][idEq]="E" ;
		        		        console.log("Modification du tounoi n°", idT, ", on enleve l'equipe n°", idEq);
            }
            console.log(JSON.stringify(jsensTournois[4]))
        }



    },

    "synchroServer" :function ()
    {   var jsEns={"jsEns":{}};


        jsEns["jsEns"]["jsensTournois"]=JSON.stringify(jsensTournois);
        jsEns["jsEns"]["jsensSynchro"]=JSON.stringify(jsensSynchro);
        jsEns["jsEns"]["ensPooldeidTcour"]=JSON.stringify(ensPooldeidTcour);
        $.post("synchro.php",jsEns,function(resul)
        {
            console.log("resul="+resul);
	    traitementResultatServer(resul);
        });
    }


};

var oaffContenuPool=
{
    "titres":
    {
        //"idP" : "idPool"
    },
    "typeEvens":
    {

    }
};

var oaffEquipes=
{
    "represente":jsensEquipes,
    "titres":
    {

        "nomEq":"Nom",
        "nivE":"Niveau"
    },

    "typeEvens":
    {


        "affGestionEquipes":function(idO)
        {

        },
        //SELECTION DE TOURNOI PARA L'UTILSATEUR ET AFFICHES EQUIPES
        /*
         * affiche les equipes disponible de l'utilisateur pour les rentrer
         * dans le tournoi.
         */

        "equipeDispoPT":  function(idTmask)
        {
            var divEqNonInT="diveqNonInidT"+idTmask;
            var divEqInT="diveqInidT"+idTmask;
            /*
             * SI LE DIV CONTENANT LES EQUIPES EST AFFICHÉ POUR LA
             * PREMIERE FOIS, ON VIENDRA CRÉE SE DIV PUIS ON
             * STOCKERA SON ID DANS jsentiteInBody
             */
            if(jsentiteInBody.equipe.eqNonInidT[divEqNonInT]==null)
            {
                jsentiteInBody.equipe.desafficheENDTs();
                jsentiteInBody.equipe.desafficheEDTs();
                jsentiteInBody.equipe.eqNonInidT[divEqNonInT]=true;
                jsentiteInBody.equipe.eqInidT[divEqInT]=true;
            }
            /*SINON ON VIENDRA SELECTIONÉ SE DIV POUR CHANGER SON
            * ATRIBUE DISPLAY AVEC TOGGLE PUIS ON STOCKERA SON ETAT
            * VISIBLE/NON VISIBLE DANS jsentiteInBody*/
            else
            {
                jsentiteInBody.equipe.desafficheENDTs();
                jsentiteInBody.equipe.desafficheEDTs();
                jsentiteInBody.equipe.changeAffENDT(divEqNonInT);
                jsentiteInBody.equipe.changeAffEDT(divEqInT);
            }


            //console.log(JSON.stringify(jsentiteInBody));
            //console.log(JSON.stringify(jsentiteInBody,null,2));
            /*
            * PUIS ON VIENS RAFRAICHIR LES INFO DANS LES DIV
            * DES EQUIPES AFFICHER SUR LE SITES
            * */
            $.each(jsensTournois,function (idT,contenuT)
            {
                if(contenuT!=null)
                {
                    if (idT!=idTmask)
                    {

                            //console.log("idT="+idT+ "idTmask="+idTmask );
                            afficheGenerique(contenuT.equipes,
                                "eqNonInidT"+idTmask,
                                oaffEquipes);
                    }
                    else
                    {
                        afficheGenerique(contenuT.equipes,
                            "eqInidT"+idTmask,
                            oaffEquipes);
                    }
                }

            });
            //afficheGenerique()




        },

        "lierNEqTEqT" : function(idEquipe,idEq)
        {
            //console.log("idEquipeAvant "+ idEquipe +" idEqAv="+idEq);
            if(/^eqInidT/.test(idEquipe))
            {
                /*
                *   ON ENLEVE UNE EQUIPE idEq D'UN TOURNOI EXTRAIT DE L'ID DU <tr> CONTENANT
                *   idEq
                */

                var regex=new RegExp(".*?idT(.+?)");
                var idTr=regex.exec(idEquipe);
                //console.log("redgexidT="+idTr[1]);
                //console.log("idEqDansLier "+"\n"+idEquipe+"idEquipe "+"\nidEq=" +idEq);

                eqInidTx=$("#"+idEquipe+"ID"+idEq);

                eqInidTx.attr("id","eqNonInidT"+idTr[1]+"ID"+idEq);
                eqInidTx.appendTo('#tbodyeqNonInidT'+idTr[1]);

                //console.log("  ");
                //console.log("  ");

                jsensTournois['-1'].equipes[idEq]=jsensTournois[idTr[1]].equipes[idEq];
                delete jsensTournois[idTr[1]].equipes[idEq];

                /*
                 * ON RAJOUTE DANS jsensSynchro L'INFO QUI INDIQUE QU'ON A
                 * ENLEVÉ UNE EQUIPE D'UN TOURNOI
                 */

                jsensSynchro.equipe.enlEqDeT(idEq,idTr[1]);
                console.log(JSON.stringify(jsensSynchro,null,2));


            }
            else
            {
                /*
                 *   ON AJOUTE UNE EQUIPE idEq DANS UN TOURNOI EXTRAIT DE id DU <tr> CONTENANT
                 *   idEq
                 */
                var regex=new RegExp(".*?idT(.+)");
                var idTr=regex.exec(idEquipe);
                console.log("redgexidT="+idTr[1] + " idEquipe="+idEquipe);
               // console.log("idEqDansNonLier "+"\n"+"idEquipe"+idEquipe +"\nidEq=" +idEq);

                eqInidTx=$("#"+idEquipe+"ID"+idEq);
                eqInidTx.attr("id","eqInidT"+idTr[1]+"ID"+idEq);
                eqInidTx.appendTo('#tbodyeqInidT'+idTr[1]);

                //console.log("  ");
                //console.log("  ");

                if(jsensTournois['-1'].equipes[idEq]!=null)
                {
                    delete jsensTournois['-1'].equipes[idEq];
                }
                if(jsensTournois[idTr[1]].equipes==null)
                    jsensTournois[idTr[1]].equipes={};
                jsensTournois[idTr[1]].equipes[idEq]=jsensEquipes[idEq];
		                jsensSynchro.equipe.ajEqDansT(idEq,idTr[1]);
		        console.log("jsensTournois de idT=" + idTr[1]);
                console.log(JSON.stringify(jsensTournois[idTr[1]]));
                console.log(JSON.stringify(jsensSynchro,null,2));
            }
        }
    }

};

tournoisExec=
{
    "idT" :
    {
    /*ptr sur un idT dans jsensTournois*/
    },
    "idTExecute" :
    {
        "idT" : {}
    },

    "initTournElim" : function(idT,nbrPool)
    {
      /* Tri des equipes par niveau*/
        if(jsensTournois[idT]!=null)
        {
        tournoisExec[idT] = jsensTournois[idT];

        }
        else
        {
        alert("Initialisation d'un tournoi qui n'existe plus :O");
        return;
        }

        var arrayidTriNiv=[];
        $.each(tournoisExec[idT].equipes,function(idEq,Equip)
        {
            var obj={};
             obj["idEq"]=idEq ;
             obj["nivE"]=Equip["nivE"];
            arrayidTriNiv.push(obj);

        });
        //console.log("arrayidtrivniv");
        //console.log(JSON.stringify(arrayidTriNiv,null,2));
        if(arrayidTriNiv.length < 4)
        {
            alert("Minimum 4 equipe pour demarer un tournoi, nbr Eq=" + arrayidTriNiv.length +" idT=" +idT);
            return -1;
        }

        arrayidTriNiv.sort(tournoisExec.sortNivE);
        //console.log("arrayidtrivnivTRIER");
        //console.log(JSON.stringify(arrayidTriNiv,null,2));

        /*Creation des Pools*/
        var idpoolArray=[];
        console.log(nbrPool);
        for(let i=0;i<nbrPool;i++)
        {
            //console.log("je boucle ");
            idpoolArray.push(jsensSynchro.pool.creerPoolDansT(idT));
        }

        /*Asignation de pool a chaque equipes dans idt*/
        var nbEq=0;
        $.each(jsensTournois[idT].equipes,function(idEq,Equip)
        {
            nbEq++;
        });
        d=0;
        verd=true;
        g=idpoolArray.length-1;
        let i=0;
        while (i<nbEq)
        {
            idEqcour=arrayidTriNiv[i].idEq;
            if(verd)
            {
                //console.log(d);
                //console.log(idpoolArray[d]);
                tournoisExec[idT].equipes[idEqcour].idP=idpoolArray[d];

                d++;
                i++;
                if(d>=idpoolArray.length && i<nbEq)
                {
                    verd=false;
                    d=0;
                }
            }
            else
            {
                //console.log(idpoolArray[g]);
                //console.log(g);
                tournoisExec[idT].equipes[idEqcour].idP=idpoolArray[g];

                g--;
                i++;
                if(g<0 && i<nbEq)
                {

                    verd=true;
                    g=idpoolArray.length-1;
                }
            }





        }
        //console.log(JSON.stringify(idpoolArray,null,2));
        //console.log(JSON.stringify(tournoisExec[idT].equipes,null,2));

        var equipesdeidT=tournoisExec[idT].equipes;
        if(ensPooldeidTcour==null)
            ensPooldeidTcour={};

        ensPooldeidTcour[idT]={};

        /*
        *   On construit les pools à partir de chaque equipe
         */

         $.each(equipesdeidT,function(idEq,equip)
        {
             if(equip!=null)
            {
                //console.log(idEq);
                //console.log(equip);

                var nouvEqui=jsensEquipes[idEq];

                if(ensPooldeidTcour[idT][equip.idP]==null)
                {
                    ensPooldeidTcour[idT][equip.idP]={};
                }

                ensPooldeidTcour[idT][equip.idP][idEq]=nouvEqui;
                jsensSynchro.pool.ajoutEquipDansP(equip.idP,idEq,idT);
                //console.log("ensPooldeidTcour");
                //console.log(JSON.stringify(ensPooldeidTcour[idT],null,2));


            }
        });
        //console.log("ensPooldeidTcour");
        //console.log(JSON.stringify(ensPooldeidTcour[idT],null,2));
        gereAffiGen.typeAff.affTC.affEqs=false;
        gereAffiGen.typeAff.affTC.affTs=false;
        gereAffiGen.typeAff.affTC.lierNEqTEqT=false;
        gereAffiGen.typeAff.affPC.affPs=true;
        afficheGenerique(ensPooldeidTcour[idT],"ensPooldeidTExec",oaffContenuPool);
        gereAffiGen.typeAff.affPC.affPs=false;


        console.log("ensPooldeidT");
        console.log(JSON.stringify(ensPooldeidTcour,null,2));
        console.log("jssynchro");
        console.log(JSON.stringify(jsensSynchro,null,2));

        if(!$("#genereMatch").length)
        {
            buttonGenereMatchsT=$("<button type =\"button\">Generer Match! </button>").attr('id','genereMatch');
            $('body').append(buttonGenereMatchsT);
            buttonGenereMatchsT.click(function()
            {
                console.log(buttonGenereMatchsT.text());



                tournoisExec.genereMatchsT(idT);

            });
            //jsentiteInBody.tournoi.tournSelec=4;




        }
        return 1;
    },

    "genereMatchsT" : function (idT)
    {

        console.log("dans genereMatchsT");
        console.log(JSON.stringify(jsensSynchro.pool,null,2));
        $.each(ensPooldeidTcour[idT],function(idP,eqsInIdP)
        {

            tournoisExec.genereMatchsDP(idP,idT);

        });
    },


     "genereMatchsDP":function (idP,idT) //genere des matchs pour la phase preliminaire;
     {
         console.log("Dans genereMatchsDP");
         var arrayidEq=[]; //array qui contiendra les idEq dans idP pour le tournoi idT
         $.each(ensPooldeidTcour[idT][idP],function(idEq,equip)
         {
            arrayidEq.push(idEq);
         });

         if(jsensTournois[idT]["matchs"]==null)
             jsensTournois[idT]["matchs"]={};

         for(var i=0;i<arrayidEq.length;i++)
         {
             for(var j=0;j<arrayidEq.length;j++)
             {
                 var nmatch={};

                
             }
         }
         console.log("arrayidEq");
         console.log(arrayidEq);


     },


      "sortNivE" : function(a,b)
      {
          return (parseInt(a.nivE)<parseInt(b.nivE))? -1 : ((parseInt(a.nivE)>parseInt(b.nivE))? 1 : 0 );
      }


};






