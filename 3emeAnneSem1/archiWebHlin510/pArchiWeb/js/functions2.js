/*
	Lorsque le client ajoute ou enleve des equipes dans le tournoi,
	ces changments sont stoqués dans un objet javascript jsensSynchro.equipe.gesTournoi.

	Lorsque l'tilisateur recharge la page, navigue vers une autre page ou referme 
	le navigateur, l'objet jsensSynchro.equipe.gesTournoi est reinitialisé,
	on perd les informations sur les changements.

	Pour eviter cette perte, on stoque jsensSynchro.equipe.gesTournoi dans localStorage
	et on appelle la fonction ci-dessous après la réinitialisation, cette fonction va affecter 
	les données enregistrées dans localStorage au nouvel objet gesTournoi contenu dans
	jsensSynchro.equipe.
*/
//$("body").append("<div id='affichageLocalStorage'></div>");

//$("#affichageLocalStorage").on("change", function(){
//	var str = "LocalStorage : ";
//	var jsensTour = JSON.parse(localStorage.getItem("jsensTournois"));
//	var jsensEqu = JSON.parse(localStorage.getItem("jsensEquipes"));
//	var jsensSync = JSON.parse(localStorage.getItem("jsensSynchro"));
//	var str1 = JSON.stringify(jsensTour);
//	var str2 = JSON.stringify(jsensEqu);
//	var str3 = JSON.stringify(jsensSync);
//	$("#affichageLocalStorage").append("<table><tr><th>ensTournois</th><th>ensEquipes</th><th>ensSynchro</th></tr><tr><td>"+str1+"</td><td>"+str2+"</td><td>"+str3+"</td></tr></table>");	
//});

//$("#affichageLocalStorage").trigger("change");

var garderInfoChangementEquipeParTournoi = function(){
	var gesTournoi = null;
	if(localStorage.getItem("jsensSynchro.equipe.gesTournoi")!==null){
        	gesTournoi = JSON.parse(localStorage.getItem("jsensSynchro.equipe.gesTournoi"));
	}
	else{
        	gesTournoi = {};
	}
	jsensSynchro.equipe["gesTournoi"] = gesTournoi;
	//console.log("Action garder info gesTournoi");
	//console.log("Equipes par tournoi : ", localStorage.getItem("jsensSynchro.equipe.gesTournoi"));
};
//garderInfoChangementEquipeParTournoi();

var garderInfoChangementJsensTournois = function(){
	if(localStorage.getItem("jsensTournois")!==null){
		jsensTournois = JSON.parse(localStorage.getItem("jsensTournois"));
	}
	//console.log("Action garder info jsensTournois");
	//console.log("Equipes tournois n°4 : ", JSON.stringify(jsensTournois["4"]["equipes"]));
};
//garderInfoChangementJsensTournois();


var buttonSynchro=$("<button>Sync localStorage</button>").attr({'id':'synclocal','class':'sync'}).on("click", function(){
	//On enregistre tout dans localStorage

    localStorage.setItem("jsensTournois",JSONfn.stringify(jsensTournois));
    localStorage.setItem("jsensEquipes",JSONfn.stringify(jsensEquipes));
    localStorage.setItem("jsensSynchro",JSONfn.stringify(jsensSynchro));
    localStorage.setItem("jsentiteInBody",JSONfn.stringify(jsentiteInBody));
    localStorage.setItem("tournoisExec",JSONfn.stringify(tournoisExec));
    localStorage.setItem("ensPooldeidTcour",JSONfn.stringify(ensPooldeidTcour));
    localStorage.setItem("gereAffiGen",JSONfn.stringify(gereAffiGen));
    localStorage.setItem("oaffTournoi",JSONfn.stringify(oaffTournoi));
    localStorage.setItem("oaffContenuPool",JSONfn.stringify(oaffContenuPool));
    localStorage.setItem("oaffEquipes",JSONfn.stringify(oaffEquipes));

	$("#affichageLocalStorage").trigger("change");

	/*
    var oafftesttournoi=JSONfn.parse(JSONfn.stringify(oaffTournoi));
    console.log("oafftesttournoi");
    console.log(oafftesttournoi);

    oafftesttournoi.typeEvens.test();
	*/
    /*
    localStorage.setItem("jsensTournois", JSON.stringify(jsensTournois));
	localStorage.setItem("jsensEquipes", JSON.stringify(jsensEquipes));
	localStorage.setItem("jsensSynchro", JSON.stringify(jsensSynchro));
	localStorage.setItem("jsentiteInBody", JSON.stringify(jsentiteInBody));
	localStorage.setItem("tournoisExec", JSON.stringify(tournoisExec));
	localStorage.setItem("ensPooldeidTcour", JSON.stringify(ensPooldeidTcour));
    localStorage.setItem("gereAffiGen", JSON.stringify(gereAffiGen));
    localStorage.setItem("oaffTournoi", JSON.stringify(oaffTournoi));
    localStorage.setItem("oaffContenuPool", JSON.stringify(oaffContenuPool));
    localStorage.setItem("oaffEquipes", JSON.stringify(oaffEquipes));
    */

    console.log("je syncro avec local storage");
	//On efface tous les element du dom

	//$("body").empty();


	//On appelle la fonction afficheGen()
	//afficheGenerique(jsensTournois, "tournoi", "")
});

$('body').append(buttonSynchro);


var buttonEfface=$("<button>Efface localStorage</button>").attr({'id':'efflocal','class':'sync'}).on("click", function(){
    localStorage.removeItem("jsensTournois");
    localStorage.removeItem("jsensEquipes");
    localStorage.removeItem("jsensSynchro");
    localStorage.removeItem("jsentiteInBody");
    localStorage.removeItem("tournoisExec");
    localStorage.removeItem("ensPooldeidTcour");
    localStorage.removeItem("gereAffiGen");
    localStorage.removeItem("oaffTournoi");
    localStorage.removeItem("oaffContenuPool");
    localStorage.removeItem("oaffEquipes");

	$("#affichageLocalStorage").trigger("change");

    console.log("local storage 100% effacé");
});

$('body').append(buttonEfface);

function traitementResultatServer(result){
	console.log("Hey je m'execute ou pas ?");
	if(result==null){
		console.log("Pas de résultat de la part du server");
	}
	else {
		console.log("Un resultat de la part du server :", result);
	}
	jsensTournois = JSON.parse(result);                                                localStorage.setItem("jsensTournois", JSONfn.stringify(jsensTournois));            
	$.each(jsensSynchro["equipe"]["gesTournoi"], function(cle, val){
    		jsensSynchro["equipe"]["gesTournoi"][val] = {};
        });        
     	localStorage.setItem("jsensSynchro", JSONfn.stringify(jsensSynchro)); 
       	console.log("gesTournoi", jsensSynchro["equipe"]["gesTournoi"]);
}
