<?php
    include ('head.php');
?>

<?php

	include_once "functions.php";
	include_once "verifInput.php";
	$nomJ = $prenomJ =$numJ=$idEq="";
	$nomJErr = $prenomJErr=$numJErr=$idEqErr="";

	$erreur=false;

	if(isset($_POST['cJoueur']) and !empty($_SESSION['idU']))

		/*
		*	VERIFICATION NOM JOUEUR 

		*/
	{
		if (empty($_POST['nomJ'])) 
		{
			$nomJErr = "Veuillez saisir le nom du joueur.";
			$erreur=true;
		} 

		else
		{
		    $nomJ = ucfirst(strtolower(test_input($_POST["nomJ"])));
		    if (!preg_match("/^[a-zA-Z ]*$/",$nomJ) or (30<mb_strlen($nomJ, 'UTF-8'))) 
		    {
 				 $nomJErr = "Maximum 30 caractères avec uniquement lettres et espaces autorisés.";
 				 $erreur=true;
			}
		}
		


		/*
		*	VERIFICATION PRENOM JOUEUR 
		*/

		if (empty($_POST["prenomJ"])) 
		{
		    $prenomJErr = "Veuillez saisir le prénom du joueur";
		    $erreur=true;
		} 
		else 
		{
		    $prenomJ = ucfirst(strtolower(test_input($_POST["prenomJ"])));
		    if (!preg_match("/^[a-zA-Z ]*$/",$prenomJ) or (30<mb_strlen($prenomJ, 'UTF-8'))) 
		    {
 				 $prenomJErr = "Maximum 30 caractères avec uniquement lettres et espaces autorisés.";
 				 $erreur=true;
			}
		}

		/*
		*VERIFICATION NUMERO JOUEUR
		*/

		if (empty($_POST["numJ"])) 
		{
		    $numJErr = "Veuillez saisir un numéro de joueur compris entre 0 et 99.";
		    $erreur=true;
		} 

		else 
		{
		    $numJ = test_input($_POST["numJ"]);
		    if (($numJ>99) or ($numJ<0)) 
		    {
 				 $numJErr = "Uniquement accepté un numéro compris entre 0 et 99.";
 				 $erreur=true;
			}
		}

		/*
		*	$erreur? print("erreur</br>") : print("pas d'erreur</br>");
		*	VERIFICATION EQUIPE JOUEUR 
		*/

		
		if (empty($_POST["idEq"])) 
		{
		    $idEqErr = "Veuillez saisir le nom de l’équipe du nouveau joueur";
		    $erreur=true;
		} 
		else 
		{
		    $idEq = ucfirst(strtolower(test_input($_POST["idEq"])));
		    if (!preg_match("/^[0-9]*$/",$idEq) or (20<mb_strlen($idEq, 'UTF-8'))) 
		    {
 				 $idEqErr = "Maximum 20 caractères avec uniquement chiffres autorisés.";
 				 $erreur=true;
			}
		}

		if($erreur==false)
		{
			try
			{
				$bdd = new PDO ("mysql:host=localhost;dbname=VoleyBallArchiWeb","fac","livefor7livefor7123");
				$bdd->exec("SET CHARACTER SET utf8");
			}
			catch(Exception $e)
			{
				error_log("Erreur>> !" . $e->getMessage() . "<br/>\n");
				exit(1);
			}


			 

			$resultcreerJ=creerJoueur($bdd,$prenomJ,$nomJ,
										$numJ,$idEq,$numJErr,$idEqErr);
			echo "résultat de la création joueur valeur=" . $resultcreerJ."</br>";
		}
		else
		{
			echo "<div class=erreur> Erreur veuillez ressayer ultérieurement <div>";
		}
	}
?>


<form method="post" action ="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<table>
		<label for='Prenom du Joueur'></label>
			<tr>
				<td>Saisir prénom du joueur:</td>
				<td><input type ='text' value='<?php isset($_POST['prenomJ'])? print ($_POST['prenomJ']) : print("prenomJTest")?>' name='prenomJ' id='prenomJ' pattern='[a-zA-Z_]{2,40}' required>
					<span class="error">* <?php echo $prenomJErr;?></span>
				</td>
			</tr>
		<label for='Nom du Joueur'></label>
			<tr>
				<td>Saisir nom du joueur :</td>
				<td><input type ='text' value='<?php isset($_POST['nomJ'])? print ($_POST['nomJ']) : print("nomJTest")?>' name='nomJ' id='nomJ' pattern='[a-zA-Z_]{2,40}' required>
					<span class="error">* <?php echo $nomJErr;?></span>
				</td>
			</tr>
		
		<label for='ID Equipe du Joueur'></label>
			<tr>
				<td>Saisir id de l'équipe du joueur:</td>
				<td><input type ='text' value='<?php isset($_POST['idEq'])? print ($_POST['idEq']) : print("Test")?>' name='idEq' id='idEq' pattern='[0-9]{1,20}' required>
					<span class="error">* <?php echo $idEqErr;?></span>
				</td>
			</tr>
		<label for='Numero Chemise du Joueur'></label>
		<tr>
			<td>Numero ch :</td>
			<td><input type='number' value='<?php isset($_POST['numJ'])? print ($_POST['numJ']) : print("20")?>' name='numJ' id='numJ' pattern='[0-9]{2}' required>
				<span class="error">* <?php echo $numJErr;?></span>
			</td>
			
		</tr>
	</table>
	<input type="submit" name="cJoueur" value="Creer joueur">
			
</form>


<?php include ('footer.php');?>