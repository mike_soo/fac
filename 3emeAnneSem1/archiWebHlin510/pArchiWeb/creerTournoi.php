<?php include ('head.php');?>
<?php include_once('functions.php')?>
<?php include_once('verifInput.php')?>


    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $( "#datepicker" ).datepicker({
                altField: "#datepicker",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>
<?php

    $dateDeroulErr = $nomTErr = $dureeJourErr = "";
    $dateDeroul = $nomT = $dureeJour = "";
    if(!empty($_POST["cTournoi"]))
    {

        $erreur = false;
        $nomT = $_POST['nomT'];
        $dateDeroul = $_POST['dateDeroul'];
        $dureeJour = $_POST['dureeJour'];


        verifNom($nomT, $nomTErr, 30, $erreur);
        verifDate($dateDeroul, $dateDeroulErr, $erreur);
        verifEntier($dureeJour, 1, 365, $dureeJourErr, $erreur);

        echo "Bien verifier:</br>\n";
        echo "dateDeroul=$dateDeroul</br>\n";
        echo "dureeJour=$dureeJour</br>\n";
        echo "nomT=$nomT</br>\n";
        $erreur?print('true'):print('false')."</br>\n";
        if($erreur===false)
        {   $idT;
            $bdd;
            try
            {
                $bdd = new PDO('mysql:host=localhost;dbname=VoleyBallArchiWeb;charset=utf8','fac', "livefor7livefor7123");
            }
            catch(Exception $e)
            {
                error_log($e->getMessage());
            }

            $idT=creerTournoi($bdd,$nomT,$dateDeroul,$dureeJour,$_SESSION['idU']);
            echo "idT= $idT </br>\n";
        }
    }

    /*
     * POUR AFFICHER TOUTES LES POULES AVEC LES EQUIPES D'UN TOURNOI idT DONNÉE
     *
     */

?>

<script >





</script>

<table>
    <form method='post' action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>' >

        <label for='Date du Tournoid'></label>
        <tr>

            <td>Date de deroulement du tournoi:</td>
            <td> <input type="text" id="datepicker" value="<?php isset($_POST['dateDeroul'])? print ($_POST['dateDeroul']) : print('')?>" name='dateDeroul'  required>
                <span class=\"error\">* <?php echo $dateDeroulErr;?></span>
            </td>
        </tr>

        <label for='Nom du Tounoi'></label>
        <tr>
            <td>Saisir nom du tournoi :</td>
            <td><input type ='text' value='<?php isset($_POST['nomT'])? print ($_POST['nomT']) : print("nomJTest")?>' name='nomT' id='nomT' pattern='[a-zA-Z_]{2,40}' required>
                <span class="error">* <?php echo $nomTErr;?></span>
            </td>
        </tr>

        <label for='Durée en jour du tournoi'></label>
        <tr>
            <td>Durée en jour du tournoi:</td>
            <td><input type ='text' value='<?php isset($_POST['dureeJour'])? print ($_POST['dureeJour']) : print("3")?>' name='dureeJour' id='dureeJour' pattern='[0-9]{1,3}' required>
                <span class="error">* <?php echo $dureeJourErr;?></span>
            </td>
        </tr>

        <tr>
            <td>
                <input type="submit" name="cTournoi" value="Créer tournoi">
            </td>
        </tr>
    </form>
</table>


<?php include ('footer.php');?>