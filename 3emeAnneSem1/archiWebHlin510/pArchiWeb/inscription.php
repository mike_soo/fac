<?php include ('head.php');?>


	<?php
		include_once "verifInput.php";
		include_once "functions.php";
		$email = $motdePasse = $nom = $prenom =$verifdePasse="";
		$emailErr = $motdePasseErr =  $nomErr = $prenomErr =$verifdePasseErr="";
		$erreur=false;

		if(isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['email']) and isset($_POST['motdePasse']))
		{

			if ($_SERVER["REQUEST_METHOD"] == "POST") 
			{
				if (empty($_POST["nom"])) 
				{
					$nomErr = "Veuillez saisir un nom.";
					$erreur=true;
				} 

				else 
				{
				    $nom = ucfirst(strtolower(test_input($_POST["nom"])));
				    if (!preg_match("/^[a-zA-Z ]*$/",$nom) or (30<mb_strlen($nom, 'UTF-8'))) 
				    {
	     				 $nomErr = "Maximum 30 caractères avec uniquement lettres et espaces autorisés.";
	     				 $erreur=true;
	    			}
				}


				if (empty($_POST["prenom"])) 
				{
				    $prenomErr = "Veuillez saisir un prénom";
				    $erreur=true;
				} 
				else 
				{
				    $prenom = ucfirst(strtolower(test_input($_POST["prenom"])));
				    if (!preg_match("/^[a-zA-Z ]*$/",$prenom) or (30<mb_strlen($prenom, 'UTF-8'))) 
				    {
	     				 $prenomErr = "Maximum 30 caractères avec uniquement lettres et espaces autorisés.";
	     				 $erreur=true;
	    			}
				}


	    		
				if (empty($_POST["motdePasse"])) 
				{
				    $motdePasseErr = "Veuillez saisir un mot de passe.";
				    $erreur=true;


				} 
				
				if(empty($_POST["verifdePasse"]))
				{
				   	$verifdePasseErr="Veuillez vérifier votre mot de passe.";
				   	$erreur=true;
				}

				else 
				{
				    $motdePasse = test_input($_POST["motdePasse"]);
				    $verifdePasse=test_input($_POST["verifdePasse"]);
				    if (!preg_match("/[a-zA-z0-9]*[-|_]*/",$motdePasse) or (30<mb_strlen($motdePasse, 'UTF-8')) ) 
				    {
	     				 $motdePasseErr = "Uniquement 30 caractères d'où lettres, numéros et traits d'unions autorisés.";
	     				 $erreur=true;

	    			}

	    			if($motdePasse!==$verifdePasse)
	    			{
	    				$erreur=true;
	    				$verifdePasseErr="Vos mots de passe ne sont pas identique, veuillez vérifier votre saisie";
	    			}	
				}


				if (empty($_POST["email"])) 
				{
				    $emailErr = "Adresse de courriel manquante.";
				    $erreur=true;
				} 
				else 
				{
				    $email = test_input($_POST["email"]);
				    
				    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
				    {
	      				$emailErr = "Format du courrier saisie invalide.";
	      				$erreur=true;
	    			}
				}
				
				if($erreur==false)
				{
					try
					{
						$bdd = new PDO ("mysql:host=localhost;dbname=VoleyBallArchiWeb","fac","livefor7livefor7123");
						$bdd->exec("SET CHARACTER SET utf8");
					}
					catch(Exception $e)
					{
						error_log("Erreur>> !" . $e->getMessage() . "<br/>\n");
						exit(1);
					}



					
					if(utilisateurExiste($bdd,$email)>=0)
					{
						$emailErr="Le compte saisie est déjà utiliser";
					}
					
					else
					{
						//error_log( "j'essaye d'inserter\n</br>");
						
						$nid=creerUtilisateur($bdd,$prenom,$nom,$email,$motdePasse); //contiendra le dernier id inserer dans la table personne
						
						if($nid>=0)
						{

							$_SESSION['idU']=$nid;
							$_SESSION['nomU']=$nom;
							$_SESSION['prenomU']=$prenom;
							header('Location:accueilU.php');
						}
						else
						{
							echo "<div class=erreur>Erreur lors de la creation de votre
							 		compte, veuillez ressayer ulterieurement </div>";
							error_log("Erreur leur de création de compte: \n
										nid=".$nid.
										" email=".$email);	
						}
					}
							
					
				}
                else
				    echo "et non!</br>";
				
			}
		}
		else if(isset($_POST['aCompte']))
		{	
			
			header('Location: connection.php');
		}
	?>
	
	
		
		
	<form method="post" action ="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<table>
            <label for='prenom'></label>
				<tr>
					<td>Saisir prénom:</td>
					<td><input type ='text' value='<?php isset($_POST['prenom'])? print ($_POST['prenom']) : ""?>' name='prenom' id='prenom' pattern='[a-zA-Z_- \u00C0-\u017F]{2,40}' required>
						<span class="error">* <?php echo $prenomErr;?></span>
					</td>
				</tr>
                <label for='nom'></label>
				<tr>
					<td>Saisir nom:</td>
					<td><input type ='text' value='<?php isset($_POST['nom'])? print ($_POST['nom']) : ""?>' name='nom' id='nom' pattern='[a-zA-Z_- \u00C0-\u017F]{2,40}' required>
						<span class="error">* <?php echo $prenomErr;?></span>
					</td>
				</tr>

			<label for='Mot de passe'>
				<tr>
					<td>Saisir mot de passe :</td></label>
					<td><input type='password' value='<?php isset($_POST['motdePasse'])? print ($_POST['motdePasse']) : ""?>' name='motdePasse' id='motdePasse' pattern='[a-zA-Z_0-9]{2,50}' required>
						<span class="error">* <?php echo $motdePasseErr;?></span>
					</td>
					
				</tr>
			<label for='Verification mot de passe'>
				<tr>
					<td>Verifier mot de passe :</td></label>
					<td><input type='password' value='<?php isset($_POST['verifdePasse'])? print ($_POST['verifdePasse']) : ""?>' name='verifdePasse' id='verifdePasse' pattern='[a-zA-Z_0-9]{2,50}' required>
						<span class="error">* <?php echo $verifdePasseErr;?></span>
					</td>
					
				</tr>
			<label for='Email'>
				<tr>
					<td>Saisir votre courrier électronique :</td></label>
					<td><input type='email' value='<?php isset($_POST['email'])? print ($_POST['email']) : ""?>' name='email' id='email' required>
						<span class="error">* <?php echo $emailErr;?></span>
					</td></br>
				</tr>
		</table>

		<input type="submit" name="cCompte" value="Creer compte">
		
	</form>
	
	<form method="post" action= <?php htmlspecialchars("connection.php")?> >
		<input type="submit" name="aCompte" value="J'ai deja un compte"> <!--aCompte pour authentification compte.-->
	</form>
<?php include('footer.php');?>
