<?php 
	function test_input($data)
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

	function filtre_input(&$data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);

    }

function verifNom(&$nom,&$nomErr,$nbc,&$erreur)
    {
        $nom = ucfirst(strtolower(test_input($nom)));
        if ($nom==="")
        {
            $nomErr = "Veuillez saisir le nom du joueur.";
            $erreur=true;
        }

        else
        {

            if (!preg_match("/^[a-zA-Z ]*$/",$nom) or ($nbc<mb_strlen($nom, 'UTF-8')))
            {
                $nomErr = "Maximum 30 caractères avec uniquement lettres et espaces autorisés.";
                $erreur=true;
            }
        }
    }
function verifDate(&$date,&$dateErr,&$erreur)
{
    $date = ucfirst(strtolower(test_input($date)));
    if ($date==="")
    {
        $dateErr = "Veuillez saisir une date.";
        $erreur=true;
    }

    else
    {

        if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-(20[1-9]{2})/",$date) )
        {
            $dateErr = "Saisir un format de date valid jj-mm-yyyy";
            $erreur=true;
        }

    }
}

function verifEntier(&$n,$min,$max,&$nErr,&$erreur)
{
    $n=test_input($n);
    if($n==="")
    {
        $nErr="Veuillez saisir un entier";
    }
    else
    {
        if (($n>$max) or ($n<$min))
        {
            $numJErr = "Uniquement accepté un numéro compris entre $min et $max.";
            $erreur=true;
        }
    }
}
?>


