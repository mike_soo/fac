//Inspiration Background Sync : https://developers.google.com/web/updates/2015/12/background-sync
//Comment envoyer des données json au server ?
//https://developers.google.com/web/updates/2015/12/background-sync

this.addEventListener('install', function(event){
	event.waitUntil(
		caches.open('vvv3').then(function(cache) {
			return cache.addAll([
				'/pArchiWeb/',
                        	'/pArchiWeb/gestionTournoi.php',
                        	'/pArchiWeb/js/animationMenu.js',
                        	'/pArchiWeb/js/functions2.js',
                        	'/pArchiWeb/js/functions.js',
                        	'/pArchiWeb/sw~j.js',
				'/pArchiWeb/test~j.php'
			]);
		})
	);
});

this.addEventListener('activate', function(event){
        var cacheWhiteList = ['vvv3'];

        event.waitUntil(
                caches.keys().then(function(keyList){
                        return Promise.all(keyList.map(function(key){
                                if (cacheWhiteList.indexOf(key)==-1){
                                        return caches.delete(keyList[key]);
                                }
                        }));
                })
        );
});

this.addEventListener('fetch', function(event) {
        event.respondWith(
                fetch(event.request).then(function(response){
                        console.log('fetch ok');
                        return caches.match(event.request).then(function(){
                                return caches.open('vvv3').then(function(cache){
                                        cache.put(event.request, response.clone());
                                        return response;
                                }
                                ).catch(function(err){
                                        console.log('Erreur ouverture cache', err);
                                        return response;
                                });
                        }).catch(function(){
                                console.log('pas dans le cache');
                                return response;
                        });
               }).catch(function(){
                        console.log('fetch non ok');
                        return caches.match(event.request).then(function(response){
                                console.log(event.request.url);
                                if(typeof(response) != "undefined"){
                                        console.log('cache ok');
                                        return response;
                                }
                                else{
                                        console.log('cache non ok');
                                        return new Response('Erreur network');
                                }
                        });
                })
        )
});
