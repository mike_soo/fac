<?php
	function securiserInputDansFunctionsPHP($data){
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
        }
        function utilisateurExiste(&$bdd,&$emailU)
        {
                $stmtVE = $bdd->prepare("   SELECT utilisateur.idP
                                                                        FROM utilisateur
                                    WHERE utilisateur.email = :emailU" );//vérification existence
                //echo "testing1\n";
                $stmtVE->bindParam(':emailU',$emailU);
                if($stmtVE->execute())
                {

                        if(null!=($util=($stmtVE->fetch())))
                        {
                error_log("j'existe donc je suis");
                                return $util['idP'];
                        }
                        else
                        {
                                return -1;
                        }
                }
                /*
                *       En cas d'erreur de requete/consultation on renvois -2.
                */
                else
                {
                        error_log("Erreur dans utilisateurExiste() avec mail " . $emailU);
                        return -2;
                }
        }
	/*
        * VERIFICATION DE L'EXISTANCE DE LEQUIPE
        * renvois true si celle ci existe.
        */
        function equipeExiste(&$bdd,&$idEq)
        {
                $idEsecure = securiserInputDansFunctionsPHP($idEq);
                $stmtVEE=$bdd->prepare("SELECT nomEq
                                                                FROM equipe
                                                                WHERE :idEquipe = idEq 
                                                        ");

                $stmtVEE->bindParam(':idEquipe',$idEsecure);

                if($stmtVEE->execute())
                {
                        if(null!= ($equip=$stmtVEE->fetch()))
                        {
                                return 1;
                        }
                        else
                        {
                                /*équipe n'existe pas*/
                                return -1;
                        }
                }
                /*
                *       En cas d'erreur de requete/consultation .
                */
                else
                {
                        error_log("Erreur dans equipeExiste() functions.php");
                        return -2;
                }
        }
	function tournoiExiste(&$bdd, &$idT){
                $idTsecure = securiserInputDansFunctionsPHP($idT);
                $stmtVTE = $bdd->prepare("
                        SELECT nomT
                        FROM tournoi
                        WHERE idT=:idT          
                ");

                $stmtVTE->bindParam(':idT', $idTsecure);

                if($stmtVTE->execute()){
                        if(null!= ($equip=$stmtVTE->fetch()))
                        {
                                return 1;
                        }
                        else
                        {
                                /*le tournoi n'existe pas*/
                                //error_log("Le tournoi ".$idTsecure." n'existe pas");
                                return -1;
                        }
                }
                else{
                        error_log("Erreur dans tournoiExiste() functions.php");
                        return -2;
                }
        }

	function equipeDansTournoiExiste(&$bdd, &$idE, &$idT){
                $idEsecure = securiserInputDansFunctionsPHP($idE);
                $idTsecure = securiserInputDansFunctionsPHP($idT);
                $stmtEdTe = $bdd->prepare("
                        SELECT *
                        FROM TetEqdonneP
                        WHERE idT = :idT
                        AND idEq = :idE
                ");

                $stmtEdTe->bindParam(':idT',$idTsecure);
                $stmtEdTe->bindParam(':idE',$idEsecure);

                if($stmtEdTe->execute()){
                        if(null!=($util=($stmtEdTe->fetch())))
                        {
                                //error_log("Functions.php/equipeDansTournoiExiste():L'equipe ".$idEsecure." existe bien dans le tournoi ".$idTsecure);
                                return 1;
                        }
                        else{
                                //error_log("Functions.php/equipeDansTournoiExiste():L'equipe ".$idEsecure." n'existe pas dans le tournoi ".$idTsecure);
                                return -1;
                        }
                }
                else {
                        //error_log("Functions.php/equipeDansTournoiExiste():Erreur dans equipeDansTournoiExiste avec idT ".$idTsecure." et idE ".$idEsecure);
                        return -2;
                }
        }

function insererEquipeDansTournoi(&$bdd, &$idE, &$idT){
                $ok = equipeDansTournoiExiste($bdd, $idE, $idT);
                $idEsecure = securiserInputDansFunctionsPHP($idE);
                $idTsecure = securiserInputDansFunctionsPHP($idT);
		//error_log("Functions.php/insererEquipeDansTournoi(): etat de ok : ".$ok);
                switch($ok){
                        case -1:
                                //on verifie que l'equipe existe

                                $ok2 = equipeExiste($bdd, $idEsecure);

                                switch($ok2){
                                        case 1:
                                                //On verifie que le tournoi existe
                                                $ok3 = tournoiExiste($bdd, $idTsecure);
                                                switch($ok3){
                                                        case 1:
                                                                $stmtInsererEdT=$bdd->prepare("
                                                                        INSERT INTO TetEqdonneP(idT, idEq, idP, numPool) 
                                                                        VALUES (:idT, :idE, -1, -1)
                                                                ");
                                                                $stmtInsererEdT->bindParam(':idT', $idTsecure);
                                                                $stmtInsererEdT->bindParam(':idE', $idEsecure);

                                                                if($stmtInsererEdT->execute()){
                                                                        error_log("Functions.php/insererEquipeDansTournoi():L'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." s'est bien passée");
                                                                        return 1;
                                                                }
                                                                else {
                                                                        error_log("Functions.php/insererEquipeDansTournoi():L'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." est possible mais ne s'est pas bien passé lors de l'insertion.");
                                                                        return -2;
                                                                }
                                                                break;
                                                        case -1:
                                                                error_log("Functions.php/insererEquipeDansTournoi():L'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." n'est pas possible car le tournoi n'existe pas.");
                                                                return -1;
                                                                break;
                                                }
                                                break;
                                        case -1:
                                                error_log("Functions.php/insererEquipeDansTournoi():'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." n'est pas possible car l'equipe n'existe pas.");
                                                return -1;
                                                break;
                                }

                                break;
                        case 1:
                                error_log("Functions.php/insererEquipeDansTournoi():L'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." n'est pas possible car l'equipe fait dejà partie du tournoi.");
                                return -1;
                                break;
                        case -2:
                                error_log("Functions.php/insererEquipeDansTournoi():L'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." est peut-être possible car l'appel à la fonction equipeDansTournoiExiste s'est mal passé");
                                return -2;
                                break;
                }
        }

	function enleverEquipeDansTournoi(&$bdd, &$idE, &$idT){
                $ok = equipeDansTournoiExiste($bdd, $idE, $idT);

                $idEsecure = securiserInputDansFunctionsPHP($idE);
                $idTsecure = securiserInputDansFunctionsPHP($idT);

		//error_log("Functions.php/enleverEquipeDansTournoi(): etat de ok : ".$ok);
                switch($ok){
                        case 1:
                                $stmtInsererEdT=$bdd->prepare("
                                        DELETE FROM TetEqdonneP
                                        WHERE idT=:idT AND idEq=:idE
                                ");
                                $stmtInsererEdT->bindParam(':idT', $idTsecure);
                                $stmtInsererEdT->bindParam(':idE', $idEsecure);

                                if($stmtInsererEdT->execute()){
                                        error_log("Functions.php/enleverEquipeDansTournoi():La suppression de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." s'est bien passée");
                                        return 1;
                                }
                                else {
                                        error_log("Functions.php/enleverEquipeDansTournoi():L'insertion de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." est possible mais ne s'est pas bien passé lors de l'insertion.");
                                        return -2;
                                }
                                break;
                        case -1:
                                error_log("Functions.php/enleverEquipeDansTournoi():La suppression  de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." n'est pas possible car l'equipe ne fait pas partie du tournoi.");
                                return -1;
                                break;
                        case -2:
                                error_log("Functions.php/enleverEquipeDansTournoi():La suppression de l'équipe ".$idEsecure." dans le tournoi ".$idTsecure." est peut-être possible car l'appel à la fonction equipeDansTournoiExiste s'est mal passé");
                                return -2;
                                break;
                }
        }

	function creerPersonne(&$bdd,&$prenom,&$nom)
	{
		$stmtInsererNP=$bdd->prepare("INSERT INTO personne(idP,prenomP,nomP)
									VALUES (NULL,?,?)");
		//$stmtInsererNP->bindParam(1,'');
		$stmtInsererNP->bindParam(1,$prenom);
		$stmtInsererNP->bindParam(2,$nom);
		if($stmtInsererNP->execute())
		{	
			return $nid=$bdd->lastInsertId();
		}
		else
		{
			return -2;
		}
	}

	/*
	*	fonction pour changer un joueur existant d’équipe existante.
	*/

	function changeJdE(&$bdd,&$idJ,&$nomEq,$idU)
	{

	}

	function existenumJDansEq(&$bdd,&$numJ,&$idEq)
	{	
		$stmtVEJdE=$bdd->prepare(
							   "SELECT numJ
								FROM joueur,equipe
								WHERE equipe.idEq=:idEquipe and joueur.numJ=:numJ");
		
		$stmtVEJdE->bindParam(':idEquipe',$idEq);
		$stmtVEJdE->bindParam(':numJ',$numJ); 
		

		if($stmtVEJdE->execute())
		{
			return (null != $stmtVEJdE->fetch());
		}
		else
		{
			return -2;
		}
	}

	

	function creerJoueur(&$bdd,&$prenomJ,&$nomJ,&$numJ,&$idEq,&$numJErr,&$idEqErr)
	{
		$nomEqExist=-2;
		if(0<($nomEqExist=equipeExiste($bdd,$idEq)))
		{

		}
		else
		{
			if($nomEqExist===-1)
				$idEqErr="Veuillez saisir une équipe existante pour le joueur ";
			else
			{
				echo "<div class=erreur>Erreur, veuillez ressayer ultérieurement<div>";
				return -1;
			}
		}



		if(false===existenumJDansEq($bdd,$numJ,$idEq)) 
		{

		}
		else
		{
			
			$numJErr="Deux joueurs dans une même équipe ne peuvent pas être désignés avec le même numéro de joueur.";
			return -1;
		}



		if(0<=($idP=creerPersonne($bdd,$prenomJ,$nomJ)))
		{
			$stmtNJ=$bdd->prepare("INSERT INTO joueur(idP, idEq, numJ) 
									VALUES (:idP,:idEq,:numJ)");
			$stmtNJ->bindParam(':idP',$idP);
			$stmtNJ->bindParam(':idEq',$idEq);
			$stmtNJ->bindParam(':numJ',$numJ);

			if($stmtNJ->execute())
			{
				return 1;
			}
			else
			{
				
				return -1;
			}
		}
		else
		{
			print("<div class=erreur>Erreur de création de joueur, veuillez ressayer ultérieurement.</div>");
			return -1;
		}
	}


	function creerUtilisateur(&$bdd,&$prenom,&$nom,&$email,&$motdePasse)
	{	
		if(utilisateurExiste($bdd,$email)<0)
		
		{
			$nid=creerPersonne($bdd,$prenom,$nom);
			//echo "lastInId=".$nid."\n</br>";
			
			
			$stmtInsererNU=$bdd->prepare("INSERT INTO utilisateur(idP,email,motdePasse)
										VALUES (?,?,?)
										");
			$stmtInsererNU->bindParam(1,$nid);
			$stmtInsererNU->bindParam(2,$email);
			$stmtInsererNU->bindParam(3,$motdePasse);
			if($stmtInsererNU->execute())
			{
				return $nid;
			}
			else 
			{
				error_log("je retourne -1 dans creerUtilisateur1");
				return -1;
			}
		
		}

		else
		{
			error_log("coucou dans creerUtilisateur");
			return -2;
		}
		
	}

	function creerEquipe(&$bdd,&$nomEq,&$nivE,&$idEnt,&$idU)
	{
		
		$stmtIEquip=$bdd->prepare(
			"INSERT INTO equipe(idEq,nomEq,nivE,idEntraineur,idU)
		 	VALUES (NULL,:nomEq,:nivE,:idEntraineur,:idU)");
		
	 	$stmtIEquip->bindParam(':nomEq',$nomEq);
		$stmtIEquip->bindParam(':nivE',$nivE);
		$stmtIEquip->bindParam(':idEntraineur',$idEnt);
		$stmtIEquip->bindParam(':idU',$idU);
		if($stmtIEquip->execute())
		{
			return 1;
		}
		else 
		{
			return -2;
		}
	

	}

	function creerTournoi(&$bdd,&$nomT,&$dateDeroul,$dureeJour,$idU)
    {
        $stmtcreatT=$bdd->prepare("INSERT INTO tournoi(idT, nomT, dateDeroul, dureeJour) 
                                        VALUES (NULL,:nomT,:dateDeroul,:dureeJour)");

        $stmtcreatT->bindParam(':nomT',$nomT);
        $stmtcreatT->bindParam(':dateDeroul',$dateDeroul);
        $stmtcreatT->bindParam(':dureeJour',$dureeJour);
        if($stmtcreatT->execute())
        {
            $idT= $bdd->lastInsertId();
            uGereT($bdd,$idT,$idU);
            return $idT;
        }
        else
        {
            error_log("Erreur dans creerTournoi, functions.php l.245\n");
            return -2;
        }
    }

    function uGereT(&$bdd,$idT,$idU)
    {
       $stmtuGereT=$bdd->prepare( "INSERT INTO orgGereT(idT, idP)
                                    VALUES (:idT,:idU)");
       $stmtuGereT->bindParam(':idT',$idT);
       $stmtuGereT->bindParam(':idU',$idU);

       if($stmtuGereT->execute())
       {
           return 1;
       }
       else
       {
           error_log("Erreur dans uGereT, functions.php l.264\n");
           return -2;

       }
    }

/*
    function synchroPoolsEtEquipe(&$ensTournois,&$ensSynchro)
    {
        foreach ($ensSynchro as $idT => $pool)
        {
            foreach($pool as $idTempPool => $equipe)
            {

            }
        }
    }

    function synchroPools(&$ensTournois,&$ensSynchro)
    {
        foreach($ensSynchro['pool']['gesTournoi'] as $idT=>$poolTemp)
        {
            foreach($poolTemp as $idPoolTemp=>$equipes)
            {
                foreach($equipes as $idEq => $status)

            }
        }
    }
*/

?>
