<?php
	class mm
	{
		private $secret=array();
		private $TAILLE;
		private $essai=array();
		private $nbessais=0;
		private $resultatC=""; //stock l"essai de l'utilsateur et le resultat obtenue a cette essai
		public function  __construct($TAILLE)
		{
			//$this->secret= array();
			$this->TAILLE=$TAILLE;
			$i=0;
			while ($i<$this->TAILLE)
			{
				array_push($this->secret,rand(0,9));
				$i++;
			}
			
		}

		public function __destruct()
		{
			
		}

		public function test($intEssai) //essai est un entier de taille TAILLE contenant l'essai de l'utilisateur
		{	
			$nLTBP=0; //nombre de lettres trouvée bien placées
			$nLTMP=0; //nombre de lettres trouvée mal placées
			$tabJeu=array_fill(0, $this->TAILLE, -1);
			$nbessais=0;
			$i=0;
			$this->essai=array();
			while ($i<$this->TAILLE) 
			{	
				array_push($this->essai,(int)($intEssai%10));
				$intEssai=(int)$intEssai/10;
				
				$i++;
			}

			$this->essai = array_reverse($this->essai);
			echo "essai= ";
			foreach ($this->essai as $key => $value) 
			{
				echo $value . " "  ;
			}
			echo "<br/>";

			if(count($this->essai)==$this->TAILLE)
			{	

				for($i=0;$i<$this->TAILLE;$i++)
				{	
					
					
					if ($this->essai[$i]==$this->secret[$i])
					{	
						//echo "<b>$i</b> ";
						$tabJeu[$i]=$i;
						$nLTBP++;
					}

					/*else
					{
						//echo $this->essai[$i] . " ";
					}*/

				}
				
				for($i=0;$i<$this->TAILLE;$i++)
				{	
					for($j=0;$j<$this->TAILLE;$j++)
					{
						if(($this->essai[$i] == $this->secret[$j] ) && ($tabJeu[$j]==-1))
						{
							$nLTMP++;
							$tabJeu[$j]=$i;
							break;
						}
					}
				}
				
				$this->nbessais++;
				$res = (($this->TAILLE + 1)*$nLTBP) + $nLTMP;


				if ($res==-1)
			    	return  "Erreur de saisie !\n";
			    
			    else if ($res==0)
			    	return "Aucune lettre correcte !\n";
			    
			    else 
			    {	
			     	return  (int)($res/($this->TAILLE+1)) . " lettres bien placées et " . (int)$res%($this->TAILLE+1) . " lettres mal placées !\n" ;
			      
			      	if($res/($this->TAILLE+1)==$this->TAILLE)
			      	{
			        	return "BRAVO ! Vous avez réussi en " . $this->getnbessais() . " essais !\n";
			        }
			    }
					
				return '1';
			}

			else 
			{	
				echo "taille du nbr essai= " . count($this->essai) ." TAILLE=" . $this->TAILLE;
				return '-1';
			}

			
		}

		public function echonbessais()
		{
			echo "nbEssais= " . $this->nbessais;
		}

		public function getSecret()
		{	

			return $this->secret;
		}
		public function echoSecret()
		{	echo "chiffreSecret=";
			foreach ($this->secret as $i => $nbr)
			{
				echo $nbr;
			}
			echo "<br/>\n";

			
		}
		public function getnbessais()
		{
			return $this->nbessais;
		}
	}
?>