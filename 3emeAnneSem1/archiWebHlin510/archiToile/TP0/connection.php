<!DOCTYPE html>
<html>
<head>
</head>
<body>
   
	<!--
		connection.php

		Ce script sert à s'authentifier.
		Il est séparé en deux blocs :
			- Un bloc php vérification des données du formulaire.
			- Un bloc  html affichage du formulaire.

	-->

	<!--
		Premier bloc php de vérification des données du formulaire :
		- Le login et le mot de passe doivent être présents et correspondre
			dans la base de données, si oui alors sauvegarde du login
			dans la variable de session et redirection vers
			la page principale.
	-->


	<?php
		if(isset($_POST['login']) && isset($_POST['password']))
		{
			try 
			{
				$bdd = new PDO('mysql:host=localhost;dbname=Gsommerz;charset=utf8','root', "liveforspeedliveforspeed123");
			}
			catch(Exception $e)
			{
				die($e->getMessage());
			}

			$req = $bdd->query("SELECT * FROM utilisateurs;");

			$trouve = false;
			$admin = false;

			while($res = $req->fetch())
			{
				if($res['login']==$_POST['login'] && $res['password']==$_POST['password'])
				{
					$trouve = true;
					echo "$trouve = true";
					$admin = $res['admin'];
				}
			}


			if ($trouve) {
				session_start();
				$_SESSION['login'] = $_POST['login'];
				$_SESSION['admin'] = $admin;
				//header('Location: http://tp.jeremybressand.com/tp5/exercice_13/pagePrincipale.php');
				
				//echo "<meta http-equiv='refresh' content='0'; url='/wordpress2/wp-content/themes/childRelia/catalog/imgtest.jpg' />";
				header('Location: /wordpress2/wp-content/themes/childRelia/catalog/modifieCatalog.php');
				exit();
			}

		}
	?>

	<!--
		Deuxième bloc php : formulaire.
	-->

	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<label for='login'>Entrer login : </label>
	<input type='text' name='login' id='login' pattern='[a-zA-Z_]{2,30}' required></br>
	<label for='password'>Entrer password :</label>
	<input type='password' name='password' id='password' pattern='[a-zA-Z_0-9]{2,50}' required></br>
	<input type='submit' value="S'authentifier">
	</form>


</body>
</html>
