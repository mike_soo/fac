\select@language {french}
\contentsline {section}{\numberline {1}Logiciels libres : une introduction}{1}
\contentsline {subsection}{\numberline {1.1}Qu'est ce qu'un logiciel ?}{1}
\contentsline {subsection}{\numberline {1.2}Qu'est-ce qu'un logiciel libre ?}{1}
\contentsline {subsection}{\numberline {1.3}La FSF et le projet GNU}{2}
\contentsline {subsection}{\numberline {1.4}GNU/Linux, une r\IeC {\'e}alit\IeC {\'e}}{2}
\contentsline {subsection}{\numberline {1.5}Pourquoi le logiciel libre est t'il meilleur? }{3}
\contentsline {subsection}{\numberline {1.6}Brevets sur les logiciels}{4}
\contentsline {subsection}{\numberline {1.7}Pour en savoir plus}{5}
