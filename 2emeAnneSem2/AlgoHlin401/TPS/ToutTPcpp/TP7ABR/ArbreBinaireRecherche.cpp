#include "ArbreBinaireRecherche.h"



SommetABR::SommetABR(Valeur v){
  racine=v; SAG=NULL; SAD=NULL;Pere=NULL;FGP=false;
}

SommetABR::SommetABR(SommetABR& s){
 
  racine=s.racine; SAG=NULL; SAD=NULL;FGP=false;
  if (s.SAG!=NULL) 
    GrefferSAG(new SommetABR(*(s.SAG)));
  
  if (s.SAD!=NULL) 
    GrefferSAD(new SommetABR(*(s.SAD)));
}

void SommetABR::GrefferSAG(ABR r)
{
  SAG=r;
  if(SAG!=NULL)
  SAG->FGP=true;
}

void SommetABR::GrefferSAD(ABR r)
{
  SAD=r;
}

ABR SommetABR::PlusPetit(){
  //implémenter
  if(SAG==NULL)
   return this;
  

  else 
  {
    ABR pPetit= SAG->PlusPetit();
    return pPetit;
  }
}

bool SommetABR::FeuilleP()
{
  
    return SAG==NULL && SAD == NULL;

}

ABR SommetABR::RechercherValeur(Valeur v)
{
  //implémenter
   if(this==NULL)
   {
    return NULL;
   }

   if(racine==v)
    return this;
   
   else 
   { 
     ABR Rv = SAG->RechercherValeur(v);

      if(Rv==NULL)
      {
        Rv = SAD->RechercherValeur(v);
        return Rv;
      } 
      return Rv; 
   }

  } 

void SommetABR::InsererValeur(Valeur v){
  //implémenter
  if (SAD==NULL && v >= racine )
    {
      SAD= new SommetABR(v);
      SAD->Pere=this;
    }

  else if (SAG==NULL && v < racine )
    {
      SAG= new SommetABR(v);
      SAG->Pere=this;
      SAG->FGP=true;
    }

  else 
    { 
      
      if(v>=racine)
      { 
        SAD->InsererValeur(v);
      }

      else
      {
        SAG->InsererValeur(v);
      }
    }

  }

void SommetABR::SupMin()
{
  //implémenter

  ABR pPetit=this->PlusPetit();
  
  if(pPetit->FeuilleP())
  {
    pPetit->Pere->SAG=NULL;
    
  }

  else //if(pPetit->SAD!=NULL)
  {
    
   

   if (pPetit->SAD!=NULL)
    {
      pPetit->RemplacerPourLePerePar(pPetit->SAD);
    }
   
      
  }

 
}

void SommetABR::RemplacerPourLePerePar(ABR r)
{
  if(Pere==NULL)
  {
    r->Pere=NULL;
  }

  else if (FGP)   //(Pere->SAG==this)
  {
    Pere->SAG=r;
    r->Pere=Pere;
    
  }


  else //if (Pere->SAD==this)
  {
    Pere->SAD=r;
    r->Pere=Pere;
    
  }

}


void SommetABR::SupprimerValeur(Valeur v)
{
  ABR rechVal=RechercherValeur(v);
  
  if (rechVal!=NULL)
   {
     if (rechVal->FeuilleP() && rechVal->Pere !=NULL) //cas ou on se trouve avec un feuille qq
      { 
        if (rechVal->FGP)//(Pere->SAG==this)
          rechVal->Pere->SAG=NULL;

        else 
          rechVal->Pere->SAD=NULL;

       
      }

     else if(rechVal->FeuilleP() && rechVal->Pere==NULL) //cas ou on supprime la racine etant elle meme une feuille
      {
        delete [] rechVal;
      }

     else if (rechVal->SAD==NULL)  //cas on le sommet n'a qu'un fils gauche
      {
        rechVal->RemplacerPourLePerePar(rechVal->SAG);
        
      }

     else //cas ou on se retrouve avec un sommet qui possede un fils droit
      { rechVal->racine=rechVal->SAD->PlusPetit()->racine;
        rechVal->SAD->SupMin();
        
      }
    } 
}

AB SommetABR::ABRverAB() //convertie ABR en AB pour la sortiLatex
{
  if (FeuilleP())
  {
    AB s=new Sommet(racine);
    return s;
  }

  else
  {
    AB s=new Sommet(racine);
    if(SAG!=NULL)
    s->SAG=SAG->ABRverAB();

    if(SAD!=NULL)
    s->SAD=SAD->ABRverAB();
    return s;

  }
}




/* compiler avec g++  ArbreBinaireRecherche.cpp SortieLatex.cpp  */
