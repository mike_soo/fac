#ifndef ARBREBINAIRERECHERCHE_H
#define ARBREBINAIRERECHERCHE_H

#include <iostream>
#include <sstream>


#include <cstdlib>
#include <fstream>
#include "AB.h"

using namespace std;


typedef int Valeur;

class SommetABR;

typedef SommetABR* ABR;

class SommetABR  {
  protected:
   Valeur racine;
  ABR Pere,SAG, SAD;
  bool FGP;

 public:
 

  SommetABR(Valeur v);
  SommetABR(SommetABR& s);

  void GrefferSAG(ABR r);
  void GrefferSAD(ABR r);

  void SupprimerSAG();
  void SupprimerSAD();

  bool FeuilleP();

  void RemplacerPourLePerePar(ABR);

   friend std::string* TikzRecursABR(int ligne,int gauche, int droite, int numeroPere, int typeFils, ABR Ar);

// ABR

  ABR PlusPetit();
  ABR RechercherValeur(Valeur v);
  void InsererValeur(Valeur v);
  void SupprimerValeur(Valeur v); // notez la dissymétrie
  void SupMin();
  AB ABRverAB();
  friend class Sommet;
};

 
 void SortieLatex(ABR);
 


#endif
