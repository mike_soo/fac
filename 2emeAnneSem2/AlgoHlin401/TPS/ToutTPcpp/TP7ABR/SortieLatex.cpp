//SortieLatex.cpp

/*
Je vous joint ce fichier avec 
AB.cpp .h
ArbreBinaireRecherche.cpp .h


Car pour afficher le tableau grace a sortieLatex vous avez besoin imperativement de ce fichier (SortieLatex.cpp)
et du AB.cpp car pour l'affichage je convertie l'ABR en AB a cause du mauvais fonctionement de sortieLatex(ABR)
puis biensur tout les algo  de construction d'un ABR se trouve dans ArbreBinaireRecherche.cpp .h
Je vous joins le ABR.h car il y a quelques modifications de definitions.

Vous trouverez plus d'information dans le main si dessous

Cordialement 

Michael OHAYON-ORDAZ


*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <stdlib.h>
#include "ArbreBinaireRecherche.h"



std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);



std::string * TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar){
  std::ostringstream ossnum, osslign,osscol,ossnumPere, ossbal, ossnum2Pere,ossnumRac;

      std::string stres("");

      if (Ar) {
    ossnumPere<<numeroPere;
    ossnumRac<<Ar->racine;
    if (Ar->Pere )ossnum2Pere<<Ar->Pere->racine; else ossnum2Pere<<0;
    int numero;
    if (typeFils==-1) numero=1; else numero= 2*numeroPere + typeFils;
    ossnum<<numero;
    osslign<<ligne;
    int mil = (gauche + droite)/2;
    osscol<<mil;

    stres="\\node[draw] (SZ" + ossnum.str() + ") at (" + osscol.str() + ", " + osslign.str() + ") { " + ossnumRac.str() + "};\n";

    if (typeFils!=-1) stres+="\\draw (SZ"+ossnumPere.str()+") -- (SZ"+ossnum.str() +");\n";
    if (Ar->SAG) stres+=*TikzRecursAB(ligne -1 ,gauche,mil-1, numero,0,Ar->SAG);
    if (Ar->SAD) stres+=*TikzRecursAB(ligne - 1,mil+1,droite, numero,1,Ar->SAD);
 }
  return new std::string(stres);
}

std::string * TikzAB(AB Ar){
  return TikzRecursAB(1,1,10,1, -1,Ar);
}

 void SortieLatex(AB Ar){
 std::ofstream fichier("figOHAYON-ORDAZ.tex", std::ios::out | std::ios::trunc);
 std::string preamb ("\\documentclass[a4paper]{article} \n \\usepackage{tikz} \n \\begin{document} \n \\begin{tikzpicture}\n");
  std::cout<<preamb<<"\n";
std::string post("\\end{tikzpicture}\n  \\end{document} \n");
  std::cout<<post<<"\n";
 std::cout<<*TikzAB(Ar)<<"\n";
std::string   res1(preamb + *TikzAB(Ar));
 std::string   res(res1 + post);
 //std::cout<<res1<<"\n";
  fichier <<res<<"\n";
  fichier.close();
  std::system("pdflatex -interaction nonstopmode figOHAYON-ORDAZ.tex; rm figOHAYON-ORDAZ.log; rm figOHAYON-ORDAZ.tex; rm figOHAYON-ORDAZ.aux; open figOHAYON-ORDAZ.pdf");
  return;
}

int main() {
  ABR A1=new SommetABR(11);
  //implémenter
  A1->InsererValeur(5);
  
  A1->InsererValeur(15);

  A1->InsererValeur(3);
  
  A1->InsererValeur(9);

  A1->InsererValeur(7);
  
  A1->InsererValeur(8);

  A1->InsererValeur(10);



  AB A = A1->ABRverAB();
  
 

  A1->SupprimerValeur(5);  //decomenter pour supprimer la valeur 5 de l'arbre qui a comme racine A1
 

  A = A1->ABRverAB();  //convertir ABR en AB pour utiliser le SortieLatex des AB qui est fonctionelle

 SortieLatex(A);
 

  return 1;
}


// g++ -c SortieLatex.cpp
