//SortieLatex.cpp

/*
Je vous joint ce fichier avec 
AB.cpp .h


Car pour afficher le tableau grace a sortieLatex vous avez besoin imperativement de ce fichier (SortieLatex.cpp)
puis biensur tout les algo  de construction d'un AB se trouve dans AB.cpp

Vous trouverez plus d'information dans le main si dessous

Cordialement 
Michael OHAYON-ORDAZ

*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <stdlib.h>


using namespace std;
typedef int Valeur;

class Sommet;

typedef Sommet* AB;

  std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);

class Sommet {
  protected:
  
  bool FGP;

  int hauteur,balanceGmoinsD;

  public:
    Valeur racine;
  AB Pere,SAG, SAD;
  void recopie(AB r);
  bool FeuilleP();

  Sommet(Sommet &r);
  Sommet(Valeur v);
  Sommet();
  void GrefferSAG(AB g);
  void GrefferSAD(AB d);
  void SupprimerSAG();
  void SupprimerSAD();
  void SupprimeAC(); //suprime arbre completement a partir de la racine/instance courante
  void RemplacerPourLePerePar(AB);
  
      
   friend std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);
};

std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);



std::string * TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar){
  std::ostringstream ossnum, osslign,osscol,ossnumPere, ossbal, ossnum2Pere,ossnumRac;

      std::string stres("");

      if (Ar) {
    ossnumPere<<numeroPere;
    ossnumRac<<Ar->racine;
    if (Ar->Pere )ossnum2Pere<<Ar->Pere->racine; else ossnum2Pere<<0;
    int numero;
    if (typeFils==-1) numero=1; else numero= 2*numeroPere + typeFils;
    ossnum<<numero;
    osslign<<ligne;
    int mil = (gauche + droite)/2;
    osscol<<mil;

    stres="\\node[draw] (SZ" + ossnum.str() + ") at (" + osscol.str() + ", " + osslign.str() + ") { " + ossnumRac.str() + "};\n";

    if (typeFils!=-1) stres+="\\draw (SZ"+ossnumPere.str()+") -- (SZ"+ossnum.str() +");\n";
     if (!(Ar->SAG==NULL)) stres+=*TikzRecursAB(ligne -1 ,gauche,mil-1, numero,0,Ar->SAG);
    if (!(Ar->SAD==NULL)) stres+=*TikzRecursAB(ligne - 1,mil+1,droite, numero,1,Ar->SAD);
 }
  return new std::string(stres);
}

std::string * TikzAB(AB Ar){
  return TikzRecursAB(1,1,10,1, -1,Ar);
}

 void SortieLatex(AB Ar)
 {
  std::ofstream fichier("figOHAYON-ORDAZ.tex", std::ios::out | std::ios::trunc);
  std::string preamb ("\\documentclass[a4paper]{article} \n \\usepackage{tikz} \n \\begin{document} \n \\begin{tikzpicture}\n");
  std::cout<<preamb<<"\n";
  std::string post("\\end{tikzpicture}\n  \\end{document} \n");
  std::cout<<post<<"\n";
  std::cout<<*TikzAB(Ar)<<"\n";
  std::string   res1(preamb + *TikzAB(Ar));
  std::string   res(res1 + post);
  //std::cout<<res1<<"\n";
  fichier <<res<<"\n";
  fichier.close();
  std::system("pdflatex -interaction nonstopmode figOHAYON-ORDAZ.tex; rm figOHAYON-ORDAZ.log; rm figOHAYON-ORDAZ.tex; rm figOHAYON-ORDAZ.aux; open figOHAYON-ORDAZ.pdf");
  return;
 }



// g++ -c SortieLatex.cpp
int main(int argc,char **argv)
{ /* Voice donc ma proposition de code qui vous permet en enlevent les slash  
    de comentaire apartir de la ligne 153 à 180, de tester les diferentes
    methodes disponible. Par default, à l'execution sans modification, ce code vous retournera 
    un fichier pdf avec un arbre dessiné grace a latex.
  */


  /*
  La valeur se trouve stocké dans Valeur racine, et on s'apercoit qu'il 
  s'agit d'un arbre binaire car on a au maximum deux fils posible par noeuds : SAG, SAD. 
  */

  AB A0=new Sommet(0);
  AB A00=new Sommet(0);
  
  AB A2=new Sommet(2);
  AB A22=new Sommet(2);
  
  
  AB A4=new Sommet(4);
  AB A44=new Sommet(4);

  AB A5=new Sommet(5);
  
  AB A6=new Sommet(6);
  
  AB A7=new Sommet(7);

  AB A8=new Sommet(8);

  AB A9=new Sommet(9);

  AB A10=new Sommet(10);

  AB A11=new Sommet(11);
  
  A6->GrefferSAG(A0);
  A6->GrefferSAD(A00);
  A0->GrefferSAD(A4);
  A0->GrefferSAG(A2);
  A00->GrefferSAG(A22);
  A00->GrefferSAD(A44);
  A44->GrefferSAG(A5);
  A5->GrefferSAD(A7);
  A7->GrefferSAG(A11);
  
  A22->GrefferSAD(A9); 
  A22->GrefferSAG(A10);

  
  AB AT=new Sommet(*A6); //creation d'un arbre par copiee
  
  
    //decomenter si desous pour tester le bon fonctionement de FeuilleP()
  /* 
   if (A10->FeuilleP())
   {
     cout<<"Je suis A10 est je suis bien une feuille"<<endl;
   }  

   if (!(A8->FeuilleP()))
   {
     cout<<"Je suis A8 est je ne suis pas une feuille"<<endl;
   }  
  */

 
  
  //AT->SupprimerSAD(); //suprime le SAD de AT SANS modifié l'arbre originel (A6)
  
  AT->SupprimerSAG(); //suprime le SAG de AT SANS modifié l'arbre originel (A6)

  
  //A00->RemplacerPourLePerePar(A5); //le pere de A00 prend comme sous arbre droit (car A00 est lui meme un sous arbre droit) l'abre pointé A5
         
  //A0->RemplacerPourLePerePar(A5); //le pere de A0 prend comme sous arbre gauche (car A0 est lui meme un sous arbre gauche) l'abre pointé A5
  //Les deux modification avec la methode RemplacerPourLePerePar(..) sont visible que à l'affichage de A6


  //SortieLatex(AT); //Affiche l'arbre generé par copie
                                  
  SortieLatex(A6); //On affiche la l'abre qui a pour racine A6;
                    //qui ne subira pas les modification apliqué sur AT.
  return 1;
}