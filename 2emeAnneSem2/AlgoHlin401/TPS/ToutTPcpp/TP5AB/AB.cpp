//AB.cpp

#include "AB.h"
using namespace std;

Sommet::Sommet(Valeur v){
  //à implémenter
 racine=v;
 FGP=false;
 Pere=NULL;
 SAG=NULL;
 SAD=NULL;
}

Sommet::Sommet(Sommet &r)
{	FGP=false;
	Pere=NULL;
	SAG=NULL;
	SAD=NULL;
	racine=r.racine;
	
	if(!(r.SAG==NULL))
		{
			SAG=new Sommet(*(r.SAG));
			SAG->FGP=true;
			SAG->Pere=this;
		}
	if(!(r.SAD==NULL))
		{
			SAD=new Sommet(*(r.SAD));
			SAD->Pere=this;
		}
}
Sommet::Sommet()
{
 Pere=NULL;
 SAG=NULL;
 SAD=NULL;
 FGP=false;
 racine=0;

}

bool Sommet::FeuilleP(){
  //à implémenter
	return (SAG==NULL) && (SAD==NULL);
	

}


void Sommet::SupprimerSAG(){
  //à implémenter
	if(FeuilleP())
	{

	}

	else if(SAG!=NULL)
	{   SAG->Pere=NULL;
	 	SAG=NULL;
	 	
	}

}


void Sommet::SupprimerSAD(){
  //à implémenter
	if(FeuilleP())
	{

	}

	else if(SAD!=NULL)

	{  	SAD->Pere=NULL;
		SAD=NULL;
		
	  
	}
}



void Sommet::GrefferSAG(AB g){
  //à implémenter

	SAG=g;
	
	if(!(SAG==NULL))
		{
			SAG->Pere=this;
			SAG->FGP=true;
		}

 }

void Sommet::GrefferSAD(AB d){
  //à implémenter
	
	SAD=d; 
	
	if(!(SAD==NULL))
		SAD->Pere=this;

 }


void Sommet::RemplacerPourLePerePar(AB Ar){
  //le pere existe
  //à implémenter
	if(FGP)
	{  
		Pere->SAG=Ar;
		Ar->Pere=this->Pere;
		Ar->FGP=true;
    }
    else if(!(Pere==NULL))
    {	
    	
    	Pere->SAD=Ar;
    
		Ar->Pere=this->Pere;
	
		Ar->FGP=false;
		

    }
}





