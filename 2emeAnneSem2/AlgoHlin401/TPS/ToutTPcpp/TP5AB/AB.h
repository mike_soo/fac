//AB.h
#ifndef AB_H
#define AB_H

#include <iostream>
#include <sstream>

typedef int Valeur;

class Sommet;

typedef Sommet* AB;

  std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);

class Sommet {
  protected:
  
  bool FGP;

  int hauteur,balanceGmoinsD;

  public:
    Valeur racine;
  AB Pere,SAG, SAD;
  void recopie(AB r);
  bool FeuilleP();
  Sommet(Sommet &r);
  Sommet(Valeur v);
  Sommet();
  void GrefferSAG(AB g);
  void GrefferSAD(AB d);
  void SupprimerSAG();
  void SupprimerSAD();
  void SupprimeAC(); //suprime arbre completement a partir de la racine/instance courante
  void RemplacerPourLePerePar(AB);
  
      
   friend std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);
};



#endif
