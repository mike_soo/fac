/*
pour compiler: make

pour lancer l'exec:

    ./fmyst 3 10 1 (avec l'explication de chaque parametre a la ligne 196)
       lancer l'exec si dessus pour avoir le resultat de chaque division de la question 2

    ./fmyst sans parametre pour avoir la somme binaire d'un entier represent� par un tableau
    
    ./fmyst n l'appelle a l'executable avec 1 parametre declanche la 
    representation de le nombre moyen d'affectations que l'on effectue 
    quand on incremente tous les tableaux qui represente les entiers 
    entre 0 et 2^n -1

Je vous joint le .h est le .o de fonctionsMysterieuses autrement la compilation ne sera pas possible, puis je vous 
joint aussi des fichier de trace de l'execution des algorithme si dessous pour avoir plus de details sur le comportment
du programme.

Cordialement 
Michael OHAYON-ORDAZ
 
*/

#include <iostream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>     /* atoi */

#include "fonctionsMysterieuses.h"

using namespace std;

int apuissanceb(int a, int b) {
// renvoie a puissance b
  if (b==0) return 1;
  if (b % 2 == 0) return apuissanceb(a * a, b / 2);
  return a * apuissanceb(a,b-1);}

int ajoute1bin(int n,bool *T)
{bool addOne=true;
 int i=n-1;
 int cptAff=0;

  while(i>=0 and addOne==true)
  {
    if(T[i]==1)
    {
      cptAff++;
      T[i]=0;
      i--;

    }

    else if(T[i]==0)
    { 
      cptAff++;
      T[i]=1;
      addOne=false;
      i--;
    }

    

  }
  return cptAff;
  cout<<"le nombre d'affectation pour l'incrementation de votre entier est de "<<cptAff<<" affectations"<<endl;
}

int representEntierBin(int entier,bool *tab)
{ int cptAff=0;
  cout<<"entier choise rep en binaire avant l'add "<<entier<<":"<<endl;
  int divi=entier;
    for(int i=0;i<64;i++)
    {
       tab[i]=0;
    }

    int i=63;
    while(divi!=0)
    {
     if(entier % 2==0)
      { divi=entier/2;
        tab[i]=0;
        entier/=2;
         i--;
      }

     else if(entier % 2==1)
      { 
        divi=entier/2;
        tab[i]=1;
        entier/=2;
        i--;
      }
    }
    
    for(int j=0;j<64;j++)
     {
       cout<<tab[j];
     }
    cout<<endl;

    cout<<"entier choise + 1 en binare"<<endl;

    cptAff=ajoute1bin(64,tab);
    for(int j=0;j<64;j++)
    {
       cout<<tab[j];
    }
    cout<<"nombreD'aff="<<cptAff<<endl;

    return cptAff;
}

int main(int argv, char** argc){


 if(argv==1)
 { 
    cout<<"l'appelle a l'executable sans parametres declanche la representation et l'adittion d'un nombre binaire!"<<endl;
    cout<<"rentrer entiers dont on ajoutera 1 avec une operation binaire ";
    int entier;
    cin>>entier;
    cout<<endl;
    bool *tab=new bool [64];
    representEntierBin(entier,tab);


    
   return 0;
 }
 
 if(argv==2)
 {
  
    float moy;
    int cptAff=0;
    int entier=atoi(argc[1]);
    
    cout<<endl;
    bool *tab=new bool [64];
    for(int i=0;i<(apuissanceb(2,entier)-1);i++)
     cptAff+=representEntierBin(i,tab);
   if(entier!=0)
     moy=(float)cptAff/(apuissanceb(2,entier)-1);

   std::cout << "\033[2J\033[1;1f";
   cout<<"l'appelle a l'executable avec 1 parametre declanche la representation de le nombre moyen d'affectations que l'on effectue quand on incremente tous les tableaux qui represente les entiers entre 0 et 2^n -1"<<endl;
   cout<<"n="<<argc[1]<<endl;
   cout<<"le nombre moyen d'affectation = "<<moy<<endl;
   return 0;

 }
 

 if(argv!=4)
   {
    cout<<"rentrez trois param min: i1=boucleInit i2=boucleFin i3=inc"<<endl;
    return 0;
   }
   unsigned int i1=atoi(argc[1]);
   unsigned int i2=atoi(argc[2]);
   unsigned int i3=atoi(argc[3]);

   double rap ;
 cout<<endl;
cout<<"------------------log(i)------------------"<<endl;
   
 for(int i=i1;i<i2;i+=i3)
 {
    cout<<(double)f1(i)/(int)(log(i))<<left<<setw(16);
    cout<<(double)f2(i)/(int)(log(i))<<left<<setw(16);
    cout<<(double)f3(i)/(int)(log(i))<<left<<setw(16);
    cout<<(double)f4(i)/(int)(log(i))<<left<<setw(16);
    cout<<(double)f5(i)/(int)(log(i))<<left<<setw(16);
    cout<<(double)f6(i)/(int)(log(i))<<left<<setw(16);
    cout<<endl;
    
  }
   
  cout<<"------------------sqrt(i)------------------"<<endl;
  
 for(int i=i1;i<i2;i+=i3)
 {  
    cout<<(int)f1(i)/(int)(3*sqrt(i))<<left<<setw(16);
    cout<<(int)f2(i)/(int)(3*sqrt(i))<<left<<setw(16);
    cout<<(int)f3(i)/(int)(3*sqrt(i))<<left<<setw(16);
    cout<<(int)f4(i)/(int)(3*sqrt(i))<<left<<setw(16);
    cout<<(int)f5(i)/(int)(3*sqrt(i))<<left<<setw(16);
    cout<<(int)f6(i)/(int)(3*sqrt(i))<<left<<setw(16);
    cout<<endl;
  }
cout<<endl;
cout<<"------------------apuissanceb(i,2)------------------"<<endl;
 
 for(int i=i1;i<i2;i+=i3)
 {
    cout<<(double)f1(i)/(int)(apuissanceb(i,2))<<left<<setw(16);
    cout<<(double)f2(i)/(int)(apuissanceb(i,2))<<left<<setw(16);
    cout<<(double)f3(i)/(int)(apuissanceb(i,2))<<left<<setw(16);
    cout<<(double)f4(i)/(int)(apuissanceb(i,2))<<left<<setw(16);
    cout<<(double)f5(i)/(int)(apuissanceb(i,2))<<left<<setw(16);
    cout<<(double)f6(i)/(int)(apuissanceb(i,2))<<left<<setw(16);
    cout<<endl;
  }

 cout<<endl; 
cout<<"------------------apuissanceb(i,5)------------------"<<endl;
 

 for(int i=i1;i<i2;i+=i3)
 {
    cout<<(double)f1(i)/(int)(apuissanceb(i,5))<<left<<setw(16);
    cout<<(double)f2(i)/(int)(apuissanceb(i,5))<<left<<setw(16);
    cout<<(double)f3(i)/(int)(apuissanceb(i,5))<<left<<setw(16);
    cout<<(double)f4(i)/(int)(apuissanceb(i,5))<<left<<setw(16);
    cout<<(double)f5(i)/(int)(apuissanceb(i,5))<<left<<setw(16);
    cout<<(double)f6(i)/(int)(apuissanceb(i,5))<<left<<setw(16);
    cout<<endl;
  }
 cout<<endl;
cout<<"------------------apuissanceb(2,i)------------------"<<endl;
 
 for(int i=i1;i<i2;i+=i3)
 { 
    cout<<(double)f1(i)/(int)(apuissanceb(2,i))<<left<<setw(16);
    cout<<(double)f2(i)/(int)(apuissanceb(2,i))<<left<<setw(16);
    cout<<(double)f3(i)/(int)(apuissanceb(2,i))<<left<<setw(16);
    cout<<(double)f4(i)/(int)(apuissanceb(2,i))<<left<<setw(16);
    cout<<(double)f5(i)/(int)(apuissanceb(2,i))<<left<<setw(16);
    cout<<(double)f6(i)/(int)(apuissanceb(2,i))<<left<<setw(16);
    cout<<endl;
  }

 cout<<endl;
cout<<"------------------apuissanceb(3,i)------------------"<<endl;

 for(int i=i1;i<i2;i+=i3)
 {
    cout<<(double)f1(i)/(int)(apuissanceb(3,i))<<left<<setw(16);
    cout<<(double)f2(i)/(int)(apuissanceb(3,i))<<left<<setw(16);
    cout<<(double)f3(i)/(int)(apuissanceb(3,i))<<left<<setw(16);
    cout<<(double)f4(i)/(int)(apuissanceb(3,i))<<left<<setw(16);
    cout<<(double)f5(i)/(int)(apuissanceb(3,i))<<left<<setw(16);
    cout<<(double)f6(i)/(int)(apuissanceb(3,i))<<left<<setw(16);
    cout<<endl;
  }

/*On constate avec le code si dessus, apres quelques essaie en changeant les valeur i1,i2,i3 passer en parametres 
 o�  i1=boucleInit; i2=boucleFin; i3=incrementation; de toutes les boucles 'for' presentes on s'apercoit que:
 

**f1(n)-> c*sqrt(n) avec c ~ 3 car pour i1=3; i2=10; i3=1; on avec phi(n)=(int)(3*sqrt(n))------

f1(n)/phi(n) f2(n)/phi(n) f3(n)/phi(n) f4(n)/phi(n) f5(n)/phi(n) f6(n)/phi(n)
 *
14            0            0            16           108          
1            17            1            0            26           270          
1            52            2            0            53           810          
1            111           2            0            91           2082         
1            240           3            0            182          6248         
1            409           4            0            320          16402        
1            656           4            0            568          43740        
  

**f2(n)-> c*n^5 avec c ~ 0.1 car pour i1=3; i2=10; i3=1;on avec phi(n)=(int)(n^5)------

f1(n)/phi(n)    f2(n)/phi(n)    f3(n)/phi(n)    f4(n)/phi(n)    f5(n)/phi(n)    f6(n)/phi(n)
                    *
0.0205761       0.0987654       0.0164609       0.00823045      0.329218        2.22222         
0.00585938      0.0996094       0.0078125       0.00195312      0.15625         1.58203         
0.00192         0.09984         0.00384         0.00096         0.1024          1.5552          
0.000900206     0.0999228       0.00231481      0.000385802     0.0823045       1.875           
0.000416493     0.0999584       0.00142798      0.000178497     0.0761587       2.60249         
0.000244141     0.0999756       0.000976562     0.00012207      0.078125        4.00452         
0.000152416     0.0999848       0.000677404     6.77404e-05     0.0867076       6.66667         


**f3(n)-> c*n^2 avec c ~ 0.5  car pour i1=3; i2=10; i3=1;on avec phi(n)=(int)(n^2)------

f1(n)/phi(n)    f2(n)/phi(n)    f3(n)/phi(n)    f4(n)/phi(n)    f5(n)/phi(n)    f6(n)/phi(n)
                                    *
0.555556        2.66667         0.444444        0.222222        8.88889         60              
0.375           6.375           0.5             0.125           10              101.25          
0.24            12.48           0.48            0.12            12.8            194.4           
0.194444        21.5833         0.5             0.0833333       17.7778         405             
0.142857        34.2857         0.489796        0.0612245       26.1224         892.653         
0.125           51.1875         0.5             0.0625          40              2050.31         
0.111111        72.8889         0.493827        0.0493827       63.2099         4860 


**f4(n)-> c*log(n) avec c ~ 2  car pour i1=3; i2=10; i3=1; on  a avec phi(n)=(int)(2*log(n))-------

 f1(n)/phi(n) f2(n)/phi(n) f3(n)/phi(n) f4(n)/phi(n) f5(n)/phi(n) f6(n)/phi(n)
                                        *
2.5           12            2           1             40           270         
3             51            4           1             80           810         
2             104           4           1             106.667      1620        
2.33333       259           6           1             213.333      4860        
2.33333       560           8           1             426.667      14580       
2             819           8           1             640          32805       
2.25          1476          10          1             1280         98415  


 f5(n)-> c*2^n avec c =10  car pour i1=3; i2=10; i3=1; on  a avec phi(n)=(int)(2^n)-------
                                                                *
0.625           3               0.5             0.25            10              67.5            
0.375           6.375           0.5             0.125           10              101.25          
0.1875          9.75            0.375           0.09375         10              151.875         
0.109375        12.1406         0.28125         0.046875        10              227.812         
0.0546875       13.125          0.1875          0.0234375       10              341.719         
0.03125         12.7969         0.125           0.015625        10              512.578         
0.0175781       11.5312         0.078125        0.0078125       10              768.867         


 f6(n)-> c*3^n avec c=20  car pour i1=3; i2=10; i3=1; on  a avec phi(n)=(int)(3^n)-------
                                                                                *
0.185185        0.888889        0.148148        0.0740741       2.96296         20              
0.0740741       1.25926         0.0987654       0.0246914       1.97531         20              
0.0246914       1.28395         0.0493827       0.0123457       1.31687         20              
0.00960219      1.06584         0.0246914       0.00411523      0.877915        20              
0.00320073      0.768176        0.0109739       0.00137174      0.585277        20              
0.00121933      0.499314        0.00487731      0.000609663     0.390184        20              
0.000457247     0.299954        0.00203221      0.000203221     0.260123        20      

Question bonus : on s'appercoit apres multiples essais que le nombre moyen 
d'affectations au tableau de bools ne depassent pas 2 !!! 
 
}

/*
ordre de compilation :  g++ SolutionsFonctionMysterieuses.cpp fonctionsMysterieuses.o -o test
Ordre d'ex�cution :  ./test 1 2 
*/
return 0;

}