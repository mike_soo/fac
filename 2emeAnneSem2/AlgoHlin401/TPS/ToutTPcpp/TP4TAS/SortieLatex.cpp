//SortieLatex.cpp
/*
Je vous joint ce fichier avec 
AB.cpp .h
Tas.cpp .h

Car pour afficher le tableau grace a sortieLatex vous avez besoin imperativement de ce fichier (SortieLatex.cpp)
Puis pour le convertissement d'un Tas ver un AB vous avez besoin lors de la compilation des fichier AB.cpp et .h
puis biensur tout les algo de tri et de construction du tas se trouve dans Tas.cpp

Vous trouverez plus d'information dans le main si dessous

Cordialement 
Michael OHAYON-ORDAZ

*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <stdlib.h>
#include "Tas.h"

using namespace std;

void afficherTab(int * T,int n); //affiche le tableau T de taille n trié a partir d'un tas 

std::string* TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar);



std::string * TikzRecursAB(int ligne,int gauche, int droite, int numeroPere, int typeFils, AB Ar){
  std::ostringstream ossnum, osslign,osscol,ossnumPere, ossbal, ossnum2Pere,ossnumRac;

      std::string stres("");

      if (Ar) {
    ossnumPere<<numeroPere;
    ossnumRac<<Ar->racine;
    if (Ar->Pere )ossnum2Pere<<Ar->Pere->racine; else ossnum2Pere<<0;
    int numero;
    if (typeFils==-1) numero=1; else numero= 2*numeroPere + typeFils;
    ossnum<<numero;
    osslign<<ligne;
    int mil = (gauche + droite)/2;
    osscol<<mil;

    stres="\\node[draw] (SZ" + ossnum.str() + ") at (" + osscol.str() + ", " + osslign.str() + ") { " + ossnumRac.str() + "};\n";

    if (typeFils!=-1) stres+="\\draw (SZ"+ossnumPere.str()+") -- (SZ"+ossnum.str() +");\n";
     if (!(Ar->SAG==NULL)) stres+=*TikzRecursAB(ligne -1 ,gauche,mil-1, numero,0,Ar->SAG);
    if (!(Ar->SAD==NULL)) stres+=*TikzRecursAB(ligne - 1,mil+1,droite, numero,1,Ar->SAD);
 }
  return new std::string(stres);
}

std::string * TikzAB(AB Ar){
  return TikzRecursAB(1,1,10,1, -1,Ar);
}

 void SortieLatex(AB Ar)
 {
  std::ofstream fichier("fig.tex", std::ios::out | std::ios::trunc);
  std::string preamb ("\\documentclass[a4paper]{article} \n \\usepackage{tikz} \n \\begin{document} \n \\begin{tikzpicture}\n");
  std::cout<<preamb<<"\n";
  std::string post("\\end{tikzpicture}\n  \\end{document} \n");
  std::cout<<post<<"\n";
  std::cout<<*TikzAB(Ar)<<"\n";
  std::string   res1(preamb + *TikzAB(Ar));
  std::string   res(res1 + post);
  //std::cout<<res1<<"\n";
  fichier <<res<<"\n";
  fichier.close();
  std::system("pdflatex -interaction nonstopmode fig.tex; rm fig.log; rm fig.tex; rm fig.aux; open fig.pdf");
  return;
 }





int main()
{ 
  cout<<"T un arbreParfaitNonTas pour tester les fonction dans arbreParfait"<<endl;

  Tas T(3);
  
  T.AjouteSommetArbreParfait(5);
  T.AjouteSommetArbreParfait(4);
  T.AjouteSommetArbreParfait(3);
  T.AjouteSommetArbreParfait(7);
  T.AjouteSommetArbreParfait(8);
  T.AjouteSommetArbreParfait(1);
  T.AjouteSommetArbreParfait(2);


  afficherTas(T);
  
  cout<<endl;
  cout<<"filsG(0)="<<T.contenu[T.FilsGauche(0)]<<endl;
  cout<<"filsG(1)="<<T.contenu[T.FilsGauche(1)]<<endl;
  cout<<"filsG(2)="<<T.contenu[T.FilsGauche(2)]<<endl;
  cout<<"filsG(3)="<<T.contenu[T.FilsGauche(3)]<<endl; //petite erreur

    cout<<"filsD(0)="<<T.contenu[T.FilsDroit(0)]<<endl;
  cout<<"filsD(1)="<<T.contenu[T.FilsDroit(1)]<<endl;
  cout<<"filsD(2)="<<T.contenu[T.FilsDroit(2)]<<endl;
  cout<<"filsD(3)="<<T.contenu[T.FilsDroit(3)]<<endl;

  cout<<"Pere(1)="<<T.contenu[T.Pere(1)]<<endl;
  cout<<"Pere(2)="<<T.contenu[T.Pere(2)]<<endl;
  cout<<"Pere(3)="<<T.contenu[T.Pere(3)]<<endl;
  cout<<"Pere(4)="<<T.contenu[T.Pere(4)]<<endl;
  cout<<"Pere(5)="<<T.contenu[T.Pere(5)]<<endl;
  cout<<"Pere(6)="<<T.contenu[T.Pere(6)]<<endl;

  cout<<"FeuilleP(3)"<<T.FeuilleP(3) ? cout<<"oui"<<endl : cout<<"non"<<endl;
  cout<<"FeuilleP(4)"<<T.FeuilleP(4) ? cout<<"oui"<<endl : cout<<"non"<<endl;
  cout<<"FeuilleP(5)"<<T.FeuilleP(5) ? cout<<"oui"<<endl : cout<<"non"<<endl;
  cout<<"FeuilleP(6)"<<T.FeuilleP(6) ? cout<<"oui"<<endl : cout<<"non"<<endl;
  cout<<endl;
    cout<<"on suprime l'ind 4 puis et on applique Remonter:"<<endl;
    T.SupprimerTas(4);
    
    //cout<<"on remonte l'ind 6:"<<endl; //decomenter pour tester
    //T.Remonter(6);

    //cout<<"on descend l'ind 0:"<<endl; //decomenter pour tester
    //T.Descendre(0);

    afficherTas(T);

    cout<<endl;
    cout<<endl;
 
 cout<<"nouveau tas T1 construi a partir des fonction dans Tas (ajouterTas):"<<endl;
    Tas T1(3);

  T1.AjouterTas(5);
  T1.AjouterTas(4);
  T1.AjouterTas(3);
  T1.AjouterTas(7);
  T1.AjouterTas(8);
  T1.AjouterTas(1);
  T1.AjouterTas(2);

  afficherTas(T1);
    
    cout<<endl;

  AB tasArbre=T1.TasVersAB(0);
  
  //SortieLatex(tasArbre); //decomenter pour creer le fichier pdf representant l'arbre
                            //du tas T1
  
 cout<<endl;
 cout<<"le tableau trié a partir du tas T1:"<<endl;
  int n=T1.IndicePremierSommetLibre;
  int *tabTrie=triPartas(T1);
  
  afficherTab(tabTrie,n);
  
 
  cout<<endl;  
  

  return 0;
}


