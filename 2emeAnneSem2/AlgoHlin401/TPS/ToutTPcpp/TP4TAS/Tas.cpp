
#include <iomanip>      // std::setw
#include <math.h>       /* pow */

#include "Tas.h"
using namespace std;

ArbreParfait::ArbreParfait(int n)
{  hauteur=n;
   this->n=0;
   this->n=pow(2,n+1)-1;
  
  IndicePremierSommetLibre=0;
  
  contenu=new int[this->n];
 

  for(int i=0;i<this->n;i++)
  	{
  		contenu[i]=-1;
  		traite[i]=false;
  	}
}

void ArbreParfait::Echanger (indDTS ind1,indDTS ind2)
{	int temp;
	
	temp=contenu[ind1];
	contenu[ind1]=contenu[ind2];
	contenu[ind2]=temp;
	
	
}

int ArbreParfait::AjouteSommetArbreParfait(int S)
{
	if(IndicePremierSommetLibre<n)
	{
		contenu[IndicePremierSommetLibre]=S;
		traite[IndicePremierSommetLibre]=false;
		IndicePremierSommetLibre++;
		return 1;
	}

	else
		return -1;
}

bool ArbreParfait::SommetValide(indDTS ind)
{
	if(ind<IndicePremierSommetLibre)
	{
		return true;
	}

	else
	{
		return false;
	}
}

indDTS ArbreParfait::Racine()
{
	return 0;
}



bool ArbreParfait::FeuilleP(indDTS ind)
{
  if((ind<IndicePremierSommetLibre) && (FilsGauche(ind)==-1) && (FilsDroit(ind)==-1))
  	return true;

  else if (ind<IndicePremierSommetLibre)
  	return false;
  else
  {
  	cout<<"IndicePremierSommetLibre depacé dans FeuilleP"<<endl;
  	return -1;
  }
}

indDTS ArbreParfait::FilsGauche(indDTS ind)
{
	if(((2*ind)+1)<IndicePremierSommetLibre)
	{
		return ((2*ind)+1);
	}
	else 
	{
		cout<<"je renvois -1"<<endl;
		return -1;
	}

}

indDTS ArbreParfait::FilsDroit(indDTS ind)
{
	if(((2*ind)+2)<IndicePremierSommetLibre)
	{
		return ((2*ind)+2);
	}
	else 
	{
		return -1;
	}

}

indDTS ArbreParfait::min2Ind (indDTS FilsG,indDTS FilsD)
{
	if (FilsD==-1)
	{
		return FilsG;
	}

	else if(contenu[FilsG]>contenu[FilsD])
	{
		return FilsD;
	}
	
	else
	{
		return FilsG;
	}
}

indDTS ArbreParfait::Pere(indDTS ind)
{
	if(ind==0)
	{
		return -1;
	}

	else if(SommetValide(ind))
	{
		if(((int)((ind-1)/2))>=0)
		return ((int)((ind-1)/2));
		
	}

	else 
	{
		cout<<"depacement de IndicePremierSommetLibre dans Pere"<<endl;
		return -1;
	}
}

void ArbreParfait::SupprimerArbreParfait(indDTS ind)
{
	if(ind>0)
	{
	   contenu[ind]=contenu[IndicePremierSommetLibre-1];
	   IndicePremierSommetLibre--;
	}

	else 
		IndicePremierSommetLibre--;
}


Tas::Tas(int h) : ArbreParfait(h)
{
 	
}

void Tas::Remonter (indDTS ind)
{
	if(ind == 0)
	{

	}

	else 
	{
		if(contenu[ind]<contenu[Pere(ind)])
		{
			Echanger(ind,Pere(ind));
			Remonter(Pere(ind));
		}

	}

}

void Tas::Descendre(indDTS ind)
{
	if(FeuilleP(ind))
	{

	}
	else 
	{   indDTS FilsMin = min2Ind(FilsGauche(ind),FilsDroit(ind));
		if(contenu[ind]>contenu[FilsMin])
		{
			Echanger(ind,FilsMin);
			Descendre(FilsMin);
		}
	}
}

void Tas::SupprimerTas(indDTS ind)
{
	contenu[ind]=contenu[IndicePremierSommetLibre-1];
	Descendre(ind);
	Remonter(ind);
	IndicePremierSommetLibre--;
}

void Tas::AjouterTas(int s)
{
	AjouteSommetArbreParfait(s);
	Remonter(IndicePremierSommetLibre-1);

}

AB Tas::TasVersAB(indDTS ind)
{
	if(FeuilleP(ind))
	{ AB s=new Sommet(contenu[ind]);
		return s;
	}

	else 
	{
		AB r=new Sommet (contenu[ind]);
		r->getSAG()=TasVersAB(FilsGauche(ind));
		r->getSAD()=TasVersAB(FilsDroit(ind));
		return r;

	}
}

void afficherTas(Tas T)
{
 for(int i=0;i<T.IndicePremierSommetLibre;i++)
  {
    cout<<setw(3)<<i;
  }
 cout<<endl;
 for(int i=0;i<T.IndicePremierSommetLibre;i++)
  {
    cout<<setw(3)<<T.contenu[i];
  }

}

void afficherTab(int * T,int n) //definition de la fonctions dans SortieLatex.cpp a cause de probleme techniques
{
	for(int i=0;i<n;i++)
  	{
    	cout<<setw(3)<<i;
  	}	
 	cout<<endl;

 	for(int i=0;i<n;i++)
  	{
   	    cout<<setw(3)<<T[i];
  	}


}


int* triPartas(Tas &T)
{   int i=0;
	indDTS IndPSL=T.IndicePremierSommetLibre; 
	int * trie=new int [IndPSL];
	

	while (IndPSL>0)
	{   
		trie[i]=T.contenu[0];
		T.SupprimerTas(0);
		i++;
		IndPSL--;

		
	}
	

	return trie;
}

