#include "progListeSC.h"
#include "progListeNC.h"
#include <string.h>

/*

Bonjour

Je vous joins progListeNC.cpp ___.h 
              progListeSC.cpp ___.h

 car j'ai cru que pour créer une fonction de complexité 2^n et 3^n il fallait contruire 
 un arbre binaire et ternaire puis les parcourir, ce qui n'est pas entierement faux
 puisqu'on a bien un parcour de complexité enoncé sauf que pour reussir cela, il m'a fallu
 construire une structure d'arborenscence Naire qui se trouve dans progListeNC.cpp .h 
 avec l'aide dans quelques algorithmes des liste SC (obtenue le semestre dernier) qui me sert 
 principalement uniquement a afficher toute les chemins de mes arbre Naire. 
 Le principe est simple dans progListeNC.cpp vous pourrai apprecier le fonctionnement des tableau de pointeurs, où chaque
 pointeurs pointent vers une entitée de type progListeNC. Ces tableau sont eux memes stocké
 dans des classe progListeNC.

Cordialement 
Michael OHAYON-ORDAZ

*/


void f1(int n)//complexité O(n)
{
	for (int i=1;i<n;i++)
	{
		n*=n;
	}
}

void f3(int n)//complexité O(n^3)
{int x=0;
 for (int i=1;i<n;i++)
	{
     for (int j=1;j<n;j++)
     	{
    	 for (int k=1;k<n;k++)
     		{
     			x+=1;
     		}
     	}
    }
}

int g2(ProgListeNC *LNC) //complexité O(2^n) calcul le nombre de noeuds contenue dans un arbre binaire.
{
    if(LNC==NULL)
    { 
        return 0;
    }
    
    else if (LNC->estFeuille())
    {   
        return 1;
    }

    else 
    {   
        return 1 + g2(LNC->fils(0)) + g2(LNC->fils(1));
    }
}

 int g3(ProgListeNC *LNC) //complexité O(3^n) calcul le nombre de noeuds contenue dans un arbre ternaire.
{   
    if(LNC==NULL)
    { 
        return 0;
    }
    
    else if (LNC->estFeuille())
    {   
        return 1;
    }

    else 
    {   
        return 1 + g3(LNC->fils(0)) + g3(LNC->fils(1)) +g3(LNC->fils(2));
    }
}

void afficheANC(ProgListeNC *LNC,string S) //affiche tout les chemin possible de la racine vers toutes les feuilles avec LNC la racine
{
    if (LNC->estFeuille())
    {  
        S+=LNC->getInfo();
        cout<<" "<<S<<endl;
    }

    else 
    {

        S+=LNC->getInfo();

       for(int i=0;i<LNC->nbFils();i++)
        { 
            afficheANC(LNC->fils(i),S);
        }
    }
}

void creerArbre(ProgListeNC *LNC,int n)  //a partir d'une racine LNC, crée un arbre ternaire de profondeur n;
{
    if(n==1)
    {
        return;
    }

    else 
    {   
        ProgListeNC *LNC1=new ProgListeNC();
        ProgListeNC *LNC2=new ProgListeNC();
        ProgListeNC *LNC3=new ProgListeNC();

        LNC->ajouteFils(LNC1);
        LNC->ajouteFils(LNC2);
        LNC->ajouteFils(LNC3);

        creerArbre(LNC->fils(0),n-1);
        creerArbre(LNC->fils(1),n-1);
        creerArbre(LNC->fils(2),n-1);
    }
    

    
}


int main (int argc,char **argv)
{ 
  
  string S="";
    ProgListeNC *LNC0=new ProgListeNC('0');
    ProgListeNC *LNC1=new ProgListeNC('1');
    ProgListeNC *LNC2=new ProgListeNC('2');
    ProgListeNC *LNC3=new ProgListeNC('3');
    ProgListeNC *LNC4=new ProgListeNC('4');
    ProgListeNC *LNC5=new ProgListeNC('5');
    ProgListeNC *LNC6=new ProgListeNC('6');
    ProgListeNC *LNC7=new ProgListeNC('7');
    ProgListeNC *LNC8=new ProgListeNC('8');
    ProgListeNC *LNC9=new ProgListeNC('9');
    ProgListeNC *LNC10=new ProgListeNC('A');
    ProgListeNC *LNC11=new ProgListeNC('B');
    ProgListeNC *LNC12=new ProgListeNC('C');
    ProgListeNC *LNC13=new ProgListeNC('D');
    ProgListeNC *LNC14=new ProgListeNC('E');
    ProgListeNC *LNC15=new ProgListeNC('F');
    ProgListeNC *LNC16=new ProgListeNC('G');
    ProgListeNC *LNC17=new ProgListeNC('H');
    ProgListeNC *LNC18=new ProgListeNC('I');
    ProgListeNC *LNC19=new ProgListeNC('J');
    ProgListeNC *LNC20=new ProgListeNC('K');
    ProgListeNC *LNC21=new ProgListeNC('L');
    ProgListeNC *LNC22=new ProgListeNC('M');
   //creation d'arbre binaire 

    LNC1->ajouteFils(LNC2);
    LNC1->ajouteFils(LNC3);

    LNC2->ajouteFils(LNC4);
    LNC2->ajouteFils(LNC5);
    
    LNC3->ajouteFils(LNC6);
    LNC3->ajouteFils(LNC7);
    
    LNC4->ajouteFils(LNC8);
    LNC4->ajouteFils(LNC9);

    LNC5->ajouteFils(LNC10);
    LNC5->ajouteFils(LNC11);

    LNC6->ajouteFils(LNC12);
    LNC6->ajouteFils(LNC13);

    LNC7->ajouteFils(LNC14);
    LNC7->ajouteFils(LNC15);

    int nbNoeudArbreBin=g2(LNC1);

    cout<<"nbNoeudArbreBin="<<nbNoeudArbreBin<<endl;

    //creation d'arbre ternaire en ajoutant une branche a chaque noeud l'arbre crée au dessus

    LNC1->ajouteFils(LNC16);
    
    LNC2->ajouteFils(LNC17);

    LNC3->ajouteFils(LNC18);

    LNC4->ajouteFils(LNC19);

    LNC5->ajouteFils(LNC20);

    LNC6->ajouteFils(LNC21);

    LNC7->ajouteFils(LNC22);


    int nbNoeudArbreTern=g3(LNC1);

    cout<<"nbNoeudArbreTern="<<nbNoeudArbreTern<<endl;
    

     /*temps de chaques fonctions*/
    

    int n;
    clock_t t1,t2,t3,t4,t5;
    
    cout<<"rentré n pour l'execution de l'algo f1(n) ou n est le nombre d'iteration de f1"<<endl;
    cin>>n;
    t1 = clock();
    f1(n);
    t2 = clock();
    cout << "Temps f1(" << n << "): " <<(double) (t2-t1)/CLOCKS_PER_SEC << endl;

    cout<<"rentré n pour l'execution de l'algo f3(n) ou n^3 est le nombre d'iteration de f3"<<endl;
    cin>>n;
    t2 = clock();
    f3(n);
    t3 = clock();
    cout << "Temps f3(" << n << "): " <<(double) (t3-t2)/CLOCKS_PER_SEC << endl;
   
    cout<<"rentré n pour l'execution de l'algo g2(n) ou 2^n est le nombre d'appel recursif de g2"<<endl;
    cin>>n;
    creerArbre(LNC0,n);  //a partir d'une racine LNC, crée un arbre ternaire de profondeur n;
    t3 = clock();
    g2(LNC0);
    t4 = clock();
    cout << "Temps g2(" << n << "): " <<(double) (t4-t3)/CLOCKS_PER_SEC << endl;  
    
    cout<<"rentré n pour l'execution de l'algo g3(n) ou 3^n est le nombre d'appel recursif de g3"<<endl;
    cin>>n;
    creerArbre(LNC0,n);   //a partir d'une racine LNC, crée un arbre ternaire de profondeur n;
    t4 = clock();
    g3(LNC0);
    t5 = clock();
    cout << "Temps g3(" << n << "): " <<(double) (t5-t4)/CLOCKS_PER_SEC << endl;  


    /*Question 1*/

    /* 1. programme ecrit a partir de la ligne 8 jusqu'a la ligne 64

       2.On constate apres de nombreux teste qu'on sens une diference entre f1(n) et f3(n) avec n>=150;
                                                                      entre f3(n) et g2(n) avec n>=25;
                                                                      entre g2(n) et g3(n) avec n>=16;

       3. Il n'aura pas de diference significative entre l'exec de f3(n) et l'exec de f3(n) et f1(n) succesivement car:
        O(f3(n))+f1(n)) ∈ O(f3(n))
        car lim n->inf de (n^3+n)/(n^3) = 1; 

        Il n'aura pas de diference significative entre l'exec de g2(n) et l'exec de f3(n) et g2(n) succesivement car:
        O(f3(n)+g2(n)) ∈ O(g2(n))
        
        Il n'aura pas de diference significative entre l'exec de g3(n) et l'exec de g2(n) et g3(n) succesivement car:
        O(g3(n)+g2(n)) ∈ O(g3(n))

    */

   


    return 0;

}