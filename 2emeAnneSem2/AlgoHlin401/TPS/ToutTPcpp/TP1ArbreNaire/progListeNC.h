#ifndef PROGLISTENC_H
#define PROGLISTENC_H
#include <iostream>


using namespace std;

class ProgListeNC {

	private:
		char info;
		//vector<ProgListeNC> succ;
		ProgListeNC **succp=NULL;
		int nbFilsIC;
		int caseOcc;
		int caseTot;

	public:
		
		void ajouteFils(ProgListeNC *LNCf); //ajout un fils à l"instance courrante.
		char getInfo();
		bool estFeuille(); //vrai si l'instace courrante est une feuille, faux sinon.
		ProgListeNC *fils(int i); //renvoi le ieme fils si il existe de l'instance courante
		ProgListeNC(char S); //ajoute l'info a l'insance courante crée 
		ProgListeNC(); //ajoute % a l'info de l'instance courante crée
		int nbFils();//renvois le nombre de fils de l'instance courante
		
};
#endif 