#include "Arbo.h"

/*
La majorité des fonctions, pile et files inclues on été modifié, c'est pour cela que je vous joint le .h 

Cordialement

Michael Ohayon-Ordaz


*/
/******* Liste doublement chainee Début *******/

//contCellule = Sommet*

Cellule::Cellule (ContCellule A){
  fils=A;
  Apres=NULL;
  Avant=NULL;
}




ListeCellules Cellule::EstDansListeP(ContCellule A){
  if (fils==A) return this;
  if (Apres==NULL) return NULL;
  return Apres->EstDansListeP(A);
}




ListeCellules Cellule::RetirerSuccesseur(ContCellule A)
{
  if (fils==A) 
    {
      if(Apres!=NULL)
      {
        Apres->Avant=Avant;
      }
      return Apres;
    }

  if (!Apres) 
    return this;

  Apres=Apres->RetirerSuccesseur(A); 
  return this;
}




/******* Liste doublement chainee Fin *******/


/************Arborescence Debut*************/


Sommet::Sommet(Valeur v)
{
  racine=v;
   ListeSuccesseurs=NULL;  // à completer et decommenter
}





ListeCellules Sommet::EstSuccesseurP(Arbo A)
{
  if (ListeSuccesseurs) 
  return ListeSuccesseurs->EstDansListeP(A);  // à completer et decommenter
  
  return NULL;
}




Cellule* Sommet::DernierEl()
{
 if(ListeSuccesseurs==NULL)
  { 
    return NULL;    
  }
  
 else
 {
    ListeCellules L=ListeSuccesseurs;
    
    while(L->Apres!=NULL)
    {
      L=L->Apres;

    }

    return L;
  }
}




void Sommet::AjouterSuccesseur(Arbo A)
{
  if (ListeSuccesseurs==NULL)
  {
    ListeSuccesseurs=new Cellule(A);
    
    return;
  }
  else if ( ListeSuccesseurs->EstDansListeP(A)==NULL)
  {
  

    ListeCellules ptrC=DernierEl();
     ptrC->Apres=new Cellule(A);
     ptrC->Apres->Avant=ptrC;
    return;
   
  }
  
  return;
}



void Sommet::RetirerSuccesseur(Arbo A){
  if (ListeSuccesseurs &&  ListeSuccesseurs->EstDansListeP(A)) 
  {
    ListeSuccesseurs= ListeSuccesseurs->RetirerSuccesseur(A);  // à completer et decommenter
  }

}

ostream& operator<<(ostream& os, Sommet& S){
  os<<S.racine<<" ";
  return os;
}

/************Arborescence Fin*************/

/************Traversee recursive Debut*************/

void TraverseePrefixeRec(Arbo A)
{
  if (!A) 
   {cout<<endl; return;};
     cout<< *A<<" ";
    
  for (ListeCellules L=A->ListeSuccesseurs; L!=NULL;L=L->Apres)   // à completer et decommenter
      TraverseePrefixeRec(L->fils);

  return;
}

/************Traversee recursive Fin*************/

/**********Pile Début*********/

Pile::Pile(){
  Sommet=NULL;
}


bool Pile::VideP(){
  return Sommet==NULL;
}


void Pile::Empiler(ContCellule A){
  Cellule* ptCellule=new Cellule(A);
  ptCellule->Apres=Sommet;
  Sommet=ptCellule;
  
  return;
}

void affichePile (Pile &P)
{ 
  for (ListeCellules L=P.Sommet; L!=NULL;L=L->Apres) 
    {
      cout<<*L->fils<<" ";
    }
  
}

ContCellule Pile::Depiler(){
  Cellule* ptCellule=Sommet;
  Sommet=Sommet->Apres;
  return ptCellule->fils;
}

/**********Pile Fin*********/

/************Traversee  prefixe iterative Debut*************/

void TraverseePrefixeIt(Arbo A){
  Pile* P=new Pile;
  P->Empiler(A);

  Arbo Inter;
 
  while(!(P->VideP())) // à completer et decommenter
  { 
    Inter=P->Depiler();
    cout<<*Inter<<" ";
    
    for (ListeCellules L=Inter->DernierEl(); L!=NULL;L=L->Avant) 
    { 
      P->Empiler(L->fils);
     
    }

  }
}

/************Traversee  prefixe iterative Fin*************/


/**********File Début*********/

File::File(){
  Sortie=NULL; Entree=NULL;
}


bool File::VideF(){
  return Sortie==NULL;
}


void File::Enfiler(ContCellule A){
  Cellule* ptCellule=new Cellule(A);
  if (Entree) Entree->Apres=ptCellule;
  Entree=ptCellule;
  if (! Sortie) Sortie=ptCellule;
  return;
}


ContCellule File::Defiler(){
  Cellule* ptCellule=Sortie;
  Sortie=Sortie->Apres;
  return ptCellule->fils;
}


void afficheFile (File &F)
{ cout<<"file=";
  for (ListeCellules L=F.Entree; L!=NULL;L=L->Apres) 
    {
      cout<<*L->fils<<" ";
    }
  cout<<";"<<endl;
}


/**********File Fin*********/

/************Traversee Largeur Debut*************/

void TraverseeLargeur(Arbo A)
{
  if(A->ListeSuccesseurs==NULL)
  {
    cout<<*A;
  }

  else 
  {
   File F;

    F.Enfiler(A);
    while (!F.VideF())
    { Sommet* Inter=F.Defiler();

      cout<<" "<<*Inter;

      for (ListeCellules L=Inter->ListeSuccesseurs; L!=NULL;L=L->Apres)   // à completer et decommenter
      { //cout<<"je boucle"<<endl;
        F.Enfiler(L->fils);
        //afficheFile(F);
      }
 
    }
  }
 }  // à completer

/************Traversee Largeur Fin*************/

void Affiche(Sommet* A)
{
  if (A->ListeSuccesseurs==NULL)
  {  //  a ajouter pour finir!
    cout<<" racine:"<<A->racine;
    cout<<" nPre:"<<A->nPr;
    cout<<" nPo:"<<A->nPo;
    cout<<" | ";
    cout<<endl;
    return;
  }

  else
  { cout<<" racine:"<<A->racine<<" ";
    cout<<"nPre:"<<A->nPr;
    cout<<" nPo:"<<A->nPo;
    cout<<" | ";
    cout<<endl;
    Cellule* pC; 

    pC=A->ListeSuccesseurs;
    while (pC!=NULL)
    {
      Affiche(pC->fils);
      pC=pC->Apres;

    }
    return;
  }
   
}

/******parcour numerotation Prefixe et postfixe********/

int Sommet::cardinalArbo()
{
  if(ListeSuccesseurs==NULL)
  {
    return 1;
  }
  else 
  {
    int card=1;
    for (ListeCellules L=ListeSuccesseurs; L!=NULL;L=L->Apres) 
    {
      card+=L->fils->cardinalArbo();
    }

    return card;

  }
}

int Sommet::parcournPrePost(int nPref,int nPost)
{
  if(ListeSuccesseurs==NULL)
  {
    nPr=nPref;
    nPo=nPost;
    return nPo;
  }
  else
  {
    nPr=nPref;
    int nFreAime=0; //numero n tu du frere Aimé du sommet courrant
    for (ListeCellules L=ListeSuccesseurs; L!=NULL;L=L->Apres) 
    {
      if (L->Avant!=NULL)
      {
        nFreAime=L->Avant->fils->cardinalArbo();
        nPref=nFreAime+(L->Avant->fils->nPr);
        nPost=L->fils->parcournPrePost(nPref,nPost)+1; //on implemente l'appel recursif vue en cour
      }

      else
      {
        nPost=L->fils->parcournPrePost(nPref+1,nPost)+1;
      }
    }
    nPo=nPost;
    return nPo;
  }

}

int main(){

 
  Arbo A0 = new Sommet(0);
  Arbo A1 = new Sommet(1);
  Arbo A2 = new Sommet(2);
  Arbo A3 = new Sommet(3);
  Arbo A4 = new Sommet(4);
  Arbo A5 = new Sommet(5);
  Arbo A6 = new Sommet(6);

  A3->AjouterSuccesseur(A6);
  A1->AjouterSuccesseur(A5);
  A3->AjouterSuccesseur(A4);
  A2->AjouterSuccesseur(A3);
  A0->AjouterSuccesseur(A2);
  A0->AjouterSuccesseur(A1);
  
  /*if (A4->ListeSuccesseurs!=NULL)
  for (ListeCellules L=A4->ListeSuccesseurs->DernierEl(); L!=NULL;L=L->Avant) 
       cout<<"je marche";*/
      //cout<<*L->fils<<" ";    
    //Affiche(A3);

//TraverseePrefixeRec(A0);
 cout<<endl;
//TraverseePrefixeIt(A0);
 

//TraverseeLargeur(A0);


cout<<" recPrefixe A0  ";
TraverseePrefixeRec(A0);
cout<< endl;


cout<<" iterPrefixe A0  ";
TraverseePrefixeIt(A0);
cout<< endl;

cout<<" iterlargeur A0 ";
TraverseeLargeur(A0);
cout<< endl;

cout<<" parcours + Numerotation Prefixe et Postfixe2";
cout<< endl;
A0->parcournPrePost(1,1);
Affiche(A0);
cout<<endl;
cout<<endl;
 


  A0->RetirerSuccesseur(A1);
 
 cout<<" apres retrait: "<<endl;
 cout<<"rec A0 ";
TraverseePrefixeRec(A0);
cout<< endl;
cout<<" iter A0  ";
TraverseePrefixeIt(A0);
cout<< endl;
cout<<" parcours+ Numerotation Prefixe et Postfixe2";
cout<< endl;
A0->parcournPrePost(1,1);
Affiche(A0);
cout<<endl;
cout<<endl;


  return 1;
}
