


(require racket/set)
;(define F1 '(^ (v p q) p))
(define N1 '(^ a (^ b c)))
(define N2 '(^ a (^ a (v a (^ b (-> a (<-> b a )))))))
;(define F2 '(<-> (^ a b) (! (v a b) )))
(define N3 '(! a))


(define estConsLogique? (lambda (f)
                          (cond [(or (estTop? f) (estBot? f)) #t]
                                [#t #f]
                           )
                         )
  )

(define estTop? (lambda (f)
      (cond [(equal? f '$T) #t]
            [#t #f]
       )
 )
)
  
(define estBot? (lambda (f)
      (cond [(equal? f '$B) #t]
            [#t #f]
       )
 )
)


(define (estSymboleProp? f)
        (cond [(and (symbol? f) (not (equal? f '())) (not (equal? f '!)) (not (equal? f '^)) (not (equal? f 'v)) (not (equal? f '->)) (not (equal? f '<->)) (not (equal? f '$T)) (not (equal? f '$B))) #t]
              [#t #f]
         )
 )

(define estNon? (lambda (f)
     (equal? f '! )))

(define estConBin? (lambda (f)
                (or (equal? f '^) (equal? f 'v) (equal? f '->) (equal? f'<->))
                      
         )
  )
  

(define P '( ^ (v (p p ->))))

(define estFbf? (lambda (lista)
                 (cond 
                       [
                        (estSymboleProp? lista) 
                          #t
                       ]
                       [
                        (estConsLogique? lista)
                           #t
                       ]
                       [
                        (and (= (length lista) 3) (estConBin? (car lista)) ) 
                            (and (estFbf? (cadr lista)) (estFbf? (caddr lista)))
                       ]
                       [
                        (and (= (length lista) 2) (estNon? (car lista)) ) 
                          (and (estFbf? (cadr lista)))
                       ] 
                       [#t #f]
                       
                 )
                )
  )

(define conRac (lambda (f)
                 (if (and (not (estSymboleProp? f)) (not (estConsLogique? f))) 
                   (cond [(estConBin? (car f)) (car f)]
                         [(estNon? (car f)) (car f)]
                         
                         [#t display "erreur conRac"]
                         
                    )
                   (display "erreur dans conRac")
                   )
     )
)
  
(define filsN (lambda (f)
               (cond [(estFbf? f) (cadr f)]
                     [#t (display "erreur dans filsN")
                         (display f) 
                     ]
               )
     )
  )

 
(define filsG (lambda (f)
                (cond [(estFbf? f) (cond [(estConBin? (car f)) (cadr f)])]
                     [#t (display "erreur dans filsG")]
               )
              )
 )

(define filsD (lambda (f)
                (cond [(estFbf? f) (cond [(estConBin? (car f)) (caddr f)])]
                     [#t (display "erreur dans filsD")]
               )
              )
 )

(define negRacFbf? (lambda (f)
                     
                    (cond [(estFbf? f) 
                              (cond [(equal? (conRac f) '!) #t]
                                    [#t #f]
                              )
                          ]
                          [#t (display "erreur dans negRacFbf une no fbf est passé en param")
                           ]
                    )
              )
 )

(define nbc-init (lambda (f)
                   (cond [(estFbf? f)(nbc-init f)]
                         [#t display "votre f n'est pas une fbf"]
                    )
                   )
  )



(define nbc (lambda (f)
               (cond [(estSymboleProp? f) 
                           0]
                     [(and (estConBin? (conRac f))  (= (length f) 3)) 
                                  (+ 1 (nbc (cadr f))) 
                                  (+ 1 (nbc (caddr f)))]
                     [(and (negRacFbf? f) (= (length f) 2))
                          (+ 1 (nbc cadr))
                      ]
               )
              )
  )

;(define max (lambda (x y)
;              (cond [(>= x y)(x)]
;                    [#t y]
;               )
;              )
;  )
              
(define prof-init (lambda (f)
                   (cond [(estFbf? f) (prof-init f)]
                         [#t display "votre f n'est pas une fbf"]
                    )
                   )
  ) 

(define prof (lambda (f)
               (cond [(estSymboleProp? f) 0]
                     [(estConBin? (car f))
                         (max (+ 1 (prof (cadr f))) (+ 1 (prof (caddr f))))]
                     [(negRacFbf? f) 
                         (+ 1 (prof (cadr f)))]
               )
              )
  )
 
(define inclue (lambda (x l)
  ( cond [(equal? l '())
              #f]
         [(equal? (car l) x)
              #t]
         [#t (inclue x (cdr l))]
   )
  )
)

#|(define set-union (lambda (l1 l2)
                    (
                     cond [(equal? l2 '())
                               l1]
                          
                          [(and (symbol? l2) (inclue l2 l1)) 
                               l1]
                        
                          [(and (symbol? l2) (not (inclue l2 l1))) 
                               (append l1 l2)]
                          
                          [(inclue (car l2) l1) 
                                  (set-union l1 (cdr l2))]
                          
                          [#t 
                              (set-union (append l1 (car l2)) (cdr l2))
                                                         ]
                     )
                    )
  
  ) |#

(define ensSP (lambda (f)
                (cond [(estSymboleProp? f) (set f)]
                      [(estConsLogique? f) (set ) ]
                      [(null? f ) (set )]
                      [#t 
                         (begin
                           (cond [(negRacFbf? f) (set-union (ensSP (filsN f)) (set ) )]
                                 [#t (set-union (ensSP (filsG f)) (ensSP (filsD f)))]
                           )
                         
                          )
                       ]
              
             )
  )
)


;Q6

(define affiche (lambda (f)
                  (cond [(estFbf? f)(affiche-aux f)]
                        [#t (display "formule non bien formée")]
                   )
                 )
  )


(define affiche-aux (lambda (f)
                   
                    (cond [(estSymboleProp? f) 
                              (begin 
                                (display  f)
                               )
                          ]
                          [(= (length f) 3) 
                                 (begin   
                                   (display "(")
                                   (affiche-aux (cadr f))
                                   (display (car f))
                                   (affiche-aux (caddr f))
                                   (display ")")
                                 )
                          ]
                          
                          [(= (length f) 2)
                           (display "(")
                           (display (car f))
                           (affiche-aux (cadr f))
                           (display ")")
                          ]
                       )
                )
  )
             
;Q7
                    
(define I1 '((a . 1) (b . 0) (c . 1) (d . 1)))
(define I2 '((a . 0) (b . 0) (c . 0) (d . 0)))
(define I3 '((a . 1) (b . 1) (c . 1) (d . 1)))

                         

(define intSymb(lambda (s I)
                 (cond 
                       [(integer? s) (= s 1)]
                       [(boolean? s) (equal? s #t)]
                       [(equal? (caar I) s)
                           (= (cdar I) 1) ]
                       [#t (intSymb s (cdr I))]
                        
                       )
                 )
  )         
                

;Q8

(define intAnd (lambda (s1 s2 I)
                 (and (intSymb s1 I) (intSymb s2 I))
                                             
                       
                 )
 )

(define intOr (lambda (s1 s2 I)
                 (or (intSymb s1 I) (intSymb s2 I))
                                             
                       
                 )
 )

(define intNeg (lambda (s I)
                 (not (intSymb s I))
                 )
  )
  
(define intImp (lambda (s1 s2 I)
                 (intOr (intNeg s1 I) (intSymb s2 I) I)
               )
 )
 
(define intEqu (lambda (s1 s2 I)
                 (and (intImp s1 s2 I) (intImp s2 s1 I))
                )
  )

(define intTop #t)
(define intBot #f)
                 

;Q10


(define valV (lambda (fbf I)
               (cond [(estSymboleProp? fbf) (intSymb fbf I)]
                     [(estConsLogique? fbf) 
                          (cond [(estTop? fbf)
                                      intTop]
                                [(estBot? fbf)
                                      intBot]
                          )
                     ]
                    [(estConBin? (conRac fbf))
                          (cond [(equal? (conRac fbf) '^)
                                    (intAnd (valV (cadr fbf) I) (valV (caddr fbf) I) I)
                                ]            
                                [(equal? (conRac fbf) 'v)
                                    (intOr (valV (cadr fbf) I) (valV (caddr fbf) I) I)
                                ]
                                [(equal? (conRac fbf) '->)
                                    (intImp (valV (cadr fbf) I) (valV (caddr fbf) I) I)
                                 ]
                                [(equal? (conRac fbf) '<->)
                                    (intEqu (valV (cadr fbf) I) (valV (caddr fbf) I) I)
                                ]
                                [#t (display "erreur dans conbin valF")]
                           )
                     ]
                     [(estNon? (conRac fbf))
                          (intNeg (valV (cadr fbf) I) I)
                     ]
                  )
            )
 )
  
(define F1 '(<-> (^ a b) (v (! a) b)))
(define F2 '(v (! (^ a (! b))) (! (-> a b))))
(define F3 '(^ (! (-> a (v a b))) (! (! (^ a (v b (! c)))))))
(define F4 '(^ (^ (^ (^ (^ (v (v (! a) b) c)
                           (v (! d) c))
                        (v c a))
                     (v (! c) b))
                  (v (! c) (! b)))
               (v (! b) d)))

(define modele? (lambda (fbf I)
                  (cond [(equal? (valV fbf I) #t) #t ]
                        [#t #f]
                   )
                  )
  )

;Q13
(define ajoute1bin (lambda (LchifBin)
                     (ajoute1bin-aux LchifBin)
                     )
  )

(define ajoute1bin-aux (lambda (LchifBin) ;Liste representant un chiffre binaire
                     (cond  [(null? LchifBin) LchifBin]
                            [(= (car LchifBin) 0) (append '(1) (cdr LchifBin))]
                            [(= (car LchifBin) 1) (append '(0) (ajoute1bin (cdr LchifBin)))]
                     )
                   )
 )

(define toutcBin1 (lambda (LchifBin) ;verifie si toute les elements de la liste binaire est a 1
                      (cond  [(null? LchifBin) #t]
                             [(= (car LchifBin) 1) (toutcBin1 (cdr LchifBin))]
                             [#t #f]
                        
                        )
                   )
  )
(define consList0 (lambda (n list0)
                    (cond [(= n 0) list0]
                          [#t (consList0 (- n 1) (cons 0 list0))] 
                       )
   )
 )



(define consPair (lambda (Lsp LchifBin lren) 
                  
                   (cond [(null? Lsp) lren]
                         [#t 
                           (begin 
                            
                            
                            (consPair (cdr Lsp) (cdr LchifBin) (append lren (list (cons (car Lsp) (car LchifBin) ))))
                           ) 
                          ]
                         ) 
                   )
)
                   
(define ensInt (lambda (Lsp ) ;;tout  les valeur de veritée posibles a partir d'une liste de symboles prop
                              ;;sous la forme de ( ((a . 1) (b . 1)  .. (z . 1)) ....) ou chaque liste contenant des paires correspond a une 
                              ;;interpretacion
              
       (begin         
          
           (ensInt-aux Lsp (consList0 (length Lsp) '()) '())
           
           
         
       )
   )
)
  
(define ensInt-aux (lambda (Lsp Lchifbin lresfinal)
           (cond [(toutcBin1 Lchifbin) (cons (consPair Lsp Lchifbin '()) lresfinal)]
                 [#t 
                  (ensInt-aux Lsp (ajoute1bin Lchifbin) (cons (consPair Lsp Lchifbin '()) lresfinal))]
           )
    )
)

;Q14

(define satisfiable? (lambda (fbf )
                       (let* [(Lsp (set->list (ensSP fbf))) (LIpos (ensInt Lsp))] ;;LIpos liste de liste d'interpretations possibles
                         (satisfiable-aux LIpos fbf )                             ;grace a la fonction ensInt
                        )
                       )
  )

(define satisfiable-aux (lambda (LIpos fbf )
                          (cond [(null? LIpos) #f]
                                [(valV fbf (car LIpos)) #t]
                                [#t (satisfiable-aux (cdr LIpos) fbf)]
                           )
                          )
  )

;Q15

(define valide? (lambda (fbf )
                       (let* [(Lsp (set->list (ensSP fbf))) (LIpos (ensInt Lsp))] ;;LIpos liste de liste d'interpretations possibles
                         (valide-aux LIpos fbf)                             ;grace a la fonction ensInt
                        )
                       )
  )

(define valide-aux (lambda (LIpos fbf)
                          (cond [(null? LIpos) #t]
                                [(valV fbf (car LIpos)) (valide-aux (cdr LIpos) fbf)]
                                [#t #f]
                           )
                          )
  )

;Q16

(define insatisfiable? (lambda (fbf )
                       (let* [(Lsp (set->list (ensSP fbf))) (LIpos (ensInt Lsp))] ;;LIpos liste de liste d'interpretations possibles
                         (insatisfiable-aux LIpos fbf)                             ;grace a la fonction ensInt
                        )
                       )
  )

(define insatisfiable-aux (lambda (LIpos fbf)
                          (cond [(null? LIpos) #t]
                                [(valV fbf (car LIpos)) #f ]
                                [#t (insatisfiable-aux (cdr LIpos) fbf)]
                                
                           )
                          )
  )

;Q17

(define equivalent1? (lambda (fbf1 fbf2)
                      
                      (let* [(Lsp (set->list (set-union (ensSP fbf1) (ensSP fbf2)))) (LIpos (ensInt Lsp))]
                         
                           (equivalent1-aux fbf1 fbf2 LIpos)                       
                        )
                       )
)

(define equivalent1-aux (lambda (fbf1 fbf2 LIpos)
                          (cond [(null? LIpos) #t]
                                [(and (valV fbf1 (car LIpos)) (valV fbf2 (car LIpos))) 
                                    (equivalent1-aux fbf1 fbf2 (cdr LIpos))
                                ]
                                [ (and (not (valV fbf1 (car LIpos))) (not (valV fbf2 (car LIpos)))) 
                                    (equivalent1-aux fbf1 fbf2 (cdr LIpos))
                                ]
                                [#t #f]
                          )
                        )
  )


(define equivalent2? (lambda (fbf1 fbf2)
                      (valide?  (list '<-> fbf1 fbf2))
                      
       )
  )

;Q18
(define consequence2? (lambda (fbf1 fbf2)
                        (valide? (list '-> fbf1 fbf2))
                       )
  )
                         
;Q19
  
(define ensSPallFbf (lambda (lifbf)
                     (letrec [(ensSPallFbf-aux (lambda (lifbf lrenv)
                                             (cond [(null? lifbf) (set->list lrenv)]
                                                   [#t (ensSPallFbf-aux (cdr lifbf) (set-union  (ensSP (car lifbf)) lrenv))]
                      
                      
                                                   )
                                               
                                               )
                             
                                    )]
                  (ensSPallFbf-aux lifbf (set ))
              )
     )
)

;Q20

(define modeleCommun? (lambda (lifbf I)
                        (letrec [(modeleCommun-aux (lambda (lifbf I )
                                                     (cond [(null? lifbf) #t]
                                                           [(modele? (car lifbf) I) (modeleCommun-aux (cdr lifbf) I )]
                                                           [#t #f]
                                                      )


                                                   )
                                                    
                                )]
                          (modeleCommun-aux  lifbf I )
                         )
                        
                        )
  )

;Q21
(define contradictoire? (lambda (lifbf)
                          (letrec [(contradictoire-aux (lambda (lifbf liEtfbf) ;liEtfbf contient toutes les formules
                                                                              ;de lifbf concatené avec un ^
                                                                              ;autrement dit pour toute fbf Fx  appartenant a lifbf telque
                                                                               ;lifbf ={F1,F2,..Fn} liEtfbf sera = {F1 ^ F2 ^... Fn}
                                                                               ;si insatisifiable de liEtfbf alors #t sinon #f
                                                        
                                                        (cond [(null? lifbf ) 
                                                                 (begin  
                                                                    ;(display (car liEtfbf)
                                                                    (insatisfiable? (car liEtfbf))
                                                                  )      
                                                                 ]
                                                              ;[(= (length lifbf) 1) (cons '^ (cons (car lifbf) liEtfbf))]
                                                              [#t 
                                                                (contradictoire-aux (cdr lifbf) (list (cons '^ (cons (car lifbf) liEtfbf))))]
                                                       )  
                                                    )
                                  
                                                       )
                                  ]
                            (contradictoire-aux (cdr lifbf) (cons (car lifbf) '()))
                          )
                        )
  )
  
;Q22 

(define listfbf->liEtfbf (lambda (lifbf)   ;on formalise le passage de lifbf vers liEtfbf enoncé si dessus
                         (letrec [(listfbf->liEtfbf-aux 
                                     (lambda (lifbf liEtfbf) 
                                         (cond [(null? lifbf ) (car liEtfbf)]
                                                              
                                               [#t 
                                                  (listfbf->liEtfbf-aux (cdr lifbf) (list (cons '^ (cons (car lifbf) liEtfbf))))]
                                          )  
                                      )
                                   )
                                  ]
                            (listfbf->liEtfbf-aux (cdr lifbf) (cons (car lifbf) '()))
                          )
                        )
  )  
                         
   

;Q23 et Q24

(define consequence? (lambda (lifbf fbf) 
                       (let* [(liEtfbf (listfbf->liEtfbf lifbf)) (impfbf (list '-> liEtfbf fbf))]
                              (cond [(valide? impfbf) #t]
                                    [#t #f]
                               )
                         )
                       )
  )
              
(define consequenceI? (lambda (lifbf fbf)
                        (let* [(liEtfbf (listfbf->liEtfbf (cons (list '! fbf) lifbf)))] ;;liEtfbf devient la conjonction de 
                                                                                        ;;toute les elements de la liste lifbf qui sont des formule bien formée  avec la negations de fbf
                                                                                        ;;fbf etant une formulebf passée en parametre
                          
                          (insatisfiable? liEtfbf)
                        )
                       )
  )

;Q25
(define oteEqu (lambda (fbf)
                 (list '^ (list '-> (cadr fbf) (caddr fbf)) (list '-> (caddr fbf) (cadr fbf) ))

                 )
  )

;Q26
(define oteImp (lambda (fbf)
                  (list 'v (list '! (cadr fbf)) (caddr fbf))
                 )
  )


;Q27
(define oteCste(lambda (cst)
                  (cond [(estTop? cst)(list 'v '(! p) 'p )]
                        [(estBot? cst)(list '^ '(! p) 'p)]
                  )
                )
  )

;Q28
(define redNeg (lambda (fbf)
                 (cond [(estSymboleProp? (cadr fbf)) fbf]
                       [(estNon? (caadr fbf )) (cadadr fbf)]
                       [(equal? '^ (conRac (cadr fbf))) (list 'v (list '! (cadadr fbf)) (list '! (cadr (cdadr fbf))))]
                       [(equal? 'v (conRac (cadr fbf))) (list '^ (list '! (cadadr fbf)) (list '! (cadr (cdadr fbf))))]
                  )
                 )
  )

;Q29
(define distOu (lambda (fbf)
                (cond [(estSymboleProp? fbf) fbf]
                       [#t (cond  [(and (not (estSymboleProp?  (cadr fbf) )) (equal? (conRac (cadr fbf)) '^)) ; cas ou fbf = (v (^ A B) C)
                                       (list '^  (list 'v (caddr fbf) (cadadr fbf)) (list  'v (caddr fbf) (car (cddadr fbf))))
                                  ]
                                   
                                  [(and (not (estSymboleProp?  (caddr fbf) )) (equal? (conRac (caddr fbf)) '^)) ; cas ou fbf = (v C (^ A B))
                                       (list '^  (list 'v (cadr fbf) (cadr(caddr fbf))) (list  'v (cadr fbf) (cadr (cdaddr fbf))))
                                   ]
                                  [#t (display "erreur dans distou")] ; cas ou fbf = (v a b)
                            )
                         
                      ]
                       
                  )
                 )
  )


;Q30


(define formeConj (lambda (fbf)
                   (letrec [(formeConj-aux1 (lambda (fbf)  ;fbfrenvoyée sans Equivalent                     
                                             (cond 
                                                   [(estSymboleProp? fbf)
                                                        fbf
                                                    
                                                    ]
                                                   [ (estConsLogique? fbf)
                                                       fbf
                                                     ]
                                                   
                                                    
                                                   
                                                   [(equal? '<-> (conRac fbf)) 
                                                      (formeConj-aux1 (oteEqu fbf))
                                                   ]
                                                   [#t
                                                       (cond [
                                                              (= (length fbf) 3) 
                                                               (begin
                                                               (list
                                                                (conRac fbf)
                                                                (formeConj-aux1 (cadr fbf) ) 
                                                                (formeConj-aux1 (caddr fbf) )
                                                               )
                                                               )
                                                             ]
                                                             [(= (length fbf) 2)
                                                                (begin 
                                                                 (list 
                                                                  (conRac fbf)
                                                                  (formeConj-aux1 (cadr fbf) )
                                                                 
                                                                 )
                                                                )
                                                              
                                                             ]
                                                       
                                                        )
                                                  ]
                                               )
                                           )
                                    )
                            ]
                    (let* [(lSansEq (formeConj-aux1 fbf))]
                        (letrec [(formeConj-aux2 (lambda (lSansEq)
                                                   
                                                  (cond [(estSymboleProp? lSansEq) 
                                                          lSansEq 
                                                        ]
                                                        [ (estConsLogique? lSansEq)
                                                          (begin
                                                            
                                                          lSansEq)
                                                        ]
                                                        [
                                                         (equal? '-> (conRac lSansEq))
                                                            (formeConj-aux2 (oteImp lSansEq))
                                                        ]
                                                        [#t
                                                         (cond [
                                                                (= (length lSansEq) 3)
                                                               (begin
                                                                 (list 
                                                                  (conRac lSansEq)
                                                                  (formeConj-aux2 (cadr lSansEq))
                                                                  (formeConj-aux2 (caddr lSansEq))
                                                                  
                                                                  )
                                                                 )
                                                               ]
                                                               
                                                                [(= (length lSansEq) 2)
                                                                (begin 
                                                                 (list 
                                                                  (conRac lSansEq)
                                                                  (formeConj-aux2 (cadr lSansEq) )
                                                                 
                                                                 )
                                                                )
                                                              
                                                                ]
                                                    
                                                            )
                                                         ]
                                                   
                                                   )))]
                            (let* [(lSansImp (formeConj-aux2 lSansEq))] ;on aplique depuration des implique
                              (display lSansImp)
                               (letrec [(formeConj-aux3 (lambda (lSansImp)
                                                         (cond 
                                                   [(estSymboleProp? lSansImp)
                                                        lSansImp
                                                    
                                                    ]
                                                   [ (estConsLogique? lSansImp)
                                                      (oteCste lSansImp)
                                                     ]
                                                   
                                                    
                                                   
                                                   [#t
                                                       (cond [
                                                              (= (length lSansImp) 3) 
                                                               (begin
                                                               (list
                                                                (conRac lSansImp)
                                                                (formeConj-aux3 (cadr lSansImp) ) 
                                                                (formeConj-aux3 (caddr lSansImp) )
                                                               )
                                                               )
                                                             ]
                                                             [(= (length lSansImp) 2)
                                                                (begin 
                                                                 (list 
                                                                  (conRac lSansImp)
                                                                  (formeConj-aux3 (cadr lSansImp) )
                                                                 
                                                                 )
                                                                )
                                                              
                                                             ]
                                                       
                                                        )
                                                  ]
                                               ) 
                                                          
                                                          ))
                                                           
                                         
                                        ]
                                   
                                  (let* [(lSansCst (formeConj-aux3 lSansImp))]
                                     ;(display "\n") 
                                     ;(display "lSansCst")
                                     ;(display lSansCst) ;liste auquelle on a enlevée toute les constantes
                                     ;(display "\n")
                                     (letrec [(formeConj-aux4 (lambda (lSansCst) ;on commence a enlever les ! 
                                                              
                                               
                                                         (cond 
                                                           [(estSymboleProp? lSansCst)
                                                            lSansCst
                                                    
                                                            ]
                                                           [ (estConsLogique? lSansCst)
                                                              lSansCst
                                                           ]
                                                   
                                                          [
                                                            (and (equal? '! (conRac lSansCst)) (not (estSymboleProp? (cadr lSansCst))) )
                                                             (begin 
                                                               ;(display lSansCst)
                                                               ;(display "\n")
                                                             (formeConj-aux4 (redNeg lSansCst))
                                                             )
                                                           ]
                                                   
                                                           [#t
                                                            (cond [
                                                              (= (length lSansCst) 3) 
                                                               (begin
                                                               (list
                                                                (conRac lSansCst)
                                                                (formeConj-aux4 (cadr lSansCst) ) 
                                                                (formeConj-aux4 (caddr lSansCst) )
                                                               )
                                                               )
                                                             ]
                                                             [(= (length lSansCst) 2)
                                                                (begin 
                                                                 (list 
                                                                  (conRac lSansCst)
                                                                  (formeConj-aux4 (cadr lSansCst) )
                                                                 
                                                                 )
                                                                )
                                                              
                                                             ]
                                                       
                                                        )
                                                  ]
                                               ) 
                                                          
                                                          ))
                                                              
                                                              ]
                                         (let* [(lNonDist (formeConj-aux4 lSansCst))]
                                            ;(display "lNonDist")
                                            ;(display lNonDist)
                                            ;(display "\n")
                                           (letrec [(formeConj-aux5 (lambda (lNonDist ) ;on commence a dist les ou 
                                                                      
                                                                      
                                                                      (cond 
                                                                        [(estSymboleProp? lNonDist)
                                                                         lNonDist
                                                                         
                                                                         ]
                                                                        [ (estConsLogique? lNonDist)
                                                                          lNonDist
                                                                          ]
                                                                        
                                                                        [                              ;(v (^ r (! p)) (v (v q r) p))
                                                                         (and  (equal? 'v (conRac lNonDist)) 
                                                                               (or (and (not (estSymboleProp?  (cadr lNonDist) )) (equal? (conRac (cadr lNonDist)) '^)) ; cas ou lNonDist = (v (^ A B) C)
                                                                                   (and (not (estSymboleProp?  (caddr lNonDist) )) (equal? (conRac (caddr lNonDist)) '^) ) )) ; cas ou lNonDist = (v C (^ A B))
                                                                         (begin
                                                                           (cond [ (not distOu?)
                                                                                   (set! distOu? #t)]
                                                                           )
                                                                           (formeConj-aux5 (distOu lNonDist))
                                                                          )
                                                                         ]
                                                                        
                                                                        [#t
                                                                         (cond [
                                                                                (= (length lNonDist) 3) 
                                                                                (begin
                                                                                  (list
                                                                                   (conRac lNonDist)
                                                                                   (formeConj-aux5 (cadr lNonDist)  ) 
                                                                                   (formeConj-aux5 (caddr lNonDist) )
                                                                                   )
                                                                                  )
                                                                                ]
                                                                               [(= (length lNonDist) 2)
                                                                                (begin 
                                                                                  (list 
                                                                                   (conRac lNonDist)
                                                                                   (formeConj-aux5 (cadr lNonDist) )
                                                                                   
                                                                                   )
                                                                                  )
                                                                                
                                                                                ]
                                                                               
                                                                               )
                                                                         ]
                                                                        ) 
                                                                      
                                                                      ))
                                                    
                                                    ]
                                               #|(let [(lDistOu (formeConj-aux5 lNonDist #f))]
                                                 (display "lDistOu")
                                                 
                                                 (display lDistOu)
                                                 (display "\n")
                                                 (affiche lDistOu)
                                                 )|#
                                               
                                                 (let* [(lDistOuFinal (do [(lDistOu lNonDist)] [(equal? #f distOu?) lDistOu]
                                                          (set! distOu? #f)
                                                          ;(display "lDistOuDo: ")
                                                          ;(display lDistOu)
                                                          ;(display "\n")
                                                          (set! lDistOu (formeConj-aux5 lDistOu)) ;si on a une distribution effectué distOu? passe a true et la boucle do continue
                                                          
                                                          )
                                                  )]
                                                 (begin 
                                                   (set! distOu? #t)
                                                   (display "\n")
                                                   (display "lDistOuFinal :")
                                                   (affiche lDistOuFinal)
                                                   (display "\n")
                                                   lDistOuFinal
                                                 )
                                                )
                                               
                                             )

                                          )
                                      )
                                    )
                                );letrec3
                                )
                         );letrec2
                      )
                    );letrec1
                
               )
           )
  

(define distOu? #t)  ; bool a true si une distribution a été effectué dans formeConj-aux5

;Q31

(define transClause (lambda (fbfdisj)
                      (cond 
                            [(estSymboleProp? fbfdisj) (set fbfdisj)]
                            [(estNon? (conRac fbfdisj)) (set fbfdisj)]
                            [#t (set-union (transClause (cadr fbfdisj)) (transClause (caddr fbfdisj)))]
                        
                        
                       )
                      )
  )

;Q32

(define transEnsClause (lambda (fbfconj)
                        (cond 
                           [(estSymboleProp? fbfconj) (set (set fbfconj))]
                           [(estNon? (conRac fbfconj)) (set (set fbfconj))]
                           [(equal? (conRac fbfconj) 'v) (set (set-union (transClause (cadr fbfconj)) (transClause (caddr fbfconj))))]
                           [(equal? (conRac fbfconj) '^) (set-union (transEnsClause (cadr fbfconj)) (transEnsClause (caddr fbfconj)))] 
                        )
                       )
 )

;Q33

(define formeClausale (lambda (fbf)
                        (transEnsClause (formeConj fbf))
                       )
  )


(define infiVersPre (lambda(fbf)
                   (cond
                      [(estSymboleProp? fbf) 
                           fbf 
                      ]
                      [(estConBin? (cadr fbf))
                   
                         (list (cadr fbf) (infiVersPre (car fbf)) (infiVersPre (caddr fbf)))
                      ]
                      [#t
                        (list (car fbf) (infiVersPre (cadr fbf)))
                      ]
                     )
                    )
  )
                    
(define fbf1 '(! (-> (-> b a) (^ (! c ) (! (-> d (^ e f)))))))
 
(define fbf2 '( ! (-> (-> b a) (^ (! c) (! (-> d (^ e f) )) ))))

(define fbf3 '(-> (-> r p) (-> (! (v q r)) p)))
;(((!b)va)^((cv((!d)ve))^(cv((!d)vf))))

;Q34

#|
(define resolutionL (lambda (E N)   ;resolution en largeur
                     (let [(P (set ))] 
                      (letrec [(neg (lambda (l)
                                         (cond 
                                               [(symbol? l)
                                                   (list '! l)
                                                ]
                                               [(estNon? (conRac l))
                                                  (cadr l) ]
                                          )
                                      )
                                    )
                               ]
                          
                      (letrec [(resolvable (lambda (c1 c2) ;c1 c2 tout les deux des set
                                             (do [(i 0) (bool #f) (c1taille (lenght c1) (c2taille (lenght c2)))
                                                      ((= i c1taille) )]
                                               
                                                   (cond [(set-member? c2 (neg (car c1)))
                                                            (begin 
                                                              (set! bool #t)
                                                              (set! i c1taille)
                                                            )
                                                         ]
                                                      
                                                   
                                                 )
                                              )
                                             )
                                           )
                                 ]
                             (letrec (resolvable (lambda (c1 c2) ;c1 c2 tout les deux des set
                                             (do [(i 0) (bool #f) (c1taille (lenght c1) (c2taille (lenght c2)))
                                                      ((= i c1taille) )]
                                                (do [((j 0))
                                                      ((= j c2taille))]
                                                   (cond [(set-member? c2 (neg (car c1)))
                                                            (begin 
                                                              (set! bool #t)
                                                              (set! i c1taille)
                                                             )
                                                         ]
                                                      
                                                   )
                                                 )
                                              )
                                             )
                                                 
                                                                                                      
                                                                                                      
                                                                ]
                       
                       
                       (cond [(set-empty? N)
                             (do [(tailleN (set-count N)) (cptN 0)] [(= cptN tailleN)]
                               (do [(EUN (set-union E N)) (tailleEUN (set-count EUN)) (cptEUN 0)][(= cptEUN EUN)]
                                   
                                 
                                 
                                 (set! cptEUN (+ cptEUN 1))
                                )
                               (set! cptN (+ cptN 1))
                              )
                             ]
                      
                      )
                     )
                    )
                   )
  ;)
  ;)
;)

|#

;Verification des fonctions
#|

;1.
(define G '(-> (v (^ (<-> a b) c) (! b)) (! a)))
(define J1 '((a . 0) (b . 0) (c . 1) ))
(define J2 '((a . 1) (b . 0) (c . 1) ))

;2.
(display "\n")
(display "\n")
(affiche G)
(display "\n")
(valV G J1)
(valV G J2)

;3.
(display "\n")
(display "\n")
(satisfiable? G)
(satisfiable? '(<-> (^ p q) (-> p (! q))))

;4
(display "\n")
(display "\n")
(valide? G)
(valide? '(-> p (-> q p)))


;5
(display "\n")
(display "\n")
(equivalent1? '(v a (^ b (! b))) '(v a (^ c (! c))))
(equivalent1? 'p '(^ p (v q (! q))))
(equivalent1? '(^ p q) '(v p q))

;6
(display "\n")
(display "\n")
(consequence? '( (-> a b) (-> b c)) '(-> a c))
(consequence? '(a (v a b) (-> b c)) 'b)

;7.

(formeConj '(^ (v (^ (^ (v (^ a b) c) a) b) c) (v a b))) ;Mon (distOu est appellé le nombre de fois necessaire pour distribuer
                                                        ;dans formeConj. Autrement dit mon distOu ne peut pas marcher dans formeConj
                                                       ;mais le resultat obtenue est equivalent!

;8 

(display "\n")
(display "\n")
(formeConj '(-> (-> p q) p))
(display "\n")
(formeConj '(^ (! (^ q (! r))) (-> p (v q (^ r (! p))))))

(display "\n")
(display "\n")
;9
(formeClausale '(-> (-> p q) p))
(display "\n")
(formeClausale '(^ (! (^ q (! r))) (-> p (v q (^ r (! p))))))

;10 Methodes de resolutions incomplet.
|#


#|
(define K1 '((u . 1) (w . 0) (t . 0) (x . 0)))
 
(valV '(^ (^ (^ (^ u (-> w u)) (-> w w)) (-> t x)) (! x)) K1)

(equivalent1? '(^ (v p q) (! (^ p q))) '(v (^ p (! q)) (^ (! p) q))) ;deux facon d'ecrire le ou exclusif

(equivalent1? (infiVersPre '((! r) -> (p -> q)  )) (infiVersPre '((p -> q) ^ (! r)))) ; comprendre sauf si , a moin que


(consequence?   (list (infiVersPre '(B <-> ((! A) ^ (! B))))) ' (! B)) ;situation 2 exercice 5 TD3 pire et pures
(define K2 '((A . 1) (B . 0)))
(valV (infiVersPre '(B <-> ((! A) ^ (! B)))) K2) |#

;(formeClausale (infiVersPre '((r -> p) -> ((! (q v r)) -> p)))) ;exercice 2 TD 4

;(insatisfiable? (infiVersPre '(((! (b ^ a)) -> (a <-> b))  ^ (! ((! a) v b))))) ;exercice 10 TD4

;(satisfiable? (infiVersPre '((((! p) ^ ((! q) v r)) v (p -> (q ^ (! r)))) ^ (p <-> (! q)))))  ;exercice 10 TD4

;(define K3 '((p . 1) (q . 0) (r . 1) (s . 1)))                           ;Exercice 13 TD4

;(valV (infiVersPre '((! (q ^(! r))) ^ (p -> (q v (r ^ (! p)))))) K3)

;(valV (infiVersPre '((! (r -> s)) v ((! p) v (r ^ s)))) K3)

;(consequence2? (infiVersPre '((! (q ^(! r))) ^ (p -> (q v (r ^ (! p)))))) (infiVersPre '((! (r -> s)) v ((! p) v (r ^ s)))))


;Exercice 18 TD4

;(consequence?  (list (infiVersPre '((MC ^ (! E)) -> O)) (infiVersPre '(MC -> (J v (! O)))) (infiVersPre '((MC ^ M) -> (! D))) (infiVersPre '((MC ^ D) <-> E )) (infiVersPre '((MC ^ J) -> (E ^ M))) (infiVersPre '((MC ^ E) -> J))) 'MC)


;Exercice 19 TD4

;(consequence? (list (infiVersPre '((D v G) <-> SV ) ) (infiVersPre '((! D) <-> SV ) ) ) 'G)


;Examen 14 mai 2014 

;(formeClausale 
 ;  (infiVersPre '(((A -> C) v (A ^ E)) v (B ^ E) )))




