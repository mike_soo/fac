#ifndef LETOUT_H
#define LETOUT_H
#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846


using namespace std;
int cptP=0;
Tabpoint gab;
GLfloat angle = 0.0;
/*axe de rotation*/
GLfloat xx=0;
GLfloat yy=0;
GLfloat zz=0;
float **Ru;
float theta=M_PI/128;
bool mousegAc=false;

Point mouseRecup; //stock les coordonées du mouse recuperées en coord fenetre;
Point pr1;
Point pr2;
Point p1p2;
bool mousemAc=false; //verifiecateur du bouton du millieu activé
bool deuxiemeCli=false;
Point vUx;
int cptPointSais=0; //compteur de points saisies par le mouse
Point vsx(1,0,0);
Point vsy(0,1,0);
Point vsz(0,0,1);
Point Psaisie; //point recuperant les point de saisie en cordonnees espace;
int isaiTabP=0;
bool preDraw=true; //bool a true indiquant le premier dessin
/*axe de rotation*/
float zoom=0.70;

Point cNormal,Normal,p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p14,N1,N2,N3,N4,Nm,U,V,vNe1,vNe2;

Point vp1p4,vp5p4,vp1p5,vp6p5,vp5p8,vp9p8,vp4p7,p8p7,vp1p2,vp8p7,vp4p1,vp2p5,vp6p8;

Point vp2p1,vp3p2,vp7p5,vp5p6,vp7p4,vp4p5;

float nNormal=0.0;

bool vNormalVis=false;

bool lignesVis=true;
bool polyGVis=false;
bool pointVis=false;
bool vNsurface=false;
bool flatShading=false;
bool hautverlebas=false;



void keyboard(unsigned char key, int x, int y);

void mouseB(int button, int state, int x, int y);

void mouseM(int x, int y);
void render(void);
void renderS(void);
void Rof (float Ux,float Uy,float Uz);
void GetOGLPosRot(int x, int y);
void GetOGLPosSai(int x, int y);
void dessinCercle(Point & ccent,float r);
float normeV(Point v);
Point normaliseV(Point v);
void rempetombF(); //remplissage et ombrage flat.



int mod(int m, int n);
#endif