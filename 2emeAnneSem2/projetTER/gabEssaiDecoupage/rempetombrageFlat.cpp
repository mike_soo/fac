#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include "operateur.h"
#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846


extern int cptP;
extern Tabpoint gab;
extern GLfloat angle ;
/*axe de rotation*/
extern GLfloat xx;
extern GLfloat yy;
extern GLfloat zz;
extern float **Ru;
extern float theta;
extern bool mousegAc;

extern Point mouseRecup; //stock les coordonées du mouse recuperées en coord fenetre;
extern Point pr1;
extern Point pr2;
extern Point p1p2;
extern bool mousemAc;
extern bool deuxiemeCli;
extern Point vUx;
extern int cptPointSais;
extern Point vsx;
extern Point vsy;
extern Point vsz;
extern Point Psaisie; //point recuperant les point de saisie en cordonnees espace;
extern int isaiTabP;
extern bool preDraw;
/*axe de rotation*/
extern float zoom;

extern Point cNormal,Normal,p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p14,N1,N2,N3,N4,Nm,U,V,vNe1,vNe2;

extern Point vp1p4,vp5p4,vp1p5,vp6p5,vp5p8,vp9p8,vp4p7,p8p7,vp1p2,vp8p7,vp4p1,vp2p5,vp6p8;

extern Point vp2p1,vp3p2,vp7p5,vp5p6,vp7p4,vp4p5;

extern float nNormal;

extern bool vNormalVis;

extern bool lignesVis;
extern bool polyGVis;
extern bool pointVis;
extern bool vNsurface;
extern bool flatShading;
extern bool hautverlebas;

void rempetombF()
{
	for(int i=0; i<14; i++) 
      { 
       for(int j=0; j<36;j++)
        { glColor4f(0,0.2,1,1);
          
          p1 =(gab.tabP[i][j%36]->getX()*vsx)+(gab.tabP[i][j%36]->getY()*vsy)+(gab.tabP[i][j%36]->getZ()*vsz);
         
          p2=(gab.tabP[i][(j+1)%36]->getX()*vsx)+(gab.tabP[i][(j+1)%36]->getY()*vsy)+(gab.tabP[i][(j+1)%36]->getZ()*vsz);
   
          p3=(gab.tabP[i+1][(j+1)%36]->getX()*vsx)+(gab.tabP[i+1][(j+1)%36]->getY()*vsy)+(gab.tabP[i+1][(j+1)%36]->getZ()*vsz);
   
          p4=(gab.tabP[i+1][j%36]->getX()*vsx)+(gab.tabP[i+1][j%36]->getY()*vsy)+(gab.tabP[i+1][j%36]->getZ()*vsz);
        //on relie pour chaque case 
          if(i==0)
          {
            U=(p3-p2);
            V=(p4-p3);
          }
          else 
          {
            U=(p2-p1);
            V=(p3-p2);
          }

          Normal=U*V;
          if(hautverlebas)
            Normal=(-1)*Normal;

          cNormal=p1 + ((0.8f)*U )+ ((1/2.0f)*V); //centre du vecteur normal
          nNormal=(float)sqrt(((float)Normal.getX()*(float)Normal.getX())+((float)Normal.getY()*(float)Normal.getY())+((float)Normal.getZ()*(float)Normal.getZ())); //norme du vecteur normal

          if(flatShading)
          {
        
            glBegin(GL_POLYGON);
              
              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());
              glVertex3f(p1.getX(),p1.getY(),p1.getZ());

              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());
              glVertex3f(p2.getX(),p2.getY(),p2.getZ());

              glVertex3f(p3.getX(),p3.getY(),p3.getZ());
              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());
             
              glVertex3f(p4.getX(),p4.getY(),p4.getZ());
              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());

            glEnd();
          }

          if(vNsurface)
          {  glBegin(GL_LINES); //on affiche le vecteur normalisé a chaque polygon
             glColor3f(0.7,0.5,0);
      
              glVertex3f(cNormal.getX(),cNormal.getY(),cNormal.getZ());
              glVertex3f(cNormal.getX()+( (1.0/(nNormal*6))*Normal.getX()),cNormal.getY()+((1.0/(nNormal*6))*Normal.getY()),cNormal.getZ()+((1.0/(nNormal*6))*Normal.getZ()));
            glEnd();
          }
        }
      }
}