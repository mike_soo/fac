
#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846


using namespace std;
int cptP=0;
Tabpoint gab;
GLfloat angle = 0.0;
/*axe de rotation*/
GLfloat xx=0;
GLfloat yy=0;
GLfloat zz=0;
float **Ru;
float theta=M_PI/128;
bool mousegAc=false;
Point mouseRecup; //stock les coordonées du mouse recuperées en coord fenetre;
Point p1;
Point p2;
Point p1p2;
bool mousemAc=false; //verifiecateur du bouton du millieu activé
bool deuxiemeCli=false;
Point vUx;
int cptPointSais=0; //compteur de points saisies par le mouses
Point vx1(1,0,0);
Point vy1(0,1,0);
Point vz1(0,0,1);
Point Psaisie; //point recuperant les point de saisie en cordonnees espace;
int isaiTabP=0;
int jsaiTabP=0; //indice du tabP
bool preDraw=false; //bool a false indiquant le premier dessin n'a pas été encore dessiné
/*axe de rotation*/
float zoom=0.70;






void keyboard(unsigned char key, int x, int y);

void mouseB(int button, int state, int x, int y);

void mouseM(int x, int y);
void render(void);
void renderS(void);
void Rof (float Ux,float Uy,float Uz);
void GetOGLPosRot(int x, int y);
void GetOGLPosSai(int x, int y);
Point operator+(Point p1,Point p2);

Point operator*(float scal,Point p1);


int main(int argc, char** argv,char *envp[])
{
  Ru=new float *[3];
 
  for(int i=0;i<3;i++)
  {
    Ru[i]=new float [3];
  } 
 
 


 

 glutInit(&argc, argv);

 

 /*glutInitWindowPosition(740+(640*2),100);
 glutInitWindowSize(320,480); 
 glutCreateWindow("Saisie");
 glutDisplayFunc(renderS);*/
 

 
 glutInitWindowPosition(100+(640*2),100);
 glutInitWindowSize(640,480); 	//Optionnel

 glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
 //auxInitDisplayMode(AUX_RGBA);
 glClearDepth(1.0f);
 glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL); 
  //glGetIntegerv(GL_AUX_BUFFERS) ;
 glutCreateWindow("Rotation");

 glutDisplayFunc(render);
 glutMouseFunc(mouseB);
 glutMotionFunc(mouseM);
 glutKeyboardFunc(keyboard) ;
 
 

 glutMainLoop();
 

	return 0;
}

bool operator==(Point p1,Point p2)
{
  
  return (p1.getX()==p2.getX()) && (p1.getY()==p2.getY()) &&(p1.getZ()==p2.getZ());

  
}


bool operator!=(Point p1,Point p2)
{

 return !(p1==p2);
}


Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator-(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()-p2.getX());
  presul.setY(p1.getY()-p2.getY());
  presul.setZ(p1.getZ()-p2.getZ());

  return presul;
}


Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}

void Rof (float Ux,float Uy,float Uz)
{
 Ru[0][0]=(float)cos(theta)+(Ux*Ux*(1-cos(theta)));
 Ru[1][0]=(float)Uy*Ux*(1-cos(theta))+(Uz*sin(theta));
 Ru[2][0]=(float)Uz*Ux*(1-cos(theta))-(Uy*sin(theta));

 Ru[0][1]=(float)Uy*Ux*(1-cos(theta))-(Uz*sin(theta));
 Ru[1][1]=(float)cos(theta)+Uy*Uy*(1-cos(theta));
 Ru[2][1]=(float)Uz*Uy*(1-cos(theta))+Ux*sin(theta);

 Ru[0][2]=(float)Ux*Uz*(1-cos(theta))+Uy*sin(theta);
 Ru[1][2]=(float)Uy*Uz*(1-cos(theta))-Ux*sin(theta);
 Ru[2][2]=(float)cos(theta)+Uz*Uz*(1-cos(theta)); 

 Point tempx1((float)vx1.getX()*Ru[0][0]+vx1.getY()*Ru[0][1]+vx1.getZ()*Ru[0][2]  , (float)vx1.getX()*Ru[1][0]+vx1.getY()*Ru[1][1]+vx1.getZ()*Ru[1][2]  ,  (float)vx1.getX()*Ru[2][0]+vx1.getY()*Ru[2][1]+vx1.getZ()*Ru[2][2]);
 Point tempy1((float)vy1.getX()*Ru[0][0]+vy1.getY()*Ru[0][1]+vy1.getZ()*Ru[0][2]  , (float)vy1.getX()*Ru[1][0]+vy1.getY()*Ru[1][1]+vy1.getZ()*Ru[1][2]  ,  (float)vy1.getX()*Ru[2][0]+vy1.getY()*Ru[2][1]+vy1.getZ()*Ru[2][2]);
 Point tempz1((float)vz1.getX()*Ru[0][0]+vz1.getY()*Ru[0][1]+vz1.getZ()*Ru[0][2]  , (float)vz1.getX()*Ru[1][0]+vz1.getY()*Ru[1][1]+vz1.getZ()*Ru[1][2]  ,  (float)vz1.getX()*Ru[2][0]+vz1.getY()*Ru[2][1]+vz1.getZ()*Ru[2][2]);

 vx1=tempx1;
 vy1=tempy1;
 vz1=tempz1;
}


void renderS(void)
{
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glutSwapBuffers();

 
}

void render(void)
{

 //cout << "\033[2J\033[1;1f";
 /*if(!preDraw)
  {
    glDrawBuffer(GL_FRONT);
    preDraw=true;
  }
 
 else 
  glDrawBuffer(GL_BACK);*/
 
 
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

 //glClearDepth(1.0);

 glDrawBuffer(GL_FRONT);
       
 glLoadIdentity();
 
 
  glRotatef(angle,xx,yy,zz) ;
 
 glScalef(zoom, zoom, zoom);
  
  cout<<" glRot=("<<angle;
  cout<<","<<xx;
  cout<<","<<yy;
  cout<<","<<zz<<") ";
  cout<<"  zoom:"<<zoom<<endl;



glBegin(GL_LINES);
 
 /*base orthonormé fixe*/

  glColor3f(0,0,0);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);

 
glEnd();
 
 

  
 

 
 
 glBegin(GL_LINES);

  
  /*vector axe de rotationt*/
 glColor3f(1,0,1);
  glVertex3f(0,0,0);
  glVertex3f(xx,yy,zz);
  /*fin axe*/
 
 glEnd();

 //ajout de points automatiques pour tests
 if(cptPointSais<15)
 { Point *pTemp;
    for(int i=0;i<14;i++)
    { pTemp=new Point((float)i/10,(float)i/10,0);
      
      gab.tabP[i][0]=*pTemp;
    }
    gab.tabP[14][0]=*(new Point (0,1.4,0));
    cptPointSais=15;
    gab.addautrepoint();
 }

 if ((cptPointSais<14) && mousegAc)
 { cout<<"ModeSaisie;"<<endl;
   
   //pour bien placer le point 0 dans l'axe par rapport au premier
  //point saisie par l'utilisateur

   if(cptPointSais==0)  
   { 
     GetOGLPosSai(mouseRecup.getX(),mouseRecup.getY());
     cout<<"Mrecup0=("<<mouseRecup.getX()<<","<<mouseRecup.getY()<<") ";

       if(Psaisie.getX()>0)
        {
          P p0=new Point (0,Psaisie.getY(),0);
          gab.tabP[isaiTabP][jsaiTabP]=*p0;
          isaiTabP++;
          P temp =new Point(Psaisie.getX(),Psaisie.getY(),0);
          gab.tabP[isaiTabP][jsaiTabP]=*temp;
          isaiTabP++;        
          cptPointSais+=2;
          cout<<"Point p0 saisie ("<<p0->getX()<<","<<p0->getY()<<")"<<endl;
          cout<<"Point saisie ("<<Psaisie.getX()<<","<<Psaisie.getY()<<")"<<endl;
        }
        else
        {
          cout<<" Point non saisie "<<endl;
        }
       mousegAc=false;
   }

   else 
    {
       
       GetOGLPosSai(mouseRecup.getX(),mouseRecup.getY());
       cout<<"Mrecup=("<<mouseRecup.getX()<<","<<mouseRecup.getY()<<") ";
       if(Psaisie.getX()>0)
        {
          P temp =new Point(Psaisie.getX(),Psaisie.getY(),0);
          gab.tabP[isaiTabP][jsaiTabP]=*temp;
          isaiTabP++;     
          cptPointSais++;
          cout<<"Point saise ("<<Psaisie.getX()<<","<<Psaisie.getY()<<")"<<endl;
        }

        else
        {
          cout<<" Point non saisie "<<endl;
        }
       mousegAc=false;
    }
 }


 
 else if(cptPointSais==14)
 { cout<<"P15Y="<<Psaisie.getY()<<endl;
   P p15=new Point(0, Psaisie.getY(), 0);
   gab.tabP[isaiTabP][jsaiTabP]=*p15;
   cout<<"je marche!!!!!!!!!!!!"<<endl;
   gab.addautrepoint();
   cptPointSais++;
   cout<<"je marche!!!!!!!!!!!!"<<endl;
 }



 else if(cptPointSais>14)
 { 
  
   GetOGLPosRot(mouseRecup.getX(),mouseRecup.getY());
   /*rotation avec mouse */
   Point temp((p1p2.getX()*cos(M_PI/2))-(p1p2.getY()*sin(M_PI/2)),(p1p2.getX()*sin(M_PI/2))+(p1p2.getY()*cos(M_PI/2)),0);
   
   glBegin(GL_LINES);   
      glColor3f(0.5,0.2,1);//affichage du vecteur p1p2 de norme 1
          glVertex3f(0,0,0);
          glVertex3f((float)p1p2.getX(),(float)p1p2.getY(),(float)p1p2.getZ());
       //glVertex3f(0,0,0);
          //glVertex3f(p1p2.getX()/100,p1p2.getY()/100,p1p2.getZ()); //affichage correct de verif

     glColor3f(0.45,0.3,0);//affichage de la perpendiculaire du vecteur p1p2 de norme 1
      glVertex3f(0,0,0);
      glVertex3f(temp.getX(),temp.getY(),temp.getZ());
   glEnd();
    
    if(mousemAc)
    {
      vUx=temp;
     Rof(vUx.getX(),vUx.getY(),vUx.getZ());
    }
   cout<<" th="<<theta;
   cout<<endl;
   cout<<endl;



   
   

   glBegin(GL_LINES); //base blanche
    glColor3f(0,0,1);

        glVertex3f(0,0,0);
        glVertex3f(vx1.getX(),vx1.getY(),vx1.getZ());

    glColor3f(0,1,0);

        glVertex3f(0,0,0);
        glVertex3f(vy1.getX(),vy1.getY(),vy1.getZ());

    glColor3f(1,0,0);
        glVertex3f(0,0,0);
        glVertex3f(vz1.getX(),vz1.getY(),vz1.getZ());
   
   glEnd();
  
 


    glColor4f(0.5,1,1,1);
     //glPolygonMode(GL_FRONT,GL_LINE);
     
       /*
        glEnable(GL_BLEND);
        glBlendFunc(GL_ZERO,GL_ZERO);
        
        glEnable(GL_DEPTH_TEST);
        glClearDepth(1.0);
        glClear(GL_DEPTH_BUFFER_BIT); */
   for(int i=0; i<14; i++) //polygones
    { 
     for(int j=0; j<36;j++)
      { 
        glBegin(GL_POLYGON);

        Point temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

       
        //temp1=(gab.tabP[i][j+1].getX()*vx1)+(gab.tabP[i][j+1].getY()*vy1)+(gab.tabP[i][j+1].getZ()*vz1);
        //glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

        temp1=(gab.tabP[i][j+1].getX()*vx1)+(gab.tabP[i][j+1].getY()*vy1)+(gab.tabP[i][j+1].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

       
        //temp1=(gab.tabP[i+1][j+1].getX()*vx1)+(gab.tabP[i+1][j+1].getY()*vy1)+(gab.tabP[i+1][j+1].getZ()*vz1);
        //glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

        temp1=(gab.tabP[i+1][j+1].getX()*vx1)+(gab.tabP[i+1][j+1].getY()*vy1)+(gab.tabP[i+1][j+1].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());


        //temp1=(gab.tabP[i+1][j].getX()*vx1)+(gab.tabP[i+1][j].getY()*vy1)+(gab.tabP[i+1][j].getZ()*vz1);
        //glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

        temp1=(gab.tabP[i+1][j].getX()*vx1)+(gab.tabP[i+1][j].getY()*vy1)+(gab.tabP[i+1][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());


        temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
        glEnd();
        
      }
      
    }
   
/* //test polygone
    Point temp1=0.5*vx1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

    temp1=0.7*vx1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
    
    
    temp1=0.7*vx1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
    
    temp1=0.7*vx1+0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

    
    temp1=0.7*vx1+0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
    
    temp1=0.5*vx1+0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
   

    temp1=0.5*vx1+0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

    temp1=0.5*vx1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());


    temp1=0.5*vx1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

    temp1=0.5*vx1-0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());


    temp1=0.5*vx1-0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
    
    temp1=0.7*vx1-0.5*vy1;
    glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());




    glEnd();*/

/*
    glColor4f(0.5,0.7,0.5,1);
 
    glPolygonMode(GL_FRONT,GL_LINES);
    glPolygonMode(GL_BACK,GL_LINES);

    for(int i=0; i<5; i++) //polygones
    { 
     for(int j=0; j<15;j++)
      { 
        glBegin(GL_POLYGON);

        Point temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

       
        //temp1=(gab.tabP[i][j+1].getX()*vx1)+(gab.tabP[i][j+1].getY()*vy1)+(gab.tabP[i][j+1].getZ()*vz1);
        //glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

        temp1=(gab.tabP[i][j+1].getX()*vx1)+(gab.tabP[i][j+1].getY()*vy1)+(gab.tabP[i][j+1].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

       
        //temp1=(gab.tabP[i+1][j+1].getX()*vx1)+(gab.tabP[i+1][j+1].getY()*vy1)+(gab.tabP[i+1][j+1].getZ()*vz1);
        //glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

        temp1=(gab.tabP[i+1][j+1].getX()*vx1)+(gab.tabP[i+1][j+1].getY()*vy1)+(gab.tabP[i+1][j+1].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());


        //temp1=(gab.tabP[i+1][j].getX()*vx1)+(gab.tabP[i+1][j].getY()*vy1)+(gab.tabP[i+1][j].getZ()*vz1);
        //glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

        temp1=(gab.tabP[i+1][j].getX()*vx1)+(gab.tabP[i+1][j].getY()*vy1)+(gab.tabP[i+1][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());


        temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
        glEnd();
        
      }
      
    }
   
*/
 
  //glDepthFunc(GL_LEQUAL); 
 
   glBegin(GL_LINES); 
    glColor4f(0,0,0,1);
     
    for(int i=0; i<14; i++) //lignes horizontal
    { 
      for(int j=0; j<36;j++)
      { 
         Point temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
        temp1=(gab.tabP[i][j+1].getX()*vx1)+(gab.tabP[i][j+1].getY()*vy1)+(gab.tabP[i][j+1].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
      }
    }

   for(int j=0; j<36;j++) //lignes vertical
    { 
     for(int i=0; i<14; i++)
      { 
        Point temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
        temp1=(gab.tabP[i+1][j].getX()*vx1)+(gab.tabP[i+1][j].getY()*vy1)+(gab.tabP[i+1][j].getZ()*vz1);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
      }  
    }
     
   glEnd();
   
   
   //theta=0;
  }
  
  
   

 glBegin(GL_POINTS); //affichae de points
    glColor3f(0,0.5,1);
  
  //cout<<"gabSize="<<gab.t().size()<<endl;
    for(size_t i=0; i<15; i++)
    {

      for (int j=0;j<36;j++)
      { 

        Point temp1=(gab.tabP[i][j].getX()*vx1)+(gab.tabP[i][j].getY()*vy1)+(gab.tabP[i][j].getZ()*vz1);
        glVertex3f(0,0,0);
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

     } 
  }
  glEnd();

 
  //glFlush();
  glFinish();
 //glutSwapBuffers();
}




void GetOGLPosSai(int x, int y)
{
  GLint viewport[4];
  GLdouble modelview[16];
  GLdouble projection[16];
  GLfloat winX, winY, winZ;
  GLdouble posX, posY, posZ;
  glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
  glGetDoublev( GL_PROJECTION_MATRIX, projection );
  glGetIntegerv( GL_VIEWPORT, viewport );
  
  winX = (float)x;
  winY = (float)viewport[3] - (float)y;
  glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
  gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);  
 // cout<<" posX="<<posX<<" posY="<<posY<<" posZ="<<posZ;
 
 Psaisie.setX(posX);
 Psaisie.setY(posY);
 Psaisie.setZ(0);

}




void GetOGLPosRot(int x, int y)
{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;
 
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );
 
    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
 
    //return CVector3(posX, posY, posZ);
    cout<<" posX="<<posX<<" posY="<<posY<<" posZ="<<posZ;

    Point verif;
    float thetaPP;
   
    if(deuxiemeCli)
    { 
      p2.setX((float)posX*10000);
      p2.setY((float)posY*10000);
      p2.setZ(0);
      
      
       
      verif=(p2-p1); //verification de la norme du nouveau vecteur p2-p1;
      float norme=(float)sqrt((verif.getX()*verif.getX())+(verif.getY()*verif.getY())+(verif.getZ()*verif.getZ()));
      cout<<" \e[34m NORME="<<norme<<"\e[0m ";
        
      if((p1!=p2) && (0<(float)norme)) //on evite la division par zero
       {
         thetaPP=(float)acos((float)verif.getX()/(float)norme); //on calcule l'angle de p2-p1
         cout<<"\e[32m thP1P2="<<thetaPP*180/M_PI<<"\e[0m"; //on affiche l'angle de p2-p1
       
         if(verif.getY()<0) //on calcule en fonction du signe de Y  l'angle thetaPP
          {
           Point tempp((float)cos(-thetaPP),(float)sin(-thetaPP),0); //on calcul le vecteur representant le vecteur p2-p1 avec un norme de 1
           p1p2=tempp; //p1p2 amplifié avec norme egal 1
          }
           else
          {
            Point tempp((float)cos(thetaPP),(float)sin(thetaPP),0); //on calcul le vecteur representant le vecteur p2-p1 avec un norme de 1
            p1p2=tempp; //p1p2 amplifié avec norme egal 1
          } 
        }
          //p1p2=verif; //le vecteur p2-p1 avec ca norme sans imposition de norme =1
        
     deuxiemeCli=false;
     cout<<endl;
      
     cout<<"\e[34m np1p2="<<(float)sqrt((p1p2.getX()*p1p2.getX())+(p1p2.getY()*p1p2.getY())+(p1p2.getZ()*p1p2.getZ()))<<" "; //norme du vecteur p1p2
     cout<<" np1="<<(float )sqrt((p1.getX()*p1.getX())+(p1.getY()*p1.getY())+(p1.getZ()*p1.getZ())); //norme du vecteur p1
     cout<<" np2="<<(float )sqrt((p2.getX()*p2.getX())+(p2.getY()*p2.getY())+(p2.getZ()*p2.getZ()))<<"\e[0m"; //norme du vectur p2
     
     cout<<"\e[35m p1=("<<p1.getX()<<","<<p1.getY()<<") "; //coordonees p1
     cout<<" p2=("<<p2.getX()<<","<<p2.getY()<<") "; //coordonees p2
     cout<<" p1p2=("<<p1p2.getX()<<","<<p1p2.getY()<<") \e[0m"; //coordonees p1p2
     cout<<endl;
     p2.setX(0);
     p2.setY(0);
     p1.setX(0);
     p1.setY(0);
     
     theta = norme * M_PI/(64*500);

     /*if(norme>500)
     {
        theta=M_PI/64;
     }
     
     else if (norme<100)
     {
      theta=M_PI/1024;
     }

     else if (norme<50)
     {
      theta=M_PI/2048;
     }
     else 
     {
       theta=M_PI/128;
     }*/
     
    }

    else if(!deuxiemeCli)
    {
      p1.setX((float)posX*10000);
      p1.setY((float)posY*10000);
      p1.setZ(0);
      deuxiemeCli=true;
    }
 

    verif.setX(posX*100);
    verif.setY(posY*100);
    thetaPP=(float)acos((float)verif.getX()/(sqrt((float)(verif.getX()*verif.getX())+(verif.getY()*verif.getY()))));


    cout<<"\e[33m tp2="<<thetaPP*180/M_PI<<"\e[0m";
    
  
}


void mouseB(int button, int state, int x, int y)
{
  switch (button)
    {
      
     case (GLUT_MIDDLE_BUTTON):
       if(state==GLUT_UP)
        {
         mousemAc=false;
         deuxiemeCli=false;

        } 

        else if(state==GLUT_DOWN)
        {
         mousemAc=true;
         deuxiemeCli=false;
        }    
      break;
     
      case (GLUT_LEFT_BUTTON):
       
      if(state==GLUT_DOWN)
        {
         mousegAc=true;
         mouseRecup.setX(x);
         mouseRecup.setY(y);
         glutPostRedisplay();
        }    
      break;
   }
}

void mouseM(int x,int y)
{ if(mousemAc)
  {
   std::cout<<" Souris a:"<<x<<" "<<y;
   mouseRecup.setX(x);
   mouseRecup.setY(y);
   glutPostRedisplay();
  }
}

void keyboard(unsigned char key, int x, int y) {


switch (key) {

case 'y':
    zoom += 0.025;
    
    glutPostRedisplay();
    break;
case 'h':
    zoom -=0.025;
    
    glutPostRedisplay() ;
    break ;


case 'p':
angle ++ ;
glutPostRedisplay() ;
break ;

case 'm':
angle --;
glutPostRedisplay();
break;

case 'u':
xx+=0.05;
glutPostRedisplay() ;
break;

case 'j':
xx-=0.05;
glutPostRedisplay() ;
break;

case 'i':
yy+=0.05;
glutPostRedisplay() ;
break;

case 'k':
yy-=0.05;
glutPostRedisplay() ;
break;

case 'o':
zz+=0.05;
glutPostRedisplay() ;
break;

case 'l':
zz-=0.05;
glutPostRedisplay() ;
break;

case 't':  //vue dessus
angle = 0.0;
xx=0;
yy=0;
zz=0;
theta=M_PI/64;;

//zoom=0.70;

vx1.setX(1);
vx1.setY(0);
vx1.setZ(0);

vy1.setX(0);
vy1.setY(0);
vy1.setZ(1);

vz1.setX(0);
vz1.setY(-1);
vz1.setZ(0);

glutPostRedisplay();
break;

case 'g': //vue dessous
angle = 0.0;
xx=0;
yy=0;
zz=0;
theta=M_PI/64;;

//zoom=0.70;

vx1.setX(1);
vx1.setY(0);
vx1.setZ(0);

vy1.setX(0);
vy1.setY(0);
vy1.setZ(-1);

vz1.setX(0);
vz1.setY(1);
vz1.setZ(0);

glutPostRedisplay();

break;
/*
case'g':
theta-=M_PI/16;
glutPostRedisplay();
break;*/

case 'r':
angle = 0.0; //vue par defaulttg
xx=0;
yy=0;
zz=0;
theta=M_PI/64;;

zoom=0.70;

vx1.setX(1);
vx1.setY(0);
vx1.setZ(0);

vy1.setX(0);
vy1.setY(1);
vy1.setZ(0);

vz1.setX(0);
vz1.setY(0);
vz1.setZ(1);



glutPostRedisplay() ;
break;

case 'v':
angle = 0.0;
xx=0;
yy=0;
zz=0;
theta=M_PI/64;;

zoom=0.70;

vx1.setX(1);
vx1.setY(0);
vx1.setZ(0);

vy1.setX(0);
vy1.setY(1);
vy1.setZ(0);

vz1.setX(0);
vz1.setY(0);
vz1.setZ(1);

for (int i=0; i<15;i++)
{
  
    delete [] gab.tabP[i];
  
   // delete [] gab.tabP[i];
  
}
delete [] gab.tabP;

Point** temptabP;
  temptabP=new Point*[15];
for (int i=0;i<15;i++)
  {
    temptabP[i]=new Point[38];
  }

gab.tabP=temptabP;
isaiTabP=0;

cptPointSais=0;
mousegAc=false;
glutPostRedisplay() ;
break;


case 27 /* Esc */:
exit(1) ;

}
}