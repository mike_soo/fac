#include "Tabpoint.h"
#include <iostream>
#include <math.h>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"
# define M_PI           3.14159265358979323846
Tabpoint gab;

GLfloat angle = 0.0 ;
/*axe de rotation*/
GLfloat xx=1;
GLfloat yy=1;
GLfloat zz=0;
GLfloat p=1;


/*axe de rotation*/
double zoom=1;

double theta=0;
double delta=0;


void keyboard(unsigned char key, int x, int y);

void render(void);

int main(int argc, char** argv,char *envp[])
{
	
Point p0, p15;
p0=Point(0,0,0);
p15=Point(0.4, 1.5, 0);
gab.addpoint(p0);
for(int i=1; i<14; i++)
{
	Point p;
	p=Point((float)i/10,(float)i/10,0);
	gab.addpoint(p);
}
 gab.addpoint(p15);
 gab.addautrepoint(gab);

 glutInit(&argc, argv);
 //glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
 glutInitWindowPosition(100,100);
 glutInitWindowSize(640,480); 	//Optionnel
 glutCreateWindow("Ma première fenêtre OpenGL !");
  
 glutDisplayFunc(render);
 glutKeyboardFunc(keyboard) ;

 glutMainLoop();



//glBegin(GL_POINTS);
/*for(size_t i=0; i<gab.t().size(); i++)
{
	glVertex3f(gab.t()[i].getX(),gab.t()[i].getY(),gab.t()[i].getZ());
}
*/
//glutCreateWindow(argv[0]);

	return 0;
}

void render(void)
{

 //cout << "\033[2J\033[1;1f";
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glRotatef(angle,xx,yy,zz) ;
  glScalef(zoom, zoom, zoom);
  
  cout<<"angledeg: "<<angle;
  cout<<"  x : "<<xx;
  cout<<"  y : "<<yy;
  cout<<"  z : "<<zz;
  cout<<"  zoom : "<<zoom;
  cout<<"  theta:"<<theta;
  cout<<"  delta:"<<delta<<endl;
  glBegin(GL_POINTS);
 glColor3f(1,1,1);
 for(size_t i=0; i<gab.t().size(); i++)
 {
 	glVertex3f(gab.t()[i].getX(), gab.t()[i].getY(), gab.t()[i].getZ());
 }
glEnd();

glBegin(GL_LINES);
 
 /*base orthonormé*/

 glColor3f(1,0,0);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);
 	
 	
  /*vector axe de rotationt*/
 glColor3f(1,0,1);
  glVertex3f(0,0,0);
  glVertex3f(xx,yy,zz);
  /*fin axe*/
 /*base polaire*/
  /*vecteur x1*/
 glColor3f(0,1,0);
 	glVertex3f(0,0,0);
  glVertex3f(p*cos(theta),0,0);
  glVertex3f(0,0,0);
  glVertex3f(0,p*sin(theta+M_PI/2),0);
  glVertex3f(0,0,0);
  glVertex3f(0,0,0);
glEnd();


 glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y) {


switch (key) {
case 't':
  theta++;
  glutPostRedisplay();
  break;
case 'g':
  theta--;
  glutPostRedisplay();
  break;

case 'r':
  delta++;
  glutPostRedisplay();
  break;

case 'f':
  delta--;
  glutPostRedisplay();
  break;
case 'y':
    zoom += 0.125;
    
    glutPostRedisplay();
    break;
case 'h':
    zoom -=0.125;
    
    glutPostRedisplay() ;
    break ;


case 'p':
angle ++ ;
glutPostRedisplay() ;
break ;

case 'm':
angle --;
glutPostRedisplay();
break;

case 'u':
xx+=0.05;
glutPostRedisplay() ;
break;

case 'j':
xx-=0.05;
glutPostRedisplay() ;
break;

case 'i':
yy+=0.05;
glutPostRedisplay() ;
break;

case 'k':
yy-=0.05;
glutPostRedisplay() ;
break;

case 'o':
zz+=0.05;
glutPostRedisplay() ;
break;

case 'l':
zz-=0.05;
glutPostRedisplay() ;
break;



case 27 /* Esc */:
exit(1) ;

}
}