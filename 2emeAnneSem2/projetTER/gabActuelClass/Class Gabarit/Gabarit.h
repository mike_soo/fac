#ifndef GABARIT_H
#define GABARIT_H

#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846

using namespace std;


class Gabarit
{ 	public:
	
	int cptP=0;
	Tabpoint gab;
	GLfloat angle = 0.0;
	/*axe de rotation*/
	GLfloat xx=0;
	GLfloat yy=0;
	GLfloat zz=0;
	float Ru[3][3];
	float theta=M_PI/128;
	bool mousegAc=false;
	Point *verif;
	Point *mouseRecup; //stock les coordonées du mouse recuperées en coord fenetre;
	Point *sp1;
	Point *sp2;
	Point *sp1p2;
	bool mousemAc=false; //verifiecateur du bouton du millieu activé
	bool deuxiemeCli=false;
	Point *vU; //vecteur rotation
	int cptPointSais=0; //compteur de points saisies par le mouses
	Point *vx1;
	Point *vy1;
	Point *vz1;
	Point *Psaisie; //point recuperant les point de saisie en cordonnees espace;
	int isaiTabP=0;
	int jsaiTabP=0; //indice du tabP
	bool preDraw=true; //bool a true indiquant le premier dessin
	/*axe de rotation*/
	float zoom=0.70;

	bool vNormalVis=false;

	bool lignesVis=true;
	bool polyGVis=false;

	bool hautverlebas=false;

	void keyboard(unsigned char key, int x, int y);

	void mouseB(int button, int state, int x, int y);

	void mouseM(int x, int y);
	void render(void);
	void renderS(void);
	void Rof (float Ux,float Uy,float Uz);
	void GetOGLPosRot(int x, int y);
	void GetOGLPosSai(int x, int y);
	void dessinCercle(Point & ccent,float r);
	float normeV(Point &v);
	Point& normaliseV(Point &v);
	



	

	int mod(int m, int n);

   Gabarit();


};


#endif