
#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include "Point.h"
# define M_PI           3.14159265358979323846

using namespace std;

GLfloat angle = 0.0;
/*axe de rotation*/
GLfloat xx=1;
GLfloat yy=1;
GLfloat zz=0;
GLfloat p=2;

vector<Point> baseCylind;
vector<Point> ortho;

float phi=0;
float xct=0.5;
float theta=(float)-xct/p;
Point M;
Point vM; //vitesse de M;
Point vMc;//vitesse de M calculé a partir du point C;
Point vC; //vitesse du centre C ;

int t=0;


/*axe de rotation*/
float zoom=0.225;




void keyboard(unsigned char key, int x, int y);

void render(void);



Point operator+(Point p1,Point p2);

Point operator*(float scal,Point p1);

int main(int argc, char** argv,char *envp[])
{ Point Ccyl(0,0,0);
  Point xx1(p*cos(theta*t),p*sin(theta*t),0);
  Point yy1(-p*sin(theta*t),p*cos(theta*t),0);
  Point zz1(0,0,1);
  baseCylind.push_back(Ccyl);
	baseCylind.push_back(xx1);
  baseCylind.push_back(yy1);
  baseCylind.push_back(zz1);
  Point Cortho(0,0,0);
  Point x0(1,0,0);
  Point y0(0,1,0);
  Point z0(0,0,1);
  ortho.push_back(Cortho);
  ortho.push_back(x0);
  ortho.push_back(y0);  
  ortho.push_back(z0);
 glutInit(&argc, argv);
 //glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
 glutInitWindowPosition(100,100);
 glutInitWindowSize(640,480); 	//Optionnel
 glutCreateWindow("Rotation");
  
 glutDisplayFunc(render);
 
 glutKeyboardFunc(keyboard) ;

 glutMainLoop();
 

	return 0;
}



Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}

void modifBaseCyl()
{ float temp;
  
  baseCylind[0].setX(0);
  baseCylind[0].setY(0);
  baseCylind[0].setZ(0);

  baseCylind[1].setX(p*cos(theta*t));
  baseCylind[1].setY(p*sin(theta*t));
  baseCylind[1].setZ(0);


  baseCylind[2].setX(p*(-sin(theta*t)));
  baseCylind[2].setY(p*cos(theta*t));
  baseCylind[2].setZ(0);

  baseCylind[3].setX(0);
  baseCylind[3].setY(0);
  baseCylind[3].setZ(p);



}

void render(void)
{

 //cout << "\033[2J\033[1;1f";
 
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glRotatef(angle,xx,yy,zz) ;
  glScalef(zoom, zoom, zoom);
  
  cout<<"angledeg: "<<angle;
  cout<<"  x:"<<xx;
  cout<<"  y:"<<yy;
  cout<<"  z:"<<zz;
  cout<<"  zoom:"<<zoom;
  cout<<"  theta:"<<theta;
  cout<<"  phi:"<<phi;
  cout<<"  t:"<<t<<endl;

  cout<<"bCyl: ";
  cout<<"C=("<<baseCylind[0].getX()<<","<<baseCylind[0].getY()<<","<<baseCylind[0].getZ()<<") ";
  cout<<"x1=("<<baseCylind[1].getX()<<","<<baseCylind[1].getY()<<","<<baseCylind[1].getZ()<<") ";
  cout<<"y1=("<<baseCylind[2].getX()<<","<<baseCylind[2].getY()<<","<<baseCylind[2].getZ()<<") ";
  cout<<"z1=("<<baseCylind[3].getX()<<","<<baseCylind[3].getY()<<","<<baseCylind[3].getZ()<<") ";
  cout<<"xct="<<xct;
  cout<<endl;
  cout<<endl;



glBegin(GL_LINES);
 
 /*base orthonormé*/

 glColor3f(1,0,0);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);
  Point textx(1,0,0);
  Point texty(0,1,0);
  Point testM;

 
 glColor3f(1,5,1);
  
  /*vector axe de rotationt*/
 glColor3f(1,0,1);
  glVertex3f(0,0,0);
  glVertex3f(xx,yy,zz);
  /*fin axe*/
 
 /*base cyl*/
  modifBaseCyl();
 glColor3f(0,1,0);
 	
  glVertex3f(baseCylind[0].getX()+xct*t,baseCylind[0].getY()+p,baseCylind[0].getZ()); /*Centre de repere R1(C,x1,y1,z1) */
  glVertex3f(baseCylind[1].getX()+xct*t,baseCylind[1].getY()+p,baseCylind[1].getZ()); /*vecteur x1*/
  
  glVertex3f(baseCylind[0].getX()+xct*t,baseCylind[0].getY()+p,baseCylind[0].getZ());
  glVertex3f(baseCylind[2].getX()+xct*t,baseCylind[2].getY()+p,baseCylind[2].getZ()); /*vecteur y1*/
  
  glVertex3f(baseCylind[0].getX()+xct*t,baseCylind[0].getY()+p,baseCylind[0].getZ());
  glVertex3f(baseCylind[3].getX()+xct*t,baseCylind[3].getY()+p,baseCylind[3].getZ()); /*vecteur z1*/
  
  
  
 

  glColor3f(0,1,2);
  glVertex3f(baseCylind[0].getX()+xct*t,baseCylind[0].getY()+p,baseCylind[0].getZ()); /*point M*/
  M =((float)(sqrt(2)/2)*baseCylind[1])+((float)(sqrt(2)/2)*baseCylind[2]);
  
  glVertex3f(M.getX()+xct*t,M.getY()+p,M.getZ());
  
  
  /*vecteur vitesse de M calcule a l'aide de Id*/ 
  
/*
 vM.setX((float)(-(((float)(theta)*(float)(sqrt(2)/2)*(sin(theta*t)+(float)cos(theta*t)))+(theta*p))));
 vM.setY((float)((float)theta*(sqrt(2)/2)*(cos(theta*t)-(float)sin(theta*t))));
 vM.setZ(0);*/
 

  glColor3f(0,1,3);
  glVertex3f(M.getX()+xct*t,M.getY()+p,0);
  glVertex3f(vM.getX()+M.getX()+xct*t,vM.getY()+M.getY()+p,vM.getZ());


  
  /*vecteur vitesse de C*/
  glColor3f(4,1,5);
    vC.setX(xct); //derivé de xct!
  vC.setY(0);
  vC.setZ(0);
  glVertex3f(M.getX()+xct*t,M.getY()+p,0);
  glVertex3f(vC.getX()+M.getX()+xct*t,vC.getY()+M.getY()+p,vC.getZ());

  

 /*vecteur vitesse de M sans ajouté xct*/ //calculé avec le point C
 
 vMc=(((-theta*(sqrt(2)/2))*baseCylind[1])+((theta*(sqrt(2)/2))*baseCylind[2]));
 
 glColor3f(4,1,5);
 glVertex3f(M.getX()+xct*t,M.getY()+p,0);
 glVertex3f(vMc.getX()+M.getX()+xct*t,vMc.getY()+M.getY()+p,vMc.getZ()+M.getZ());

/*vecteur vitesse de M*/ //calculé avec le point C
   glColor3f(2,1,0);


 vMc=((xct*ortho[1])+((-theta*(sqrt(2)/2))*baseCylind[1])+((theta*(sqrt(2)/2))*baseCylind[2]));
 
 glVertex3f(M.getX()+xct*t,M.getY()+p,0);
 glVertex3f(vMc.getX()+M.getX()+xct*t,vMc.getY()+M.getY()+p,vMc.getZ()+M.getZ());


  cout<<endl;
  
  
  
  cout<<"x1x*y1x+x1y*y1y="<<(float)((float)baseCylind[1].getX()*baseCylind[2].getX())+((float)baseCylind[1].getY()*baseCylind[2].getY())<<endl;

  cout<<"CM=("<<M.getX()<<","<<M.getY()<<","<<M.getZ()<<")"<<endl;
  
  /*droite representé dans la base polaire*/

  //glVertex3f(0,0,0);
  //glVertex3f(p*cos(theta)+p*cos(theta +M_PI/2),p*sin(theta)+p*sin(theta+M_PI/2),0);


  /*baseSpherique*/
  /*glColor3f(0,1,0);
  glVertex3f(0,0,0);
  glVertex3f(p*sin(phi)*cos(theta),p*sin(phi)*sin(theta),p*cos(phi));
  glVertex3f(0,0,0);
  glVertex3f(p*cos(phi),p*sin(phi)*cos(theta),p*sin(phi)*sin(theta));*/
 /* glVertex3f(0,0,0);
  glVertex3f(0,0,0);*/

glEnd();

glBegin(GL_POINTS);

for(int i=0;i<1000;i++)
{
  glVertex3f(xct*t+p*cos(i),p+p*sin(i),0);
}

glEnd();

 glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y) {


switch (key) {
/*case 't':
  theta+=(float)M_PI/8;
  glutPostRedisplay();
  break;
case 'g':
  theta-=(float)M_PI/8;
  glutPostRedisplay();
  break;*/

case 'r':
  phi+=0.1;
  glutPostRedisplay();
  break;

case 'f':
  phi-=0.1;
  glutPostRedisplay();
  break;
case 'y':
    zoom += 0.005;
    
    glutPostRedisplay();
    break;
case 'h':
    zoom -=0.005;
    
    glutPostRedisplay() ;
    break ;


case 'p':
angle ++ ;
glutPostRedisplay() ;
break ;

case 'm':
angle --;
glutPostRedisplay();
break;

case 'u':
xx+=0.05;
glutPostRedisplay() ;
break;

case 'j':
xx-=0.05;
glutPostRedisplay() ;
break;

case 'i':
yy+=0.05;
glutPostRedisplay() ;
break;

case 'k':
yy-=0.05;
glutPostRedisplay() ;
break;

case 'o':
zz+=0.05;
glutPostRedisplay() ;
break;

case 'l':
zz-=0.05;
glutPostRedisplay() ;
break;

case 'x':
xct+=0.1;
theta=(float)-xct/p;
cout<<"xct="<<xct<<endl;
//glutPostRedisplay() ;
break;

case 'w':
xct-=0.1;
theta=(float)-xct/p;
cout<<"xct="<<xct<<endl;
//glutPostRedisplay() ;
break;

case 'a':
t++;
glutPostRedisplay();
break;

case 'q':
t--;
glutPostRedisplay();
break;

case 'c':
angle = 0.0;
xx=1;
yy=1;
zz=0;
phi=0;
xct=0.5;
theta=(float)-xct/p;
t=0;
zoom=0.225;
baseCylind[0].setX(0);
baseCylind[0].setY(0);
baseCylind[0].setZ(0);

baseCylind[1].setX(p*cos(theta*t));
baseCylind[1].setY(p*sin(theta*t));
baseCylind[1].setZ(0);


baseCylind[2].setX(-p*sin(theta*t));
baseCylind[2].setY(p*cos(theta*t));
baseCylind[2].setZ(0);

baseCylind[3].setX(0);
baseCylind[3].setY(0);
baseCylind[3].setZ(1);
glutPostRedisplay() ;
break;


case 27 /* Esc */:
exit(1) ;

}
}