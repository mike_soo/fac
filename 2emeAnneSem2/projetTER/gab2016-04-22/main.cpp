
#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846


using namespace std;
int cptP=0;
Tabpoint gab;
GLfloat angle = 0.0;
/*axe de rotation*/
GLfloat xx=0;
GLfloat yy=0;
GLfloat zz=0;
float **Ru;
float theta=M_PI/128;
bool mousegAc=false;

Point mouseRecup; //stock les coordonées du mouse recuperées en coord fenetre;
Point p1;
Point p2;
Point p1p2;
bool mousemAc=false; //verifiecateur du bouton du millieu activé
bool deuxiemeCli=false;
Point vUx;
int cptPointSais=0; //compteur de points saisies par le mouse
Point vsx(1,0,0);
Point vsy(0,1,0);
Point vsz(0,0,1);
Point Psaisie; //point recuperant les point de saisie en cordonnees espace;
int isaiTabP=0;
bool preDraw=true; //bool a true indiquant le premier dessin
/*axe de rotation*/
float zoom=0.70;

bool vNormalVis=false;

bool lignesVis=true;
bool polyGVis=false;
bool pointVis=false;
bool vNsurface=false;
bool flatShading=false;
bool hautverlebas=false;



void keyboard(unsigned char key, int x, int y);

void mouseB(int button, int state, int x, int y);

void mouseM(int x, int y);
void render(void);
void renderS(void);
void Rof (float Ux,float Uy,float Uz);
void GetOGLPosRot(int x, int y);
void GetOGLPosSai(int x, int y);
void dessinCercle(Point & ccent,float r);
float normeV(Point v);
Point normaliseV(Point v);

Point operator+(Point p1,Point p2);

Point operator*(float scal,Point p1);

int mod(int m, int n);


int main(int argc, char** argv,char *envp[])
{
  Ru=new float *[3];
 
  for(int i=0;i<3;i++)
  {
    Ru[i]=new float [3];
  } 
 
 


 

 glutInit(&argc, argv);

 

 /*glutInitWindowPosition(740+(640*2),100);
 glutInitWindowSize(320,480); 
 glutCreateWindow("Saisie");
 glutDisplayFunc(renderS);*/
 

 glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
 
 glutInitWindowPosition(200/*+(640*2)*/,100);

 glutInitWindowSize(640,480); 	//Optionnel
 glutCreateWindow("Gabarit 2016");

 


    
 glDepthMask(GL_TRUE);
 glEnable(GL_DEPTH_TEST);
 glDepthRange(1.0f,0.0f);
 glDepthFunc(GL_LEQUAL); 
 
 glEnable(GL_COLOR_MATERIAL);
 glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
 glShadeModel(GL_SMOOTH); //GL_FLAT GL_SMOOTH
 


 glClearColor(0, 0, 0, 0);
 
 
 
 glutDisplayFunc(render);
 glutMouseFunc(mouseB);
 glutMotionFunc(mouseM);
 glutKeyboardFunc(keyboard) ;
 
 

 glutMainLoop();
 

	return 0;
}

int mod (int m, int n) 
{ return m >= 0 ? m % n : ( n - abs ( m%n ) ) % n; }

bool operator==(Point p1,Point p2)
{
  
  return (p1.getX()==p2.getX()) && (p1.getY()==p2.getY()) &&(p1.getZ()==p2.getZ());

  
}


bool operator!=(Point p1,Point p2)
{

 return !(p1==p2);
}


Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator-(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()-p2.getX());
  presul.setY(p1.getY()-p2.getY());
  presul.setZ(p1.getZ()-p2.getZ());

  return presul;
}


Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}


Point operator * (Point p1,Point p2) //produit vectorielle
{
  Point presul;
  presul.setX( (p1.getY()*p2.getZ())  -  (p1.getZ()* p2.getY()) );

  presul.setY( (p1.getZ()*p2.getX())  -  (p1.getX()* p2.getZ())  );

  presul.setZ( (p1.getX()*p2.getY())  -  (p1.getY()* p2.getX())  );
  return presul;

}

float normeV(Point v)
{
  return (float)sqrt(((float)v.getX()*(float)v.getX())+((float)v.getY()*(float)v.getY())+((float)v.getZ()*(float)v.getZ())); //norme du vecteur normal


}

Point normaliseV(Point v)
{
  Point vNormalise;
  float nV=normeV(v);
  if(nV!=0)
  {
    vNormalise.setX((1.0f/nV)*v.getX());
    vNormalise.setY((1.0f/nV)*v.getY());
    vNormalise.setZ((1.0f/nV)*v.getZ());
    return vNormalise;
  }

  else 
    return v;
  
  
}

void Rof (float Ux,float Uy,float Uz)
{
 Ru[0][0]=(float)cos(theta)+(Ux*Ux*(1-cos(theta)));
 Ru[1][0]=(float)Uy*Ux*(1-cos(theta))+(Uz*sin(theta));
 Ru[2][0]=(float)Uz*Ux*(1-cos(theta))-(Uy*sin(theta));

 Ru[0][1]=(float)Uy*Ux*(1-cos(theta))-(Uz*sin(theta));
 Ru[1][1]=(float)cos(theta)+Uy*Uy*(1-cos(theta));
 Ru[2][1]=(float)Uz*Uy*(1-cos(theta))+Ux*sin(theta);

 Ru[0][2]=(float)Ux*Uz*(1-cos(theta))+Uy*sin(theta);
 Ru[1][2]=(float)Uy*Uz*(1-cos(theta))-Ux*sin(theta);
 Ru[2][2]=(float)cos(theta)+Uz*Uz*(1-cos(theta)); 

 Point tempvsx((float)vsx.getX()*Ru[0][0]+vsx.getY()*Ru[0][1]+vsx.getZ()*Ru[0][2]  , (float)vsx.getX()*Ru[1][0]+vsx.getY()*Ru[1][1]+vsx.getZ()*Ru[1][2]  ,  (float)vsx.getX()*Ru[2][0]+vsx.getY()*Ru[2][1]+vsx.getZ()*Ru[2][2]);
 Point tempvsy((float)vsy.getX()*Ru[0][0]+vsy.getY()*Ru[0][1]+vsy.getZ()*Ru[0][2]  , (float)vsy.getX()*Ru[1][0]+vsy.getY()*Ru[1][1]+vsy.getZ()*Ru[1][2]  ,  (float)vsy.getX()*Ru[2][0]+vsy.getY()*Ru[2][1]+vsy.getZ()*Ru[2][2]);
 Point tempvsz((float)vsz.getX()*Ru[0][0]+vsz.getY()*Ru[0][1]+vsz.getZ()*Ru[0][2]  , (float)vsz.getX()*Ru[1][0]+vsz.getY()*Ru[1][1]+vsz.getZ()*Ru[1][2]  ,  (float)vsz.getX()*Ru[2][0]+vsz.getY()*Ru[2][1]+vsz.getZ()*Ru[2][2]);

 vsx=tempvsx;
 vsy=tempvsy;
 vsz=tempvsz;
}


void renderS(void)
{
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glutSwapBuffers();

 
}

void render(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //gluPerspective(45, 4.0/3, 0.1, 2);

 
 if(preDraw)
  {
    glDrawBuffer(GL_FRONT);
    preDraw=false;
  }
 
 else 
  glDrawBuffer(GL_BACK);
 
  
 
 
 //glDrawBuffer(GL_FRONT);
  
  
  glLoadIdentity();



 
       

 
 
  glRotatef(angle,xx,yy,zz) ;
 
 glScalef(zoom, zoom, zoom);
  
  cout<<" glRot=("<<angle;
  cout<<","<<xx;
  cout<<","<<yy;
  cout<<","<<zz<<") ";
  cout<<"  zoom:"<<zoom<<endl;

  cout<<"vsx=("<<vsx.getX()<<","<<vsx.getY()<<","<<vsx.getZ()<<")"<<endl;

  cout<<"vsy=("<<vsy.getX()<<","<<vsy.getY()<<","<<vsy.getZ()<<")"<<endl;

  cout<<"vsz=("<<vsz.getX()<<","<<vsz.getY()<<","<<vsz.getZ()<<")"<<endl;

  

glBegin(GL_LINES);
 
 /*base orthonormé fixe*/

  glColor3f(1,1,1);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);

 
glEnd();
 
 

  
 

 
 
 glBegin(GL_LINES);

  
  /*vector axe de rotationt*/
 glColor3f(1,0,1);
  glVertex3f(0,0,0);
  glVertex3f(xx,yy,zz);
  /*fin axe*/
 
 glEnd();

 //ajout de points automatiques pour tests
 /*
 if(cptPointSais<15)
 { Point *pTemp;
    for(int i=0;i<14;i++)
    { pTemp=new Point((float)i/10,(float)i/10,0);
      
      gab.tabP[i][0]=pTemp;
    }
    gab.tabP[14][0]=(new Point (0,1.4,0));
    cptPointSais=15;
    gab.addautrepoint();
    glEnable(GL_MULTISAMPLE);
   glEnable(GL_LIGHTING);   //lumieres.
   GLfloat specular[] = {1.0f, 1.0f, 1.0f , 1.0f};
   GLfloat globalAmbient[] = { 0.2f, 0.2f, 0.2f, 0.2f };
   GLfloat diffuseLight[] = {1, 1, 1, 1};
   GLfloat lightPosition[] = {1.0f, 0.0f, 1.5f, 1};
   
    //glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbient);

    
    glEnable(GL_LIGHT0);
 }
 */

 //cptPointSais : comme son nom l'indique, nous permet de savoir combien de point nous avons déjà saisie.
 if ((cptPointSais<14) && mousegAc) //si le boolean mousegAc == true alors un point viens d'être 
                                    //saisie par l'utilisateur en coordonnées fenêtre.
 { cout<<"ModeSaisie;"<<endl;
 
   GetOGLPosSai(mouseRecup.getX(),mouseRecup.getY()); //pasage de cfen en cmv
   //Psasie: variable global de type Point contient les cmv du point saisie par l'utilisateur.
  
   cout<<"Mrecup0=("<<mouseRecup.getX()<<","<<mouseRecup.getY()<<") "; //Affichage des cfen pour tests.

     if(Psaisie.getX()>0) //on assure que le point saisie se trouve à droite de l'axe y0.
      {
        
        if(cptPointSais==0) 
        { //pour bien placer le point ps0 dans l'axe par rapport au premier
          //point ps1 saisie par l'utilisateur , on crée
          //automatiquement le point ps0 tel que ps0y=ps1y et ps0x=0. Autrement dit 
          //le point ps0 se trouve a la même hauteur de ps1 avec ces coordonnées en x égal à 0.
          
          gab.tabP[isaiTabP][0]=new Point (0,Psaisie.getY(),0);  //ps0
          isaiTabP++; //indice pour parcourir gab.tab[][]
          cptPointSais+=1;
        }

       
        gab.tabP[isaiTabP][0]=new Point(Psaisie.getX(),Psaisie.getY(),0); //ps1
        isaiTabP++;        
        cptPointSais+=1;
       
        cout<<"Point saisie ("<<Psaisie.getX()<<","<<Psaisie.getY()<<")"<<endl;
      }
      
      else
      {
        cout<<" Point non saisie "<<endl;
      }
     mousegAc=false; //on remet le booléen a false pour "attendre" le point suivant 
   
   
 }


 
  if(cptPointSais==14)
 { cout<<"P15Y="<<Psaisie.getY()<<endl;
   gab.tabP[isaiTabP][0]=new Point(0, Psaisie.getY(), 0); //ps15 stocké a l'indice 14 avec 
                                                          //yps15 qui sera égal au dernier point saisie 
                                                          //enregistré dans Psasie.
   if(gab.tabP[14][0]->getY()<gab.tabP[0][0]->getY()) //on detecte le sens de la figure
      hautverlebas=true;
   else 
      hautverlebas=false;
   
   gab.addautrepoint();
   cptPointSais++;

   
   
   glEnable(GL_MULTISAMPLE);
   glEnable(GL_LIGHTING);   //lumieres.
   //GLfloat specular[] = {1.0f,1.0f,1.0f,0.0f};
   GLfloat globalAmbient[] = { 0.7f, 0.7f, 0.7f, 0.7f };
   GLfloat diffuseLight[] = {1, 1, 1, 1};
   GLfloat lightPosition[] = {10.0f, 0.0f, 0.0f, 0.f};
   
   // glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbient);

    
    glEnable(GL_LIGHT0);
    
 
    
   
 }



 if(cptPointSais>14)
 { 
  
   GetOGLPosRot(mouseRecup.getX(),mouseRecup.getY());
   /*rotation avec mouse */
   Point temp((p1p2.getX()*cos(M_PI/2))-(p1p2.getY()*sin(M_PI/2)),(p1p2.getX()*sin(M_PI/2))+(p1p2.getY()*cos(M_PI/2)),0);
   
   /*glBegin(GL_LINES);   
      glColor3f(0.5,0.2,1);//affichage du vecteur p1p2 de norme 1
          glVertex3f(0,0,0);
          glVertex3f((float)p1p2.getX(),(float)p1p2.getY(),(float)p1p2.getZ());
       //glVertex3f(0,0,0);
          //glVertex3f(p1p2.getX()/100,p1p2.getY()/100,p1p2.getZ()); //affichage correct de verif

     glColor3f(0.45,0.3,0);//affichage de la perpendiculaire du vecteur p1p2 de norme 1
      glVertex3f(0,0,0);
      glVertex3f(temp.getX(),temp.getY(),temp.getZ());
   glEnd();*/
    
    if(mousemAc)
    {
      vUx=temp;
     Rof(vUx.getX(),vUx.getY(),vUx.getZ());
    }
   
   //indicatif 
   //cout<<" th="<<theta;
   //cout<<endl;
   //cout<<endl;



   
   

   
  
 


    
   glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
   //glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

   Point cNormal,Normal,p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p14,N1,N2,N3,N4,Nm,U,V,vNe1,vNe2;

   Point vp1p4,vp5p4,vp1p5,vp6p5,vp5p8,vp9p8,vp4p7,p8p7,vp1p2,vp8p7,vp4p1,vp2p5,vp6p8;

   Point vp2p1,vp3p2,vp7p5,vp5p6,vp7p4,vp4p5;

   float nNormal=0.0;
        
   for(int i=1; i<13; i++)
    { 
     for(int j=0; j<36;j++)  // for(int j=27; j<45;j++) 
      { 
        glColor4f(0,0.2,1,1);


       
       int ii=i;
       if(polyGVis)
       glBegin(GL_POLYGON);
        
         for(int jj = j ; jj<j+2 ; jj++)
          { 
            
           
            p1=(gab.tabP[ii-1][mod(jj-1,36)]->getX()*vsx)+(gab.tabP[ii-1][mod(jj-1,36)]->getY()*vsy)+(gab.tabP[ii-1][mod(jj-1,36)]->getZ()*vsz);
            p2=(gab.tabP[ii-1][jj%36]->getX()*vsx)+(gab.tabP[ii-1][jj%36]->getY()*vsy)+(gab.tabP[ii-1][jj%36]->getZ()*vsz);
            p3=(gab.tabP[ii-1][(jj+1)%36]->getX()*vsx)+(gab.tabP[ii-1][(jj+1)%36]->getY()*vsy)+(gab.tabP[ii-1][(jj+1)%36]->getZ()*vsz); 
          
            p4=(gab.tabP[ii][mod(jj-1,36)]->getX()*vsx)+(gab.tabP[ii][mod(jj-1,36)]->getY()*vsy)+(gab.tabP[ii][mod(jj-1,36)]->getZ()*vsz);
            p5=(gab.tabP[ii][jj%36]->getX()*vsx)+(gab.tabP[ii][jj%36]->getY()*vsy)+(gab.tabP[ii][jj%36]->getZ()*vsz);
            p6=(gab.tabP[ii][(jj+1)%36]->getX()*vsx)+(gab.tabP[ii][(jj+1)%36]->getY()*vsy)+(gab.tabP[ii][(jj+1)%36]->getZ()*vsz); 
          
            p7=(gab.tabP[ii+1][mod(jj-1,36)]->getX()*vsx)+(gab.tabP[ii+1][mod(jj-1,36)]->getY()*vsy)+(gab.tabP[ii+1][mod(jj-1,36)]->getZ()*vsz);
            p8=(gab.tabP[ii+1][jj%36]->getX()*vsx)+(gab.tabP[ii+1][jj%36]->getY()*vsy)+(gab.tabP[ii+1][jj%36]->getZ()*vsz);
            p9=(gab.tabP[ii+1][(jj+1)%36]->getX()*vsx)+(gab.tabP[ii+1][(jj+1)%36]->getY()*vsy)+(gab.tabP[ii+1][(jj+1)%36]->getZ()*vsz); 
            
            vp1p4=normaliseV(p4-p1);
            vp5p4=normaliseV(p4-p5);

            vp4p7=normaliseV(p7-p4);
            vp8p7=normaliseV(p7-p8);

            vp5p8=normaliseV(p8-p5);
            vp9p8=normaliseV(p8-p9);

            vp2p5=normaliseV(p5-p2);
            vp6p5=normaliseV(p5-p6);


            N1=normaliseV(vp1p4*vp5p4);
            N2=normaliseV(vp4p7*vp8p7);
            N3=normaliseV(vp5p8*vp9p8);
            N4=normaliseV(vp2p5*vp6p5);

            Nm=normaliseV((1.0/4.0)*(N1+N2+N3+N4));
            if(hautverlebas)
            {
              Nm=-1*Nm;
            }

            nNormal=normeV(Nm);

            if(polyGVis)
            {
              glNormal3f(Nm.getX(),Nm.getY(),Nm.getZ());
              glVertex3f(p5.getX(),p5.getY(),p5.getZ());
            }
            
            if(!polyGVis && vNormalVis)
            {  
              glBegin(GL_LINES); //affice les normes de chaque points
              glVertex3f(p5.getX(),p5.getY(),p5.getZ());
              glVertex3f(p5.getX()+(Nm.getX()/9.0f),p5.getY()+(Nm.getY()/9.0f),p5.getZ()+(Nm.getZ()/9.0f));

              glEnd();
            }
            glColor4f(0.5,0.2,0,1);
            //dessinCercle(p5,0.02);
            glColor4f(0,0.2,1,1);  
          }
        
          ii+=1;

         for(int jj = j+1 ; jj>j-1 ; jj--)
          { 
            

            
            p1=(gab.tabP[ii-1][mod(jj-1,36)]->getX()*vsx)+(gab.tabP[ii-1][mod(jj-1,36)]->getY()*vsy)+(gab.tabP[ii-1][mod(jj-1,36)]->getZ()*vsz);
            p2=(gab.tabP[ii-1][jj%36]->getX()*vsx)+(gab.tabP[ii-1][jj%36]->getY()*vsy)+(gab.tabP[ii-1][jj%36]->getZ()*vsz);
            p3=(gab.tabP[ii-1][(jj+1)%36]->getX()*vsx)+(gab.tabP[ii-1][(jj+1)%36]->getY()*vsy)+(gab.tabP[ii-1][(jj+1)%36]->getZ()*vsz); 
          
            p4=(gab.tabP[ii][mod(jj-1,36)]->getX()*vsx)+(gab.tabP[ii][mod(jj-1,36)]->getY()*vsy)+(gab.tabP[ii][mod(jj-1,36)]->getZ()*vsz);
            p5=(gab.tabP[ii][jj%36]->getX()*vsx)+(gab.tabP[ii][jj%36]->getY()*vsy)+(gab.tabP[ii][jj%36]->getZ()*vsz);
            p6=(gab.tabP[ii][(jj+1)%36]->getX()*vsx)+(gab.tabP[ii][(jj+1)%36]->getY()*vsy)+(gab.tabP[ii][(jj+1)%36]->getZ()*vsz); 
           
            p7=(gab.tabP[ii+1][mod(jj-1,36)]->getX()*vsx)+(gab.tabP[ii+1][mod(jj-1,36)]->getY()*vsy)+(gab.tabP[ii+1][mod(jj-1,36)]->getZ()*vsz);
            p8=(gab.tabP[ii+1][jj%36]->getX()*vsx)+(gab.tabP[ii+1][jj%36]->getY()*vsy)+(gab.tabP[ii+1][jj%36]->getZ()*vsz);
            p9=(gab.tabP[ii+1][(jj+1)%36]->getX()*vsx)+(gab.tabP[ii+1][(jj+1)%36]->getY()*vsy)+(gab.tabP[ii+1][(jj+1)%36]->getZ()*vsz); 
            
            vp1p4=normaliseV(p4-p1);
            vp5p4=normaliseV(p4-p5);

            vp4p7=normaliseV(p7-p4);
            if(ii!=13)
             vp8p7=normaliseV(p7-p8);
            else
             vp7p5=normaliseV(p5-p7);


            vp5p8=normaliseV(p8-p5);
            if(ii!=13)
             vp9p8=normaliseV(p8-p9);
            else
             vp6p5=normaliseV(p5-p6);

            vp2p5=normaliseV(p5-p2);
            vp6p5=normaliseV(p5-p6);           

            N1=normaliseV(vp1p4*vp5p4);

        
            if(ii!=13)
            N2=normaliseV(vp4p7*vp8p7);
            else
            N2=normaliseV(vp7p5*vp4p7);


            if(ii!=13)
            N3=normaliseV(vp5p8*vp9p8);
            else
            N3=normaliseV(vp5p8*vp6p5);


            N4=normaliseV(vp2p5*vp6p5);

            Nm=normaliseV((1.0/4.0)*(N1+N2+N3+N4));
            if(hautverlebas)
            {
              Nm=-1*Nm;
            }
           
         
            if(!polyGVis && vNormalVis)
            {
             glBegin(GL_LINES); //affice les normes de chaque points
              glVertex3f(p5.getX(),p5.getY(),p5.getZ());
              glVertex3f(p5.getX()+(Nm.getX()/9.0f),p5.getY()+(Nm.getY()/9.0f),p5.getZ()+(Nm.getZ()/9.0f));
             glEnd();
            }
           
            if(polyGVis)
            {  

              glNormal3f(Nm.getX(),Nm.getY(),Nm.getZ());
              glVertex3f(p5.getX(),p5.getY(),p5.getZ());
            }
          }

          
       if(polyGVis)
       glEnd();
      }
    }
   
    



    //remplissage de l'extrémité1 polygons et lumieres
        
   int ii=0;
   glColor4f(0,0.2,1,1);
   //glColor3f(1,0.3,0.1);
   //glColor3f(0.3,1,0.1);
   if(polyGVis)
   glBegin(GL_POLYGON);
    
  for(int jj = 0 ; jj<=37 ; jj++) // for(int jj = 26 ; jj<=46 ; jj++)  
   { 
     
      
     p1=p2=p3=(gab.tabP[ii][mod(jj,36)]->getX()*vsx)+(gab.tabP[ii][mod(jj,36)]->getY()*vsy)+(gab.tabP[ii][mod(jj,36)]->getZ()*vsz);
      
      p4=(gab.tabP[ii+1][mod(jj,36)]->getX()*vsx)+(gab.tabP[ii+1][mod(jj,36)]->getY()*vsy)+(gab.tabP[ii+1][mod(jj,36)]->getZ()*vsz);
      p5=(gab.tabP[ii+1][mod((jj+1),36)]->getX()*vsx)+(gab.tabP[ii+1][mod((jj+1),36)]->getY()*vsy)+(gab.tabP[ii+1][mod((jj+1),36)]->getZ()*vsz);
      p6=(gab.tabP[ii+1][mod((jj+2),36)]->getX()*vsx)+(gab.tabP[ii+1][mod((jj+2),36)]->getY()*vsy)+(gab.tabP[ii+1][mod((jj+2),36)]->getZ()*vsz); 
      
      p7=(gab.tabP[ii+2][mod(jj,36)]->getX()*vsx)+(gab.tabP[ii+2][mod(jj,36)]->getY()*vsy)+(gab.tabP[ii+2][mod(jj,36)]->getZ()*vsz);
      p8=(gab.tabP[ii+2][mod((jj+1),36)]->getX()*vsx)+(gab.tabP[ii+2][mod((jj+1),36)]->getY()*vsy)+(gab.tabP[ii+2][mod((jj+1),36)]->getZ()*vsz);
      p9=(gab.tabP[ii+2][mod((jj+2),36)]->getX()*vsx)+(gab.tabP[ii+2][mod((jj+2),36)]->getY()*vsy)+(gab.tabP[ii+2][mod((jj+2),36)]->getZ()*vsz); 
      
   
      vp1p4=normaliseV(p4-p1);
      vp5p4=normaliseV(p4-p5);

      vp4p7=normaliseV(p7-p4);
      vp8p7=normaliseV(p7-p8);

      vp5p8=normaliseV(p8-p5);
      vp9p8=normaliseV(p8-p9);

      vp2p5=normaliseV(p5-p2);
      vp6p5=normaliseV(p5-p6);

      N1=normaliseV(vp1p4*vp5p4);
      N2=normaliseV(vp4p7*vp8p7);
      N3=normaliseV(vp5p8*vp9p8);
      N4=normaliseV(vp2p5*vp6p5);

      Nm=normaliseV((1.0/4.0)*(N1+N2+N3+N4));
      if(hautverlebas)
      {
        Nm=-1*Nm;
      }

      
      //nNormal=normeV(Nm);

      if(polyGVis)
      {  
        glNormal3f(Nm.getX(),Nm.getY(),Nm.getZ());
        glVertex3f(p5.getX(),p5.getY(),p5.getZ());
        
        if((jj+1)%2==0)
        { if(!hautverlebas)
          vNe1=-1*vsy; //normal de p0
          else
          vNe1=1*vsy; //normal de p0 

          glNormal3f(vNe1.getX(),vNe1.getY(),vNe1.getZ());

          p0=(gab.tabP[0][0]->getX()*vsx)+(gab.tabP[0][0]->getY()*vsy)+(gab.tabP[0][0]->getZ()*vsz);

          glVertex3f(p0.getX(),p0.getY(),p0.getZ());
          glEnd();

          
         glBegin(GL_POLYGON);
         glNormal3f(Nm.getX(),Nm.getY(),Nm.getZ());
         glVertex3f(p5.getX(),p5.getY(),p5.getZ());

        }
      }
   }
   if(polyGVis)
   glEnd();
    
  
   
 
   //remplissage de l'extrémité2

   ii=12;
   if(polyGVis)


    glBegin(GL_POLYGON);
     //glColor3f(0.3,1,0.1);
   for(int jj = 0 ; jj<=37 ; jj++) //for(int jj = 36 ; jj>=-1 ; jj--) //for(int jj = 25 ; jj<=45 ; jj++)
   { 
      
      p1=(gab.tabP[ii][mod(jj,36)]->getX()*vsx)+(gab.tabP[ii][mod(jj,36)]->getY()*vsy)+(gab.tabP[ii][mod(jj,36)]->getZ()*vsz);
      p2=(gab.tabP[ii][mod((jj+1),36)]->getX()*vsx)+(gab.tabP[ii][mod((jj+1),36)]->getY()*vsy)+(gab.tabP[ii][mod((jj+1),36)]->getZ()*vsz);
      p3=(gab.tabP[ii][mod((jj+2),36)]->getX()*vsx)+(gab.tabP[ii][mod((jj+2),36)]->getY()*vsy)+(gab.tabP[ii][mod((jj+2),36)]->getZ()*vsz); 
      
      p4=(gab.tabP[ii+1][mod(jj,36)]->getX()*vsx)+(gab.tabP[ii+1][mod(jj,36)]->getY()*vsy)+(gab.tabP[ii+1][mod(jj,36)]->getZ()*vsz);
      p5=(gab.tabP[ii+1][mod((jj+1),36)]->getX()*vsx)+(gab.tabP[ii+1][mod((jj+1),36)]->getY()*vsy)+(gab.tabP[ii+1][mod((jj+1),36)]->getZ()*vsz);
      p6=(gab.tabP[ii+1][mod((jj+2),36)]->getX()*vsx)+(gab.tabP[ii+1][mod((jj+2),36)]->getY()*vsy)+(gab.tabP[ii+1][mod((jj+2),36)]->getZ()*vsz); 
      
      p7=p8=p9=(gab.tabP[ii+2][mod(jj,36)]->getX()*vsx)+(gab.tabP[ii+2][mod(jj,36)]->getY()*vsy)+(gab.tabP[ii+2][mod(jj,36)]->getZ()*vsz);
      //p8=(gab.tabP[ii+2][mod((jj+1),36)]->getX()*vsx)+(gab.tabP[ii+2][mod((jj+1),36)]->getY()*vsy)+(gab.tabP[ii+2][mod((jj+1),36)]->getZ()*vsz);
      //p9=(gab.tabP[ii+2][mod((jj+2),36)]->getX()*vsx)+(gab.tabP[ii+2][mod((jj+2),36)]->getY()*vsy)+(gab.tabP[ii+2][mod((jj+2),36)]->getZ()*vsz); 
        
   
      vp1p4=normaliseV(p4-p1);
      vp5p4=normaliseV(p4-p5);

      vp4p7=normaliseV(p7-p4);
      vp7p5=normaliseV(p5-p7);


      vp5p8=normaliseV(p8-p5);
      vp6p5=normaliseV(p5-p6);

      vp2p5=normaliseV(p5-p2);
      vp6p5=normaliseV(p5-p6);           

      N1=normaliseV(vp1p4*vp5p4);
      N2=normaliseV(vp7p5*vp4p7);
      N3=normaliseV(vp5p8*vp6p5);
      N4=normaliseV(vp2p5*vp6p5);

      Nm=normaliseV((1.0/4.0)*(N1+N2+N3+N4));
      if(hautverlebas)
      {
       Nm=-1*Nm;
      }

      
      //nNormal=normeV(Nm);

      if(polyGVis)
      {
        glNormal3f(Nm.getX(),Nm.getY(),Nm.getZ());
        glVertex3f(p5.getX(),p5.getY(),p5.getZ());

        if((jj+1)%2==0)
        { 
          if(!hautverlebas)
          vNe2=1*vsy; //normal de p14
          else
          vNe2=-1*vsy; //normal de p14 
          


          glNormal3f(vNe2.getX(),vNe2.getY(),vNe2.getZ());

          p14=(gab.tabP[14][0]->getX()*vsx)+(gab.tabP[14][0]->getY()*vsy)+(gab.tabP[14][0]->getZ()*vsz);

          glVertex3f(p14.getX(),p14.getY(),p14.getZ());
          glEnd();

          
          glBegin(GL_POLYGON);
          glNormal3f(Nm.getX(),Nm.getY(),Nm.getZ());
          glVertex3f(p5.getX(),p5.getY(),p5.getZ());

        }
      }

      
      
      if(!polyGVis && vNormalVis)
      {glColor4f(0.5,0.2,0,1);
       glBegin(GL_LINES); //purement indicatif

        glVertex3f(p5.getX(),p5.getY(),p5.getZ());
        glVertex3f(p5.getX()+(Nm.getX()/9.0f),p5.getY()+(Nm.getY()/9.0f),p5.getZ()+(Nm.getZ()/9.0f));
       glEnd();
      }

    }
   
   if(polyGVis)
   glEnd();
   

   //affichage des vecteurs normal de chaque surface


   if(vNsurface || flatShading)
   {
      for(int i=0; i<14; i++) 
      { 
       for(int j=0; j<36;j++)
        { glColor4f(0,0.2,1,1);
          
          p1 =(gab.tabP[i][j%36]->getX()*vsx)+(gab.tabP[i][j%36]->getY()*vsy)+(gab.tabP[i][j%36]->getZ()*vsz);
         
          p2=(gab.tabP[i][(j+1)%36]->getX()*vsx)+(gab.tabP[i][(j+1)%36]->getY()*vsy)+(gab.tabP[i][(j+1)%36]->getZ()*vsz);
   
          p3=(gab.tabP[i+1][(j+1)%36]->getX()*vsx)+(gab.tabP[i+1][(j+1)%36]->getY()*vsy)+(gab.tabP[i+1][(j+1)%36]->getZ()*vsz);
   
          p4=(gab.tabP[i+1][j%36]->getX()*vsx)+(gab.tabP[i+1][j%36]->getY()*vsy)+(gab.tabP[i+1][j%36]->getZ()*vsz);
        //on relie pour chaque case 
          if(i==0)
          {
            U=(p3-p2);
            V=(p4-p3);
          }
          else 
          {
            U=(p2-p1);
            V=(p3-p2);
          }

          Normal=U*V;
          if(hautverlebas)
            Normal=(-1)*Normal;

          cNormal=p1 + ((0.8f)*U )+ ((1/2.0f)*V); //centre du vecteur normal
          nNormal=(float)sqrt(((float)Normal.getX()*(float)Normal.getX())+((float)Normal.getY()*(float)Normal.getY())+((float)Normal.getZ()*(float)Normal.getZ())); //norme du vecteur normal

          if(flatShading)
          {
        
            glBegin(GL_POLYGON);
              
              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());
              glVertex3f(p1.getX(),p1.getY(),p1.getZ());

              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());
              glVertex3f(p2.getX(),p2.getY(),p2.getZ());

              glVertex3f(p3.getX(),p3.getY(),p3.getZ());
              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());
             
              glVertex3f(p4.getX(),p4.getY(),p4.getZ());
              glNormal3d( (1.0/nNormal)*Normal.getX()   ,    (1.0/nNormal)*Normal.getY()  ,  (1.0/nNormal)*Normal.getZ());

            glEnd();
          }

          if(vNsurface)
          {  glBegin(GL_LINES); //on affiche le vecteur normalisé a chaque polygon
             glColor3f(0.7,0.5,0);
      
              glVertex3f(cNormal.getX(),cNormal.getY(),cNormal.getZ());
              glVertex3f(cNormal.getX()+( (1.0/(nNormal*6))*Normal.getX()),cNormal.getY()+((1.0/(nNormal*6))*Normal.getY()),cNormal.getZ()+((1.0/(nNormal*6))*Normal.getZ()));
            glEnd();
          }
        }
      }
    }  




   if(lignesVis) //booleen a true si l'utilisteur souhaite afficher les lignes avec la touche 'a'.
   {
   
     glBegin(GL_LINES); 
      glColor4f(0.5,0.2,0,1);
       
      for(int i=0; i<14; i++) //lignes horizontal for(int i=0; i<14; i++) 
      { 
        for(int j=0; j<36;j++)
        { 
           Point temp1=(gab.tabP[i][j%36]->getX()*vsx)+(gab.tabP[i][j%36]->getY()*vsy)+(gab.tabP[i][j%36]->getZ()*vsz);
          glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
          temp1=(gab.tabP[i][(j+1)%36]->getX()*vsx)+(gab.tabP[i][(j+1)%36]->getY()*vsy)+(gab.tabP[i][(j+1)%36]->getZ()*vsz);
          glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
        }
      }

     for(int j=0; j<=36;j++) //lignes vertical
      {
       for(int i=0; i<14; i++)
        { 
          Point temp1=(gab.tabP[i][j%36]->getX()*vsx)+(gab.tabP[i][j%36]->getY()*vsy)+(gab.tabP[i][j%36]->getZ()*vsz);
          glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
          temp1=(gab.tabP[(i+1)%36][j%36]->getX()*vsx)+(gab.tabP[(i+1)%36][j%36]->getY()*vsy)+(gab.tabP[(i+1)%36][j%36]->getZ()*vsz);
          glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
        }  
      }
       
     glEnd();
   }
   
   //theta=0;
  
  }  //fin else if(cptPointSais>14)
 
 if(cptPointSais<=15)
  { 

   glBegin(GL_POINTS); //affichage de points
   glColor3f(1,1,1);
  
   //cout<<"gabSize="<<gab.t().size()<<endl;
   for(size_t i=0; i<cptPointSais; i++)
   {
      Point temp1=(gab.tabP[i][0]->getX()*vsx)+(gab.tabP[i][0]->getY()*vsy)+(gab.tabP[i][0]->getZ()*vsz);
      glVertex3f(0,0,0);
      glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());

      
    }
   glEnd();
 }
  
  if(cptPointSais==15) //permet de ne plus afficher les points un fois tout les points sont saisie
  cptPointSais++;
  
  if (cptPointSais>15 && pointVis)
  {
   glBegin(GL_POINTS); //affichage de points
    glColor3f(1,1,1);
  
    //cout<<"gabSize="<<gab.t().size()<<endl;
    for(int i=0; i<14; i++)
    {
      for(int j=0;j<36;j++)
      {
        Point temp1=(gab.tabP[i][j]->getX()*vsx)+(gab.tabP[i][j]->getY()*vsy)+(gab.tabP[i][j]->getZ()*vsz);
     
        glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
      }
    }
   glEnd();
  }

  glBegin(GL_LINES); //base  Rs
    glColor3f(0,0,1);

        glVertex3f(0,0,0);
        glVertex3f(vsx.getX(),vsx.getY(),vsx.getZ());

    glColor3f(0,1,0);

        glVertex3f(0,0,0);
        glVertex3f(vsy.getX(),vsy.getY(),vsy.getZ());

    glColor3f(1,0,0);
        glVertex3f(0,0,0);
        glVertex3f(vsz.getX(),vsz.getY(),vsz.getZ());
   
   glEnd();

  /*if(cptPointSais>=15)
    dessinCercle(*gab.tabP[14][0],0.1);*/
 //glFlush();
 //glFinish();
 glutSwapBuffers();


}


void dessinCercle(Point & ccent,float r)
{ float th=0;
  
   while(th<=2*M_PI)
   {
      glBegin(GL_POINTS);


      glVertex3f(ccent.getX(),ccent.getY(),ccent.getZ());

      glVertex3f(ccent.getX()+r*cos(th),ccent.getY()+r*sin(th),ccent.getZ());

      th+=M_PI/64;

      glEnd();
   }
   
  glEnd();
}

void GetOGLPosSai(int x, int y)
{
  GLint viewport[4];  //Les valeur de la fenêtre affichée seront stocker ici 
  GLdouble modelview[16];  //Les 16 double correspondant a la matrice de la vue du modèle seront stocker ici
  GLdouble projection[16];   //Les 16 double correspondant a la matrice de projection seront stocker ici
  GLfloat winX, winY, winZ;
  GLdouble posX, posY, posZ;
  glGetDoublev( GL_MODELVIEW_MATRIX, modelview ); //Obtient et stocke les valeur des 16 doubles dans modelview[]
  
  glGetDoublev( GL_PROJECTION_MATRIX, projection ); //Obtient et stocke les valeur des 16 doubles dans projection[]
  
  glGetIntegerv( GL_VIEWPORT, viewport ); //Obtient les valeur X,Y,Longueur,hauteur de l'écran  et les stocke
                                          //dans viewport[] où X et Y correspondes au coordonnées du haut gauche de la fenêtre glut
  
  winX = (float)x;
  winY = (float)viewport[3] - (float)y; //on inverse l'axe des y car de base, le centre de l'axe des x et y se trouve 
                                        //se trouve en haut a gauche, se qui entraîne un axe y qui est dirigé ver le bas. Or 
                                        //notre axe y en cmv se trouve ver le haut.
  //glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
  gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ); 
               //stock dans posX et dans posY les coordonnées du monde virtuel des
               //coordonnées fenetres x et y passée en parametres de la fonction. 
 
 
 Psaisie.setX(posX); //On stocke posX puis posY dans Psaisie, une variable globale de type Point qu'on pourra utiliser plus tard.
 Psaisie.setY(posY);
 Psaisie.setZ(0);

}




void GetOGLPosRot(int x, int y)
{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;
 
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );
 
    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
 
    
    //indicatif
    cout<<" posX="<<posX<<" posY="<<posY<<" posZ="<<posZ;

    Point verif;
    float thetaPP;
   
    if(deuxiemeCli)
    { 
      p2.setX((float)posX*10000);
      p2.setY((float)posY*10000);
      p2.setZ(0);
      
      
       
      verif=(p2-p1); //verification de la norme du nouveau vecteur p2-p1;
      float norme=(float)sqrt((verif.getX()*verif.getX())+(verif.getY()*verif.getY())+(verif.getZ()*verif.getZ()));
      // indicatif
       //cout<<" \e[34m NORME="<<norme<<"\e[0m ";
        
      if((p1!=p2) && (0<(float)norme)) //on evite la division par zero
       {
         thetaPP=(float)acos((float)verif.getX()/(float)norme); //on calcule l'angle de p2-p1
         // indicatif
          //cout<<"\e[32m thP1P2="<<thetaPP*180/M_PI<<"\e[0m"; //on affiche l'angle de p2-p1
       
         if(verif.getY()<0) //on calcule en fonction du signe de Y  l'angle thetaPP
          {
           Point tempp((float)cos(-thetaPP),(float)sin(-thetaPP),0); //on calcul le vecteur representant le vecteur p2-p1 avec un norme de 1
           p1p2=tempp; //p1p2 amplifié avec norme egal 1
          }
           else
          {
            Point tempp((float)cos(thetaPP),(float)sin(thetaPP),0); //on calcul le vecteur representant le vecteur p2-p1 avec un norme de 1
            p1p2=tempp; //p1p2 amplifié avec norme egal 1
          } 
        }
          //p1p2=verif; //le vecteur p2-p1 avec ca norme sans imposition de norme =1
        
     deuxiemeCli=false;
     cout<<endl;
      
     /*titre indicatif
     cout<<"\e[34m np1p2="<<(float)sqrt((p1p2.getX()*p1p2.getX())+(p1p2.getY()*p1p2.getY())+(p1p2.getZ()*p1p2.getZ()))<<" "; //norme du vecteur p1p2
     cout<<" np1="<<(float )sqrt((p1.getX()*p1.getX())+(p1.getY()*p1.getY())+(p1.getZ()*p1.getZ())); //norme du vecteur p1
     cout<<" np2="<<(float )sqrt((p2.getX()*p2.getX())+(p2.getY()*p2.getY())+(p2.getZ()*p2.getZ()))<<"\e[0m"; //norme du vectur p2
     
     cout<<"\e[35m p1=("<<p1.getX()<<","<<p1.getY()<<") "; //coordonees p1
     cout<<" p2=("<<p2.getX()<<","<<p2.getY()<<") "; //coordonees p2
     cout<<" p1p2=("<<p1p2.getX()<<","<<p1p2.getY()<<") \e[0m"; //coordonees p1p2*/
     //cout<<endl;
     p2.setX(0);
     p2.setY(0);
     p1.setX(0);
     p1.setY(0);
     
     theta=norme*M_PI/(64*500);
     
    }

    else if(!deuxiemeCli)
    {
      p1.setX((float)posX*10000);
      p1.setY((float)posY*10000);
      p1.setZ(0);
      deuxiemeCli=true;
    }
 

    verif.setX(posX*100);
    verif.setY(posY*100);
    thetaPP=(float)acos((float)verif.getX()/(sqrt((float)(verif.getX()*verif.getX())+(verif.getY()*verif.getY()))));

    //indicatif 
    //cout<<"\e[33m tp2="<<thetaPP*180/M_PI<<"\e[0m";
    
  
}


void mouseB(int button, int state, int x, int y)
{
  switch (button)
    {
      
     case (GLUT_MIDDLE_BUTTON):
       if(state==GLUT_UP)
        {
         mousemAc=false;
         deuxiemeCli=false;

        } 

        else if(state==GLUT_DOWN)
        {
         mousemAc=true;
         deuxiemeCli=false;
        }    
      break;
     
      case (GLUT_LEFT_BUTTON):
       
      if(state==GLUT_DOWN)
        {
         mousegAc=true;
         mouseRecup.setX(x);
         mouseRecup.setY(y);
         glutPostRedisplay();
        }    
      break;
   }
}

void mouseM(int x,int y)
{ if(mousemAc)
  {//indicatif
   //std::cout<<" Souris a:"<<x<<" "<<y;
   mouseRecup.setX(x);
   mouseRecup.setY(y);
   glutPostRedisplay();
  }
}

void keyboard(unsigned char key, int x, int y) 
{


  switch (key) 
  {

   case 'y':
    zoom += 0.025;
        
    glutPostRedisplay();
   break;

  case 'h':
    zoom -=0.025;
      
    glutPostRedisplay() ;
  break ;


  case 'p':
    angle ++ ;
    
    glutPostRedisplay() ;
  break ;

  case 'm':
    angle --;
    
    glutPostRedisplay();
  break;

  case 'u':
    xx+=0.05;
    
    glutPostRedisplay() ;
  break;

  case 'j':
    xx-=0.05;
    
    glutPostRedisplay() ;
  break;

  case 'i':
    yy+=0.05;

    glutPostRedisplay() ;
  break;

  case 'k':
    yy-=0.05;

    glutPostRedisplay() ;
  break;

  case 'o':
    zz+=0.05;
    
    glutPostRedisplay() ;
  break;

  case 'l':
    zz-=0.05;
    
    glutPostRedisplay() ;
  break;

  case 't':  //vue dessus
    angle = 0.0;
    xx=0;
    yy=0;
    zz=0;
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(0);
    vsy.setZ(1);

    vsz.setX(0);
    vsz.setY(-1);
    vsz.setZ(0);

    glutPostRedisplay();
  break;

  case 'g': //vue dessous
    angle = 0.0;
    xx=0;
    yy=0;
    zz=0;
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(0);
    vsy.setZ(-1);

    vsz.setX(0);
    vsz.setY(1);
    vsz.setZ(0);

    glutPostRedisplay();

  break;
  /*
  case'g':
  theta-=M_PI/16;
  glutPostRedisplay();
  break;*/

  case 'r':
    angle = 0.0; //vue par default
    xx=0;
    yy=0;
    zz=0;
    theta=M_PI/64;;

    zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(0);
    vsz.setY(0);
    vsz.setZ(1);



    glutPostRedisplay() ;
  break;

    case 'f':
    angle = 0.0; //vue par default
    xx=0;
    yy=0;
    zz=0;
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(0);
    vsx.setY(0);
    vsx.setZ(1);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(-1);
    vsz.setY(0);
    vsz.setZ(0);



    glutPostRedisplay() ;
  break;

    case 'd':
    angle = 0.0; //vue par default
    xx=0;
    yy=0;
    zz=0;
    theta=M_PI/64;;

    //zoom=0.70;

    vsx.setX(0);
    vsx.setY(0);
    vsx.setZ(-1);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(1);
    vsz.setY(0);
    vsz.setZ(0);



    glutPostRedisplay() ;
  break;

  case 'v':
    angle = 0.0;
    xx=0;
    yy=0;
    zz=0;
    theta=M_PI/64;

    mousegAc=false;
    mousemAc=false;
    deuxiemeCli=false;

    vNormalVis=false;

   lignesVis=true;
   polyGVis=false;
   pointVis=false;
   vNsurface=false;
   hautverlebas=false;

    zoom=0.70;

    vsx.setX(1);
    vsx.setY(0);
    vsx.setZ(0);

    vsy.setX(0);
    vsy.setY(1);
    vsy.setZ(0);

    vsz.setX(0);
    vsz.setY(0);
    vsz.setZ(1);


    for (int i=0; i<15;i++)
    {
      for(int j=0;j<37;j++)
      {
        delete [] gab.tabP[i][j];
      
        // delete [] gab.tabP[i];
        gab.tabP[i][j]=NULL;
      }
      
    }



    

 
    isaiTabP=0;

    cptPointSais=0;
    mousegAc=false;
    glutPostRedisplay() ;
    break;
    
    case 'a':

    if (lignesVis)
       lignesVis=false;
     else
       lignesVis=true;
    glutPostRedisplay();

    break;


    case 'q':

    if (polyGVis)
       polyGVis=false;
     else if(flatShading)
     { 
       flatShading=false;
       polyGVis=true;
      }
      else
        polyGVis=true;
    glutPostRedisplay();

    break;

    case 's':
    if(!flatShading && polyGVis)
      {
        flatShading=true;
        polyGVis=false;
      }

    else if(flatShading)
    {
      flatShading=false;
    }

    else
    {
      flatShading=true;
    }
    glutPostRedisplay();
    break;

    case 'x':

    if (vNsurface)
       vNsurface=false;
     else
       vNsurface=true;
    glutPostRedisplay();

    break;


    case 'w':
    
     if (vNormalVis)
       vNormalVis=false;
     else if (polyGVis)
      {
       polyGVis=false;
       vNormalVis=true;
      }
      else 
      {
        vNormalVis=true;
      }
    glutPostRedisplay();

    break;

    case 'z':
    if (pointVis)
      pointVis=false;
    else
      pointVis=true;

    glutPostRedisplay();

    break;
   
    case 27 /* Esc */:
    exit(1) ;

  } 
}