#ifndef POINT_H
#define POINT_H
#include <iostream>
using namespace std;

class Point{

	//attributs

	private:

		float x;
		float y;
		float z;
		bool actif;

	//methodes
	
	public:

		//constructeurs
		Point();
		Point(float _x,float _y,float _z);
		Point &operator=(Point &p2);
		
		Point &operator+(Point &p2);

		Point &operator-(Point &p2);

		bool operator==(Point &p2);

		bool operator!=(Point &p2);

		Point &operator * (Point &p2); //produit vectorielle

		


		//destructeur
		~Point();

		//accesseurs en lecture
		float getX();
		float getY();
		float getZ();
		bool estActif();

		//accesseurs en ecriture
		void setX(float x);
		void setY(float y);
		void setZ(float z);
		void setEtat(bool b);

};
Point &operator*(float scal,Point &p1); 


#endif   



