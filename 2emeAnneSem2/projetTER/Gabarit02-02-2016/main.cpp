#include "Tabpoint.h"
#include <iostream>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

Tabpoint gab;

GLfloat angle = 0.0 ;
GLfloat xx=1.0;
GLfloat yy=1.0;
GLfloat zz=1.0;

void keyboard(unsigned char key, int x, int y);

void render(void);

int main(int argc, char** argv,char *envp[])
{
	
Point p0, p15;
p0=Point(0,0,0);
p15=Point(4, 15, 0);
gab.addpoint(p0);
for(int i=1; i<14; i++)
{
	Point p;
	p=Point((float)i/10,(float)i/10,0);
	gab.addpoint(p);
}
gab.addpoint(p15);
gab.addautrepoint(gab);

 glutInit(&argc, argv);
 //glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
 glutInitWindowPosition(100,100);
 glutInitWindowSize(640,480); 	//Optionnel
 glutCreateWindow("Ma première fenêtre OpenGL !");
  
 glutDisplayFunc(render);
 glutKeyboardFunc(keyboard) ;

 glutMainLoop();



//glBegin(GL_POINTS);
/*for(size_t i=0; i<gab.t().size(); i++)
{
	glVertex3f(gab.t()[i].getX(),gab.t()[i].getY(),gab.t()[i].getZ());
}
*/
//glutCreateWindow(argv[0]);

	return 0;
}

void render(void)
{

 //cout << "\033[2J\033[1;1f";
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glRotatef(angle,xx,yy,zz) ;
 
  cout<<"angledeg: "<<angle;
  cout<<"  x : "<<xx;
  cout<<"  y : "<<yy;
  cout<<"  z : "<<zz<<endl;
  glBegin(GL_POINTS);
 for(size_t i=0; i<gab.t().size(); i++)
 {
 	glVertex3f(gab.t()[i].getX(), gab.t()[i].getY(), gab.t()[i].getZ());
 }
glEnd();

glBegin(GL_LINES);
 	glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);
 	glVertex3f(1,1,1);
glEnd();


 glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y) {
switch (key) {

case 'p':
angle ++ ;
glutPostRedisplay() ;
break ;

case 'm':
angle --;
glutPostRedisplay();
break;

case 'u':
xx+=0.01;
glutPostRedisplay() ;
break;

case 'j':
xx-=0.01;
glutPostRedisplay() ;
break;

case 'i':
yy+=0.01;
glutPostRedisplay() ;
break;

case 'k':
yy-=0.01;
glutPostRedisplay() ;
break;

case 'o':
zz+=0.01;
glutPostRedisplay() ;
break;

case 'l':
zz-=0.01;
glutPostRedisplay() ;
break;



case 27 /* Esc */:
exit(1) ;

}
}