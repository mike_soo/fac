\select@language {french}
\contentsline {section}{Introduction}{3}{section*.1}
\contentsline {section}{\numberline {1}OpenGL / Glut }{3}{section.1.1}
\contentsline {section}{\numberline {2}Domaine d'application}{3}{section.1.2}
\contentsline {section}{\numberline {3}D\IeC {\'e}roulement du projet}{3}{section.1.3}
\contentsline {section}{Projet}{4}{section.1.3}
\contentsline {section}{\numberline {1}Manuel d'utilisation}{4}{section.2.1}
\contentsline {section}{\numberline {2}\IeC {\`A} savoir}{4}{section.2.2}
\contentsline {section}{\numberline {3}Saisie et Stockage des points}{5}{section.2.3}
\contentsline {subsection}{\numberline {3.1}Saisie}{5}{subsection.2.3.1}
\contentsline {subsection}{\numberline {3.2}Stockage}{7}{subsection.2.3.2}
\contentsline {subsection}{\numberline {3.3}Stockage et saisie}{8}{subsection.2.3.3}
\contentsline {section}{\numberline {4}2D vers 3D}{10}{section.2.4}
\contentsline {section}{\numberline {5}Remplissage de l'objet}{11}{section.2.5}
\contentsline {subsection}{\numberline {5.1}Remplissage par fils de fers}{12}{subsection.2.5.1}
\contentsline {subsection}{\numberline {5.2}Remplissage avec polygones}{13}{subsection.2.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Remplissage extr\IeC {\'e}mit\IeC {\'e} 1 et 2}{14}{subsubsection.2.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Remplissage du corps de l'objet}{16}{subsubsection.2.5.2.2}
\contentsline {section}{\numberline {6}Ombrage Plat (flat shading)}{18}{section.2.6}
\contentsline {section}{\numberline {7}Ombrage Phong (Phong shading)}{19}{section.2.7}
\contentsline {subsection}{\numberline {7.1}Remplissage avec Ombrage Phong: extr\IeC {\'e}mit\IeC {\'e} 1}{20}{subsection.2.7.1}
\contentsline {subsection}{\numberline {7.2}Remplissage avec Ombrage Phong: extr\IeC {\'e}mit\IeC {\'e} 2}{22}{subsection.2.7.2}
\contentsline {section}{\numberline {8}Rotation Gabarit}{24}{section.2.8}
\contentsline {section}{Conclusion}{27}{section.2.8}
\contentsline {section}{\numberline {1}Gr\IeC {\^a}ce au projet}{28}{section.3.1}
\contentsline {section}{\numberline {2}Sources utilis\IeC {\'e}es}{28}{section.3.2}
