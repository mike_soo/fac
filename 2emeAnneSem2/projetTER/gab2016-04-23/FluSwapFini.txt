In simple terms, glFlush forces the renderer to start rendering if it hasn't already started rendering. This is useful only in certain situations.

In simple terms, SwapBuffers displays the current frame and starts a new frame.

There is also glFinish(), which simply waits until rendering is done before returning. It is useful when rendering into a texture, preventing the texture from being used before it is complete. 