

glBegin(GL_POLYGON); //Remplissage extrémité1

	ii:=0;

	  p1:=gab.tabP[0][0];
      
      p4:=gab.tabP[1][0];
      p5:=gab.tabP[1][1];
     
	  //On appel vpapb tout vecteur reliant les points a et b dans le sens d'apparition des
      //points dans le nom, tel que vpapb c'est le vecteur qui va de a vers b.

      vp1p4:=p4-p1;  //Soustraction des coordonnées grâce à une éventuelle surcharge 
      vp5p4:=p4-p5;	 //d'opérateurs.


      vN:=vp1p4*vp5p4; //Produit vectorielle.
      si  la surface de extrémité1 se trouve en haut de extémité2
         alors  vN:=-vN;  //On oriente les vecteur normal par rapport à la position des
          				  //points p0,0 et p15,0, ces deux points étant le centre
         				  //des extrémités 1 et 2 respectivement; il suffit de savoir, grâce à leur coordonnées en 
         				  //y , lequel des deux est au dessus de l'autre, ce qui nous permettra de définir
                          //le sens des vecteur normal
      fin si  

      glNormal(normalise(vN)); //On s'aperçoit qu'on a un seul vecteur normal pour toute
      						   //la surface ce qui est suffisant car les extrémités sont 
      							//coplanaire.

	Pour jj allant de 0 à 35;

	   glVertex3f(gab.tab[ii+1][jj+1]); //On appel p5 ce point; écriture simplifié de la fonction glVertex3f                     
	  
	   //On rejoint tous les points entre eux d'indices [1][j] car si vous reprenez 
	   //la relation entre la position des points dans le tableau par rapport à l'objet vous constaterai
	   //qu'on gère les points qui constitue la surface colorée (verte ou orange, la couleur dépend 
	   //toujours du sens de la saisie) dans l'image précédente. 
	   
	       //Par la suite on remplira ce pseudocode avec la partie responsable du calcul des vecteurs normaux 
	       //aux surfaces. En effet les deux taches (remplissages et lumières) sont effectuées en 
	       //même temps.  
	Fin pour;
glEnd();

glBegin(GL_POLYGON); //Remplissage extrémité2.
	
	ii:=12;

	p4:=gab.tabP[13][0]

    p5:=(gab.tabP[13][1]

    p7:=(gab.tabP[14][0]

     
	  //On appel vpapb tout vecteur reliant les points a et b dans le sens d'apparition des
      //points dans le nom, tel que vpapb c'est le vecteur qui va de a vers b.

      vp4p7:=p7-p4;  //Soustraction des coordonnées grâce à une éventuelle surcharge 
      vp7p5:=p5-p7;	 //d'opérateurs.


      vN:=vp7p5*vp4p7; //Produit vectorielle.
      si  la surface de extrémité1 se trouve en haut de extémité2
         alors  vN:=-vN;  //On oriente les vecteur normal par rapport à la position des
          				  //points p0,0 et p15,0, ces deux points étant le centre
         				  //des extrémités 1 et 2 respectivement; il suffit de savoir, grâce à leur coordonnées en 
         				  //y , lequel des deux est au dessus de l'autre, ce qui nous permettra de définir
                          //le sens des vecteur normal.
      fin si  

      glNormal(normalise(vN)); //On s'aperçoit qu'on a un seul vecteur normal pour toute
      						   //la surface ce qui est suffisant car les extrémités sont 
      							//coplanaire.

	

	Pour jj allant de 36 à 0

	   glVertex3f(gab.tab[ii+1][jj+1]); //On appel p5 ce point; écriture simplifié de la fonction glVertex3f.                     
	  
	    //Pareil que pour le premier parcours, les raisons du choix des indices vous sera justifié lors 
	   //de l'explication du calcul des vecteur normal. 
	Fin pour
glEnd();


pour i allant de 0 à 12 //Remplissage corps.
	pour j allant de 0 a 35

		ii=i;
		glBegin(GL_POLYGON)
			
			pour jj allant de j à j+1 
				glVertex3f(gab.tabP[ii][jj]);
			fin pour
			
			ii++;

			pour jj allant de j+1 à j 
				glVertex3f(gab.tabP[ii][jj]);
			fin pour
			
		glEnd()
	fin pour
fin pour


