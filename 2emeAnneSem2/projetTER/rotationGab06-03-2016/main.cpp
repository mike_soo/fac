
#include <iostream>
#include <math.h>
#include <vector>
#include <cstdlib>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "Point.h"
#include "Tabpoint.h"
# define M_PI           3.14159265358979323846

using namespace std;
int cptP=0;
Tabpoint gab;
GLfloat angle = 0.0;
/*axe de rotation*/
GLfloat xx=0;
GLfloat yy=0;
GLfloat zz=0;
float phi=0;
float theta=0;
float psi=0;
Point mouseRecup; //stock les coordonées du mouse recuperées en px;
Point p1;
Point p2;
Point p1p2;
bool mouseAc=false;
bool deuxemeCli=false;
 


/*axe de rotation*/
float zoom=0.70;






void keyboard(unsigned char key, int x, int y);

void mouseB(int button, int state, int x, int y);

void mouseM(int x, int y);
void render(void);
void renderS(void);
//GLint gluUnProject (GLdouble winX, GLdouble winY, GLdouble winZ, const GLdouble *model, const GLdouble *proj, const GLint *view,GLdouble* objX, GLdouble* objY, GLdouble* objZ);

void GetOGLPos(int x, int y);

Point operator+(Point p1,Point p2);

Point operator*(float scal,Point p1);


int main(int argc, char** argv,char *envp[])
{ 
 

 Point p0, p15;
 p0=Point(0,0,0);
 p15=Point(0, 1.5, 0);
 gab.addpoint(p0);
 /*for(int i=1; i<14; i++)
 {
   Point p((float)i/10,(float)i/10,0);
   
   gab.addpoint(p);
 }*/
  gab.addpoint(p15);
  gab.addautrepoint(gab);

 glutInit(&argc, argv);

 

 glutInitWindowPosition(740+(640*2),100);
 glutInitWindowSize(320,480); 
 glutCreateWindow("Saisie");
 glutDisplayFunc(renderS);
 

 //glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
 glutInitWindowPosition(100+(640*2),100);
 glutInitWindowSize(640,712); 	//Optionnel
 glutCreateWindow("Rotation");

 glutDisplayFunc(render);
 glutMouseFunc(mouseB);
 glutMotionFunc(mouseM);
 glutKeyboardFunc(keyboard) ;

 

 glutMainLoop();
 

	return 0;
}

bool operator==(Point p1,Point p2)
{
  
  return (p1.getX()==p2.getX()) && (p1.getY()==p2.getY()) &&(p1.getZ()==p2.getZ());

  
}


bool operator!=(Point p1,Point p2)
{

 return !(p1==p2);
}


Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator-(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()-p2.getX());
  presul.setY(p1.getY()-p2.getY());
  presul.setZ(p1.getZ()-p2.getZ());

  return presul;
}


Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}



void renderS(void)
{
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glutSwapBuffers();

 
}
void render(void)
{

 //cout << "\033[2J\033[1;1f";
 
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity();
 glRotatef(angle,xx,yy,zz) ;
  glScalef(zoom, zoom, zoom);
  
  cout<<"angledeg: "<<angle;
  cout<<"  x:"<<xx;
  cout<<"  y:"<<yy;
  cout<<"  z:"<<zz;
  cout<<"  zoom:"<<zoom;



glBegin(GL_LINES);
 
 /*base orthonormé*/

 glColor3f(1,0,0);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);
 

 
glEnd();
 
 /*glBegin(GL_POINTS);
  glColor3f(1,1,1);
  Point temp1;
  cout<<"gabSize="<<gab.t().size()<<endl;
    for(size_t i=0; i<gab.t().size(); i++)
    { 
      temp1=gab.t()[i];
      glVertex3f(temp1.getX(),temp1.getY(),temp1.getZ());
    } 
  glEnd();*/
  
 


 
 glBegin(GL_LINES);

  
  /*vector axe de rotationt*/
 glColor3f(1,0,1);
  glVertex3f(0,0,0);
  glVertex3f(xx,yy,zz);
  /*fin axe*/
 
 glEnd();

 
 GetOGLPos(mouseRecup.getX(),mouseRecup.getY());
 /*rotation avec mouse */
 Point temp((p1p2.getX()*cos(M_PI/2))-(p1p2.getY()*sin(M_PI/2)),(p1p2.getX()*sin(M_PI/2))+(p1p2.getY()*cos(M_PI/2)),0);
 

 glBegin(GL_LINES);
    glColor3f(0.5,0.2,1);
        glVertex3f(0,0,0);
        glVertex3f(p1p2.getX(),p1p2.getY(),p1p2.getZ());
    //glColor3f(0.45,0.3,0);
      //  glVertex3f(0,0,0);
       // glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  glEnd();
  
  if(mouseAc)
  {
    xx=temp.getX();
    yy=temp.getY();
    zz=0;
  }
 cout<<endl;
 glutSwapBuffers();
}

void GetOGLPos(int x, int y)
{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;
 
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );
 
    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
 
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
 
    //return CVector3(posX, posY, posZ);
    cout<<"posX="<<posX<<" posY="<<posY<<" posZ="<<posZ;

   
   
    if(deuxemeCli)
    { 
      p2.setX(posX);
      p2.setY(posY);
      p2.setZ(0);
      if(p1!=p2)
        p1p2=p2-p1;
      deuxemeCli=false;
    }

    else if(!deuxemeCli)
    {
      p1.setX(posX);
      p1.setY(posY);
      p1.setZ(0);
      deuxemeCli=true;
    }

    


    
  
}


void mouseB(int button, int state, int x, int y)
{
  switch (button)
    {
      
     case (GLUT_MIDDLE_BUTTON):
       if(state==GLUT_UP)
        {
         mouseAc=false;
        } 

        else if(state==GLUT_DOWN)
        {
         mouseAc=true;
        }    
        
        
     
   }
}

void mouseM(int x,int y)
{ if(mouseAc)
  {
   std::cout<<" Souris a:"<<x<<" "<<y;
   mouseRecup.setX(x);
   mouseRecup.setY(y);
   glutPostRedisplay();
  }
}

void keyboard(unsigned char key, int x, int y) {


switch (key) {

case 'y':
    zoom += 0.005;
    
    glutPostRedisplay();
    break;
case 'h':
    zoom -=0.005;
    
    glutPostRedisplay() ;
    break ;


case 'p':
angle ++ ;
glutPostRedisplay() ;
break ;

case 'm':
angle --;
glutPostRedisplay();
break;

case 'u':
xx+=0.05;
glutPostRedisplay() ;
break;

case 'j':
xx-=0.05;
glutPostRedisplay() ;
break;

case 'i':
yy+=0.05;
glutPostRedisplay() ;
break;

case 'k':
yy-=0.05;
glutPostRedisplay() ;
break;

case 'o':
zz+=0.05;
glutPostRedisplay() ;
break;

case 'l':
zz-=0.05;
glutPostRedisplay() ;
break;

case 'c':
angle = 0.0;
xx=0;
yy=0;
zz=0;


zoom=0.70;

glutPostRedisplay() ;
break;


case 27 /* Esc */:
exit(1) ;

}
}