#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"


void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, 800, 600);
 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
 
	gluPerspective(45, 4.0/3, 0.1, 2);
	gluLookAt(0, 0, 0, 0, 0, 1, 0, 1, 0);
 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
 
	glPushMatrix();
		glColor3f(1, 0, 0);
		glTranslatef(.05, .05, .5);
 
		glutSolidSphere(.05, 20, 20);
	glPopMatrix();
 
	glPushMatrix();
		glColor3f(0, 1, 0);
		glTranslatef(0, 0, 1);
 
		glutSolidCube(.1);
	glPopMatrix();
 
	glPushMatrix();
		glColor3f(0, 0, 1);
		glTranslatef(-.1, -.1, 1.5);
 
		glutSolidSphere(.05, 20, 20);
	glPopMatrix();
 
	glutSwapBuffers();
}
 
int main(int argc, char ** argv)
{
	glutInit(&argc, argv);
 
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Demonstration");
 
	glEnable(GL_LIGHTING);
		GLfloat diffuseLight[] = {.75, .75, .75, 1};
		GLfloat lightPosition[] = {-1, -1, -1, 1};
		glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
		glEnable(GL_LIGHT0);
 
	glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
 
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
 
        glShadeModel(GL_SMOOTH);
 
	glClearColor(0, 0, 0, 0);
 
	glutDisplayFunc(display);
 
	glutMainLoop();
 
	return 0;
}