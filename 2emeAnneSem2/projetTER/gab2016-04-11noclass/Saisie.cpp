#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"
#include "GL/freeglut.h"
#include <stdlib.h>
#include "Tabpoint.h"
#include "Point.h"

using namespace std;



 Tabpoint gab;

// compilation de librairie glut  -lglut -lGL -lGLU
void display(void);


void vMouse(int button, int state, int x, int y);


int main(int argc,char** argv)
{

  glutInit(&argc, argv);



  glutInitDisplayMode(GLUT_RGB|GLUT_DOUBLE);
  
  glutCreateWindow(argv[0]);
  
  glutDisplayFunc(display);
  glutMouseFunc(vMouse);

  glutMainLoop();
  


  return 0;
}

void display(void)
{
  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT); // efface le frame buffer
  glutSwapBuffers();
}


void vMouse(int button, int state, int x, int y)
{
  switch (button)
    {
      
     case (GLUT_LEFT_BUTTON):
      if (state==GLUT_DOWN)
	     {
	       std::cout<<"Souris a:"<<x<<" "<<y<<std::endl;
	       Point p;
	       p=Point(x,y,0);
	       gab.addpoint(p);
	     }
          break;
   }
}
