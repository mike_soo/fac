#include "Point.h"
#include <iostream>
using namespace std;



Point::Point(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {};
Point::Point()
{
	x=0;
	y=0;
	z=0;
}

Point& Point::operator=(Point p2)
{
  
  this->setX(p2.getX());
  this->setY(p2.getY());
  this->setZ(p2.getZ());

  return *this;
}



float Point::getX(){return this->x;}
float Point::getY(){return this->y;}
float Point::getZ(){return this->z;}


void Point::setX(float x){ this->x = x;}
void Point::setY(float y){ this->y = y;}
void Point::setZ(float z){ this->z = z;}



