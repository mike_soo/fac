
#include <iostream>
#include <math.h>
#include <vector>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include "Point.h"
# define M_PI           3.14159265358979323846

using namespace std;

GLfloat angle = 0.0;
/*axe de rotation*/
GLfloat xx=0;
GLfloat yy=0;
GLfloat zz=0;
float phi=0;
float theta=0;
float psi=0;

Point xx0(1,0,0);
Point yy0(0,1,0);
Point zz0(0,0,1);
Point Cortho(0,0,0);
Point xx1(cos(psi),sin(psi),0);
Point yy1(-sin(psi),cos(psi),0);

Point yy2(yy1.getX(),yy1.getY(),yy1.getZ());
Point zz2(zz0.getX(),zz0.getY(),zz0.getZ());

Point xx3(xx1.getX(),xx1.getY(),xx1.getZ());
Point yy3(yy2.getX(),yy2.getY(),yy2.getZ());


/*axe de rotation*/
float zoom=0.70;


void modifAng();

void keyboard(unsigned char key, int x, int y);

void render(void);

Point operator+(Point p1,Point p2);

Point operator*(float scal,Point p1);


int main(int argc, char** argv,char *envp[])
{ 
  
 glutInit(&argc, argv);
 //glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
 glutInitWindowPosition(100,100);
 glutInitWindowSize(640,480); 	//Optionnel
 glutCreateWindow("Rotation");
  
 glutDisplayFunc(render);
 
 glutKeyboardFunc(keyboard) ;

 glutMainLoop();
 

	return 0;
}



Point operator+(Point p1,Point p2)
{
  Point presul;
  presul.setX(p1.getX()+p2.getX());
  presul.setY(p1.getY()+p2.getY());
  presul.setZ(p1.getZ()+p2.getZ());

  return presul;
}

Point operator*(float scal,Point p1)
{
  Point presul;
  presul.setX((float)p1.getX()*scal);
  presul.setY((float)p1.getY()*scal);
  presul.setZ((float)p1.getZ()*scal);

  return presul;
}

void modifAng()
{ Point temp;
  xx1.setX(cos(psi));
  xx1.setY(sin(psi));
  xx1.setZ(0);
  yy1.setX(-sin(psi));
  yy1.setY(cos(psi));
  yy1.setZ(0);
  
  temp=(((float)cos(theta))*yy1)+((float)sin(theta)*zz0);
  yy2=temp;
  temp=(((float)-sin(theta))*yy1)+((float)cos(theta)*zz0);
  zz2=temp;
  

  temp=(((float)cos(phi))*xx1)+((float)sin(phi)*yy2);
  xx3=temp;
  temp=(((float)-sin(phi))*xx1)+((float)cos(phi)*yy2);
  yy3=temp;
  


}

void render(void)
{

 //cout << "\033[2J\033[1;1f";
 
 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 glLoadIdentity() ;
 glRotatef(angle,xx,yy,zz) ;
  glScalef(zoom, zoom, zoom);
  
  cout<<"angledeg: "<<angle;
  cout<<"  x:"<<xx;
  cout<<"  y:"<<yy;
  cout<<"  z:"<<zz;
  cout<<"  zoom:"<<zoom;
  cout<<"  theta:"<<theta;
  cout<<"  phi:"<<phi;
  cout<<"  psi:"<<psi<<endl;


  cout<<endl;
  cout<<endl;

  if(psi>=2*M_PI)
  {
    psi=0;
  }

  if(phi>=2*M_PI)
  {
    phi=0;
  }

  if(theta>=2*M_PI)
  {
    theta=0;
  }



modifAng();
glBegin(GL_LINES);
 
 /*base orthonormé*/

 glColor3f(1,0,0);
  glVertex3f(0, 0, 0);
 	glVertex3f(1,0,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,1,0);
 	glVertex3f(0,0,0);
 	glVertex3f(0,0,1);
 
 /*(0,x1,y1); vert*/
 glColor3f(0.2,0.5,0); 
  glVertex3f(0,0,0); /*x1*/
  glVertex3f(xx1.getX(),xx1.getY(),xx1.getZ());

  glVertex3f(0,0,0);/*y1*/
  glVertex3f(yy1.getX(),yy1.getY(),yy1.getZ());
 
 /*(0,y2,z2) bleu clair*/
 glColor3f(0.2,1,1);
  glVertex3f(0,0,0); /*y2*/
  glVertex3f(yy2.getX(),yy2.getY(),yy2.getZ());

  glVertex3f(0,0,0); /*z2*/
  glVertex3f(zz2.getX(),zz2.getY(),zz2.getZ());

 /*(0,x3,y3) marron*/
 glColor3f(0.6,0.2,0.2);
  glVertex3f(0,0,0);
  glVertex3f(xx3.getX(),xx3.getY(),xx3.getZ());

  glVertex3f(0,0,0);
  glVertex3f(yy3.getX(),yy3.getY(),yy3.getZ());
 
 /*Dessin Carrée*/
 glColor3f(1,0.3,0.1);
 Point temp;
  
  glVertex3f(0,0,0);
  temp=yy3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());

  
  temp=yy3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=xx3+yy3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());

  
  temp=xx3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=xx3+yy3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());

  
  glVertex3f(0,0,0);
  temp=xx3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());

  
  temp=zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=yy3+zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());

   
  temp=yy3+zz2; 
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=xx3+yy3+zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());


  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=xx3+zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());


  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  

  glVertex3f(0,0,0);
  temp=zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  
  
  temp=yy3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=yy3+zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());

  
  temp=xx3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=xx3+zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());


  temp=xx3+yy3;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  temp=xx3+yy3+zz2;
  glVertex3f(temp.getX(),temp.getY(),temp.getZ());
  


 

  
  /*vector axe de rotationt*/
 glColor3f(1,0,1);
  glVertex3f(0,0,0);
  glVertex3f(xx,yy,zz);
  /*fin axe*/
 
 /*base cyl*/
  

 	
  
  
 


  
  
 
  
  /*droite representé dans la base polaire*/

  //glVertex3f(0,0,0);
  //glVertex3f(p*cos(theta)+p*cos(theta +M_PI/2),p*sin(theta)+p*sin(theta+M_PI/2),0);


  /*baseSpherique*/
  /*glColor3f(0,1,0);
  glVertex3f(0,0,0);
  glVertex3f(p*sin(phi)*cos(theta),p*sin(phi)*sin(theta),p*cos(phi));
  glVertex3f(0,0,0);
  glVertex3f(p*cos(phi),p*sin(phi)*cos(theta),p*sin(phi)*sin(theta));*/
 /* glVertex3f(0,0,0);
  glVertex3f(0,0,0);*/

glEnd();



 glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y) {


switch (key) {
case 't':
  psi+=M_PI/16;
  glutPostRedisplay();
  break;
case 'g':
  psi-=M_PI/16;
  glutPostRedisplay();
  break;

case 'r':
  theta+=M_PI/16;
  glutPostRedisplay();
  break;

case 'f':
  theta-=M_PI/16;
  glutPostRedisplay();
  break;
case 'y':
    zoom += 0.005;
    
    glutPostRedisplay();
    break;
case 'h':
    zoom -=0.005;
    
    glutPostRedisplay() ;
    break ;


case 'p':
angle ++ ;
glutPostRedisplay() ;
break ;

case 'm':
angle --;
glutPostRedisplay();
break;

case 'u':
xx+=0.05;
glutPostRedisplay() ;
break;

case 'j':
xx-=0.05;
glutPostRedisplay() ;
break;

case 'i':
yy+=0.05;
glutPostRedisplay() ;
break;

case 'k':
yy-=0.05;
glutPostRedisplay() ;
break;

case 'o':
zz+=0.05;
glutPostRedisplay() ;
break;

case 'l':
zz-=0.05;
glutPostRedisplay() ;
break;

case 'e':
phi+=M_PI/16;
glutPostRedisplay() ;
break;

case 'd':
phi-=M_PI/16;
glutPostRedisplay() ;
break;

/*case 'x':
xct+=0.1;
theta=(float)-xct/p;
cout<<"xct="<<xct<<endl;
//glutPostRedisplay() ;
break;

case 'w':
xct-=0.1;
theta=(float)-xct/p;
cout<<"xct="<<xct<<endl;
//glutPostRedisplay() ;
break;

case 'a':
t++;
glutPostRedisplay();
break;

case 'q':
t--;
glutPostRedisplay();
break;*/

case 'c':
angle = 0.0;
xx=0;
yy=0;
zz=0;
phi=0;
psi=0;
theta=0;

zoom=0.70;

glutPostRedisplay() ;
break;


case 27 /* Esc */:
exit(1) ;

}
}