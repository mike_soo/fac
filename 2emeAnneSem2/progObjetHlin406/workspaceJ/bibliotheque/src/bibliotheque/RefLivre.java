package bibliotheque;

import java.util.ArrayList;

public class RefLivre {
 private String ISBN ;
 private String titre;
 private String stitre;
 private ArrayList <Auteur> auteur; 
 private Public publicCible;
 private ArrayList<ExemLivre> exemLivres; 
 private int nombreExemDispo;
 
 public RefLivre(String ISBN, String titre,String stitre,ArrayList <Auteur> auteur,Public publicCible,int nbExemplaires)
 {
	 this.ISBN = ISBN;
	 this.titre = titre;
	 this.stitre = stitre;
	 this.auteur = auteur;
	 this.publicCible=publicCible;
	 this.nombreExemDispo=nbExemplaires;
	 
	 for (int i=0;i<nbExemplaires;i++) //nb exemplaires passé en param
	 {
		 ExemLivre exemLiv = new ExemLivre (this);
		 exemLivres.add(exemLiv);
	 }
 }
 
 public RefLivre() 
 {
	auteur=new ArrayList <Auteur> ();
	exemLivres=new ArrayList <ExemLivre> (); 
 }
 
 public boolean estDisponible()
 {
	 if (nombreExemDispo==0)
	 {
		 return false; 
	 }
	 
	 else 
	 {
		 return true;
	 }
 }
 
 public ArrayList <ExemLivre> exemLivresDispo ()
 {
	 if (estDisponible())
	    {
		  return exemLivres;
	    }
	 
	 else 
	 	{
		   System.out.println("Pas d'exemplaires disponibles");
		   return null;
		}
 }

 public ArrayList<Auteur> getAuteur() 
 {
	return auteur;
 }


 public Public getPublicCible() 
 {
	return publicCible;
 }

 public void setPublicCible(Public publicCible) 
 {
	this.publicCible = publicCible;
 }

public String getTitre() {
	return titre;
}

public String getISBN() {
	return ISBN;
}

public void modifNbExemDisp(int n)
{
	nombreExemDispo+=n;
}

public String getStitre() {
	return stitre;
}

public ArrayList<ExemLivre> getExemLivres() {
	return exemLivres;
}

public int getNombreExemDispo() {
	return nombreExemDispo;
}
 
}
