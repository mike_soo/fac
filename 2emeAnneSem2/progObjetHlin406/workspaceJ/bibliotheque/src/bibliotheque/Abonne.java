package bibliotheque;

import java.util.ArrayList;

public class Abonne extends Personne {
 private String numAbonne;
 
 
 private ArrayList <ExemLivre> livresEmpruntes ;//livres emprunté par l'abonnée de l'instance courant
 
 
 public Abonne(String nom,String prenom, int age,String numAbonne) 
 {   super(nom,prenom,age);
	 livresEmpruntes = new ArrayList <ExemLivre>();
	 this.numAbonne=numAbonne;
 }
 
 
 public boolean emprunter(ExemLivre l)
 {
	 if(livresEmpruntes.size()<=5)
	 {   //ajouter les modfication necesaire de l
		 
	     l.emprunter();
		 livresEmpruntes.add(l);
		 
	 	 return true;
     }
 
	 else
	 {
		 System.out.println("L'abonnée possede deja 5 livres");
		 return false;
	 }
}
 
 
 public void rendre (ExemLivre l)
  {
	 for (int i=0;i<livresEmpruntes.size();i++)
	 {
		 if (l == livresEmpruntes.get(i))
		 {   livresEmpruntes.get(i).rendre();
		    
			 livresEmpruntes.remove(l);
			 break;
			 //livresEmpruntés.remove (i); question !!
		 }
	 }
  }


public String getNumAbonne() {
	return numAbonne;
}


}


