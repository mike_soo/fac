package bibliotheque;

public class ExemLivre {
	private boolean emprunté;
	private boolean enreparation;
	private RefLivre livreRef;
	
	
	
	public ExemLivre(RefLivre livreRef)
	{
	 this.livreRef=livreRef;
     emprunté=false;
	 enreparation=false;
	}
	
	public void emprunter()
	{
		emprunté=true;
		livreRef.modifNbExemDisp(-1);
	}
	
	public void rendre()
	{
		emprunté=false;
		livreRef.modifNbExemDisp(+1);
	}
	
	public boolean estDisponible()
	{
		return !emprunté;
	}
	
	public void mettreEnReparation()
	{
		enreparation=true;
	}
	
	public void remettreEnRayon()
	{
		enreparation=false;
	}
	
	public boolean estEnReparation()
	{
		return enreparation;
	}
	
}
