package bibliotheque;

import java.util.ArrayList;
import java.util.HashMap;

public class Catalogue 
{
	private ArrayList <Abonne> listAbonnes;
	private HashMap <String,RefLivre> listLivres;
	
	public Catalogue(ArrayList <Abonne> listAbonnes,HashMap <String,RefLivre> listLivres)
	{
		this.listAbonnes=listAbonnes;
		this.listLivres=listLivres;
	}
	
	public void ajouteLivre (RefLivre l)
	{
		listLivres.put(l.getISBN(),l);
	}
	
	public RefLivre getLivre(String ISBN)
	{
		return listLivres.get(ISBN);
		
	}
	
	public void ajouteAbonne (Abonne abo)
	{
		listAbonnes.add(abo);
	}
	
	public Abonne getAbonne(String nom)
	{
		for (int i=0;i<listAbonnes.size();i++)
		{
			if (listAbonnes.get(i).getNom().equals(nom))
			{
				return listAbonnes.get(i);
			}
		}
		System.out.println("l'abonné n'existe pas");
		return null;
	  
	}
}
