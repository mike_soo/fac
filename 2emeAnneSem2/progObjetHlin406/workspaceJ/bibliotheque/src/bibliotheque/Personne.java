package bibliotheque;

public class Personne {
	private String nom;
	private String prenom;
	int age;
	private boolean estMineur;
	
	public Personne(String nom,String prenom, int age)
	{
		if (age<18)
		{
			estMineur=true;
		}
		else 
			estMineur=false;
		
		this.nom=nom;
		this.prenom=prenom;
		this.age=age;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public int getAge() {
		return age;
	}

	public boolean estMineur() {
		return estMineur;
	}

}
