package associatioinCollections;

import java.util.ArrayList;

public class Promotion {
	private ArrayList <Etudiant> listEtudiant;
	private int annee;
	
	
	
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}


	public Promotion ()
	{
		listEtudiant = new ArrayList <Etudiant> ();
	}
	public Promotion (int annee)
	{
		this.annee=annee;
		listEtudiant = new ArrayList <Etudiant> ();
	}
	 
	public Etudiant getEtudiant(int i)
	{  if((i<listEtudiant.size()) && (i>=0))
		{return listEtudiant.get(i);}
	   else 
		{return null;}
	}
	public int nbEtudiantTot()
	{
		return listEtudiant.size();
	}
	public void inscrireEtud(Etudiant etud)
	{
		if(!(listEtudiant.contains(etud)))
		{
			listEtudiant.add(etud);
		}
		else
		{
			System.out.println("Etudiant deja inscrit!");
		}
	}
	public double moyennePromo()
	{
		double moyTotalPromo=0;
		for (int i=0;i<listEtudiant.size();i++)
		{
			moyTotalPromo+=listEtudiant.get(i).calculMoy();
		}
		return moyTotalPromo/listEtudiant.size();
	}
	public void afficheResultats ()
	{for (int i=0;i<listEtudiant.size();i++)
		{
		 listEtudiant.get(i).ligneResultats();
		}
		
	}
	public Etudiant recherche(String nom)
	{
	   for (int i=0;i<listEtudiant.size();i++)
		{
		 if(listEtudiant.get(i).getNom().equals(nom))
		 {
			 return listEtudiant.get(i);
		 }
		
		}
	    System.out.println("Etudiant non existant");
		return null;
		
	}
	public ArrayList <Etudiant> admis ()
	{  
		ArrayList <Etudiant> tabAdmis = new ArrayList<Etudiant> ();
		for(int i=0;i<listEtudiant.size();i++)
		{
		 if (listEtudiant.get(i).calculMoy()>=10)
		 	{
			 	tabAdmis.add(listEtudiant.get(i));
			 	
		 	}
		 
		}
		
		return tabAdmis;
	}
	
	public ArrayList <Etudiant> nouvInscNonFranco()
	{
	 ArrayList<Etudiant> nINF = new ArrayList <Etudiant> ();
	 for (int i=0;i<listEtudiant.size();i++)
	 {
		 if (listEtudiant.get(i).getCodePays()==CodePays.EENF)
		 {  nINF.add(listEtudiant.get(i));
			 
		 }
		 
	 }
	 
	 return nINF;
	}
	
	public ArrayList <Etudiant> majors ()
	{ double noteMax=0;
		ArrayList <Etudiant> majorants =new ArrayList<Etudiant> ();
		for (int i=0; i<listEtudiant.size();i++)
		{
			if (listEtudiant.get(i).calculMoy()>noteMax)
			{
				noteMax=listEtudiant.get(i).calculMoy();
			}
			
		}
		for (int i=0; i<listEtudiant.size();i++)
		{
			if (listEtudiant.get(i).calculMoy()==noteMax)
			{
				majorants.add(listEtudiant.get(i));
			}
			
		}
		
		return majorants;
		
	}
	
	
}

	
	

