package associatioinCollections;
import java.util.*;
import java.text.*;



 enum Mention 
{
	NA,P,AB,B,TB
};

 enum CodePays
{
	EF,EENF,EEF
};
public class Etudiant 
{	Scanner sc = new Scanner(System.in);
	private String nom;
	private String prenom;
	private int age;
	private String dateN;
	private boolean codeIns;
	private CodePays codePays;
	private NoteE noteE;
	
	/*public int getAge();
	{
		return age;
	}*/
 
		public void setAge() throws ParseException
		{
		   String temp="";
		   int year=0;
		   int month=0;
		   int day=0;
		   int cpt=0;

			for (int i=0;i<dateN.length();i++)
			{  if(dateN.charAt(i)!='/')
				{   //System.out.print(dateN.charAt(i));
					temp+=dateN.charAt(i);

				}

				else if (cpt==0)
				{    System.out.print("temp dans cpt==0: "+temp);
					day=Integer.parseInt(temp);
					temp="";
					cpt++;
				}
				else if (cpt==1)
				{ System.out.print("temp dans cpt==1: "+temp);
					month=Integer.parseInt(temp);
					temp="";
					cpt++;
				}
			}
				 
		 System.out.print("temp dans cpt==2: "+temp);
		 year=Integer.parseInt(temp);
		 temp="";
		
		
			System.out.println(" ");
			System.out.println();
			System.out.println(dateN);
			System.out.println(year);
			System.out.println(month);
			System.out.println(day);

			Calendar dob = Calendar.getInstance();
			dob.set(year,month,day);
			
			Calendar now = Calendar.getInstance(Locale.FRANCE);
			age=now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

			System.out.println(age);
			System.out.println(now.get(Calendar.YEAR));
			System.out.println(dob.get(Calendar.YEAR));
			
		/* 

		 try
		   {	*/
		 		

		 		/*DateFormat dd = new SimpleDateFormat("dd MM yyyy");
		 	 	Date date = dd.parse("07 05 1993");
		 	 
		 	 	System.out.println(" 7. " + 
            	DateFormat.getDateInstance(DateFormat.LONG).format(date));
		 	 	System.out.printf(date.toString()+"\n");*/


		 	 	//DateFormat df =DateFormat.getDateInstance(DateFormat.LONG);
		 	 	//Date d=df.parse("may 05 1993");
	     		//System.out.printf(d.toString()+"\n");

	     		//Calendar cal = new GregorianCalendar();
	     		//cal.setTime("07 05 1993");
	     		

            /*} 
         catch (ParseException ex) 
            {
              ex.printStackTrace(); // or log it using a logging framework
            }*/

		}
	public String getdateN()
	{
		return dateN;
	}

	public void setdateN(String dateN)
	{
		this.dateN=dateN;
	}

	public boolean getCodeIns()
	{
		return codeIns;
	}

	public void setCodeIns(boolean codeIns)
	{
		this.codeIns=codeIns;
	}

	public CodePays getCodePays()
	{
		return codePays;
	}

	public void setCodePays(CodePays codePays)
	{
		this.codePays=codePays;
	}

	public NoteE getnote()
	{
		return noteE;
	}

	public double calculMoy()
	{
		return (noteE.e1+noteE.e2+noteE.e3)/3;
	}

	public Mention calculMention()
	{	
		Mention mention;
		double moy =calculMoy();
		if (moy>=16)
		{   
		 	mention=Mention.TB;
		}
		else if (moy>=14)
		{
			mention=Mention.B;
		}
		else if(moy>=12)
		{
			mention=Mention.AB;
		}
		else if(moy>=10)
		{
			mention=Mention.P;
		}
		else 
		{
			mention=Mention.NA;
		}

	 return mention;
	}

	public void ligneResultats()
	{	double moy=calculMoy();
		System.out.println("nom="+this.nom+"\nprenom="+this.prenom+"\nmoyenne="+moy+"\nmention="+calculMention());
		if(moy>=10)
		{ 
		 	if(noteE.e1>=10)
		 	{
				System.out.println("noteE1="+noteE.e1);
			}
		  
		  	if(noteE.e2>=10)
		  	{
		  		System.out.println("noteE2="+noteE.e2);	
			}
		  
		  	if(noteE.e3>=10)
		  	{
		  		System.out.println("noteE3="+noteE.e3);
		  	}
		}
		System.out.printf("\n");

	}
	
	public Etudiant(String nom,String prenom,String dateN,boolean codeIns,CodePays codePays,NoteE noteE) throws ParseException
	{
		this.nom=nom;
		this.prenom=prenom;
		this.dateN=dateN;
		this.codeIns=codeIns;
		this.codePays=codePays;
		this.noteE=noteE;
		setAge();
		
	}

	public Etudiant()
	{   NoteE noteE=new NoteE();

		this.nom=sc.next();
		this.prenom=sc.next();
		this.dateN=sc.next();
		this.codeIns=sc.nextBoolean();
		this.codePays=CodePays.valueOf(sc.next());
		noteE.e1=sc.nextDouble();
		noteE.e2=sc.nextDouble();
		noteE.e3=sc.nextDouble();
		this.noteE=noteE;
		//this.note=.valueOf(sc.next());
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
}