package quincaillerie;

public abstract class Piece {
 
	
	private String nom;
	private String Ref;

	
	public Piece(String nom,String reference)
	{
		this.nom=nom;
		this.Ref=reference;
	}

	public String getNom() {
		return nom;
	}

	
	public void setNom(String nom) {
		this.nom = nom;
	}

	
	public String getRef() {
		return Ref;
	}

	
	public void setRef(String ref) {
		Ref = ref;
	}

	
	public void ficheCaract()
	{
		
		
		System.out.println("Nom:" + getNom());
		System.out.println("Ref:" + getRef());
		System.out.println("Prix:" + getPrix());
		System.out.println("Garantie:" + getDureeGar()/2 + " mois");
		System.out.println("DureeFab:" + getDureeFab() + " jour");
		System.out.print("Composants:" + "\n" +composants(""));
		
		
	}
	
	
	
	public abstract double getPrix();
	public abstract int getDureeGar();
	public abstract int getDureeFab();
	public abstract String composants(String decal);
	
	
}
