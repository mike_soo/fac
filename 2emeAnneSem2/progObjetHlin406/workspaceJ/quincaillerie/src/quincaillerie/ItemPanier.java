package quincaillerie;

public class ItemPanier implements Comparable<ItemPanier>{
  private Piece p;
  
  private int qte;
  
  public ItemPanier(Piece p)
  {
	  this.p = p;
	  qte=1;
  }
  
  
  public Piece getPiece()
  {
	  
	  return p;
  }
  
  
  public int compareTo(ItemPanier it)
  {
	  if (this.calculPrix()==it.calculPrix())
		  return 0;
	  
	  if (this.calculPrix()<it.calculPrix())
		  return -1;
	  
	  else 
		  return 1;
  }
  
 
  public double calculPrix()
  {
	 return qte*p.getPrix();
  }
  
  public void plus1Qte()
  {
	  qte++;
  }
  public void moins1Qte()
  {
	  qte--;
  }
  
  public int getQte()
  {
	  return qte;
  }
  
  
   
   
  
}
