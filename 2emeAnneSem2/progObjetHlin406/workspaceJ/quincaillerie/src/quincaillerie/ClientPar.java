package quincaillerie;

public class ClientPar extends Client{
	
	private Civilite civ; 
	private Sexe S;
	private String prenom;
	
	public ClientPar()
	{
		
	}
	public void identifie(String nom,String prenom,Civilite civ,Sexe S)
	{
		this.prenom=prenom;
		setNom(nom);
		this.civ=civ;
		this.S=S;
	 
	}
	public Civilite getCiv() {
		return civ;
	}
	
	public Sexe getS() {
		return S;
	}
	
	public String getPrenom()
	{
		return prenom;
	}
	
}
