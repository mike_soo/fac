package quincaillerie;

public class Main {

	
	public static void main(String[] args) {
		
		       //String nom,string ref,int dureeFab,int dureeGar,double prix
		PieceSimple p1s=new PieceSimple("Vis","001",7,12,2);
		//PieceSimple p2s=new PieceSimple("Clou","002",7,12,1);
		
		PieceSimple p3s=new PieceSimple("Rayon de Roue","003",14,6,5);
		PieceSimple p4s=new PieceSimple("Chambre à Air","004",15,7,6);
		PieceSimple p5s=new PieceSimple("Barre Metallique","005",17,15,3);
		
		PieceComposite pCk1=new PCk("Chaise Metallique","001",30);
		PieceComposite pCk2=new PCk("Table Metallique","002",45);
		PieceComposite pCk3=new PCk("Salle à manger Metallique","003",75);
		
		
		//chaise metallique 4 vis 4 barre Met
		pCk1.addPiece(p1s); 
		pCk1.addPiece(p1s);
		pCk1.addPiece(p1s);
		pCk1.addPiece(p1s);
		pCk1.addPiece(p5s);
		pCk1.addPiece(p5s);
		pCk1.addPiece(p5s);
		pCk1.addPiece(p5s);
		
		//table metallique 8 vis 4 barre Met
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p1s);
		pCk2.addPiece(p5s);
		pCk2.addPiece(p5s);
		pCk2.addPiece(p5s);
		pCk2.addPiece(p5s);
		
		
		
		pCk3.addPiece(pCk2);
		pCk3.addPiece(pCk1);
		
		p5s.ficheCaract(); //fiche Caracteristique de Barre Metallique
		pCk1.ficheCaract(); //fiche Caracteristique de Chaise Metallique
		pCk2.ficheCaract();	//fiche Caracteristique de Table Metallique
		pCk3.ficheCaract();	//fiche Caracteristique de Salle à manger Metallique
		
		ClientPar cp1=new ClientPar();
		cp1.identifie("Ohayon", "Michael", Civilite.M, Sexe.H);
		
		cp1.ajoutPanier(pCk3);
		cp1.ajoutPanier(pCk3);
		cp1.ajoutPanier(pCk3);
		cp1.ajoutPanier(pCk3);
		cp1.ajoutPanier(pCk2);
		cp1.ajoutPanier(p5s);
		cp1.ajoutPanier(p4s);
		cp1.ajoutPanier(p3s);
		System.out.println();
		System.out.println();
		cp1.getPrixPan();
		System.out.println();
		cp1.acheterPNPF();
		System.out.println();
		
		
		
	}

}
