package quincaillerie;

import java.util.ArrayList;

public class Panier 
{
	
	 private ArrayList <ItemPanier> listItems = new ArrayList <ItemPanier> (); 
	
	 
	 public void ajoutItem(ItemPanier it)
	 {
		for(ItemPanier pit : listItems)
		{
			if(pit.getPiece().getNom().equals(it.getPiece().getNom()))
			{
				pit.plus1Qte();
				return;
			}
			
		}
				
		listItems.add(it);
	 }
	 
	 
	 public void enleveItem(Piece p)
	 {
		 for(ItemPanier pit : listItems)
			{
			 	if(pit.getPiece().getNom()==p.getNom())
			 	{  if(pit.getQte()>0)
			 		  pit.moins1Qte();
			 		
			 	   return;
			 	}
			}
	 }
	 
	 public void afficheContenue()
	 {System.out.println("Voici la liste des articles dans le paniers:");
	 System.out.println();
	 	for (ItemPanier pitp : listItems)
		 {
			 System.out.println(">" + pitp.calculPrix() 
					            + "e" +" "
					            +pitp.getQte()+"x"+" "
					            + pitp.getPiece().getNom() );
			 
		 }
		 System.out.println();
	 }
	 
	 public double getPrixPanier()
	 {
		 double prixPaniers=0;
		 for(ItemPanier pit : listItems)
		 {
			 prixPaniers+=pit.calculPrix();
		 }
		return prixPaniers;
	 }
	 
	 public void restartPanier()
	 {
		// listItems = null;
		 listItems = new ArrayList <ItemPanier> ();
	 }
	 
}
