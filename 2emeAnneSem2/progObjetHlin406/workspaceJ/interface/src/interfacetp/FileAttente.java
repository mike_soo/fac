package interfacetp;

import java.util.ArrayList;



public class FileAttente implements IFileAttente{
    private String nomFile;
    private static String reglementationFile = "sans priorité";
    private ArrayList<Personne> contenu;
    private int nbentre=0;
    private int nbsortie=0;

   //Accesseurs
    public String getNomFile() {
        return nomFile;    }
    public void setNomFile(String nomFile) {
        this.nomFile = nomFile;    }
     public int getNbentre() {
        return nbentre;    }
     public int getNbsortie() {
        return nbsortie;    }
     
     
//Constructeur
    public FileAttente(){contenu=new ArrayList<Personne>();}

    public void entre(Personne p)
    {
    	contenu.add(p); nbentre++;
    }

    
    public Personne sort()
    {
    	Personne p=null;
        if(!contenu.isEmpty())
        {p=contenu.get(contenu.size()-1);
        contenu.remove(contenu.size()-1);}
        nbsortie++;
        return p;    
    }
    
    
    public int taille()
    {
    	return contenu.size();
    }
    
    public boolean estVide()
    {
    	return contenu.isEmpty();
    }

    public String toString()
    {
    	return ""+descriptionContenu();
    }

    public String descriptionContenu()
    {    
    	String resultat="";
        for(Personne p:this.contenu)
            resultat+= p+" ";
        
        return resultat;
    }

}