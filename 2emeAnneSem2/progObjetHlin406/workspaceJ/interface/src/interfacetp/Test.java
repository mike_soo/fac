package interfacetp;

import java.util.ArrayList;
import java.util.Collections;

public class Test 
{
	public static void main (String[] a)
	{   
		ArrayList <Personne> listPers = new ArrayList <Personne>();
		Personne p1=new Personne ("Ohayon","Michael",22);
		Personne p2=new Personne ("Dupond","George",25);
		Personne p3=new Personne ("Ramirez","Jorge",18);
		Personne p4=new Personne ("Ordaz","Daniel",12);
		Personne p5=new Personne ("Bolivar","Simon",22);
		listPers.add(p1);
		listPers.add(p2);
		listPers.add(p3);
		listPers.add(p4);
		listPers.add(p5);
	    
		
		//on stock dans pmaxList le max avec la methode max de la classe Maxtest
		System.out.println("maxTest:");
		Personne pmaxList=Maxtest.max(listPers);
		System.out.println(pmaxList.getPrenom()+" "+pmaxList.getNom());
		
		//on stock dans pmaxList1 le max avec la methode max de la classe Collections
		System.out.println("maxCollections:");
		Personne pmaxListC=Collections.max(listPers);
		
		System.out.println(pmaxListC.getPrenom()+" "+pmaxListC.getNom());
		
		
		//trie le trableau listPers toujours grace a compareto
		
		System.out.println();
		System.out.println("Ordre par alphabet lexicographique:");
		Collections.sort(listPers);
		
		for (Personne p : listPers)
		{
			System.out.println(p.getNom()+" "+p.getPrenom());
		}
		
		System.out.println();
		System.out.println("Ordre par age");
		
		Collections.sort(listPers,ComparatorC.ORDRE_AGE);
		for (Personne p : listPers)
		{
			System.out.println(p.getNom()+" "+p.getPrenom());
		}
		
	}
}
