package interfacetp;

import java.util.Comparator;

public class ComparatorC  {
	 static final Comparator<Personne> ORDRE_AGE = 
             new Comparator<Personne>()
  {
	 public int compare(Personne p1,Personne p2)
	 {
			 return p1.getAge()-p2.getAge();
	 }
  };
}

