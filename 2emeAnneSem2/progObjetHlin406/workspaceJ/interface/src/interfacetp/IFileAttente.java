package interfacetp;

public interface IFileAttente 
{
	public abstract void entre (Personne P);
	public abstract Personne sort();
	public abstract boolean estVide();
	public abstract int taille();
	public abstract String toString();
	default void vider()
	{
		for(int i=0;i<taille();i++)
		{
			sort();
		}
	}
	
}
