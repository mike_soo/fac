package interfacetp;

import java.util.List;

public class Maxtest {
	public static <E extends Comparable <E>> E max (List <E> c)
	{
		if(c. isEmpty())
			return null;
		E max = c.get(0);
		for (E e : c)
			if (e.compareTo(max)>0)
				max = e;
		return max;
		
		
	}
}
