package herite;
import java.util.*;



public class Collisexp extends Colis{

	Scanner sc=new Scanner(System.in);
	
	String dateEnv;
	private static int nbColisExp;
	private int numColisIC;
	private Date date;
	private Calendar now;
	boolean embale;
	public Collisexp(String origine,String destination,int codePostal,double poid,double volume,int trec,String contenu,double valcontenu,boolean embale)
	{
	 super(origine,destination,codePostal,poid,volume,trec,contenu,valcontenu);
	  while( getPoid()>30)
		{
			System.out.println("Poid < 30 obligatoire! Saisir un poid correct:");
			setPoid(sc.nextInt());
		}
	  this.embale=embale;
	  nbColisExp++;
	  numColisIC=nbColisExp;

	  date=new Date();
	  now = Calendar.getInstance(Locale.FRANCE);
	  setTaffranch(30);
	  testInc();
	  
	}

	/*public static void testInc()
	{
		nbColisExp++;
		numColisIC++;
	}*/

	public void tarifaffcal()
	{
		if (embale)
	  	{
	  		setTaffranch(3);
	  	}
	}

	public void configIC()
	{
		tarifaffcal();
	  	tarifRemb();
	}

	
	public void descrip()
	{ configIC();
		System.out.println(getCp()+" "+getDestination()+" "+getTrec()+ " " + getPoid()+ " "+now.get(Calendar.DAY_OF_MONTH)+"/"+now.get(Calendar.MONTH)+"/"+now.get(Calendar.YEAR) +" "+nbColisExp+" "+ numColisIC);
		System.out.println(getTaffranch()+" "+getTarifrembours());
	}
}