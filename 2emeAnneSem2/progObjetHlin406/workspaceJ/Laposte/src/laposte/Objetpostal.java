package laposte;





public class Objetpostal{


	private String origine;
	private String destination;
	private int codePostal;
	private double poid;
	private double volume;
	private int trec;
	private double taffranch; //atribut derivée
	private double tarifrembours; //atribut derivée

	public Objetpostal()
	{
		origine="Mexico";
		destination="France";
		codePostal=34090;
		poid=5;
		volume=4;
		trec=2;
	}

    public Objetpostal(String origine,String destination,int codePostal,double poid,double volume,int trec)
    {this.origine=origine;
     this.destination=destination;
     this.codePostal=codePostal;
     this.poid=poid;
     this.volume=volume;
     this.trec=trec;
    }
	public double getPoid()
	{
		return poid;
	}

	public int getCp()
	{
		return codePostal;
	}

	public String getDestination()
	{
		return destination;
	}

    protected void setPoid(double poid)
    {
    	this.poid=poid;
    }

	public void setTrembours(double tTrembours)
	{
		this.tarifrembours=tTrembours;
	}

	public double getTarifrembours()
	{
		return tarifrembours;
	}
	public double getVolume()
	{
		return volume;
	}	

	public void setTaffranch(double x)
	{
		this.taffranch+=x;
	}

	public double getTaffranch()
	{
		return taffranch;
	}

	public int getTrec()
	{
		return trec;
	}
}