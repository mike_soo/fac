package laposte;

import java.util.ArrayList;

public class SacPostaux {
	private ArrayList <Objetpostal> sacPostal;
	private double cap;
	private double valeurRemboursement;
	
	public void ajoutObjPost(Objetpostal objP)
	{
		if(cap<objP.getVolume())
		{
			System.out.println("Espace insufisant");
		}
		else 
		{
			sacPostal.add(objP);
			cap-=objP.getVolume();
			valeurRemboursement+=objP.getTarifrembours();
			
		}
	}
	
	public double getCap() {
		return cap;
	}
	
	public void setCap(double cap) {
		this.cap = cap;
	}
	
	public double getValeurRemboursement() {
		return valeurRemboursement;
	}
	
	
	public SacPostaux()
	{
		sacPostal=new ArrayList<Objetpostal>();
	}
	
	public SacPostaux(double cap)
	{
		sacPostal=new ArrayList<Objetpostal>();
		this.cap=cap;
	}

	public Objetpostal getObjPostal(int i) {
		return sacPostal.get(i);
	}

	public void setSacPostal(ArrayList<Objetpostal> sacPostal) {
		this.sacPostal = sacPostal;
	}
	
	public int sacTabsize()
	{
		return sacPostal.size();
	}
	
	
	

}
