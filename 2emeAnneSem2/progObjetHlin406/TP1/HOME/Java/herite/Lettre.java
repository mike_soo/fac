package herite;

public class Lettre extends Objetpostal{
	private boolean urgent;
	
	

	public Lettre(String origine,String destination,int codePostal,double poid,double volume,int trec,boolean urgent)
	{ 
	  super(origine,destination,codePostal,poid,volume,trec);
	  this.urgent=urgent;
	}

	public Lettre()
	{
	  super("Mexico","France",34090,5,4,2);
	  this.urgent=true;  
	}


	public void tarifaffcal()
	{
		setTaffranch(0.5);

		if(getVolume()>0.125)
		{
			setTaffranch(3);
		}
		
		if(urgent)
		{
			setTaffranch(0.3);
		}


		if(getTrec()==1)
		{
		 	setTaffranch(0.5);
		}

		else if(getTrec()==2)
		{
			setTaffranch(1.5);
		}
	}

	public void tarifRemb()
	{
		if(getTrec()==0)
		{
		 	setTrembours(0);
		}
		
		else if(getTrec()==1)
		{
		 	setTrembours(1.5);
		}

		else if(getTrec()==2)
		{
			setTrembours(15);
		}
	}
}