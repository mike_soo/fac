package herite;


public class Colis extends Objetpostal{

	private String contenu;
	private double valcontenu;

	public Colis(String origine,String destination,int codePostal,double poid,double volume,int trec,String contenu,double valcontenu)
	{ 
		//super(origine,destination,codePostal,poid,volume,trec);
	  	this.contenu=contenu;
	  	this.valcontenu=valcontenu;
	  	
	  	System.out.println("je boucle de construcColis");
	}
	public void configIC()
	{
		tarifaffcal();
	  	tarifRemb();
	}

	public void tarifaffcal()
	{
		setTaffranch(2);

		if(getVolume()>0.125)
		{
			setTaffranch(3);
		}
		
		if(getTrec()==1)
		{
		 	setTaffranch(0.5);
		}

		else if(getTrec()==2)
		{
			setTaffranch(1.5);
		}
	}

	public void tarifRemb()
	{
		if(getTrec()==0)
		{
		 	setTrembours(0);
		}
		
		else if(getTrec()==1)
		{
		 	setTrembours(valcontenu*10/100);
		}

		else if(getTrec()==2)
		{
			setTrembours(valcontenu*50/100);
		}
	}
	
	public void descrip()
	{   configIC();
	 	System.out.println(getCp()+" "+getDestination()+" "+getTrec()+ " "+getVolume()+" "+valcontenu+"/"+getTaffranch()+"/"+getTarifrembours());
	}
}