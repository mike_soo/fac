
%clc
%clear all
%close all

load data1.mat
addpath('./music.m')

figure('name' , '4 signaux de micros');
hold on
    showTemporalSignal1(MICROS.Fe , MICROS.Signal(:,1) ,'r');
    showTemporalSignal1(MICROS.Fe , MICROS.Signal(:,2) ,'g');
    showTemporalSignal1(MICROS.Fe , MICROS.Signal(:,3) ,'b');
    showTemporalSignal1(MICROS.Fe , MICROS.Signal(:,4) ,'m');
hold off

%retard graphique
taux = 0.00018;
c = ANTENNE.C;
p1 =1* ANTENNE.D ;% ANTENNE.Pos(1);
p2 =2* ANTENNE.D ;% ANTENNE.Pos(2);
p3 =3* ANTENNE.D ;% ANTENNE.Pos(3);
p4 =4* ANTENNE.D ;% ANTENNE.Pos(4);

%calcul des 4 angles
theta1 = acos(taux * c / p1)* 180/pi;
theta2 = acos(taux * c / p2)* 180/pi;
theta3 = acos(taux * c / p3)* 180/pi;
theta4 = acos(taux * c / p4)* 180/pi;

%déterminer la  fréquence f0 presente avec un maximum d'énergie
% fft(
m = musicAlg(MICROS , ANTENNE );

