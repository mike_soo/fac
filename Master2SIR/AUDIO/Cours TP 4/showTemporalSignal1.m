function showTemporalSignal1(fe , signal , color)
    N = length(signal);
    tp = [0:N-1].'*1/fe;    
    if size(signal ,2) == 2
        plot(tp , signal(:,1) , 'r');
        plot(tp , signal(:,2) , 'g');
    else %if(size(signal , 2) == 1)
        tp;
        plot(tp , signal , color);  
        
    end
    
end