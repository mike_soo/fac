% pour executer, rajouter quelque part:
% m = musicAlg(MICROS , ANTENNE);


classdef musicAlg < handle
  properties
    signals
    
    microsSignals
    M
    Y
    TM
  end
   
  methods  
    %1. Decouper le signal de chaque microphones en fenetres de K points
    % lors de la construction de l'objet
    function obj = musicAlg(MICROS , ANTENNE)
      obj.microsSignals = MICROS.Signal;
      obj.M = [];
      K = 512;
      Q = floor( size(obj.microsSignals , 1) / K);
      R = mod(size(obj.microsSignals , 1) , K);
      nb_of_m_signals=size(obj.microsSignals ,2);
      Ks =[];

      for i=1 : Q
        Ks = [Ks K];
      end
      Ks = [Ks R];

      for j=1:nb_of_m_signals
        
        obj.M = [obj.M; 
                 [mat2cell(obj.microsSignals(: , j) , Ks)]'];
        
      end
      
    

      for i=1:nb_of_m_signals
        for j=1:Q
          obj.M{i,j} = fft(obj.M{i,j});
          obj.M{i,j} = obj.M{i,j}(1:257);
        end
      end

      %3.Conserver uniquement les r�sultats correspondant � la fr�quence f0 et former la matrice Y

      %Cr�ation des matrices Y pour chacun des micros
      obj.Y = zeros(nb_of_m_signals , Q);

      for i =1 : nb_of_m_signals
        Yi = [];
        for j=1:Q
          [maxi , ind] = max(obj.M{i,j});
          V = obj.M{i,j}(ind);
          Yi = [Yi V];
        end
        obj.Y(i , :) = Yi(:);
      end

      Yc = transpose(conj(obj.Y));
      obj.TM = (1/(Q)*(obj.Y*Yc));

      %6. Examiner les valeurs de valeurs propres de ^?M. Que constatez vous ? Combien de sources sonores

      [A, B] = eig(obj.TM)
      %N-S = 4 -1 = 3 valeurs propres non nulles
      %N = 4 nb de micro

      %% Part 3 
      % M�thode MUSIC
      Gb = A(:,1:end -1); 
      Gs = A(:,end);

      %3. projecteur dans l'espace bruit 
      pib =zeros(size(A , 2));
      for i=1:size(Gb,2)
          pib = pib + Gb(:,i)*transpose(conj(Gb(:,i)));
      end

      % calcul du pseudo-spectre
      r = (0:0.5:20);
      theta = (0:1:180);

      P=zeros(length(r), length(theta));
      
      for nb_theta=1:length(theta)
          for nb_r=1:length(r)
              sv = steering_vector(r(nb_r),theta(nb_theta),1000, ANTENNE);
              svh = transpose(conj(sv));
              P(nb_r, nb_theta) = 1/(svh*pib*sv);
          end
      end

      figure(4)
      surf(abs(P))
      xlabel('theta')
      ylabel('r')
      zlabel('P')
    
    end

    
  end
    
  
end