function [ fo, ind ] = max_energie(X, Fe)
%max_energie Summary of this function goes here
%   Renvoie la fr�quence et l'indice du maximum d'�nergie � partir d'un signal temporel
ind = find(abs(X).^2>=max(abs(X).^2));
f = (0:length(X)/2+1)*(Fe/length(X));
fo = f(ind);
end

