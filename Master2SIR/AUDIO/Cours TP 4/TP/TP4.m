%clear all
close all
%clc


%% TP1
% 1. Chargements des signaux
data1 = load('data1.mat');
data2 = load('data2.mat');
data3 = load('data3.mat');

% tailles des signaux
pas = 1/data1.MICROS.Fe; 
size_data_1 = size(data1.MICROS.Signal);
size_data_2 = size(data2.MICROS.Signal);
size_data_3 = size(data3.MICROS.Signal);

% 3. Tracer l'ensemble des signaux issus des microphones. Que constatez
% vous
figure(1)
subplot(4,1,1)
plot(data1.MICROS.t, data1.MICROS.Signal(:,1))
subplot(4,1,2)
plot(data1.MICROS.t, data1.MICROS.Signal(:,2))
subplot(4,1,3)
plot(data1.MICROS.t, data1.MICROS.Signal(:,3))
subplot(4,1,4)
plot(data1.MICROS.t, data1.MICROS.Signal(:,4))

%on constate que les signaux des microphones 1 � 4 sont les m�mes. 

% 4. Mesurer le d�calage temporel entre chacun des microphones. En d�duire
% l'azimuth de la source.

    % Mesure temporelle du premier pique pour les sinus de chacun des signaux
T1 = 0.000839;
T2 = 0.0006349;
T3 = 0.0004989;
T4 = 0.0003401;


    % Calcul du retard entre le micro 4 et le micro 1
retard = abs(T4 - T1)/3;

    % D�termination de l'angle azimut
theta = acos(retard*data1.ANTENNE.C/data1.ANTENNE.D);
theta = theta *180/pi; 

%5 D'apr�s l'allure des signaux, la source se situe-t-elle en champ lointain ? 

% non car l'amplitude des signaux sont l�g�rement diff�rents, on est donc
% dans le cas d'une onde sph�rique.

%6. Ecrire une fonction qui d�termine la fr�quence f0 pr�sente avec un
% maximum d'�nergie dans le signal

%regarder max_energie.m
size_data_M = size(data1.MICROS.Signal, 1);
Fe = data1.MICROS.Fe;
freq = (0:size_data_M - 1)*(Fe/size_data_M);


M1_FFT = fft(data1.MICROS.Signal(:,1));
M2_FFT = fft(data1.MICROS.Signal(:,2));
M3_FFT = fft(data1.MICROS.Signal(:,3));
M4_FFT = fft(data1.MICROS.Signal(:,4));


figure(3)
subplot(4,1,1)
plot(freq(1:size(freq, 2)/2), (abs(M1_FFT(1:size(freq,2)/2))).^2)
subplot(4,1,2)
plot(freq(1:size(freq, 2)/2), abs(M2_FFT(1:size(freq,2)/2)).^2)
subplot(4,1,3)
plot(freq(1:size(freq, 2)/2), abs(M3_FFT(1:size(freq,2)/2)).^2)
subplot(4,1,4)
plot(freq(1:size(freq, 2)/2), abs(M4_FFT(1:size(freq,2)/2)).^2)

find(abs(M4_FFT)>=max(abs(M4_FFT)))
% f0 = max_energie(data1.MICROS.Signal(:,1), Fe)
% max = 101
% freq(101) = 999.77 Hz

%retourner f0
%% Part 2 
% Matrice de covariance

%1. Decouper le signal de chaque microphones en fenetres de K points

K = 512; 
R = mod(size_data_1(1), K); % on ne prend pas en compte le reste, on l'oublie
Q = floor(size_data_1(1)/K);
M1 = mat2cell(data1.MICROS.Signal(:,1), [K K K K K K K K R]);
M2 = mat2cell(data1.MICROS.Signal(:,2), [K K K K K K K K R]);
M3 = mat2cell(data1.MICROS.Signal(:,3), [K K K K K K K K R]);
M4 = mat2cell(data1.MICROS.Signal(:,4), [K K K K K K K K R]);

%2. Effectuer sur chaque fen�tre une FFT (pour chaque signal)

for i=1:Q
    M1{i,1} = fft(M1{i, 1});
    M1{i,1} = M1{i, 1}(1:257);
    M2{i,1} = fft(M2{i, 1});
    M2{i,1} = M2{i, 1}(1:257);
    M3{i,1} = fft(M3{i, 1});
    M3{i,1} = M3{i, 1}(1:257);
    M4{i,1} = fft(M4{i, 1});
    M4{i,1} = M4{i, 1}(1:257);
end

%3.Conserver uniquement les r�sultats correspondant � la fr�quence f0 et former la matrice Y

    %Cr�ation des matrices Y pour chacun des micros
    
Y1=[];
Y2=[];
Y3=[];
Y4=[];

for i=1:Q
    ind = find(abs(M1{i,1})>=max(abs(M1{i,1})));
    V = M1{i,1}(ind);
    Y1 = [Y1 V];
end

for i=1:Q
    ind = find(abs(M2{i,1})>=max(abs(M2{i,1})));
    V = M2{i,1}(ind);
    Y2 = [Y2 V];
end

for i=1:Q
    ind=find(abs(M3{i,1})>=max(abs(M3{i,1})));
    V = M3{i,1}(ind);
    Y3 = [Y3 V];
end

for i=1:Q
    ind=find(abs(M4{i,1})>=max(abs(M4{i,1})));
    V = M4{i,1}(ind);
    Y4 = [Y4 V];
end

%4. D�terminer une estimation de la matrice de covariance sigmaM estim�e
Y = [Y1; Y2; Y3; Y4];
Yc = transpose(conj(Y)); % ne pas utiliser ctranspose
TM = (1/(Q)*(Y*Yc)); % on ne prend pas la derni�re fen�tre

%5. Examiner la matrice sigmaM estim� obtenue (Matrice hermitienne?,
%semi-d�finie positivz?)

%SI

%6. Examiner les valeurs de valeurs propres de ^?M. Que constatez vous ? Combien de sources sonores
% semblent ^etre presentes dans l'environnement ?
[A, B] = eig(TM);
%N-S = 4 -1 = 3 valeurs propres non nulle
%N = 4 nombre de micro
%N valeurs positives dans les valeurs propres
% on peut utiliser Spectrogramme pour d�terminer fo

%% Part 3 
% M�thode MUSIC

%2 d�terminer les valeurs et les vecteurs propres de la matrice(sigma m)
Gb = A(:,1:3); % c'est pib 
Gs = A(:,end);

%3. projecteur dans l'espace bruit 
pib =zeros(4);
for i=1:3
    pib = pib + Gb(:,i)*transpose(conj(Gb(:,i)));
end

% ou pib = A(:,1:3) avec la matrice des 

% calcul du pseudo-spectre
r = (0:0.5:20);
theta = (0:1:180);
P=zeros(length(r), length(theta));
for nb_theta=1:length(theta)
    for nb_r=1:length(r)
        sv = steering_vector(r(nb_r),theta(nb_theta),1000,data1.ANTENNE);
        svh = transpose(conj(sv));
        P(nb_r, nb_theta) = 1/(svh*pib*sv);
    end
end

%trac� de la figure
figure(4)
surf(abs(P))
xlabel('r')
ylabel('theta')
zlabel('P')
