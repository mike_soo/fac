clear all; clc ; close all;

[HRIR , Fe ] = hrir_loader(60,0,'1003');

% Extraction de hrirs
hrir_g = HRIR(: , 1);
hrir_d = HRIR(: , 2);

% Creation d'axe temporelle
temp = [0 : size(HRIR, 1) - 1] / Fe;
Te = 1/Fe
% Affiche des hrir de chaque oreille
figure('name' , '2.1.2')
hold on;
plot(temp , hrir_g);
plot(temp , hrir_d);
hold off;
title('Question:2.1.2')
legend('hrir oreille gauche' , 'hrir oreille droite');
xlabel('temps [s]')
ylabel('amplitude[dB]')


% Calcul de hrtfs
hrtf_g = abs(fft(hrir_g));
hrtf_d = abs(fft(hrir_d));
hrtf_g =hrtf_g(1:end /2);
hrtf_d =hrtf_d(1:end /2);
% Axe des frequences de hrts
freq = [0 : size(hrtf_g,1) - 1]/ ( size(hrtf_g, 1) * Te);

%affichage hrtfs
figure()
plot(freq , hrtf_g);
title('hrtf oreille gauche')
xlabel('frequence [Hz]')
ylabel('amplitude[dB]')

figure()
plot(freq , hrtf_d);
title('hrtf oreille droite')
xlabel('frequence [Hz]')
ylabel('amplitude[dB]')

% Calcul d'amplitude des hrtfs

amp_hrtf_g =  moyenne_glissante(hrtf_g,Fe);
amp_hrtf_d =  moyenne_glissante(hrtf_d,Fe);

%affiche amplitudes en fonction de la frequence

figure()
plot(freq , amp_hrtf_g)
title('amplitudes lisées Hl')
xlabel('frequence [Hz]')
ylabel('amplitude[dB]')

figure()
plot(freq , amp_hrtf_d)
title('amplitudes lisées Hr')
xlabel('frequence [Hz]')
ylabel('amplitude[dB]')


% Calcul de tout les HRTF_F pour toutes les elevations possible
clear all;
elevations = [-30 -15 0 15 30 45 60 75 90];
HRTFS_G = [];
Fes = [];
size(elevations , 2)
for i=1 : size(elevations,2)
    
    [HRTF_G , Fe_g]  = hrir_loader(60,elevations(i),'1003');
    HRTFS_G = [HRTFS_G HRTF_G(: , 1)];
    Fes = [Fes Fe_g];
end

for i = 1 : length(Fes)
    for j = 1 : size(HRTFS_G , 2)
        
    end
end
