function [hrir,Fs] = hrir_loader(az,el,to_load)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% [hrir,Fs] = hrir_loader(az,el,to_load)                               %
%                                                                      %
% SORTIES                                                              %
%                                                                      %
% hrir : Matrice contenant les hrir correspondantes aux oreilles       %
%        gauches et droites pour la position (az,el)                   %
%        - hrir(:,1) : Oreille gauche                                  %
%        - hrir(:,2) : Oreille droite                                  %
% Fs   : Fr�quence d'�chantillonnage associ�e � ces hrirs.             %
%                                                                      %
% ENTREES                                                              %
%                                                                      %
% az      : Position d�sir�e en azimuth                                %
%           ( 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150 165, 180,  %
%           ... 195, 210, 225, 240, 255, 270, 285, 300, 315, 330, 345) %
% el      : Position d�sir�e en �l�vation                              %
%           (-30 / -15 / 0 / 15 / 30 / 45 / 60 / 75 / 90)              %
% to_load : num�ro de la banque de hrir � charger                      %
%           (1002 ou 1003)                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Chargement des donn�es
cd Data
load(['IRC_' num2str(to_load) '_R_HRIR.mat'])
cd ..

% Recherche de l'azimuth et de l'�l�vation
az_vec = l_hrir_S.azim_v ;
el_vec = l_hrir_S.elev_v ;

if el==90
    az = 0;
end

az_ind = find(az_vec==az) ;
el_ind = find(el_vec==el) ;
pos_ind = intersect(az_ind,el_ind) ;

if isempty(pos_ind)
    
    % Position non-trouv�e
    display('ERREUR: La direction indiqu�e n''existe pas dans la base de donn�e...')
    hrir= NaN ;
    Fs  = NaN ; 
    
else
    
    % Extraction des hrirs
    hrir = zeros(size(l_hrir_S.content_m,2),2) ;

    Fs = l_hrir_S.sampling_hz ;
    hrir(:,1) = l_hrir_S.content_m(pos_ind,:) ;
    hrir(:,2) = r_hrir_S.content_m(pos_ind,:) ;
    
end

