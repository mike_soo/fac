function [RF_moy] = moyenne_glissante(RF,fs)
%
% R�alise un moyennage glissant de la r�ponse fr�quentielle RF.
%
% [RF_moy] = moyenne_glissante(RF,fs)
%
% RF : R�ponse fr�quentielle � moyenner
% fs : Fr�quence d'�chantillonnage

coeff = 150 ;

InterT = size(RF,1) ;
N = size(RF,2) ;

RF_moy = zeros(InterT,N) ;

for n = 1:N
    
    for k =1:InterT
        delta = round(InterT/100);
        
        n_min = max(1,k-delta) ;
        n_max = min(InterT,k+delta) ;
        RF_moy(k,n) = mean(abs(RF(n_min:n_max,n)));
        
        end
        
    end
        
end