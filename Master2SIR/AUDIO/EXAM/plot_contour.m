function [] = plot_contour(el,f_vec,HRTF_L,f_min,f_max)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% plot_contour(el,f_vec,HRTF_L)
%
% Trac� des HRTFs avec des contours
%
% ENTREES
% el : elevations pour lesquelles le trac� doit �tre effectu�
% f_vec : vecteur de fr�quence associ� au HRTFs
% HRTF_L : Amplitude liss� des HRTFs en fonction
% de la fr�quence et de l'�l�vation (matrice 2D)
% f_min et f_max : Fr�quence minimales et maximales du trac�
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a = figure() ;
set(a,'windowstyle','docked')
contour(el,f_vec,HRTF_L,(min(HRTF_L(:)):1:max(HRTF_L(:))))
ylim([f_min f_max])
ylabel('Fr�quence (Hz)')
xlabel('Elevation (�)')
cbar =colorbar ;
set(get(cbar,'ylabel'),'String','Amplitude (dB)')