close all 

clc

%%%
%%% SIMULATION DE LA PROPAGATION
%%%
%%% ***********************************************************************

% Chargement du signal de source
% -------------------------------------------------------------------------
% [signal,fs,nbits] = audioread('voix1.wav');
audioName = 'phrase.wav';
[signal,fs] = audioread(audioName);
audioInfo = audioinfo(audioName)
N = length(signal);
tp = [0:N-1].'*1/fs;
p = 1/ fs
figure('name' , 'signal and sous echantilloné signal');
hold on;
plot(tp , signal , 'g');  
% plot(tp , signalsousechantillone.Data,'r');
hold off;
%normalisation du signal
signal_source = [tp, signal/max(abs(signal))];
structSignal= {};
structSignal.time=tp;
structSignal.signals.values=signal;
structSignal.signals.dimensions=1;
structSignal
% we sort the signal amplitudes values
sortedsignal = sort(signal);

% we use dif the diferentiate the sorted amplitudes values looking 
% to find the minimum variation between 2 amplitudes. That will be our
% quantum values.
sorteddif= sort(diff(sortedsignal));
min (sorteddif(sorteddif>0))
figure('name','sorted signal');
plot(sortedsignal);
% Pour le sinus :
f0 = 1000;

% Dur�e de la simulation (en sec):
T_sim = 1.4;


% Filtre passe-bas
% -------------------------------------------------------------------------
load filtre

% Param�tres de l'antenne
% -------------------------------------------------------------------------
ANTENNE.N = 4; % Nombre de microphones
ANTENNE.C = 340; % Vitesse du son
ANTENNE.D = 0.8; % Distance entre les microphones
for i=1:ANTENNE.N % Position des microphones
    ANTENNE.Pos(i) = (i-(ANTENNE.N + 1)/2)*ANTENNE.D;
end