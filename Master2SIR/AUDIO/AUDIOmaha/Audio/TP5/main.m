%Cri du prof
load('data5.mat')
sig = MICROS.Signal(:,1);
f = MICROS.fe;

%Notes
F = {'mi','do','la','re','do','si','do','la','do','si','la','si','sol','la','sol','mi','mi','do','la','la','si','do','re','si','mi','re','mi'};

%Octaves
O = [1,2,1,2,2,1,2,1,2,1,1,1,1,1,1,1,1,2,1,1,1,2,2,1,2,2,2]-1.7;

%Dur�es
D = [1,2,2,1,1,1,2,2,1,1,1,2,1,2,1,2,1,2,2,1,1,1,2,1,2,2,2]/2;

%PLAY
to_note(sig,f,F,D,O);