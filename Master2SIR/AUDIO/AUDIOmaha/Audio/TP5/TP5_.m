close all
clear all
%% Etude du diagramme d'antenne
% ---------------------------------------------------------
ANTENNE.N = 8; % Nombre de microphones
ANTENNE.C = 340; % Vitesse du son
ANTENNE.D = 0.16; % Distance entre les microphones
for i=1:ANTENNE.N % Position des microphones
    ANTENNE.Pos(i) = (i-(ANTENNE.N + 1)/2)*ANTENNE.D;
end

% % Antenne
% load('data1.mat')

% Source
load('source1.mat');

% % FFT
% sig = SOURCE.signal;
% Fe = SOURCE.fe;
% sig_fft = abs(fft(sig));
% frq = [0:length(sig_fft)-1]*Fe/length(sig_fft);
% frq = frq(frq<Fe/2);
% sig_fft = sig_fft(1:length(frq));
% plot(frq,sig_fft)
%
% % DIAGRAMME
% theta = [1:180]';
% E = zeros(length(theta),1);
% for itheta = 1:length(theta)
%     y = diagramme(SOURCE,theta(itheta),ANTENNE);
%     E(itheta) = rms(y);
% end
% polar(theta*pi/180,E)

%% Formation de voie
% -------------------------------------------------

load('data1.mat')
%pointes � 647 & 795 Hz

theta = [1:180]';
E = zeros(length(theta),1);
for itheta = 1:length(theta)
    y = beamforming(MICROS.Signal,MICROS.fe,ANTENNE,theta(itheta));
    E(itheta) = rms(y);
end
polar(theta*pi/180,E)

%% Formation de voie temporelle
% -------------------------------------------------

load('data4.mat')
N = size(MICROS.Signal,2);
%pointes � 647 & 795 Hz

%Taille des fen�tres
K = 2048;

%Adaptation des fen�tres
Ntot = size(MICROS.Signal,1);
L = ceil(Ntot/K);
s = [MICROS.Signal ; zeros(K-(rem(Ntot,K)),N)];
Ntot = size(s,1);

t = ones(360,1)/2;
maxs = zeros(L,1);

for idx = 1:L
    E = zeros(360,1);
    for theta = 1:180
        sK = s((idx-1)*K+1:idx*K,:);
        y = beamforming(sK,MICROS.fe,ANTENNE,theta);
        E(theta) = rms(y);
    end
    [m,th] = max(E);
    t(th) = m;
    maxs(idx) = th;
    polarplot(E)
    hold on
    polarplot(t)
    hold off
    drawnow()
    t(th) = 0.5;
end
close all

dir = median(maxs);
%plot(maxs)
hold on
%plot(ones(259,1)*dir)
pause
close all

sig_dir = beamforming(MICROS.Signal,MICROS.fe,ANTENNE,dir);

soundsc(MICROS.Signal(:,1),MICROS.fe)
pause(4)
soundsc(sig_dir,MICROS.fe)