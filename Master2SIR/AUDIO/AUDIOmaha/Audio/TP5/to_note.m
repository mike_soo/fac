function to_note(sig,Fe,LF,LD,LO)

f0 = fondamental(sig,Fe);

sig = [sig ; sig(end:-1:1)];
sig = [sig ; sig(end:-1:1)];
sig = [sig ; sig(end:-1:1)];

N = length(sig);
L = N/Fe;

notes = {'do';'dod';'re';'mib';'mi';'fa';'fad';'sol';'lab';'la';'sib';'si'};

for idx = 1:length(LF)
    D = LD(idx);
    O = LO(idx);
    note = find(strcmp(notes, LF(idx)),1) + 12*O;
    F = 261.63*2^((note-1)/12);
    
    Fe2 = Fe*F/f0;
    
    N2 = ceil((N*D/L)*(F/f0));
    sig2 = sig(1:N2);
    
    soundsc(sig2,Fe2)
    pause(0.9*D)
    
end
end