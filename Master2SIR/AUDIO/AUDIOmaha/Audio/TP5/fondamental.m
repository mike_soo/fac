function f0 = fondamental(sig,Fe)
%Renvoie la fr�quence du signal portant
%le maximum d'�nergie

sig_fft = abs(fft(sig(:,1)));
frq = [0:length(sig_fft)-1]*Fe/length(sig_fft);

frq = frq(frq<Fe/2);
sig_fft = sig_fft(1:length(frq));

sig_fft(frq<20)=0;

% plot(frq,sig_fft)

[~,idx] = max(sig_fft);
f0 = frq(idx);

end