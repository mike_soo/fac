function [itf,ild,itd] = itf_from_hrir(az_pos,el_pos,to_load)

[HR,~] = hrir_loader(az_pos,el_pos,to_load);

HRL=(HR(:,1));
HRL = HRL(:);

HRR=(HR(:,2));
HRR = HRR(:);

FFTL = fft(HRL);
FFTR = fft(HRR);

itf = FFTL./FFTR;
ild = abs(itf);
itd = unwrap(angle(itf));

end