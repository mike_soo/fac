function aff = TFDCT(x,FS,T)

Ntot = length(x);
Nper = fix(T*FS);
Nwin = ceil(Ntot/Nper);

s = [x ; zeros(Nper-(rem(Ntot,Nper)),1)];
Ntot = length(s);

fq = [0:Nper-1]/Nper*FS;
limit = find(fq>(FS/2),1);
fq = fq(1:limit);

aff = zeros(limit,Nwin);

for k = 1:Nwin
    signal = s((k-1)*Nper+1 : k*Nper);
    TFD = abs(fft(signal));
    TFD = TFD(1:limit);
    
    aff(:,k) = 20*log10(TFD);
end
aff = flipud(aff);
imagesc((aff-min(aff(:)))/(max(aff(:))-min(aff(:))))
end