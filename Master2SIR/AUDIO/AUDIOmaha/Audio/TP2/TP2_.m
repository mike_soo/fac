[bruit_blanc,FS] = audioread('bruit_blanc.wav');
sinus = audioread('sinus.wav');
voix = audioread('voix.wav');

tp = [0:length(voix)-1]'/FS;
fq = [0:length(voix)-1]'/length(voix)*FS;

voixTFD = abs(fft(voix));

limit = find(fq>22050,1);
voixTFD = voixTFD(1:limit);
fq = fq(1:limit);

plot(fq,voixTFD)