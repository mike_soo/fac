function f0 = fondamental(sig,Fe)
%Renvoie f0 la fr�quence du signal portant 
%le maximum d'�nergie

sig_fft = abs(fft(sig(:,1)));
frq = [0:length(sig_fft)-1]*Fe/length(sig_fft);

frq = frq(frq<Fe/2);
sig_fft = sig_fft(1:length(frq));

 [~,idx] = max(sig_fft);
 f0 = frq(idx);

end