function Proj = proj_bruit(M)

[vec_p, val_p] = eig(M);
val_p = diag(val_p);

idx_bruit = val_p < max(val_p)/5;
idx_s = val_p >= max(val_p)/5;

vec_bruit = vec_p(:,idx_bruit);
vec_s = vec_p(:,idx_s);

Proj = zeros(size(M));
for k = 1:size(vec_bruit,2)
    Proj = Proj + vec_bruit(:,k)*vec_bruit(:,k)';
end

end