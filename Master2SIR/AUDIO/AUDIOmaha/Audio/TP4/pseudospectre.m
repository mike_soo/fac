function S = pseudospectre(V,P)

k = V'*P*V;
S = abs(1/k);

end