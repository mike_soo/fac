function mat = cov_mat(sig,Fe,K)
%Renvoie la matrice de covariance correspondant au signal donn�

N = size(sig,2);
Ntot = size(sig,1);
f0 = fondamental(sig(:,1),Fe);
L = ceil(Ntot/K);

Y = zeros(N,L);

for m = 1:N
    
    s = [sig(:,m) ; zeros(K-(rem(Ntot,K)),1)];
    Ntot = length(s);
    frq = [0:K-1]*Fe/K;
    frq = frq(frq<Fe/2);
    findd = find(frq>f0);
    if0 = findd(1)-1;
    
    Km = zeros(1,L);
    
    for idx = 1:L
        sK = s((idx-1)*K+1:idx*K);
        sKfft = (fft(sK));
        sKfft = sKfft(1:length(frq));
        Km(idx) = sKfft(if0);
    end
    Y(m,:) = Km;
end
mat = Y*Y'/L;
end