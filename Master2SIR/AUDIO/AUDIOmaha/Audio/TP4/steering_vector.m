% *************************************************************
% Calcul du vecteur d'antenne
% *************************************************************

function V = steering_vector(r,theta,f,ARRAY)

pos = ARRAY.Pos.';

for nb_r=1:length(r)
    for nb_theta=1:length(theta)
        for nb_f=1:length(f)
            k = 2*pi*f(nb_f)/ARRAY.C;
            
            if (r(nb_r) == Inf) % champ lointain
                
                V(:,nb_f) = exp(j*k*pos*cos(theta(nb_theta)*pi/180));
                
            else
                
                d = sqrt(r(nb_r)*r(nb_r) + pos.*pos - 2*r(nb_r).*pos.*cos(theta(nb_theta)*pi/180));
                tau = d./ARRAY.C;
                V(:,nb_f) = r(nb_r)./d.*exp(j*k*r(nb_r)).*exp(-j*2*pi*f(nb_f).*tau);
                
            end
        end
    end
end
