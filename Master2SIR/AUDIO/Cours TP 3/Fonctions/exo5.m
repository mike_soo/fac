clear all ; clc ; close all;
addpath('../Données')
figure('name' , 'hrir: reponse impulsion temporelle')
hold on
angleSourceD = 30
[hrir , fe_hrir] = hrir_loader(angleSourceD , 0 , '1002');
Te = [1:size(hrir,1)]'  * (1/fe_hrir);
plot(Te , hrir(: , 1 ));
 
plot(Te , hrir(: , 2 ));
hold off

figure('name' , 'hrtf: reponse impulsion frequentielle')
hold on
hrtf_g = fft(hrir(: , 1 ));
hrtf_d = fft(hrir(: , 2 ));
f = [0:size(hrir(:,1) , 1) - 1] / size( hrir(: , 1 ) , 1) * fe_hrir;
plot(f , abs(hrtf_g));
plot(f , abs(hrtf_d));


[corr , lags] = xcorr(hrir(: , 1) , hrir(: , 2));

figure('name' ,'correlation entre oreille gauch droite')
plot(lags,corr)

[ corMax  , argCorMax] = max(corr);

% en unité echantillon, la valeur "temporelle" ou la corelation est max autrement
% le nombre de deplacement que l'on a effectué pour que la correlation des signaux
% soit maximale
targCorMax = lags(argCorMax);

% targCorMax correspond à l'indice ou se retrouve la valeur temporelle (en s) 
% dans Te
itd = Te(abs(targCorMax));
itd = itd * sign(targCorMax)

hrir_d_non_delayed = delayseq(hrir(: , 2) , targCorMax);
figure('name' , 'undelayed hrirs')
hold on
plot(Te , hrir(: , 1 ));
plot(Te , hrir_d_non_delayed);
hold off


%% Calcul de diamètre de la tête du sujet 
angleSourceR = angleSourceD * pi / 180; 
c = 340;
diamTeteSujet = (c * itd) / (angleSourceR + sin(angleSourceR))

itdWoodworth = (diamTeteSujet / c) * (angleSourceR + sin(angleSourceR))