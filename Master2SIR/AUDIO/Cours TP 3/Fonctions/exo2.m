addpath('../../Cours TP 2/Matlab/')
addpath('../Données')
clc;
clear all;
close all;

figure('name' , 'hrir: reponse impulsion temporelle')
hold on
[hrir , fe_hrir] = hrir_loader(30 , 0 , '1002');
Te = [1:size(hrir,1)]'  * (1/fe_hrir);
plot(Te , hrir(: , 1 ));
 
plot(Te , hrir(: , 2 ));
hold off

figure('name' , 'hrtf: reponse impulsion frequentielle')
hold on
ffthrir_g = fft(hrir(: , 1 ));
ffthrir_d = fft(hrir(: , 2 ));
f = [0:size(hrir(:,1) , 1) - 1] / size( hrir(: , 1 ) , 1) * fe_hrir;
plot(f , abs(ffthrir_g));
plot(f , abs(ffthrir_d));



hold off;