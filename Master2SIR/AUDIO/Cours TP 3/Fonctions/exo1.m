addpath('../../Cours TP 2/Matlab/')
clc;
clear all;
close all;

T = 0.5; %secondes
fe = 44100;

signal_mono = gene_mono(T,fe);
% soundsc(signal_mono , fe);

signal_stereo_itd = apply_itd(signal_mono , 0.8 , fe);
% soundsc(signal_stereo_itd , fe);
signal_stereo_ild = apply_ild(signal_mono , 10);
% soundsc(signal_stereo_ild , fe);

%  generating ild signal to compasate itd = 0.3
signal_stereo_itd_comp = apply_itd(signal_mono , 0.6 , fe);
% soundsc(signal_stereo_itd , fe);
signal_stereo_ild_comp = apply_ild(signal_mono , 5);
% soundsc(signal_stereo_ild , fe);

showTemporalSignal(fe , signal_mono , 'signal mono');
showTemporalSignal(fe , signal_stereo_ild_comp , 'signal stereo ild');
showTemporalSignal(fe ,signal_stereo_itd_comp , 'signal stereo itd');

