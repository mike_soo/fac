clear all ; clc ; close all;
addpath('../Données')

load('office.mat')
load('bathroom.mat')
load('stairway.mat')
%load('bathroom.mat')


figure('name' , 'office reverb analysis')

% creation de l'axe des abscisses  (temps)
te_office = [1:size(office_h_air,2)] * 1 / office_fs;
plot(te_office' , office_h_air)

figure('name' , 'bathroom reverb analysis')
te_bathroom = [1:size(bathroom_h_air,2)] * 1 / bathroom_fs;
plot(te_bathroom' , bathroom_h_air)


fe_bruit = 44000;
duree_bruit = 0.1;
N_bruit =  floor(duree_bruit / (1/fe_bruit)) + 1 ; 
s_bruit = rand( N_bruit, 1);
axe_temp_bruit = [0 : N_bruit - 1] * (1 / fe_bruit);
figure('name' , 'bruit généré ');
plot(axe_temp_bruit , s_bruit);

conv_res_bruit_bathroom = convq(s_bruit , bathroom_h_air);
conv_res_bruit_office = convq(s_bruit , office_h_air);
conv_res_bruit_stairway = convq(s_bruit , stairway_h_air);


figure('name' , 'convolution entre bruit et rep imp office');
plot(conv_res_bruit_office);
plot(conv_res_bruit_bathroom);
plot(conv_res_bruit_stairway);

soundsc(conv_res_bruit_bathroom , bathroom_fs);

soundsc(conv_res_bruit_office  , office_fs);

soundsc(conv_res_bruit_stairway , stairway_fs);