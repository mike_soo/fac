close all
clear all
%% Etude du diagramme d'antenne
% ---------------------------------------------------------

%Q.1 Choix des valeurs adapt�es aux param�tres
ANTENNE.N = 8; % Nombre de microphones
ANTENNE.C = 340; % Vitesse du son
ANTENNE.D = 0.16; % Distance entre les microphones
for i=1:ANTENNE.N % Position des microphones
    ANTENNE.Pos(i) = (i-(ANTENNE.N + 1)/2)*ANTENNE.D;
end

%  Antenne
% load('data1.mat')

%Q.2 Charger le signal
% Source
load('source1.mat');

%Analyse du contenu fr�quentiel du signal
%FFT
sig = SOURCE.signal;
Fe = SOURCE.fe;
sig_fft = abs(fft(sig));
frq = [0:length(sig_fft)-1]*Fe/length(sig_fft);
frq = frq(frq<Fe/2);
sig_fft = sig_fft(1:length(frq));
%Trac� de la Tranform�e de Fourier en fonction de la fr�quence
plot(frq,sig_fft)


%Q.3 DIAGRAMME
%La fonction diagramme permet d'obtenir la sortie
%de la formation de voie pour les parametres choisis.
theta = [1:180]';
E = zeros(length(theta),1);
for itheta = 1:length(theta)
    y = diagramme(SOURCE,theta(itheta),ANTENNE);
    E(itheta) = rms(y); %rms(y)=sqrt(sum(abs(y).^2)
end

%Q.4 Trac� du diagramme d'antenne 
%plot(theta*pi/180,E)
polar(theta*pi/180,E)

%Q.5 Influence des param�tres :
%Le nombre de lobes d�pend du nombre des micros (plusieurs micros =
%plusieurs lobes)
%Plus on augmente le nombre de micros, plus le lobe est fin ==> Antenne 
%plus directive (pour un �cartement entre les micros constant)

%Distance entre les microphones : plus la distance est grande, meilleure
%est la polarisation. L'antenne devient directive et les autres directions
%deviennent moins importantes.

%Plus le capteur est de grande taille, meilleures seront les performances. 
%Cela s'explique par le fait qu'en chaque position, on observe le signal
%sur une portion de l'espace (un sinus sur une echelle de l'espace) ssi on a une infinit� de micros

%On a alors globalement la distance entre les antennes qui correspond � Te la periode
%d'�chantillonage  --> possibilit� de soucis de repliement d'apr�s le
%th�or�me de Shannon Spatial.
% d <d_max = lambda_min / 2 = c/(2fmax)
%Shannon : la distance entre les micros doit etre plus petite que la moitie
%de la longueur d'onde
%Existence de plusieurs lobes principaux ==> respecte pas le th�or�me de
%Shannon.

%Les basses frequences sont tr�s mal localis�es et spatialis�es. Cela est d� au fait que le
%lambda associ� � ces signaux, on a alors la distance entre les micros qui
%n'est pas assez grande afin d'assurer un bon echantillonage, on ne vera
%seulement qu'une partie du sinus, l'antenne sera donc tr�s mal polaris�e
%==> Plus la freq � localiser est basse, plus le diagramme � localiser est 
%large (donc mauvaise r�solution) 

% Quand on fait varier la distance entre les micros on a 
%On pr�l�ve l'information accoustique en ANTENNE1.N points de l'espaces

%Plus la taille de l'antenne est petite, plus le lobe est large.
%Mais plus il est grand, plus il est difficile de l'embarquer




%%
% %% Utilisation de la formation de voie pour la localisation
% % -------------------------------------------------
% 
%Q.1 Charger le signal 
load('data1.mat')
%pointes � 647 & 795 Hz


%Q.2 Calcul de la carte d'energie pour des amiziths 
%compris entre 0 et 180 degres
%BEAMFORMING
theta = [1:180]'; %Amizuths
E = zeros(length(theta),1);
for itheta = 1:length(theta)
    y = beamforming(MICROS.Signal,MICROS.fe,ANTENNE,theta(itheta));
    E(itheta) = rms(y);
end

%Q.3 Trac� de la carte
polar(theta*pi/180,E)
%Emission depuis 60� dans 2 directions diff�rentes


%Q.4 D�coupage des signaux issus des microphones
load('data2.mat')
%Taille des micros
N = size(MICROS.Signal,2);
%pointes � 647 & 795 Hz

%Taille des fen�tres
K = 2048;

%Adaptation des fen�tres
Ntot = size(MICROS.Signal,1);
L = ceil(Ntot/K);
s = [MICROS.Signal ; zeros(K-(rem(Ntot,K)),N)];
Ntot = size(s,1);

t = ones(360,1)/2;
maxs = zeros(L,1);

for idx = 1:L
    E = zeros(360,1);
    for theta = 1:180
        sK = s((idx-1)*K+1:idx*K,:);
        y = beamforming(sK,MICROS.Fe,ANTENNE,theta);
        E(theta) = rms(y);
    end
    [m,th] = max(E);
    t(th) = m;
    maxs(idx) = th;
    polarplot(E)
    hold on
    polarplot(t)
    hold off
    pause
    drawnow()
    t(th) = 0.5;
end
close all

dir = median(maxs);
%plot(maxs)
hold on
%plot(ones(259,1)*dir)
pause
close all

% sig_dir = beamforming(MICROS.Signal,MICROS.Fe,ANTENNE,dir);
% 
% soundsc(MICROS.Signal(:,1),MICROS.Fe)
% pause(4)
% soundsc(sig_dir,MICROS.Fe)
