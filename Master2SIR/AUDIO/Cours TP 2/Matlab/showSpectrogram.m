function showFreqSignal(varargin)
    signal      = varargin{1};
    fe          = varargin{2};
    nameofSignal= varargin{3};
    Nx  = length(signal);
    
    if (nargin >= 4) && (varargin{4} ~= -1)
        nsc = varargin{4};
    else
        nsc = floor(Nx/10);
    end
    

    if (nargin >= 5 )&& (varargin{5} ~= -1)
        nov = varargin{5};
    else
        nov = floor(nsc/2);
    end

    if (nargin >= 6) && (varargin{6} ~= -1)
        nff = varargin{6};
    else
        nff = max(256,2^nextpow2(nsc));
    end
    figure('name' , nameofSignal);
    
    
    % explanation of parameters here: https://stackoverflow.com/questions/29321696/what-is-a-spectrogram-and-how-do-i-set-its-parameters
    spectrogram(signal , hamming(nsc) , nov , nff , fe ,'yaxis' );
    ['temps fenetre : ' num2str(nsc*1/fe)]
end