close all 
clear all
clc

audioName1 = 'voix.wav'
audioName2 = 'sinus.wav'
audioName3 = 'bruit_blanc.wav'

[signal1,fe1] = audioread(audioName1);
[signal2,fe2] = audioread(audioName2);
[signal3,fe3] = audioread(audioName3);

audioInfo1 = audioinfo(audioName1)
audioInfo2 = audioinfo(audioName2)
audioInfo3 = audioinfo(audioName3)

% showTemporalSignal( fe1 , signal1 , audioName1);
% showTemporalSignal( fe2 , signal2 , audioName2);
% showTemporalSignal( fe3 , signal3 , audioName3);

reverseSignal1 = flip(signal1);
reverseSignal2 = flip(signal2);
reverseSignal3 = flip(signal3);


fftSignal1 = fft(signal1);
fftSignal2 = fft(signal2);
fftSignal3 = fft(signal3);

fftReverseSignal1 = fft(reverseSignal1);
fftReverseSignal2 = fft(reverseSignal2);
fftReverseSignal3 = fft(reverseSignal3);


% showFreqSignal(fftSignal1 , fe1 , 'fftSignal1')
% showFreqSignal(fftSignal2 , fe2 , 'fftSignal2')
% showFreqSignal(fftSignal3 , fe3 , 'fftSignal3')

% showFreqSignal(fftReverseSignal1 , fe1 , 'fftReverseSignal1')
% showFreqSignal(fftReverseSignal2 , fe2 , 'fftReverseSignal2')
% showFreqSignal(fftReverseSignal3 , fe3 , 'fftReverseSignal3')

% soundsc(signal1 , fe1);
% soundsc(signal2 , fe2);
% soundsc(signal3 , fe3);


window1 = 100 / audioInfo1.Duration;
window2 = 100 / audioInfo2.Duration;
window3 = 100 / audioInfo3.Duration;

spectrogram1 = spectrogram(signal1);

showSpectrogram(signal1 , fe1 , 'spectrogram de signal voix');
showSpectrogram(signal2 , fe2 , 'spectrogram de signal sinus');
showSpectrogram(signal3 , fe3 , 'spectrogram de signal bruit blanc');

showSpectrogram(signal1, fe3 , 'Heisenberg:spectrogram de signal voix temporelle ++ '    , length(signal1)/10000 , 0 )
showSpectrogram(signal1, fe3 , 'Heisenberg:spectrogram de signal voix frequencielle ++ ' , length(signal1)/6    , 0 )

showSpectrogram(reverseSignal1 , fe1 , 'spectrogram de signal voix inversée');

cochleogram(signal1 , 20 , 20000 , 4 , fe1);
cochleogram(signal1 , 20 , 20000 , 30 , fe1);
cochleogram(reverseSignal1 , 20 , 20000 , 30 , fe1)
cochleogram(signal2 , 20 , 20000 , 10 , fe2);
cochleogram(signal3 , 20 , 20000 , 10 , fe3);