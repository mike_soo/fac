function showTemporalSignal(fe , signal , nameOfSignal)
    N = length(signal);
    tp = [0:N-1].'*1/fe;
    figure('name' , nameOfSignal);
    hold on;
    
    if size(signal ,2) == 2
        plot(tp , signal(:,1) , 'r')
        plot(tp , signal(:,2) , 'g')
    else %if(size(signal , 2) == 1)
        plot(tp , signal , 'g');  
        
    end
    hold off;
end