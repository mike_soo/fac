function showFreqSignal(fftSignal , fe , nameOfSignal)
    Te= 1/fe;
    N = length(fftSignal);
    freqk = [1:N]./(N * Te);
    
    fftSignal = abs(fftSignal);
    figure('name' , nameOfSignal);
    hold on;
    plot(freqk , fftSignal , 'g');  
    hold off;

    
end