close all 
clear all
clc
N = 8;
img1=imread('vue droitef.png');
figure;
imshow(img1);
% [X , Y] = ginput(N);

img2=imread('vue gauchef.png');
figure;
imshow(img2);

pointsImg1= [
487.175333333333 , 716.856000000000;
439.207333333333 , 742.838666666667;
431.212666666667 , 696.869333333333;
477.182000000000 , 618.921333333333;
521.152666666667 , 542.972000000000;
415.223333333333 , 798.801333333333;
453.198000000000 , 830.780000000000;
487.175333333333 , 864.757333333333;
];
pointsImg2 =[                      % x   y   z    
  595.5371 , 739.7154; % 0 , 2 , 0
  551.4048 , 769.8056; % 0 , 1 , 0 
  545.3868 , 719.6553; % 0 , 1 , 1
  587.5130 , 643.4269; % 0 , 2 , 2 
  629.6393 , 563.1864; % 0 , 3 , 3
  543.3808 , 827.9800; % 1 , 0 , 0
  589.5190 , 858.0701; % 2 , 0 , 0
  637.6633 , 890.1663; % 3 , 0 , 0
];

worldPoints=[
 0 , 2 , 0;
 0 , 1 , 0;
 0 , 1 , 1;
 0 , 2 , 2;
 0 , 3 , 3;
 1 , 0 , 0;
 2 , 0 , 0;
 3 , 0 , 0;
]
% [cameraParams1,imagesUsed,estimationErrors] = estimateCameraParameters(pointsImg1,worldPoints)
% [cameraParams2,imagesUsed,estimationErrors] = estimateCameraParameters(pointImgs2,worldPoints)
%scatter3(X3d(:,1) , X3d(: , 2) , X3d(: , 3)); 

pointsImg1 = [pointsImg1 ones(size(pointsImg1,1),1)]
pointsImg2 = [pointsImg2 ones(size(pointsImg2,1),1)]

worldPoints = [worldPoints ones(size(worldPoints , 1),1)];

% scène gauche
Pa = dlt(worldPoints , pointsImg1);

% scène droite
Pb = dlt(worldPoints , pointsImg2);

% p1test = (Pa * worldPoints(1 , :)' )
% p1test = p1test / p1test(3)
% estimateCameraParameters

M = transfo2d3d(Pa , pointsImg1(1 , 1:2) , Pb , pointsImg2(1 , 1:2))

 
  
  
  