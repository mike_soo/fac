function M=  transfo2d3d(Pa , ma , Pb , mb);
    xa = ma(1);
    ya = ma(2);
    
    xb = mb(1);
    yb = ma(2);
    S =[   
        Pa(2,:) - (ya * Pa(3,:)) ;
        (xa * Pa(3,:)) - Pa(1,:) ;
        Pb(2,:) - yb * Pb(3,:)   ;
        xb* Pb(3,:)  - Pb(1,:)   ;
        ];

    [~,~,V] = svd(S)
    assignin('base','S',S)
    assignin('base','V',V)

    M = V();

end