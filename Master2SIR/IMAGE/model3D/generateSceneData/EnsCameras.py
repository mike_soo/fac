class EnsCameras():
    def __init__(self):
        self.cameras = []
    
    

    def addCamera(self , camera):
        self.cameras.append(camera)

    def toFile(self):
        fVoxels = open("./voxels.off", "a")
        fVoxels.write(str(len(self.cameras)) + "\n")
        for camera in self.cameras:
            camera.toFile(fVoxels)
        
        fVoxels.close()
        
