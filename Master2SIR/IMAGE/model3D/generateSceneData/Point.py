
class Point():
    def __init__(self , x , y , z):
        self.x = x
        self.y = y
        self.z = z
    def __str__(self):
        return '(' + str(round(self.x,4)) + ' , ' + str(round(self.y,4)) + ' , ' + str(round(self.z,4))+ ')'
    def setCoords(self , x , y , z):
        self.x = x
        self.y = y
        self.z = z
