from Laser import *
from Voxel import *

class EnsVoxel:
    def __init__(self , haut , larg , prof , cote , origVoxels):
        self.voxels = []

        self.larg = larg
        self.haut = haut
        self.prof = prof
        self.cote = cote

        for i in range(0 , larg ):
            for j in range(0 , haut ):
                for k in range(0 , prof ):
                    v= Voxel(
                        i*cote + origVoxels.x, 
                        j*cote + origVoxels.y, 
                        k*cote + origVoxels.z, 
                         cote )
                    self.voxels.append(v)

        # for voxel in self.voxels:
        #             + " i=" + str(i)
        #             + " j=" + str(j) 
        #             + " k=" + str(k) 
        #             + " x=" + str(self.voxel.x) 
        #             + " y=" + str(self.voxel.y) 
        #             + " z=" + str(self.voxel.z) )
    def intersect(self , pointInteret , type = Laser.typeAugm ):
    
        voxelIntersected = next((voxel for voxel in self.voxels 
            if ((voxel.x <= pointInteret.x <= (voxel.x + voxel.l))  and
                (voxel.y <= pointInteret.y <= (voxel.y + voxel.l))  and
                (voxel.z <= pointInteret.z <= (voxel.z + voxel.l))  and 
                (voxel.eff == False))) , None)

        if (voxelIntersected == None):
            return False

        # On incremente le nombre de fois qu'un voxel a été touché par un rayon
        if( type == Laser.typeAugm):
            voxelIntersected.v = voxelIntersected.v  + 1
        # Contact the type destructif , demande d'effacement de voxel activé
        elif (type == Laser.typeEfface):
            voxelIntersected.eff = True
        print('EnsVoxels3d.intersect(): pointInteret=' + str(pointInteret) + ' interesect avec voxel='+ str(voxelIntersected) )
        return True
            
        
    def subdivise(self , seuilv):
        subdivisedVoxels = []
        for voxel in self.voxels:
            if voxel.v >= seuilv:
                print("EnsVoxels.subdivise(): voxel.subdivise() : " + str(voxel.subdivise()))
                subdivisedVoxels.extend(voxel.subdivise())

        self.voxels = subdivisedVoxels
        
        print("EnsVoxels: subdivise() : self.voxels:")
        for i in range(0 , len(self.voxels)):
            print(self.voxels[i])

        if (len(self.voxels) >= 1 ):
            self.cote = self.voxels[0].l

    def toFile(self):
        fVoxels = open("./voxels.off", "w+")
        fVoxels.write("OFF\n")
        fVoxels.write(str(len(self.voxels))+ '\n')
        
        for i in range(0 , len(self.voxels)):
            fVoxels.write(str(self.voxels[i].x) + " " + str(self.voxels[i].y) + " " + str(self.voxels[i].z)  + " " + str(self.voxels[i].l) + "\n")
        
        fVoxels.close()
