from EnsCameras import *
from Laser import *
from EnsTirs import *
from EnsVoxel import *
from Point import *
from Vecteur import *

class World3d:
    def __init__(self):
        self.ensCameras = EnsCameras()
        self.wLaser = Laser()
        self.ensTirs = EnsTirs()

    def createEnsVoxels(self):
        origVoxels = Point(0 , 0, 0)
        self.ensVoxels = EnsVoxel(4 , 4 , 4 , 1 , origVoxels)

    def addCamera(self , camera):
        self.ensCameras.addCamera(camera)

    def addTir(self , tir):
        self.ensTirs.addTir(tir)

    def laserTirTest(self):
        vecTir = Vecteur()
        
        # print('World3d.laserTirTest(): vecteurTest 1' )
        vecTir.setData(Point(0.5 , 0 , 6) , Point(0 , 0.3 , -1))
        tir1 = self.wLaser.tir( vecTir , self.ensVoxels , 0.1 )
        self.ensTirs.addTir(tir1)
        # print('World3d.laserTirTest(): vecteurTest 2' )
        vecTir.setData(Point(6 , 0 , 0.5) , Point(-1 , 0.3 , 0))
        tir2 = self.wLaser.tir( vecTir , self.ensVoxels , 0.1)
        self.ensTirs.addTir(tir2)
        
    def subdivisionTest(self):
        print("World3d.subdivisionTest(): starting subdivision" )
        self.ensVoxels.subdivise(1)

    def toFile(self):
        self.ensVoxels.toFile()
        self.ensTirs.toFile()
        self.ensCameras.toFile()
