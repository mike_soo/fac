#include "Utils.hpp"
extern float ortho;


void Utils::readOFFVoxels(Base *base , vector <VoxelG*> &voxels , vector <Ligne*> &lignes, vector<Camera*> &cameras ,string nomFichierOFF)
{
	string line;
	ifstream source;
	source.open(nomFichierOFF, ios_base::in);
	if(!source)
	{
		cerr<<"Imposible d'ouvrir le fichier"<<endl;
		return;
	}

	getline(source,line);
	std::istringstream in(line);      //make a stream for the line itself

    std::string type;
    in >> type; // Lecture de OFF
    cout<<"Type : "<<type<<endl;
    if(type != "OFF")       //and check its value
    {
    	cerr<<"type de fichier invalide"<<endl;
    	return;
    }
	
    unsigned long long int nb_voxels;
    getline(source , line);
	in.str(line);
	in.seekg(0);
	cout <<"in "<<in.str()<<endl;

    in >> nb_voxels;
	cout <<"nb_voxels : "<<nb_voxels <<endl;
	double x, y, z ,l;
    
        
    for (unsigned long long int i = 0 ; i < nb_voxels; i ++)
    {
        getline(source,line);
        in.str(line);      //make a stream for the line itself
        in.seekg(0);

        in >> x >> y >> z >>l;       //now read the whitespace-separated floats
        // cout<<"Utils;;readOFFVoxels() : x="<<x<<" "<<" y="<<y <<" z="<<z<<endl;
        Vector* voxelOrig = new Vector(x , y , z , *base);
        voxels.push_back(new VoxelG(base , voxelOrig , l));


    }

    unsigned long long int nb_tirs;
    
    getline(source , line);
	in.str(line);
	in.seekg(0);
    in >>  nb_tirs;
	cout <<"nb_tirs : "<<nb_tirs <<endl;
	double x1, y1, z1 , x2, y2, z2 ;
    
        
    for (unsigned long long int i = 0 ; i < nb_tirs; i ++)
    {
        getline(source,line);
        in.str(line);      //make a stream for the line itself
        in.seekg(0);

        in >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;       //now read the whitespace-separated floats
        Vector* porigTir = new Vector(x1 , y1 , z1 , *base);

        Vector* pfinTir = new Vector(x2 , y2 , z2 , *base);

        lignes.push_back(new Ligne(porigTir , pfinTir));
    }
    
    
    // 
    // 
    // 
    // Cameras
    // 
    // 
    // 


    unsigned long long int nb_cameras;
    
    getline(source , line);
	in.str(line);
	in.seekg(0);
    in >>  nb_cameras;
	cout <<"nb_cameras : "<<nb_cameras <<endl;
	double vOx , vOy , vOz , vXx , vXy , vXz , vYx , vYy , vYz , vZx , vZy , vZz;

    Ligne *ligne1 , *ligne2 , *ligne3;
    
    cout<<setw(10)<<left<<"vOx"<<setw(10)<<left<<"vOy"<<setw(10)<<left<<"vOz"
        <<setw(10)<<left<<"vxx"<<setw(10)<<left<<"vxy"<<setw(10)<<left<<"vxz"
        <<setw(10)<<left<<"vyx"<<setw(10)<<left<<"vyy"<<setw(10)<<left<<"vyz"
        <<setw(10)<<left<<"vzx"<<setw(10)<<left<<"vzy"<<setw(10)<<left<<"vzz"<<endl;


    
    for (unsigned long long int i = 0 ; i < nb_cameras; i ++)
    {
        getline(source,line);
        in.str(line);      //make a stream for the line itself
        in.seekg(0);

        in >> vOx >> vOy >> vOz >> vXx >> vXy >> vXz >> vYx >> vYy >> vYz >> vZx >> vZy >> vZz;

        // Aggrandissement des lignes représentants les répères de la camera)
        vXx = (vXx  * 50);
        vXy = (vXy  * 50);
        vXz = (vXz  * 50);
        vYx = (vYx  * 50);
        vYy = (vYy  * 50);
        vYz = (vYz  * 50);
        vZx = (vZx  * 50);
        vZy = (vZy  * 50);
        vZz = (vZz  * 50);
        
        
        cout<<setw(10)<<left<<setw(10)<<vOx<<setw(10)<<vOy<<setw(10)<<vOz<<setw(10)<<vXx<<setw(10)<<vXy<<setw(10)<<vXz<<setw(10)<<vYx<<setw(10)<<vYy<<setw(10)<<vYz<<setw(10)<<vZx<<setw(10)<<vZy<<setw(10)<<vZz<<setw(10)<<endl;

        Vector * orig = new Vector(vOx , vOy , vOz , *base);
        Vector * vx   = new Vector(vXx + vOx , vXy + vOy , vXz + vOz , *base);
        Vector * vy   = new Vector(vYx + vOx , vYy + vOy , vYz + vOz , *base);
        Vector * vz   = new Vector(vZx + vOx , vZy + vOy , vZz + vOz , *base);


        ligne1 = new Ligne( orig , vx );
        ligne2 = new Ligne( orig , vy );
        ligne3 = new Ligne( orig , vz );

        cameras.push_back(new Camera(ligne1 , ligne2 , ligne3));

        
    }


}