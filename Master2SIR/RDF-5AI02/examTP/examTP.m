clear all ; close all;


%% SVM
load 'data.mat'

svm = SVM(baseApp , labelApp)
svm.calcSeparatorPlan('primal')

figure('name' , 'separateur optimal');
hold on;
svm.showSeparatorPlan()
svm.showPoints()
hold on;

%% Adaboost

% Petite erreur retrouvé lors de l’exécution des bases de fournis pour l'examen,
% cependant les exemples de tp marche et ils sont vérifiables dans le fichier
% ex1et2Adaboost.m

adaboost2D = Adaboost(baseApp , labelApp' , 4);
adaboost2D.decisionBorders();