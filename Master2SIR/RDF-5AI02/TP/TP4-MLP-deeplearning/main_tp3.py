
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt


def mlp1def(m,n):
     return np.random.uniform(-1,1,(m,n))
 
def mlp2def(m,c,n):
    w1 = np.random.uniform(-1,1,(c,n))
    w2 = np.random.uniform(-1,1,(m,c+1))
    return w1, w2

def sigmo(v):
  v[v<-2] = -2
  v[v>2] = 2
  return (1 - np.exp(-2 * v)) / (1 + np.exp(-2 * v))
  #return 1 / ( 1 + np.exp(-v))
    

def mlp1run(x, w):
    #x = np.insert(x,0,1, axis=1)
    y = np.zeros((w.shape[0], x.shape[0]))
    v = np.zeros((w.shape[0], x.shape[0]))
    v = np.dot(x,w.T)           
    y = sigmo(v)
    
    return y.T, v.T    

def mlp2run(x, w1, w2):
    print("mlp2run : w2 shape=")
    print(w2.shape)
    print("mlp2run : x  shape=")
    print(x.shape)
    print(x)
    y1 = np.zeros((w1.shape[0], x.shape[0]))
    y2 = np.zeros((w2.shape[0], x.shape[0]))
    v1 = np.zeros((w1.shape[0], x.shape[0]))
    v2 = np.zeros((w2.shape[0], x.shape[0]))
    
    

    v1 = np.dot(x,w1.T)
    print('mlp2run : v1=')
    print(v1)
   # v1 = np.insert(v1,0,1, axis=1)
    y1 = sigmo(v1)
    y1 = np.insert(y1,0,1, axis=1)
    #v1 = np.insert(v1,0,1, axis=1)
    v2 = np.dot(y1,w2.T)
    y2 = sigmo(v2)
    
    return [y1.T, y2.T, v1.T, v2.T]   
  
 
def initialize_poids(n_inputs, n_neurone):
    poids = list()
    for i in range(n_neurone):
        poids.append({'poids': mlp1def(1,n_inputs)}) 
    return poids 
            
 
def initialize_network(n_inputs, n_cachee, n_output):
    network = list()
    hidden_layer = initialize_poids(n_inputs, n_cachee)
    output_layer = initialize_poids(n_cachee, n_output)
    network.append(hidden_layer)
    network.append(output_layer)
    return network

def forward_propagation(network, inputs):
    for layer in network:
        new_inputs =[]
        for neurone in layer:
            new_inputs.append(mlp1run(inputs, neurone['poids'][0]))
        inputs = new_inputs
    return inputs
        
def mlpclass(y):  
    classe = np.zeros(y.shape[1])
    for i in range(y.shape[1]):
        classe[i] = np.argmax(y[:,i])
    return classe
    
def score(Label, LabelD):
    score = 0
    for i in range(len(Label)):
        if Label[i] == LabelD[i]:
            score += 1
    score = score/len(Label)
    rate = score * 100
    return score, rate

def sigmop(v):
    return  (1 - sigmo(v)**2)

def label2target(c):
    output = -1 * np.ones((len(np.unique(c)), len(c)))
   # output = -1 * np.ones((n_neurone_sortie, len(c)))
    n_neurone_sortie = len(np.unique(c))
    output = []
    for i in range(0,len(c)):
        a =  -1 * np.ones(n_neurone_sortie)
        a[c[i]] = 1
        #a[c.index(c[i])] = 1
        """if c[i] == 0:
            a[c[i]] = 1
        else:
             a[c[i]-1] = 1"""
        output.append(a)
        """if c.index(c[i]) > output.shape[0]-1:
            print(c.index(c[i]),i)
            output[c.index(c[i]) - 1 ,i] = 1  
        else:
            output[c.index(c[i]),i] = 1"""
    return np.asarray(output).T

def mlperror(y, target):
   print("mlperror()  y.shape =")
   print(y.shape)
   return y - target

def sqrerror(error):
    
    error = pow(error,2)
    sq = np.zeros(error.shape[1])
    for i in range(error.shape[1]):
            sq[i] = 1/2*sum(error[:,i])       
    return sq

def delta(error, v):
    print("delta():error =")
    print(error)

    print("delta():error =")
    print(error)
    v_prime = sigmop(v)
    return np.multiply(error, v_prime)

def delta2(error, v1, v2, w2):
    v_prime_1 = sigmop(v1)
    v_prime_2 = sigmop(v2)
    
    delta_1 = delta(error, v_prime_2)
    #print(delta_1.shape)
    w2 = np.delete(w2,0,axis=1)
    print(delta_1.shape)
    dq_a = np.dot(delta_1.T, w2)
    #print(dq_a.shape, v_prime_1.T.shape)
    delta_2 = np.multiply(dq_a, v_prime_1.T)
    #print(delta_2.shape)
    return delta_2
    

def mlp1train(x, target, w, lr, it):
    
    lk = np.zeros(it)
    for i in range(it):
        y, v = mlp1run(x,w)
        err = mlperror(y, target)
        lk[i] = sum(sqrerror(err))
        delt = delta(err, v)
       # x = np.insert(x,0,1, axis=1)
        w = w - lr * (np.dot(delt, x))
       # x = x[:,1:]
    
    return np.asarray(w), lk

def mlp2train(x, target, w1, w2, lr, it):
    
    lk = np.zeros(it)
   
    for i in range(it):
        
        [y1, y2, v1, v2] = mlp2run(x,w1, w2)
        err = mlperror(y2, target)
        delt_2 = delta(err, v2)
        nw2 = w2 - lr * (np.dot(delt_2, y1.T)/y2.shape[1])
        
        delt_1 = delta2(err, v1, v2, w2)
    
        nw1 =  w1 - lr * (np.dot(delt_1.T, x)/y2.shape[1])
         
        lk[i] = sum(sqrerror(err))
        
        w1 = np.copy(nw1)
        w2 = np.copy(nw2)
    
    return np.asarray(nw1), np.asarray(nw2), lk
    
    
     
        
  
# X_test = np.load('basetest.npy').T
# y_test = np.load('labeltest.npy')
# X_train = np.load('basetrain.npy').T
# y_train = np.load('labeltrain.npy')

# affichage:
"""plt.figure(1, figsize=(3, 3))
plt.imshow(X_train[0:,:].reshape(28,28),cmap=plt.cm.gray_r)
plt.show()"""


# adding x0 ( set to 1 ) for bias
#X_train = np.insert(X_train,0,1, axis=1)
   
   
"""a = initialize_network(2,2,1)
for layer in a:
     for neurone in layer:
         print(neurone)
                
y = forward_propagation(a,[1,2,3,4,5,6,7,8,9,10])"""


# 4 jeux d’entrees pour un reseau a 5 cellules :
x = np.array([[-1, -0.1, 0.8, 0.7, 1], [-3, -0.1, 1, 2.7, 0.2] , [-2, -0.2, 3, 2.5, 1]]) #, [-0.3, -0.9, 0, 0.1, 2]
y_train = [0 , 1 , 0 ]

nb_neurone = len(y_train)
nb_feature = x.shape[1] #nombre de pixels (5)
print("nb_feature "+ str(x.shape[1]))
x = np.insert(x,0,1, axis=1)
# definition d’un reseau a 5 entrees et 2 cellules:
# w = mlp1def(nb_neurone, nb_feature+1)
c = 3

[w1, w2] = mlp2def(nb_neurone, c, nb_feature+1)
# calcul des sorties du reseau:
#y, v = mlp1run(x,w)

c = [0, 1, 2]
 
target = label2target(y_train)
print("main target = ")
print(target)
#error = mlperror(y, target)
   
#a = updatePoids(y[0,0], target[:,0], w[0,0], x[0,0])
it = 2
lr = 0.05
# apprentissage:
#[nw, L] = mlp1train(x, target, w, lr, it) # affichage:
[nw1, nw2, L] = mlp2train(x, target, w1, w2, lr, it)
axe_x=np.linspace(1,it,it) 
plt.plot(axe_x,L)
plt.ylabel('Cout quadratique') 
plt.xlabel("Iterations d’apprentissage") 
plt.show()

# estimation WTA des labels:
#y, v= mlp1run(x,nw)

[y1, y2, v1, v2] = mlp2run(x,nw1, nw2)
Labels = mlpclass(y2)      
# score obtenu:
[rScore, rate] = score(Labels,y_train)
#print( "Taux de reconnaissance: " + str(rate) )



