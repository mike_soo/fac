#!/usr/bin/python3
import math
import os
  # on linux / os x

class MLP:
    def __init__(self , basetrain , labeltrain , basetest , labeltest):
        # ones = np.full((1 , np.shape(basetrain)[1]) , 1)    
        
        print("basetrain")
        print(basetrain)
        print(np.shape(basetrain))
        ones = np.ones((1 , np.shape(basetrain)[1]), dtype=np.int32 )
        self.basetrain  = np.concatenate((ones , basetrain) , axis=0)

        
        ones = np.ones((1 , np.shape(basetest)[1]), dtype=np.int32 )
        self.basetest   = np.concatenate((ones , basetest ) , axis=0) 
        
        self.basetrain  = np.matrix(self.basetrain)
        self.basetest   = np.matrix(self.basetest)

        self.labeltrain = labeltrain
        self.labeltest  = labeltest 
        self.imgsize    = np.shape(basetrain)[0]
        
        

    def getLabelOf(self , type ,i):
        if type == "train":
            return self.labeltrain[i]
        elif type== "test":
            return self.labeltest[i]
        else:
            return -1

    # permettant de générer une matrice de poids aléatoires
    # (distribution uniforme) dont les valeurs sont comprises dans l’intervalle [−1, +1] et de dimension
    
    def mlp1def(self , nbligs , nbcols):
        import random
        self.W=np.random.uniform(-1, 1, nbligs * (nbcols +1)).reshape([nbligs, nbcols + 1])
        self.m = nbligs
        self.nbOfClasses = nbligs

    @staticmethod
    def sigmox( x ):
        # print("sigmox: x= "+ str(x))
        s = None
        if (np.any(x > 708) or np.any(x < -354)):
            return 1 * np.sign(x)
        
        try:
            s = (1 - np.exp(-2*x)) / (1 + np.exp(-2*x))
        except:
            print("sigmox: except")
            print(x)
            raise ValueError('Exponential problems s=' + str(s))
        return  s
    @staticmethod
    def sigmoV( v ):
        print("sigmoV: v=" )
        print(v)
        
        # return [MLP.sigmox(el) for el in v ]
        # return MLP.sigmox(v)
        return np.apply_along_axis(MLP.sigmox , 1 , v)
    @staticmethod
    def dsigmox( x ):        
        return  1 - np.power(MLP.sigmox(x) , 2)

    # def dsigmoV( v ):
    #     return [MLP.dsigmox(el) for el in v.transpose() ].tranpose()
    @staticmethod
    def dsigmov1( v):
        np.apply_along_axis(MLP.dsigmox , 0 , v)
        return v 

    def mlp1run(self , X):
        self.nbOfImgs   = np.shape(X)[1]
        # print("mlp1run self.W: ")
        # print(self.W)
        # print("mlp1run X:")
        # print(X)
        self.V = self.W * X
        # print("ml1run: self.Y" )
        # print(self.Y)
        
        self.Y = np.apply_along_axis(MLP.sigmox , 0 , self.V )

        # self.Y = np.matrix([MLP.sigmoV(colEl) for colEl in self.Y.transpose()]).transpose()
        # print("ml1run: self.Y=" )
        # print(self.Y)
        # print("end sigmoverification \n\n")
    
    def mlpclass(self , i):
        # print("mlpclass : =i" + str(i))
        return np.argmax(self.Y[:,i])

    def mlpestimateClasses(self):
        # print("mlpestimatedClasses: estimatedClasses:")        
        self.estimatedClasses = [self.mlpclass(i) for i in range(0 , self.nbOfImgs ) ]
        # print(self.estimatedClasses)

    def setScore(self , label):
        self.score = np.sum([ 1 for i,l in enumerate(label) if l == self.estimatedClasses[i] ])
        # print("score: self.score="    + str(self.score))
        print("setScore: estimatedClases exact=")
        print(self.Y)

        print("setScore: estimatedClasses=")
        print(self.estimatedClasses)

        print("setScore: real classes=")
        print(self.C)

        self.succRate = self.score / len(label)
        print("score: self.succRate=" + str(self.succRate))

    def label2target(self , C ):
        # print("label2target: C=")
        # print(C)
        self.C = C
        self.Ydes = -1 * np.ones((self.nbOfClasses , len(C)) , dtype = np.int32)
        for i in range(0,len(C)):
            c = C[i]
            self.Ydes[c,i] =  1
        self.Ydes = np.array(self.Ydes)
        # print("label2target: Ydes")
        # print(self.Ydes)
    
    def error(self , i , j):
        return self.Y[i , j] - self.Ydes[i , j]  

    def setQ(self):
        # print("error: np.power(self.Y - self.Ydes)")
        # print(np.power(self.Y - self.Ydes , 2))
        self.Q=[1/2 *np.sum(np.power(self.Y[:,[i]] - self.Ydes[:,[i]] , 2)) for i in range(0 , self.nbOfImgs)]
        
        

        # print("erreur: self.Q=" , end='' , flush = True)
        # print(self.Q)
        print("erreur total :" + str(sum(self.Q)))
    
    def epsilonik(self , i , k):
        return self.Y[i , k] - self.Ydes[i , k]
        
    # X : base d'aprentissage où test
    # j : column index of a given element (coresponding to the index of an image column)
    # i : line index of a give row element (coresponding for exemple the i-th lvl of the vertical cels layers  )
    def deltaik(self , X , i , k):
        return self.epsilonik(i , k) * MLP.dsigmox( self.V[i,k])

    def calcdQdw(self , X):
        dQdw= np.zeros((self.m , self.imgsize + 1) , dtype=np.float64)
        epsilon = self.epsilon()
        dsigmoxs = np.apply_along_axis(MLP.dsigmox , 0 , self.Y)
        
        # print("calcdQdw")
        # print("epsilon")
        # print(epsilon)
        # print( "dsigmoxs")
        # print( dsigmoxs)
        
        # dQdw[i,j] = dQdw[i,j] + (self.deltaik(X , i , k) * X[j , k])
        dQdw[: , :] = (np.multiply(epsilon , dsigmoxs ) * X.transpose())
        
        return dQdw

    def epsilon(self):
        return self.Y - self.Ydes
        
    def apprentissage(self , lambdavar , X):
        # print("apprentissage: self.W" )
        # print(self.W)
        # print("self.calcdQdw(X):")
        # print(self.calcdQdw(X))
        self.dQdw =  self.calcdQdw(X)
        self.W = self.W - (lambdavar * self.dQdw)
        
class MLP2(MLP):
    def __init__(self , basetrain , labeltrain , basetest , labeltest , n , m , c):
        MLP.__init__(self , basetrain , labeltrain , basetest , labeltest)
        self.mlp1def(m , n)
        self.mlp2def(m , n , c)        

    def regression(self , lambdavar , X , XdesLabels):
        print("regression(): X=")
        print(X)
        print("regression(): Wc=")
        print(np.matrix(self.Wc))
        self.label2target(XdesLabels)
        for c in range(self.nbCouches -1 , -1 , -1):
            if c == self.nbCouches - 1:
                self.mlp1run(X)         
                self.mlpestimateClasses()
                self.apprentissage(lambdavar , X)
            else:
                V = self.Wc[:,:,c] * X 
                sigmoids = np.apply_along_axis(MLP.dsigmov1 , 1 , V )
            
                print("regression: c = " + str(c) + "nombre de couches = " + str(self.nbCouches))

                if c == self.nbCouches - 2:
                    self.deltaCs[:,:,c] = sigmoids * self.Wc[:,:,c] *  self.dQdw
                else:
                    self.deltaCs[:,:,c] = sigmoids * self.Wc[:,:,c] *  self.deltaCs[:,:,c + 1]


                self.Wc[: ,: , c] = self.Wc[: , : , c] - (lambdavar * self.deltaCs[: , : , c])


    def mlp2def(self , n , m , c):
        self.Wc = np.random.uniform(-1, 1, n * m * (c - 1)).reshape([m, n, c-1])
        self.deltaCs = np.zeros( m , n , c - 1)
        self.nbCouches = c

    def mlperror(self , y, target):
        print("mlperror()  y.shape =")
        print(y.shape)
        return y - target

    def mlp2train(self , x, target, w1, w2, lr, it):
        
        lk = np.zeros(it)
    
        for i in range(it):
            
            [y1, y2, v1, v2] = self.mlp2run(x,w1, w2)
            err = self.mlperror(y2, target)
            delt_2 = self.delta(err, v2)
            nw2 = w2 - lr * (np.dot(delt_2, y1.T)/y2.shape[1])
            
            delt_1 = self.delta2(err, v1, v2, w2)
        
            nw1 =  w1 - lr * (np.dot(delt_1.T, x)/y2.shape[1])
            
            lk[i] = sum(self.sqrerror(err))
            
            w1 = np.copy(nw1)
            w2 = np.copy(nw2)
        
        return np.asarray(nw1), np.asarray(nw2), lk

    def sigmo(self , v):
        v[v<-2] = -2
        v[v>2] = 2
        return (1 - np.exp(-2 * v)) / (1 + np.exp(-2 * v))

    def sigmop(self , v):
        return  (1 - self.sigmo(v)**2)

    def mlp2run(self , x , w1 , w2):
        y1 = np.zeros((w1.shape[0], x.shape[0]))
        y2 = np.zeros((w2.shape[0], x.shape[0]))
        v1 = np.zeros((w1.shape[0], x.shape[0]))
        v2 = np.zeros((w2.shape[0], x.shape[0]))
    
        print('mlp2run:x.shape[0] ' + str(x.shape[0]) +  ' x=')
        print(x)

        v1 = np.dot(x,w1.T)
        print('mlp2run : v1=')
        print(v1)
    # v1 = np.insert(v1,0,1, axis=1)
        y1 = self.sigmo(v1)
        y1 = np.insert(y1,0,1, axis=1)
        #v1 = np.insert(v1,0,1, axis=1)
        v2 = np.dot(y1,w2.T)
        y2 = self.sigmo(v2)
        
        return [y1.T, y2.T, v1.T, v2.T]   

    def delta(self , error, v):
        print("delta():error =")
        print(error)

        print("delta():error =")
        print(error)
        v_prime = self.sigmop(v)
        return np.multiply(error, v_prime)


    def delta2(self , error , v1, v2, w2):
        v_prime_1 = self.sigmop(v1)
        v_prime_2 = self.sigmop(v2)
        
        delta_1 = self.delta(error, v_prime_2)
        #print(delta_1.shape)
        w2 = np.delete(w2,0,axis=1)
        print(delta_1.shape)
        dq_a = np.dot(delta_1.T, w2)
        #print(dq_a.shape, v_prime_1.T.shape)
        delta_2 = np.multiply(dq_a, v_prime_1.T)
        #print(delta_2.shape)
        return delta_2

    def sqrerror(self , error):    
        error = pow(error,2)
        sq = np.zeros(error.shape[1])
        for i in range(error.shape[1]):
                sq[i] = 1/2*sum(error[:,i])       
        return sq
    

if __name__ == '__main__':
    # chargement des bases d'apprentissage et de test:
    import numpy as np
    import matplotlib.pyplot as plt

    # Test de sizes of bases
    # print("len of labeltrain:" + str(len(labeltrain)))
    # print(labeltrain)
    # print("len of labeltest:" + str(len(labeltest)))
    # print(labeltest)
    # print("len of basetrain:" + str(len(basetrain)))
    # print("len of basetrain[1,:]" + str(len(basetrain[1,:])))
    # print("len of basetest:" + str(len(basetest)))
    # print("len of basetest[1,:]" + str(len(basetest[1,:])))

    # affichage:
    # plt.figure(1 , figsize=(3 , 3))
    # plt.imshow( basetest[:,1].reshape(28,28) , cmap=plt.cm.gray_r)
    # plt.show()

    # rangement des exemples en colonne:
    # dimensions de la matrice obtenue:

    choice = 2
    XdesLabels=None
    nbOfClasses =None
    X=None

    # Un exemple d'aprentissage
    if(choice == 1):
        X = np.array([[-1, -0.5, 0, 0.5, 1]])
        XdesLabels = [1]
        nbOfClasses = 1
        mlp1 = MLP(X , XdesLabels , X , XdesLabels)
        mlp1.label2target( [1] )

    # 3 exemples d'aprentissage
    elif (choice == 2 or choice == 4):
        X = np.array([[-1, -0.5, 0, 0.5, 1], [-1, -1, 0, 0.5, -1], [-1, -2, 0, -0.5, 1]])
        XdesLabels = [1 , 2 , 3]
        nbOfClasses = 5

    # Bases minst d'apprentissage
    elif (choice == 3):
        X  = np.load('basetrain.npy')
        XdesLabels = np.load('labeltrain.npy')
        nbOfClasses = max(XdesLabels) + 1
    if(choice != 3):
        X = np.transpose(X)
    print("X:")
    print(X)
    print(np.shape(X))
    
    print("XdesLabels:")
    print(XdesLabels)
    print(np.shape(XdesLabels))
    
    print("nbOfClasses: " + str(nbOfClasses))

    mlp1 = MLP(X , XdesLabels , X , XdesLabels)
    mlp1.mlp1def(nbOfClasses , X.shape[0])
    mlp1.label2target( XdesLabels )
    lambdavar = 0.01
    # for i in range(0, 3000):
    #     print("i=" + str(i))
    #     e = (1 - math.exp(-2*i)) / (1 + math.exp(-2*i))
    #     print("i=-" + str(i))
    #     e = (1 - math.exp(-2*(-i))) / (1 + math.exp(-2*(-i)))

    # apprentisage 
    if 1 <= choice <=3:
        for i in range(0, 15000):
            print("mlp1run")
            mlp1.mlp1run(mlp1.basetrain)
            print("mlpestimateClasses")
            mlp1.mlpestimateClasses()
            print("setScore")
            mlp1.setScore(XdesLabels)
            print("apprentissage")
            mlp1.apprentissage(lambdavar , mlp1.basetrain)
            print("setQ")
            mlp1.setQ()
            
            # time.sleep(0.08)
            os.system('clear')
    
    # retropropagation
    if choice == 4:
        print("choice == 4")
        print("np.array(XdesLabels[0:1])")
        print(XdesLabels[0:1])
        mlp2 = MLP2(X[:,[0]] , XdesLabels[0:1] , X[:,[0]] , XdesLabels[0:1] , n = np.shape(X)[0] , m= nbOfClasses , c = 2)
        mlp2.regression(lambdavar , mlp2.basetrain , mlp2.labeltrain)
        

    # mlp1 = MLP(x , labeltrain , x , labeltest)
    # mlp1.mlp1def(3 , x.shape[0])

    # mlp1.mlp1run(X = mlp1.basetrain)
    # mlp1.mlpestimateClasses()
    # mlp1.setScore(xlabels)
    # mlp1.label2target([0,1,2])
    # mlp1.setQ()

    # lambdavar = 0.01
    # for i in range( 0 , 1000):
    #     mlp1.apprentissage(lambdavar , mlp1.basetrain)
    #     mlp1.mlp1run(mlp1.basetrain)
    #     mlp1.setQ()