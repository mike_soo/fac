import torch
import torch.nn as nn
from torch.autograd import Variable
import argparse
import os

from model import *
from generate import *

# Parse command line arguments
argparser = argparse.ArgumentParser()
argparser.add_argument('filename', type=str)
argparser.add_argument('--n_epochs', type=int, default=2000)
argparser.add_argument('--print_every', type=int, default=100)
argparser.add_argument('--hidden_size', type=int, default=50)
argparser.add_argument('--n_layers', type=int, default=2)
argparser.add_argument('--learning_rate', type=float, default=0.01)
argparser.add_argument('--chunk_len', type=int, default=200)
args = argparser.parse_args()

file, file_len = read_file(args.filename)

# Decoupage aleatoire des donnees en paquets 
def random_training_set(chunk_len):
    start_index = random.randint(0, file_len - chunk_len)
    end_index = start_index + chunk_len + 1
    chunk = file[start_index:end_index]
    inp = char_tensor(chunk[:-1])
    target = char_tensor(chunk[1:])
    return inp, target

# definition du decodeur, de l'algorithme d'optimisation, et de la fonction de perte
decoder = RNN(n_characters, args.hidden_size, n_characters, args.n_layers)
decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=args.learning_rate)
criterion = nn.CrossEntropyLoss()

# initialisation des variables
start = time.time()
all_losses = []
loss_avg = 0

# fonction principale d'entrainement (pour une epoque)
# calcul des sorties
# calcul de la perte
# retro-propagation de la perte
# une passe de l'algorithme d'optimisation
# sortie : la perte

def train(inp, target):
    hidden = decoder.init_hidden()
    decoder.zero_grad()
    loss = 0

	%%%%%%%%%%%%%%%%%%
    % VOTRE CODE ICI %
    %%%%%%%%%%%%%%%%%%

# sauve le modele
def save():
    save_filename = os.path.splitext(os.path.basename(args.filename))[0] + '.pt'
    torch.save(decoder, save_filename)
    print('Saved as %s' % save_filename)

# fonction principale d'entrainement
# creation du boucle pour iterer l'apprentissage sur n epoques
# stockage et affichage de la perte moyenne a chaque epoque
try:
    print("Training for %d epochs..." % args.n_epochs)
    
	%%%%%%%%%%%%%%%%%%
    % VOTRE CODE ICI %
    %%%%%%%%%%%%%%%%%%

except KeyboardInterrupt:
    print("Saving before quit...")
    save()

