
clear all;
close all;
clc;


trainDataFileName  = 'zip.train'
testDataFileName   = 'zip.test'

trainData  = importfiledata(trainDataFileName);
testData   = importfiledata(testDataFileName);



% 	showing 1D data sample as image
% trainDataSample  = trainData(1 , :);
% trainDataSample(1) = 0;
% figure('name', 'train data sample');
% hold on;
% imshow(trainDataSample);
% hold off;


%%    showing data sample in a 2d image
% trainDataSample2d = zeros(16 ,16);
% k=1;
% length(trainDataSample)
% for i= 1:16
%     for j = 1:16
%     	trainDataSample(k);
%     	trainDataSample2d(i,j);
%         trainDataSample2d(i,j)=trainDataSample(k);
%         k = k + 1;
%     end
% end

K = 10;
% figure('name' , 'train data sameple 2d');
% hold on;
% imshow(trainDataSample2d);
trainData(: , 1) = trainData(: , 1) + 1;
testData(: , 1) = testData(: , 1) + 1;
confusionTable = zeros(K , K);
for k =1 : 1
	noLabelTrainData=zeros(size( trainData , 1) , 256);
	trainDataY = [];

	 
	for i = 1 : size(trainData , 1)
		
		noLabelTrainData(i , 1:256) = trainData(i , 2:257);
		
		if k == trainData(i , 1)
			trainDataY(i) = -1;
		else
			trainDataY(i) =  1;
		end
	end

	Mdl=fitcsvm(noLabelTrainData , trainDataY , 'KernelFunction' , 'linear',...
		'BoxConstraint' , 1);
	noLabelTestData(: , 1:256) = testData(:,2:257);
	trueLabelsOfTestData = testData(: , 1);
	
	deducLablesOfTestData{k} = predict(Mdl,noLabelTestData); 
	% For each data element clasified as a k class show an image representation
	% of it.
	sizeOfTruePositives  = 0;
	sizeOfDeducPositives = 0;
	sizeOfDeducNegatives = 0;
	sizeOfFalsePositives  = 0;
	for l = 1:length(trueLabelsOfTestData)
		
		if trueLabelsOfTestData(l) == k && deducLablesOfTestData{k}(l) == -1 
			sizeOfTruePositives = sizeOfTruePositives + 1;
		elseif not(trueLabelsOfTestData(l) == k) && (deducLablesOfTestData{k}(l) == -1)
			sizeOfFalsePositives = sizeOfFalsePositives + 1;
		end


	end
	

	% InfoOfLablesOfTrainData = [ deducedLabelsOfTrainData trueLabelsOfTrainData];
	['TP=' num2str(sizeOfTruePositives) ' FP=' num2str(sizeOfFalsePositives)]

	['sensitivity of class ' num2str(k) ' TP/(TP + FP) = ' num2str(sizeOfTruePositives/(sizeOfTruePositives + sizeOfFalsePositives))]
	
end


% figure('name' , 'test data sample');
% hold on;
% imshow(te stDataSample);
% hold off;