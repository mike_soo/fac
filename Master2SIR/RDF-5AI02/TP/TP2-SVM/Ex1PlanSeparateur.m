clear all;
close all;
clc;
X = [0 0 
	 2 2
	 3 0 
	 2 0] ;

Y =[-1
	-1
	1
	1];

w= [1.2
	-3.2
	];
b=-0.5;

%
% figure()
% plot(X(:,1),X(:,2) ,'o');
% hold on

% hypx = zeros(1 , 20 );
% for i=1:20
% 	hypx(i) = i 
% end 


syms y

%'Calcul de plan separateur';
hypx = [0 1 2 3 4 5 6 7 8 9 10 ];
sols1=[];
sols2 = [];
N = size(X , 1);
M = size(X , 2);

for i = 1 : length( hypx)
	sol= solve((w(1)*hypx(i)) +(w(2)*y)+ b == 0, y);
	sols1 =[sols1 sol];

end

sols1;
sols2;



% plot(hypx(1 , :) , sols1(1 , :));
%}
%'Calcul de marge fonctionelle'
XT = X';

margesFonctionelles = [];

for i = 1 : N
    (w' * XT(:,i)) * Y(i);
	margesFonctionelles(i) = (w' * XT(:,i) + b) * Y(i);

end




%'Calcul de paramètres dhyperplan  avec marge fonctionelle = 1'

w = w / min(margesFonctionelles);
b = b / min(margesFonctionelles);


sols2 = [];


for i = 1 : length( hypx)
	sol= solve((w(1)*hypx(i)) +(w(2)*y)+ b == 0, y);
	sols2 =[sols2 sol];

end
% figure()
% plot(X(:,1),X(:,2) ,'o');
% hold on
% plot(hypx(1 , :) , sols2(1 , :));

for i = 1 : N
    (w' * XT(:,i)) * Y(i);
	margesFonctionelles(i) = (w' * XT(:,i) + b) * Y(i);

end

nouvelleMarge = min(margesFonctionelles);


%Optimistaion quadratique

u = [ w ;
      b];
B( 1 : N , 1) = b;
C( 1 : N , 1) = 1;

uns(1:size(X ,1) , 1) = 1;

A  =  [X uns];
Au =  A * u ;

%contient xn1* w1 xn2 *w2 .. xnm*wn + b ou n app { 1 , ... , n} et m correspond
% a la dimension des points (M);



C = ones(size(X,1) , 1);

zer0s(1 , 1 : M + 1) = 0;

Q = [ 
  		eye(M)                  zeros(M  , 1);
  		zer0s
	];

P = zeros(M + 1,1);


%multiplication de chaque coordonnées plus cst * Yi pour satisfaire Yn(wn*Xn +b)
for i = 1 : size( X , 1)
	for j = 1 : M + 1
		A(i,j) = A(i,j) * Y(i);
	end
end 

C = - C;
A = - A;
%minimizes 1/2*x'*Q*x + p'*x subject to the restrictions A*x ≤ b. The input A is a matrix of doubles, and b is a vector of doubles.
%original for debug purpose 
%	x = quadprog(H,f,A,b)
u = quadprog(Q,P,A,C)

svm = SVM(X , Y );
figure('name' , 'separateur optimal');
hold on;
svm.calcSeparatorPlan('primal');
svm.showSeparatorPlan();
svm.showPoints();
hold off;
Xnonsep = X;
Ynonsep = Y;



% We create a non divisible system by inversing the Y values leaving all the
% points that should be up the hyperplane under it and vice versa. This causes,
% as suspected a wrong estimation of the separator line.
% for i = 1: 5
% 	Xnonsep = [ Xnonsep ; 
% 				[i i+2]];
% 	Ynonsep = [Ynonsep; 1];
% end

% for i = 1:5
% 	Xnonsep = [Xnonsep ;
% 			   [i i-2]];
% 	Ynonsep = [Ynonsep; -1];
% end

% Xnonsep
% Ynonsep
% svmNonLinSeparable =SVM(Xnonsep , Ynonsep)

% figure('name' , 'separateur à ens de points non separables');
% hold on;
% svmNonLinSeparable.calcSeparatorPlan();
% svmNonLinSeparable.showSeparatorPlan();
% svmNonLinSeparable.showPoints();
% hold off;


%Mdl = fitcsvm(X,Y ,'KernelFunction' ,'linear', ...
%	'OptimizeHyperparameters','auto', ...
% 'HyperparameterOptimizationOptions', ...
% struct('AcquisitionFunctionName', ... 
% 	'expected-improvement-plus','ShowPlots',true))
% fitcsvm(X, Y,'KernelFunction','rbf','OptimizeHyperparameters','auto',... 'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName',... 'expected-improvement-plus','ShowPlots',true));
svm.listenForNewPoints('primal');

