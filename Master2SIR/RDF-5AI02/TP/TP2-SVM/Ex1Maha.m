clear all;
close all;
%Exercice 1
X = [0 0 ; 2 2 ; 3 0 ; 2 0];
Y = [-1; -1; 1; 1];
w = [1.2 ; -3.2];
b = -0.5;

[n,m] = size(X);
%Exercice 1 : Plan s�parateur
%question 1
xa = -1:10;
ya = -(w(1) .* xa + b)./w(2);
plot(xa,ya);
mat= [X,Y];
hold on;
scatter(X(1:2,1),X(1:2,2));
scatter(X(3:4,1),X(3:4,2));


%question 2
marge = [];
for i = 1 : 4
    rho = Y(i,1) * (w.' * X(i,:).' + b);
    marge = [marge; rho];
end

rho = min(marge);

%question 3
%hyperplan equivalent (w; b) et (w/rho; b=b/rho)

w_eq = w/rho;
b_eq = b/rho;

marge_eq = [];
for i = 1 : 4
    rho_eq = Y(i,1) * (w_eq.' * X(i,:).' + b_eq);
    marge_eq = [marge_eq; rho_eq];
end

ya_eq = -(w_eq(1) .* xa -b_eq)/w_eq(2);

%plot(ya_eq,xa);
%plot(ya_eq-ya,xa);