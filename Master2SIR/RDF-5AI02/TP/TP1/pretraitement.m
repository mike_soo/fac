close all
clear all
name=['B'; 'C'; 'D'; 'E'; 'F'; 'G';'H';'I';'J';'K'];

tab=[];
for lettre=1:size(name)
    for i=1:250 %250
        name1=[name(lettre,:) num2str(i) '.bmp'];
        I=imread(name1);
        [y,x]=find(I);%find(X) returns a vector containing the linear 
                      %indices of each nonzero element in array I.

       % Propertis_Larg = regionprops(I,'BoundingBox','Area','Centroid','MajorAxisLength','MinorAxisLength','Eccentricity','Orientation');
        %rectangle('Position',Propertis_Larg(J).BoundingBox,'EdgeColor','g','LineWidth',2);
        %ellipse(Propertis_Larg(J).MinorAxisLength,Propertis_Larg(J).MajorAxisLength,Propertis_Larg(J).Centroid(1,1),Propertis_Larg(J).Centroid(1,2),Propertis_Larg(J).Orientation,'r',Propertis_Larg(J).Area)
       % Read binary image into workspace.

%             BW = imread('text.png');

%             Calculate centroids for connected components in the image using regionprops.

        s = regionprops(I,'centroid');
        
        %Concatenate structure array containing centroids into a single matrix.

        centroids = cat(1, s.Centroid);

        %Display binary image with centroid locations superimposed.
%         figure(i)
%         imshow(I)
%         hold on
%         plot(centroids(:,1),centroids(:,2), 'b*')
%         hold off
%         x = centroids(255 , 1);
%         y = centroids(255 , 2);
        
        bbox = regionprops(I, 'BoundingBox');
        
        
        rect = [bbox(255).BoundingBox(1) bbox(255).BoundingBox(2) bbox(255).BoundingBox(3) bbox(255).BoundingBox(4) ];
%         rect =  [x-6 y-6 12 12];
%         
        J = imcrop(I,rect);
%         figure('name', ['croped image' i] );
%         imshow(J)
        
%         figure('name', ['croped and resized image ' i] );
        K = imresize(J,[12 12]);  
%         imshow(K)
%         I=#####;
%         I=#####;
        K=K(:)';
        tab=[tab ; K lettre];
        
%         figure('name', ['coded image' i]);
%         imshow(tab);
    end
end
save tab tab;
   
