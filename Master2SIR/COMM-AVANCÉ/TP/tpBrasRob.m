%% 
% Question 1
%
% Le couple de gravité sur l'axe 1 ne dépend pas de la position de l'axe 2 car le couple de l'axe 1 varie en fonction de la position du deuxième.
%
% Question 2 
%
% $$\vec{F_c} = -m\vec{a_c} = -2m\vec{\Omega_{R/Rg}} \wedge \vec{V_{M/R}}$$.
%
% Or, tous les repères Galiléens sont en translation rectiligne uniforme les un par rapport aux autres. Donc $$\vec{\Omega_{R/Rg}} = \vec{0}$$. 
% Ce qui implique que la force de Coriolis est nulle.
%
% Question 3
%
% Pour que le système soit découplé d'un point de vue inertiel, il faut chercher des positions $\theta_1$ et $\theta_2$ qui vont annuler les cosinus et sinus tels que : $\theta_1=\frac{ -\pi}{2}$ et $\theta_2=\frac{ -\pi}{2}$.
%
% Comme la position de 
% $$ \theta_1 $$ 
% est constante, alors : 
% $$ \dot \theta_{1} =  \ddot \theta_{1}= 0 $$
%
% et comme la vitesse de l'axe de rotation 2 est constante, alors :
% $$ \dot \theta_{2} = constante $$ 
% $\Rightarrow$
% $$ \ddot \theta_{2} = 0 $$ 
%
% Ainsi, nous retrouvons la formule suivante:
% $$ \ddot \theta_{1} = \dot \theta_{2}= 0 $$
% et
% $$ N_2K_{c_2}i_2 = \pmatrix{cos(\theta_2) & sign(\dot{\theta}_2) & \dot{\theta}_2 & 1 \cr} \pmatrix{\alpha_2 \cr a_2 \cr b_2 \cr c_2} $$
%
% Dans ce cas, nous aurons :
%
% $$\pmatrix{cos(\theta_2) = 0 \cr sign(\dot{\theta}_2) = 0\cr \dot \theta_2 = 0\cr}$$
%
% Question 5 
%
% Nous avons montré dans la question 4 que: 
%
% $$N_2K_{c2}i_2 = gm_2\lambda_2cos(\theta_2) +\Gamma_{f2}$$
%
% D'après l'équation (8), nous aurons pour l'axe 2 :
%
% $$\Gamma_{f2} = a_2sign(\dot \theta_2)+b_2(\dot \theta_2)+c_2$$
%
% Donc : $N_2K_{c2}i_2 = gm_2\lambda_2cos(\theta_2)+a_2sign(\dot \theta_2)+b_2(\dot \theta_2)+c_2$
%
% Alors : 
%
% $$N_2K_{c_2}i_2 = \pmatrix{cos(\theta_2) & sign(\dot{\theta}_2) & \dot{\theta}_2 & 1 \cr} \pmatrix{\alpha_2 \cr a_2 \cr b_2 \cr c_2} = Y(\theta_2,\dot \theta_2)p $$
%
% Avec : $\alpha_2 = gm_2 \lambda_2$
%
% Question 6
%
% Afin d'identifier p, nous pouvons choisir différentes positions des axes du robot afin d'isoler les paramètres $a_2$, $b_2$ et $c_2$.
%
% Pour cela, nous choisissons : $\theta_2 =\frac{\pi}{2}$
%
% Dans ce cas, nous aurons:
%
% $$\pmatrix{cos(\theta_2) = 0\cr sign(\dot{\theta}_2) = 0\cr \dot \theta_2 = 0 \cr}$$
%
% Question 7 
%
% Pour l'axe 1, nous considérons $\dot \theta_2 = 0 \Rightarrow  \ddot \theta_2$ = 0 et $\dot \theta_1$ = constante .
%
% L'équation en (5) devient : 
%
% $N_1K_{c1}i_1 = g(m_2l_1+m_1 \lambda_1)cos(\theta_1) + \Gamma_{f1}$
%
% or: $\Gamma_{f1} = a_1sign(\dot \theta_1)+b_1(\dot \theta_1)+c_1$
%
% Donc : $N_1K_{c1}i_1 = g(m_2l_1+m_1 \lambda_1)cos(\theta_1) + a_1sign(\dot \theta_1)+b_1(\dot \theta_1)+c_1$
%
% $N_1K_{c_1}i_1 = \pmatrix{cos(\theta_1) & sign(\dot\theta_1 & \dot\theta_1 & 1 \cr} \pmatrix{\alpha_1 \cr a_1 \cr b_1 \cr c_1} = c_1$
%
% Donc: $\alpha_1 = g(m_2l_1+m_1\lambda_1)$
%
% Cette fois-ci, nous choisissons 
%
% $\theta_1 = \frac{\pi}{2} \Rightarrow cos(\theta_1) = sign(\dot\theta_1) = \dot\theta_1 = 0$.
%
% Donc : $N_1K_{c_1}i_1 = \pmatrix{0 & 0 & 0 & 1 \cr} \pmatrix{\alpha_1 \cr a_1 \cr b_1 \cr c_1} = c_1$
%
% Ainsi: $c_1 = N_1K_{c1}i_1$
%
% De même manière que la démarche précédente, nous choisissons deux vitesses angulaires différentes $\dot{\theta}_1$. Nous aurons donc deux équations à deux inconnus que nous pouvons résoudre pour déterminer $a_1$ et $b_1$.
%
% $$ \pmatrix{N_1K_{c_1}i_1 \cr N_2K_{c_2}i_2}-g(q)-\Gamma_f = \pmatrix{I_1\prime +m_2l_1^{2}+I_{\alpha_1} & hcos(\theta_2-\theta1) \cr hcos(\theta_2-\theta_1) & I_2\prime+I_{\alpha_2}} \pmatrix{\ddot\theta_1 \cr \ddot\theta_2 } $$
%
% $+ \pmatrix{0 & -hsin(\theta_2-\theta1) \cr hsin(\theta_2-\theta_1) & 0} \pmatrix{\dot\theta_1^2 \cr \dot\theta_2^2}$ 
%
% Ainsi, nous retrouvons la formule (11) :
%
% $$ \pmatrix{N_1K_{c_1}i_1 \cr N_2K_{c_2}i_2}-g(q)-\Gamma_f = \pmatrix{\ddot\theta_1 & \ddot\theta_2cos(\theta_2-\theta1)-\dot\theta_2^2sin(\theta_2-\theta1) & 0 \cr 
% 0 & \ddot\theta_1cos(\theta_2-\theta1)+\dot\theta_1^2sin(\theta_2-\theta1) & \ddot\theta_2 } 
% \pmatrix{I_1\prime +m_2l_1^{2}+I_{\alpha_1} \cr h \cr I_2\prime +I_{\alpha_2} }$$
%
% Afin d'identifier les 3 paramètres de la matrice G, nous appliquons la méthode de pseudo-inverse : 
% $$ E = FG \Rightarrow {F}^t \!E = {F}^t \!FG \Rightarrow $$
% Nous obtenons donc :
% $$ G=({F}^tF)^{-1}{F}^tE $$
%
% Question 13
%
% Nous avons d'après la question 8 :
%
% $$ \pmatrix{N_1K_{c_1}i_1 \cr N_2K_{c_2}i_2}-g(q)-\Gamma_f = \pmatrix{\ddot\theta_1 & \ddot\theta_2cos(\theta_2-\theta1)-\dot\theta_2^2sin(\theta_2-\theta1) & 0 \cr 0 & \ddot\theta_1cos(\theta_2-\theta1)+\dot\theta_1^2sin(\theta_2-\theta1) & \ddot\theta_2 } \pmatrix{I_1\prime +m_2l_1^{2}+I_{\alpha_1} \cr h \cr I_2\prime +I_{\alpha_2} }$$
%
% Or, pour le premier axe nous avons : $\dot\theta_1=\dot\theta_2$ et $\theta_2 - \theta_1 =-\pi$
%
% Donc : $\ddot\theta_2cos(\theta_2-\theta1)-\dot\theta_2^2sin(\theta_2-\theta1) = 0$ car $sin(-\pi) = 0$ 
%
% et $\ddot\theta_1 = \ddot\theta_2 = 0$ (car $\dot\theta_1 = \dot\theta_2 =constante$).
%
% Donc : $\pmatrix{N_1K_{c_1}i_1}-g(q)-\Gamma_f = 0 \Rightarrow  \pmatrix{N_1K_{c_1}i_1} = g(q)-\Gamma_f$
%
% Nous retrouvons ainsi le modèle en $\dot\theta_2 = 0$
