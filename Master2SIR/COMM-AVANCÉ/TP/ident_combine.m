%% CE SCRIPT PERMET D'IDENTIFIER LES PARAMETRES DES
%% TERMES DE COUPLAGE POUR LES AXES 1 et 2
%% v1 G. MOREL - 2005/12/29
%% v2 V. PADOIS - 2007/10/31
%% v3 V. PADOIS - 2012/12/03

clear all;
load releve_mvts_combines;

%% constantes connues
kc1=0.0525;
N1=20.25;
kc2=0.0525;
N2=4.5;

%% Parametres identifies a vitesse constante
%% Pour l'axe 1 : 
alpha1 = 0.382160668685625;
a1     = 0.317228556167459;
b1     = 0.00431100260117954;
c1     = -0.0543505338469585;
%% Pour l'axe 2 :
alpha2 = 0.084924593041250;
a2     = 0.070495234703877;
b2     = 0.000958000578040;
c2     =-0.012077896410435;

%% identification a partir des donnees filtrees
for(i=1:length(t)) 
    qppf1 = qppfil1(i);
    qppf2 = qppfil2(i);
    qpf1  = qpfil1(i);
    qpf2  = qpfil2(i);
    q_1   = q1(i);
    q_2   = q2(i);
    i1f   = ifil1(i);
    i2f   = ifil2(i);
    
    
    Z(2*i-1:2*i,1:3)=[
         qppf1    (qppf2 * cos(q_2 - q_1)) - (qpf2^2 * sin(q_2 - q_1)) 0;
         0        (qppf1 * cos(q_2 - q_1)) - (qpf1^2 * sin(q_2 - q_1)) qppf2];
    u(2*i-1,1)= N1 * kc1 * i1f;
    u(2*i,1)  = N2 * kc2 * i2f;
end

%% Nous calculons par la suite les paramètres alpha2 a2 b2 c2 en appliquant l'équation (14)

p=pinv(Z)*u;
format long
disp('Parametres estimes a partir des donnees filtrees :');
p'

% reconstruction du modele complet
p1=p(1);
p2=p(2);
p3=p(3);

for(i=1:length(t)) 
    qppf1 = qppfil1(i);
    qppf2 = qppfil2(i);
    qpf1  = qpfil1(i);
    qpf2  = qpfil2(i);
    q_1   = q1(i);
    q_2   = q2(i);
    i1f   = ifil1(i);
    i2f   = ifil2(i);
    
    gravitation = [ alpha1 *cos(q_1);
                    alpha2 *cos(q_2)];
    inertie    = [
        (p1*qppf1)+(p2* cos(q_2 - q_1) * qppf2);
        (p2*cos(q_2 - q_1) * qppf1)+( p3* qppf2) ];
    centrifuge =[
        -p2 * sin(q_2 - q_1) * qpf2^2;
         p2 * sin(q_2 - q_1) * qpf1^2
    ];
    cfrottement = [
        (a1 * sign(qpf1)) + (b1 * qpf1) + c1;
        (a2 * sign(qpf2)) + (b2 * qpf2) + c2;
    ];
    %% couple d'inertie
    ciner(2*i-1,1)  = inertie(1); %% AXE 1 
    ciner(2*i,1)    = inertie(2); %%AXE 2
    %% couple centrifuge
    ccentri(2*i-1,1)= centrifuge(1); %% AXE 1
    ccentri(2*i,1)  = centrifuge(2); %% AXE 2
    %% couple de gravite
    cgravi(2*i-1,1) = gravitation(1); %% AXE 1
    cgravi(2*i,1)   = gravitation(2); %% AXE 2
    %% couple de frottements
    cfrott(2*i-1,1) = cfrottement(1); %% AXE 1
    cfrott(2*i,1)   = cfrottement(2); %% AXE 2
    %% couple total
    ctotal(2*i-1:2*i,1)=ciner(2*i-1:2*i,1)+ccentri(2*i-1:2*i,1)+cgravi(2*i-1:2*i,1)+cfrott(2*i-1:2*i,1);
end

%% Affichage des commandes.
figure() %% pour l'axe 1
clf
hold on
grid on
h=plot(t,N1*kc1*i1,'y');
h=plot(t,N1*kc1*ifil1,'b');
set(h,'LineWidth',1.5);
h=plot(t,ciner(1:2:length(ctotal)),'r');
set(h,'LineWidth',1.5);
h=plot(t,cgravi(1:2:length(ctotal)),'m');
set(h,'LineWidth',1.5);
h=plot(t,ccentri(1:2:length(ctotal)),'k');
set(h,'LineWidth',1.);
h=plot(t,cfrott(1:2:length(ctotal)),'g');
set(h,'LineWidth',1.);
h=plot(t,ctotal(1:2:length(ctotal)),'c--');
set(h,'LineWidth',1.5);
legend('\Gamma_1 mesure','\Gamma_1 filtre','inertie','gravite','centrifuge','frottements','modele total');
title('Resultats axe 1 ; identification a partir de donnees filtrees');
figure()
clf
hold on
grid on
h=plot(t,N2*kc2*i2,'y');
h=plot(t,N2*kc2*ifil2,'b');
set(h,'LineWidth',1.5);
h=plot(t,ciner(2:2:length(ctotal)),'r');
set(h,'LineWidth',1.5);
h=plot(t,cgravi(2:2:length(ctotal)),'m');
set(h,'LineWidth',1.5);
h=plot(t,ccentri(2:2:length(ctotal)),'k--');
set(h,'LineWidth',1);
h=plot(t,cfrott(2:2:length(ctotal)),'g');
set(h,'LineWidth',1);
h=plot(t,ctotal(2:2:length(ctotal)),'c--');
set(h,'LineWidth',1.5);
legend('\Gamma_2 mesure','\Gamma_2 filtre','inertie','gravite','centrifuge','frottements','modele total');
title('Resultats axe 2 ; identification a partir de donnees filtrees');
xlabel('temps')