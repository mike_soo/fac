clear all; 
clc ;
close all;

%% Question 10

load 'releve_vit_cste_axe2.mat'
who
figure();
plot(t,qp2)
xlabel('temps')
ylabel('vitesses sans régimes transitoires [rad/s]')
title('Vitesses mesurées non filtrées au cours du temps');

figure('name' , 'q2, i2, q2, ifil2 , qp2 , ifil2');
plot(q2, i2, q2, ifil2 , qp2 , ifil2)
legend('courantEnreg(positions) : axe2 [A]','courantFiltre(positions) : axe2 [A] ','courantFitlre(vitesse) : axe2 [A]');
title('Résultats axe 2: vitesses , positions , courant enregistré/filtré ');


figure('name' , 'q2 , qp2, ifil2');
plot3(q2, qp2 , ifil2)
xlabel('positions ang axe 2 [rad]')
ylabel('vitesses ang axe 2 [rad/s]')
zlabel('courant filtré axe 2 [A]')
title('Mouvement, positions et courant consommé ')

%% Question 11
run('ident_axe2_v_cste.m')

%% Question 12
run('ident_axe2_v_cste_filtre.m')

%% Question 14
run ('ident_axe1q14.m')

%% Question 15
run ('ident_combine.m')