import sys, select, termios, tty
import roslib
import message_filters
import rospy
import math

from geometry_msgs.msg import Twist, LaserScan


def callback(velocity, laserdata):
    pub = rospy.Publisher('~cmd_vel', Twist, queue_size=10)
    # Get laser data
    #angle_min = laserdata.angle_min
    #angle_max = laserdata.angle_max + 2*math.pi
    #nb_angle = int ((angle_max - angle_min) / laserdata.angle_increment)

    # Get velocity data
    #TODO
    print("coucou")
    if min(laserdata.intensities) < 1:
        new_vel = Twist()
        new_vel.linear.x = 0
        new_vel.linear.y = 0
        new_vel.linear.z = 0
        new_vel.angular.x = 0
        new_vel.angular.y = 0
        new_vel.angular.z = 0
        pub.publish(new_vel)

    else:
        pub.publish(velocity)


if __name__=="__main__":

    settings = termios.tcgetattr(sys.stdin)
    
    rospy.init_node('minilab_stopper')

    vel_sub = message_filters.Subscriber('~cmd_vel', Twist)
    laser_sub = message_filters.Subscriber('~scan', LaserScan)

    ts = message_filters.TimeSynchronizer([vel_sub, laser_sub], 10)
    ts.registerCallback(callback)
    rospy.spin()
