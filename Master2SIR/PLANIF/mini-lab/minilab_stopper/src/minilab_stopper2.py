#! /usr/bin/env python
import sys, select, termios, tty
import roslib
import message_filters
import rospy
import math
from sensor_msgs.msg import LaserScan

roslib.load_manifest('minilab_teleop')


from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

def scan_cb(scan):
    print("scan")
    print(scan)

def velocity_cb(velocity):
    print("callback")
    print(velocity)

def callback(velocity, laserdata):
    print("debug: in callback")
    pub = rospy.Publisher('~cmd_vel', Twist, queue_size=10)
    # Get laser data
    #angle_min = laserdata.angle_min
    #angle_max = laserdata.angle_max + 2*math.pi
    #nb_angle = int ((angle_max - angle_min) / laserdata.angle_increment)

    # Get velocity data
    #TODO
    print("velocity")
    print("laserdate")
    if min(laserdata.intensities) < 1:
        new_vel = Twist()
        new_vel.linear.x = 0
        new_vel.linear.y = 0
        new_vel.linear.z = 0
        new_vel.angular.x = 0
        new_vel.angular.y = 0
        new_vel.angular.z = 0
        pub.publish(new_vel)

    else:
        pub.publish(velocity)


class NavTest():
    def initCmvVel(self):
        rospy.init_node('nav_test', anonymous=True)
        rospy.on_shutdown(self.shutdown)   

        # Publisher to manually control the robot (e.g. to stop it)
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

        # Subscribe to the move_base action server
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)

        rospy.loginfo("Waiting for move_base action server...")

        # Wait 60 seconds for the action server to become available
        self.move_base.wait_for_server(rospy.Duration(60))

        rospy.loginfo("Connected to move base server")

        # A variable to hold the initial pose of the robot to be set by
        # the user in RViz
        initial_pose = PoseWithCovarianceStamped()
        rospy.loginfo("*** Click the 2D Pose Estimate button in RViz to set the robot's initial pose...")
        rospy.wait_for_message('initialpose', PoseWithCovarianceStamped)
        self.last_location = Pose()
        rospy.Subscriber('initialpose', PoseWithCovarianceStamped, self.update_initial_pose)

        while initial_pose.header.stamp == "":
            rospy.sleep(1)
            rospy.loginfo("Cmd vel init done")
      
if __name__=="__main__":

    settings = termios.tcgetattr(sys.stdin)
    
    rospy.init_node('minilab_stopper')
    rospy.loginfo('node initiated')

    vel_sub  = rospy.Subscriber('/cmd_vel', Twist, velocity_cb)
    scan_sub = rospy.Subscriber('/scan', LaserScan, scan_cb)    
    print("LaserScan()")
    scans = LaserScan()
    print(scans)
    
    # rospy.loginfo(vel_sub)
    #laser_sub = message_filters.Subscriber('/scan', LaserScan)
    print("debug1")
    #ts = message_filters.TimeSynchronizer([vel_sub, laser_sub], 10)
    print("debug2")
    #ts.registerCallback(callback)
    rospy.loginfo('spin')
    rospy.spin()
    rospy.loginfo('turning off')