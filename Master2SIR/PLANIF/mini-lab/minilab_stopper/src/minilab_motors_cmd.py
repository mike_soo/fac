#!/usr/bin/env python

# before launching execute in robot
# roslaunch minilab_driver minilab_driver.launch 
# inspired in the ~/catkin_wd/src/minilab_teleop/src/keyboard_teleop_ZQSD.py script
import roslib
# roslib.load_manifest('minilab_teleop')
import rospy

from geometry_msgs.msg import Twist


class VelCmder:
    def __init__(self , ang_speed , vel_speed publisher):
        self.ang_speed = ang_speed
        self.vel_speed = vel_speed
        self.twist     = Twist()
        self.publisher = publisher

    def turn(self , orientation ):
        self.twist.linear.x = 0
        self.twist.linear.y = 0
        self.twist.linear.z = 0
        
        if(orientation == 'l'):
            self.twist.angular.x = 0
            self.twist.angular.y = 0
            self.twist.angular.z = self.ang_speed             
        
        elif(orientation == 'r'):
            self.twist.angular.x = 0
            self.twist.angular.y = 0
            self.twist.angular.z = -self.ang_speed             
        self.command()
    def command(self):
        self.publisher.publish(self.twist)


if __name__ == '__main__':
    rospy.init_node('minilab_cmd_velocity')
    pub = rospy.Publisher('cmd_vel', Twist)
    velCmder= VelCmder(0.5 , 0.3 , pub)
    
    while 1 : 
        velCmder.turn('r')


