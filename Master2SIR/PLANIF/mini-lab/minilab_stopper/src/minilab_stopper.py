#! /usr/bin/env python
import sys, select, termios, tty
import roslib
import rospy

from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)

class Obstacle:
    def __init__(self, pos, dist):
        self.pos = pos
	self.dist = dist

    def __init__(self, obj):
	

def angle(laserpos):
    return float(laserpos) / 512 * 180 - 90


def side(angle):
    if angle < -20:
        return "left"
    elif angle > 20:
        return "right"
    else:
        return "front"


class ObstacleDetector:
    def __init__():
	obstacle_list = []
        obstacle_pos = []


    def get_closest_pos(laserdata):
	dist_min = min(laserdata.ranges)
	pos_min = laserdata.ranges.index(dist_min)
	return side(angle(pos_min))


    def find_obstacles(matrix, range)
        list_obstacles = []
        obstacle = [matrix[0]]
        for el, _ in enumerate(matrix, 1):
            last_obs = obstacle[-1]
            if el.dist - last_obs.dist < range and abs(el.pos - last_obs.pos) < 2:
                obstacle.append(el)
            else:
                closest = min(obstacle, key=lamba x -> x.dist)
                list_obstacles.append(Obstacle(closest))
                obstacle = [el]

        return list_obstacles


    def get_obstacles(laserdata, range_min, range_max, delta):
        in_range = [Obstacle(angle(pos), dist) for dist, pos in enumerate(laserdata.ranges) if (dist < range_max and dist > range_min)]
        return find_obstacles(in_range, delta)



class Stopper:
    def __init__(self):
        self.stop = False

    def scan_callback(self, laserdata):
        # Check if there is an obstacle in front
        dist_min = min(laserdata.ranges)
        pos_min = laserdata.ranges.index(dist_min)
        print(angle(pos_min))
        if min(laserdata.ranges) < 0.2:
            self.stop = True
            print("Obstacle detected !")
        else:
            self.stop = False
            print("No obstacle")

    def velocity_callback(self, velocity):
        # Send velocity
        if self.stop:
            # New velocity set to 0
            pub.publish(Twist())
        else:
            pub.publish(velocity)


if __name__=="__main__":

    settings = termios.tcgetattr(sys.stdin)
    stopper = Stopper()

    rospy.init_node('minilab_stopper')

    vel_sub = rospy.Subscriber('cmd_vel_input', Twist, stopper.velocity_callback)
    laser_sub = rospy.Subscriber('scan', LaserScan, stopper.scan_callback)
    print("main")
    rospy.spin()
