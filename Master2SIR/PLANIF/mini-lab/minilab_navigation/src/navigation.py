class NavTest():
  def initCmvVel(self):
      rospy.init_node('nav_test', anonymous=True)
      rospy.on_shutdown(self.shutdown)   

      # Publisher to manually control the robot (e.g. to stop it)
      self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

      # Subscribe to the move_base action server
      self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)

      rospy.loginfo("Waiting for move_base action server...")

      # Wait 60 seconds for the action server to become available
      self.move_base.wait_for_server(rospy.Duration(60))

      rospy.loginfo("Connected to move base server")

      # A variable to hold the initial pose of the robot to be set by
      # the user in RViz
      initial_pose = PoseWithCovarianceStamped()
      rospy.loginfo("*** Click the 2D Pose Estimate button in RViz to set the robot's initial pose...")
          rospy.wait_for_message('initialpose', PoseWithCovarianceStamped)
      self.last_location = Pose()
			rospy.Subscriber('initialpose', PoseWithCovarianceStamped, self.update_initial_pose)
      
			while initial_pose.header.stamp == "":
            rospy.sleep(1)
			rospy.loginfo("Cmd vel init done")