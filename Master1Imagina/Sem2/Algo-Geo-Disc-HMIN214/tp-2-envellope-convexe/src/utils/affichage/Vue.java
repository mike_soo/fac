package utils.affichage;
import javax.swing.*;

import utils.couleurs.Couleur;
import utils.fileIo.ReadWritePoint;

import utils.vecteur.PointVisible;
import utils.vecteur.Triangle;

import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Vue extends JPanel implements MouseWheelListener, MouseListener, ActionListener{
	Color bgColor = Couleur.bg; // la couleur de fond de la fen�tre
	Color fgColor = Couleur.fg; // la couleur des lignes
	int width;
	ArrayList<PointVisible> points = new ArrayList<PointVisible>();
	static int premAffiche = 1;
	// n : le nombre de lignes
	// width, height : largeur, hauteur de la fen�tre
	public Vue(int n, int width) {
		super();
		initView(width);
		initPoints(n, (width - 100)/2 , width/2, width/2);
		addMouseListener(this);
		addMouseWheelListener(this);
	}
	
	public Vue (int width , ArrayList<PointVisible> points)
	{
		super();
		initView(width);
		initPoints(points);
//		addMouseListener(this);
//		addMouseWheelListener(this);
	}
	// initialisation random
	// NB: l'initialisation dans disque est � faire (exercice 1)
	public void initView(int width)
	{
		JButton b1 = new JButton("Enregistrer");
		b1.setActionCommand("b1");
		b1.addActionListener(this);
		JButton b2 = new JButton("Charger");
		b2.addActionListener(this);
		Box b = Box.createHorizontalBox();
		b.add(b1);b.add(Box.createHorizontalStrut(10));b.add(b2);
		add(b);
		setBackground(bgColor);
		this.width = width;
		setPreferredSize(new Dimension(width, width));
	}
	public void initPoints(int n, int r, int x, int y){
			int xp,yp;
			Random random = new Random();
			points = new ArrayList<PointVisible>();
			float randomFloat=0;
			for (int i = 0; i <n; i++){
				randomFloat= random.nextFloat() ;
				xp =x + (int) (random(0, r)  * Math.cos(randomFloat * 2 * Math.PI));
				yp =y + (int) (random(0, r)  * Math.sin(randomFloat * 2 * Math.PI));
				points.add(new PointVisible(xp,  yp));
				points.get(i).setLabel("Point "+i);
			}
	}
	
	public void initPoints(ArrayList<PointVisible> points)
	{
		this.points = points;
	}
	
	
	public ArrayList <PointVisible> envellopeConvexe()
	{	
		ArrayList <PointVisible> envConvexe = new ArrayList <PointVisible>();
		PointVisible pmin = points.get(0); 
		for (int i = 0 ; i< points.size() ; i ++)
		{
			if (points.get(i).getX()< pmin.getX() && points.get(i).getY() < pmin.getY())
			{
				pmin = points.get(i);
			}
		}
		
		
		PointVisible pcour = pmin;
		PointVisible psuiv = null;
		try {
			psuiv = getNext(pcour, points);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("Je beug dans 1er calcul de getNext");
			e1.printStackTrace();
		}
		while( !psuiv.equalsP (pmin))
		{
			System.out.println("LOOP");
			try {
				psuiv = getNext(pcour,points);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Je beug dans calcul de getNext");
				e.printStackTrace();
				return null;
			}
			envConvexe.add(psuiv);
			pcour = psuiv;
			psuiv.setSize(5,5);
			System.out.println("psuiv ="+psuiv.getLabel() + " pmin =" +pmin.getLabel() );
			if(psuiv.equalsP (pmin))
			{
				System.out.println("egaux!");
			}
		}
		
		return envConvexe;
		
	}
	public PointVisible getNext(PointVisible pointCourant , ArrayList <PointVisible> V) throws Exception
	{
		PointVisible pointSuivant = V.get(0);
		
		//Mettra à true les points deja considéré comme ceux les plus à gauche
		
		
		
		int i = 1;
		while (pointCourant.equalsP(pointSuivant))
		{
			pointSuivant = V.get(i);
			i++;
		}
		i=0;
		while (i < V.size())
		{
			System.out.print(" Pcour = " + pointCourant);
			System.out.print(" Psuiv = " + pointSuivant);
			System.out.println(" V.get(i)= "+ V.get(i));
			if ( !(pointCourant.equalsP(V.get(i))) && !(V.get(i).equalsP( pointSuivant)))
			{
				if (pointCourant.tourGauche(pointSuivant, V.get(i) ))
				{
					System.out.println("G");
					i++;
				}
				else if(pointCourant.tourDroit(pointSuivant, V.get(i)))
				{
					System.out.println("D");
					pointSuivant = V.get(i);
					i++;
				}
				else
				{
					System.out.println("getNext problem");
					throw new Exception();
				}
			}
			else
			{
				i++;
			}
			
		}
		return pointSuivant;
	}
	
	// m�thode utilitaire 
	// retourne un entier compris entre xmin et xmax
	int random(int xmin,int xmax){
		double dr = Math.random() * (double) (xmax - xmin) + (double) xmin;
		return (int) dr;
	}
	
	
	public ArrayList<Triangle> triangulation( Graphics g) throws InterruptedException
	{
		Graphics2D g2d = (Graphics2D) g;
		ArrayList <PointVisible> envConvexe = new ArrayList <PointVisible>();
		ArrayList <Triangle> T = new ArrayList <Triangle>();
		//TODO tri x _ y de tous les points
		
		for(int i = 0 ;i < 3 ; i++ )
		{
			
		}
		envConvexe.add(this.points.get(2));
		envConvexe.add(this.points.get(1));
		envConvexe.add(this.points.get(0));
		envConvexe.add(this.points.get(2));
		
		T.add(new Triangle(this.points.get(0), this.points.get(1) , this.points.get(2) ,"T00"));
		for(int i = 3 ; i< this.points.size(); i++)
		{
			PointVisible p = this.points.get(i);
			int m = envConvexe.size() -  1;
			int k = 0;
			while(k < m  &&  p.tourDroit( envConvexe.get(k), envConvexe.get(k+1))) 
			{
				T.add(new Triangle(p,envConvexe.get(k),envConvexe.get(k+1) , "TD"+i));
				k++;
			}
			
			int h = k ;
			k = m;
			while(k > h && p.tourGauche(envConvexe.get(k), envConvexe.get(k-1)))
			{
				T.add(new Triangle(p,envConvexe.get(k),envConvexe.get(k-1) , "TG"+i) );
				k--;
				
			}
			int b= k;
			//mise à jour de l'EC
			
			
			ArrayList<PointVisible> envConvexeNouv = new ArrayList<PointVisible>();
			envConvexeNouv.add(p);
			for(int j = h ; j < b+1 ; j++)
			{
				envConvexeNouv.add(envConvexe.get(j));
				
			}
			envConvexeNouv.add(p);
			envConvexe = envConvexeNouv;
			
			
//			for (int ii = 0 ; ii < envConvexe.size() -1 ; ii ++)
//			{
//				g2d.drawLine((int)envConvexe.get(ii).getX(), (int)envConvexe.get(ii).getY(), 
//						(int)envConvexe.get(ii+1).getX(), (int)envConvexe.get(ii+1).getY());
//			}
//			g2d.drawLine((int)envConvexe.get(envConvexe.size()-1).getX(),
//						(int)envConvexe.get(envConvexe.size()-1).getY(),
//						(int)envConvexe.get(0).getX(),
//						(int)envConvexe.get(0).getY());
//			
			Thread.sleep(1);
		}
		for(int ii = 0 ; ii < T.size(); ii++)
		{
			T.get(ii).dessine(g2d);
		}
		return T;
		
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaintMode(); 
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,	RenderingHints.VALUE_ANTIALIAS_ON);	

		g2d.setColor(fgColor);
		
		for (PointVisible p: points) {
			p.dessine(g2d);
		}
//		
		//Envellope convexe simpleaddMouseListener(this);
//		
//		========== TP2 =========
//		if(premAffiche == 1)
//			{
//				premAffiche = 0;
//				repaint(); 
//				
//			}
//			
//		System.out.println("envellopeConvexe start");
//		ArrayList <PointVisible> envConvexe = envellopeConvexe();
//		
//		for (int i = 0 ; i < envConvexe.size() -1 ; i ++)
//		{
//			g2d.drawLine((int)envConvexe.get(i).getX(), (int)envConvexe.get(i).getY(), 
//					(int)envConvexe.get(i+1).getX(), (int)envConvexe.get(i+1).getY());
//		}
//		g2d.drawLine((int)envConvexe.get(envConvexe.size()-1).getX(),
//					(int)envConvexe.get(envConvexe.size()-1).getY(),
//					(int)envConvexe.get(0).getX(),
//					(int)envConvexe.get(0).getY());


//		========== TP3 =========
		System.out.println("Triangulation");

		try {
			this.triangulation(g2d);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Triangulation done");
//		========================
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int n = points.size();
		if (e.getButton() == MouseEvent.BUTTON1){
			initPoints(n, (width-100)/2, width/2, width/2);
			repaint();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String testFile = "tmp.txt";
		ReadWritePoint rw = new ReadWritePoint(testFile);
	
		if(e.getActionCommand().equals("b1")){
			for (PointVisible s: points){
				rw.add(s.x+";" + s.y+";"+s.getLabel());
			}
			try {
				rw.write();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}else{
			initFromFile(testFile);
			
		}
	}
	public void initFromFile(String f){
		ReadWritePoint rw = new ReadWritePoint(f);
		try {
			points = rw.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
}

}
	

