package utils.vecteur;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Comparator;


import utils.couleurs.Couleur;


public class PointVisible extends Rectangle implements Comparator<PointVisible>{
	public static int midWidth = 5;
	Color color = Couleur.nw;
	Role role ;
	private String label;
	Vecteur segment;
	
	public PointVisible minus(PointVisible p)
	{

		super.setLocation((int) (this.getX() - p.getX()) ,(int) (this.getY() - p.getY()));
		return this;
	}
	
	public double det(PointVisible B ,PointVisible C)
	{
	
		PointVisible cB = (PointVisible) B.clone();
		PointVisible cC = (PointVisible) C.clone();
		
		PointVisible vAB = cB.minus(this);
		
		PointVisible vAC = cC.minus(this);
		
		
		return (vAB.getX() * vAC.getY()) - (vAB.getY()*vAC.getX()); 
		
	}
	
	public int tour(PointVisible B , PointVisible C)
	{
		if (this.det(B,C) > 0 )
		{
			return 1;
		}
		
		else if (this.det(B,C) ==0)
		{
			return 0;
		}
		//if (this.det(B,C) < 0
		{
			return -1;
		}
		
	}
	
	public Boolean tourDroit(PointVisible B , PointVisible C)
	{
		return tour(B,C) == 1; 
	}
	public Boolean tourGauche(PointVisible B, PointVisible C)
	{
		return tour(B,C) == -1;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}

	public PointVisible(int x, int y, Role r) {
		super(x,y, 2 * PointVisible.midWidth,2 * PointVisible.midWidth);
		this.role = r;
		switch(role){
		case begin: 
			color = Couleur.beginPoint;
			break;
		case intersect: // blanc
			color = Couleur.intersection;
			break;
		case end: //rouge
			color = Couleur.endPoint;
			break;
		default:
			color = Couleur.fg;
	}
	}

	public PointVisible(int xp, int yp) {
		this(xp, yp, Role.undefined);
	}

	public void dessine(Graphics2D g2d) {
		g2d.setColor(color);
		g2d.fill(this);	
		drawLabel(g2d);
	}

	@Override 
	public int compare(PointVisible p1, PointVisible p2) {
		return p2.x - p1.x;
	}	
	
	public Boolean equalsP(PointVisible p)
	{
		return (int)this.getX() == (int) p.getX() && (int)this.getY() == (int)p.getY() ;
 	}
	public void print() {
		System.out.println("x = " + x + " y = " + y+" w = " + width + " h = " + height + " role = "+role);
	}

	public void drawLabel(Graphics2D g) {
		FontMetrics fm = g.getFontMetrics();
		int centeredText = (int) (x - fm.stringWidth(getLabel())/2 + fm.stringWidth("_"));
		g.drawString(getLabel(), centeredText, (int) (y-2));
	}

	public String getLabel() {
		return label;
		
	}
	
	public String toString()
	{
		return getLabel() +" ("+getX() + ","+getY() +")";
	}

}

