package main;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import utils.affichage.Vue;
import utils.fileIo.ReadWritePoint;
import utils.vecteur.PointVisible;

public class MainTriangulation {
	
	public static void main(String s[]) throws IOException {
		JFrame frame = new JFrame("Calcul d'enveloppe convexe");
		ReadWritePoint rwp = new ReadWritePoint("triangulation-1.csv");
		ArrayList<PointVisible> points = rwp.read();
		Vue v = new Vue(900,points);
		frame.add(v);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	} 
}
