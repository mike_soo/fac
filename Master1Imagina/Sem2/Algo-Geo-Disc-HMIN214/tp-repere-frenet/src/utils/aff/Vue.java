package utils.aff;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.sql.Time;
import java.util.ArrayList;

import javax.swing.JPanel;

import utils.go.Frenet;
import utils.go.PointVisible;
import utils.go.Repere;
import utils.go.Vecteur;
import utils.io.ReadWritePoint;


public class Vue extends JPanel implements MouseListener, MouseMotionListener{
	Color bgColor;
	Color fgColor; 
	int width, height;
	private ArrayList<PointVisible> points = new ArrayList<PointVisible>();
	private ArrayList<Vecteur> aretes = new ArrayList<Vecteur>();
	private ArrayList<Repere> reperes = new ArrayList<Repere>();
	Point initialLocation, previousLocation, newLocation;
	Rectangle rectangleElastique;
	
	private Vecteur droiteTrajectoireSinus , droiteTrajectoireCosinus , droiteTrajectoirePuis2 ;
	private static long t=-1;
	public static void incTime()
	{
		t++;
	}
	public static long getTime()
	{
		return t;
	}
	
	
	public Vue(int width, int height, String fileName, boolean modelCoordinates) {
		super();
		Couleur.forPrinter(true);
		this.bgColor = Couleur.bg;  
		this.fgColor = Couleur.fg; 
		this.width = width;
		this.height = height;	
		this.setBackground(Couleur.bg);
		this.setPreferredSize(new Dimension(width, width));
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		initFromLog(fileName, modelCoordinates); 
		if(!modelCoordinates)export("trio-hypo-2.csv");
		
		droiteTrajectoireSinus = new Vecteur(points.get(1),points.get(2));
		droiteTrajectoireCosinus = new Vecteur(points.get(5), points.get(6));
		droiteTrajectoirePuis2 = new Vecteur (points.get(8) , points.get(9));
	}

	
	
	private void copyModelToViewportCoords() {
		for(PointVisible p: points) {
			p.copyModelToViewportCoords();
		}
	}
	
	private void initFromLog(String fileName, boolean modelCoordinates) {
		ReadWritePoint rw = new ReadWritePoint(fileName);
		points = rw.read();
		aretes = new ArrayList<Vecteur>();
		int n = points.size();
		for (int i = 0 ; i < n; i++) {
			aretes.add(new Vecteur(points.get(i), points.get((i+1)%n)));
		}

	}
	
	public void export(String logFile) {
		ReadWritePoint rw = new ReadWritePoint(logFile);
		for (PointVisible p: points){
			rw.add((int)p.getMC().x+";"+(int)p.getMC().y+";"+p.toString());
		}
		rw.write();
	}
	
	public void setPoints(ArrayList<PointVisible> points) {
		this.points = points;
	}	
			
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaintMode(); 
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,	RenderingHints.VALUE_ANTIALIAS_ON);	
		g2d.setColor(fgColor);
		if (rectangleElastique != null) g2d.draw(rectangleElastique);
		
//		for (Vecteur v: aretes) {
//			v.dessine(g2d);
//		}		
		
		
		
		
		incTime();
		reperes.add(Frenet.genRepere("sin", getTime(), points.get(0), Math.PI / 64.0));
		reperes.add(Frenet.genRepere("cos", getTime(), points.get(4), Math.PI / 64.0));
		reperes.add(Frenet.genRepere("puis2", getTime(), points.get(8), 0));
		
		Courbes.drawF(g2d, droiteTrajectoirePuis2, 0, 1000, "puis2");
		Courbes.drawF(g2d, droiteTrajectoireSinus, Math.PI / 64.0 ,1000 ,"sin");
		Courbes.drawF(g2d, droiteTrajectoireCosinus , Math.PI / 64.0 , 1000 , "cos");
		
		for(Repere repere : reperes)
		{
			for(Vecteur vecteur : repere.getVecs())
			{
				vecteur.dessine(g2d);
				System.out.println(vecteur);
			}
		}
		reperes.clear();
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
//		initialLocation = new Point(e.getX(), e.getY());
//		rectangleElastique = new Rectangle (e.getX(), e.getY(), 0, 0);
//		previousLocation = new Point(e.getX(), e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
//		updateElasticRectangle(e.getX(), e.getY());
//		previousLocation = null;
//		initialLocation = null;
	}

	private void updateElasticRectangle(int newX, int newY) {
		int w = newX - initialLocation.x;
		int h = newY - initialLocation.y;
		previousLocation.x = newX;
		previousLocation.y = newY;		
		
		rectangleElastique.width = (w >=0)? w: -w;
		rectangleElastique.height = (h >=0)? h: -h;
		
		if (h < 0) {
			rectangleElastique.y = initialLocation.y +h;
		}

		if (w < 0) {
			rectangleElastique.x = initialLocation.x +w;
		}
		
		repaint();
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		if (previousLocation != null) {
			updateElasticRectangle(e.getX(), e.getY());
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
System.out.println("mouseClicked");
		
		repaint();
	}
}
	

