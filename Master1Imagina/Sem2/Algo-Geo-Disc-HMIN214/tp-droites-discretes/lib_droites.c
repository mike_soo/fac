#include "lib_droites.h"
#include "image_ppm.h"

void algo_bressen_ham(int x0 , int y0 , int x1 , int y1 , OCTET **img_out , int nb_lig , int nb_col , OCTET val_gris)
{
	int dx = abs(x1 - x0);
	int dy = abs(y1 - y0);

	int x = x0;
	int y = y0;
	
	int incrHor  = 2 *  dy;
	int incrDiag = 2 * (dx - dy);

	int e = (2*dy) - dx;


	for (x = x0 ; x<= x1 ;  x ++)
	{
		img_out[y][x] = val_gris ;
		if( e>=0)
		{
			--y;
			e-= incrDiag;
		} 
		else
		{
			e+= incrHor;
		}
	}
}


void algo_reveilles(int x0 , int y0 , int x1 , int y1 , OCTET **img_out , int nb_lig , int nb_col , OCTET val_gris)
{
	int vx = x1 - x0;
	int vy = abs(y1 - y0);
	int mu = (vy * x0) - (vx * y0);
	int r  = (vy * x0) - (vx * y0) - mu;
	int x  = x0 ; 
	int y  = y0;

	while(x < x1)
	{
		++x;
		r+= vy;
		if( r < 0 || r >= vx)
		{
			--y;
			r-= vx;
		}
		img_out[y][x] = val_gris;
	}
}