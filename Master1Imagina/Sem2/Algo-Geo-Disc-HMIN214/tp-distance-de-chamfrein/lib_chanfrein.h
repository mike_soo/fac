
#ifndef LIB_CHANFREIN_H
#define LIB_CHANFREIN_H
#include "image_ppm.h"
#include <stdio.h>

struct Mask{
	int** tabMask;
	unsigned int nb_col_mask_ch;
	unsigned int nb_lig_mask_ch;
    

};

void draw_boules_chanfrein(unsigned int rayon , struct Mask * m , OCTET** img , int nb_lig_img , int nb_col_img);
void draw_boules_chanfrein_aux(unsigned int rayon , struct Mask * m , OCTET** img , int nb_lig_img ,int nb_col_img , int i , int j );
#endif