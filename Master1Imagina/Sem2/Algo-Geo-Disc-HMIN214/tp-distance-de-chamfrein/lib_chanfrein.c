#include "lib_chanfrein.h"
void draw_boules_chanfrein(unsigned int rayon , struct Mask * m , OCTET** img , int nb_lig_img ,int nb_col_img)
{
	for (int i = 0 ; i < nb_lig_img  ; i++ )
	{
		for(int j = 0 ; j < nb_col_img  ; j++)
		{
			img[i][j] = 255;
		}
	}
	
	img[nb_lig_img/2][nb_col_img/2] = 0 ;

	// for(unsigned int ii = nb_lig_img /2   ; ii < rayon + (nb_lig_img/2) ; ii++ )
	// {
	// 	for(unsigned int jj = (nb_col_img /2) ; jj < rayon + (nb_col_img/2)   ; jj++)
	// 	{
	// 		printf("%4i" , img[ii][jj]);
	// 	}
	// 	printf("\n");

	// }
	// printf("verif test = %i\n" , img[nb_lig_img/2][nb_col_img/2] );
	printf("start\n");
	int cpt_lig = 0 ;
	int orient=0;

	for(unsigned int i = nb_lig_img /2   ; i < rayon + (nb_lig_img/2) ; i++  )
	{

		for(unsigned int j = (nb_col_img /2) ; j < rayon + (nb_col_img/2) - cpt_lig  ; j++)
		{
			draw_boules_chanfrein_aux( rayon , m , img , nb_lig_img , nb_col_img , i , j );
		}
		cpt_lig ++ ;
	}
	cpt_lig = 0 ;

	for(unsigned int i = nb_lig_img /2   ; i > nb_lig_img/2 - rayon ;  i-- )
	{

		for(unsigned int j = (nb_col_img /2) ; j < rayon + (nb_col_img/2) - cpt_lig  ; j++)
		{
			draw_boules_chanfrein_aux( rayon , m , img , nb_lig_img , nb_col_img , i , j );
		}
		cpt_lig ++ ;
	}
	cpt_lig = 0 ;

	for(unsigned int i = nb_lig_img /2   ; i < rayon + (nb_lig_img/2) ; i++  )
	{

		for(unsigned int j = (nb_col_img /2) ; j > (nb_col_img/2)- rayon + cpt_lig  ; j--)
		{
			draw_boules_chanfrein_aux( rayon , m , img , nb_lig_img , nb_col_img , i , j );
		}
		cpt_lig ++ ;
	}
	cpt_lig = 0 ;
	
	for(unsigned int i = nb_lig_img /2   ; i > nb_lig_img/2 - rayon ;  i-- )
	{

		for(unsigned int j = (nb_col_img /2) ; j > (nb_col_img/2)- rayon + cpt_lig  ; j--)
		{
			draw_boules_chanfrein_aux( rayon , m , img , nb_lig_img , nb_col_img , i , j );
		}
		cpt_lig ++ ;
	}
	cpt_lig = 0 ;
		

}


void draw_boules_chanfrein_aux(unsigned int rayon , struct Mask * m , OCTET** img , int nb_lig_img ,int nb_col_img , int i , int j )
{
	printf("i= %i j=%i\n", i , j );
	int mask_dist_min = 9999;
	unsigned int i_mask_img = i - (m->nb_lig_mask_ch /2);
	for(unsigned int i_mask =  0; i_mask < m->nb_lig_mask_ch ; i_mask++)
	{

		unsigned int j_mask_img = j - (m->nb_col_mask_ch /2);
		for(unsigned int j_mask = 0 ; j_mask < m->nb_col_mask_ch; j_mask++)
		{

			printf(" i_mask=%i j_mask=%i i_mask_img=%i j_mask_img=%i\n ", i_mask , j_mask , i_mask_img , j_mask_img);
			
			if (m->tabMask[i_mask][j_mask] 
				+ img[i_mask_img][j_mask_img] < mask_dist_min)
			{
				mask_dist_min = m->tabMask[i_mask][j_mask] + img[i_mask_img][j_mask_img] ;
			}
			j_mask_img++;
		}
		i_mask_img++;
	}
	// printf(" mask_dist_min %i \n" , mask_dist_min);
	if(mask_dist_min < rayon)
		img[i][j] = mask_dist_min;

	
	// printf("MATRIX\n");
	// for(unsigned int ii = nb_lig_img /2   ; ii < rayon + (nb_lig_img/2) ; ii-- )
	// {
	// 	for(unsigned int jj = (nb_col_img /2) ; jj < rayon + (nb_col_img/2)   ; jj++)
	// 	{
	// 		printf("%4i" , img[ii][jj]);	
	// 	}
	// 	printf("\n");

	// }
}