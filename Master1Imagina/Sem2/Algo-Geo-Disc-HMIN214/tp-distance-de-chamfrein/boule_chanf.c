// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#include "lib.h"
#include "lib_chanfrein.h"
const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  char cNomImgEcrite[250];
  int nb_col_img = 700, nb_lig_img=700, nTaille;  
  if (argc != 1)
  {
    printf("Non arg\n");
    exit (-1) ;
  }
  

  OCTET * ImgOut;
  OCTET ** ImgOut2d;
  nTaille = nb_col_img * nb_lig_img;
  
  allocation_tableau(ImgOut, OCTET, nTaille);

  ImgOut2d = allocation_tableau2D(nb_lig_img, nb_col_img);
  
  

  // int tabMask[nb_lig_mask_ch][nb_col_mask_ch] = { {4,3,4},
  //                                         {3,0,3},
  //                                         {4,3,4}
  //                                         };
  
  struct Mask m ;
  m.tabMask = (int**)malloc(3 * sizeof(int*));
  for(int i = 0 ; i < 3 ; i++)
  {
    m.tabMask[i]= (int*)malloc(3 * sizeof(int));
  }
  m.tabMask[0][0] = 4; m.tabMask[0][1] = 3 ; m.tabMask[0][2] = 4;
  m.tabMask[1][0] = 3; m.tabMask[1][1] = 0 ; m.tabMask[1][2] = 3;
  m.tabMask[2][0] = 4; m.tabMask[2][1] = 3 ; m.tabMask[2][2] = 4;
  m.nb_lig_mask_ch = 3;
  m.nb_col_mask_ch = 3;
  draw_boules_chanfrein(255 ,  &m , ImgOut2d , nb_lig_img , nb_col_img);

  
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  
  sscanf ("boule_mask<3,4>.pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);


  m.tabMask[0][0] = 1; m.tabMask[0][1] = 1; m.tabMask[0][2] = 1;
  m.tabMask[1][0] = 1; m.tabMask[1][1] = 0; m.tabMask[1][2] = 1;
  m.tabMask[2][0] = 1; m.tabMask[2][1] = 1; m.tabMask[2][2] = 1;
  draw_boules_chanfrein(255 ,  &m , ImgOut2d , nb_lig_img , nb_col_img);
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  sscanf ("boule_mask<1,1>.pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);
  

  m.tabMask[0][0] = 2; m.tabMask[0][1] = 1; m.tabMask[0][2] = 2;
  m.tabMask[1][0] = 1; m.tabMask[1][1] = 0; m.tabMask[1][2] = 1;
  m.tabMask[2][0] = 2; m.tabMask[2][1] = 1; m.tabMask[2][2] = 2;
  draw_boules_chanfrein(255 ,  &m , ImgOut2d , nb_lig_img , nb_col_img);
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  sscanf ("boule_mask<1>.pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);
  

  struct Mask m2;
  m2.tabMask = (int**)malloc(5 * sizeof(int*));
  for(int i = 0 ; i < 5 ; i++)
  {
    m2.tabMask[i]= (int*)malloc(5 * sizeof(int));
  }
  m2.tabMask[0][0] = 14; m2.tabMask[0][1] = 11; m2.tabMask[0][2] = 10; m2.tabMask[0][3] = 11; m2.tabMask[0][4] = 14;
  m2.tabMask[1][0] = 11; m2.tabMask[1][1] = 7;  m2.tabMask[1][2] = 5;  m2.tabMask[1][3] = 7; m2.tabMask[1][4]  = 11;
  m2.tabMask[2][0] = 10; m2.tabMask[2][1] = 5;  m2.tabMask[2][2] = 0;  m2.tabMask[2][3] = 5; m2.tabMask[2][4]  = 10;
  m2.tabMask[3][0] = 11; m2.tabMask[3][1] = 7;  m2.tabMask[3][2] = 5;  m2.tabMask[3][3] = 7; m2.tabMask[3][4]  = 11;
  m2.tabMask[4][0] = 14; m2.tabMask[4][1] = 11; m2.tabMask[4][2] = 10; m2.tabMask[4][3] = 11; m2.tabMask[4][4] = 14;

  m2.nb_col_mask_ch = 5;
  m2.nb_lig_mask_ch = 5;

  draw_boules_chanfrein(255 ,  &m2 , ImgOut2d , nb_lig_img , nb_col_img);
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  sscanf ("boule_mask<5,7,11>.pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);
  

  m2.tabMask[0][0] = 14; m2.tabMask[0][1] = 9; m2.tabMask[0][2] = 10; m2.tabMask[0][3] = 9; m2.tabMask[0][4] = 14;
  m2.tabMask[1][0] = 9; m2.tabMask[1][1] = 7;  m2.tabMask[1][2] = 5;  m2.tabMask[1][3] = 7; m2.tabMask[1][4]  = 9;
  m2.tabMask[2][0] = 10; m2.tabMask[2][1] = 5;  m2.tabMask[2][2] = 0;  m2.tabMask[2][3] = 5; m2.tabMask[2][4]  = 10;
  m2.tabMask[3][0] = 9; m2.tabMask[3][1] = 7;  m2.tabMask[3][2] = 5;  m2.tabMask[3][3] = 7; m2.tabMask[3][4]  = 9;
  m2.tabMask[4][0] = 14; m2.tabMask[4][1] = 9; m2.tabMask[4][2] = 10; m2.tabMask[4][3] = 9; m2.tabMask[4][4] = 14;

  m2.nb_col_mask_ch = 5;
  m2.nb_lig_mask_ch = 5;

  draw_boules_chanfrein(255 ,  &m2 , ImgOut2d , nb_lig_img , nb_col_img);
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  sscanf ("boule_mask<5,7,9>.pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);
    

  m.tabMask[0][0] = 1; m.tabMask[0][1] = 3 ; m.tabMask[0][2] = 1;
  m.tabMask[1][0] = 3; m.tabMask[1][1] = 0 ; m.tabMask[1][2] = 3;
  m.tabMask[2][0] = 1; m.tabMask[2][1] = 3 ; m.tabMask[2][2] = 1;
  m.nb_lig_mask_ch = 3;
  m.nb_col_mask_ch = 3;
  draw_boules_chanfrein(255 ,  &m , ImgOut2d , nb_lig_img , nb_col_img);

  
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  
  sscanf ("boule_mask<3,1>.pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);





  struct Mask m3;
  m3.tabMask = (int**)malloc(7 * sizeof(int*));
  for(int i = 0 ; i < 7 ; i++)
  {
    m3.tabMask[i]= (int*)malloc(7 * sizeof(int));
  }
  m3.tabMask[0][0] = 21; m3.tabMask[0][1] = 19; m3.tabMask[0][2] = 16; m3.tabMask[0][3] = 15; m3.tabMask[0][4] = 16; m3.tabMask[0][5] = 19; m3.tabMask[0][6] = 21;
  m3.tabMask[1][0] = 19; m3.tabMask[1][1] = 14; m3.tabMask[1][2] = 12; m3.tabMask[1][3] = 10; m3.tabMask[1][4] = 12; m3.tabMask[1][5] = 14; m3.tabMask[1][6] = 19;
  m3.tabMask[2][0] = 16; m3.tabMask[2][1] = 12; m3.tabMask[2][2] = 7;  m3.tabMask[2][3] = 5;  m3.tabMask[2][4] = 7;  m3.tabMask[2][5] = 12; m3.tabMask[2][6] = 16;
  m3.tabMask[3][0] = 15; m3.tabMask[3][1] = 10; m3.tabMask[3][2] = 5;  m3.tabMask[3][3] = 0;  m3.tabMask[3][4] = 5;  m3.tabMask[3][5] = 10; m3.tabMask[3][6] = 15;
  m3.tabMask[4][0] = 16; m3.tabMask[4][1] = 12; m3.tabMask[4][2] = 7;  m3.tabMask[4][3] = 5;  m3.tabMask[4][4] = 7;  m3.tabMask[4][5] = 12; m3.tabMask[4][6] = 16;
  m3.tabMask[5][0] = 19; m3.tabMask[5][1] = 14; m3.tabMask[5][2] = 12; m3.tabMask[5][3] = 10; m3.tabMask[5][4] = 12; m3.tabMask[5][5] = 14; m3.tabMask[5][6] = 19;
  m3.tabMask[6][0] = 21; m3.tabMask[6][1] = 19; m3.tabMask[6][2] = 16; m3.tabMask[6][3] = 15; m3.tabMask[6][4] = 16; m3.tabMask[6][5] = 19; m3.tabMask[6][6] = 21;


  m3.nb_col_mask_ch = 7;
  m3.nb_lig_mask_ch = 7;

  draw_boules_chanfrein(256 ,  &m3 , ImgOut2d , nb_lig_img , nb_col_img);

  
  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  
  sscanf ("boule_mask(5,7,16).pgm\0", "%s", cNomImgEcrite);
  ecrire_image_pgm( cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);

  return 1;
}
