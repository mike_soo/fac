#include "Triangle.hpp"
 
#include <iostream>
#include <vector>
using namespace std;

extern Base base0;
Triangle::Triangle(Vector *p1 , Vector *p2 , Vector *p3)
{
	points[0] = p1;
	points[1] = p2;
	points[2] = p3;
}

void Triangle::calcNorme(){
	
	if(norme == NULL)
		this->norme = new Vector(0.0 , 0.0 , 0.0 , *points[0]->getBase());
	
	Vector p0p1 =  *points[1] - *points[0];
	Vector p0p2 =  *points[2] - *points[0]; 

	Vector normeTemp = p0p1.cross(p0p2);
	normeTemp.normalize();
	norme->setX(normeTemp.getXr());
	norme->setY(normeTemp.getYr());
	norme->setZ(normeTemp.getZr());

}	

Vector * Triangle::getNorme()
{
	if(this->norme == NULL)
		this->calcNorme();

	// cout<<"norme = "<<" "<<this->norme->getXr()<<" "<<this->norme->getYr()<<" "<<this->norme->getZr()<<endl;
	return this->norme;
}

void Triangle::drawNorme(Draw *d)
{

	Vector p0p1 =  *points[1] - *points[0];
	Vector p0p2 =  *points[2] - *points[0]; 

	p0p1 *= 1.0/3.0;
	p0p2 *= 1.0/3.0;

	Vector center = *points[0] + p0p1 + p0p2;
	Vector normecentree = (*this->norme + center);
	// cout<<"normecentree calculed"<<endl;	

	d->drawLine(&center , &normecentree , 1.0 , 0.0 , 0.0);
}
vector <Figure2D*> * Triangle::getVoisins()
{
	Draw d;
	vector <Figure2D*> * voisins = new vector <Figure2D*> ();
	set <Vector *>* sommetsCommuns =NULL;
	vector <Vector *> sommetsTriangle;
	set<Vector *>::iterator it;
	Vector * sommetsCommunsFiltree = NULL;
	for(int i = 0 ; i < 3 ; i ++)
	{
		//TODO gerer fuite mémore de sommetsCommuns
		sommetsCommuns = points[i]->getSommetsCommunsDansTrianglesAdjacents2D(points[(i + 1)%3]);
		
		for(it=sommetsCommuns->begin(); it!=sommetsCommuns->end(); ++it)
		{

			if(**it != *points[(i+2) % 3])
			{
				
				(*it)->affichePoint(&d , 1.0 , 1.0 , 0.4);
				sommetsCommunsFiltree = *it;
				delete sommetsCommuns;
				break;
			}


		}
	
		sommetsTriangle.push_back(points[i] );
		sommetsTriangle.push_back(points[(i+1)%3]);
		sommetsTriangle.push_back(sommetsCommunsFiltree);
		

		voisins->push_back(Triangle::getCommonTriangle(&sommetsTriangle));;
		sommetsTriangle.clear();
	}
	

	for(unsigned int i = 0 ; i< voisins->size() ; i++)
	{
		voisins->at(i)->drawTrLignes(&d , 1,0,0);
	}
	return voisins;

	
}



Vector * Triangle::getPoint(int i)
{
	if ( i < 3 && i >= 0)
	{
		return points[i];
	}

	else
	{
		cerr<<"Triangle::getPoint(): Warning i="<<i<<endl;
		return NULL;
	}
}

void Triangle::drawTrLignes( Draw *d )
{

	d->drawLine(points[0] , points[1] , 1.0 , 1.0 , 1.0  );
	d->drawLine(points[1] , points[2] , 1.0 , 1.0 , 1.0  );
	d->drawLine(points[2] , points[0] , 1.0 , 1.0 , 1.0  );
}

void Triangle::drawTrLignes( Draw *d , float r , float g , float b )
{
	
	d->drawLine(points[0] , points[1] , r , g , b  );
	d->drawLine(points[1] , points[2] , r , g , b  );
	d->drawLine(points[2] , points[0] , r , g , b  );
}

bool Triangle::contientPoint(Vector * p)
{
	for(int i = 0 ; i < 3 ; i++)
	{
		if (*p == *points[i])
			return true;
	}
	return false;
}

Vector * Triangle::getSommetDeTrVoisinNonCommun(Triangle * triangleVois)
{
	for(int i = 0 ; i < 3 ; i++)
	{
		if (!this->contientPoint(triangleVois->getPoint(i)))
		{
			return triangleVois->getPoint(i);
		}
	}

	
	return NULL;
}


Figure2D * Triangle::getCommonTriangle(vector <Vector*> * points)
{
    for(unsigned int k = 0 ; k < points->size() ; k++)
    {
        
        vector <Figure2D*>* figures = points->at(k)->getAllFigures2D();
        for(unsigned int i = 0 ; i < figures->size() ; i++)
        {
            if(figures->at(i)->contientPoint(points->at((k+1)%3)) 
            && figures->at(i)->contientPoint(points->at((k+2)%3)) )
            {
                return figures->at(i);
            }
        }
        
    }
    return NULL;
}

unsigned int Triangle::getNombrePoints()
{
	return 3;
}