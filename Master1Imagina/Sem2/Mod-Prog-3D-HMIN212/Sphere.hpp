#ifndef SPHERE_HPP
#define SPHERE_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

extern Base base0;
extern Base base1;

class Sphere {

private:
	Vector centre;
	double rayon;
	Vector **points;
	Base *base;
	unsigned int nb_parall;
	unsigned int nb_merid;
	unsigned int nb_lig;
	unsigned int nb_col;
public:
	Sphere(Base *base , double rayon, unsigned int nb_parall , unsigned int nb_merid );
	void affiche_parall_lignes_corps(Draw *d);
	void affiche_merid_lignes_corps(Draw * d);
	void affiche_surface_corps(Draw *d);
	void affiche_merid_lignes_nord(Draw *d);
	void affiche_merid_lignes_sud(Draw *d);
	void set_nb_parall_merid(unsigned int nb_parall , unsigned int nb_merid);
	void init_new(unsigned int nb_parall , unsigned int nb_merid);
	void init_valeurs(Base *base , double rayon , unsigned int nb_parall , unsigned int nb_merid );
	Vector get_p_nord();
	Vector get_p_sud();
	void add_merid();
	void rm_merid();
	void add_parall();
	void rm_parall();
	void affiche_surface_nord(Draw *d);
	void affiche_surface_sud(Draw *d);
	bool contient(Vector &p);
};
#endif