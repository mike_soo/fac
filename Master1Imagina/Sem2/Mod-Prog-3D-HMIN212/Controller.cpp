#include "Controller.hpp"


extern bool mousegAc , mousemAc ,deuxiemeCli;
extern Vector mouseRecup;
extern bool clickSurPoint;
extern bool parall_modif;
extern bool merid_modif;
extern bool zoom_modif;
extern float zoom;
extern int mode_voxel;
extern Sphere sphere;
extern Cilindre cilindre_triangles;
extern float angle;
extern Vector p1_tr_2;
extern bool divise_modif;
extern short int divise;
bool angle_modif=false;
bool nb_surface_modif=false; 
extern unsigned int nb_visible_surfaces;

extern int ind_voisins;
bool voisins_modif ;
void mouseB(int button, int state, int x, int y)
{

  mouseRecup.setX(x);
  mouseRecup.setY(y);
  switch (button)
  {

  case (GLUT_MIDDLE_BUTTON):
    if (state == GLUT_UP)
    {
      // cout<<"mousmDeac"<<endl;
      mousemAc = false;
      deuxiemeCli = false;
    }

    else if (state == GLUT_DOWN)
    {
      mousemAc = true;
      glutPostRedisplay();
    }
    break;

  case (GLUT_LEFT_BUTTON):

    if (state == GLUT_DOWN)
    {
      mousegAc = true;
      
      glutPostRedisplay();
    }
    else if (state == GLUT_UP)
    {
      mousegAc = false;
      clickSurPoint = false;
      glutPostRedisplay();
    }
    break;


  }
}

void mouseM(int x, int y)
{ 
  // if (mousegAc)
  // { //indicatif
    // std::cout<<" Souris a:"<<x<<" "<<y<<endl;
    mouseRecup.setX(x);
    mouseRecup.setY(y);
    glutPostRedisplay();
  // }
}


void keyboard(unsigned char key, int x, int y) 
{

 

  if(key == 27)
  {
    exit(1);
  }
  else if(key =='v')
  {
    if(voisins_modif)
    {
      cout<<"voisins_modif : off"<<endl;
      voisins_modif=false;
    }
    else
    {
      cout<<"voisins_modif : on"<<endl;
      voisins_modif=true;
    }
  }
  else if(key =='z')
  {
    if(zoom_modif)
    {
      cout<<"zoom_modif : off"<<endl;
      zoom_modif = false;
    }
    else
    {
      cout<<"zoom_modif : on"<<endl;
      zoom_modif =true;
    }
  }

  else if(key == 'p') 
  {
    if(parall_modif)
    {
      parall_modif = false;
    }
    else
    {
      parall_modif = true;
    }
  }

  else if(key == 'm') 
  {
    if(merid_modif)
    {
      merid_modif = false;
    }
    else
    {
      merid_modif = true;
    }
  }

  else if(key == 'x') 
  {

    mode_voxel  = (mode_voxel +1) % 6; 
    cout<<"mode_voxel "<<mode_voxel<<endl;
  }
  
  else if (key =='s')
  {
    if(nb_surface_modif)
    {
      cout<<"nb_surface_modif : off"<<endl;
      nb_surface_modif = false;
    }
    else
    {
      cout<<"nb_surface_modif : on"<<endl;
      nb_surface_modif = true;
    }
  }

  else if (key == 'a')
  {
    //angle dièdre test
    if(angle_modif)
    {
      cout<<"angle_modif : off"<<endl;
      angle_modif = false;
    }
    else 
    {
      cout<<"angle_modif : on"<<endl;
      angle_modif = true;
    }
  }
  else if (key == 'd')
  {

    if(divise_modif)
    {
      cout<<"divise_modif : off"<<endl;
      divise_modif = false;
    }
    else
    {
      cout<<"divise_modif : on"<<endl;
      divise_modif = true;
    }
  }
  else if(key == 43 ) // +
  {
    if(voisins_modif)
    {

      ind_voisins++;
      cout<<"voisins_modif : "<<ind_voisins<<endl;
    }

    if(angle_modif)
    {
      cout<<"angle++"<<endl;
      angle+= M_PI/32.0;
      p1_tr_2.setX(0.0);
      p1_tr_2.setY(sin(angle));
      p1_tr_2.setZ(cos(angle));
    }
    if(parall_modif)
    {
      sphere.add_parall();
      cilindre_triangles.add_parall();
    }

    if(zoom_modif)
      zoom += 0.25;
      
    if(merid_modif)
    {
      sphere.add_merid();
      cilindre_triangles.add_merid();
    }
    if(nb_surface_modif)
    {
      nb_visible_surfaces++;
      cout<<"nb_visible_surfaces"<<nb_visible_surfaces<<endl;
      
    }
    if(divise_modif)
    {
      divise = 1;
    }
     
  }
  
  else if(key == 45 ) // -
  {
    if(voisins_modif)
    {
      ind_voisins--;
      cout<<"voisins_modif : "<<ind_voisins<<endl;
    }

    if(angle_modif)
    {
      angle-= M_PI/32.0;
      p1_tr_2.setX(0.0);
      p1_tr_2.setY(sin(angle));
      p1_tr_2.setZ(cos(angle));
    }
    if (parall_modif)
    {
      sphere.rm_parall();
      cilindre_triangles.rm_parall();
    }
      
    if (merid_modif)
    {
      sphere.rm_merid();
      cilindre_triangles.rm_merid();
    }
      
    if (zoom_modif)
      zoom -= 0.25;
    
    if(nb_surface_modif)
      nb_visible_surfaces--;

    if(divise_modif)
      divise=-1;
        
  }      
  else
    printf ("La touche %d n´est pas active.\n", key);
    
  cout<<"La touche "<<(int)key << "a été detectée"<<endl;
  glutPostRedisplay();
    
     
  
}