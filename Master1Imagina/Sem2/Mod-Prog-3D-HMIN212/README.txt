TODO

Quelques problèmes de conception objet: 
- la classe BezierSurface devrait avoir un attribut de type BezierCurveByBernstein. 
	- Ceci pourrait améliorer la gestion de création de points par l'algorithme de casteljau lorsque le nombre de colonne n'est pas égal au nombre de ligne (par rapport aux points de contrôle). 

- La méthode BezierCurveByBerstein::setPointsControl et a enlever pour utiliser le constructeur (de BCBB) dans constructeur de BezierSurfarce
	- Ceci peut potentiellement éviter de recréer à chaque itération le tableau contenant les points de contrôle de grille (i.e surface de bezier)

- La façon dont nbU et nbV sont utilisés dans BezierSurface est à revoir car ceci sont passés uniquement en paramètre lors des calculs  contrairement au fait que l'on pourrait les définir comme attribut de la classe BezierSurface, pour ainsi aider à la construction de la classe BCBB car cette dernière prend nbU en paramètre uniquement!

Debut dans branche cor_constr_set_in_CurveBezier