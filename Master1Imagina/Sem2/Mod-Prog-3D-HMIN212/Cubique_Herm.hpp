#ifndef CUBIQUE_HERM_HPP
#define CUBIQUE_HERM_HPP

#include "Vector.hpp"
#include "Point.hpp"
#include "Draw.hpp"
#include <iostream>
using namespace std;
#include <cmath>
class Cubique_Herm{

private:
	Vector * cubiqueHermPoints; //contient p1 p2 v1 v2
	Vector *points; //contient courbe
	size_t nbU;
	Vector _p0,_p1;
	Vector _v0,_v1;

public: 
	Cubique_Herm(Vector * cubiqueHermPoints, Vector * points, size_t nbU);
	void hermiteCubicCurve(Draw * d);
	double f1(double u);
	double f2(double u);
	double f3(double u);
	double f4(double u);

};

#endif