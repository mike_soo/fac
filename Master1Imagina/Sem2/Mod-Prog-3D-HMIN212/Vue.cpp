#include "Vue.hpp"
#include "Draw.hpp"
extern Vector mouseCord3D;
extern Vector* ptClicked;
extern bool clickSurPoint;
extern Vector p1,p2,p1p2;
extern bool deuxiemeCli , mousemAc;
extern float **Ru;
extern Vector vso;
extern Vector vsx;
extern Vector vsy;
extern Vector vsz;
extern float theta;
extern float zoom,ortho;

extern Draw d;
void getOGLPosSai( int x , int y)
{
  GLint viewport[4];  //Les valeurs de la fenêtre affichées seront stockées ici.
  GLdouble modelview[16];  //Les 16 doubles correspondants à la matrice de vue du modèle seront stockés ici.
  GLdouble projection[16];   //Les 16 doubles correspondants à la matrice de projection seront stockés ici.
  GLfloat winX, winY, winZ;
  GLdouble posX, posY, posZ;
  glGetDoublev( GL_MODELVIEW_MATRIX, modelview ); //Obtient et stocke les valeurs des 16 doubles dans modelview[].

  glGetDoublev( GL_PROJECTION_MATRIX, projection ); //Obtient et stocke les valeurs des 16 doubles dans projection[].

  glGetIntegerv( GL_VIEWPORT, viewport ); //Obtient les valeurs X, Y, longueur et hauteur de l'écran  puis on les stocke
  //dans viewport[] où X et Y correspondent aux coordonnées en haut à gauche de la fenêtre.

  winX = (float)x;
  winY = (float)viewport[3] - (float)y; //on inverse l'axe des y car de base, le centre de l'axe des x et y
  winZ = 0 ;
  //se trouve en haut a gauche, avec un axe y qui est dirigé vers le bas. Or
  //notre axe y en cmv se dirige vers le haut.
  //glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
  
  gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
  //stocke dans posX et dans posY les coordonnées du monde virtuel des
  //coordonnées fenêtres x et y passées en paramètres de la fonction.


  mouseCord3D.setX(posX); //On stocke posX puis posY dans mouseCord3D, une variable globale de type Point qu'on pourra utiliser plus tard.
  mouseCord3D.setY(posY);
  mouseCord3D.setZ(0);

  // cout<<"mouseCord3D ="<<"("<<mouseCord3D.getX()<<","<<mouseCord3D.getY()<<")"<<endl;
}

bool clickedPoint(Vector *point)
{
  if (mouseCord3D.getX() - 0.03 < point->getX() && mouseCord3D.getX() + 0.03 > point->getX() && mouseCord3D.getY() - 0.03 < point->getY() && mouseCord3D.getY() + 0.03 > point->getY())
  {
    clickSurPoint = true;
    ptClicked = point;
    cout << "Click sur points!" << endl;
    return true;
  }
  else
  {
    return false;
  }    
}

// NOOOOOON !!!!!!!!!!!!!!!!!!!!!
bool clickedPointFromArray(Vector *points, int n)
{
  for (int i = 0 ; i < n ; i ++)
  {
    if( clickedPoint(&points[i]))
    {
      return true;
    }
  }
  return false;
}



void determineAxeRotation()
{
  //indicatif
    double posX = mouseCord3D.getX() , posY = mouseCord3D.getY();

    Vector verif;
    double thetaPP;
   
    if(deuxiemeCli)
    { 
      p2.setX((float)posX*10000);
      p2.setY((float)posY*10000);
      p2.setZ(0);
      // cout<<"p2:("<<p2.getX()<<","<<p2.getY()<<","<<p2.getZ()<<")"<<endl; 
      
       
      verif=(p2-p1); 
      // cout << "verif:(" << verif.getX() << "," << verif.getY() << "," << verif.getZ() << ")" << endl;

      //vérification de la norme du nouveau vecteur p2-p1;
      float norme=(float)sqrt((verif.getX()*verif.getX())+(verif.getY()*verif.getY())+(verif.getZ()*verif.getZ()));
      // indicatif
       //cout<<" \e[34m NORME="<<norme<<"\e[0m ";
        
      if((p1!=p2) && (0<(float)norme)) //on évite la division par zéro
       {
         thetaPP=(float)acos((float)verif.getX()/(float)norme); //on calcule l'angle de p2-p1
         // indicatif
          //cout<<"\e[32m thP1P2="<<thetaPP*180/M_PI<<"\e[0m"; //on affiche l'angle de p2-p1
       
         if(verif.getY()<0) //on calcule en fonction du signe de Y l'angle thetaPP
          {
           Vector tempp((float)cos(-thetaPP),(float)sin(-thetaPP),0); //on calcul le vecteur représentant le vecteur p2-p1 avec une norme de 1
           p1p2=tempp; //p1p2 amplifié avec norme égal 1
          }
           else
          {
            Vector tempp((float)cos(thetaPP),(float)sin(thetaPP),0); //on calcul le vecteur représentant le vecteur p2-p1 avec une norme de 1
            p1p2=tempp; //p1p2 amplifié avec norme égal 1
          } 

          // cout<<"p1p2:("<<p1p2.getX()<<","<<p1p2.getY()<<","<<p1p2.getZ()<<")"<<endl; 
        }
          //p1p2=verif; //le vecteur p2-p1 avec sa norme sans imposition de norme =1
        
      

      
     /*titre indicatif
     cout<<"\e[34m np1p2="<<(float)sqrt((p1p2.getX()*p1p2.getX())+(p1p2.getY()*p1p2.getY())+(p1p2.getZ()*p1p2.getZ()))<<" "; //norme du vecteur p1p2
     cout<<" np1="<<(float )sqrt((p1.getX()*p1.getX())+(p1.getY()*p1.getY())+(p1.getZ()*p1.getZ())); //norme du vecteur p1
     cout<<" np2="<<(float )sqrt((p2.getX()*p2.getX())+(p2.getY()*p2.getY())+(p2.getZ()*p2.getZ()))<<"\e[0m"; //norme du vecteur p2
     
     cout<<"\e[35m p1=("<<p1.getX()<<","<<p1.getY()<<") "; //coordonnées p1
     cout<<" p2=("<<p2.getX()<<","<<p2.getY()<<") "; //coordonnées p2
     cout<<" p1p2=("<<p1p2.getX()<<","<<p1p2.getY()<<") \e[0m"; //coordonnées p1p2*/
     //cout<<endl;
     p2.setX(0);
     p2.setY(0);
     p1.setX(0);
     p1.setY(0);
     
     theta=zoom * (norme*M_PI/(64*500*ortho)) ;
     // cout<<"theta : "<<theta<<endl;
    }

    else if(!deuxiemeCli)
    {
      p1.setX((float)posX*10000);
      p1.setY((float)posY*10000);
      p1.setZ(0);
      deuxiemeCli=true;
      // cout<<"p1:("<<p1.getX()<<","<<p1.getY()<<","<<p1.getZ()<<")"<<endl; 
      
      
    }
 

    // verif.setX(posX*100);
    // verif.setY(posY*100);
    // thetaPP=(float)acos((float)verif.getX()/(sqrt((float)(verif.getX()*verif.getX())+(verif.getY()*verif.getY()))));

}




void rotation (float Ux,float Uy,float Uz)
{
 Ru[0][0]=(float)cos(theta)+(Ux*Ux*(1-cos(theta)));
 Ru[1][0]=(float)Uy*Ux*(1-cos(theta))+(Uz*sin(theta));
 Ru[2][0]=(float)Uz*Ux*(1-cos(theta))-(Uy*sin(theta));

 Ru[0][1]=(float)Uy*Ux*(1-cos(theta))-(Uz*sin(theta));
 Ru[1][1]=(float)cos(theta)+Uy*Uy*(1-cos(theta));
 Ru[2][1]=(float)Uz*Uy*(1-cos(theta))+Ux*sin(theta);

 Ru[0][2]=(float)Ux*Uz*(1-cos(theta))+Uy*sin(theta);
 Ru[1][2]=(float)Uy*Uz*(1-cos(theta))-Ux*sin(theta);
 Ru[2][2]=(float)cos(theta)+Uz*Uz*(1-cos(theta)); 

 Vector tempvsx((float)vsx.getX()*Ru[0][0]+vsx.getY()*Ru[0][1]+vsx.getZ()*Ru[0][2]  , (float)vsx.getX()*Ru[1][0]+vsx.getY()*Ru[1][1]+vsx.getZ()*Ru[1][2]  ,  (float)vsx.getX()*Ru[2][0]+vsx.getY()*Ru[2][1]+vsx.getZ()*Ru[2][2]);
 Vector tempvsy((float)vsy.getX()*Ru[0][0]+vsy.getY()*Ru[0][1]+vsy.getZ()*Ru[0][2]  , (float)vsy.getX()*Ru[1][0]+vsy.getY()*Ru[1][1]+vsy.getZ()*Ru[1][2]  ,  (float)vsy.getX()*Ru[2][0]+vsy.getY()*Ru[2][1]+vsy.getZ()*Ru[2][2]);
 Vector tempvsz((float)vsz.getX()*Ru[0][0]+vsz.getY()*Ru[0][1]+vsz.getZ()*Ru[0][2]  , (float)vsz.getX()*Ru[1][0]+vsz.getY()*Ru[1][1]+vsz.getZ()*Ru[1][2]  ,  (float)vsz.getX()*Ru[2][0]+vsz.getY()*Ru[2][1]+vsz.getZ()*Ru[2][2]);

 vsx=tempvsx;
 vsy=tempvsy;
 vsz=tempvsz;
 // cout<<"vsx:("<<vsx.getX()<<","<<vsx.getY()<<","<<vsx.getZ()<<")"<<endl; 
 // cout<<"vsy:("<<vsy.getX()<<","<<vsy.getY()<<","<<vsy.getZ()<<")"<<endl; 
 // cout<<"vsz:("<<vsz.getX()<<","<<vsz.getY()<<","<<vsz.getZ()<<")"<<endl; 
}




void gestionRotation()
{
  if(mousemAc)
  {
    // cout<<"mousemAC!"<<endl;
    if(deuxiemeCli)
    {
      determineAxeRotation();
      Vector vUx((p1p2.getX()*cos(M_PI/2))-(p1p2.getY()*sin(M_PI/2)),(p1p2.getX()*sin(M_PI/2))+(p1p2.getY()*cos(M_PI/2)),0);
      
      d.drawLine(&vso , &vUx);
      // cout<<"vUx:("<<vUx.getX()<<","<<vUx.getY()<<","<<vUx.getZ()<<")"<<endl<<endl;; 
      
      rotation(vUx.getX(),vUx.getY(),vUx.getZ());
      deuxiemeCli=false;
    }

    determineAxeRotation();
  }
  else
  {

  }
}