#ifndef VOXEL_HPP
#define VOXEL_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "Base.hpp"
#include "Sphere.hpp"
#include "Cilindre.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <unistd.h>
extern Base base0;
extern Base base1;

class Voxel 
{
private:
	Base *base; 
	Vector origine_rel; 
	Vector *origine;
	double longueur;
	Vector p1;
	Vector p2;
	Vector p3;
	Vector p4;
	Vector p5;
	Vector p6;
	Vector p7;
	vector<Voxel*> * filsVoxel;
	vector<Vector*> * points;
	vector<Vector*> * pointsVoxel;
	vector<Vector*> * pointsContenus;
public:
	Voxel(Base *base , Vector origine_rel , double longueur );
	Voxel(Base *base , Vector* origine , double longeur );
	void init_valeurs(Base *base ,Vector  origine_rel , double longueur);
	void init_valeurs(Base *base ,Vector *origine     , double longueur);
	void addPoint(Vector *p);
	void draw(Draw *d);
	void octree(Sphere *s , Cilindre *c , int res , int res_max , Draw *d , bool go_next , int mode);
	void draw_surfaces(Draw *d);
	void draw_lines(Draw *d ,float r , float g , float b);
	void octree( int res , int res_max , Draw *d );
	bool contientPoint(Vector * p);

};
#endif