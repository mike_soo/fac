#ifndef MAILLAGE_HPP
#define MAILLAGE_HPP
#include "Vector.hpp"
#include "Base.hpp"
#include "Figure2D.hpp"
#include "Triangle.hpp"
#include "Utils.hpp"
#include "VoxelG.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <unistd.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <limits>
extern Base base0;
extern Base base1;

using namespace std;
class Maillage 
{
private:
	
public:
	static void afficheMaillageTrs(Draw *d , vector <Figure2D*>* figures2D);
	static void spotAngleDiedre(float angleSeuil , Draw *d , vector <Figure2D*>* figures2D);
	static void butterflySubdivision(vector <Figure2D*> *maillage, vector <Figure2D*> *maillageSubidivisee , Base * base);
	static void distances(vector <Figure2D*> *maillage1, vector <Figure2D*> *maillage2 , Draw *d , vector<Vector*> *points2a2plusProchesNaif);
	static Vector * calcPointPlusProche(Vector* p , vector <Figure2D*>* maillage);
	static void afficheMaillage(Draw *d , vector <Figure2D*>* figures2D , float r , float g , float b);
	static unsigned long long int idMaillageDispo;
	static unsigned long long int getIdMaillageDispo();
	static vector<Vector*> *getToutLesPointsDeMaillages(vector<vector<Figure2D*> *> * maillages);

	static void distancesVoxels(vector<Vector*>* pointsMaillages , Draw * d , unsigned long long int idMaillage1 , unsigned long long int idMaillage2 , vector<Vector*>* points2a2plusProches);
	
};
#endif