#include "Sphere.hpp"

Sphere::Sphere(Base *base , double rayon , unsigned int nb_parall , unsigned int nb_merid )
{
	init_new(nb_parall , nb_merid);
	init_valeurs( base , rayon , nb_parall , nb_merid);
}

void Sphere::init_new(unsigned int nb_parall , unsigned int nb_merid)
{
	this->nb_lig = nb_parall * 5;
	this->nb_col = nb_merid *5;
	points = new Vector * [this->nb_lig];
	for(unsigned int i = 0 ; i < this->nb_lig ; i++)
	{
		points[i]=new Vector[this->nb_col];
	}

}

void Sphere::init_valeurs(Base *base , double rayon , unsigned int nb_parall , unsigned int nb_merid )
{
	this->rayon = rayon ;
	this->nb_parall = nb_parall;
	this->nb_merid = nb_merid;
	this->base = base;

	cout<<"nb_parall="<<nb_parall << " nb_merid="<<nb_merid<<" nb_lig="<<nb_lig<<" nb_col="<<nb_col<<endl;
		
	bool retrecir = false;

	double d_zi = ((1.0/nb_parall) *2* rayon) ;
	double z_start = rayon  - d_zi ;
	
	double d_r = rayon * (2.0 / nb_parall);
	double r = 0;
	double z = z_start;
	


	for(unsigned int i = 0 ; i < nb_parall ; i++)
	{
		if(!retrecir && r + d_r <=this->rayon )
		{
			r+= d_r;

		}
		else 
		{
			retrecir = true;
			r-= d_r; 
		}

		//parallèles
		for(unsigned int j = 0 ; j < nb_merid ; j++)
		{	
			
			points[i][j].setX( base->getO().getX() +
							 (sqrt(pow(rayon,2) - pow(z,2))) * cos(((double) j /nb_merid) * (2.0 * M_PI)));
			
			points[i][j].setY( base->getO().getY() +
							 (sqrt(pow(rayon,2) - pow(z,2))) * sin(((double) j /nb_merid) * (2.0 * M_PI)));
			
			points[i][j].setZ(base->getO().getZ() + z);

			points[i][j].setBase(*base);
			

		}
		
		z = z_start - (d_zi * i);
		
	}
	
}

bool Sphere::contient(Vector &p)
{
	// cout<<pow(p.getXr(),2.0) + pow(p.getYr(),2.0) + pow(p.getZr(),2.0) <<" ?< "<< pow(rayon,2.0)<<endl;;
	return 	p.getX() < base->getO().getX() + rayon && 
			p.getY() < base->getO().getY() + rayon &&
			p.getZ() < base->getO().getZ() + rayon &&
			
			p.getX() > base->getO().getX() - rayon && 
			p.getY() > base->getO().getY() - rayon &&
			p.getZ() > base->getO().getZ() - rayon &&
			((pow(p.getXr(),2) + pow(p.getYr(),2) + pow(p.getZr(),2)) <= pow(rayon,2)) ;

			
}

Vector Sphere::get_p_nord()
{
	return Vector (0 , 0 , this->rayon , base1);
}

Vector Sphere::get_p_sud()
{
	return Vector (0 , 0 , -this->rayon , base1);
}


void Sphere::affiche_parall_lignes_corps(Draw *d)
{
	for(unsigned int i = 0 ; i < nb_parall ; i++)
	{

		d->drawCurve(points[i] , nb_merid , 0.0 , 0.3 , 1.0 );
		d->drawLine(&points[i][nb_merid - 1 ] , &points[i][0] , 0.0 , 0.3 , 1.0);
	}
}


void Sphere::affiche_merid_lignes_nord(Draw *d)
{
	double z_nord =  this->rayon;
	Vector point_nord(0 , 0 , z_nord , base1);

	for(unsigned int j = 0 ; j < nb_merid ; j++)
	{
		d->drawLine(&point_nord , &points[0][j] , 0.0 , 1.0 , 0.0);
	}
}

void Sphere::affiche_merid_lignes_corps(Draw * d)
{
	for(unsigned int j = 0 ; j < nb_merid  ; j++)
	{
		for(unsigned int i =0 ; i< nb_parall -1 ; i++)
		{
			d->drawLine(&points[i][j] , &points[(i+1) % nb_parall][j] , 0.0 , 0.6 , 1.0);
		}
	}
}

void Sphere::affiche_merid_lignes_sud(Draw *d)
{
	double z_sud = - this->rayon;
	Vector point_sud(0 , 0 , z_sud , base1);
	
	 		
	for(unsigned int j = 0 ; j < nb_merid ; j++)
	{
		d->drawLine(&point_sud , &points[nb_parall - 1][j] ,1.0 , 0.0 , 0.0);
	}
}

void Sphere::affiche_surface_nord(Draw *d)
{
	Vector point_nord =this->get_p_nord();
	d->drawTriangleFan(nb_merid, &point_nord , points[0] , 1);
	
}

void Sphere::affiche_surface_sud(Draw *d )
{
	Vector point_sud = this->get_p_sud();
	d->drawTriangleFan(nb_merid , &point_sud , points[nb_parall - 1] , -1);

}

void Sphere::affiche_surface_corps(Draw *d)
{
	for(unsigned int i = 0 ; i < nb_parall ; i ++ )
	{
		for(unsigned int j = 0 ; j < nb_merid ; j ++)
		{
			d->drawSurface(&points[i][j] , &points[i][(j+1) % nb_merid] , &points[(i+1) % nb_parall][(j+1) % nb_merid] , &points[(i+1) % nb_parall][j]);

		}
	}
}

void Sphere::set_nb_parall_merid(unsigned int nb_parall , unsigned int nb_merid)
{
	unsigned new_nb_lig , new_nb_col;
	new_nb_lig = this->nb_lig;
	new_nb_col = this->nb_col;
	if(nb_parall > nb_lig)
	{
		cout<<"coucou"<<endl;
		new_nb_lig = nb_parall * 5;
		
	}
	if(nb_merid > nb_col)
	{
		new_nb_col = nb_merid * 5;
	}

	if(nb_parall > nb_lig || new_nb_col > nb_col)
	{
		cout<<"readaptation"<<endl;
		Vector ** new_tab_points = new Vector *[new_nb_lig];;
		
		for(unsigned int i = 0 ; i < new_nb_lig ; i ++)
		{
			new_tab_points[i] = new Vector[new_nb_col];
		}
		
		//liberation de mémoire
		for(unsigned int i = 0 ; i < nb_lig ; i++)
		{
			// delete points[i];
		}
		// delete points;

		points = new_tab_points;
		this->nb_lig 	= new_nb_lig ;
		this->nb_col 	= new_nb_col ;
	}

	cout<<"init valeurs"<<endl;
	init_valeurs(this->base,rayon , nb_parall , nb_merid);

}

void Sphere::add_merid()
{
	this->set_nb_parall_merid(this->nb_parall , this->nb_merid + 1);
}

void Sphere::rm_merid()
{
	if(this->nb_merid - 1 >= 0)
		this->set_nb_parall_merid(this->nb_parall , this->nb_merid - 1);
}


void Sphere::add_parall()
{
	this->set_nb_parall_merid(this->nb_parall + 1 , this->nb_merid);
}

void Sphere::rm_parall()
{
	if(this->nb_parall  - 1> 0 )
		this->set_nb_parall_merid(this->nb_parall - 1 , this->nb_merid);
}

