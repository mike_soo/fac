#ifndef DRAW_HPP
#define DRAW_HPP

#include "Graphics.h"

#include "Vector.hpp"
#include <iostream>

 
#include "Base.hpp"

using namespace std;
class Draw
{
private:
	size_t nb_ps;
	Vector *points;
public:
	Draw();
	Draw( Vector *points, size_t nb_ps);
	
	void drawCurve();
	void drawCurve(Vector * points[] , size_t nb_ps , Base &base);
	void drawCurve(Vector points[] , size_t nb_ps , Base &base);
	void drawCurve(Vector * points[] , size_t nb_ps , Base &baseSelect , float r , float g , float b);
	void drawCurve(Vector points[] , size_t nb_ps , Base &baseSelect , float r , float g , float b);
	void drawCurve(Vector points[] , size_t nb_ps , float r, float g , float b);

	void setPoints(Vector *points, size_t nb_ps);
	
	void drawPoint(Vector * p);
	void drawPoint(Vector * p, float r, float g, float b);		
	void drawPoint(Vector * p , Base *base , float r , float g , float b);
	
	void drawLine(Vector *o , Vector *m);
	void drawLine(Vector *o , Vector *m , float r , float g , float b);
	void drawLine(Vector *o , Vector *m , Base *base , float r , float g , float b);
	
	void drawBase(Base *base , float r , float g , float b);

	void drawSurface(Vector *p1 , Vector *p2 , Vector *p3 , Vector *p4);
	void drawSurface(Vector *p1 , Vector *p2 , Vector *p3 , Vector *p4 , float r , float g , float b);

	void drawTriangleFan(unsigned int nb_vertex_bord, Vector *center , Vector bord[] , short int direction);
	
	void drawTrianglesVbo(GLfloat* CubeArray, GLuint* IndiceArray , GLsizeiptr sizeofCubeArray , GLsizeiptr sizeofIndiceArray);
};

#endif