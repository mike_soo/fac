
///////////////////////////////////////////////////////////////////////////////
// Imagina
// ----------------------------------------------------------------------------
// IN - Synthèse d'images - Modélisation géométrique
// Auteur : Gilles Gesquière
// ----------------------------------------------------------------------------
// Base du TP 1
// programme permettant de créer des formes de bases.
// La forme représentée ici est un polygone blanc dessiné sur un fond rouge
///////////////////////////////////////////////////////////////////////////////
// #include <GL/glew.h>
// #include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Vector.hpp"
#include "Base.hpp"
#include "Draw.hpp"
#include "Cubique_Herm.hpp"
#include "BezierCurveByBernstein.hpp"
#include "Controller.hpp"
#include "Vue.hpp"
#include "BezierSurface.hpp"
#include "Sphere.hpp"
#include "Cilindre.hpp"
#include "Voxel.hpp"
#include "Graphics.h"

#include "Triangle.hpp"
/* Dans les salles de TP, vous avez généralement accès aux glut dans C:\Dev. Si ce n'est pas le cas, téléchargez les .h .lib ...
Vous pouvez ensuite y faire référence en spécifiant le chemin dans visual. Vous utiliserez alors #include <glut.h>.
Si vous mettez glut dans le répertoire courant, on aura alors #include "glut.h"
*/

#define TP1_2 0
#define TP3 0
#define TP4 0
#define TP5 0
#define TP7 1

// Définition de la taille de la fenêtre
#define WIDTH  600

#define HEIGHT 600

// Définition de la couleur de la fenêtre
#define RED   0.8
#define GREEN 0.8
#define BLUE  0.8
#define ALPHA 1


/* ==================== ROTATION ========================*/

Vector p1(0, 0, 0);
Vector p2(0, 0, 0);
Vector p1p2(0, 0, 0);
float **Ru;
bool deuxiemeCli;

Vector vo0(0 , 0 , 0);
Vector vx0(1 , 0 , 0);
Vector vy0(0 , 1 , 0);
Vector vz0(0 , 0 , 1);
Base base0(vo0 , vx0 , vy0 , vz0);

Vector vso(0 , 0 , 0);
Vector vsx(1 , 0 , 0);
Vector vsy(0 , 1 , 0);
Vector vsz(0 , 0 , 1);
Base base1(vso , vsx , vsy , vsz);


float theta = M_PI / 128;
/* ======================================================*/


/*=====================TP_1_2=============================*/
//Projection
Vector v[3];

size_t const nbU = 30;
size_t const nbControlPoints = 4;

//CubiqueHerm
Vector cubiqueHermPoints[4];
Vector tabPointsHCC[nbU];
Cubique_Herm c_H(cubiqueHermPoints, tabPointsHCC, nbU );

//BezierCurveByBernsteinPointControl
Vector controlPoints[nbControlPoints];
Vector tabCurveByBernstein[nbU];
BezierCurveByBernstein b = BezierCurveByBernstein(controlPoints, tabCurveByBernstein, nbControlPoints, nbU);

//BezierCurveByCasteljauv
Vector p_Casteljauv_test;
Vector p_casteljauv_pitch_y;




Draw d;

//Cordonnés 2D (interfaz)
Vector mouseRecup;
Vector *ptClicked;

//Cordonnés 3D (opengl)
Vector mouseCord3D;

bool mousegAc = false , mousemAc = false; 
bool clickSurPoint = false;


/*=======================TP_3============================*/
//BezierSurfaceByCasteljauv
Vector p_casteljauv_pitch_x_y;
int const nbControlPoints_i = 3;
int const nbControlPoints_j = 5;

const float MIN_CASTEL_PITCH_X = 0.4;
const float MAX_CASTEL_PITCH_X = 1.4;
const float MIN_CASTEL_PITCH_Y = -1.4;
const float MAX_CASTEL_PITCH_Y = -0.4;


BezierCurveByBernstein bcurveforSurface= BezierCurveByBernstein(controlPoints, tabCurveByBernstein, nbControlPoints, nbU);
BezierSurface bsurface = BezierSurface(nbControlPoints_i, nbControlPoints_j , &bcurveforSurface);


/*======================TP_4=============================*/
Vector centreSphere(0,0,0);
Sphere sphere(&base1 , 0.5 , 5 , 5);
bool parall_modif =false;
bool merid_modif=false;
bool preDraw = true;
float zoom=0.2;
bool zoom_modif=false;
/*======================================================*/

/*=====================TP_5=============================*/
Vector voxel_orig(-5,-5,-5,base1);

// Voxel voxel(&base1 , voxel_orig , 10);
// Sphere sphere_voxel(&base1 , 5 , 20 , 20);
// Cilindre cilindre_voxel(&base1 , 6 , 3 , 20 , 20);
int mode_voxel = 0;
/*======================================================*/

/*===================== TP_7 =================== */
Cilindre cilindre_triangles(&base1 , 6 , 3 , 7 , 4);
Triangle *tr_1  = new Triangle(&vsx , &vsy , &vso);
Triangle *tr_2  = new Triangle(&vsx , &vsz , &vso);

/*===========================================*/


// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);


int main(int argc, char **argv)
{
  Ru = new float *[3];

  for (int i = 0; i < 3; i++)
  {
    Ru[i] = new float [3];
  }

  cout << "Ru initialised" << endl;

  if (TP1_2 == 1)
  {
    v[0] = Vector( -1.2  , -1 ,  0.0);
    v[1] = Vector( -1.2  , -0.5 ,  0.0);
    v[2] = Vector( -1.0  , -1 ,  0.0);


    controlPoints[0] = Vector( 0.2  , -1.7 ,  0.0);
    controlPoints[1] = Vector( 1.2  , -1.7 ,  0.0);
    controlPoints[2] = Vector( 1.2  ,  0.0 ,  0.0);
    controlPoints[3] = Vector( 1.0  ,  0.0 ,  0.0);
    // controlPoints[4] = Vector( 0.0  ,  0.2 ,  0.0);

    cubiqueHermPoints[0] = Vector(0.2,  0.9,  0.0);
    cubiqueHermPoints[1] = Vector(1.2,  0.9,  0.0);
    cubiqueHermPoints[2] = Vector(0.8,  1.2,  0.0);
    cubiqueHermPoints[3] = Vector(1.5,  1.3,  0.0);

    p_casteljauv_pitch_y = Vector(1.4 ,  0.0,  0.0);
  }


  if (TP3 == 1)
  {
    double r = 0.1;
    double pas_angulaire_theta = 1.0 / nbControlPoints_j;
    double pas_distance_axe = 1.0 / nbControlPoints_j;

    Vector v;
    for (int i = 0 ; i < nbControlPoints_i ; i++)
    {
      for (int j = 0 ; j < nbControlPoints_j ; j++)
      {
        v.setX(r * cos(pas_angulaire_theta * j * M_PI) );
        v.setY(r * sin(pas_angulaire_theta * j * M_PI) );
        v.setZ(pas_distance_axe * i * 1.5);
        v.setBase(base0);
        bsurface.setControlPoint(i, j, v);
      }
      if ((double) (i / nbControlPoints ) < 0.5 )
      {
        r += 0.8;
      }
      else
      {
        r -= 0.8;
      }

    }

    p_casteljauv_pitch_x_y = Vector(MIN_CASTEL_PITCH_X + 0.5 , MIN_CASTEL_PITCH_Y + 0.5,  0.0);
  }


  if(TP4 == 1)
  {
    vsx.setX(1.0);
    vsx.setY(0.0);
    vsx.setZ(0.0);

    vsy.setX(0.0);
    vsy.setY(0.0);
    vsy.setZ(-1.0);

    vsz.setX(0.0);
    vsz.setY(1.0);
    vsz.setZ(0.0);
  }

  if(TP7 == 1)
  {     
    cout<<"test"<<endl;
    vsx.addFigure2D(tr_1);
    vsy.addFigure2D(tr_1);
    vso.addFigure2D(tr_1);
    cout<<"test"<<endl; 
  }

  // initialisation  des paramètres de GLUT en fonction
  // des arguments sur la ligne de commande

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

  // définition et création de la fenêtre graphique, ainsi que son titre
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(1500, 0);
  glutCreateWindow(":)");

  // initialisation de OpenGL et de la scène
  initGL();
  init_scene();
  glewInit();
     
  // choix des procédures de callback pour
  // le tracé graphique
  glutDisplayFunc(&window_display);
  // le redimensionnement de la fenêtre
  glutReshapeFunc(&window_reshape);
  // la gestion des événements clavier
  glutKeyboardFunc(&keyboard);

  // gestion des événements boutons de souris
  glutMouseFunc(mouseB);
  // gestion des mouvements souris
  glutMotionFunc(mouseM);

  // la boucle prinicipale de gestion des événements utilisateur
  cout << "Start glutMainLoop" << endl;
  glutMainLoop();

  return 1;
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL()
{
  glClearColor(RED, GREEN, BLUE, ALPHA);
}

// Initialisation de la scene. Peut servir à stocker des variables de votre programme
// à initialiser
void init_scene()
{
  glClearColor(0, 0, 0, 0);
  glDepthMask(GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glDepthRange(0.0f,1.0f);
  glDepthFunc(GL_LEQUAL); 

  glEnable(GL_COLOR_MATERIAL);
  /*
  float mcolor[] = { 0.2f, 0.0f, 1.0f, 1.0f };
  float specReflection[] = { 0.1f, 0.1f, 0.8f, 1.0f };

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mcolor);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specReflection);

  glMateriali(GL_FRONT, GL_SHININESS, 96);*/
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glShadeModel(GL_SMOOTH); //GL_FLAT GL_SMOOTH
  glEnable(GL_MULTISAMPLE);



}

// fonction de call-back pour l´affichage dans la fenêtre

GLvoid window_display()
{

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

 

 
   // if(preDraw)
   //  {
   //    glDrawBuffer(GL_FRONT);
   //    preDraw=false;
   //  }
   
   // else 
  glDrawBuffer(GL_BACK);
   
    
   
 

  
  glLoadIdentity();
  glScalef(zoom, zoom, zoom);

  if (mousegAc || mousemAc)
  {
    getOGLPosSai( mouseRecup.getX() , mouseRecup.getY() );
  }

  gestionRotation();




  if (mousegAc)
  {
    //Si j'ai un point sélectionné (ayant reçu un clic)
    if (clickSurPoint)
    {
      ptClicked->setX(mouseCord3D.getX());
      ptClicked->setY(mouseCord3D.getY());

    }
    else
    {
      if (TP1_2 == 1)
      {
        for (int i = 0 ; i < 4; i++)
        {
          if (mouseCord3D.getX() - 0.03 < cubiqueHermPoints[i].getX() &&
              mouseCord3D.getX() + 0.03 > cubiqueHermPoints[i].getX() &&
              mouseCord3D.getY() - 0.03 < cubiqueHermPoints[i].getY() &&
              mouseCord3D.getY() + 0.03 > cubiqueHermPoints[i].getY())
          {
            clickSurPoint = true;
            ptClicked = &cubiqueHermPoints[i];
            // cout << "Click sur points!" << endl;
          }
        }


        for (unsigned int i = 0 ; i < nbControlPoints; i++)
        {
          if (mouseCord3D.getX() - 0.03 < controlPoints[i].getX() &&
              mouseCord3D.getX() + 0.03 > controlPoints[i].getX() &&
              mouseCord3D.getY() - 0.03 < controlPoints[i].getY() &&
              mouseCord3D.getY() + 0.03 > controlPoints[i].getY())
          {
            clickSurPoint = true;
            ptClicked = &controlPoints[i];
            cout << "Click sur points!" << endl;
          }
        }

        if (mouseCord3D.getX() - 0.03 < p_casteljauv_pitch_y.getX() && mouseCord3D.getX() + 0.03 > p_casteljauv_pitch_y.getX() && mouseCord3D.getY() - 0.03 < p_casteljauv_pitch_y.getY() && mouseCord3D.getY() + 0.03 > p_casteljauv_pitch_y.getY())
        {
          clickSurPoint = true;
          ptClicked = &p_casteljauv_pitch_y;
          cout << "Click sur points!" << endl;
        }

        for ( int i = 0 ; i < 3; i++)
        {
          if (mouseCord3D.getX() - 0.03 < v[i].getX() &&
              mouseCord3D.getX() + 0.03 > v[i].getX() &&
              mouseCord3D.getY() - 0.03 < v[i].getY() &&
              mouseCord3D.getY() + 0.03 > v[i].getY())
          {
            clickSurPoint = true;
            ptClicked = &v[i];
            cout << "Click sur points!" << endl;
          }
        }
      }

      if (TP3 == 1)
      {
        clickSurPoint = clickedPoint(&p_casteljauv_pitch_x_y);
      }
    }
  }
  else
  {
    clickSurPoint = false;
  }


  if (TP3 == 1)
  {
    // for (int i = 0 ; i < nbControlPoints_i ; i ++)
    // {
    //   for (int j =0 ; j < nbControlPoints_j ; j ++)
    //   {
    //     clickedPoint(bsurface.getControlPoint(i,j));
    //   }
    // }


  }




  // C'est l'endroit où l'on peut dessiner. On peut aussi faire appel
  // à une fonction (render_scene() ici) qui contient les informations
  // que l'on veut dessiner
  render_scene();


  // trace la scène grapnique qui vient juste d'être définie
  // glFlush();

  glutSwapBuffers();
}

// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
  // possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est
  // de trop grosse taille par rapport à la fenêtre.
  glOrtho(-3.0, 3.0, -3.0, 3.0, -3.0, 3.0);

  // toutes les transformations suivantes s´appliquent au modèle de vue
  glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des événements clavier

GLvoid window_key(unsigned char key, int x, int y)
{
  switch (key) {
  case KEY_ESC:
    exit(1);
    break;

  default:
    printf ("La touche %d n´est pas active.\n", key);
    break;
  }
}



//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene()
{
  

  /* ================= TP1 - 2 ======================*/
  if (TP1_2 == 1)
  {
    glPointSize(5);
    glColor3f(1, 1, 1);

    for (int i = 0 ; i < 3 ; i++)
    {

      d.drawPoint(&v[i]);
    }
    d.setPoints(v , 3);
    d.drawCurve();

    glColor3f(0, 0, 1);
    Vector p_proj;
    v[0].projection(v[1], v[2], p_proj);
    d.drawPoint(&p_proj);


    glBegin(GL_POINTS);

    for (int i = 0 ; i < 4 ; i++)
    {
      glVertex3f(cubiqueHermPoints[i].getX(), cubiqueHermPoints[i].getY(), cubiqueHermPoints[i].getZ());
    }


    for (unsigned int i = 0 ; i < nbControlPoints ; i++)
    {
      glVertex3f(controlPoints[i].getX(), controlPoints[i].getY(), controlPoints[i].getZ());
    }


    glEnd();

    p_casteljauv_pitch_y.setX(1.6);
    if (p_casteljauv_pitch_y.getY() < 0)
    {
      p_casteljauv_pitch_y.setY(0);
    }
    if (p_casteljauv_pitch_y.getY() > 1)
    {
      p_casteljauv_pitch_y.setY(1);
    }
    d.drawPoint(&p_casteljauv_pitch_y);



    c_H.hermiteCubicCurve(&d);

    b.calcDataCurveByBernstein();
    b.drawCurveByBernstein(&d);


    b.calcDataBezierCurveByCasteljau((double)p_casteljauv_pitch_y.getY());
    b.drawCurveByCasteljau(&d);




  }

  if (TP3 == 1)
  {


    if (p_casteljauv_pitch_x_y.getX() < MIN_CASTEL_PITCH_X)
    {
      p_casteljauv_pitch_x_y.setX(MIN_CASTEL_PITCH_X);
    }
    if (p_casteljauv_pitch_x_y.getX() > MAX_CASTEL_PITCH_X)
    {
      p_casteljauv_pitch_x_y.setX(MAX_CASTEL_PITCH_X);
    }
    if (p_casteljauv_pitch_x_y.getY() < MIN_CASTEL_PITCH_Y)
    {
      p_casteljauv_pitch_x_y.setY(MIN_CASTEL_PITCH_Y);
    }
    if (p_casteljauv_pitch_x_y.getY() > MAX_CASTEL_PITCH_Y)
    {
      p_casteljauv_pitch_x_y.setY(MAX_CASTEL_PITCH_Y);
    }

    d.drawPoint(&p_casteljauv_pitch_x_y , 1 , 1 , 0);

    d.drawLine(&vso , &base1.getVi() );
    d.drawLine(&vso , &base1.getVj() );
    d.drawLine(&vso , &base1.getVk() );

    cout<<"MAIN : bsurface.drawSurfaceCasteljau"<<endl;
    bsurface.drawSurfaceCasteljau(10, 10, &d);
    

    
    cout<<"MAIN : bsurface.drawSurfaceControlPoints"<<endl;
    bsurface.drawSurfaceControlPoints(&d);

    bsurface.calcSurfacePointCasteljau(p_casteljauv_pitch_x_y.getX() - MIN_CASTEL_PITCH_X , p_casteljauv_pitch_x_y.getY() - MIN_CASTEL_PITCH_Y);
    cout<<"MAIN : bsurface.drawSurfacePointCasteljau"<<endl;
    bsurface.drawSurfacePointCasteljau(&d);
    
    // d.drawLine(&vso , &vsx);
    // d.drawLine(&vso , &vsy);
    // d.drawLine(&vso , &vsz);


    // cout<<"calcul avec base"<<endl;
    // bsurface.getControlPoint(0,0)->getBase()->getVi();
    // cout<<"drawSurface"<<endl;
    // d.drawBase(bsurface.getControlPoint(0,0)->getBase() , 0 , 1 , 0);

  }

  if( TP4 == 1)
  {
    
    sphere.affiche_surface_nord(&d);
    sphere.affiche_merid_lignes_nord(&d);

    sphere.affiche_surface_corps(&d);  
    sphere.affiche_parall_lignes_corps(&d);
    sphere.affiche_merid_lignes_corps(&d);

    sphere.affiche_surface_sud(&d);
    sphere.affiche_merid_lignes_sud(&d);

  }
  

  if( TP5 == 1)
  {

    // // voxel.draw(&d);    
    // // voxel.draw_lines(&d , 0 , 0 , 1);
    // voxel.octree(&sphere_voxel , &cilindre_voxel, 0 , 3, &d ,true , mode_voxel);
    // // sphere_voxel.affiche_surface_nord(&d);
    // // sphere_voxel.affiche_surface_corps(&d);  
    // // sphere_voxel.affiche_surface_sud(&d);

    // sphere_voxel.affiche_merid_lignes_nord(&d);
    // // sphere_voxel.affiche_parall_lignes_corps(&d);
    // sphere_voxel.affiche_merid_lignes_corps(&d);
    // sphere_voxel.affiche_merid_lignes_sud(&d);

    // cilindre_voxel.affiche_merid_lignes_nord(&d);
    // cilindre_voxel.affiche_parall_lignes_corps(&d);
    // cilindre_voxel.affiche_merid_lignes_corps(&d);
    // cilindre_voxel.affiche_merid_lignes_sud(&d);

  }
  /*======================================================*/


  if( TP7 == 1 )
  { 

//     GLfloat CubeArray[24] = {
//       (float)vso.getX(), (float)vso.getY(), (float)vso.getZ(),
//       (float)vsx.getX(), (float)vsx.getY(), (float)vsx.getZ(),
//       (float)vsy.getX(), (float)vsy.getY(), (float)vsy.getZ(),
//       (float)vsz.getX(), (float)vsz.getY(), (float)vsz.getZ(),
//       0.0f, 0.0f, 0.0f,
//       0.0f, 0.0f, 0.0f,
//       0.0f, 0.0f, 0.0f,
//       0.0f, 0.0f, 0.0f
//     };

//     // GLfloat CubeArray[24] = {
//     //   -1.0f, 1.0f, -1.0f,
//     //   -1.0f, -1.0f, -1.0f,
//     //   -1.0f, 1.0f, 1.0f,
//     //   -1.0f, -1.0f, 1.0f,
//     //   1.0f, 1.0f, 1.0f,
//     //   1.0f, -1.0f, 1.0f,
//     //   1.0f, 1.0f, -1.0f,
//     //   1.0f, -1.0f, -1.0f
//     // };

//     GLfloat ColorArray[24] = {
//       1.0f, 0.0f, 0.0f,
//       1.0f, 0.0f, 1.0f,
//       1.0f, 1.0f, 1.0f,
//       0.0f, 0.0f, 1.0f,
//       0.0f, 1.0f, 0.0f,
//       0.0f, 1.0f, 1.0f,
//       1.0f, 1.0f, 0.0f,
//       1.0f, 1.0f, 1.0f
//     };

//     GLuint IndiceArray[36] = {
//       0,1,2,0,1,3,
//       4,5,6,6,5,7,
//       3,1,5,5,1,7,
//       0,2,6,6,2,4,
//       6,7,0,0,7,1,
//       2,3,4,4,3,5
//     };

// // Génération des buffers
//     unsigned int CubeBuffers[2];
    
//     // Génération des buffers
//     glGenBuffers( 2, CubeBuffers );

//     // Buffer d'informations de vertex
//     glBindBuffer(GL_ARRAY_BUFFER, CubeBuffers[0]);
//     cout<<"sizeof(cubeArray) ="<<sizeof(CubeArray)<< 24 * sizeof(GLfloat) <<endl;
//     glBufferData(GL_ARRAY_BUFFER, sizeof(CubeArray), CubeArray, GL_STATIC_DRAW);

//     // Buffer d'indices
//     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, CubeBuffers[1]);
//     cout<<"sizeof(IndiceArray) ="<<sizeof(IndiceArray)<<" "<< 36 * sizeof(GLuint) <<endl;
//     glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IndiceArray), IndiceArray, GL_STATIC_DRAW);
  
//     // // Utilisation des données des buffers
//     glBindBuffer(GL_ARRAY_BUFFER, CubeBuffers[0]);
//     glVertexPointer( 3, GL_FLOAT, 6 * sizeof(float), ((float*)NULL + (3)) );
//     glColorPointer( 3, GL_FLOAT, 6 * sizeof(float), 0 );

//     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, CubeBuffers[1]);

//     // // Activation d'utilisation des tableaux
//     glEnableClientState( GL_VERTEX_ARRAY );
//     glEnableClientState( GL_COLOR_ARRAY );

//     // // Rendu de notre géométrie
//     glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

//     glDisableClientState( GL_COLOR_ARRAY );
//     glDisableClientState( GL_VERTEX_ARRAY );


    // cout<<"coucou"<<endl;
    // cilindre_triangles.affiche_tr_surface_nord(&d);
    // cilindre_triangles.affiche_tr_surface_corps(&d);  
    cilindre_triangles.affiche_tr_surface_sud(&d);  
    
    // cilindre_triangles.affiche_merid_lignes_nord(&d);
    // cilindre_triangles.affiche_parall_lignes_corps(&d);
    // cilindre_triangles.affiche_merid_lignes_corps(&d);
    // cilindre_triangles.affiche_merid_lignes_sud(&d);
  

    
    // tr_1->drawTrLignes(&d);
  }




  // // base 1
  d.drawLine(&vso , &vsx , 1.0 , 0.0 , 0.0);
  d.drawLine(&vso , &vsy , 0.0 , 1.0 , 0.0);
  d.drawLine(&vso , &vsz , 0.0 , 0.0 , 1.0);

}
