#ifndef UTILS_HPP
#define UTILS_HPP
#include "Vector.hpp"
#include "Base.hpp"
#include "Figure2D.hpp"
#include "Triangle.hpp"
#include "Maillage.hpp"
#include "VoxelG.hpp"
#include "Ligne.hpp"
#include "Camera.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <cmath>
#include <unistd.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
extern Base base0;
extern Base base1;

using namespace std;
class Utils 
{
private:
	
public:
	static void readOFFVoxels(Base *base , vector <VoxelG*> &voxels , vector <Ligne*> &lignes , vector<Camera*> &cameras, string nomFichierOFF);
};

template <typename M, typename V> 
void MapToVec( const  M & m, V & v );
#endif