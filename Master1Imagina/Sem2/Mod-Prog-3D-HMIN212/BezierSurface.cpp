#include "BezierSurface.hpp"

BezierSurface::BezierSurface(int nbControlPoints_i , int nbControlPoints_j)
{
	init( nbControlPoints_i , nbControlPoints_j);

	this->base[0] = new Vector(1,0,0);
	this->base[1] = new Vector(0,1,0);
	this->base[2] = new Vector(0,0,1);
}

BezierSurface::BezierSurface(int nbControlPoints_i , int nbControlPoints_j , Vector *vi , Vector *vj , Vector * vk)
{
	init( nbControlPoints_i , nbControlPoints_j);
	this->base[0] = vi;
	this->base[1] = vj;
	this->base[2] = vk;
}

BezierSurface::BezierSurface(int nbControlPoints_i , int nbControlPoints_j , BezierCurveByBernstein *bcurve)
{	
	init(nbControlPoints_i , nbControlPoints_j);
	this->bcurve = bcurve;
}

void BezierSurface::init(int nbControlPoints_i , int nbControlPoints_j)
{	

	int nbControlPoints_max ;
	this->nbControlPoints_i = nbControlPoints_i;
	this->nbControlPoints_j = nbControlPoints_j;

	if (nbControlPoints_i > nbControlPoints_j)
	{
		nbControlPoints_max = nbControlPoints_i;
	}
	else
	{
		nbControlPoints_max = nbControlPoints_j;
	}

	this->prof_k = nbControlPoints_max;
	this->tabControlPoints = new Vector**[ nbControlPoints_i];
	for (int i = 0 ; i < nbControlPoints_i ; i ++)
	{
		tabControlPoints[i] = new Vector * [nbControlPoints_j];
		
		for (int j = 0 ; j < nbControlPoints_j ; j++)
		{
			tabControlPoints[i][j] = new Vector[nbControlPoints_max];
			for(int k = 0 ; k < prof_k ; k ++)
			{
				tabControlPoints[i][j][k].setBase(base0);
			}
		}
	}
	this->bcurve = NULL;
}



void BezierSurface::setControlPoint(int i , int j , Vector &p_contr )
{
	this->tabControlPoints[i][j][0]=p_contr;	
}	

Vector* BezierSurface::getControlPoint(int i , int j)
{
	if ( i >= 0 && i < this->nbControlPoints_i && 
		 j >= 0 && j < this->nbControlPoints_j )
	{
		return &this->tabControlPoints[i][j][0];
	}
	else
	{
		return NULL;
	}
	
}


void BezierSurface::calcSurfacePointCasteljau(double u , double v)
{	
	if(this->nbControlPoints_i == this->nbControlPoints_j)
	{
		this->prof_attente = 0;
		for (int k = 0 ; k < this-> prof_k - 1 ; k ++)
		{
			for (int i = 0 ; i < this->nbControlPoints_i - 1 - k ; i++)
			{	
				for(int j = 0 ; j < this->nbControlPoints_j - 1 - k; j++)
				{

					Vector A =  ((1 - u) * tabControlPoints[ i   ][ j+1 ][k]) 
							   + (u      * tabControlPoints[ i+1 ][ j+1 ][k]);


					Vector B =  ((1 - u) * tabControlPoints[ i   ][ j   ][k]) 
							   + (u      * tabControlPoints[ i+1 ][ j   ][k]);

					tabControlPoints[i][j][k + 1 ] = 
								((1 - v) * B) + (v*A);
				}	
			}
			this->prof_attente++;
		}
	}

	else //m != n 
	{
		this->prof_attente=0;
		for(int k = 0 ;   
				(k < this->nbControlPoints_i - 1) 
			  &&(k < this->nbControlPoints_j - 1) ; 	
			  k ++)
		{
			for (int i = 0 ; i < this->nbControlPoints_i - 1 - k ; i++)
			{	
				for(int j = 0 ; j < this->nbControlPoints_j - 1 - k; j++)
				{

					Vector A =  ((1 - u) * tabControlPoints[ i   ][ j+1 ][k]) 
							   + (u      * tabControlPoints[ i+1 ][ j+1 ][k]);


					Vector B =  ((1 - u) * tabControlPoints[ i   ][ j   ][k]) 
							   + (u      * tabControlPoints[ i+1 ][ j   ][k]);

					tabControlPoints[i][j][k + 1 ] = 
								((1 - v) * B) + (v*A);
				}	
			}
			this->prof_attente++;
		}
		// Ligne de points de contrôle généré en dernier quand m != n
		// (Nombre de points colonne != nb de points ligne) 
		unsigned int sizelastControlPoints =this->prof_k - this->prof_attente;

		Vector lastControlPoints[sizelastControlPoints];

		// Extraction de colonne ou ligne en fonction de où 
		// se situent les derniers points de contrôle à traiter 
		// (De tabControlPoints 2D vers lastControlPoints 1D)
		float w;
		if( this->nbControlPoints_i > this->nbControlPoints_j)
		{
			for (unsigned int i = 0 ; i < sizelastControlPoints ; i ++)
			{
				lastControlPoints[i] = tabControlPoints[i][0][this->prof_attente];
			}
			w=u;
		}
		else
		{
			for (unsigned int j = 0 ; j < sizelastControlPoints ; j ++)
			{
				lastControlPoints[j] = tabControlPoints[0][j][this->prof_attente];
			}
			w=v;
		}
		
		bcurve->setPointsControl(lastControlPoints  , sizelastControlPoints);
		bcurve->calcDataBezierCurveByCasteljau(w);

		
	}
}

Vector* BezierSurface::getPointSurfaceCasteljau()
{
	if(this->nbControlPoints_i != this->nbControlPoints_j)
		return bcurve->getPointCurveCasteljau(); 
	
	else
		return &tabControlPoints[ 0 ][ 0 ][this->prof_attente];
}


void BezierSurface::drawSurfacePointCasteljau(Draw *d)
{
	
	//Ilustration visuel de l'algorithme de casteljau
	Vector *carre[5];
	for (int k = 0 ; k <= this->prof_attente ; k ++)
	{
		for (int i = 0 ; i < this->nbControlPoints_i - 1 - k ; i++)
		{	
			for(int j = 0 ; j < this->nbControlPoints_j - 1 - k; j++)
			{
				
				carre[0]=&tabControlPoints[i  ][j  ][k];
				carre[1]=&tabControlPoints[i  ][j+1][k];
				carre[2]=&tabControlPoints[i+1][j+1][k];
				carre[3]=&tabControlPoints[i+1][j  ][k];
				carre[4]=&tabControlPoints[i  ][j  ][k];

				d->drawCurve(carre , 5 ,base1 , 0 , 1 , (float)(this->prof_attente - k ) / this->prof_attente);				
				
			}
		}
	}
	
	
	Vector *p_surface = this->getPointSurfaceCasteljau();
	
	if(this->nbControlPoints_i != nbControlPoints_j)
	{
		bcurve->drawCurveByCasteljau(d); 
		d->drawPoint(p_surface , &base1 , 0 , 1 , 0 );

	}
	else
	{
		d->drawPoint(p_surface , &base1 , 0 , 1 , 0 );
	}
	
}

void BezierSurface::drawSurfaceControlPoints(Draw *d)
{
	for (int i = 0 ; i < this->nbControlPoints_i ; i ++)
	{
		for (int j = 0 ; j < this -> nbControlPoints_j ; j++)
		{	
			tabControlPoints[i][j][0].setBase(base1);
			d->drawPoint(&tabControlPoints[i][j][0] , 0 , 0 , 1);
			tabControlPoints[i][j][0].setBase(base0);
		}
	}
}


void BezierSurface::drawSurfaceCasteljau(unsigned int nbU , unsigned int nbV , Draw *d)
{
	Vector pointsCurveV[nbV];
	Vector pointsCurveU[nbU];
	for(unsigned  int i = 0 ; i < nbU  ; i ++)
	{
		for (unsigned int j = 0 ; j < nbV ; j++)
		{	
			this->calcSurfacePointCasteljau(i / (nbU - 1.0) , j / (nbV - 1.0));
			pointsCurveV[j]= *(this->getPointSurfaceCasteljau());
			

		}
		d->drawCurve(pointsCurveV,nbV,base1,0.5,0.5,0.5);
		
	}

	for (unsigned int i = 0 ; i < nbV ; i ++)
	{
		for( unsigned int j = 0 ; j < nbU ; j++)
		{
			this->calcSurfacePointCasteljau(j / (nbU - 1.0) , i / (nbV - 1.0));
			pointsCurveU[j]=*(this->getPointSurfaceCasteljau());
			
		}
		d->drawCurve(pointsCurveU,nbU,base1,0.5,0.5,0.5);
	}
}


