#ifndef VUE_HPP
#define VUE_HPP
#include <iostream>
// #include "GL/freeglut.h"
// #include "GL/gl.h"
// #include "GL/glu.h"
#include "Vector.hpp"
#include <math.h>
# define M_PI           3.14159265358979323846
using namespace std;
void getOGLPosSai(int x, int y);
bool clickedPoint(Vector *point);
bool clickedPointFromArray(Vector *point, int n);
void determineAxeRotation();
void gestionRotation();
#endif