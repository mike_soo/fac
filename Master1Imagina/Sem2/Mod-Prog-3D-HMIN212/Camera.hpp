#ifndef CAMERA_HPP
#define CAMERA_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "Base.hpp"
#include "Ligne.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <unistd.h>
extern Base base0;
extern Base base1;

class Camera 
{
private:
	Ligne *ligne1 , *ligne2 , *ligne3;
public:
	Camera( Ligne *ligne1 , Ligne *ligne2 , Ligne *ligne3);
	void draw(Draw *d) ;
};

// ostream& operator<<(ostream& os, const Ligne& Ligne);  
#endif