#ifndef BASE_HPP
#define BASE_HPP

#include "Base.hpp"
class Vector;
class Base {
private:
    Vector *vi,*vj,*vk,*o;
public:
    Base();
    Base(Vector &o , Vector &vi, Vector &vj , Vector &vk);    
    Vector& getO()  const;
    Vector& getVi() const;
    Vector& getVj() const;
    Vector& getVk() const;
};

#endif
