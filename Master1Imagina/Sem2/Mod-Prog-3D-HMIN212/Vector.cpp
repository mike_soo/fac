 
#include <cmath>

#include "Vector.hpp"
#include "Base.hpp"

Vector::Vector() : Vector(0,0,0)
{
    
}

Vector::Vector(const double x, const double y, const double z) : _x(x), _y(y), _z(z) 
{

    this->pairsDeSubdivision = new map<unsigned long long int , Vector*>();
    this->parentsDeSubdivision= new map<unsigned long long int , Vector*>();
    this->parentsDeSubdivisionOrd = new vector <Vector*>();
    this->idsMaillages = new set<unsigned long long int> ();
    this->base = NULL; 
    this->initId();
    // cout<<"id =="<<this->id<<endl;

}

Vector::Vector(const double x, const double y , const double z , Base& base) : Vector( x, y ,z )
{
    

    this->base = &base;
    
}

Vector::Vector(const Vector& vec) {
    
    this->base = vec.getBase(); 
   
    _x = vec.getXr();
    _y = vec.getYr();
    _z = vec.getZr();
}

double Vector::norme() const {
    return sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2));
}

void Vector::normalize() {
    double n = norme();
    _x = _x / n;
    _y = _y / n;
    _z = _z / n;
}

double Vector::scalar(const Vector& vec) const {
    return (_x * vec.getXr()) + (_y * vec.getYr()) + (_z * vec.getZr());
}

Vector Vector::cross(const Vector& vec) const {
    double x = _y * vec.getZr() - _z * vec.getYr();
    double y = _z * vec.getXr() - _x * vec.getZr();
    double z = _x * vec.getYr() - _y * vec.getXr();

    return Vector(x, y, z);
}

void Vector::projection(const Vector& b , const Vector& c , Vector& p_proj)
{
    Vector vBA = *this - b;
    Vector vBC = c - b;


    double normeBAp = (vBA.scalar(vBC)) / vBC.norme();
    vBC.normalize();
    
    p_proj.setX(b.getXr() + (vBC.getXr() * normeBAp));
    p_proj.setY(b.getYr() + (vBC.getYr() * normeBAp));
    p_proj.setZ(b.getZr() + (vBC.getZr() * normeBAp));
    
}

double Vector::getTest()const
{
    return 3.5;
}


double Vector::getXr() const 
{
    return this->_x;   
}

double Vector::getYr() const 
{
    return this->_y;   
}

double Vector::getZr() const 
{
       return this->_z;   
}

double Vector::getX() const 
{
    // cout<<"\t\t"<<"getX()"<<endl;
    if (base== NULL)
    {

        return this->_x;   
    }
          // cout<<"x * base->getVi())"<<endl;
          // cout<<(_x * base->getVi())<<endl;
          // cout<<"y * base->getVj())"<<endl;
          // cout<<(_y * base->getVj())<<endl;
          // cout<<"z * base->getVk()).getX()"<<endl;
          // cout<<(_z * base->getVk()).getX()<<endl;


    return ((_x * base->getVi())
          +(_y * base->getVj())
          +(_z * base->getVk())).getX();
}

double Vector::getY() const 
{
    // cout<<"\t"<<"getY()"<<endl;
    // if(base == NULL)
    // {
    //     cout<<"\tNULL;"<<endl;
    // }
    // else
    // {
    //     cout<<"\t not null"<<endl;
    // }
    if (base== NULL)
    {
        return this->_y;   
    }
    return ((_x * base->getVi())
          +(_y * base->getVj())
          +(_z * base->getVk())).getY();
}

double Vector::getZ() const 
{
    // cout<<"\t\t"<<"getZ()"<<endl;
    if (base== NULL)
    {   
        return this->_z;   
    }
    return ((_x * base->getVi())
          +(_y * base->getVj())
          +(_z * base->getVk())).getZ();
}



Base* Vector::getBase() const{
    if (base == NULL)
        return NULL;
    return (this->base);
}

void Vector::setX(const double x) {
    _x = x;
}

void Vector::setY(const double y) {
    _y = y;
}

void Vector::setZ(const double z) {
    _z = z;
}

void Vector::setBase(Base& base)
{
    this->base = &base;
}

Vector& Vector::operator=(const Vector& vec)
{
    _x = vec.getXr();
    _y = vec.getYr();
    _z = vec.getZr();
    this->base = vec.getBase();
    return *this;
}

Vector& Vector::operator+=(const Vector& vec) {
    _x += vec.getXr();
    _y += vec.getYr();
    _z += vec.getZr();

    return *this;
}

Vector& Vector::operator-=(const Vector& vec) {
    _x -= vec.getXr();
    _y -= vec.getYr();
    _z -= vec.getZr();

    return *this;
}

Vector& Vector::operator*=(const double lambda) {
    _x *= lambda;
    _y *= lambda;
    _z *= lambda;

    return *this;
}

Vector& Vector::operator/=(const double lambda) {
    _x /= lambda;
    _y /= lambda;
    _z /= lambda;

    return *this;
}

Vector operator+(const Vector& vec1, const Vector& vec2) {
    Vector copy = Vector(vec1);
    copy += vec2;
    return copy;
}

Vector operator-(const Vector& vec1, const Vector& vec2) {
    Vector copy = Vector(vec1);
    copy -= vec2;
    return copy;
}

Vector operator*(const Vector& vec1, const double lambda) {
    Vector copy = Vector(vec1);
    copy *= lambda;
    return copy;
}

Vector operator*(const double lambda, const Vector& vec1) {
    Vector copy = Vector(vec1);
    copy *= lambda;
    return copy;
}

double operator*( const Vector& vec1 , const Vector& vec2) {
    return vec1.scalar(vec2);
} 

Vector operator/(const Vector& vec1, const double lambda) {
    Vector copy = Vector(vec1);
    copy /= lambda;
    return copy;
}

Vector operator/(const double lambda, const Vector& vec1) {
    Vector copy = Vector(vec1);
    copy /= lambda;
    return copy;
}
//TODO rajouter comparaisons des bases pour plus de
//sécurité 
bool operator==(const Vector &v1 , const Vector &v2)
{
    return v1.getX() == v2.getX() &&
           v1.getY() == v2.getY() &&
           v1.getZ() == v2.getZ();
}

bool operator!= (const Vector &v1 , const Vector &v2)
{
    return !(v1 == v2);
}

vector <Figure2D*> * Vector::getAllFigures2D()
{
    return this->figures2D;
}


void Vector::addFigure2D(Figure2D* fig2D)
{
    if(this->figures2D == NULL)
        this->figures2D = new vector <Figure2D*> ();
    
    this->figures2D->push_back(fig2D);
}

void Vector::clearFigures2D()
{
    if(this->figures2D != NULL)
        this->figures2D->clear();
    // else
    //     cerr<<"Warning clearing none existing figures"<<endl;
}



set <Vector*> *Vector::getSommetsCommunsDansTrianglesAdjacents2D(Vector * p)
{
    set <Vector *> * sommetsCommuns = new set <Vector*> ();
    if(this->getAllFigures2D() == NULL || p->getAllFigures2D() == NULL)
        return sommetsCommuns;
    for (unsigned int i = 0 ; i < this->getAllFigures2D()->size() ; i++ )
    {
        for (int j = 0 ; j < 3 ; j++)
        {

            for(unsigned int ii = 0 ; ii < p->getAllFigures2D()->size();ii++)
            {
    
                for(int jj = 0 ; jj< 3 ; jj++)
                {
        
                    if(*(this->getAllFigures2D()->at(i)->getPoint(j)) 
                            == *(p->getAllFigures2D()->at(ii)->getPoint(jj)) && 
                          *(this->getAllFigures2D()->at(i)->getPoint(j)) 
                            != *(this)
                          && 
                          *(p->getAllFigures2D()->at(ii)->getPoint(jj)) 
                            != *(p))
                    {
                        sommetsCommuns->insert(this->getAllFigures2D()->at(i)->getPoint(j));
                    }
                    
                }
            }
        }
    }

    return sommetsCommuns;
}



void Vector::affichePoint(Draw * d , float r, float g , float b){
    d->drawPoint(this , r , g , b);
}

void Vector::initId()
{
    this->myid = Vector::giveId();
}
 
unsigned long long int Vector::getId() const
{
     return myid;
}

unsigned long long int Vector::id=-1;

unsigned long long int Vector::giveId()
{
    ++Vector::id;
    return Vector::id;
}

float Vector::distance(Vector *p)
{
    if(p==NULL)
    {
        cerr<<"Vector::distance: p == NULL"<<endl;
        return 0.0;
    }
    return 
       sqrt( 
            pow(p->getX() - this->getX() , 2) +
            pow(p->getY() - this->getY() , 2) +
            pow(p->getZ() - this->getZ() , 2)
            );
}


ostream& operator<<(ostream& os, const Vector& v)  
{  
    os <<v.getId()<<"  "<< v.getX() << "  " << v.getY() << "  " << v.getZ();;  
    return os;  
}  


void Vector::estDansVoxelG(VoxelG* voxelG)
{
    this->voxelG = voxelG;
}
VoxelG* Vector::getVoxel()
{
    return this->voxelG;
}