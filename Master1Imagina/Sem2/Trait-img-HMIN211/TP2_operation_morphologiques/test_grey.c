// test_couleur.cpp : seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cnomimglue[250], cnomimgecrite[250];
  int nh, nw, ntaille, s;

  if (argc != 4)
  {
    printf("usage: imagein.pgm imageout.pgm seuillll \n");
    exit (1) ;
  }

  sscanf (argv[1], "%s", cnomimglue) ;
  sscanf (argv[2], "%s", cnomimgecrite);
  sscanf (argv[3], "%d", &s);

  OCTET *imgin, *imgout;

  lire_nb_lignes_colonnes_image_pgm(cnomimglue, &nh, &nw);
  ntaille = nh * nw;

  allocation_tableau(imgin, octet, ntaille);
  lire_image_pgm(cnomimglue, imgin, nh * nw);
  allocation_tableau(imgout, octet, ntaille);

  //   for (int i=0; i < ntaille; i++)
  // {
  //  if ( imgin[i] < s) imgout[i]=0; else imgout[i]=255;
  //  }


  for (int i = 0; i < nh; i++)
  {
    for (int j = 0; j < nw; j++)
    {
      if ( imgin[i * nw + j] < s) imgout[i * nw + j] = 0; else imgout[i * nw + j] = 255;
    }
  }
  ecrire_image_pgm(cnomimgecrite, imgout,  nh, nw);
  free(imgin);
  return 1;
}
