#include "lib.h"
#include "image_ppm.h"
int main(int argc, char* argv[])
{
	char cNomImgLue[250], cNomImgEcrite[250];
	int nb_lig_im, nb_col_im, nTaille;

	if (argc != 3)
	{
		printf("Usage: ImageIn.ppm ImageOut.ppm \n");
		exit (1) ;
	}

	sscanf (argv[1], "%s", cNomImgLue) ;
	sscanf (argv[2], "%s", cNomImgEcrite);


	OCTET *ImgIn1D, ***ImgIn3D, *ImgOut1D , ***ImgOut3D;


	lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nb_lig_im, &nb_col_im);
	nTaille = nb_lig_im * nb_col_im;
 
	allocation_tableau(ImgIn1D, OCTET, nTaille * 3);
	lire_image_ppm(cNomImgLue, ImgIn1D, nb_lig_im * nb_col_im );
	allocation_tableau(ImgOut1D, OCTET, nTaille * 3);
	
	ImgIn3D = allocation_tableau3D(nb_lig_im,nb_col_im);
	ImgOut3D = allocation_tableau3D(nb_lig_im,nb_col_im);

	conv1Dvers3D_coul( ImgIn1D , ImgIn3D , nb_lig_im , nb_col_im);

	erosion_coul(ImgIn3D, ImgOut3D , nb_lig_im , nb_col_im );
	
	conv3Dvers1D_coul( ImgOut1D, ImgOut3D, nb_lig_im ,nb_col_im);

	
	ecrire_image_ppm(cNomImgEcrite, ImgOut1D,  nb_lig_im, nb_col_im);
	free(ImgIn1D);
	exit(0);
}
