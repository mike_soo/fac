// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB, Sr_max, Sg_max, Sb_max,Sr_min, Sg_min, Sb_min;
  
  if (argc != 9) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm SeuilR_max SeuilG_max SeuilB_max SeuilR_min SeuilG_min SeuilB_min \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&Sr_max);
   sscanf (argv[4],"%d",&Sg_max);
   sscanf (argv[5],"%d",&Sb_max);
   sscanf (argv[6],"%d",&Sr_min);
   sscanf (argv[7],"%d",&Sg_min);
   sscanf (argv[8],"%d",&Sb_min);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
  
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ImgOut[i]=nR;
       ImgOut[i+1]=nG;
       ImgOut[i+2]=nB;
       if (nR > Sr_max) ImgOut[i]=Sr_max;
       if (nR < Sr_min) ImgOut[i]=Sr_min;

       if (nG > Sg_max) ImgOut[i+1]=Sg_max;
       if (nG < Sg_min) ImgOut[i+1]=Sg_min;

       if (nB > Sb_max) ImgOut[i+2]=Sb_max;
       if (nB < Sb_min) ImgOut[i+2]=Sb_min;

     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}
