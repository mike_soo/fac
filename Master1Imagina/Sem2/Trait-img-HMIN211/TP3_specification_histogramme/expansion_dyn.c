// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  OCTET Vmin, Vmax;
  unsigned int V1, V0;
  char cNomImgLue[250], cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 3)
  {
    printf("Rentrez imageLue imageEcr\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  sscanf (argv[2], "%s", cNomImgEcrite) ;

  OCTET *ImgIn, * ImgOut;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig, &nb_col);
  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille);
  allocation_tableau(ImgOut, OCTET, nTaille);

  lire_image_pgm(cNomImgLue, ImgIn, nb_col * nb_lig);
  unsigned int H[256] , H_expand[256];

  for (int i = 0 ; i < 256 ; i++)
  {
    H[i] = 0;
    H_expand[i] = 0;
  }

  for (int i = 0 ; i < nTaille ; i ++)
  {
    H[ImgIn[i]]++;
  }

  double d , g;
  short int first_val = 0;

  for (unsigned int i = 0; i < 256 ; i ++)
  {
    if (H[i] > 0)
    {
      V1 = i;
    }
    if (H[i] > 0 && first_val ==  0)
    {

      first_val = 1;
      V0 = i;
    }

  }

  Vmin = 0;
  Vmax = 255;
  d = (double)((Vmin * V1) - (Vmax * V0)) / (V1 - V0);
  g = (double)(Vmax - Vmin) / (V1 - V0);

  


  for (int i = 0 ; i < nTaille ; i ++)
  {
    //bnazarian
    // ImgOut[i] = (g * ImgIn[i]) + d;
    
    // tangi
    // ImgOut[i] =(OCTET) (255 / (Vmax - Vmin)) * (ImgIn[i] - Vmin);
    
    // mti-egal histo
    ImgOut[i] = (OCTET) round( 255 * (ImgIn[i] - V0) / (V1 - V0));
  }

  for (int i = 0 ; i < nTaille ; i ++)
  {
    H_expand[ImgOut[i]]++;
  }
  printf("Val \"histogramme original\" \"histogramme expansion dynamique\" \n");
  for (int i = 0 ; i < 256 ; i ++)
  {
    printf("%u %u %u\n", i , H[i], H_expand[i]);
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut , nb_lig , nb_col);

  printf("#alpha = %f , betha =%f", d , g);

  free(ImgIn);
  return 1;
}
