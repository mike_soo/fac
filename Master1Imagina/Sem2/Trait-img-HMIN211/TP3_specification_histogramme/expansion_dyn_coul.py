#!/usr/bin/env python3
import subprocess
import sys

if __name__ == "__main__":
	img_name = sys.argv[1]
	img_no_ext_name = img_name[:-4]
	ext_img = ".ppm"
	ext_data = ".dat"
	img_name_out  = img_no_ext_name + "_exp_dyn_coul" + ext_img
	hist_dat_name = img_no_ext_name + "_exp_dyn_hist_coul" + ext_data
	plot_plg = "plot-hist-coul.plg"
	c_prog = "./expansion_dyn_coul"

	cmd1=[]
	cmd1.append(c_prog)
	cmd1.append(img_name)
	cmd1.append(img_name_out)
	with open(hist_dat_name, "w") as outfile:
		subprocess.call(cmd1, stdout=outfile)
	
	cmd2 =[]
	cmd2.append("gnuplot")
	cmd2.append("-e")
	cmd2.append("filename=\""+ hist_dat_name +"\"")
	cmd2.append(plot_plg)
	subprocess.call(cmd2)
	