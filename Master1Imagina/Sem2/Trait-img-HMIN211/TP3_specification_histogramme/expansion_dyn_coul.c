// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  OCTET VminR=0,VminG=0,VminB=0,VmaxR=255,VmaxG=255,VmaxB=255;
  unsigned int V1_R , V1_G , V1_B,V0_R , V0_G , V0_B;
  char cNomImgLue[250], cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 3)
  {
    printf("Rentrez imageLue imageEcr\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  sscanf (argv[2], "%s", cNomImgEcrite) ;
  
  OCTET *ImgIn, * ImgOut;
  
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nb_lig, &nb_col);
  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  printf("#nTaille =%i\n",nTaille);
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille*3);
  allocation_tableau(ImgOut, OCTET, nTaille*3);
  
  lire_image_ppm(cNomImgLue, ImgIn, nb_col * nb_lig);
  unsigned int H[256][3] , H_expand[256][3];

  for (int i = 0 ; i < 256 ; i++)
  {
    H[i][0]=0;
    H[i][1]=0;
    H[i][2]=0;
    H_expand[i][0]=0;
    H_expand[i][1]=0;
    H_expand[i][2]=0;
  }
  VmaxR = VmaxG = VmaxB = 0;
  VminR = VminG = VminB = 255;
  V0_R = 0;
  V1_R = 0;
  V0_G = 0;
  V1_G = 0;
  V0_B = 0;
  V1_B = 0;
  for (int i = 0 ; i < nTaille * 3 ; i +=3)
  {
    
    H[ImgIn[i]][0]++;
    H[ImgIn[i+1]][1]++;
    H[ImgIn[i+2]][2]++;

  }

 
  short int first_val_R = 0 , first_val_G = 0 , first_val_B = 0;
  
  for (unsigned int i= 0; i < 256 ; i ++)
  {
    if(H[i][0] > 0)
    {
      V1_R = i;
    }
    if(H[i][1] > 0)
    {
      V1_G = i;
    }
    if(H[i][2] > 0)
    {
      V1_B = i;
    }
    
    if(H[i][0] > 0 && first_val_R ==  0)
    {
      
      first_val_R = 1;
      V0_R = i;
    }
    if(H[i][1] > 0 && first_val_G ==  0)
    {
      
      first_val_G = 1;
      V0_G = i;
    }
    if(H[i][2] > 0 && first_val_B ==  0)
    {
      
      first_val_B = 1;
      V0_B = i;
    }

  }


  // d= (double)((Vmin * V1) - (Vmax * V0))/ (V1 - V0);
  // g= (double)(Vmax - Vmin) / (V1 - V0);

  printf("#V0_R = %i\n",V0_R);
  printf("#V1_R = %i\n",V1_R);

  printf("#V0_G = %i\n",V0_G);
  printf("#V1_G = %i\n",V1_G);

  printf("#V0_B = %i\n",V0_B);
  printf("#V1_B = %i\n",V1_B);


  for (int i = 0 ; i < nTaille*3 ; i +=3)
  {
    
    ImgOut[i]   = (OCTET) round((double)255 * (ImgIn[i]   - V0_R) /(V1_R - V0_R));
    ImgOut[i+1] = (OCTET) round((double)255 * (ImgIn[i+1] - V0_G) /(V1_G - V0_G));
    ImgOut[i+2] = (OCTET) round((double)255 * (ImgIn[i+2] - V0_B) /(V1_B - V0_B));
  }

  for (int i = 0 ; i < nTaille*3 ; i +=3)
  {
    H_expand[ImgOut[i]][0]++;
    H_expand[ImgOut[i+1]][1]++;
    H_expand[ImgOut[i+2]][2]++;
  }
  printf("Val \"R avant expansion\" \"G avant expansion\" \"B avant expansion\" \"R après expansion\" \"G après expansion\" \"B après expansion\"\n");

  for (int i = 0 ; i < 256 ; i ++)
  {
    printf("%i %u %u %u %u %u %u\n", i , H[i][0] , H[i][1] , H[i][2], H_expand[i][0] , H_expand[i][1] , H_expand[i][2]);
  }

  double d_r,g_r, d_g,g_g, d_b,g_b;

  d_r = (double) ((VminR * V1_R) - (VmaxR * V0_R)) / (V1_R - V0_R);
  g_r = (double) (VmaxR - VminR) / (V1_R - V0_R);

  d_g = (double) ((VminG * V1_G) - (VmaxG * V0_G)) / (V1_G - V0_G);
  g_g = (double) (VmaxG - VminG) / (V1_G - V0_G);

  d_b = (double) ((VminB * V1_B) - (VmaxB * V0_B)) / (V1_B - V0_B);
  g_b = (double) (VmaxB - VminB) / (V1_B - V0_B);


  printf("# alpha_r =%f , betha_r=%f , alpha_g =%f , betha_g=%f , alpha_b=%f , betha_b=%f",d_r,g_r , d_g,g_g , 
    d_b,g_b);
  ecrire_image_ppm(cNomImgEcrite, ImgOut , nb_lig ,nb_col);

  
  
  free(ImgIn);
  return 1;
}
