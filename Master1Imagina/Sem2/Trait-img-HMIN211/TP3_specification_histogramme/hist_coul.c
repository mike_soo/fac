// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  
  char cNomImgLue[250], cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 2)
  {
    printf("Rentrez imageLue\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  
  
  OCTET *ImgIn, * ImgOut;
  
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nb_lig, &nb_col);
  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  printf("#nTaille =%i\n",nTaille);
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille*3);
  
  lire_image_ppm(cNomImgLue, ImgIn, nb_col * nb_lig);
  unsigned int H[256][3];

  for (int i = 0 ; i < 256 ; i++)
  {
    H[i][0]=0;
    H[i][1]=0;
    H[i][2]=0;
  }
  for (int i = 0 ; i < nTaille * 3 ; i +=3)
  {
    H[ImgIn[i]][0]++;
    H[ImgIn[i+1]][1]++;
    H[ImgIn[i+2]][2]++;
  }
  printf("pixel Rouge Vert Bleu\n");
  for (int i = 0 ; i < 256 ; i ++)
  {
    printf("%i %u %u %u\n", i , H[i][0] , H[i][1] , H[i][2]);
  }
  
  
  free(ImgIn);
  return 1;
}


