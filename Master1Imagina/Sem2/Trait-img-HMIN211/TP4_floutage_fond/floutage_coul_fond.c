// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#include "lib.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  char cNomImgLueCoul[250], cNomImgLueBin[250], cNomImgEcrite[250];
  int nb_col_coul, nb_lig_coul , nb_col_bin, nb_lig_bin, nTailleCoul, nTailleBin;

  if (argc != 4)
  {
    printf("Rentrez imageCouleurFlouage imageBinaireFond imageEcr\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLueCoul);
  sscanf (argv[2], "%s", cNomImgLueBin);
  sscanf (argv[3], "%s", cNomImgEcrite);
  
  OCTET *ImgInCoul , *ImgInBin, * ImgOut;
  
  lire_nb_lignes_colonnes_image_ppm(cNomImgLueCoul, &nb_lig_coul, &nb_col_coul);
  lire_nb_lignes_colonnes_image_pgm(cNomImgLueBin, &nb_lig_bin , &nb_col_bin);
  nTailleCoul = nb_col_coul * nb_lig_coul;
  nTailleBin = nb_col_bin * nb_lig_bin;
  
  allocation_tableau(ImgInCoul, OCTET, nTailleCoul*3)  ;
  allocation_tableau(ImgOut, OCTET, nTailleCoul*3) ;
  allocation_tableau(ImgInBin , OCTET , nTailleBin);

  OCTET *** ImgInCoul3D  = allocation_tableau3D(nb_lig_coul , nb_col_coul) ;
  OCTET *** ImgOut3D = allocation_tableau3D(nb_lig_coul , nb_col_coul) ;

  lire_image_ppm(cNomImgLueCoul, ImgInCoul, nb_col_coul * nb_lig_coul);
  lire_image_pgm(cNomImgLueBin , ImgInBin , nb_col_bin  * nb_lig_bin);
  
  conv1Dvers3D_coul(ImgInCoul  , ImgInCoul3D  , nb_lig_coul , nb_col_coul);
  printf("floutage_soul\n");

  floutage_coul(ImgInCoul3D , ImgInBin , ImgOut3D , nb_lig_coul , nb_col_coul , 3);
  printf("floutage_soul end\n");
  
  conv3Dvers1D_coul(ImgOut , ImgOut3D , nb_lig_coul , nb_col_coul);

 
  ecrire_image_ppm(cNomImgEcrite, ImgOut , nb_lig_coul , nb_col_coul);

  
  
  
  return 1;
}
