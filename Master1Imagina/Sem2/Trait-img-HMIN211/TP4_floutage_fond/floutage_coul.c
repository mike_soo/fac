// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#include "lib.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 3)
  {
    printf("Rentrez imageLue imageEcr\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue);
  sscanf (argv[2], "%s", cNomImgEcrite);
  
  OCTET *ImgIn, * ImgOut;
  
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nb_lig, &nb_col);
  
  nTaille = nb_col * nb_lig;
  
  allocation_tableau(ImgIn, OCTET, nTaille*3)  ;
  allocation_tableau(ImgOut, OCTET, nTaille*3) ;
  OCTET *** ImgIn3D  = allocation_tableau3D(nb_lig , nb_col) ;
  OCTET *** ImgOut3D = allocation_tableau3D(nb_lig , nb_col) ;

  lire_image_ppm(cNomImgLue, ImgIn, nb_col * nb_lig);

  
  conv1Dvers3D_coul(ImgIn  , ImgIn3D  , nb_lig , nb_col);
  printf("floutage_soul\n");

  floutage_coul(ImgIn3D    , ImgOut3D , nb_lig , nb_col , 3);
  printf("floutage_soul end\n");
  
  conv3Dvers1D_coul(ImgOut , ImgOut3D , nb_lig , nb_col);

 
  ecrire_image_ppm(cNomImgEcrite, ImgOut , nb_lig , nb_col);

  
  
  free(ImgIn);
  return 1;
}
