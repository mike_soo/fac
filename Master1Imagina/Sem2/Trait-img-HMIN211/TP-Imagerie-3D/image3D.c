#include "image3D.h"
#include <errno.h>
#include <string.h>
#include <cmath>
#include "lib.h"

// unsigned long long int getInd(unsigned long int i , unsigned long int j , unsigned long k)
// {


// }


int main(int argc, char* argv[])
{

	char nomFichier[250];
	unsigned int dimX, dimY, dimZ;


	if (argc != 5)
	{
		printf("Rentrez nomFichier dimX dimY dimZ\n");
		return -1 ;
	}
	sscanf (argv[1], "%s", nomFichier) ;
	sscanf (argv[2], "%u", &dimX) ;
	sscanf (argv[3], "%u", &dimY) ;
	sscanf (argv[4], "%u", &dimZ) ;
	FILE *f = fopen(nomFichier, "r");
	if(f == NULL)
	{
		printf("erreur lors d'ouverture du fichier : %s \n" ,strerror(errno));
		return 0;
	} 


	unsigned long long int sizeofImg= dimX * dimY * dimZ;
	
	// unsigned short int bufferFile*** = malloc(dimX* )

	unsigned short int *bufferFileTemp =(unsigned short int *) malloc(sizeofImg * sizeof(unsigned short int));

	// unsigned short bufferFileTemp[sizeofImg];
	
	

	printf("allocation works\n");	
	
	fread(bufferFileTemp , sizeof(unsigned short) , sizeofImg , f );

	printf("researching max min\n");
	unsigned short int max = 0, min = pow(2,16) - 1;

	int j = 0;

	for(unsigned long long int i = 0 ; i < sizeofImg ; i ++)
	{
		bufferFileTemp[i] = _bswap16(bufferFileTemp[i]);
		
	}

	for(unsigned long long int i = 0 ; i < sizeofImg ; i ++)
	{
		if(bufferFileTemp[i] > max)
			max = bufferFileTemp[i];
		if(bufferFileTemp[i]< min)
			min = bufferFileTemp[i];
	}

	printf("max = %hu , min = %hu \n" , max , min);

}
