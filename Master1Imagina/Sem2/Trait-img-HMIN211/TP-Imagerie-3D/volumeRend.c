#include "image3D.h"
#include <errno.h>
#include <string.h>
#include <cmath>
#include "image_ppm.h"


short unsigned int *** img1Dto3D(unsigned short int *bufferFile1D, unsigned int dimX , unsigned int dimY , unsigned int dimZ)
{
	unsigned short int ***bufferFile3D;
	bufferFile3D =(unsigned short int ***) malloc(dimX * sizeof(unsigned short int**));
	for(unsigned int i = 0 ; i < dimX ; i++)
		bufferFile3D[i] = (unsigned short int **) malloc(dimY * sizeof(unsigned short int*));

	for(unsigned int i = 0 ; i < dimX ; i++)
	{
		for(unsigned int j = 0 ; j < dimY ; j++)
		{

			bufferFile3D[i][j]= (unsigned short int *) malloc(dimZ * sizeof(unsigned short int));
		}
	}
	



	unsigned int l=0;
	for(unsigned int i = 0 ; i < dimX ; i++)
	{
		for(unsigned int j = 0 ; j < dimY ; j++)
		{
			for(unsigned int k = 0 ; k < dimZ ; k++)
			{
				
				bufferFile3D[i][j][k] = bufferFile1D[l];
				// printf("%hu %hu\n " , bufferFile1D[l] , bufferFile3D[i][j][k]);
				l++;
			}
		}
	}

	return bufferFile3D;
}

void imgXray(OCTET *imgOut, unsigned short int ***bufferFile3D, unsigned int dimX , unsigned int dimY , unsigned int dimZ)
{
	unsigned long long int l=0;
	for(unsigned int i = 0 ; i < dimX ; i++)
	{
		for(unsigned int j = 0 ; j < dimY ; j++)
		{
			unsigned long long int  moyenne=0;
			unsigned short int min = 0;
			--min;

			unsigned short int max = 0;
			
			for(unsigned int k = 0 ; k < dimZ ; k++)
			{
				// printf("%u %u %u " , i , j , k);
				// printf("%hu\n" , bufferFile3D[i][j][k]);
				moyenne += bufferFile3D[i][j][k];
				if(bufferFile3D[i][j][k] < min)
					min = bufferFile3D[i][j][k];
				
				if(bufferFile3D[i][j][k] > max)
					max = bufferFile3D[i][j][k];
				
			}	
			// printf("moyenne sans div par dimZ : %llu \n", moyenne);
			if(dimZ != 0)
				moyenne = moyenne / dimZ;


			printf("moyenne avec div par dimZ : %llu \n", moyenne);
			// moyenne =round((double) moyenne * 255 / 228);
			printf("moyenne quantifié : %llu \n", moyenne);
			imgOut[l]=(char)moyenne;
			printf("min %hu\n" , min);
			min = round((double)min * 255.0 /228.0);
			printf("min quantifié : %hu\n", min);
			// imgOut[l]=min ;

			printf("max %hu\n" , max);
			max =round((double)max * 255.0 /228.0);
			printf("max quantifié : %hu\n", max);
			// imgOut[l]=max ;
			l++;
			
			printf("\n");

		}
	}
}


void imgGetCoupe(OCTET *imgOut, unsigned short int ***bufferFile3D, unsigned int dimX , unsigned int dimY , unsigned int dimZ , unsigned int coupeZ)
{
	unsigned long long int l=0;
	for(unsigned int i = 0 ; i < dimX ; i++)
	{
		for(unsigned int j = 0 ; j < dimY ; j++)
		{
			
			// imgOut[l] = round((double)bufferFile3D[i][j][coupeZ] * 255.0 / 228.0 ) ;
			imgOut[l] = (char) bufferFile3D[i][j][coupeZ];
			l++;	
		}
	}

}

int main(int argc, char** argv)
{

	char nomFichier[250] , nomFichierOut[250];
	unsigned int dimX, dimY, dimZ;


	if (argc != 6)
	{
		printf("Rentrez nomFichier dimX dimY dimZ nomFichierOut \n");
		return -1 ;
	}

	sscanf (argv[1], "%s", nomFichier) ;
	sscanf (argv[2], "%u", &dimX) ;
	sscanf (argv[3], "%u", &dimY) ;
	sscanf (argv[4], "%u", &dimZ) ;
	sscanf (argv[5] ,"%s" , nomFichierOut);
	
	FILE *f = fopen(nomFichier, "r");
	if(f == NULL)
	{
		printf("erreur lors d'ouverture du fichier : %s \n" ,strerror(errno));
		return 0;
	} 


	unsigned long long int sizeofImg= dimX * dimY * dimZ;
	
	// unsigned short int bufferFile*** = malloc(dimX* )

	unsigned short int *bufferFileTemp =(unsigned short int *) malloc(sizeofImg * sizeof(unsigned short int));

	// unsigned short bufferFileTemp[sizeofImg];
	
	

	
	
	fread(bufferFileTemp , sizeof(unsigned short) , sizeofImg , f );

	
	

	

	for(unsigned long long int i = 0 ; i < sizeofImg ; i ++)
	{
		bufferFileTemp[i] = _bswap16(bufferFileTemp[i]);
		
	}

	unsigned short int ***bufferFile3D =NULL;
	
	
	printf("allocating\n");
	bufferFile3D= img1Dto3D(bufferFileTemp, dimX , dimY , dimZ);
	printf("done\n");

	OCTET * imgOut;
	allocation_tableau(imgOut, OCTET, sizeofImg) ;

	printf("imgXraying\n");
	imgXray(imgOut , bufferFile3D , dimX , dimY , dimZ);
	printf("done\n");
	ecrire_image_pgm(nomFichierOut , imgOut  , dimX , dimY);
	// for(unsigned int coupeZ = 0 ; coupeZ < dimZ ; coupeZ++)
	// {
	// 	char nomFichierOutCoupe[200]="coupe_img";
	// 	char numCoupeZ[4];
	// 	char ext[5]= ".pgm";
	// 	imgGetCoupe(imgOut , bufferFile3D , dimX , dimY , dimZ , coupeZ);
		
		
	// 	sprintf(numCoupeZ , "%u" , coupeZ);
	// 	strcat(nomFichierOutCoupe , numCoupeZ);
	// 	strcat(nomFichierOutCoupe , ext );
	// 	ecrire_image_pgm(nomFichierOutCoupe, imgOut , dimX , dimY);
	// }

		

}
