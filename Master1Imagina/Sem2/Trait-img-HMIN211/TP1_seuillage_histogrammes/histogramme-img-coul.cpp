// test_couleur.cpp : Seuille une image couleur

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille, nR, nG, nB;

  if (argc != 2)
  {
    printf("Usage: ImageIn.ppm dont on souhaite obtenir l'histogramme \n");
    exit (1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;

  OCTET *ImgIn;

  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

  OCTET H[256][3];

  for (int i = 0; i < nTaille3; i += 3)
  {
    nR = ImgIn[i];
    nG = ImgIn[i + 1];
    nB = ImgIn[i + 2];

    H[nR][0]++;
    H[nG][1]++;
    H[nB][2]++;
  }

  for (int i = 0 ; i< 256 ; i ++)
  {
    printf("%u %u %u %u\n", i , H[i][0] , H[i][1] , H[i][2]);

  }

  free(ImgIn);
  return 1;
}
