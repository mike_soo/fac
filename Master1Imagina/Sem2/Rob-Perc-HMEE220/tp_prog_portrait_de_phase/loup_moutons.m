
%programme de trace de portrait de phase et de trajectoire
% pour un systeme non lineaire x'=y et y'=-3x-x^2-0.06y
close all
clear all
%d?finition de l'?quation differentielle
%f = @(t,Y) [Y(2); -3*Y(1)-Y(1)^2-0.06*Y(2)];  
f = @(t,Y) [Y(1) - 0.01*Y(1)*Y(2) ; -Y(2)+(Y(1)*Y(2)*0.02)]; 
%x' = x-0.01xy et y' = -y+0.02xy
y1 = linspace(-1,1,20); %vecteur de 20 points
y2 = linspace(-1,1,20); %idem

[x,y] = meshgrid(y1,y2);

u = zeros(size(x));  %vecteur derive
v = zeros(size(x)); % idem

%calcul des derivees
t=0; 
for i = 1:numel(x)
    Yprime = f(t,[x(i); y(i)]);
    u(i) = Yprime(1);
    v(i) = Yprime(2);
end
%norme et fleche
for i = 1:numel(x)
Vmod = sqrt(u(i)^2 + v(i)^2);
u(i) = u(i)/Vmod;
v(i) = v(i)/Vmod;
end
%portrait de phase
quiver(x,y,u,v,'r'); figure(gcf)
xlabel('y_1')
ylabel('y_2')
axis tight equal;

%trajectoire
hold on
y10 = [ -3 0.9 1 1 ];
y20 = [ 1 3 2 1];
for i=1:4
    [ts,ys] = ode45(f,[0,5],[y10(i) y20(i)]);
%     y1=ys(:,2);
%     n=y1(y1>-2);
%     j=length(n);
    plot(ys(:,1),ys(:,2),'k')
    plot(ys(1,1),ys(1,2),'bo') % starting point
    plot(ys(i,1),ys(i,2),'ks') % ending point
end
[ts,ys] = ode45(f,[0,0.2],[-5 0]);
   plot(ys(:,1),ys(:,2),'k')
    plot(ys(1,1),ys(1,2),'bo') % starting point
    plot(ys(i,1),ys(i,2),'ks') % ending point
hold off
