% 
%   COMMANDE en CINEMATIQUE INVERSE des 2 premieres articulations (JOINTS) du SCARA
%   BUT: L'organe terminal suit une ligne droite
        %%Implémenter loi de commande
        %% 
        %   f(to + delta_t) = f(to) + f'(to) * delta_t
        %                        MGD          MCD
        % Chercher developpement limité le hécient
%   POUR LANCER: 
%   1. Ouvrez test_scara.ttt avec Vrep; 
%   2. Lancer Scara.m sous Matlab en appuant sur F5 (ou RUN). 
%   P.S.: Tous les fichiers doivent etre dans le meme dossier

clc
clear all
close all
disp('Program started');
vrep = remApi('remoteApi');         % ouverture d'une bibliotheque Matlab-Vrep (remoteApiProto.m)
vrep.simxFinish(-1);                % Matlab plante si elle n'arrive pas de fermer les connexions precedentes

id = vrep.simxStart('127.0.0.1', 19997, true, true, 5000, 5); %id identificateur de la connexion avec VREP

%	true : la fonction attend la connexion jusqu'au time out
%	true : si la connexion est perdue pas d'attente d'une  seconde tentative
%	5000 timeout en ms
%   19997 Modifiable dans le dossier VREP
%	5 valeur recommand?e pour le renvoi des data
if ( id > -1 )
    disp('Serveur connecte')
else 
    disp('Serveur non connecte')
    return
end

%% VALEURS INIT
    Joints_Number=2; % On ne commande que les 2 premieres articulations
    l1=0.467; % Length link 1
    l2=0.405; % Length link 2
    Joint_Handle_Name='MTB_axis'; %attention les articulations dans vrep commencent par ce prefixe
    Joint_Initial_Angle=[0.1 0.1]; % angles initiaux des articulations en radian
    Joint_Final_Angle=[-pi pi/4]; %idem 
    IndexMax = 100;
    dq1 = (Joint_Final_Angle(1)-Joint_Initial_Angle(1))/IndexMax;
    dq2 = (Joint_Final_Angle(2)-Joint_Initial_Angle(2))/IndexMax;
    PointPassage_q1=[];
    PointPassage_q2=[];
    Time=zeros(1,IndexMax);
    PointPassage_q1d=Joint_Initial_Angle(1);
    PointPassage_q2d=Joint_Initial_Angle(2);
    for i=1:IndexMax-1
        PointPassage_q1d = [PointPassage_q1d Joint_Initial_Angle(1)+i*dq1];
        PointPassage_q2d = [PointPassage_q2d Joint_Initial_Angle(2)+i*dq2];
    end
%%fin init

%% LES HANDLES DES ARTICULATIONS
Joint_Handles = get_handles( id, vrep, Joints_Number, Joint_Handle_Name);
% Les identifiants (handles) les moteurs des axes 1 et 2 sont dans
% Joint_Handles(1) et Joint_Handles(2)
%% START

        %% SYNCHRONISATION Vrep attendra l'ordre issu de Matlab pour executer un pas de simulation
        sync_status = vrep.simxSynchronous(id,1);  % on passe en mode synhrone 
        % On demarre VREP. Si err code =0 -> ok
        [error]=vrep.simxStartSimulation(id,vrep.simx_opmode_oneshot_wait); %on attend que vrep soit pr?t
        % TRIGGER DE LA SYNCHRONISATION
        trig_status = vrep.simxSynchronousTrigger(id); %on se synchronise
        vrep.simxGetPingTime(id); % on attend que tout soit ok
        
        %% METTRE LES ARTICULATIONS AUX ANGLES INITIAUX
        set_joint_angle( id, vrep,Joint_Handles, Joints_Number,Joint_Initial_Angle, 'oneshot')
                PointPassage_q1 = [PointPassage_q1 Joint_Initial_Angle(1)];
                PointPassage_q2 = [PointPassage_q2 Joint_Initial_Angle(2)];
       
        %% DATA STREAMING
        % On prepare la recuperation des angles des articulations. On
        % indique le mode streaming, vrep enregistrera automatiquement les
        % donnees dans un buffer
        Joint_Angle =get_joint_angle( id, vrep, Joint_Handles,Joints_Number, 'streaming');
        pause(1) % Important ! un petit retard pour bien initialiser la communication
        Time(1)=vrep.simxGetLastCmdTime(id);
        %% MAIN
        Index=2;
        while Index<=IndexMax     
                %% Lecture valeurs des articulations
                Joint_Angle =get_joint_angle( id, vrep, Joint_Handles, Joints_Number, 'buffer');
                % Memorisation Position articulaire intermediaire
                PointPassage_q1 = [PointPassage_q1 Joint_Angle(1)];
                PointPassage_q2 = [PointPassage_q2 Joint_Angle(2)];
                %increment de deplacement
                 Joint_Angle(1) = PointPassage_q1d(Index);
                 Joint_Angle(2) = PointPassage_q2d(Index);
                %% On commande les articulations
                set_joint_angle( id, vrep,Joint_Handles, Joints_Number,Joint_Angle, 'oneshot');
                trig_status = vrep.simxSynchronousTrigger(id); %on se synchronise
                Time(Index)=vrep.simxGetLastCmdTime(id);
                Index=Index+1;
        end
     
        disp('On est arrive')
         
        %% STOP DATA STREAMING
        for i = 1:Joints_Number
            Joint_Angle(i)=vrep.simxGetJointPosition( id, Joint_Handles(i), vrep.simx_opmode_discontinue);
        end
        % Stop VREP

        %% Affichage dans resultats
plot(Time,PointPassage_q1);
hold;
plot(Time,PointPassage_q1d,'r');
title('Articulation 1')
legend('valeur lue','Valeur desiree')
xlabel('Temps');
figure
plot(Time,PointPassage_q2);
hold;
plot(Time,PointPassage_q2d,'r');
title('Articulation 2')
legend('valeur lue','Valeur desiree');
xlabel('Temps');

disp('en pause avant de fermer la com');
pause

[error]=vrep.simxStopSimulation(id,vrep.simx_opmode_oneshot_wait);
vrep.simxFinish(id); % On libere le socket pour que les autres puissent se connecter
vrep.delete();
disp('Programme termine');