function [ Joint_Angle ] = get_joint_angle( id, vrep,Joint_Handles, Joints_Number, mode)
%id 

%% CHOIX DU MODE DE FONCTIONNEMENT

if strcmp(mode,'streaming')
    opmode=vrep.simx_opmode_streaming;
elseif strcmp(mode,'buffer')
    opmode=vrep.simx_opmode_buffer;
else
    disp('Error get_joint_angle mode \n')
end

%% MAIN
Joint_Angle=zeros(1,Joints_Number);
for i = 1:Joints_Number
    [error, Joint_Angle(i)] = vrep.simxGetJointPosition( id, Joint_Handles(i),opmode);
    if (error>1) disp('Error get_joint_angle \n'); error
    end
end
end

