package dico;

public abstract class AbstractDict implements IDictionnary{
	protected Object keys[];
	protected Object values[];
	protected int size;
	protected int nbOfElements;
	
	AbstractDict(int tailleInit){
		nbOfElements=0;
		size = tailleInit;
		keys = new Object[tailleInit];
		values = new Object[tailleInit];
	}

	AbstractDict(){
		nbOfElements=0;
		size = 50;
		keys = new Object[50];
		values = new Object[50];
	}
	public int getnbOfElements()
	{
		return nbOfElements;
	}
	
	protected void grow(){
		
		Object newKeys[] = new Object[size*2];
		Object newValues[] = new Object[size*2];
		for (int i=0;i<size; ++i){
			newKeys[newIndexOf(i)] = keys[i];
			newValues[newIndexOf(i)] = keys[i];
		}
		keys = newKeys;
		values = newValues;
		size*=2;
		System.gc();
	}

	/** Recherche séquentielle */
	public Object get(Object Key)
	{
		for (int i=0; i<size; ++i) /* keys.length = values.length */
		{
			if(keys[i]==null)
			{
				
				
			}
			else if (keys[i].equals(Key))
			{	
			
				return values[i];
			}
		}
		
		return null;
	}

	public boolean containsKey(Object Key){
		return get(Key)!=null;
	}

	public boolean isEmpty(){
		return size==0;
	}

	protected int indexOf(Object Key){
		for (int i=0; i<keys.length; ++i){ /* keys.length = values.length */
			if (keys[i].equals(Key))
				return i;
		}
		return -1;
	}
	
	public abstract IDictionnary put(Object key, Object value);
	
	protected abstract int newIndexOf(Object Key);
	public int getSize()
	{
		return size;
	}
}
