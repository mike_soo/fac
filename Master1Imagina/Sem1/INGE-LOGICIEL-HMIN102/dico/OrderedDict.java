package dico;

public class OrderedDict extends AbstractDict {
	
	public OrderedDict()
	{
		super();
	}
	
	public IDictionnary put(Object key, Object value){
		
		if (containsKey(key))
		{
			values[indexOf(key)] = value;
		} 
		else 
		{
			if (nbOfElements+1>=keys.length)
				grow();
			
			keys[newIndexOf(key)] = key;
			values[newIndexOf(key)] = value;
			++nbOfElements;
		}
		return null; // TODO
	}
	
	protected int newIndexOf(Object Key) {
		
		return nbOfElements;
	}
	
	
}
