package dico;

public class SortedDict extends AbstractDict implements IDictionnary {	
	public IDictionnary put(Object key, Object value){
		if (containsKey(key)){
			values[indexOf(key)] = value;
		} else {
			int i = newIndexOf(key);
			if (size+1>=keys.length)
				grow();
			if (i!=size){
				// moving values if newIndex isn't at end of array
				for (int j=i+1; j<size; ++j){
					keys[j-1] = keys[j];
					values[j-1] = values[j];
				}
			}
			keys[newIndexOf(key)] = key;
			values[newIndexOf(key)] = value;
			++size;
		}
		return null;
	} // TODO

	@Override
	protected int newIndexOf(Object key){
		
		for (int i=0; i<size ;i++)
		{
		
			if((keys[i].toString()).compareTo(key.toString())<0)
			{
				return i;
			}
			
			//TODO
			System.err.println("TODO newIndexOf");
			 
		}
	
		return -1;
		
	}
	
	
	protected void addObject(Object object)
	{
		int newIndexOfObject=newIndexOf(object);
		
		Object interKey=keys[newIndexOfObject];
		Object interValue=values[newIndexOfObject];
		
	}

}
