package dico;

public class FastDico extends AbstractDict implements IDictionnary {
	public FastDico() {
		super(1000);
	}
	
	public int size(){
		return size; // size already returns number of *defined* array's cells, not array's length
	}
	
	public boolean mustGrow(){
		return (keys.length - size)/keys.length <= 0.25;
	}
	
	public int nextAvaibleIndex(int indexOf)
	{	
		int nbCasesLooked=0;
		int i = indexOf;
		while (nbCasesLooked<size)
		{
			if(keys[i%size]==null)
			{
				return i;
			}
			i++;
			nbCasesLooked++;
		}
		return -1;
	}
	
	
	private int getHash(Object key){
		return key.toString().hashCode()%size;
	}
	
	public int getIndexFrom(Object key , int indexOf)
	{
		int nbCasesLooked=0;
		int i = indexOf;
		while (nbCasesLooked<size)
		{
			if(keys[i%size]==key)
			{
				return i;
			}
			i++;
			nbCasesLooked++;
		}
		return -1;
	}
	
	@Override
	protected int indexOf(Object key){
		
		int hashkey=getHash(key);
		if (keys[hashkey] == key)
		{
			return hashkey;
		}
		else
		{ 
			return getIndexFrom(key,hashkey);
		}
	}

	@Override
	protected void grow()
	{
		Object oldKeys[]=keys;
		Object oldValues[]=values;
		int oldSize = size;
		keys = new Object[oldSize*2];
		values = new Object[oldSize*2];
		//new size		
		size = oldSize*2;
		for (int i=0;i<oldSize; ++i)
		{
			put( oldKeys[i] , oldValues[i]);
		}
		
		System.gc();
	}
	
	@Override
	public IDictionnary put(Object key, Object value) {
		
		if(mustGrow())
		{
			grow();
		}
		int indexOf=newIndexOf(key);
		if(indexOf!=-1)
		{
			keys[indexOf]=key;
			values[indexOf]=value;
			size++;
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	public Object get(Object key)
	{
		int indexOf=indexOf(key);
		if(indexOf!=-1)
			return values[indexOf];
		else return -1;
	}

	@Override
	protected int newIndexOf(Object key) 
	{
		
		int indexOf= getHash(key);
		if(keys[indexOf]==key)
		{
			return -1;
		}
		
		else if (keys[indexOf]!=null)
		{
			return nextAvaibleIndex(indexOf);
		}
		
		else
		{
			return -1;
		}
		
		
	}
	
	

}
