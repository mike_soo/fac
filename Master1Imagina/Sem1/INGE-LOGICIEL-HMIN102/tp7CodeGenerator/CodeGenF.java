package tp7CodeGenerator;

public class CodeGenF extends ACodeGenerator
{
	public String convertPhrase(String phrase)
	{
		int derLetCode = (int) (0.30 * phrase.length());

		char c[] = new char[phrase.length()] ;
		
		
		for(int i =0 ; i< derLetCode ; i++)
		{
			c[i]= (char)(phrase.charAt(i) + 1000) ;
		}

		for(int i = derLetCode ; i < phrase.length() ; i ++  )
		{
			c[i] = phrase.charAt(i);
		}

		
		return new String (c);
	}

	
	
}