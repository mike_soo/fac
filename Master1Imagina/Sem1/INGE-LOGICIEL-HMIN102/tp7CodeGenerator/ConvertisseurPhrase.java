package tp7CodeGenerator;

public class ConvertisseurPhrase
{
	protected ACodeGenerator cg;
	

	public ConvertisseurPhrase(String niv, String phrase)
	{
		cg = ACodeGenerator.getInstance(niv, phrase);
		String convertedPhrase=cg.convertPhrase(phrase);

		System.out.println("Converted phrase = " + convertedPhrase );

	}	
}