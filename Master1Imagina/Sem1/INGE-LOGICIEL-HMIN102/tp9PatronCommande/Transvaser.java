package tp9PatronCommande;

public class Transvaser extends Command{
	
	public Transvaser(Bidon source, Bidon cible) {
		super(source);
		// TODO Auto-generated constructor stub
		this.cible = cible;
	}

	private Bidon cible;
	
	int qteEauUtiliseCible;
	
	public Bidon getCible() {
		return cible;
	}

	public void setCible(Bidon cible) {
		this.cible = cible;
	}

	public void doit()
	{
		setQteEauUtilise( getSource().getVolumeCourant());
		
		setQteEauUtiliseCible(getCible().getVolumeCourant());
		
		int interQteEau = getSource().getVolumeCourant();
		
		getSource().setVolumeCourant(getCible().getVolumeCourant());
		
		getCible().setVolumeCourant(interQteEau);
		
	}
	
	public int getQteEauUtiliseCible() {
		return qteEauUtiliseCible;
	}

	public void setQteEauUtiliseCible(int qteEauUtiliseCible) {
		this.qteEauUtiliseCible = qteEauUtiliseCible;
	}

	public void undo()
	{
		getSource().setVolumeCourant(getQteEauUtilise());
		getCible().setVolumeCourant(getQteEauUtiliseCible());
		
	}

}
