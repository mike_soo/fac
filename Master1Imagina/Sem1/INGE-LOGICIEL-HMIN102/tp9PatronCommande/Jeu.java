package tp9PatronCommande;

import java.util.ArrayList;

public class Jeu {
	
	private ArrayList <Command> historique;
	private ArrayList <Bidon> bidons;
	private int lastUndo;
	
	
	public Jeu(int nbBidons)
	{
		historique = new <Command> ArrayList();
		bidons = new <Bidon> ArrayList();
		
		
		for (int i = 0; i < nbBidons ; i ++ )
		{
			bidons.add(new Bidon(i + 1  , 0));
			
		}
		
	}
	
	public void commandDo(String com , int idBidonSource , int idBidonCible)
	{	
		Command c = null;
		if( com.equals("r"))
		{
			c = new Remplir(bidons.get(idBidonSource));
			c.doit();
			historique.add(c);
			
		}
		if( com.equals("v"))
		{
			c = new Vider(bidons.get(idBidonSource));
			c.doit();
			historique.add(c);
		}
		if( com.equals("t"))
		{	c= new Transvaser(bidons.get(idBidonSource),bidons.get(idBidonCible));
			c.doit();
			historique.add(c);
		}
		lastUndo++ ;
		
	}
	
	public void commandUndo()
	{
		historique.get(lastUndo - 1 ).undo();
		lastUndo--;
	}
	
	public void afficheBidons()
	{
		
		for (Bidon b : bidons)
		{
			System.out.print(b.getVolumeCourant()+ " , ");
		}
		System.out.println("");
	}
}
