package tp9PatronCommande;

public class Remplir extends Command{
	

	public Remplir(Bidon source) {
		super(source);
		// TODO Auto-generated constructor stub
	}

	public void doit()
	{
		// on sauvegarde la quantité d'eau utilisé pour l'historique
		setQteEauUtilise(getSource().getVolumeMax());
		
		// on remplit le bidon source
		getSource().setVolumeCourant(getSource().getVolumeMax());
		
	}
	
	public void undo()
	{
		getSource().setVolumeCourant( getSource().getVolumeCourant() - getQteEauUtilise());
		
	} 

}
