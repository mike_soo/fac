package tp9PatronCommande;

public abstract class Command {
	Bidon source ;
	int qteEauUtilise;
	
	
	public Bidon getSource() {
		return source;
	}

	public void setSource(Bidon source) {
		this.source = source;
	}

	public int getQteEauUtilise() {
		return qteEauUtilise;
	}

	public void setQteEauUtilise(int qteEauUtilise) {
		this.qteEauUtilise = qteEauUtilise;
	}

	public  Command(Bidon source)
	{
		this.source = source;
	}
	
	public abstract void doit();
	public abstract void undo();
	
}
