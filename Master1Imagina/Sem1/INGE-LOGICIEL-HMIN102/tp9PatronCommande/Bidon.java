package tp9PatronCommande;

public class Bidon {
	private int volumeMax;
	private int volumeCourant;
	
	public Bidon ( int volumeMax , int volumeCourant)
	{
		this.volumeMax = volumeMax;
		this.volumeCourant = volumeCourant;
		
		
	} 
	public int getVolumeCourant()
	{
		return volumeCourant;
	}
	public int getVolumeMax() {
		return volumeMax;
	}
	public void setVolumeMax(int volumeMax) {
		this.volumeMax = volumeMax;
	}
	public void setVolumeCourant(int volumeCourant) {
		this.volumeCourant = volumeCourant;
	}
}
