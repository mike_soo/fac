package tp8PatronDecorator;


public class Produit extends AProduit
{
	public Produit(String nom,float prix)
	{
		super(nom, prix);
	}

	public float getPrix()
	{
		return (float) prix;
	}
}
