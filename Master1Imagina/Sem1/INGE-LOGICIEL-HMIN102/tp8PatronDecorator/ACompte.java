package tp8PatronDecorator;

public abstract class ACompte {
	
	protected ACompte decoratorCompte = null;
	private AClient client = null;
	
	public ACompte(AClient client)
	{
		this.client= client;

	}

	public ACompte(AClient client, ACompte decoratorCompte)
	{
		this.client = client;
		this.decoratorCompte = decoratorCompte;
	}
	
	public abstract float getPrixLocationDecorator(AProduit p);
	public abstract float getPrixLocation(AProduit p);
}