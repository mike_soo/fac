package tp8PatronDecorator;

public class CompteReduc extends ACompte{
	

	
	public CompteReduc( AClient client)
	{
		super (client);
	}
	
	public CompteReduc(AClient client, ACompte decoratorCompte)
	{
		super (client ,decoratorCompte);
	}

	public float getPrixLocation(AProduit p)
	{
		return (float) 0.9*p.getPrix() ;

	}

	public float getPrixLocationDecorator(AProduit p)
	{
		float res;

		if(decoratorCompte ==null)
		{
			return (float)0.9*p.getPrix();
		}
		else
		{
			res = decoratorCompte.getPrixLocationDecorator(p);

			return  (float) (res*0.9);

		}
	}
}