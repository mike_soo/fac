package tp8PatronDecorator;

public class CompteAvecSeuil extends ACompte{
	private int nbProduitLoue;		

	// le nombre de produit loué pour avoir une location gratuite
	private static int nbLouePourAvoirLocGratuite = 2;
	
	public CompteAvecSeuil(AClient client, ACompte decoratorCompte)
	{
		super (client ,decoratorCompte);
	}
	
	public CompteAvecSeuil( AClient client)
	{
		super (client);
		this.nbProduitLoue = 0;
		
	}

	public float getPrixLocation(AProduit p)
	{
		if(nbProduitLoue == nbLouePourAvoirLocGratuite)
		{
			return 0;
		}
		else 
		{
			nbProduitLoue ++;
			return (float) p.getPrix() ;
		}
	

	}

	public float getPrixLocationDecorator(AProduit p)
	{
		if(nbProduitLoue == nbLouePourAvoirLocGratuite)
		{
			return 0;
		}
		else 
		{
			
			if(decoratorCompte == null)
			{
				return getPrixLocation(p);
			}
			else
			{
				if(nbProduitLoue == nbLouePourAvoirLocGratuite)
				{
					nbProduitLoue =0;
					return 0;
				}
				else
				{
					nbProduitLoue++;
					return decoratorCompte.getPrixLocationDecorator(p);
				}
					
				
			}
		}
	

	}
}