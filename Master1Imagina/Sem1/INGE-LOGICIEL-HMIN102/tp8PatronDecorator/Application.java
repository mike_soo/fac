package tp8PatronDecorator;

public class Application
{

	public static void main ( String [] args ) 
	{
		Produit lgv = new Produit("La grande vadrouille" , (float) 10.0 );
		Client cl = new Client("Dupont");
		Compte cmt = new Compte(cl);
		System.out.println ( " Compte" + cmt.getPrixLocation(lgv) ) ;

		ACompte cmt2 = new CompteReduc (cl) ;
		System.out.println ( " CompteReduction : " + cmt2.getPrixLocation(lgv)) ;
		ACompte cmt3 = new CompteAvecSeuil (cl)  ; // l e s e u i l e s t à 2 par d é f a u t
		System.out.println ( " CompteSeuil : " + cmt3.getPrixLocation(lgv)) ;
		System.out.println ( " CompteSeuil : " + cmt3.getPrixLocation(lgv)) ;
		System.out.println ( " CompteSeuil : " + cmt3.getPrixLocation(lgv)) ; // d o i t a f f i c h e r 0
		AProduit r4=new ProduitSolde( "RockyIV" , (float)10.0 ) ;
		System.out.println ( "CompteNormal+ProduitSoldé : " + cmt.getPrixLocation(r4)) ;
		System.out.println ( " CompteReduction+ProduitSoldé : " + cmt2.getPrixLocation(r4)) ; 
		
		
		//Exemple avec reductor
		
		ACompte cmtr2 = new CompteReduc(cl);
	    ACompte cmtr1 = new CompteReduc(cl,cmtr2);
	    
	    ACompte cmtrs = new CompteAvecSeuil(cl , cmtr1);
	    System.out.println("Compte reduc x2 en Decorator ="+cmtr1.getPrixLocationDecorator(lgv));
	    
	    System.out.println("Compte reduc x2 + seuil en decorator " + cmtrs.getPrixLocationDecorator(lgv) );
	    System.out.println("Compte reduc x2 + seuil en decorator " + cmtrs.getPrixLocationDecorator(lgv) );
	    System.out.println("Compte reduc x2 + seuil en decorator devrait afficher zero " + cmtrs.getPrixLocationDecorator(lgv) );
	}
}