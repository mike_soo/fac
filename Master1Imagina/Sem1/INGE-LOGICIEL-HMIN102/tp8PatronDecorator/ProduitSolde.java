package tp8PatronDecorator;


public class ProduitSolde extends AProduit
{
	public ProduitSolde(String nom,float prix)
	{
		super(nom, prix);
	}

	public float getPrix()
	{
		return (float) (0.50 * prix);
	}
}
