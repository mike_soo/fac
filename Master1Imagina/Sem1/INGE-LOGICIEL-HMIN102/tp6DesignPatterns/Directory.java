package tp6DesignPatterns;

import java.util.ArrayList;
import java.util.HashMap;

public class Directory extends AbDirectory{

	private HashMap <String, Structure > content;	
	

	public ArrayList<Structure> getContent()
	{
		ArrayList <Structure> acontent= new ArrayList<Structure>();
		
		for (String s : content.keySet())
		{
			acontent.add(content.get(s));
		}
		return acontent;
	}

	public int size()
	{
		
		int totalSize =0;
		
		for (Structure s : getContent())
		{
			totalSize += s.size();
		}

		return totalSize;
					 
	}	
	

	@Override
	public String ls() {
		// TODO Auto-generated method stub
		String out="";

		for (Structure s : getContent())
		{
			if(!(s instanceof AbDirectory))
			{
				out += s.ls() + "\n";
			}	
		}

		return out;
	}

	@Override
	public int nbElements() {
		return getContent().size();
	}

	@Override
	public void addStructure(Structure s) {
		// TODO Auto-generated method stub
		getContent().add(s);
	}

	@Override
	public void rmStructure(Structure s) {
		// TODO Auto-generated method stub
		getContent().remove(s.getName());
	}

	@Override
	public int basicSize() {
		// TODO Auto-generated method stub

		return 4;
	}	
}