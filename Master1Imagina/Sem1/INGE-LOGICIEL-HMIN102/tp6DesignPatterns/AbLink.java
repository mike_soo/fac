package tp6DesignPatterns;

public abstract class AbLink extends Structure {

	private Structure pointedStruct;
	abstract public void addPointedStruct(Structure s);
	abstract public void rmPointedStruct(Structure s);
	
}