package tp7AbstractFactory;

public class Compiler
{
	protected AFactoryCompiler fc;
	protected ALexer lexer;
	protected AParser parser;
	protected AGenerator gen;
	
	public Compiler( String l) throws Exception
	{
		fc = AFactoryCompiler.getInstance(l);
		if (fc == null )
		{
			System.err.println("AFactoryCompiler.getInstance = null");
			throw new Exception() ;
		}
		
		this.lexer = fc.createLexer();
		this.parser = fc.createParser();
		this.gen = fc.createGenerator();
		
		
		lexer.scan(l);
		parser.parse(l);
		gen.generate(l);
	}
}