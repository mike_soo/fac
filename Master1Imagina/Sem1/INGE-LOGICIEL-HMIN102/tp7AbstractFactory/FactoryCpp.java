
package tp7AbstractFactory;

public class  FactoryCpp extends AFactoryCompiler {

	public LexerCpp createLexer()
	{
		return new LexerCpp();
	}
	public ParserCpp createParser()
	{
		return new ParserCpp();
	}
	public GenCpp createGenerator()
	{
		return new GenCpp();
	}

}