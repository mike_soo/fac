package tp7AbstractFactory;

abstract class AParser
{
	public abstract void parse(String t);
}