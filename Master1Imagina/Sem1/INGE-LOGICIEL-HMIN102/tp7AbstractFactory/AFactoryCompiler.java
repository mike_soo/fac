package tp7AbstractFactory;

public abstract class  AFactoryCompiler {

	public abstract ALexer createLexer();
	public abstract AParser createParser();
	public abstract AGenerator createGenerator();

	

	public static AFactoryCompiler getInstance(String l )
	{
		if(l.equals( "java"))
		{
			return new FactoryJava();
		}

		if ( l.equals( "cpp"))
		{
			return new FactoryCpp();
		}
		
		return null;
	}
}