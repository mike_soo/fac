//package mochito;
//
//import static org.mockito.Mockito.*;
//import org.junit.*;
//import org.junit.runner.RunWith;
//import org.mockito.junit.MockitoJUnitRunner;
//
//
//
//@SuppressWarnings("unused")
//@RunWith(MockitoJUnitRunner.class)
//
//public class TestSpy {
//	private A a=new A();
//	private A spyA=spy(a);
//	private A mockA= mock(A.class);
//	@Test
//	public void test1()
//	{
//		Assert.assertEquals(42,spyA.m1());
//		Assert.assertEquals(25,spyA.m2(5));
//		
//		verify(spyA,times(1)).m1();
//		verify(spyA,times(1)).m2(5);
//		
//		
//	}
//	@Test
//	public void test2()
//	{
//		when(mockA.m2(42)).thenReturn(0);
//		Assert.assertEquals(0,spyA.m2(42));
////		Assert fail car meme si on a mocker la methode
////		m2 pour qu'elle envois 0 quand on fournis 42 comme 
////		parametre le spy est sur la class et donc non prend
////		pas en compte les modification par le mock
//	}
//}