#include "commons.h"

#define nombre_de_pixel 40
#define nombre_de_procesus 5

extern bool sig_intACT;


int main(int argc , char ** argv)
{
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	
	if (signal(SIGINT, sig_handler) == SIG_ERR) {
        fputs("Erreur lors de signal handler.\n", stderr);
        return EXIT_FAILURE;
    }
	int res;

		
	// init valeur de semaphores à un
	unsigned short sems_vals [nombre_de_procesus];
	sems_vals[0]=nombre_de_pixel;

	for(int i = 1 ; i < nombre_de_procesus ; i ++  )
	{
		sems_vals[i]=0;
	}

	int idSem = semget(sesame, nombre_de_procesus ,IPC_CREAT | 0600);
	gestionErr(idSem,"semget");
	

	semun s;
	s.array = sems_vals;


	res = semctl( idSem , 0 , SETALL , s);
	gestionErr(res,"semctl");

	//init valeurs de mémoire partagé.
	int idMem = shmget(sesame, nombre_de_procesus*sizeof(unsigned int), IPC_CREAT|0600);
	gestionErr(idMem,"shmget");

	unsigned int *ptrMemP = (unsigned int*)shmat(idMem,NULL,0);

	while(1){
		
		
		res = semctl(idSem, 0 /*semno ignored*/ ,GETALL ,  s.array);
		gestionErr(res,"semctl main ");
		
		// Affichage des sémaphores 
		for(int i = 0 ; i < nombre_de_procesus; i++)
		{
			printf("%-3i",i);
		}
		printf("\n\e[1;31m");
		for(int i = 0 ; i < nombre_de_procesus; i++)
		{
			printf("%-3i",s.array[i]);			
		}
		printf("\e[0m\n");
		
		// Affichage mémoire partagé (valeur de zone en cour de traitement)
		printf("\e[1;32m");
		for(int i = 0 ; i < nombre_de_procesus; i++)
		{
			printf("%-3i",ptrMemP[i]);			
		}

		printf("\e[0m\n\n");
			

		sleep(1);
	

		
		if(sig_intACT)
		{
			break;
		}	
	}	

	res=shmctl(idMem,IPC_RMID,0);
	gestionErr(res,"shmctl");	
	res=semctl(idSem,IPC_RMID,0);
	gestionErr(res,"semctl");	
	
	return 0;
}