#include "commons.h"

#define nombre_de_pixel 40
#define nombre_de_procesus 5

void traitement_image(int idp, int idSem , unsigned int* ptrMemP)
{
	// semaphore +1
	srand(time(NULL));

	struct sembuf opp={(u_short)(idp + 1),(short)1,SEM_UNDO};
	// semaphore -1
	struct sembuf opv={(u_short)idp,(short)-1,SEM_UNDO};
	int res ;
	
	printf("idp = %i : demande sem0\n", idp);
	res = semop( idSem , &opv , 1);
	gestionErr(res,"semop0", idp );
	
	printf("idp = %i : obtient sem0\n", idp);
	while( ptrMemP[idp] < nombre_de_pixel )
	{
		printf("idp = %i : zone[idp]=%i \n",idp , ptrMemP[idp]);
		// inc nombre de zones parcourus
		int r = rand() % 4;	
		sleep(r);
		
		ptrMemP[idp]++;
		
		// inc nombre de semaphores a demander 
		


		// si il s'agit pas du dernier processus
		if( idp != (nombre_de_procesus - 1) )
		{

			// rajoute un semaphore au procesus de droite
			printf("idp = %i : rajoute sem droite\n",idp );
			res = semop(idSem, &opp , 1) ;
			gestionErr(res,"semop1", idp );
		}

		// attente sur semaphores 
		printf("idp = %i : demande sem1\n", idp);
		res = semop( idSem , &opv , 1);
	}

}


int main(int argc , char ** argv)
{
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	gestionErr(sesame,"ftok");

	int idSem = semget(sesame, nombre_de_procesus , 0600);
	gestionErr(idSem,"semget");

	int idMem = shmget(sesame, nombre_de_procesus*sizeof(unsigned int),0600);
	gestionErr(idMem,"shmget");

	
	unsigned int *ptrMemP = (unsigned int*)shmat(idMem,NULL,0);
	gestionErr((void *) ptrMemP,"shmat");

	pid_t pid;
	for(int i =0 ; i< nombre_de_procesus ; i++)
	{	
		pid = fork();	
		if (pid == 0)
		{
			traitement_image(i,idSem , ptrMemP);
			return 0;
		}
	}

	wait();
	return 0;
}
