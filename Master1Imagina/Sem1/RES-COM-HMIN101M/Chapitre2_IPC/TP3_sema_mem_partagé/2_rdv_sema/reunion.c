#include "commons.h"




int main(int argc , char **argv)
{
	// sémaphore +1
	if (argc != 2)
	{
	 	printf("veuillez rentrer le nombre de participant au rdv\n");
	 	return -1;
		
	}
	int res;
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	gestionErr(sesame,"ftok");

	int idSem = semget(sesame, 1 , 0600);
	gestionErr(idSem,"semget");
	
	// nombre de participant de la reunion
	int nbr_par=atoi(argv[1]);

	// opération consistant à attendre que le nombre
	// de sémaphores soit égal à 0.
	struct sembuf opwait={(u_short)0,(short)0,SEM_UNDO};
		
	// opération consistant à crée 
	// nbr_par sémaphores égal au nombre de participant 
	// dans la réunion
	semun usem ;
	usem.val=nbr_par;
	res=semctl (idSem, 0, SETVAL, usem);
	gestionErr(res,"semctl init");
	
	
	unsigned short sem_tab [1];
	usem.array=sem_tab;

	// Contrôle de sémaphores
	res=semctl (idSem, 0, GETALL, usem);
	gestionErr(res,"semctl");
	printf("nombre de sem : %i\n",usem.array[0]);

	 
	int pid;
	
	for(int i=0 ;i<nbr_par ; i++)
	{
		pid=fork();
		if(pid == 0)
		{
			sleep(1);
			execl("./tache","tache",(char*) 0);
			printf("erreur execl %i\n",getpid());
		}
		
	}


	printf("j'attends que tout le monde arrive à la réunion\n");
	res=semop(idSem, &opwait , 1);
	gestionErr(res,"semop");

	res=semctl (idSem, 0, GETALL, usem);
	gestionErr(res,"semctl");
	printf("nombre de sem : %i\n",usem.array[0]);

	printf("Tout le monde est la!\n");




	return 0;
}