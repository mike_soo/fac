#ifndef commonsTP_h
#define commonsTP_h


#define NB_J_PAR_EQ 5
#define NB_EQS 2

#define NB_SEMS (NB_J_PAR_EQ + 1)
#define IND_REUNION NB_J_PAR_EQ

#endif