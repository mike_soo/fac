#include "commonsTP.h"


int main(int argc , char ** argv)
{
	
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	int res;

	
	
	// get id de la memoire partagé
	int idMem = shmget(sesame, sizeof(unsigned int),0600);
	gestionErr(idMem,"shmget");
	
	// get pointeur vers mémoire partagé
	int *ptrMemP = (int*)shmat(idMem,NULL,0);
	gestionErr((void *) ptrMemP,"shmat");

	// get id tableau de sémaphores
	int idSem = semget(sesame,0,0);
	gestionErr(idSem,"semget");
	// creation d'operation des sémaphores
	// semaphore +1
	struct sembuf opp={(u_short)0,(short)1,SEM_UNDO};
	// semaphore -1
	struct sembuf opv={(u_short)0,(short)-1,SEM_UNDO};
	srand(time(NULL));
	int r;
	int nbVoitIn;
	while(1)
	{
		
		
		//On demande un sémaphore
		sleep(2);
		
		
		res=semop(idSem,&opv,1);
		gestionErr(res,"semop");
		
		printf("total place disponible %i \n",*ptrMemP);
		
		// nombre de voiture à l’intérieur du parking
		nbVoitIn = 20 - *ptrMemP;


		if(nbVoitIn > 0)
		{
			r=(rand() % (nbVoitIn )) + 1;

			(*ptrMemP)+=r;
			printf("nombre de voiture sortante %i \n",r);
			printf("total place disponible %i post sorti \n",*ptrMemP);
			
		}
		
		res=semop(idSem,&opp,1);
		
		
	}
	



	return 0;
}