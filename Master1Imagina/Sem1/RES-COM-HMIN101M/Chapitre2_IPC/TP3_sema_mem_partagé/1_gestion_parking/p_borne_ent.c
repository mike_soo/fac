#include "commonsTP.h"



int main(int argc , char ** argv)
{
	struct dem_ent dem_e;
	char chemin[20] = "./cle.txt";
	
	key_t sesame =ftok(chemin,1);
	gestionErr(sesame,"ftok");
	
	int res;

	int f_id=getFileMessCle(sesame,chemin, 1);
	gestionErr(f_id,"getFileMessCle");

	
	// get id de la memoire partagé
	int idMem = shmget(sesame, sizeof(unsigned int),0600);
	gestionErr(idMem,"shmget");
	
	// get pointeur vers mémoire partagé
	int *ptrMemP = (int*)shmat(idMem,NULL,0);
	gestionErr((void *) ptrMemP,"shmat");

	// get id tableau de sémaphores
	int idSem = semget(sesame,0,0);
	gestionErr(idSem,"semget");
	// creation d'operation des sémaphores
	// semaphore +1
	struct sembuf opp={(u_short)0,(short)1,SEM_UNDO};
	// semaphore -1
	struct sembuf opv={(u_short)0,(short)-1,SEM_UNDO};
	srand(time(NULL));
	int enAtt=0;


	while(1)
	{	
		sleep(1);
		printf("\nnb_place_dispo : %i , ",*ptrMemP);
		printf("nb_de_voiture_en_attente: %i\n", enAtt);
		
		if(enAtt == 0)
		{
			printf("j'attend une voiture\n");
			res=msgrcv(f_id, (void*) &dem_e,sizeof(struct dem_ent)-sizeof(long),BORNE_ENT,0);
			enAtt+=dem_e.nb_voiture_dem_ent;
			printf("demande de voiture recu avec %i voitures\n",dem_e.nb_voiture_dem_ent);
		}
		//gestionErr(res,"string");
		//On demande un sémaphore
		res=semop(idSem,&opv,1);
		gestionErr(res,"semop");
		
		while (*ptrMemP > 0 && enAtt>0)
		{
			(*ptrMemP)--;
			enAtt--;	
		}
				
		printf("nb_place_dispo post traitement : %i , ",*ptrMemP);
		printf("nb_de_voiture_en_attente post traitement: %i\n", enAtt);
		// printf("voiture rentre nb_place_dispo : %i \n",*ptrMemP);
		res=semop(idSem,&opp,1);



	}
	



	return 0;
}