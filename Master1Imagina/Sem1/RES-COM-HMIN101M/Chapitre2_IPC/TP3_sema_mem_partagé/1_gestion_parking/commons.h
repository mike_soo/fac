#ifndef commons_h
#define commons_h

#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>

int getFileMessCle(key_t sesame, char * chemin, int canal);
int gestionErr(int e,char const *f);
int gestionErr(void* e,char const *f);
void sig_handler(int signo);

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};

#endif