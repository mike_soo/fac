#include "Q1_2structs.cpp"

using namespace std;

int main()
{
	key_t sesame =ftok("./cle.txt",1);
	if(sesame < 0) 
	{
		cout<<"ftok():"<<strerror(errno)<<endl;
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0664);
	if(f_id < 0) 
	{
		cout<<"msgget():"<<strerror(errno)<<endl ;
		return -1;
	}

	struct req requete;
	struct rep reponse;
	char buf[33];
	while(1)
	{
		msgrcv(f_id,(void *) &requete,sizeof(struct req),0,0);
		switch(requete.op)
		{
			case '*' :
				printf("%i * %i",requete.a,requete.b);
				reponse.r = requete.a * requete.b;
				// snprintf(buf, sizeof(int), "%d", requete.a);
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;
			
			case '+':
				printf("%i + %i",requete.a,requete.b);
				reponse.r = requete.a + requete.b;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;
			
			case '-':
				printf("%i - %i",requete.a,requete.b);
				reponse.r = requete.a - requete.b;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;

			case '/':
				printf("%i - %i",requete.a,requete.b);
				reponse.r = requete.a / requete.b;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;

		}
		printf("message recu\n");
	}

	if (msgctl(f_id, IPC_RMID,NULL) <0)
	{
		cout<<"msgctl() : "<<strerror(errno)<<endl;
	}


	return 0;
}
