
#include "Q1_2structs.cpp"
using namespace std;

int main()
{
	key_t sesame =ftok("./cle.txt",1);
	if(sesame < 0) 
	{
		cout<<"ftok():"<<strerror(errno)<<endl;
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0664);
	if(f_id < 0) 
	{
		cout<<"msgget():"<<strerror(errno)<<endl ;
		return -1;
	}


	struct req requete;
	
	int res = 2;

	printf("Saisir calcul:\n");
	while (res >= 2) 
	{	int i=0;
		char buf[100];
		fgets(buf, sizeof(buf), stdin);
		// res = sscanf(buf, "%s %s %s", buf);    
		printf("%s", buf);
		char *token=strtok (buf," ");
		
		printf("start switch case\n");
		while (token != NULL)
		{
			// printf("%s\n",token);

			switch (i)
			{ 
				case 0:
					requete.a = atoi(token);
					break;

				case 1:
					requete.op = *token;
					break;

				case 2:
					requete.b = atoi(token);
					break;
			}
			token = strtok (NULL, " ");

			i++;
		}

		res=msgsnd(f_id,(void *) &requete,sizeof(struct req),0);
		if (res <0)
		{
			cout<<"msgsnd():"<<strerror(errno)<<endl;
		}


		struct rep reponse;
		res=msgrcv(f_id,(void *) &reponse,sizeof(struct req),0,2);
		if( res < 0)
		{	
			cout<<"msgrcv():"<<strerror(errno)<<endl;
		}
		cout<< "rep="<<reponse.r<<endl;
   	}
   	
   	printf("Fin de saisie\n");

	
	


	return 0;
}
