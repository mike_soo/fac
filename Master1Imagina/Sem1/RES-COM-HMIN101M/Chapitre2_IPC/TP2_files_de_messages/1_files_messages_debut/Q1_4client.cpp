
#include "Q1_2structs.cpp"
using namespace std;

int main()
{
	
	key_t sesame =ftok("./cle.txt",1);
	if(sesame < 0) 
	{
		cout<<"ftok():"<<strerror(errno)<<endl;
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0664);
	if(f_id < 0) 
	{
		cout<<"msgget():"<<strerror(errno)<<endl ;
		return -1;
	}

	printf("key_t sesame=%x ",sesame);
	printf("key_t f_id=%i\n",f_id);

	struct req requete;
	
	

	printf("Saisir calcul:\n");
	while (/*res >= 2*/ 1) 
	{	int i=0;
		char buf[100];
		fgets(buf, sizeof(buf), stdin);
		
		// res = sscanf(buf, "%s %s %s", buf);    
		printf("%s", buf);
		char *token=strtok (buf," ");
		
		
		while (token != NULL)
		{
			// printf("%s\n",token);

			switch (i)
			{ 
				case 0:
					requete.a = atoi(token);
					break;

				case 1:
					requete.op = *token;
					break;

				case 2:
					requete.b = atoi(token);
					break;
			}
			token = strtok (NULL, " ");

			i++;
		}
		int res;
		// requête.mtype contiens l’opération souhaité (choix de 
		// calculatrice):
		// 1->+, 2->-, 3->*, 4->:
		struct reqsansop requetesansop;
		
		requetesansop.a=requete.a;
		requetesansop.b=requete.b;
		requetesansop.mtypeClient=ETIQ_CLIENT1;
		
		switch(requete.op)
		{
			case '+':
				requetesansop.mtypeDest=SOM;
				res=msgsnd(f_id,(void *) &requetesansop,sizeof(struct reqsansop),0);
				break;
			
			case '-':
				requetesansop.mtypeDest=MOINS;
				res=msgsnd(f_id,(void *) &requetesansop,sizeof(struct reqsansop),0);
				break;
				
			case '*' :
				requetesansop.mtypeDest=FOIS;
				res=msgsnd(f_id,(void *) &requetesansop,sizeof(struct reqsansop),0);
				break;
			
			case '/':
				requetesansop.mtypeDest=DIV;
				res=msgsnd(f_id,(void *) &requetesansop,sizeof(struct reqsansop),0);
				break;
			
			default :	
				printf("Error switch");
				break;
		}

		if (res <0)
		{
			cout<<"msgsnd():"<<strerror(errno)<<endl;
		}

		printf("j’attends une réponse\n" );
		struct rep reponse;
		res=msgrcv(f_id,(void *) &reponse,sizeof(struct rep),ETIQ_CLIENT1, 0);
		if( res < 0)
		{	
			cout<<"msgrcv():"<<strerror(errno)<<endl;
		}
		cout<< "rep="<<reponse.r<<endl;
   	}
   	
   	printf("Fin de saisie\n");

	
	


	return 0;
}
