Q1.6

Si ipc toujours active, et que la clé et modifié, il n'y a pas de changement ni au niveau de la clé, ni au niveau de l'id de la file de messages.

Si on désactive la file de message (avec ipcrm msg <id>) et qu'on modifie le contenu du fichier clé, on aura effectivement l'identifiant la file de message qui changera mais sans changer la valeur key (adresse du fichier clé).


Si on désactive la file de message (avec ipcrm msg <id>) et qu'on efface le fichier clé pour en refaire un nouveau, on aura effectivement l'identifiant la file de message et l'adresse key du fichier clé qui changera mais sans changer la valeur key (adresse du fichier clé).

