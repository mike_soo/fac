#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>


#define ETIQ_CLIENT1 1
#define ETIQ_CLIENT2 2
#define ETIQ_CLIENT3 3
#define ETIQ_CLIENT4 4

#define SOM 11 
#define MOINS 22
#define FOIS 33
#define DIV 44
struct req{
	long mtype;
	int a,b;
	char op;
};

struct rep{
	long mtype;
	int r;
};

struct reqsansop{
	long mtypeDest;
	long mtypeClient;
	int a,b;

};