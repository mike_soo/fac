
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>

#include "commons.h"

#define PERE (long) 1
#define FILS (long) 2
extern bool sig_intACT;
struct decomposition
{
	long mtype;
	int d;

};

struct a_decomposer
{
	long mtype;
	int n;
	
};

int getFileMessCle(char * chemin, int canal)
{

	key_t sesame =ftok(chemin,canal);
	if(sesame < 0) 
	{
		printf("ftok():%s\n",strerror(errno));
		return -1;
	}
	
	int idFile =msgget(sesame, IPC_CREAT | 0600	);
	if(idFile < 0) 
	{
		printf("msgsnd():%s\n",strerror(errno));
		return -1;
	}

	return idFile;
}


void decomposeNbPremier(int idFile , int fils_id)
{
	struct a_decomposer a_dec,a_dec1, a_dec2;
	a_dec1.mtype = FILS;	
	a_dec2.mtype = FILS;	
	
	struct decomposition dec;
	dec.mtype = PERE;

	bool decompose;
	
	// reçois de structure contenant chiffre à décomposer
	int res=msgrcv(idFile, (void*) &a_dec,sizeof(struct a_decomposer)-sizeof(long),FILS,0);
	if(res < 0)
		{printf("\tfils_id=%i decomposeNbPremier msgrcv():%s\n",fils_id,strerror(errno));}	

	printf("\tfils_id=%i : a_dec.n=%i \n",fils_id,a_dec.n);  
	
	
	while(a_dec.n != -1)
	{
		decompose=false;
		// on cherche une décomposition

		for(int i = sqrt(a_dec.n); i >= 2 ; i-- )
		{
			if( a_dec.n % i ==0 )
			{
				decompose=true;
				a_dec1.n = a_dec.n/i;
				a_dec2.n = i;
				break;
			}
		}
		// Si le fils a réussi une décomposition, on envoi les deux nombres
		// sinon on envois un nombre avec un indication informant que le
		// nombre n'a pas été décomposer car il est lui même un nombre
		// premier.
		
		
		if(!decompose)
		{
			// nombre à décomposer est un nombre premier,
			// on enverra une décomposition au père
			
			dec.d = a_dec.n;
			res=msgsnd(idFile,(void* )& dec , sizeof(struct decomposition) - sizeof(long),0);
			printf("\tfils_id=%i : dec.d=%i\n",fils_id,dec.d);	
		}
		else 
		{
			res=msgsnd(idFile,(void* )& a_dec1 , sizeof(struct a_decomposer) - sizeof(long),0);	
			gestionErr(res,"fils msgsnd(a_dec1)",fils_id);
			res=msgsnd(idFile,(void* )& a_dec2 , sizeof(struct a_decomposer) - sizeof(long),0);	
			gestionErr(res,"fils msgsnd(a_dec2)",fils_id);
			
			printf("\tfils_id=%i : a_dec1.n=%i , a_dec2.n=%i\n \n",fils_id,a_dec1.n,a_dec2.n);	
		}
		
		
		printf("\tfils_id=%i : attends nb à décomposer\n",fils_id);
		
		res=msgrcv(idFile,(void *) &a_dec,sizeof(struct a_decomposer) - sizeof(long),FILS,0);
		printf("\tfils_id=%i : a_dec.n=%i\n",fils_id,a_dec.n);
	}
	printf("\tfils_id=%i : stop\n",fils_id);
}


void genereNbPremier(int idFile,int n,int nbFils)
{
	struct a_decomposer a_dec;
	struct decomposition dec;

	a_dec.mtype=FILS;
	a_dec.n=n;
	
	
	// Envoi dans la file le nombre à décomposer.
	int res=msgsnd(idFile,(void* )&a_dec , sizeof(struct a_decomposer)-sizeof(long),0);
	if(res < 0){printf("genereNbPremier msgsnd():%s\n",strerror(errno));}	
	
	int mult=1;
	while (mult!=n)
	{	
		res=msgrcv(idFile, (void *)&dec,sizeof(struct decomposition) -sizeof(long), PERE, 0);	
		mult *= dec.d; 
		printf("mult = %i\n" , mult);
		sleep(1);
	}
		
	
	for(int i=0 ;  i< nbFils ; i++)
	{
		a_dec.n = -1;
		res=msgsnd(idFile,(void*)&a_dec , sizeof(struct a_decomposer)-sizeof(long),0);
		if(res<0) printf("erreur fin %s",strerror(errno));

	}


}


int main(int argc, char *argv[])
{
	pid_t child_pid;
	
	int nbFils,nb_a_dec;
	char chemin[40]="./cle.txt";	
	

	int idFile=getFileMessCle(chemin,1);
	
	if(argc>2)
	{
		nbFils=atoi(argv[1]);
		nb_a_dec=atoi(argv[2]);
		printf("nbFils=%i nb_a_dec=%i\n",nbFils,nb_a_dec);
	}
	else
	{
		printf("rentrez arguments: nbrDeFils et le nb à décomposer");
		return -1;
	}

	
	
	printf("Pere: Création de %i fils\n",nbFils);
	
 	
 	for(int i=0 ; i < nbFils  ; i++)
	{
		printf("Creation i=%i\n" , i);
		child_pid = fork();
		
		if(child_pid <0)
		{
			printf("fork(): %s\n",strerror(errno));
			abort();
		}
		
		if (child_pid == 0)
		{
			/* Nous sommes dans le processus fils. */	
			decomposeNbPremier(idFile,i);
			
			return 1;
		} 
		
	}
	

	child_pid = fork();
	if(child_pid ==0 )
	{
		genereNbPremier(idFile,nb_a_dec,nbFils);
		return 0;	
	}
 	else
 	{
 		if (signal(SIGINT, sig_handler) == SIG_ERR) {
        	fputs("Erreur lors de signal handler.\n", stderr);
        	return EXIT_FAILURE;
    	}
 		while(1)
		{
			
			sleep(1);
		
			if(sig_intACT)
			{
				break;
			}	
		}		
 	}
 	

	int res=shmctl(idFile,IPC_RMID,0);
	gestionErr(res,"shmctl idFile");	
	
 

	
	return 0;
}