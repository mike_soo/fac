#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>

#define PERE (long) 1
#define FILS (long) 2

struct decomposition
{
	long mtype;
	int a,b;

};

struct a_decomposer
{
	long mtype;
	int n;
	
};

int getFileMessCle(char * chemin, int canal)
{

	key_t sesame =ftok(chemin,canal);
	if(sesame < 0) 
	{
		printf("ftok():%s\n",strerror(errno));
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0666	);
	if(f_id < 0) 
	{
		printf("msgsnd():%s\n",strerror(errno));
		return -1;
	}

	return f_id;
}


void decomposeNbPremier(int f_id , int fils_id)
{
	struct a_decomposer a_dec;
	
	struct decomposition dec;
	dec.mtype=PERE;
	
	bool decompose;
	// bool decompose=false;
	
	// printf ("\tfils_id=%i\n",fils_id);
	// printf("\tfils : f_id=%i\n",f_id );
	// reçois de structure contenant chiffre à décomposer
	
	int res=msgrcv(f_id, (void*) &a_dec,sizeof(struct a_decomposer)-sizeof(long),FILS,0);
	if(res < 0){printf("\tfils_id=%i decomposeNbPremier msgrcv():%s\n",fils_id,strerror(errno));}	

	printf("\tfils_id=%i : a_dec.n=%i \n",fils_id,a_dec.n);  
	
	
	while(a_dec.n != -1)
	{
		decompose=false;
		// on cherche une décomposition

		for(int i = sqrt(a_dec.n); i >= 2 ; i-- )
		{
			if( a_dec.n % i ==0 )
			{
				decompose=true;
				dec.a = a_dec.n/i;
				dec.b = i;
				break;
			}
		}
		// Si le fils a réussi une décomposition, on envoi les deux nombres
		// sinon on envois un nombre avec un indication informant que le
		// nombre n'a pas été décomposer car il est lui même un nombre
		// premier.
		
		
		if(!decompose)
		{
			// nombre à decomposer est un nombre premier,
			// on enverra une décomposition au père avec 
			// le premier entier = 0, le père pourra donc 
			// reconnaître que la partie droite est le nombre premier
			// que le fils courant n'a pas pu décomposer.
			dec.a=0;
			dec.b=a_dec.n;
		}
		
		res=msgsnd(f_id,(void* )& dec , 2*sizeof(int),0);
		printf("\tfils_id=%i : dec.a=%i dec.b=%i\n",fils_id,dec.a,dec.b);  
		
		if(res < 0)
		{
			printf("msgsnd():%s\n",strerror(errno));
		}
		printf("\tfils_id=%i : attends nb à décomposer\n",fils_id);
		
		res=msgrcv(f_id,(void *) &a_dec,sizeof(int),FILS,0);
		printf("\tfils_id=%i : a_dec.n=%i\n",fils_id,a_dec.n);
	}
	printf("\tfils_id=%i : stop\n",fils_id);
}


void genereNbPremier(int f_id,int n,int nbFils)
{
	struct a_decomposer a_dec;
	struct decomposition dec;

	a_dec.mtype=FILS;
	a_dec.n=n;
	
	
	// Envoi dans la file le nombre à décomposer.
	int res=msgsnd(f_id,(void* )&a_dec ,sizeof(int),0);
	if(res < 0){printf("genereNbPremier msgsnd():%s\n",strerror(errno));}	
	
	
	
	int mult=1;
	while (mult!=n)
	{	
		printf("mult=%i\n",mult);
		printf("père: j'attends décomposition\n");
		res=msgrcv(f_id,(void *)&dec,2*sizeof(int),PERE,0);
		
		printf("père: décomposition reçu: a=%i b=%i\n",dec.a,dec.b);
		if(res < 0){printf("genereNbPremier msgrcv():%s\n",strerror(errno));}		
		
		
		if(dec.a!=0)
		{	
			// On envois les deux nombres décomposé.
			a_dec.n=dec.a;
			res=msgsnd(f_id,(void* )&a_dec , sizeof(int),0);
			printf("pere: A décomposer >%i\n",a_dec.n);
			if(res < 0)	{printf("genereNbPremier msgrcv():%s\n",strerror(errno));}	

			a_dec.n=dec.b;
			res=msgsnd(f_id,(void* )&a_dec , sizeof(int),0);
			printf("pere: A décomposer >%i\n",a_dec.n);
			if(res < 0)	{printf("genereNbPremier msgrcv():%s\n",strerror(errno));}	
		}
		else
		{
			mult*=dec.b;
		}
	}
	printf("mult=%i\n",mult);
	a_dec.n=-1;
	for(int i=0 ;  i< nbFils ; i++)
	{
		res=msgsnd(f_id,(void*)&a_dec , sizeof(int),0);
		if(res<0) printf("erreur fin %s",strerror(errno));

	}


}


int main(int argc, char *argv[])
{
	pid_t child_pid;
	
	int nbFils,nb_a_dec;
	char chemin[40]="./cle.txt";	
	
	int f_id=getFileMessCle(chemin,1);
	if (msgctl(f_id, IPC_RMID,NULL) <0){printf("msgctl() : %s",strerror(errno));}

	f_id=getFileMessCle(chemin,1);
	
	if(argc>2)
	{
		nbFils=atoi(argv[1]);
		nb_a_dec=atoi(argv[2]);
		printf("nbFils=%i nb_a_dec=%i\n",nbFils,nb_a_dec);
	}
	else
	{
		printf("rentrez arguments: nbrDeFils et le nb à décomposer");
		return -1;
	}
	printf("Pere: Création de %i fils\n",nbFils);
	
 	
 	for(int i=0 ; i< nbFils  ; i++)
	{
		child_pid = fork();
		
		if(child_pid <0)
		{
			printf("fork(): %s\n",strerror(errno));
			abort();
		}
		
		if (child_pid == 0)
		{
			/* Nous sommes dans le processus fils. */	
			decomposeNbPremier(f_id,i);
			
			return 1;
		} 
		else 
		{
	
		 	/*proc Pere. */
			printf("je suis le pere! pid=%i\n",getpid());	 	
		}
	}
 	genereNbPremier(f_id,nb_a_dec,nbFils);
	
	return 0;
}