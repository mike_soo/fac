1.
Fonctionnement générale du système:
Il s'agit donc de gérer une ressource commune propre à plusieurs processus
Pi (1 < i < N). On aura donc Pfile, un processus gérant l’accès à une file de messages partagé entre tous les processus évoqués pour mutualiser l’accès à la ressource partagé. 

2. 
On pourra utilise une file de messages pour matérialiser la file d'attente pour l'accès à la ressource commune.

3.
Une manière pour que Pfile prenne en compte les requêtes, est celui de le mettre à l’écoute de messages rentrant dans une file de messages partagés entre tout les processus voulant avoir accès à la variable partagé. 
Pfile aura besoin d'entrés sorties bloquants dans le sens ou, il sera en attente de demande d'accès à la variable partagé (donc en attente de message dans la file), puis en attente bloquante lors que Pfile attendra un message informant la fin d'un accès à la variable partagé donc en attente de message dans la file d'un message provenant uniquement du processus ayant un accès à la ressource partagé. Cette deuxième attente est indispensable afin d'assurer l’exclusion d'utilisation de variable partagé à un seul processus. 

Il faudra donc que Pfile vérifie la présence de messages, et s'il en existe,
il faut qu'il vérifie quel est le processus émetteur Pem afin de lui accorder l’accès à la ressource partagé. Puis, Pfile devra vérifier quand est ce Pem aura terminé de manipulé la variable, en restant en attente un message spécifique de Pem permettant d'informer que son accès à la variable est terminé. Pendant cette attente, d'autre messages de demande d’accès à la mémoire partagé pourront arriver, dans ce cas, Pfile sera uniquement à la recherche du message de Pem, ce qui aura comme conséquence de mettre en attente tout Pi!=Pem.

Allons un peu plus loin:

1. Cas de pannes, plusieurs scénario possible.
	Si Pi disparaît avant d'avoir demandé un accès à la structure partagé:
	Taches sur la variable partagé pourrait ne pas être accomplis, pourrait avoir des effet sur le comportement des processus dépendant de la manipulation à effectuer par le processus décédé.
	Solution :
		Créer un tableau dans Pfile permettant de mémoriser les identifiant chaque processus lors de la demande d'accès à la structure pour ainsi ne pas permettre le déroulement du programme si un processus devait manipulé le segment partagé avant un autre. On devra donc implémenter dans Pfile des attentes de messages non bloquante afin de vérifier à chaque t secondes, si l'identifiant du processus existe toujours dans le système. 
	Si Pi disparaît pendant l'accès à la structure partagé:
		idem, une attente non bloquante sur Pfile permettant de vérifier à chaque t secondes si Pi existe toujours dans la liste des processus en cours d’exécution.

2. Sans le concept de mémoire partagé, on pourrait s'orienter vers l'utilisation des tubes(pipe) pour la communication entre processus. Une solution pour établir le même comportement serait celui de relié tout les Pi avec Pfile en utilisant 2 tubes par Pi. Pour chaque Pi le premier sera pour envoyer des requêtes vers Pfile et le deuxième sera donc orienté de Pfile vers Pi pour générer des réponses. Pfile aura donc un tableau de tubes entrant et sortant vers chaque Pi qui devra parcourir à la recherche d'un message rentrant puis ainsi modifier une ressource "commune" qui serait contenu dans Pfile puis répondre au Pi effectuant à la requête.
Optimisation: on pourrait juste garder un tube pour la comunication des Pi vers Pfile, on gagne en efficacité car Pfile se bloque lors qu'il y a pas de message puis, le message contenant information sur l'émetteur, Pfile pourra ainsi répondre apres avoir effectué des actions sur la structure partagé.

