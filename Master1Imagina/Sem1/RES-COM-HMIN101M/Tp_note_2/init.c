#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>

#define NB_SEMS 2
#define BUS_DEP 0
#define BUS_ARR 1
#define NB_CASE_MEMSP 0

bool sig_intACT;


int gestionErr(int e,char const *f);
int gestionErr(void* e,char const *f);
int gestionErr(int e,char const *f,int id);
void sig_handler(int signo);

// Affiche l'état d'un tableau de sémaphore active 
// et d'une zone de mémore partagé en fonction des parametres.
void afficheSemsMemP( int idSem, int nb_sems , char * ptrMemP ,\
						unsigned int nb_case_memsP, unsigned int ptr_type_size);
union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};





int passager(int idSem , int idPas);
int bus(int idSem , int cap_max_passagers , int duree_voyage);

int main(int argc , char** argv)
{
	if(argc != 4)
	{
		printf("saisir cap_max_passagers nombre_de_passagers duree_voyage\n");
		exit(-1);
	}
	int cap_max_passagers = atoi(argv[1]);
	int nb_de_passagers = atoi(argv[2]);
	int duree_voyage = atoi(argv[3]);

	if (signal(SIGINT, sig_handler) == SIG_ERR) {
        fputs("Erreur lors de signal handler.\n", stderr);
        return EXIT_FAILURE;
    }
	
	char chemin[20] = "./cle.c";
	key_t sesame =ftok(chemin,1);
	
	
	// ====================
	// INIT SEMAPHORES
	// ====================
	unsigned short sems_vals [NB_SEMS];

	sems_vals[0]= cap_max_passagers;
	sems_vals[1]=0;


	int idSem = semget(sesame, NB_SEMS ,IPC_CREAT | 0600);
	gestionErr(idSem,"semget");
	

	semun s;
	s.array = sems_vals;

	int res = semctl( idSem , 0 , SETALL , s);
	gestionErr(res,"semctl");

	printf("creation du bus\n");
	int pid = fork();
	if(pid == 0 )
	{
		bus(idSem, cap_max_passagers,duree_voyage);
		return 0;
	}


	printf("creation des passagers\n");
	
	for(int i = 0 ; i < nb_de_passagers; i++ )
	{
		pid = fork();
		if (pid == 0)
		{
			passager(idSem,i);
			return 0;
		}
	}



    while(1)
	{



		

		sleep(1);
		
		if(sig_intACT)
		{
			break;
		}
	}	

	res=semctl(idSem,IPC_RMID,0);
	gestionErr(res,"semctl");	
		
	return 0;
}





void sig_handler(int signo)
{
  if (signo == SIGINT)
    {
    	printf("received SIGINT\n");
		sig_intACT = true;
	}
}


int gestionErr(int e , const char* f)
{
	if (e < 0)
	{
		printf("Erreur > %s : %s\n",f, strerror(errno));
		exit(-1);
	}
	else 
		return 1;

}

int gestionErr(void *e, const char* f)
{
	if((void*) e == (void*) -1)
	{
	
		printf("Erreur > %s : %s\n",f , strerror(errno));
		exit(-1);

	}
	else 
		return 1;
}

int gestionErr(int e, const char* f,int id)
{
	if (e < 0)
	{
		printf("id = %i Erreur > %s : %s\n",id,f , strerror(errno));
		exit(-1);
	}
	else 
		return 1;
}


void afficheSemsMemP( int idSem, int nb_sems , char * ptrMemP ,\
						unsigned int nb_case_memsP, unsigned int ptr_type_size)
{

	unsigned short  sems_vals[nb_sems];
	semun s;
	char * p = ptrMemP;
	s.array = sems_vals;
	int res = semctl(idSem, 0 /*semno ignored*/ ,GETALL ,  s.array);
	gestionErr(res , "afficheSemsMemP");
	printf("\n");
	for(int i = 0 ; i < nb_sems; i++)
	{
		printf("%-3i",i);
	}
	printf("\n\e[1;31m");
	
	for(int i = 0 ; i < nb_sems; i++)
	{
		printf("%-3i",s.array[i]);			
	}
	printf("\e[0m\n");
	
	// Affichage mémoire partagé (valeur de zone en cour de traitement)
	
	for(unsigned int i = 0 ; i < nb_case_memsP; i++)
	{
		printf("%-3i",i);
	}

	printf("\n\e[1;32m");
	unsigned int i = 0 ;
	for(p = ptrMemP ; i < nb_case_memsP ; p = p + ptr_type_size)
	{
		printf("%-3u",*p);			
		i++;
	}
	printf("\e[0m\n");
}