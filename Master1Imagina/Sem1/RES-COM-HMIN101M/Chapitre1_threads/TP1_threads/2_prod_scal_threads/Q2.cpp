#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

#define VECTORSIZE 5
using namespace std;

// v(a) = (a_1, a_2, a_3), v(b) de base 3,
// alors produit scalaire = a_1 * b_1 + a_2 * b_2 + a_3 * b_3
struct vecteur;
struct resultat;
struct opVectoriel;
void* tMulti(void *params);
void* tSomme(void *params);

struct vecteur
{
	float *values;
};


struct resultat
{	
	float *res;
};

struct opVectoriel
{
	struct vecteur* vecteurs[2];
	struct resultat* resultat;
	pthread_cond_t *cond;
	pthread_mutex_t *verrou;
	unsigned int *cpt;
	unsigned int id;
};

void* tSomme(void *params)
{
	opVectoriel *opV = (struct opVectoriel*) params;
	pthread_mutex_lock(opV->verrou);
	while( *(opV->cpt) < VECTORSIZE )
	{
		cout<<"boucle"<<endl;
		pthread_cond_wait(opV->cond,opV->verrou);
	}

	cout<<"resultat="<<*(opV->resultat->res)<<endl;
	pthread_mutex_unlock(opV->verrou);
	return NULL;
}

void* tMulti(void *params)
{
	opVectoriel *opV = (struct opVectoriel*) params;
	
	// Chaque thread est initialisé avec un id passé en parametre qui
	// peut prendre comme valeur un entier compris entre dans 
	// {0,1..,VECTORSIZE-1}.
	// Cette id correspond egalement à quel dimension d-1 sera effectué la multiplication.
	// ex: si id = 1, v1=(1,6,3) et v2=(2,3.8,1.6), le thread courrant 
	// rajoutera dans le resultat de 6*3.8.

	float res= (opV->vecteurs[0]->values[opV->id]) * (opV->vecteurs[1]->values[opV->id]) ;
	pthread_mutex_lock(opV->verrou);
	*(opV->resultat->res) += res; 
	*(opV->cpt) += 1;
	if(*opV->cpt > VECTORSIZE)
	{
		pthread_cond_signal(opV->cond);
	}

	pthread_mutex_unlock(opV->verrou);

	return NULL;
}



int main(int argc, char* argv[]){
	pthread_mutex_t verrou = ((pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER );
	pthread_cond_t cond = ((pthread_cond_t)PTHREAD_COND_INITIALIZER);


	struct opVectoriel opVs[VECTORSIZE];
	struct opVectoriel opVsR;

	unsigned int cpt=0;
	float res=0;

	struct vecteur vecs[2];
		
	float values1[VECTORSIZE]={1.5,2.6,3,0.3,5.6};
	float values2[VECTORSIZE]={3,8,9.6,0.2,2.4};
	
	vecs[0].values=values1;
	vecs[1].values=values2;		

	struct resultat resultat;
	resultat.res=&res;

	pthread_t idTv[VECTORSIZE];
	pthread_t idTr;
	
	for(int i=0;i<VECTORSIZE;i++)
	{
		for (int j=0;j<2;j++)
			opVs[i].vecteurs[j]=&vecs[j];

		opVs[i].cond=&cond;
		opVs[i].verrou=&verrou;
		opVs[i].resultat=&resultat;
		opVs[i].id=i;
		opVs[i].cpt=&cpt;

	
		pthread_create(&idTv[i],NULL,tMulti,(void*) &opVs[i]);
	}
	
	// opVsR.vecteurs=vecs;
	opVsR.cond=&cond;
	opVsR.verrou=&verrou;
	opVsR.resultat=&resultat;
	opVsR.id=VECTORSIZE;
	opVsR.cpt=&cpt;

	pthread_create(&idTr,NULL,tSomme,(void*) &opVsR);
	
	for(int i=0;i<VECTORSIZE;i++)
	{
		pthread_join(idTv[i],NULL);
	}
	pthread_join(idTr,NULL);
	
	return 0;
}
