#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

using namespace std;

struct OrganiserReunion{
	pthread_mutex_t verrou;
	pthread_cond_t cond;
	int nbDarriveAuRDV;
	int n;
};

void *fTravail(void *params)
{
	OrganiserReunion* orgReu= (OrganiserReunion*) params;
	bool dernier=true;
	int n=10000;
	// int T[n];
	// sleep(0.5);
	cout<<"je travail"<<endl;
	
	pthread_mutex_lock(&orgReu->verrou);
	//je suis arrivé au rdv
	//incrementation de variable commune à tous les thread
	((orgReu->nbDarriveAuRDV))++;

	//je previens que je suis arrivé a la reunion
	//broadcast sur variable conditionel
	
	while((orgReu->nbDarriveAuRDV) < orgReu->n)
	{
		cout<<"j'ai fini de travailler et je suis à la Reunion et j'attend"<<endl;
		cout<< "nombre d'arrivés "<<(orgReu->nbDarriveAuRDV)<<endl; 
		dernier=false;	
		pthread_cond_wait(&(orgReu->cond),&(orgReu->verrou));		
	}

	if(dernier)
	{	
		cout<<"j'ai fini de travailler et je suis à la Reunion et je réveil tout le monde"<<endl;
		cout<< "nombre d'arrivés "<<(orgReu->nbDarriveAuRDV)<<endl; 
		pthread_cond_broadcast(&(orgReu->cond));
	}
	else
	{
		cout<<"je me reveil"<<endl;
	}

	// chaque arrivant reveil quelqu'un 
	
	pthread_mutex_unlock(&(orgReu->verrou));
}

int main()
{	
	OrganiserReunion orgReu;
	orgReu.nbDarriveAuRDV=0;
	orgReu.verrou =(pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	orgReu.cond = (pthread_cond_t)PTHREAD_COND_INITIALIZER;
	int n=5;
	orgReu.n=n;
	pthread_t idT[n];


	for(int i=0;i<n;i++)
	{
		pthread_create (&idT[i],NULL,fTravail,(void*) &orgReu);
	}
	

	// cout<<"threads crées"<<endl;
	
	for (int i = 0 ; i< n; i++)
	{
		pthread_join(idT[i], NULL);
	}
	

  
  return 0;
}
