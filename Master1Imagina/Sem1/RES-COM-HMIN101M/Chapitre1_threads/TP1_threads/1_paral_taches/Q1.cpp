#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

void *T1(void* parent){
  cout << __FUNCTION__  << " : " << __TIME__  << " : Thread created [" << getpid() << "]" << endl;
  int *x = (int*) parent;
  for (int i=0; i<5; i++){
    cout << __FUNCTION__ << " : x = " << ++(*x) << endl;
    sleep(1);
  }
  cout << __FUNCTION__ << " : " << "Thread closed" << endl;
  pthread_exit(NULL);
}

void *T2(void* parent){
  
  cout << __FUNCTION__  << " : " << __TIME__  << " : Thread created [" << getpid() << "]" << endl;
  exit(1);
  int *x = (int*) parent;
  for (int i=0; i<5; i++){
    cout << __FUNCTION__ << " : x = " << ++(*x) << endl;
    sleep(1);
  }
  cout << __FUNCTION__ << " : " << "Thread closed" << endl;
  pthread_exit(NULL);

}

void *T3(void* parent){
  cout << __FUNCTION__  << " : " << __TIME__  << " : Thread created [" << getpid() << "]" << endl;
  int *x = (int*) parent;
  sleep(3);
  cout << __FUNCTION__ << " : x = " << ++(*x) << endl;
  cout << __FUNCTION__ << " : " << "Thread closed" << endl;
  pthread_exit(NULL);

}

void Q1(){
  pthread_t idT1, idT2;
  int x=0;
  cout << "Q1 : x = " << x << endl;
  if (pthread_create (&idT1, NULL, T1, &x) !=0)
    cerr << "erreur de creation" << endl;
  if (pthread_create (&idT2, NULL, T2, &x) !=0)
    cerr << "erreur de creation" << endl;
  if (pthread_create (&idT2, NULL, T3, &x) !=0)
    cerr << "erreur de creation" << endl;
  int res = pthread_join(idT1, NULL);
  res = pthread_join(idT2, NULL);
  cout << "Q1 : x = " << x << endl;
  // 1. Voir ci-dessus

  // 2. Si le processus meurt avant les threads, les fils décedent avec.

  // 3. Si une tâche fait appel à exit, elle tue le processus, donc
  // l'ensemble des threads de celui-ci (ref q2)
}

int main(int argc, char* argv[]){
  cout << "Q1" << endl;
  Q1();
  return 0;
}

