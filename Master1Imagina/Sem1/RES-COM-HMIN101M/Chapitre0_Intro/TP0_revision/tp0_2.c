#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void widthFork(int n){
  pid_t self = getpid();
  for (int i=0; i<n; i++){
    pid_t pid = fork();
    if (getpid() == self)
      printf("child created [%d] from parent [%d]\n", pid, getpid());
    else if(pid==1){
      sleep(5);
    }
  }
}

void heightFork(int n){
  if (n==0) return;
  pid_t pid = fork();
  if (pid==0){
    printf("child created [%d] from parent [%d]\n", getpid(), getppid());
    heightFork(n-1);
  } else {
    int status;
    wait(&status);
  }
}

void treeFork(int depth, int childrenCreated){ /* TODO */
  if (depth==0 || childrenCreated==2) return;
  pid_t pid;
  pid = fork();
  printf("child created [%d] from parent [%d], depth %d \n", getpid(), getppid(), depth);
  if (pid==0){                /* inside child */
    treeFork(depth-1, childrenCreated);
  } else {
    treeFork(depth, childrenCreated+1);
  }
  sleep(10);
}

void Q2(){
  printf("parent : %d\n", getpid());
  /* widthFork(3); */
  heightFork(3);
  /* treeFork(2, 0); */
}
