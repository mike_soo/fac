#define TABSIZE(X) sizeof(X)/sizeof(*X)

void Q11(){
  int a = 10;
  int b = 25;
  int *p = &b;
  int *pp = &a;

  /* expected :
   1 : *(&(*(*(&p))))
   = *(&(*(p)))
   = *(p)
   = *(&b)
   = b
   -> OK

   2 : *(&b-1)
   = *(&a)
   = a
   -> OK

   3 : *(*(&p)-1)
   =  *(*(&p)-1)
   =  *(p-1) = *(&b-1)
   = *(a)
   = adresse de a ?
   -> FAUX : a

   4 : *(*(&pp)+1)
   = *(pp+1) = *(&a+1) = *(&b) = b
   -> OK

   5 : *(&(*(*(&p))) - 1)
   = (&(*(p) - 1)
   = (&( *(b)))
   = *b
   -> FAUX : a

   */
  
  printf("1. %-20s: %d\n", "*(&(*(*(&p))))", *(&(*(*(&p)))));
  printf("2. %-20s: %d\n", "*(p-1)", *(p-1));
  printf("3. %-20s: %d\n", "*(*(&p)-1)", *(*(&p)-1));
  printf("4. %-20s: %d\n", "*(*(&pp)+1)", *(*(&pp)+1));
  printf("5. %-20s: %d\n", "*(&(*(*(&p))) - 1)", *(&(*(*(&p))) - 1)); 
}

void printtab(int *T, int n){
  printf("[");
  for (int i=0; i<n; i++){
    printf("%d",T[i]);
    if (i!=n-1) printf(", ");
  }
  printf("]\n");
}

int* extract(int* T, int n, int a, int b){ /* b >= a */
  int len = 0;                             /* length of res */
  int* res = malloc(sizeof(int) * len);
  for (int i=0; i<n; ++i){              /* i cursor inside T */
    if (a <= T[i] && T[i] <= b){
      ++len;
      /* int* aux = malloc(sizeof(int) * len); */
      res = (int*) realloc(res, len*sizeof(int));
      res[len-1] = T[i];
    }
  }
  return res;
}

int* inputTab(){
  printf("Taille du tableau : ");
  int n;
  scanf("%d", &n);
  int* res = malloc(sizeof(int) * n);
  for (int i=0; i<n; i++){
    printf("Element n°%d = ", i);
    scanf("%d", &res[i]);
  }
  return res;
}

int sumTab(int* T, int n){
  int res=0;
  for (int i=0; i<n; i++){
    res+=T[i];
  }
  return res;
}

int Q1(){
  printf("\nQ1.1\n");
  Q11();
  
  printf("\nQ1.2\n");
  int T[5] = {1,3,5,7,9};
  /* printf("%d\n", T); */
  /* printf("len = %d\n", sizeof(T)/sizeof(*T)); */
  printf("input : ");
  printtab(T, TABSIZE(T));
  int* T2 = extract(T, TABSIZE(T), 2, 5);
  printf("output : ");
  printtab(T2, TABSIZE(T2));

  printf("\nQ1.3\n");
  int* T3 = inputTab();
  printf("Somme : %d\n", sumTab(T3, TABSIZE(T3)));

}
