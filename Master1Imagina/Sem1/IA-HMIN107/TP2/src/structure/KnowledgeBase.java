package structure ;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import tp1CSP.Assignment;



public class KnowledgeBase{

	private FactBase fb,sfb;
	private RuleBase rb , rbPropositionalized;

	public KnowledgeBase()
	{
		fb = new FactBase();
		rb = new RuleBase();
		rbPropositionalized = new RuleBase();
		sfb = new FactBase();
		sfb.addAtoms(fb.getAtoms());
		
	}

	public KnowledgeBase(FactBase fb, RuleBase rb)
	{
		this.fb = fb;
		this.rb = rb;
		rbPropositionalized = new RuleBase();
		sfb = new FactBase();
		sfb.addAtoms(fb.getAtoms());

	}
	


	public KnowledgeBase(BufferedReader in) throws Exception 
	{
		this();
		// Les variables et domaines
		
		String[] factVars = in.readLine().split(";"); // Var;Val1;Val2;Val3;...
		
		for(int i =0 ; i< factVars.length ; i++ )
		{
			fb.addAtomWithoutCheck(new Atom (factVars[i]));
		}		

		String ruleVars = null;

		while ((ruleVars = in.readLine()) != null )
		{
			
			rb.addRule(new Rule(ruleVars));
			
		}
		sfb = new FactBase();
		sfb.addAtoms(fb.getAtoms());
		
		System.out.println("fb : " + fb.toString() );
		System.out.println("fb partiellement surchagé  : " + sfb.toString() );
		System.out.println("rb : " + rb.toString() );
		
	}
	
	public Rule getRule(int i )
	{
		return rb.getRule(i);
	}
	
	public FactBase getFactBase()
	{
		return fb;
	}
	
	public FactBase getSaturedFactBase()
	{
		return sfb;
	}
	
	public void showSaturedFactBase()
	{
		
		System.out.println(sfb.toString());
	}
	
	public Boolean contains(Atom atom)
	{
		return sfb.belongsAtom(atom);
	}
	
	public void FCnaiveStart()
	{	
		System.out.println("Start FC naive");
		this.FCnaive();
		System.out.println("End FC naive");
		System.out.println(sfb);
		
	}
	
	/**
	 * Rajout des predicats à la base de fait généré par des assignment 
	 * retrouvé par aplication du backtrack
	 * 
	 * @param assignments liste d'assingments contenant à chaque ligne
	 * des variables dans le format Vx ou V est un prefixe pour indiqué
	 * qu'il s'agit d'une variable et x represente le nom de la variable
	 * Cette forme de code l'information nous permetre de mettre en relation
	 * chaque variable presente dans assignments au predicat appartenant à 
	 * la conclusion de la regle utilisé pour généré des faits
	 * @param predicate conclusion de la régle utilisé auparavant pour 
	 * généré les fait contenue dans assignments.
	 */
	
	public ArrayList <ArrayList <Atom>> getHomsFromCSPAssignmentSolutions(ArrayList<Assignment>assignments , ArrayList <Atom> requete)
	{
		/**
		 * Homorphise de la requte en question généré à partir d'une
		 * assignation.
		 */
		ArrayList <Atom> homDeRequete = new ArrayList<Atom>();
		ArrayList <ArrayList <Atom>> ensHomDeRequete  = new ArrayList <ArrayList <Atom>>();
		for (Assignment a : assignments)
		{
			/*
			 * Resultat de la requete reconstruite à partir des assignations;
			 */
			homDeRequete.clear();
			for (Atom predicate : requete)
			{
				//System.out.println("Reconstruction from " + predicate);
				String reconsPredicate="";
				reconsPredicate += predicate.getPredicate();
				reconsPredicate +="(";
				
				for (int i = 0 ; i < predicate.getArity(); i++)
				{
					Term t = predicate.getArgI(i);
					if(t.isConstant())
					{
						reconsPredicate += t.toString();
					}
					else
					{
						reconsPredicate += a.get("V" + t.toString()).toString();
						
					}
					if(i < predicate.getArity() - 1) 
					{
						reconsPredicate+=",";
					}
				}
				//System.out.println("");
				reconsPredicate +=")";
				Atom reconsAtom = new Atom(reconsPredicate);
				//System.out.println("Atom reconstruit ="+ reconsAtom.toString());
				homDeRequete.add(reconsAtom);
				
			}
			ensHomDeRequete.add((ArrayList <Atom>) homDeRequete.clone()); 
			
		}
		
		System.out.println("Ensemble d'homomorphismes de la requete " + requete + ":");
		
		for (ArrayList <Atom> homDeReq : ensHomDeRequete)
		{
			System.out.println(homDeReq);
		}
		return ensHomDeRequete;
		
		//sfb.addAtoms(atomsForFactBase);
	}
	
	
	private HashMap <String, Atom > atomsArrayToHashMap(ArrayList <Atom> atoms)
	{
		HashMap <String,Atom> hashmap = new HashMap <String,Atom> ();
		for(Atom atom : atoms)
		{
			hashmap.put(atom.toString(),atom);
			
		}
		return hashmap;
	}

	public void FCnaive()
	{
		FactBase newFacts = new FactBase();
		ArrayList <Boolean> appliedRules = new ArrayList <Boolean> ();
		
		
		for (int i = 0 ; i < rb.size(); i++)
		{
			appliedRules.add(Boolean.FALSE);
		}
		Boolean end = Boolean.FALSE;
		
		while (!end)
		{
			newFacts.getAtoms().clear();
//			Pour toute règle R = H à C de BR telle que Appliquée(R) = faux
			for (int i = 0 ; i < rb.size() ; i++ )
			{
				if(appliedRules.get(i) == Boolean.FALSE)
				{
					Rule rule = rb.getRule(i);
					
//					Si rule(hyp) est incluse dans sbf // rule est applicable
					//System.out.println(rule.toString() + " in? " + sfb.toString());
					if (sfb.ruleInFactBase(rule))
					{	
						System.out.println("\t in " );
						appliedRules.set(i, Boolean.TRUE);
//						Si C n'est ni dans sbf ni dans newFacts
						if(!sfb.belongsAtom((rule.getConclusion())) 
								&& !newFacts.belongsAtom(rule.getConclusion()) )
						{
							
							sfb.addAtomWithoutCheck(rule.getConclusion());
						}
					}
					else
					{
						//System.out.println("\tnot in");
					}
				}
			}
			if (newFacts.getAtoms().size() == 0)
			{
				end = Boolean.TRUE;
			}
			
		}
	}
	
	public void FCcompteursStart() throws InterruptedException
	{	
		System.out.println("Start FC compteurs");
		
		this.FCcompteurs();
		System.out.println("End FC compteurs");
		
		
	}
	
	
	
	public void FCcompteurs() throws InterruptedException
	{
		HashMap <String,Atom> toTreat = null;
		HashMap <String,Atom> sfbLocalMap = null;
		
		ArrayList <HashMap <String,Atom>> arrayrbmaps= new ArrayList <HashMap <String, Atom>> () ;
		
		//initialisation de tableau de atraiter (toTreat) avec les atoms de la base de faits
		
		toTreat=atomsArrayToHashMap(fb.getAtoms());
		sfbLocalMap= atomsArrayToHashMap(fb.getAtoms());
		
		for(int i = 0 ; i < rb.size() ; i++)
		{
			Rule rule = rb.getRule(i);
			HashMap <String,Atom> rbmap = null;
			rbmap= atomsArrayToHashMap(rule.getHypothesis());
			arrayrbmaps.add(rbmap);
//			System.out.println(arrayrbmaps.get(i).toString());
		}
		//System.out.println("rb.size()=" + rb.size()) ;
		ArrayList <Integer> counters = new ArrayList();
		
		
		
		for(int i = 0 ; i < rb.size() ; i++)
		{
			counters.add( rb.getRule(i).getHypothesis().size());
		}
		Atom a=null;
		while(toTreat.size()> 0)
		{	 
//			TimeUnit.SECONDS.sleep(1);
//			System.out.println("Counters : "+ counters.toString());
//			System.out.println("sfbLocalMap : " + sfbLocalMap.keySet());
//			System.out.println("toTreat : " + toTreat.keySet());
			
			//on extrait le premier atom à traiter 
			a = toTreat.get(toTreat.keySet().iterator().next());
			toTreat.remove(a.toString(),a);
			//System.out.println("en cours de traitement atom = " + a.toString());
			//Pour tout regle dans array de regles sous forme de hasmaps
		 	for(int i= 0 ; i < arrayrbmaps.size() ; i++)
			{	
//				si la regle (hashmap) i contient l'atom a (String)
				if (arrayrbmaps.get(i).get(a.toString()) != null )
				{
					//On décrémente le compteur associé la regle i) 
					counters.set(i, counters.get(i)-1);
					
					//si le compteur est passé à zero
					if (counters.get(i) == 0)
					{
						//System.out.println("Hyp : " + rb.getRule(i).getHypothesis() + " vrai");
						Atom ruleiConclusion = rb.getRule(i).getConclusion(); 
						// si l'atom de la conclusion n'est pas deja dans la base de fais surchargée 
						// ni dans les atoms à traiter 
						if ((sfbLocalMap.get(ruleiConclusion.toString())==  null) 
								&& (toTreat.get(ruleiConclusion.toString()) == null))
						{
							//System.out.println("\tOn rajoute " + ruleiConclusion.toString() );
							// on rajoute l'atom de conclusion de la regle i dans la base de fais surchargée
							sfbLocalMap.put(ruleiConclusion.toString() , ruleiConclusion);
							//on rajoute l'atom de la conclusion de la regle i dans la base faits à traiter
							toTreat.put(ruleiConclusion.toString(), ruleiConclusion);
							
							//System.out.println("\tdans sfbLocalMap" + sfbLocalMap.keySet());
							//System.out.println("\tdans toTreat" + toTreat.keySet());
							
						}
						
					
						
					}
				}
			}
		}
	
		sfb.getAtoms().clear();
		
		for (String atomPredicate : sfbLocalMap.keySet())
		{
			sfb.addAtomWithoutCheck(sfbLocalMap.get(atomPredicate));
		}
		
		
		
	}
	
	public Boolean BC3(Atom atomToProve)
	{
		 HashMap <String, Atom> notUsablesAtoms = new HashMap <String, Atom>();
		 HashMap <String, Atom> failedAtoms = new HashMap <String, Atom>();
		 HashMap <String, Atom> fbLocal = atomsArrayToHashMap(fb.getAtoms());
		 
		 HashMap <String ,ArrayList <Integer>> ruleIndsOfAtom= new HashMap<String , ArrayList<Integer>>();
		 
		 for(int i = 0 ; i < rb.size(); i++)
		 {	
			 Atom concl = rb.getRule(i).getConclusion();
			 
			 if(ruleIndsOfAtom.get(concl.toString())== null)
			 {
				 ruleIndsOfAtom.put(concl.toString(),new ArrayList<Integer>());
			 }
			 
			 ruleIndsOfAtom.get(concl.toString()).add(i);
		 }
		 
//		 System.out.println("tableau de conlusion (atom) associé à leurs "
//		 		+ "indices d'apparition dans le tableau de regles");
//		 for (String atomS : ruleIndsOfAtom.keySet() )
//		 {
//			 System.out.print(atomS+ ":");
//			 System.out.println(ruleIndsOfAtom.get(atomS).toString());
//		 }
		 return BC3Aux(atomToProve
				 	, notUsablesAtoms
				 	, fbLocal
				 	, ruleIndsOfAtom
				 	, failedAtoms);
		 
	}
	
	private Boolean BC3Aux(Atom atomToProve, HashMap <String, Atom> notUsablesAtoms
							,HashMap <String, Atom> fbLocal
							,HashMap <String ,ArrayList <Integer>> ruleIndsOfAtom
							,HashMap <String, Atom > failedAtoms)

	{
		System.out.println("atomToProve : "+ atomToProve.toString() );
//		try {
//			TimeUnit.SECONDS.sleep(1);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return Boolean.FALSE;
//		}
		if(fbLocal.get(atomToProve.toString()) != null)
		{
//			System.out.println("atomToProve : "+atomToProve +"proved" );
//			System.out.println("fbLocal : " + fbLocal.keySet().toString());
			return Boolean.TRUE;
		}
		
		
		if( ruleIndsOfAtom.get(atomToProve.toString()) == null ||
				failedAtoms.containsKey(atomToProve.toString()))
		{
			System.out.println("atomToProve : " + atomToProve +" not proved" );
			if(!failedAtoms.containsKey(atomToProve.toString() ))
			{
				failedAtoms.put(atomToProve.toString(),atomToProve);
			}
			else
			{
				System.out.println("in failedAtoms ");
			}
			System.out.println("failedAtoms : " + failedAtoms.keySet().toString());
			return Boolean.FALSE;
		}
		notUsablesAtoms.put(atomToProve.toString(),atomToProve);
		Boolean atomProved = Boolean.FALSE;
		for (int i : ruleIndsOfAtom.get(atomToProve.toString()))
		{
		
			//SI aucun des H1 .... HN n'appartient à notUsablesAtoms
			if(! ruleInNotUsablesAtoms(rb.getRule(i),notUsablesAtoms)
					&& !ruleInNotUsablesAtoms(rb.getRule(i),failedAtoms))
			{
				
				Rule rulei = rb.getRule(i);
				System.out.println("ruleToInspect:"+ rulei.toString());
				int j = 0;
				
				
				
				// L U {Q} on rajoute la conclusion à notUsablesAtoms
				//pour eviter des boucles
				
				
				
				while(j < rulei.getHypothesis().size())
				{
					Atom Hj = rulei.getAtomHyp(j);
					if(!BC3Aux(Hj,notUsablesAtoms,fbLocal,ruleIndsOfAtom,failedAtoms))
					{
						break;
					}
					j++;
				}
				
				if(j == rulei.getHypothesis().size())
				{
					atomProved = Boolean.TRUE;
					break;
				}
				
			}
			

		}
		
		System.out.println("removed from notUsablesAtoms : "+ atomToProve);
		notUsablesAtoms.remove(atomToProve.toString());
		System.out.println("notUsableAtoms : " + notUsablesAtoms.keySet().toString());
		
		
		if (atomProved)
		{
			fbLocal.put(atomToProve.toString(), atomToProve);
			System.out.println("Proved "+atomToProve);
			System.out.println("fbLocal : " + fbLocal.keySet().toString());	
			return Boolean.TRUE;

		}
		else
		{
			System.out.println(atomToProve +" not proved" );
			
			if(!failedAtoms.containsKey(atomToProve.toString()));
			{
				failedAtoms.put(atomToProve.toString(),atomToProve);
			}
			System.out.println("failedAtoms : " + failedAtoms.keySet().toString());
			
					
			return Boolean.FALSE;
		}
		
	}
	
	
	
	private Boolean ruleInNotUsablesAtoms(Rule rule , HashMap <String, Atom > notUsablesAtoms)
	{
		ArrayList <Atom> atoms= rule.getHypothesis();
		for(int i= 0 ; i < atoms.size() ; i++)
		{
			if( notUsablesAtoms.get(atoms.get(i).toString()) != null)
			{
//				System.out.println("\t"+rule.getHypothesis().toString()+ 
//						" non testable car inclusee dans "
//						+ notUsablesAtoms.keySet().toString());
				
				return Boolean.TRUE;
			}
			
			
		}
//		System.out.println("\t"+rule.getHypothesis().toString()
//				+" testable car non incluse dans "
//				+ notUsablesAtoms.keySet().toString());
		
		return Boolean.FALSE;
	}
	
	
	//return une hashmaps couplé <nom du terme , term > contenant tout les terms de type
	//type(cst ou var) de la base de connaissance
	
	public HashMap<String , Term > getAllTermsFromKB(String type)
	{
		HashMap<String ,Term > allTypedTermsFromKB = new HashMap<String, Term>();
		
		//Extract de tous les terms de la base de fait
		allTypedTermsFromKB.putAll(getAllTermsFromListOfAtoms(type, fb.getAtoms()));
		
		//Extract  les constantes de la base de regles
		ArrayList <Atom > allAtomsInRulei = null;
		for( int i=0 ; i< rb.size(); i++)
		{
			
			Rule r = rb.getRule(i);
			//System.out.println("Processing rule: " + r);
			
			allAtomsInRulei= r.getAtoms();
			allTypedTermsFromKB.putAll(getAllTermsFromListOfAtoms(type, allAtomsInRulei));
			
		}
		
		//Rajout de tout les predicats (hyp + ccl) dans allAtomsInRulei
		System.out.println("All "+type +" of KB:" + allTypedTermsFromKB.keySet() );
		return allTypedTermsFromKB;
		
		
	}
	
	
	public HashMap<String , Term> getAllTermsFromListOfAtoms(String type, ArrayList <Atom> listAtoms)
	{
		HashMap <String, Term > alltypedterms = new HashMap<String,Term>();
		
		 
		
		//Recherche de toutes les constantes dans la base de regles
		
		//System.out.println("Extracting "+type+"s  of listAtoms: "+ listAtoms );
		
		
		//Analyse de tous les predicats de la regle i (ccl incluse)
		for ( Atom a : listAtoms)
		{
			
			//System.out.println("Extracting" +type+"s of atom: "+ a.toString() );
			//Analise de tout les termes de chaque predicat
			for ( int j = 0 ; j < a.getArity() ; j++)
			{
				Term t = a.getArgI(j);
				if (type.equals("cst") && t.isConstant() &&  !alltypedterms.containsKey(t.toString()))
				{
					//rajout de terme t dans collection de constantes si
					//ssi t n'app pas à cette meme collection et t est une constante
					//System.out.println("new constant "+t.toString() + "added to list of allconsts : "+ alltypedterms.keySet());
					alltypedterms.put(t.toString(), t);
				}
				else if (type.equals("var") && !t.isConstant() && !alltypedterms.containsKey(t.toString()))
				{
					//rajout de terme t dans collection de variables si
					//ssi t n'app pas à cette meme collection et t est une variables
					//System.out.println("new var "+t.toString() + "added to list of allvars : "+ alltypedterms.keySet());
					alltypedterms.put(t.toString(), t);
				}
//				else
//				{
//					//Affichage de la raison par laquelle t n'a pas été
//					//ajouté (debeugage)
//					System.out.print("term "+ t +" not added because ");
//					if(alltypedterms.containsKey(t.toString()))
//					{
//						System.out.print("it's already in ") ; 
//					}
//					if(!t.isConstant())
//					{
//						System.out.print("it's not a constant");
//					}
//					if(t.isConstant())
//					{
//						System.out.print("it's not a var");
//					}
//					System.out.println("");
//				}
			}
		
			
			
			
			
		}
		
//		System.out.println(alltypedterms.keySet());
		return alltypedterms;
	}
	
	/**
	 * Nous permet de généré toutes les substitutions possible et donc 
	 * de passé d'une base de regles d'ordre 1 à une base de regles
	 * d'ordre 0.
	 */
	
	public void generateAllSubstitutions()
	{
		ArrayList<Term> allConstsOfKB = new ArrayList<Term> (getAllTermsFromKB("cst").values());
		
		ArrayList<Term> allConstsArray = new ArrayList<Term>();
		ArrayList <Assignment> substitutionReglei = null;
		ArrayList <Atom> allAtomsInRulei = new ArrayList<Atom>();
		ArrayList <Term> allVarsInRulei = null;
	
		

		for (int i = 0 ; i < rb.size() ; i ++)
		{
			Rule r = rb.getRule(i);
			
			allVarsInRulei =new ArrayList <Term>( getAllTermsFromListOfAtoms("var", r.getAtoms()).values());
			
			
			System.out.println("Substitution for : " + rb.getRule(i));
			substitutionReglei=generateSubstitutions(allVarsInRulei , allConstsOfKB);  
			generatePropositionsfromSubstitutions(substitutionReglei, r.getAtoms());
			
		}
		
		this.rb = this.rbPropositionalized;
		System.out.println("\nBase de regle d'ordre 0 à laquelle on rajoute "
				+ "toutes les propositions propositions genérés à partir d'une KB d'ordre 1 "
				+ "(sans doublons dans une même hypothése)");
//		
		System.out.println(rb);
//		Rule r = rb.getRule(0);
//		allVarsInRulei =new ArrayList <Term>( getAllTermsFromListOfAtoms("var", r.getAtoms()).values());
//		System.out.println("Substitution for :"+ r);
//		generatePropositionsfromSubstitutions(generateSubstitutions(allVarsInRulei , allConstsOfKB), r.getAtoms());
//		
	}
	/**
	 * À partir d'une ArrayList d'assignments substitutions, correspondant à toutes les
	 * assignations possibles des variables contenues 
	 * dans une regle représenté par la liste d'atoms atoms on 
	 * remplie this.rbPropositionalized : la base d'ordre 0 généré à partir
	 * de predicat
	 * @param substitutions liste de substitions possibles des variables dans la
	 * regle représenté par atom
	 * @param atoms liste d'atom appartenant à une regle
	 */
	public void generatePropositionsfromSubstitutions(ArrayList <Assignment> substitutions, ArrayList <Atom> atoms )
	{
		LinkedHashMap <String , Atom> propositionsfromSubstitutionsHM = new LinkedHashMap <String,Atom>();
		ArrayList<ArrayList <Atom>> listPropositionfromSubs = new ArrayList<ArrayList<Atom>>();
		
		//System.out.println("predicate atoms : " + atoms);
		for(Assignment s : substitutions)
		{
			int atomi =0;
			//System.out.printf("%-40s","predicatsVersProp : ");
			for (Atom a : atoms)
			{	
				String predicatVersProp ="";
				predicatVersProp += a.getPredicate();
				predicatVersProp += "(";
				
				for (int i =0 ; i < a.getArity() ; i++)
				{
					Term t = a.getArgI(i);
					
					if(!t.isConstant())
					{
						Term tnouv = (Term)s.get(t.toString());
						predicatVersProp+= tnouv.toString() + ",";
						
					}
					else 
					{
						predicatVersProp+= t.toString() + ",";
					}
				}
				predicatVersProp=predicatVersProp.substring(0, predicatVersProp.length() - 1 );
				predicatVersProp += ")";
				//System.out.print( predicatVersProp + " ");
				
				/**
				 * Afin d'eliminer les doublons à chaque ligne de propositions généré
				 * on utilise une hashMap nous permettant de verifier le contenue de la
				 * lignes de propositions généré 
				 * Exemple p(a) p(b) p(a) deviendra p(a) p(b)
				 * 
				 *  atomi == atoms.size() - 1 => implique que l'Atom a est
				 *  la conclusion est donc, même si doublons, on la rajoute
				 *  dans la liste des propositions généré sinon on n'obtiendra
				 *  pas une formule equivalente 
				 */
				
				if(atomi <atoms.size() -1 && !propositionsfromSubstitutionsHM.containsKey(predicatVersProp))
				{
					propositionsfromSubstitutionsHM.put(predicatVersProp, new Atom (predicatVersProp));
				}
				/*
				 * Si on traite le dernier atom (donc la conclusion, on la rajoute même si doublon
				 * dans la liste des propositions généré
				*/
				else if(atomi == atoms.size() -1 )
				{
					ArrayList <Atom >listPropositions = new ArrayList <Atom> (propositionsfromSubstitutionsHM.values());
					listPropositions.add(new Atom (predicatVersProp));
//					System.out.printf("\n%-40s","listPropositions sans doublons (hyp):");
//					System.out.println(listPropositions);
//					System.out.println("");
					Rule r = new Rule ( listPropositions);
					rbPropositionalized.addRule(r);
					
				}
				atomi++;
				
			}
			//System.out.println("");
			
			propositionsfromSubstitutionsHM.clear();
			
			
			
			
		}
		
//		System.out.println("\nBase de regle d'ordre 0 à laquelle on rajoute "
//				+ "des propositions genérés à partir d'une regle d'ordre 1");
//		
//		System.out.println(rbPropositionalized);
	}
	/**
	 * Algorithme recursif nous permettant de générer tout les
	 * substitutions possible à partir d'une liste de term (variables)
	 * et d'une liste de constantes csts
	 * 
	 * @param vars liste de variable normalement extrait 
	 * contenue dans un predicat
	 * 
	 * @param csts liste de constante, pour ce cas si il s'agit de la liste
	 * des constantes de toute la base de connaissance.
	 * 
	 * @return toutes les assignations possible des variables par 
	 * toutes les constantes
	 */
	public ArrayList <Assignment > generateSubstitutions(ArrayList <Term> vars, ArrayList <Term > csts)
	{
		int i = 0 ;
		int j =0;
		
		ArrayList<Assignment> listAssign = new ArrayList<Assignment>();  
		Assignment assignment = new Assignment();
		
		
		for (int k = 0 ; k < csts.size() ; k++)
		{
			generateSubstitutionsAux(listAssign , assignment,vars, csts,0 ,k);
		}
		
		
		System.out.println("Generate all substitutions : ");
		
		
		
		for (Assignment a :  listAssign)
		{
			
				System.out.println(a.toString());
		}
		
		return listAssign;
	}   
	
	public void generateSubstitutionsAux(ArrayList<Assignment> listAssign ,Assignment assignment, ArrayList <Term> vars , ArrayList <Term> csts , int i , int j)
	{
		
		assignment.put(vars.get(i).toString(), csts.get(j));
		
		if( i == vars.size() - 1 )
		{
			
			
			listAssign.add(assignment.clone());
			//System.out.println(listAssign);	
			return ;
		}
		
		else
		{
			
			
			
			for (int k =  0 ; k < csts.size(); k++)
			{
				generateSubstitutionsAux(listAssign,assignment ,vars, csts, i + 1  , k  );
				assignment.remove(vars.get(i));
			}
			
			
		}
		
	}
}