import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import structure.Atom;
import structure.FactBase;
import structure.Rule;
import structure.RuleBase;
import structure.KnowledgeBase;

public class Application0 {
	public static void main(String[] args) throws Exception
	{
		// // creation de la base de faits de 2 facons differentes
		// FactBase bf = new FactBase("P");
		// bf.addAtomWithoutCheck(new Atom("Q"));
		// System.out.println("Base de faits initiale :\n"+bf);
	
		// // creation de la base de regles
		// RuleBase br = new RuleBase();
		// br.addRule(new Rule("P;S"));
		// br.addRule(new Rule("Q;S;R"));
		// System.out.println("Base de regles :\n"+br);
		
		
		String instance ="reunion.txt";
		String td4ex4 = "td4exo4.txt";
		String td4ex5 ="td4exo5.txt";
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+instance);
		BufferedReader readFile = new BufferedReader(new FileReader (instance));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		
		readFile.close();
		
//		kb.FCnaiveStart();
		
		kb.FCcompteursStart();
		String atomInput = "Start";
		
		while (!atomInput.equals("exit"))
		{
			System.out.println("Saisir atom à prouver:");
			Scanner sc = new Scanner(System.in);
			atomInput = sc.next();
			Atom atom = new Atom( atomInput);
			
			System.out.println(atomInput.toString());
			System.out.println("resultat par recherche dans la base de faits saturée");
			if (kb.contains(atom))
			{
				System.out.println( "oui");
			}
			else
			{
				System.out.println( "non");
			}
			
			System.out.println("résultat par recherche en chainage arrière");
			if(kb.BC3(atom))
			{
				System.out.println("oui");
			}
			else
			{
				System.out.println("non");
			}
		}
			
	}
}
