package mains_TP2_ordre_0_et_1;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import structure.Atom;
import structure.FactBase;
import structure.Rule;
import structure.RuleBase;
import structure.KnowledgeBase;

public class TP2_Deb_Main_moteur_ordre_0 {
	/*
	 * Ce main fonctionne exactement comme demandé dans le TP2_moteur_ordre_0
	 * Etape 5
	 */
	
	public static void main(String[] args) throws Exception
	{
		
		String instance ="reunion.txt";
		String td4ex4 = "td4exo4.txt";
		String td4ex5 ="td4exo5.txt";
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+instance);
		BufferedReader readFile = new BufferedReader(new FileReader (instance));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		
		readFile.close();
		
//		kb.FCnaiveStart();
		
		kb.FCcompteursStart();
		
		System.out.println("\nBase de faits surchargé:\n");
		kb.showSaturedFactBase();
		
		String atomInput = "Start";
		
		while (!atomInput.equals("exit"))
		{
			System.out.println("\n\nSaisir atom à prouver:\n");
			Scanner sc = new Scanner(System.in);
			atomInput = sc.next();
			Atom atom = new Atom( atomInput);
			
			System.out.println(atomInput.toString());
			System.out.println("\nresultat par recherche dans la base de faits saturée\n");
			if (kb.contains(atom))
			{
				System.out.println( "=========>OUI");
			}
			else
			{
				System.out.println( "=========>NON");
			}
			
			System.out.println("\nrésultat par recherche en chainage arrière\n");
			if(kb.BC3(atom))
			{
				System.out.println("=========>OUI");
			}
			else
			{
				System.out.println("=========>NON");
			}
		}
			
	}
}
