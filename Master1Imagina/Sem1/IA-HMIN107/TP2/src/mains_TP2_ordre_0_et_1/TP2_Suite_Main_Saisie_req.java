package mains_TP2_ordre_0_et_1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import structure.Atom;
import structure.KnowledgeBase;
import tp1CSP.CSP;
import tp1CSP.Network;

public class TP2_Suite_Main_Saisie_req {
	/*
	 * 
	 * Ce main permet l'execution du traitement de requetes saisies grace 
	 * à l'implémentation de la solution demandé à l'étape 3.3 du 
	 * TP2_suite_moteur_ordre_1. Il s'agit donc de transformer la requete
	 * et la base de fait saturé par la propositionalisation de la base 
	 * de fait d'ordre 1 en une instance de Network puis d'apliquer
	 * l'algorithme de CSP.backtrackAll() et d'interpreter les 
	 * assignations genéré.
	 */

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String tp2animaux ="tp2animaux.txt";
		
		String fichier = tp2animaux;
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+fichier);
		BufferedReader readFile = new BufferedReader(new FileReader (fichier));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		
//				kb.getAllTermsFromKB("cst");
		
		System.out.println("\n\n substitutions regle par regle\n");
		kb.generateAllSubstitutions();
		
		System.out.println("\n\nFC compteurs" );
		kb.FCcompteurs();
		
		System.out.println("\n\nBase de faits saturés");
		kb.showSaturedFactBase();
		
		
		ArrayList <Atom> requete = new ArrayList <Atom>();
		
	
		
		String requeteInput = "Start";
		
		while (Boolean.TRUE)
		{
			requete.clear();
			System.out.println("==============================================================");
			System.out.println("==============================================================");
			
			System.out.println("Saisir requete de la forme : nomPred(x,y,..,z);nomPred2(y,x..,t);..;nomPredn(z,..);");
			Scanner sc = new Scanner(System.in);
			requeteInput = sc.next();
			
			if (requeteInput.equals("exit"))
				break;
			
			String[] requeteAtoms = requeteInput.split(";");
			
			for (int i = 0 ; i < requeteAtoms.length; i++)
			{
				requete.add(new Atom(requeteAtoms[i]));
			}
			
			System.out.print("\ntraitement de requete :\n");
			System.out.println(requete);
			Network net = new Network();
			System.out.println("\nrequete vers Network:\n ");
			net.setNetworkFromRequete(requete, kb.getSaturedFactBase(), kb.getAllTermsFromKB("cst"));
			
			System.out.println("\nApplication de CSP.BacktrackAll au Network representant la requete\n");
			CSP csp = new CSP(net);
			csp.maxCardEtDegree();
			csp.searchAllSolutionsBT();
			kb.getHomsFromCSPAssignmentSolutions(csp.getSolutions(), requete);
			System.out.print("\n\n\n");
			
			
		}
		readFile.close();

	}

}
