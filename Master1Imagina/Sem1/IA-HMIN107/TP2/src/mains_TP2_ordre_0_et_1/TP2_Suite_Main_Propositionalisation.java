package mains_TP2_ordre_0_et_1;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import structure.Atom;
import structure.KnowledgeBase;
import tp1CSP.CSP;
import tp1CSP.Network;


/**
 * 
 * Propositionalisation de la base de regles par le TP2_suite  d'ordre 1, puis traitement
 * des requetes proposé par le TP2_suite_Annexe_1.
 * 
 * La reponse des requetes s'effectuent grace à la transformation d'une instance
 * d'homomorphisme en une instance de CSP. (Etape 3.3)
 * 
 * Pour tester il suffit d'executer, pour rentrer des requetes à la main
 * (saisie) se referer au fichier TP2_Suite_Main_Saisie_req.java
 *
 */
public class TP2_Suite_Main_Propositionalisation {
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String tp2animaux ="tp2animaux.txt";
		
		String fichier = tp2animaux;
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+fichier);
		BufferedReader readFile = new BufferedReader(new FileReader (fichier));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		
//		kb.getAllTermsFromKB("cst");
		
		System.out.println("\n\n substitutions regle par regle\n");
		kb.generateAllSubstitutions();
		
		System.out.println("\n\nFC compteurs" );
		kb.FCcompteurs();
		
		System.out.println("\n\nBase de faits saturés");
		kb.showSaturedFactBase();
		
		
		ArrayList <Atom> requete = new ArrayList <Atom>();
		
		ArrayList<String> listInput = new ArrayList<String>();
		listInput.add("Anim(x);Cruel(x);");
		listInput.add("Anim(x);Cruel(x);Mange(x,y);");
		listInput.add("Anim(x);Cons(x,'chevre');");
		listInput.add("Cons('chevre',y)");
		
		String requeteInput = "Start";
		int inputi = 0 ;
		while (!requeteInput.equals("exit"))
		{
			requete.clear();
			System.out.println("==============================================================");
			System.out.println("==============================================================");
			
			System.out.println("Saisir requete de la forme : nomPred(x,y,..,z);nomPred2(y,x..,t);..;nomPredn(z,..);");

			
			requeteInput = listInput.get(inputi);
			
			String[] requeteAtoms = requeteInput.split(";");
			
			for (int i = 0 ; i < requeteAtoms.length; i++)
			{
				requete.add(new Atom(requeteAtoms[i]));
			}
			System.out.print("\ntraitement de requete :\n");
			System.out.println(requete);
			Network net = new Network();
			System.out.println("\nrequete vers Network:\n ");
			net.setNetworkFromRequete(requete, kb.getSaturedFactBase(), kb.getAllTermsFromKB("cst"));
			
			System.out.println("\nApplication de CSP.BacktrackAll au Network representant la requete\n");
			CSP csp = new CSP(net);
			csp.maxCardEtDegree();
			csp.searchAllSolutionsBT();
			kb.getHomsFromCSPAssignmentSolutions(csp.getSolutions(), requete);
			System.out.print("\n\n\n");
			
			
			inputi++;
			if(inputi >= listInput.size())
				break;
		}
		readFile.close();
	}

}
