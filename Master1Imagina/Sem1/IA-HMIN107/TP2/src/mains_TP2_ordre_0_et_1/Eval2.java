package mains_TP2_ordre_0_et_1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import structure.Atom;
import structure.KnowledgeBase;
import tp1CSP.CSP;
import tp1CSP.Network;

public class Eval2 {
	public static void main(String[] args) throws Exception
	{
		/*==========================================
		 * EXERCICE 1
		 * ==========================================
		 * */
		
		System.out.println("\n\nEXERCICE 1 =================================<\n\n");
		String cc2ex1 ="Eval_TP2_2017/CC2-ex1.txt";
		
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+cc2ex1);
		BufferedReader readFile = new BufferedReader(new FileReader (cc2ex1));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		
		readFile.close();
		

		
		kb.FCcompteursStart();
		
		System.out.println("\nBase de faits surchargé:\n");
		kb.showSaturedFactBase();
		
		
		ArrayList <String > listStrAtoms = new ArrayList<String>();
		listStrAtoms.add("G");
		listStrAtoms.add("L");
		listStrAtoms.add("I");
		listStrAtoms.add("E");
		listStrAtoms.add("D");
		
		for (String strAtom : listStrAtoms)
		{
			
			
			Atom atom = new Atom( strAtom);
			
			
			System.out.println("\nrésultat par recherche en chainage arrière pour atom\n" + strAtom);
			if(kb.BC3(atom))
			{
				System.out.println("=========>OUI");
			}
			else
			{
				System.out.println("=========>NON");
			}
		}
		
		
		/*==========================================
		 * EXERCICE 2
		 * ==========================================
		 * */
		System.out.println("\n\nEXERCICE 2 =================================<\n\n");
		
		String cc2ex2 ="Eval_TP2_2017/CC2-ex2.txt";
		
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+cc2ex2);
		readFile = new BufferedReader(new FileReader (cc2ex2));

		kb = new KnowledgeBase(readFile);
		readFile.close();
		System.out.println("\n\n substitutions regle par regle\n");
		kb.generateAllSubstitutions();
		
		System.out.println("\n\nFC compteurs" );
		kb.FCcompteurs();
		
		System.out.println("\n\nBase de faits saturés");
		kb.showSaturedFactBase();
		
		ArrayList<String> listReqStr = new ArrayList<String>();
		ArrayList <Atom> requete = new ArrayList <Atom>();
		listReqStr.add("aMere(x,y);h(x);");
		listReqStr.add("aMere(x,y);traumatise(x);f(x);");
		listReqStr.add("traumatise('A');");
		
		for (int k = 0 ; k< listReqStr.size(); k++)
		{
			requete.clear();
			System.out.println("==============================================================");
			System.out.println("==============================================================");
			
			

			
			
			
			String[] requeteAtoms = listReqStr.get(k).split(";");
			
			for (int i = 0 ; i < requeteAtoms.length; i++)
			{
				requete.add(new Atom(requeteAtoms[i]));
			}
			System.out.print("\ntraitement de requete :\n");
			System.out.println(requete);
			Network net = new Network();
			System.out.println("\nrequete vers Network:\n ");
			net.setNetworkFromRequete(requete, kb.getSaturedFactBase(), kb.getAllTermsFromKB("cst"));
			
			System.out.println("\nApplication de CSP.BacktrackAll au Network representant la requete\n");
			CSP csp = new CSP(net);
			csp.maxCardEtDegree();
			csp.searchAllSolutionsBT();
			kb.getHomsFromCSPAssignmentSolutions(csp.getSolutions(), requete);
			System.out.print("\n\n\n");
			
			
			
			
		}
		/*
		 * On trouve bien traumatiste('A') !
		 */
		
		
		/*==========================================
		 * EXERCICE 3
		 * ==========================================
		 * */
		System.out.println("\n\nEXERCICE 3 =================================<\n\n");
		
		String cc2ex3 ="Eval_TP2_2017/CC2-ex3.txt";
		
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+cc2ex3);
		readFile = new BufferedReader(new FileReader (cc2ex3));

		kb = new KnowledgeBase(readFile);
		readFile.close();
		
		
		System.out.println("\n\n substitutions regle par regle\n");
		kb.generateAllSubstitutions();
		
		System.out.println("\n\nFC compteurs" );
		kb.FCcompteurs();
		
		System.out.println("\n\nBase de faits saturés");
		kb.showSaturedFactBase();
		
		listReqStr = new ArrayList<String>();
		requete = new ArrayList <Atom>();
		listReqStr.add("p(u,u);r(u,u,v);");
		listReqStr.add("p(u,v);p(v,v);p(v,u);");
		listReqStr.add("r(u,u,u);");
		listReqStr.add("p(u,'C');");
		
		for (int k = 0 ; k< listReqStr.size(); k++)
		{
			requete.clear();
			System.out.println("==============================================================");
			System.out.println("==============================================================");
			
			

			
			
			
			String[] requeteAtoms = listReqStr.get(k).split(";");
			
			for (int i = 0 ; i < requeteAtoms.length; i++)
			{
				requete.add(new Atom(requeteAtoms[i]));
			}
			System.out.print("\ntraitement de requete :\n");
			System.out.println(requete);
			Network net = new Network();
			System.out.println("\nrequete vers Network:\n ");
			net.setNetworkFromRequete(requete, kb.getSaturedFactBase(), kb.getAllTermsFromKB("cst"));
			
			System.out.println("\nApplication de CSP.BacktrackAll au Network representant la requete\n");
			CSP csp = new CSP(net);
			csp.maxCardEtDegree();
			csp.searchAllSolutionsBT();
			kb.getHomsFromCSPAssignmentSolutions(csp.getSolutions(), requete);
			System.out.print("\n\n\n");
		}
			
		
	}
	
}
