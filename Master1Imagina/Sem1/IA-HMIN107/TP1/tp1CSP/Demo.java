package tp1CSP;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import java.util.Iterator;

public class Demo {

	public static void main(String[] args) throws Exception
	{
		ArrayList <String > listFichiers = new ArrayList<String>();
		
		


		listFichiers.add("csp1.txt"); 
		listFichiers.add("csp2.txt");
		listFichiers.add("5reines.txt");
		listFichiers.add("8reines.txt");
		listFichiers.add("colore.txt");
		
		
		listFichiers.add("5reinesExp.txt");
		listFichiers.add("cryptoMoney.txt"); 
		// todo reformulation de crypto en intention




		// reseaux crée en td tp 
		listFichiers.add("colorationsDif.txt");
		listFichiers.add("cryptogramme2+2Exp.txt");
		
		// ++ EXPERIMENTATION
		// instance géneré aléatoirement par l'executable sur moodle;
		listFichiers.add("instance.txt");
		

        ArrayList <Network> networks = new ArrayList <Network>();
		Network myNetwork=null;
		BufferedReader readFile= null;
        for (String fichier : listFichiers)
        {
			System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+fichier);
			readFile = new BufferedReader(new FileReader (fichier));
			myNetwork = new Network(readFile);
			readFile.close();
			networks.add(myNetwork);	
		}
		int i=0;

		for (Network network : networks)
		{
			System.out.println("\n\n\nFichier : "+listFichiers.get(i));

			CSP csp = new CSP(network);
			csp.maxCardEtDegree();
			
			System.out.println("\nSEARCHSOLUTIONBT");
			csp.searchSolutionBT();

			System.out.println("\nSEARCHALLSOLUTIONBT");
			csp.searchAllSolutionsBT();

			// System.out.println("\nSEARCHALLSOLUTIONFC");
			// csp.searchAllSolutionsFC();	

			i++;
		}

		System.out.print("FIN");
//		System.out.println("Construction example");
//		ArrayList <Assignment> alAsignment = csp.searchAllSolutions();
//		System.out.println(alAsignment);
//		System.out.println("EOL");
//		 csp.searchAllSolutionsFC();
//		csp.searchSolutionsFC();

	}

}