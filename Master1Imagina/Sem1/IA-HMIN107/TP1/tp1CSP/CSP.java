package tp1CSP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import java.util.Iterator;

/**
 * Solveur : permet de résoudre un problème de contrainte par Backtrack : 
 * 	Calcul d'une solution, 
 * 	Calcul de toutes les solutions
 *
 */
public class CSP {

		private Network network;			// le réseau à résoudre
		private ArrayList<Assignment> solutions; 	// les solutions du réseau (résultat de searchAllSolutions)
		private Assignment assignment;			// l'assignation courante (résultat de searchSolution)
		int cptr;					// le compteur de noeuds explorés
		private int profCour;
		private ArrayList<String> vars;
		/**	
		 * Crée un problème de résolution de contraintes pour un réseau donné
		 * 
		 * @param r le réseau de contraintes à résoudre
		 */
		public CSP(Network r) 
		{
			network = r;
			solutions = new ArrayList<Assignment>();
			assignment = new Assignment();
			vars = r.getVars();
		}
		
		public ArrayList <Assignment> getSolutions()
		{
			return solutions;
		}
		

		public String toString()
		{
			return network.toString(); 
		}
		/**
		 * Affiche une solution trouvée par rapport
		 * à l'assignation courante. 
		 */
		private void afficheSolution()
		{
			String aVar;
			ArrayList<String> varsAssignment=this.assignment.getVars();
			System.out.println("Solution");
			for (int i = 0 ; i< varsAssignment.size() ; i++)
			{
				aVar = varsAssignment.get(i);
				
				System.out.print(aVar+ "=" + this.assignment.get(aVar)+ ", ");
				
			}
		}
		

		private void afficheSolutionAll()
		{
			for(Assignment a : solutions)
			{
				for(String var : a.getVars())
				{
					System.out.print(var+"="+a.get(var).toString()+ ", ");
				}
				System.out.println("");
			}
			
		}
		/*  
		 *	Cherche dans l'assignment courant une variable  
		 * 	non assigné
		 */
		/**
		 * 
		 * 
		 * @return String correspondant au nom de variable non Asignee
		 */
		private String choixVariableNonAsignee()
		{
			return null;
		}
		
		
		
		/* La methode bactrack ci-dessous travaille directement sur l'attribut assignment. 
		 * On peut aussi choisir de ne pas utiliser cet attribut et de créer plutot un objet Assignment local à searchSolution : 
		 * dans ce cas il faut le passer en parametre de backtrack
		 */
		/**
		 * Exécute l'algorithme de backtrack à la recherche d'une solution en étendant l'assignation courante
		 * Utilise l'attribut assignment
		 * @return la prochaine solution ou null si pas de nouvelle solution
		 */
		
		
		
		
		/********************** BACKTRACK UNE SOLUTION *******************************************/
		 
		/**
		 * Cherche une solution au réseau de contraintes
		 * 
		 * @return une assignation solution du réseau, ou null si pas de solution
		 */
		
		public Assignment searchSolutionBT() {
			cptr=1;
			profCour=-1;
			
			// Implanter appel a backtrack
			Assignment sol = backtrack();
			System.out.println("\nNb de noeuds parcourus = " + this.cptr );
			afficheSolution();
			

			return sol;
		}
		
		
		/********************** BACKTRACK TOUTES SOLUTIONS *******************************************/
		
		/**
		 * Calcule toutes les solutions au réseau de contraintes
		 * 
		 * @return la liste des assignations solution
		 * 
		 */
		public void searchAllSolutionsBT(){
			this.cptr=1;
			this.profCour=-1;
			assignment.clear();
			this.backtrackAll();
			System.out.println("Total noeuds parcouru : "+this.cptr);
			System.out.println("Total solutions trouvée : " + this.solutions.size());
			afficheSolutionAll();
			
			
		}
		
		/**
		 * Calcule une solution au réseau de contraintes en 
		 * en utilisant l'algo de foward checking.
		 * 
		 * 
		 * 
		 */
		public void searchSolutionsFC(){
			this.cptr=1;
			this.profCour=-1;
			assignment.clear();
			this.preFowardChecking();
			System.out.println("Total noeuds parcouru : "+this.cptr);
			System.out.println("Total solutions trouvée : " + this.solutions.size());
			afficheSolution();
			
			
		}



		/**
		 * Calcule toutes les solutions au réseau de contraintes en 
		 * en utilisant l'algo de foward checking.
		 * 
		 * 
		 * 
		 */
		public void searchAllSolutionsFC(){
			this.cptr=1;
			this.profCour=-1;
			assignment.clear();
			this.preFowardCheckingAll();
			System.out.println("Total noeuds parcouru : "+this.cptr);
			System.out.println("Total solutions trouvée : " + this.solutions.size());
			afficheSolutionAll();
			
			
		}

		
    
  		
    // IMPLANTER l'UNE DES DEUX METHODES CHOOSEVAR CI-DESSOUS (SELON QUE L'ASSIGNATION COURANTE EST PASSEE EN PARAMETRE OU PAS)
		
		/**
		 * Retourne la prochaine variable à assigner étant donné assignment (qui doit contenir la solution partielle courante)
		 *  
		 * @return une variable non encore assignée
		 */
		private String chooseVar() {
		    // A IMPLANTER
			String var= this.vars.get(this.profCour);		
		    return var;
		}
		
		/*****************************************************************/
		
		
		
		/**
		 * Fixe un ordre de prise en compte des valeurs d'un domaine
		 * 
		 * @param values une liste de valeurs
		 * @return une liste de valeurs
		 */
		private ArrayList<Object> tri(ArrayList<Object> values) 
		{
			return values; // donc en l'état n'est pas d'une grande utilité !
		}
		
		
		public String varMaxDegree(ArrayList<String> vars)
		{
			if(vars.size()==0 || vars ==null)
			{	
				System.err.println("varMaxDegree: Error ");
				return "";
			}
			int degreVar;
			int maxDegre=network.getConstraints(vars.get(0)).size();
			int maxDegreInd=0;
			
			for(int i=1 ; i < vars.size(); i++)
			{	
				degreVar=network.getConstraints(vars.get(i)).size();
				
				if(maxDegre < degreVar)
				{
					maxDegre = degreVar; 
					maxDegreInd=i;
				}
			}

			return vars.get(maxDegreInd);
		}



		
		private HashMap <String,InfoVar> preTraitementMaxCard()
		{
			// Remplissage d'un réseau de contraintes 
			// qui nous permet de connaître à partir d'une variable
			// les contraintes qui lui sont associés.
			// Ceci n'étant pas possible actuellement, nous permettra de
			// réaliser un algorithme heuristique max-card.

			HashMap<String,InfoVar> reseauContraintes=new HashMap<String,InfoVar>();
			System.out.println("preTraitement:");
			for( String var : network.getVars())
			{
				// System.out.println(var+","+network.getConstraints(var));
				reseauContraintes.put(var,
					new InfoVar(network.getConstraints(var)));
				
			}

			// for( String var : network.getVars())
			// {
			// 	for (Constraint c : network.getConstraints())
			// 	{
			// 		if(c.getVars().contains(var))
			// 		{
			// 			reseauContraintes.get(var).addContraint(c);
			// 		}
			// 	}
			// }
			return reseauContraintes;
		}

		public void maxCardEtDegree()
		{
			HashMap<String,InfoVar> reseauContraintes=preTraitementMaxCard();

			ArrayList <String> orderedVars = new ArrayList(network.getVars());
 			Collections.sort(orderedVars);

 			// On associe à chaque variable dans reseauContraintes sont indice 
 			// pour retrouver cette même variable dans orderedVars.
 			for(int i = 0; i < orderedVars.size(); i++)
 			{
 				reseauContraintes.get(orderedVars.get(i)).setIndInOrderedVars(i);
 			}
 			
 			ArrayList <Boolean> alreadySelected = new ArrayList<Boolean>(orderedVars.size());
			for(int i=0;i<orderedVars.size();i++)
			{
				alreadySelected.add(Boolean.FALSE);
			}

			

 			ArrayList <String> maxCardVars = new ArrayList<String>();
 			ArrayList <String> maxCardVarsEgaux = new ArrayList<String>();
 			ArrayList <Integer> maxCardVarsEgauxInd = new ArrayList<Integer>();
 			// System.out.println("orderedVars:");
 			// System.out.println(orderedVars);
 			int maxInd;
			String selVar; 		
 			while(maxCardVars.size()<orderedVars.size())
			{	
				// On cherche le premier indice non encore sélectionné
				// dans le tableau des booleans afin d'avoir 
				// un premier candidat sélectionnable parmi 
				// les sommets non encore sélectionnés.
				maxInd=0;
				maxCardVarsEgaux.clear();
				for(int i=0;i < orderedVars.size();i++)
				{
					if(!alreadySelected.get(i))
					{
						maxInd=i;
						break;
					}
				}
				// On cherche si le sommet candidat est en fait 
				// celui qui possède le plus grand nombre de voisins sélectionnés 
				// par un simple parcours linéaire qui vérifie 
				// l'attribut de la class InfoVar associé à chaque variable
				// qui contient l'information du nombre de voisins sélectionnés.
				for(int i=maxInd;i<orderedVars.size();i++)
				{	
					// System.out.println("v="+v);	
					if(
						!alreadySelected.get(i) &&
						(reseauContraintes.get(orderedVars.get(i)).getNbVoisinSelected() >
						reseauContraintes.get(orderedVars.get(maxInd)).getNbVoisinSelected())
					)
					{
						maxInd = i;
						maxCardVarsEgaux.clear();
						maxCardVarsEgaux.add(orderedVars.get(i));
					}
					
					// Si il y a égalité du nombre de voisin sélectionnés maximaux pour plusieurs 
					// variable on crée un tableau des variables à nombre de voisins sélectionnés 
					// égaux, pour ensuite en choisir un par rapport à l'algorithme de maxDegre
					else if(!alreadySelected.get(i) &&
						reseauContraintes.get(orderedVars.get(i)).getNbVoisinSelected() ==
						reseauContraintes.get(orderedVars.get(maxInd)).getNbVoisinSelected())
					{
						maxCardVarsEgaux.add(orderedVars.get(i));
					}	

				}
				
				if (maxCardVarsEgaux.size() >= 2)
				{
					selVar=varMaxDegree(maxCardVarsEgaux);
					maxInd = reseauContraintes.get(selVar).getIndInOrderedVars();
					

				}	
				else
				{				
					// On en extrait le String associé à la variable 
					// sélectionné.
					selVar=orderedVars.get(maxInd);	
				}

				// System.out.println("SelectedVariableIndDansOrderedVars="+reseauContraintes.get(selVar).getIndInOrderedVars());
				// On mets à true l'indice dans le tableau de boolean
				// correspondant à l'indice de la variable sélectionné
				// dans orderedVars
				alreadySelected.set(maxInd,Boolean.TRUE);
				
				// On rajoute la variable sélectionné dans le tableau
				// ordonné par max-card
				maxCardVars.add(orderedVars.get(maxInd));
				
				// System.out.println("selVar="+selVar+" RC="+ reseauContraintes.get(selVar).getContraints());

				// System.out.println("Selected Vars : ");
				// System.out.println(maxCardVars);
				// Pour tout les voisins de la variable sélectionné on les 
				// parcours afin d’incrémenter un cpteur contenant l'information 
				// de combien de voisins on t-ils déjà été sélectionnés.
				for(Constraint c : reseauContraintes.get(selVar).getContraints())
				{	
					
					for( String v1 : c.getVars())
					{
						if(!v1.equals(selVar))
							reseauContraintes.get(v1).voisinSelected();
						
					}
				}
			
				// System.out.print("\t");
				for(int i=0 ;i < orderedVars.size();i++)
				{
					// System.out.print(orderedVars.get(i)+ "."+"vSels=");
					// System.out.print(
//						reseauContraintes.get(orderedVars.get(i)).getNbVoisinSelected()+", ");
					
				}
				// System.out.println("");
 			}

 			System.out.println("Max-card + MaxDegre:");
 			System.out.println(maxCardVars);
 			vars=maxCardVars;
		}
  // IMPLANTER l'UNE DES DEUX METHODES CONSISTANT CI-DESSOUS (SELON QUE L'ASSIGNATION COURANTE EST PASSEE EN PARAMETRE OU PAS)
		
		/**
		 * Teste si l'assignation courante stokée dans l'attribut assignment est consistante, c'est à dire qu'elle
		 * ne viole aucune contrainte.
		 * 
		 * @param lastAssignedVar la variable que l'on vient d'assigner à cette étape
		 * @return vrai ssi l'assignment courante ne viole aucune contrainte
		 */
		private boolean consistant(String lastAssignedVar) 
		{	 
			
			for(Constraint c : network.getConstraints(lastAssignedVar))
			{	
				// System.out.println("consistant(str) : assignation en cours= "+assignment);
				if(c.violation(assignment))
				{
					return false;
				}	
			}
			return true;	
		}

		
		/**
		 * Teste si l'assignation courante stockée dans assignment est consistante par rapport à sol, c'est à dire qu'elle
		 * ne viole aucune contrainte.
		 * 
		 * @param sol solution partielle courante
		 * @param lastAssignedVar la variable que l'on vient d'assigner à cette étape
		 * @return vrai ssi l'assignment courante ne viole aucune contrainte
		 */
		private boolean consistant(Assignment sol, String lastAssignedVar) {
			// A IMPLANTER
			System.err.println("Méthode consistant() à implanter !!!");
			return true;

		}


		private Assignment backtrack() {
		    // A IMPLANTER
		    // AJOUTER UN PARAMETRE DE TYPE ASSIGNMENT SI ON NE TRAVAILLE PAS DIRECTEMENT SUR L'ATTRIBUT assignment
		    // quelque part : cptr++
//			System.out.print("Assignation courante");
//			for(String var : assignment.getVars())
//			{
//				System.out.print(var+" ");
//				System.out.println(assignment.get(var));
//			}
		    
		    this.profCour++;
			if (this.assignment.getVars().size()==network.getVarNumber())
			{
				
				return this.assignment;
				
			}
			String var=chooseVar();
//			System.out.println(var);
			// for(Object valeurdeDom : network.getDom(var))
			for(Object valeurdeDom : network.getDom(var))
			{
		    	this.cptr++;
				// System.out.println("BT en cours:");
				assignment.put(var, valeurdeDom);
				
				if(consistant(var))
				{
					
					Assignment a=backtrack();
					this.profCour--;
					if (a!=null)
					{
						return a;
					}
					
				}
				assignment.remove(var);
			}

		    return null;
		}



		/**
		 * Exécute l'algorithme de backtrack à la recherche de toutes les solutions
		 * étendant l'assignation courante
		 * 
		 */
		private void backtrackAll() {
		    // AJOUTER UN PARAMETRE DE TYPE ArrayList<Assignment> SI ON NE TRAVAILLE PAS DIRECTEMENT SUR L'ATTRIBUT solutions
		    // A IMPLANTER
		    // quelque part : cptr++;
			
			this.profCour++;
			
			if (this.assignment.getVars().size()==network.getVarNumber())
			{
				this.solutions.add(this.assignment.clone());
			}
			else
			{
				String var=chooseVar();
	//			System.out.println(var);
				// for(Object valeurdeDom : network.getDom(var))
				for(Object valeurdeDom : network.getDom(var))
				{
					assignment.put(var, valeurdeDom);
					this.cptr++;
					if(consistant(var))
					{
		    			
						backtrackAll();
						this.profCour--;
						
						
					}
					assignment.remove(var);

				}
			}		    
		
		}

		


		public void preFowardChecking( )
		{
			HashMap<String , ArrayList<Object>> varDom 
				= new HashMap<String , ArrayList<Object>>();
			this.profCour=-1;
			this.cptr=1;
			// On construit une structure identique à celle de network.varDom
			// afin de pouvoir modifier uniquement des copies de références
			// ce qui nous permettra de faire de manipulation sans perdre 
			// les caractéristiques du réseau.
			for(String var : network.getVars())
			{
				varDom.put(var,network.getDom(var));
			}

			// on reprend la construction d'un réseau de contraintes afin d'associer
			// à chaque variable, les contraintes auxquelles elles font parties.
			HashMap <String,InfoVar> reseauContraintes = preTraitementMaxCard();

			fowardChecking(varDom,reseauContraintes);
			

		}

		public void preFowardCheckingAll( )
		{
			HashMap<String , ArrayList<Object>> varDom 
				= new HashMap<String , ArrayList<Object>>();
			this.profCour=-1;
			this.cptr=1;
			// On construit une structure identique à celle de network.varDom
			// afin de pouvoir modifier uniquement des copies de références
			// ce qui nous permettra de faire de manipulation sans perdre 
			// les caractéristiques du réseau.
			for(String var : network.getVars())
			{
				varDom.put(var,network.getDom(var));
			}

			// on reprend la construction d'un réseau de contraintes afin d'associer
			// à chaque variable, les contraintes auxquelles elles font parties.
			HashMap <String,InfoVar> reseauContraintes = preTraitementMaxCard();

			fowardCheckingAll(varDom,reseauContraintes);
			

		}

		public Assignment fowardChecking(HashMap<String,ArrayList<Object>> varDom,
									HashMap <String,InfoVar> reseauContraintes )
		{
		    
		    this.profCour++;
			if (this.assignment.getVars().size()==network.getVarNumber())
			{
				
				
				return this.assignment;
			}
			else
			{


				String var=chooseVar();
	//			System.out.println(var);
				HashMap<String,ArrayList<Object>> newVarsDom = null;

				


				for(Object valeurdeDom : varDom.get(var))
				{
			    	this.cptr++;
					assignment.put(var, valeurdeDom);
					System.out.println("FC en cours:" + assignment);
					
					newVarsDom=checking(var,varDom,reseauContraintes);
					
					System.out.println("Doms : "+ varDom );
					System.out.println();
					
					Assignment a;
					a=fowardChecking(newVarsDom,reseauContraintes);
					if(a != null)
					{
						return a;
					}
					this.profCour--;
					
				
					assignment.remove(var);
				} 
				
				return null;

			}

			
					
		    
		}

		public void fowardCheckingAll(HashMap<String,ArrayList<Object>> varDom,
									HashMap <String,InfoVar> reseauContraintes )
		{
		    
		    this.profCour++;
			if (this.assignment.getVars().size()==network.getVarNumber())
			{
				
				this.solutions.add(this.assignment.clone());
				
			}
			else
			{


				String var=chooseVar();
	//			System.out.println(var);
				HashMap<String,ArrayList<Object>> newVarsDom = null;

				


				for(Object valeurdeDom : varDom.get(var))
				{
			    	this.cptr++;
					assignment.put(var, valeurdeDom);
					System.out.println("FC en cours:" + assignment);
					
					newVarsDom=checking(var,varDom,reseauContraintes);
					
					System.out.println("Doms : "+ varDom );
					System.out.println();
					
						
					fowardCheckingAll(newVarsDom,reseauContraintes);
					this.profCour--;
					
				
					assignment.remove(var);
				} 
				
				// System.out.println("out" +varDom);
			}

			
					
		    
		}

		private HashMap<String, ArrayList<Object>> checking 
								( String var,
								HashMap<String,ArrayList<Object>> varDom, 
								HashMap <String,InfoVar> reseauContraintes)
		{
			
			// Pour toutes les contraintes qui incluent var
			for(Constraint c : reseauContraintes.get(var).getContraints())
			{
				// On modifie le domaine des variables 
				// en contrainte avec var en fonction de la contrainte c (dif, eg, ext ...)
				// On aura donc plusieurs surcharges de la fonction modify.

				varDom=c.modify(var,varDom,assignment);
			}
			return varDom;
		}
		
		
}
