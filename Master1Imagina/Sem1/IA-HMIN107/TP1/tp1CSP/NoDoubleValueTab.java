package tp1CSP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class NoDoubleValueTab
{
	private HashMap<String,HashMap <String,Object>> tab;

	public NoDoubleValueTab()
	{
		tab=new HashMap<String,HashMap <String,Object>>(); 
//		
		
	}

	public void add(String var , Object ob)
	{
		
		if(tab.get(var)== null)
		{
			tab.put(var, new HashMap<String,Object>());
		}
		
		if(!tab.get(var).containsKey(ob.toString()))
		{
			
			tab.get(var).put(ob.toString(),ob);
		}
	}

	public ArrayList<Object> getValues (String var)
	{
		
		HashMap<String,Object> values = tab.get(var);
		if(values == null)
			System.out.println("null");
		
		ArrayList <Object> tabOb = new ArrayList <Object>();

		for(String val : values.keySet())
		{
			tabOb.add(values.get(val));
		}

		return tabOb;
	}

	public Set<String> getVars()
	{
		return tab.keySet();
	}
}