package tp1BackTrack;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Pour manipuler des contraintes en extension
 *
 */
public class ConstraintExt extends Constraint{
	
	private ArrayList<ArrayList<Object>> tuples;	// ensemble des tuples de la contrainte
	
	/**
	 * Construit une contrainte d'extension vide à partir
	 * d'une liste de variables
	 * 
	 * @param var la liste de variables
	 */
	public ConstraintExt(ArrayList<String> var) {
		super(var);
		tuples = new ArrayList<ArrayList<Object>>();
	}
	
	/**
	 * Construit une contrainte d'extension vide à partir
	 * d'une liste de variables et d'un nom
	 * 
	 * @param var la liste de variables
	 * @param name son nom
	 */
	public ConstraintExt(ArrayList<String> var, String name) {
		super(var,name);
		tuples = new ArrayList<ArrayList<Object>>();
	}
	
	/**
	 * Construit une contrainte en extension à partir d'une représentation
	 * textuelle de la contrainte. La liste de variables est donnée sous la forme : Var1;Var2;...
	 * Puis le nombre de tuples est indiqué et enfin chaque tupe est donné sous la forme d'une
	 * suite de valeurs "String" : Val1;Val2;...
	 * Aucune vérification n'est prévue si la syntaxe n'est pas respectée !!
	 * 
	 * @param in le buffer de lecture de la représentation textuelle de la contrainte
	 * @throws Exception en cas d'erreur de format
	 */
	public ConstraintExt(BufferedReader in) throws Exception{
		super(in);
		tuples = new ArrayList<ArrayList<Object>>();
		int nbTuples = Integer.parseInt(in.readLine());		// nombre de tuples de valeurs
		for(int j=1;j<=nbTuples;j++) 
		{
			ArrayList<Object> tuple = new ArrayList<Object>();
			
			for (String v : in.readLine().split(";")) 
				tuple.add(v);	// Val1;Val2;...;Val(arity)
			
			if(tuple.size() != varList.size()) 
				System.err.println("Le tuple " + tuple + " n'a pas l'arité " + varList.size() + " de la contrainte " + name);
			else if(!tuples.add(tuple)) 
				System.err.println("Le tuple " + tuple + " est déjà présent dans la contrainte "+ name);
		}
	}
	
	/**
	 * Ajoute un tuple de valeur à la contrainte
	 * 
	 * @param valTuple le tuple à ajouter
	 */
	public void addTuple(ArrayList<Object> valTuple) {
		if(valTuple.size() != varList.size()) 
			System.err.println("Le tuple " + valTuple + " n'a pas l'arité " + varList.size() + " de la contrainte " + name);
		else if(!tuples.add(valTuple)) System.err.println("Le tuple " + valTuple + " est déjà présent dans la contrainte "+ name);
	}
	

	/* (non-Javadoc)
	 * A Implanter !
	 * @see Constraint#violation(Assignment)
	 */
	public boolean violation(Assignment a) 
	{
		// A IMPLANTER				
		// System.out.println("Constraint en cours:  " + this.toString());
		
		ArrayList<String> varDeContrainte=new ArrayList<String>();
		ArrayList<String> valeurDeVarsDansContraints=new ArrayList<String>();
				
	
		for(String varName : this.varList)
		{
			varDeContrainte.add(varName);
			//On rajoute dans la valeurDeContrainte l'assignation de
			//la variable dans la contrainte courrante.
			
			// System.out.print("\tvarName="+varName);
			// System.out.println(" varValue="+a.get(varName));
			Object assignCourDeVarDansContr=a.get(varName);
			//	TODO : Si variable sans assignation dans contrainte, verifié
			//	quand les variables qui sont assigné. Sinon on verifira tous les
			//	contraintes au feuilles de l'arbre de parcours de csp.
			
			
			if (assignCourDeVarDansContr==null)
			{	
				valeurDeVarsDansContraints.add("nonAtt");	
			}
			else
			{
				valeurDeVarsDansContraints.add(assignCourDeVarDansContr.toString());
			}
		
			
		}
		
		
		
		Boolean tupleiDeContjRespectd=false;
		for(ArrayList<Object> tuple : this.getTuples())
		{
			int i=0;
			tupleiDeContjRespectd=true;
			for (Object valTuple : tuple)
			{

				if(!valeurDeVarsDansContraints.get(i).equals("nonAtt") 
						&& ! valTuple.toString().equals(valeurDeVarsDansContraints.get(i)))
				{
					tupleiDeContjRespectd=false;
					// System.out.println("\tX");
					break;
				}
				else
				{
					// System.out.println("\t"+valTuple.toString()+"="+valeurDeVarsDansContraints.get(i) );
				}
				
				if(i==this.varList.size()-1)
				{
					// System.out.println("\tvalTuple respected: "+ tuple);
				}
				
				i++;
				
			}
			if (tupleiDeContjRespectd)
			{
				break;
			}
		}
		
		
		
	return !tupleiDeContjRespectd;
	
			
	}


	/* (non-Javadoc)
	 * A Implanter !
	 * @see Constraint#violation(Assignment)
	 */
	protected  HashMap<String, ArrayList<Object>> modify(String var, HashMap<String, ArrayList<Object>> varsDom, Assignment assignment) 
	{
		// A IMPLANTER				
		System.out.println("Constraint en cours:  " + this.toString());
		
		ArrayList<String> varDeContrainte=new ArrayList<String>();
		ArrayList<String> valeurDeVarsDansContraints=new ArrayList<String>();
				
	
		for(String varName : this.varList)
		{
			varDeContrainte.add(varName);
			
			//On rajoute dans la valeurDeContrainte l'assignation de
			//la variable dans la contrainte courrante
			// System.out.print("\tvarName="+varName);
			// System.out.println(" varValue="+assignment.get(varName));
			Object assignCourDeVarDansContr=assignment.get(varName);
			
			if (assignCourDeVarDansContr==null)
			{	
				valeurDeVarsDansContraints.add("nonAtt");	
			}
			else
			{
				valeurDeVarsDansContraints.add(assignCourDeVarDansContr.toString());
			}
		
			
		}
		
		ArrayList<Object> respectedTuple;
		NoDoubleValueTab domainesModifies= new NoDoubleValueTab();
		Boolean tupleiDeContjRespectd=false;
		for(ArrayList<Object> tuple : this.getTuples())
		{
			int i=0;
			tupleiDeContjRespectd=true;
			for (Object valTuple : tuple)
			{

				if(!valeurDeVarsDansContraints.get(i).equals("nonAtt") 
						&& ! valTuple.toString().equals(valeurDeVarsDansContraints.get(i)))
				{
					tupleiDeContjRespectd=false;
					// System.out.println("\tX");
					break;
				}
				else
				{
					// System.out.println("\t"+valTuple.toString()+"="+valeurDeVarsDansContraints.get(i) );
				}
				
				if(i==this.varList.size()-1)
				{
					// System.out.println("\tvalTuple respected: "+ tuple);
					respectedTuple=tuple;

				}
				
				i++;
				
			}
			if (tupleiDeContjRespectd)
			{
				
				// Si un tuple est respecté, on 
				// stock les valeurs autorisé par rapport à
				// la contrainte this 				
				//  exemple 
				// WA NT T
				// R  G  B
				// R  B  R
				// et que la dernière variable assigné est WA
				// on stock le domaine de la variable autorisé 
				// pour NT : [G] , T : [B] puis itération suivantes NT : [G R] et T: [B R]
				// car on rempli ligne par ligne les nouveaux domaines de variables.

				String varModif;
				for(int j=0 ; j < this.getVars().size();j++)
				{
					varModif = varList.get(j);
					if(!varModif.equals(var))
					{
						domainesModifies.add(varModif,tuple.get(j));
					}
				}				
			}
		}
		
		 HashMap<String, ArrayList<Object>> newVarsDom =
		 	 new HashMap<String, ArrayList<Object>>();
		for (String varD : varsDom.keySet())
		{
			newVarsDom.put(varD,varsDom.get(varD));
		}
	 	// System.out.println("domainesModifies=" + domainesModifies.getVars());
		
		for(String varModif : domainesModifies.getVars())
		{
			// Liste contenant le domaine modifié par rapport à la contrainte this
			ArrayList <Object> d1= new ArrayList<Object>(domainesModifies.getValues(varModif));
			
			// Liste contenant le domaine avant modification de la
			// la part de la contrainte this
				
			ArrayList <Object> d2 
				=new ArrayList<Object>(varsDom.get(varModif));

			// HashMap dverif nous permettant resté linéaire 
			// dans la création de dfinal , l'intersection 
			// de d1 et d2.
			ArrayList<Object>dfinal = InterArrayList.intersection(d1,d2);
			// System.out.println(varModif + "=" + varsDom.get(varModif));
			
			newVarsDom.put(varModif , dfinal);
		}
		return newVarsDom;
		
	}
	


	
	/* (non-Javadoc)
	 * @see Constraint#toString()
	 */
	public String toString() {
		return "\n\t Ext "+ super.toString() + " : " + tuples; 
	}
	
	public ArrayList<ArrayList<Object>> getTuples()
	{
		return tuples;
	}

	

}
