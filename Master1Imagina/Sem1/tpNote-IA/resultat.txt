IMAGINA 
S. BESSY

Fichier : csp1.txt
preTraitement:
Max-card + MaxDegre:
[x2, x3, x4, x1, x5]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 35
Solution
x1=5, x2=1, x3=6, x4=1, x5=5, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 409
Total solutions trouvée : 5
x1=5, x2=1, x3=6, x4=1, x5=5, 
x1=3, x2=4, x3=2, x4=3, x5=7, 
x1=8, x2=4, x3=2, x4=3, x5=7, 
x1=8, x2=7, x3=8, x4=5, x5=6, 
x1=8, x2=7, x3=8, x4=5, x5=8, 



Fichier : csp2.txt
preTraitement:
Max-card + MaxDegre:
[x6, x3, x2, x5, x1, x4, x0, x8, x7]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 25
Solution
x8=4, x0=1, x1=0, x2=0, x3=2, x4=0, x5=0, x6=1, x7=4, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 79
Total solutions trouvée : 6
x8=4, x0=1, x1=0, x2=0, x3=2, x4=0, x5=0, x6=1, x7=4, 
x8=4, x0=1, x1=0, x2=0, x3=2, x4=0, x5=0, x6=1, x7=5, 
x8=4, x0=1, x1=0, x2=0, x3=2, x4=1, x5=1, x6=1, x7=4, 
x8=4, x0=1, x1=0, x2=0, x3=2, x4=1, x5=1, x6=1, x7=5, 
x8=4, x0=1, x1=0, x2=0, x3=2, x4=2, x5=2, x6=1, x7=4, 
x8=4, x0=1, x1=0, x2=0, x3=2, x4=2, x5=2, x6=1, x7=5, 



Fichier : 5reines.txt
preTraitement:
Max-card + MaxDegre:
[rl1, rl2, rl3, rl4, rl5]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 16
Solution
rl1=c1, rl3=c5, rl2=c3, rl5=c4, rl4=c2, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 221
Total solutions trouvée : 10
rl3=c5, rl2=c3, rl5=c4, rl4=c2, rl1=c1, 
rl3=c2, rl2=c4, rl5=c3, rl4=c5, rl1=c1, 
rl3=c1, rl2=c4, rl5=c5, rl4=c3, rl1=c2, 
rl3=c3, rl2=c5, rl5=c4, rl4=c1, rl1=c2, 
rl3=c4, rl2=c1, rl5=c5, rl4=c2, rl1=c3, 
rl3=c2, rl2=c5, rl5=c1, rl4=c4, rl1=c3, 
rl3=c3, rl2=c1, rl5=c2, rl4=c5, rl1=c4, 
rl3=c5, rl2=c2, rl5=c1, rl4=c3, rl1=c4, 
rl3=c4, rl2=c2, rl5=c3, rl4=c1, rl1=c5, 
rl3=c1, rl2=c3, rl5=c2, rl4=c4, rl1=c5, 



Fichier : 8reines.txt
preTraitement:
Max-card + MaxDegre:
[rl1, rl2, rl3, rl4, rl5, rl6, rl7, rl8]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 877
Solution
rl1=1, rl3=8, rl2=5, rl5=3, rl4=6, rl7=2, rl6=7, rl8=4, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 15721
Total solutions trouvée : 92
rl1=1, rl3=8, rl2=5, rl5=3, rl4=6, rl7=2, rl6=7, rl8=4, 
rl1=1, rl3=8, rl2=6, rl5=7, rl4=3, rl7=2, rl6=4, rl8=5, 
rl1=1, rl3=4, rl2=7, rl5=8, rl4=6, rl7=5, rl6=2, rl8=3, 
rl1=1, rl3=5, rl2=7, rl5=2, rl4=8, rl7=6, rl6=4, rl8=3, 
rl1=2, rl3=6, rl2=4, rl5=3, rl4=8, rl7=7, rl6=1, rl8=5, 
rl1=2, rl3=7, rl2=5, rl5=3, rl4=1, rl7=6, rl6=8, rl8=4, 
rl1=2, rl3=7, rl2=5, rl5=1, rl4=4, rl7=6, rl6=8, rl8=3, 
rl1=2, rl3=1, rl2=6, rl5=4, rl4=7, rl7=3, rl6=8, rl8=5, 
rl1=2, rl3=8, rl2=6, rl5=1, rl4=3, rl7=7, rl6=4, rl8=5, 
rl1=2, rl3=3, rl2=7, rl5=8, rl4=6, rl7=1, rl6=5, rl8=4, 
rl1=2, rl3=5, rl2=7, rl5=1, rl4=8, rl7=6, rl6=4, rl8=3, 
rl1=2, rl3=6, rl2=8, rl5=3, rl4=1, rl7=7, rl6=5, rl8=4, 
rl1=3, rl3=7, rl2=1, rl5=8, rl4=5, rl7=4, rl6=2, rl8=6, 
rl1=3, rl3=2, rl2=5, rl5=1, rl4=8, rl7=4, rl6=7, rl8=6, 
rl1=3, rl3=2, rl2=5, rl5=6, rl4=8, rl7=7, rl6=4, rl8=1, 
rl1=3, rl3=7, rl2=5, rl5=4, rl4=1, rl7=8, rl6=2, rl8=6, 
rl1=3, rl3=8, rl2=5, rl5=1, rl4=4, rl7=2, rl6=7, rl8=6, 
rl1=3, rl3=2, rl2=6, rl5=8, rl4=5, rl7=7, rl6=1, rl8=4, 
rl1=3, rl3=2, rl2=6, rl5=1, rl4=7, rl7=8, rl6=4, rl8=5, 
rl1=3, rl3=2, rl2=6, rl5=5, rl4=7, rl7=8, rl6=1, rl8=4, 
rl1=3, rl3=4, rl2=6, rl5=8, rl4=1, rl7=7, rl6=5, rl8=2, 
rl1=3, rl3=4, rl2=6, rl5=8, rl4=2, rl7=7, rl6=5, rl8=1, 
rl1=3, rl3=8, rl2=6, rl5=4, rl4=1, rl7=5, rl6=7, rl8=2, 
rl1=3, rl3=8, rl2=6, rl5=5, rl4=1, rl7=2, rl6=7, rl8=4, 
rl1=3, rl3=8, rl2=6, rl5=4, rl4=2, rl7=7, rl6=1, rl8=5, 
rl1=3, rl3=2, rl2=7, rl5=5, rl4=8, rl7=4, rl6=1, rl8=6, 
rl1=3, rl3=2, rl2=7, rl5=6, rl4=8, rl7=1, rl6=4, rl8=5, 
rl1=3, rl3=4, rl2=8, rl5=1, rl4=7, rl7=2, rl6=6, rl8=5, 
rl1=4, rl3=5, rl2=1, rl5=2, rl4=8, rl7=3, rl6=7, rl8=6, 
rl1=4, rl3=5, rl2=1, rl5=6, rl4=8, rl7=7, rl6=3, rl8=2, 
rl1=4, rl3=5, rl2=2, rl5=6, rl4=8, rl7=3, rl6=1, rl8=7, 
rl1=4, rl3=7, rl2=2, rl5=6, rl4=3, rl7=1, rl6=8, rl8=5, 
rl1=4, rl3=7, rl2=2, rl5=6, rl4=3, rl7=5, rl6=8, rl8=1, 
rl1=4, rl3=7, rl2=2, rl5=1, rl4=5, rl7=6, rl6=8, rl8=3, 
rl1=4, rl3=8, rl2=2, rl5=7, rl4=5, rl7=3, rl6=1, rl8=6, 
rl1=4, rl3=8, rl2=2, rl5=1, rl4=6, rl7=5, rl6=3, rl8=7, 
rl1=4, rl3=1, rl2=6, rl5=2, rl4=5, rl7=3, rl6=8, rl8=7, 
rl1=4, rl3=8, rl2=6, rl5=7, rl4=2, rl7=3, rl6=1, rl8=5, 
rl1=4, rl3=8, rl2=6, rl5=1, rl4=3, rl7=5, rl6=7, rl8=2, 
rl1=4, rl3=1, rl2=7, rl5=5, rl4=8, rl7=6, rl6=2, rl8=3, 
rl1=4, rl3=3, rl2=7, rl5=2, rl4=8, rl7=1, rl6=5, rl8=6, 
rl1=4, rl3=5, rl2=7, rl5=6, rl4=2, rl7=3, rl6=1, rl8=8, 
rl1=4, rl3=5, rl2=7, rl5=1, rl4=3, rl7=8, rl6=6, rl8=2, 
rl1=4, rl3=1, rl2=8, rl5=6, rl4=3, rl7=7, rl6=2, rl8=5, 
rl1=4, rl3=1, rl2=8, rl5=7, rl4=5, rl7=6, rl6=2, rl8=3, 
rl1=4, rl3=5, rl2=8, rl5=1, rl4=3, rl7=2, rl6=7, rl8=6, 
rl1=5, rl3=4, rl2=1, rl5=8, rl4=6, rl7=7, rl6=2, rl8=3, 
rl1=5, rl3=8, rl2=1, rl5=2, rl4=4, rl7=3, rl6=7, rl8=6, 
rl1=5, rl3=8, rl2=1, rl5=3, rl4=6, rl7=2, rl6=7, rl8=4, 
rl1=5, rl3=4, rl2=2, rl5=8, rl4=6, rl7=1, rl6=3, rl8=7, 
rl1=5, rl3=4, rl2=2, rl5=3, rl4=7, rl7=6, rl6=8, rl8=1, 
rl1=5, rl3=6, rl2=2, rl5=7, rl4=1, rl7=8, rl6=4, rl8=3, 
rl1=5, rl3=8, rl2=2, rl5=4, rl4=1, rl7=3, rl6=7, rl8=6, 
rl1=5, rl3=1, rl2=3, rl5=8, rl4=6, rl7=4, rl6=2, rl8=7, 
rl1=5, rl3=1, rl2=3, rl5=2, rl4=7, rl7=6, rl6=8, rl8=4, 
rl1=5, rl3=8, rl2=3, rl5=7, rl4=4, rl7=6, rl6=1, rl8=2, 
rl1=5, rl3=1, rl2=7, rl5=8, rl4=3, rl7=4, rl6=6, rl8=2, 
rl1=5, rl3=1, rl2=7, rl5=2, rl4=4, rl7=6, rl6=8, rl8=3, 
rl1=5, rl3=2, rl2=7, rl5=8, rl4=4, rl7=3, rl6=1, rl8=6, 
rl1=5, rl3=2, rl2=7, rl5=3, rl4=6, rl7=4, rl6=1, rl8=8, 
rl1=5, rl3=2, rl2=7, rl5=3, rl4=6, rl7=8, rl6=1, rl8=4, 
rl1=5, rl3=4, rl2=7, rl5=3, rl4=1, rl7=6, rl6=8, rl8=2, 
rl1=5, rl3=4, rl2=8, rl5=3, rl4=1, rl7=2, rl6=6, rl8=7, 
rl1=5, rl3=4, rl2=8, rl5=7, rl4=1, rl7=6, rl6=2, rl8=3, 
rl1=6, rl3=5, rl2=1, rl5=8, rl4=2, rl7=7, rl6=3, rl8=4, 
rl1=6, rl3=7, rl2=2, rl5=3, rl4=1, rl7=8, rl6=5, rl8=4, 
rl1=6, rl3=7, rl2=2, rl5=4, rl4=1, rl7=5, rl6=8, rl8=3, 
rl1=6, rl3=1, rl2=3, rl5=5, rl4=7, rl7=2, rl6=8, rl8=4, 
rl1=6, rl3=1, rl2=3, rl5=4, rl4=8, rl7=7, rl6=2, rl8=5, 
rl1=6, rl3=1, rl2=3, rl5=5, rl4=8, rl7=4, rl6=2, rl8=7, 
rl1=6, rl3=5, rl2=3, rl5=1, rl4=7, rl7=2, rl6=4, rl8=8, 
rl1=6, rl3=5, rl2=3, rl5=1, rl4=8, rl7=2, rl6=4, rl8=7, 
rl1=6, rl3=7, rl2=3, rl5=4, rl4=2, rl7=1, rl6=8, rl8=5, 
rl1=6, rl3=7, rl2=3, rl5=8, rl4=2, rl7=1, rl6=5, rl8=4, 
rl1=6, rl3=7, rl2=3, rl5=1, rl4=4, rl7=2, rl6=8, rl8=5, 
rl1=6, rl3=1, rl2=4, rl5=8, rl4=5, rl7=7, rl6=2, rl8=3, 
rl1=6, rl3=2, rl2=4, rl5=5, rl4=8, rl7=1, rl6=7, rl8=3, 
rl1=6, rl3=7, rl2=4, rl5=3, rl4=1, rl7=2, rl6=5, rl8=8, 
rl1=6, rl3=7, rl2=4, rl5=8, rl4=1, rl7=5, rl6=2, rl8=3, 
rl1=6, rl3=2, rl2=8, rl5=1, rl4=4, rl7=5, rl6=7, rl8=3, 
rl1=7, rl3=3, rl2=1, rl5=6, rl4=8, rl7=2, rl6=4, rl8=5, 
rl1=7, rl3=4, rl2=2, rl5=8, rl4=1, rl7=3, rl6=5, rl8=6, 
rl1=7, rl3=6, rl2=2, rl5=1, rl4=3, rl7=8, rl6=4, rl8=5, 
rl1=7, rl3=1, rl2=3, rl5=8, rl4=6, rl7=2, rl6=5, rl8=4, 
rl1=7, rl3=8, rl2=3, rl5=5, rl4=2, rl7=6, rl6=1, rl8=4, 
rl1=7, rl3=2, rl2=4, rl5=8, rl4=5, rl7=3, rl6=1, rl8=6, 
rl1=7, rl3=2, rl2=4, rl5=6, rl4=8, rl7=3, rl6=1, rl8=5, 
rl1=7, rl3=3, rl2=5, rl5=6, rl4=1, rl7=2, rl6=8, rl8=4, 
rl1=8, rl3=4, rl2=2, rl5=7, rl4=1, rl7=3, rl6=5, rl8=6, 
rl1=8, rl3=5, rl2=2, rl5=1, rl4=3, rl7=4, rl6=7, rl8=6, 
rl1=8, rl3=1, rl2=3, rl5=2, rl4=6, rl7=7, rl6=5, rl8=4, 
rl1=8, rl3=1, rl2=4, rl5=6, rl4=3, rl7=7, rl6=2, rl8=5, 



Fichier : colore.txt
preTraitement:
Max-card + MaxDegre:
[SA2, NSW2, Q2, NT2, V2, WA2, WA, SA, NT, Q, NSW, V, T, T2]

SEARCHSOLUTIONBT
varValue=null
varValue=R
varValue=R
varValue=R
varValue=R
varValue=null
varValue=R
varValue=R

Nb de noeuds parcourus = 28
Solution
Q2=B, NSW2=G, SA2=R, NT=B, WA2=B, WA=R, NT2=G, SA=G, Q=R, NSW=B, T=R, V=R, V2=B, T2=R, 
SEARCHALLSOLUTIONBT
varValue=null
varValue=R
varValue=R
varValue=R
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=G
varValue=R
violation de contrainte 
varValue=B
varValue=R
violation de contrainte 
varValue=R
varValue=R
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=G
varValue=R
violation de contrainte 
varValue=B
varValue=R
violation de contrainte 
varValue=null
varValue=G
varValue=R
varValue=G
violation de contrainte 
varValue=G
varValue=G
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=B
varValue=G
violation de contrainte 
varValue=R
varValue=G
violation de contrainte 
varValue=G
varValue=G
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=B
varValue=G
violation de contrainte 
varValue=null
varValue=B
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
varValue=R
varValue=null
varValue=R
varValue=R
varValue=R
varValue=G
violation de contrainte 
varValue=R
varValue=B
violation de contrainte 
varValue=G
varValue=null
varValue=G
varValue=R
violation de contrainte 
varValue=G
varValue=G
varValue=G
varValue=B
violation de contrainte 
varValue=B
varValue=null
varValue=B
varValue=R
violation de contrainte 
varValue=B
varValue=G
violation de contrainte 
varValue=B
varValue=B
Total noeuds parcouru : 409
Total solutions trouvée : 36
Q2=B, NSW2=G, SA2=R, NT=B, WA2=B, WA=R, NT2=G, SA=G, Q=R, NSW=B, T=R, V=R, V2=B, T2=R, 
Q2=B, NSW2=G, SA2=R, NT=B, WA2=B, WA=R, NT2=G, SA=G, Q=R, NSW=B, T=G, V=R, V2=B, T2=G, 
Q2=B, NSW2=G, SA2=R, NT=B, WA2=B, WA=R, NT2=G, SA=G, Q=R, NSW=B, T=B, V=R, V2=B, T2=B, 
Q2=B, NSW2=G, SA2=R, NT=G, WA2=B, WA=R, NT2=G, SA=B, Q=R, NSW=G, T=R, V=R, V2=B, T2=R, 
Q2=B, NSW2=G, SA2=R, NT=G, WA2=B, WA=R, NT2=G, SA=B, Q=R, NSW=G, T=G, V=R, V2=B, T2=G, 
Q2=B, NSW2=G, SA2=R, NT=G, WA2=B, WA=R, NT2=G, SA=B, Q=R, NSW=G, T=B, V=R, V2=B, T2=B, 
Q2=G, NSW2=B, SA2=R, NT=B, WA2=G, WA=R, NT2=B, SA=G, Q=R, NSW=B, T=R, V=R, V2=G, T2=R, 
Q2=G, NSW2=B, SA2=R, NT=B, WA2=G, WA=R, NT2=B, SA=G, Q=R, NSW=B, T=G, V=R, V2=G, T2=G, 
Q2=G, NSW2=B, SA2=R, NT=B, WA2=G, WA=R, NT2=B, SA=G, Q=R, NSW=B, T=B, V=R, V2=G, T2=B, 
Q2=G, NSW2=B, SA2=R, NT=G, WA2=G, WA=R, NT2=B, SA=B, Q=R, NSW=G, T=R, V=R, V2=G, T2=R, 
Q2=G, NSW2=B, SA2=R, NT=G, WA2=G, WA=R, NT2=B, SA=B, Q=R, NSW=G, T=G, V=R, V2=G, T2=G, 
Q2=G, NSW2=B, SA2=R, NT=G, WA2=G, WA=R, NT2=B, SA=B, Q=R, NSW=G, T=B, V=R, V2=G, T2=B, 
Q2=B, NSW2=R, SA2=G, NT=B, WA2=B, WA=G, NT2=R, SA=R, Q=G, NSW=B, T=R, V=G, V2=B, T2=R, 
Q2=B, NSW2=R, SA2=G, NT=B, WA2=B, WA=G, NT2=R, SA=R, Q=G, NSW=B, T=G, V=G, V2=B, T2=G, 
Q2=B, NSW2=R, SA2=G, NT=B, WA2=B, WA=G, NT2=R, SA=R, Q=G, NSW=B, T=B, V=G, V2=B, T2=B, 
Q2=B, NSW2=R, SA2=G, NT=R, WA2=B, WA=G, NT2=R, SA=B, Q=G, NSW=R, T=R, V=G, V2=B, T2=R, 
Q2=B, NSW2=R, SA2=G, NT=R, WA2=B, WA=G, NT2=R, SA=B, Q=G, NSW=R, T=G, V=G, V2=B, T2=G, 
Q2=B, NSW2=R, SA2=G, NT=R, WA2=B, WA=G, NT2=R, SA=B, Q=G, NSW=R, T=B, V=G, V2=B, T2=B, 
Q2=R, NSW2=B, SA2=G, NT=B, WA2=R, WA=G, NT2=B, SA=R, Q=G, NSW=B, T=R, V=G, V2=R, T2=R, 
Q2=R, NSW2=B, SA2=G, NT=B, WA2=R, WA=G, NT2=B, SA=R, Q=G, NSW=B, T=G, V=G, V2=R, T2=G, 
Q2=R, NSW2=B, SA2=G, NT=B, WA2=R, WA=G, NT2=B, SA=R, Q=G, NSW=B, T=B, V=G, V2=R, T2=B, 
Q2=R, NSW2=B, SA2=G, NT=R, WA2=R, WA=G, NT2=B, SA=B, Q=G, NSW=R, T=R, V=G, V2=R, T2=R, 
Q2=R, NSW2=B, SA2=G, NT=R, WA2=R, WA=G, NT2=B, SA=B, Q=G, NSW=R, T=G, V=G, V2=R, T2=G, 
Q2=R, NSW2=B, SA2=G, NT=R, WA2=R, WA=G, NT2=B, SA=B, Q=G, NSW=R, T=B, V=G, V2=R, T2=B, 
Q2=G, NSW2=R, SA2=B, NT=G, WA2=G, WA=B, NT2=R, SA=R, Q=B, NSW=G, T=R, V=B, V2=G, T2=R, 
Q2=G, NSW2=R, SA2=B, NT=G, WA2=G, WA=B, NT2=R, SA=R, Q=B, NSW=G, T=G, V=B, V2=G, T2=G, 
Q2=G, NSW2=R, SA2=B, NT=G, WA2=G, WA=B, NT2=R, SA=R, Q=B, NSW=G, T=B, V=B, V2=G, T2=B, 
Q2=G, NSW2=R, SA2=B, NT=R, WA2=G, WA=B, NT2=R, SA=G, Q=B, NSW=R, T=R, V=B, V2=G, T2=R, 
Q2=G, NSW2=R, SA2=B, NT=R, WA2=G, WA=B, NT2=R, SA=G, Q=B, NSW=R, T=G, V=B, V2=G, T2=G, 
Q2=G, NSW2=R, SA2=B, NT=R, WA2=G, WA=B, NT2=R, SA=G, Q=B, NSW=R, T=B, V=B, V2=G, T2=B, 
Q2=R, NSW2=G, SA2=B, NT=G, WA2=R, WA=B, NT2=G, SA=R, Q=B, NSW=G, T=R, V=B, V2=R, T2=R, 
Q2=R, NSW2=G, SA2=B, NT=G, WA2=R, WA=B, NT2=G, SA=R, Q=B, NSW=G, T=G, V=B, V2=R, T2=G, 
Q2=R, NSW2=G, SA2=B, NT=G, WA2=R, WA=B, NT2=G, SA=R, Q=B, NSW=G, T=B, V=B, V2=R, T2=B, 
Q2=R, NSW2=G, SA2=B, NT=R, WA2=R, WA=B, NT2=G, SA=G, Q=B, NSW=R, T=R, V=B, V2=R, T2=R, 
Q2=R, NSW2=G, SA2=B, NT=R, WA2=R, WA=B, NT2=G, SA=G, Q=B, NSW=R, T=G, V=B, V2=R, T2=G, 
Q2=R, NSW2=G, SA2=B, NT=R, WA2=R, WA=B, NT2=G, SA=G, Q=B, NSW=R, T=B, V=B, V2=R, T2=B, 



Fichier : 5reinesExp.txt
preTraitement:
Max-card + MaxDegre:
[rl1, rl2, rl3, rl4, rl5]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 16
Solution
rl1=1, rl3=3, rl2=2, rl5=5, rl4=4, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 1031
Total solutions trouvée : 120
rl3=3, rl2=2, rl5=5, rl4=4, rl1=1, 
rl3=3, rl2=2, rl5=4, rl4=5, rl1=1, 
rl3=4, rl2=2, rl5=5, rl4=3, rl1=1, 
rl3=4, rl2=2, rl5=3, rl4=5, rl1=1, 
rl3=5, rl2=2, rl5=4, rl4=3, rl1=1, 
rl3=5, rl2=2, rl5=3, rl4=4, rl1=1, 
rl3=2, rl2=3, rl5=5, rl4=4, rl1=1, 
rl3=2, rl2=3, rl5=4, rl4=5, rl1=1, 
rl3=4, rl2=3, rl5=5, rl4=2, rl1=1, 
rl3=4, rl2=3, rl5=2, rl4=5, rl1=1, 
rl3=5, rl2=3, rl5=4, rl4=2, rl1=1, 
rl3=5, rl2=3, rl5=2, rl4=4, rl1=1, 
rl3=2, rl2=4, rl5=5, rl4=3, rl1=1, 
rl3=2, rl2=4, rl5=3, rl4=5, rl1=1, 
rl3=3, rl2=4, rl5=5, rl4=2, rl1=1, 
rl3=3, rl2=4, rl5=2, rl4=5, rl1=1, 
rl3=5, rl2=4, rl5=3, rl4=2, rl1=1, 
rl3=5, rl2=4, rl5=2, rl4=3, rl1=1, 
rl3=2, rl2=5, rl5=4, rl4=3, rl1=1, 
rl3=2, rl2=5, rl5=3, rl4=4, rl1=1, 
rl3=3, rl2=5, rl5=4, rl4=2, rl1=1, 
rl3=3, rl2=5, rl5=2, rl4=4, rl1=1, 
rl3=4, rl2=5, rl5=3, rl4=2, rl1=1, 
rl3=4, rl2=5, rl5=2, rl4=3, rl1=1, 
rl3=3, rl2=1, rl5=5, rl4=4, rl1=2, 
rl3=3, rl2=1, rl5=4, rl4=5, rl1=2, 
rl3=4, rl2=1, rl5=5, rl4=3, rl1=2, 
rl3=4, rl2=1, rl5=3, rl4=5, rl1=2, 
rl3=5, rl2=1, rl5=4, rl4=3, rl1=2, 
rl3=5, rl2=1, rl5=3, rl4=4, rl1=2, 
rl3=1, rl2=3, rl5=5, rl4=4, rl1=2, 
rl3=1, rl2=3, rl5=4, rl4=5, rl1=2, 
rl3=4, rl2=3, rl5=5, rl4=1, rl1=2, 
rl3=4, rl2=3, rl5=1, rl4=5, rl1=2, 
rl3=5, rl2=3, rl5=4, rl4=1, rl1=2, 
rl3=5, rl2=3, rl5=1, rl4=4, rl1=2, 
rl3=1, rl2=4, rl5=5, rl4=3, rl1=2, 
rl3=1, rl2=4, rl5=3, rl4=5, rl1=2, 
rl3=3, rl2=4, rl5=5, rl4=1, rl1=2, 
rl3=3, rl2=4, rl5=1, rl4=5, rl1=2, 
rl3=5, rl2=4, rl5=3, rl4=1, rl1=2, 
rl3=5, rl2=4, rl5=1, rl4=3, rl1=2, 
rl3=1, rl2=5, rl5=4, rl4=3, rl1=2, 
rl3=1, rl2=5, rl5=3, rl4=4, rl1=2, 
rl3=3, rl2=5, rl5=4, rl4=1, rl1=2, 
rl3=3, rl2=5, rl5=1, rl4=4, rl1=2, 
rl3=4, rl2=5, rl5=3, rl4=1, rl1=2, 
rl3=4, rl2=5, rl5=1, rl4=3, rl1=2, 
rl3=2, rl2=1, rl5=5, rl4=4, rl1=3, 
rl3=2, rl2=1, rl5=4, rl4=5, rl1=3, 
rl3=4, rl2=1, rl5=5, rl4=2, rl1=3, 
rl3=4, rl2=1, rl5=2, rl4=5, rl1=3, 
rl3=5, rl2=1, rl5=4, rl4=2, rl1=3, 
rl3=5, rl2=1, rl5=2, rl4=4, rl1=3, 
rl3=1, rl2=2, rl5=5, rl4=4, rl1=3, 
rl3=1, rl2=2, rl5=4, rl4=5, rl1=3, 
rl3=4, rl2=2, rl5=5, rl4=1, rl1=3, 
rl3=4, rl2=2, rl5=1, rl4=5, rl1=3, 
rl3=5, rl2=2, rl5=4, rl4=1, rl1=3, 
rl3=5, rl2=2, rl5=1, rl4=4, rl1=3, 
rl3=1, rl2=4, rl5=5, rl4=2, rl1=3, 
rl3=1, rl2=4, rl5=2, rl4=5, rl1=3, 
rl3=2, rl2=4, rl5=5, rl4=1, rl1=3, 
rl3=2, rl2=4, rl5=1, rl4=5, rl1=3, 
rl3=5, rl2=4, rl5=2, rl4=1, rl1=3, 
rl3=5, rl2=4, rl5=1, rl4=2, rl1=3, 
rl3=1, rl2=5, rl5=4, rl4=2, rl1=3, 
rl3=1, rl2=5, rl5=2, rl4=4, rl1=3, 
rl3=2, rl2=5, rl5=4, rl4=1, rl1=3, 
rl3=2, rl2=5, rl5=1, rl4=4, rl1=3, 
rl3=4, rl2=5, rl5=2, rl4=1, rl1=3, 
rl3=4, rl2=5, rl5=1, rl4=2, rl1=3, 
rl3=2, rl2=1, rl5=5, rl4=3, rl1=4, 
rl3=2, rl2=1, rl5=3, rl4=5, rl1=4, 
rl3=3, rl2=1, rl5=5, rl4=2, rl1=4, 
rl3=3, rl2=1, rl5=2, rl4=5, rl1=4, 
rl3=5, rl2=1, rl5=3, rl4=2, rl1=4, 
rl3=5, rl2=1, rl5=2, rl4=3, rl1=4, 
rl3=1, rl2=2, rl5=5, rl4=3, rl1=4, 
rl3=1, rl2=2, rl5=3, rl4=5, rl1=4, 
rl3=3, rl2=2, rl5=5, rl4=1, rl1=4, 
rl3=3, rl2=2, rl5=1, rl4=5, rl1=4, 
rl3=5, rl2=2, rl5=3, rl4=1, rl1=4, 
rl3=5, rl2=2, rl5=1, rl4=3, rl1=4, 
rl3=1, rl2=3, rl5=5, rl4=2, rl1=4, 
rl3=1, rl2=3, rl5=2, rl4=5, rl1=4, 
rl3=2, rl2=3, rl5=5, rl4=1, rl1=4, 
rl3=2, rl2=3, rl5=1, rl4=5, rl1=4, 
rl3=5, rl2=3, rl5=2, rl4=1, rl1=4, 
rl3=5, rl2=3, rl5=1, rl4=2, rl1=4, 
rl3=1, rl2=5, rl5=3, rl4=2, rl1=4, 
rl3=1, rl2=5, rl5=2, rl4=3, rl1=4, 
rl3=2, rl2=5, rl5=3, rl4=1, rl1=4, 
rl3=2, rl2=5, rl5=1, rl4=3, rl1=4, 
rl3=3, rl2=5, rl5=2, rl4=1, rl1=4, 
rl3=3, rl2=5, rl5=1, rl4=2, rl1=4, 
rl3=2, rl2=1, rl5=4, rl4=3, rl1=5, 
rl3=2, rl2=1, rl5=3, rl4=4, rl1=5, 
rl3=3, rl2=1, rl5=4, rl4=2, rl1=5, 
rl3=3, rl2=1, rl5=2, rl4=4, rl1=5, 
rl3=4, rl2=1, rl5=3, rl4=2, rl1=5, 
rl3=4, rl2=1, rl5=2, rl4=3, rl1=5, 
rl3=1, rl2=2, rl5=4, rl4=3, rl1=5, 
rl3=1, rl2=2, rl5=3, rl4=4, rl1=5, 
rl3=3, rl2=2, rl5=4, rl4=1, rl1=5, 
rl3=3, rl2=2, rl5=1, rl4=4, rl1=5, 
rl3=4, rl2=2, rl5=3, rl4=1, rl1=5, 
rl3=4, rl2=2, rl5=1, rl4=3, rl1=5, 
rl3=1, rl2=3, rl5=4, rl4=2, rl1=5, 
rl3=1, rl2=3, rl5=2, rl4=4, rl1=5, 
rl3=2, rl2=3, rl5=4, rl4=1, rl1=5, 
rl3=2, rl2=3, rl5=1, rl4=4, rl1=5, 
rl3=4, rl2=3, rl5=2, rl4=1, rl1=5, 
rl3=4, rl2=3, rl5=1, rl4=2, rl1=5, 
rl3=1, rl2=4, rl5=3, rl4=2, rl1=5, 
rl3=1, rl2=4, rl5=2, rl4=3, rl1=5, 
rl3=2, rl2=4, rl5=3, rl4=1, rl1=5, 
rl3=2, rl2=4, rl5=1, rl4=3, rl1=5, 
rl3=3, rl2=4, rl5=2, rl4=1, rl1=5, 
rl3=3, rl2=4, rl5=1, rl4=2, rl1=5, 



Fichier : cryptoMoney.txt
preTraitement:
Max-card + MaxDegre:
[E, N, O, R, T2, D, Y, M, S, T1, T3]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 8162
Solution
R=8, S=9, D=7, E=5, Y=2, M=1, T1=1, N=6, T2=1, O=0, T3=0, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 14782
Total solutions trouvée : 1
R=8, S=9, D=7, E=5, Y=2, M=1, T1=1, N=6, T2=1, O=0, T3=0, 



Fichier : colorationsDif.txt
preTraitement:
Max-card + MaxDegre:
[SA, NSW, Q, NT, V, WA, T]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 16
Solution
Q=B, NSW=G, T=R, NT=G, V=B, WA=B, SA=R, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 103
Total solutions trouvée : 18
Q=B, NSW=G, T=R, NT=G, V=B, WA=B, SA=R, 
Q=B, NSW=G, T=G, NT=G, V=B, WA=B, SA=R, 
Q=B, NSW=G, T=B, NT=G, V=B, WA=B, SA=R, 
Q=G, NSW=B, T=R, NT=B, V=G, WA=G, SA=R, 
Q=G, NSW=B, T=G, NT=B, V=G, WA=G, SA=R, 
Q=G, NSW=B, T=B, NT=B, V=G, WA=G, SA=R, 
Q=B, NSW=R, T=R, NT=R, V=B, WA=B, SA=G, 
Q=B, NSW=R, T=G, NT=R, V=B, WA=B, SA=G, 
Q=B, NSW=R, T=B, NT=R, V=B, WA=B, SA=G, 
Q=R, NSW=B, T=R, NT=B, V=R, WA=R, SA=G, 
Q=R, NSW=B, T=G, NT=B, V=R, WA=R, SA=G, 
Q=R, NSW=B, T=B, NT=B, V=R, WA=R, SA=G, 
Q=G, NSW=R, T=R, NT=R, V=G, WA=G, SA=B, 
Q=G, NSW=R, T=G, NT=R, V=G, WA=G, SA=B, 
Q=G, NSW=R, T=B, NT=R, V=G, WA=G, SA=B, 
Q=R, NSW=G, T=R, NT=G, V=R, WA=R, SA=B, 
Q=R, NSW=G, T=G, NT=G, V=R, WA=R, SA=B, 
Q=R, NSW=G, T=B, NT=G, V=R, WA=R, SA=B, 



Fichier : cryptogramme2+2Exp.txt
preTraitement:
Max-card + MaxDegre:
[O, W, T, U, F, R]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 7
Solution
R=0, T=0, U=0, F=0, W=0, O=0, 
SEARCHALLSOLUTIONBT
Total noeuds parcouru : 1016
Total solutions trouvée : 30
R=0, T=0, U=0, F=0, W=0, O=0, 
R=0, T=5, U=0, F=1, W=0, O=0, 
R=0, T=0, U=2, F=0, W=1, O=0, 
R=0, T=5, U=2, F=1, W=1, O=0, 
R=0, T=0, U=4, F=0, W=2, O=0, 
R=0, T=5, U=4, F=1, W=2, O=0, 
R=0, T=0, U=6, F=0, W=3, O=0, 
R=0, T=5, U=6, F=1, W=3, O=0, 
R=0, T=0, U=8, F=0, W=4, O=0, 
R=0, T=5, U=8, F=1, W=4, O=0, 
R=4, T=1, U=0, F=0, W=0, O=2, 
R=4, T=6, U=0, F=1, W=0, O=2, 
R=4, T=1, U=2, F=0, W=1, O=2, 
R=4, T=6, U=2, F=1, W=1, O=2, 
R=4, T=1, U=4, F=0, W=2, O=2, 
R=4, T=6, U=4, F=1, W=2, O=2, 
R=4, T=1, U=6, F=0, W=3, O=2, 
R=4, T=6, U=6, F=1, W=3, O=2, 
R=4, T=1, U=8, F=0, W=4, O=2, 
R=4, T=6, U=8, F=1, W=4, O=2, 
R=8, T=2, U=0, F=0, W=0, O=4, 
R=8, T=7, U=0, F=1, W=0, O=4, 
R=8, T=2, U=2, F=0, W=1, O=4, 
R=8, T=7, U=2, F=1, W=1, O=4, 
R=8, T=2, U=4, F=0, W=2, O=4, 
R=8, T=7, U=4, F=1, W=2, O=4, 
R=8, T=2, U=6, F=0, W=3, O=4, 
R=8, T=7, U=6, F=1, W=3, O=4, 
R=8, T=2, U=8, F=0, W=4, O=4, 
R=8, T=7, U=8, F=1, W=4, O=4, 



Fichier : instance.txt
preTraitement:
Max-card + MaxDegre:
[x1, x5, x6, x2, x4, x3]

SEARCHSOLUTIONBT

Nb de noeuds parcourus = 11
Solution

SEARCHALLSOLUTIONBT
Total noeuds parcouru : 11
Total solutions trouvée : 0
FIN