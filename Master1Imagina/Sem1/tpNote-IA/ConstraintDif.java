package tp1BackTrack;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;



public class ConstraintDif extends Constraint
{
	public ConstraintDif(BufferedReader in) throws Exception
	{
		super(in);

	}

	public boolean violation (Assignment a)
	{
		// System.out.println(this.toString());
		// on récupère la valeur de la premier variable 
		// ayant une contrainte d'égalité avec les variables suivante
		//  dans varlist
		Object varValue1 , varValue2;
		
		for (int i=0; i<varList.size() - 1; i++)
		{	
			varValue1 = a.get(varList.get(i));
			for (int j = i+1; j < varList.size(); j++)
			{	
				// System.out.println("i="+i+ " j="+j);
				
				varValue2 = a.get(varList.get(j));
				// System.out.println("\t"+varList.get(i)+"=" + varValue1 +", "+varList.get(j)+"=" +varValue2);
				
				if ( (varValue1!=null && varValue2!=null ) 
					&& varValue1.toString().equals(varValue2.toString()) )
				{
					// System.out.println("violation de contrainteDif");
					return true;
				}	
			}
		}

		// // System.out.println("Pas de viol de C");
		 return false;
		
	}

	protected HashMap<String, ArrayList<Object>> modify(String var , HashMap<String,ArrayList<Object>> varsDom , Assignment assignment)
	{
		// TODO Auto-generated method stub
		
		// Pour toutes les variables varModifDom
		// en relation à var par la contrainte this.
		Object valeurdevar= assignment.get(var);
		
		HashMap<String, ArrayList<Object>> newVarsDom =
 	 		new HashMap<String, ArrayList<Object>>();

 	 	ArrayList <Object> d1= new ArrayList<Object>();
		ArrayList <Object> dfinal=null;
		
		


 	 	for (String varD : varsDom.keySet())
		{
			newVarsDom.put(varD,varsDom.get(varD));
		}
		
		for(String varModifDom : this.getVars())
		{
			// On modifie le domaine de la variable 
			// varModifDom en lui enlevant toutes les valeur
			// du domaine non égale à l'assignation de var courante.
			if (!varModifDom.equals(var))
			{

						
				// On parcours toutes les valeurs du domaine de 
				// la variable varModifDom et on élimine toutes
				// les valeurs qui ne sont pas égales à la valeur
				// courante de var. 
				for(Iterator <Object> iterator = newVarsDom.get(varModifDom).iterator();iterator.hasNext();)
				{
					Object valeurdeDom = iterator.next();
					if((!valeurdeDom.toString().equals(valeurdevar.toString() )))
					{
						// System.out.println("Comme var="+var+" "+"valvar=" +valeurdevar +" j'enleve  varModifDom="+varModifDom
						// 		+ " valeurdeDom="+valeurdeDom.toString());
						d1.add(valeurdeDom);

						// iterator.remove();
					}

				}

				dfinal = InterArrayList.intersection(d1,newVarsDom.get(varModifDom));

				newVarsDom.put(varModifDom,dfinal);
				

			}



		}
//		TODO
		return newVarsDom;
		
	}

	@Override
	public ArrayList<ArrayList<Object>> getTuples() {
		// TODO Auto-generated method stub
		return null;
	}
}
