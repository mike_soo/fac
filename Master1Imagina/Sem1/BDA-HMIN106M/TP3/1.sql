CREATE OR REPLACE TYPE Adresse_t AS OBJECT
( numero NUMBER, 
  rue VARCHAR2(20), 
  ville VARCHAR2(30), 
  code-postal VARCHAR2(10));
/

CREATE OR REPLACE TYPE Personne_t AS OBJECT
(
	num NUMBER,
	nom VARCHAR(20),
	prenom VARCHAR(20)
);

CREATE OR REPLACE TYPE Etudiant_t AS OBJECT
( NumCarte  NUMBER,
  Adr Adresse_t,
  Date_naiss DATE,
  TAD Etudiant_t
  age;
/

ALTER TABLE Etudiant
  ADD CONSTRAINT CTYPE_ABONNE CHECK (TYPE_AB IN (''ADULTE'', ''ENFANT''));
