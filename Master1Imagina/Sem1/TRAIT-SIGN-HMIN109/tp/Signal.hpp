#ifndef SIGNAL_HPP
#define SIGNAL_HPP

class Signal {


private :
  

public:
       
 
  static void DFT(double *signal, double *partie_reelle, double *partie_imaginaire, size_t N);
  static void IDFT(double *signal, double *partie_reelle, double *partie_imaginaire, size_t N);       

  static void ampl_spectral_analysis(double *spectre, double *partie_reelle, double *partie_imaginaire, size_t N);  
  
  static void createSignal(double *tabSinLaEch, double freqSignal, double freqEch, double retard, size_t N /*nb d'echantillons*/ );
  static int FFT(int dir,int m,double *x,double *y);

  static double get_indice_tab_freq(double freq_souh ,size_t N, double freq_ech);

  

  static void bass_pass_filter(double *data_chord_re, double *data_chord_im, double freq_ech, size_t N , double freq_c);
  static void high_pass_filter(double *data_chord_re, double *data_chord_im, double freq_ech, size_t N , double freq_c);
  static void band_pass_filter(double *data_chord_re, double *data_chord_im, double freq_ech, size_t N , double freq_c1, double freq_c2 );
  static void notch_pass_filter(double *data_chord_re,double *data_chord_im , double freq_ech, size_t N, double freq_c1 , double freq_c2);
  static void filter_butterworth(double *data_in , double * data_out,size_t N ,double a);
  

  Signal();
  ~Signal();
  
};

void ctod(double* dsignal , unsigned char* csignal , size_t N);
void dtoc(double* dsignal , unsigned char* csignal , size_t N);
#endif //IO_SIGNAL_HPP
