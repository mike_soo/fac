

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <math.h>
using namespace std;

#include "Wave.hpp"




int main (int argc , char ** argv)
{
	// Create LA
	char nom_WAV[100];
	char cfreqEch[50];
	memset(cfreqEch,'\0',100);
	memset(nom_WAV,'\0',100);
	
	int freqEch = 1 * 5120.0;
	int duration = 1;/* temps du signal souhaité en sec.*/
	
	size_t N = duration * freqEch; /*nombre d'échantillon total*/
	
	sprintf(cfreqEch,"%d",freqEch);
	
	
	unsigned char* tabSinLaEchQuan = new unsigned char[N];
	double * tabSinLaEch = new double[N];
     
	 
   
	Wave::createSignal(tabSinLaEch,261.63 /*freq do*/,freqEch,0,N);;
	
	dtoc(tabSinLaEch , tabSinLaEchQuan , N);

	strcpy(nom_WAV,"./Do_fe_");
	strcat(nom_WAV, cfreqEch);
	strcat(nom_WAV, ".wav");
	Wave Do (tabSinLaEchQuan, N ,1 , freqEch) ;
	Do.write(nom_WAV); 
	 
	
	double *partie_reelleEch = new double[N];
	double *partie_imaginaireEch= new double[N];

	cout<<endl<<"Do DFT"<<endl;
	Wave::DFT ( tabSinLaEch , partie_reelleEch , partie_imaginaireEch, N);
	
	unsigned char * partie_reelleEchQuan = new unsigned char[ N];
	unsigned char * partie_imagEchQuan = new unsigned char[N];

	dtoc(partie_reelleEch , partie_reelleEchQuan, N);
	dtoc(partie_imaginaireEch, partie_imagEchQuan, N);


	strcpy(nom_WAV,"./Do_tf_Reel_fe_");
	strcat(nom_WAV,cfreqEch);
	strcat(nom_WAV, ".wav");
	Wave DotfRe( partie_reelleEchQuan, N , 1, freqEch);
	DotfRe.write(nom_WAV);

	strcpy(nom_WAV,"./Do_tf_Im_fe_");
	strcat(nom_WAV,cfreqEch);
	strcat(nom_WAV, ".wav");
	Wave DotfIm(partie_imagEchQuan, N , 1 , freqEch);
	DotfIm.write(nom_WAV);


	for(size_t i=0 ; i< N ; i++)
	{
		tabSinLaEch[i]=0;
		tabSinLaEchQuan[i]=0;
	}

	cout<<endl<<"Do IDFT"<<endl;
	Wave::IDFT(tabSinLaEch, partie_reelleEch, partie_imaginaireEch, N);
	dtoc (tabSinLaEch , tabSinLaEchQuan , N );
	
	strcpy(nom_WAV,"./Do_tf_Inv_fe_");
	strcat(nom_WAV,cfreqEch);
	strcat(nom_WAV, ".wav");
	Wave DotfInv(tabSinLaEchQuan, N , 1 , freqEch );
	DotfInv.write(nom_WAV);


	
	double  *tabFreqLaEch = new double[N];
	unsigned char *tabFreqLaEchQuan = new unsigned char[N];
	for (size_t i =0 ; i < N ; i ++ )
	{
		tabFreqLaEch[i]= 0;

	}
	tabFreqLaEch[261]= 2200 ;
	tabFreqLaEch[N - 261] = 2200;

	dtoc(tabFreqLaEch , tabFreqLaEchQuan , N);

	
	Wave Do_freq(tabFreqLaEchQuan, N , 1, freqEch);
	strcpy(nom_WAV,"./Do_freq_fe_");
	strcat(nom_WAV,cfreqEch);
	strcat(nom_WAV,".wav");
	Do_freq.write(nom_WAV);
	
	cout<<endl<<"Do freq DFT"<<endl;
	Wave::DFT(tabFreqLaEch , partie_reelleEch,partie_imaginaireEch,N);

	dtoc(partie_reelleEch, partie_reelleEchQuan,N);
	dtoc(partie_imaginaireEch, partie_imagEchQuan,N);

	Wave Do_freq_tf_reel(partie_reelleEchQuan, N , 1 , freqEch);
	strcpy(nom_WAV,"./Do_freq_tf_reel_fe_");
	strcat(nom_WAV,cfreqEch);
	strcat(nom_WAV,".wav");
	Do_freq_tf_reel.write(nom_WAV);


	Wave Do_freq_tf_im(partie_imagEchQuan, N , 1 , freqEch );
	strcpy(nom_WAV,"./Do_freq_tf_im_fe_");
	strcat(nom_WAV,cfreqEch);
	strcat(nom_WAV,".wav");
	Do_freq_tf_im.write(nom_WAV);




	return 0;
}