#!/usr/bin/gnuplot
# $Id: simple.dem,v 1.7 2012/11/18 23:12:22 sfeam Exp $
#
# Requires data files "[123].dat" from this directory,
# so change current working directory to this directory before running.
# gnuplot> set term <term-type>
# gnuplot> load 'simple.dem'
#

set title "Simple Plots" font ",20"
set key left box
set samples 500
set style data points

plot [-10:10] sin(x),atan(x),cos(atan(x))

pause -1