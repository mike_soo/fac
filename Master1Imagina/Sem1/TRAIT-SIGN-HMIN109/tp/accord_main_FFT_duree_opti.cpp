#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <unordered_map>	
#include <vector>
#include <string.h>
using namespace std;

#include "Wave.hpp"


const int TAILLE_ACCORD = 50;

const unsigned int FREQ_INIT = 2000;


const unordered_map <string , double > note_freq(
	{
		{"do",261.63},
		{"re",277.18},
		{"mi",329.63},
		{"fa",349.23},
		{"sol",392.00},
		{"la",440.00},
		{"si",493.88}
		
	});


/*
	params
	freq : fréquence souhaité
	dure : la durée en seconde du signal représenté 
*/	
double get_indice_tab_freq_FFT(double freq_souh , double duree , double freq_ech)
{
	return (freq_souh*pow(2.0,floor(log2(duree*freq_ech)) + 1))/freq_ech;
}

/*
	params
	freq : fréquence souhaité
	dure : la durée en seconde du signal représenté 
*/	
double get_indice_tab_freq(double freq, double duree)
{
	return freq*duree;
}


int main(int argc, char ** argv)
{
	if(argc < 3)
	{
		cout<<"Rentrez params : note1 note2 ... noteN duree"<<endl;
		exit(0);
	}

	vector <double >* freq_souhaitees = new vector<double> ();

	
	/*
	Lecture de paramètres indiquant quels note devront être pensantes
	dans l'accord en cours de construction.
	*/
	char nom_WAV[100],tmp_nom_WAV[100];
	memset(nom_WAV,'\0',100);

	double freq_max=0;
	for(int i = 1 ; i < argc - 1 ;i++)
	{
		string note(argv[i]);

		auto it = note_freq.find(note);
		
		if (it != note_freq.end())
		{
			freq_souhaitees->push_back(it->second);
			if (it->second > freq_max)
			{
				freq_max = it->second;
			}

			strcat(nom_WAV,argv[i]);
			strcat(nom_WAV,"_");
		}
		else
		{
			cout<<"note: "<<argv[i]<<" non trouvé"<<endl;
		}

	}
	
	cout<< "frequences souhaité : ";
	for( unsigned int i = 0 ; i < freq_souhaitees->size() ; i ++)
	{
		cout<<freq_souhaitees->at(i)<<"Hz , ";
	}
	cout<<endl;
	int duree = atoi(argv[ argc - 1 ]);

	unsigned int freq_ech= freq_max * 10;
	size_t N_temp = duree * freq_ech;
	size_t m = log2(N_temp);
	m++;
	size_t N= pow(2,m); 
	freq_ech = N / duree;
	size_t diff_Ns = N - N_temp;
	freq_ech 

	cout<<"m = "<<m<<" , N = "<<N<<endl;
	double* chord_data_re = new double[N];
	double* chord_data_im = new double[N];	
	double* chord_ampl_spectre = new double[N];	
	
	unsigned char* chord_data_re_quan = new unsigned char[N];
	unsigned char* chord_data_im_quan = new unsigned char[N];	
	unsigned char* chord_ampl_spectre_quan = new unsigned char[N];


	for (unsigned int i =0 ; i< N ; i ++)
	{
		chord_data_re[i] = 0;
		chord_data_im[i] = 0;		
	}

	size_t ind_acces_freq ;

	for (unsigned int i =0 ; i < freq_souhaitees->size() ; i++)
	{
		ind_acces_freq= get_indice_tab_freq_FFT(freq_souhaitees->at(i), duree, freq_ech);
		chord_data_re[ind_acces_freq]= FREQ_INIT; 
		chord_data_re[ N - ind_acces_freq ] = FREQ_INIT;
	}

	cout<<"Creation FREQ ---> TEMPORELLE"<<endl;
	


	dtoc(chord_data_re, chord_data_re_quan, N);
	
	Wave chord_wave (chord_data_re_quan, N, 1, freq_ech);
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"freq.wav");
	chord_wave.write(tmp_nom_WAV);

	//Transformé rapide de fourrier inverse discréte de tab de freqs
	Wave::FFT(-1,m,chord_data_re,chord_data_im);	
	dtoc(chord_data_re, chord_data_re_quan,N);
	dtoc(chord_data_im, chord_data_im_quan,N);

	//modif data pour ecrire .wav de la partie réel du DFT	
	chord_wave.modifData8(chord_data_re_quan);
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"freq_tf_re.wav");
	chord_wave.write(tmp_nom_WAV);

	//modif data pour ecrire .wav de la partie imaginaire du DFT
	chord_wave.modifData8(chord_data_im_quan);
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"freq_tf_im.wav");
	chord_wave.write(tmp_nom_WAV);

	cout<<"Creation TEMPORELLE ---> FREQ"<<endl;
	
	
	for (unsigned int i =0 ; i< N ; i ++)
	{
		chord_data_re[i] = 0;
		chord_data_im[i] = 0;	
	}

	double two_pi = M_PI * 2 ;
		
	for (unsigned int i = 0 ; i< freq_souhaitees->size() ; i++)
	{
		freq_souhaitees->at(i) = freq_souhaitees->at(i) / freq_ech;
	}

	//Creation d'accord par addition de sinus
	for(unsigned int k=0 ; k < N ; k++)
	{
		double two_pi_k = two_pi * k;
		for (unsigned int i = 0 ; i< freq_souhaitees->size() ; i++)
		{
			chord_data_re[k]+=(double) sin(two_pi_k * freq_souhaitees->at(i));	
			
		}
		

		
	}
	dtoc(chord_data_re, chord_data_re_quan , N);


	chord_wave.modifData8(chord_data_re_quan);
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"temp.wav");
	chord_wave.write(tmp_nom_WAV);

	
	Wave::FFT(1,m,chord_data_re,chord_data_im);
		
	dtoc(chord_data_re, chord_data_re_quan , N);
	chord_wave.modifData8(chord_data_re_quan);	
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"temp_tf_re.wav");
	chord_wave.write(tmp_nom_WAV);

	dtoc(chord_data_im, chord_data_im_quan , N);		
	chord_wave.modifData8(chord_data_im_quan);
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"temp_tf_im.wav");
	chord_wave.write(tmp_nom_WAV);
	

	Wave::ampl_spectral_analysis(chord_ampl_spectre , chord_data_re, chord_data_im, N);
	dtoc(chord_ampl_spectre, chord_ampl_spectre_quan, N );
	chord_wave.modifData8(chord_ampl_spectre_quan);
	strcpy(tmp_nom_WAV,nom_WAV);
	strcat(tmp_nom_WAV,"temp_tf_ampl_spect.wav");
	chord_wave.write(tmp_nom_WAV);

	

	

}