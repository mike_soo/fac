#ifndef JEUDEVIE_HPP
#define JEUDEVIE_HPP
#include <iostream>

class jeuDeVie{
private:
  bool vivante;
  int x,y;
  unsigned int contin,m,n,bord;
  int **tabJeu,**tabJeuExt;
public:
  
  jeuDeVie(char *fichier);

  void modifCase(int x,int y); 

  void afficheTab2(int m,int n);
  
  void modifCase2(int x,int y);
  
  void iniCel(int i,int j);
  
  bool Vivante(int x, int y)const;
  //  voisine compteur:
  int verifieVoisin(int x,int y,int m,int n);
 
  void chargeFichier(char *fichier);

  void extend();
  
  void saisieLive();
  
  void effaceLive();
  
  void sauvegardeFichier();
 //autre methode
 //nouvelle fonction ecrit pour le td 4 
 void nettoie(std::string &s);

 void jeuLive();
 
 bool findCleVal(std::string &s,std::string &s1,std::string &s2);
 //finNouvelle fonction td4
 void consTabCel();

 void afficheTab2SM(); //AFFICHE TAB SANS MODIFICATION

};
#endif

