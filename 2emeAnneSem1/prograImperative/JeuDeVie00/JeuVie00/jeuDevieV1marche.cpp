#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "jeuDevie.hpp"

using namespace std;
/*
Cellule::Cellule(): vivante(false) {}
*/

bool Cellule::Vivante(int x,int y)const
{ 
  if((tabJeu[y][x]==1)||(tabJeu[y][x]==2))
     
      {return true;}
  else if(tabJeu[y][x]==3)
    {return false;}
  
 }

void Cellule::iniCel(int x,int y)
{if((x<m)&&(y<n))
  tabJeu[y][x]=1;

  else 
    cout<<"depassement du tableau!"<<endl; 
}

int Cellule::verifieVoisin(int x,int y,int m,int n)
{ int cpt=0;
  int i=y-1; //ordonné
  int j=x-1; //abscisse
  int i2=i+2;
  int j2=j+2;
  
  while((i<=i2)&&(i<n)) //on evite le depassement du tableau
    {
      while ((j<=j2)&&(j<m)) 
	{
	  if((i>=0)&&(j>=0)&&(Vivante(j,i)))
            {
              cpt++;
	    
              if ((i==y)&&(j==x))
	      {cpt--;}
            }
          j++;
        }
      
     j-=3; i++;
      
    }
 
  /*{ cout<<">la cellule ("<<x<<",-"<<y<<")"<<"est = "<<Vivante(x,y)<<" et posséde "<<cpt<<" voisin"<<endl;
    }*/
  return cpt;
}

void Cellule::afficheTab2(int x,int y)
{ 
  if(tabJeu[y][x]==2)
   cout<<"2 ";
  else if(tabJeu[y][x]==3) 
   cout<<"3 ";
  else if (tabJeu[y][x]==1)
   cout<<"1 ";
  else 
   cout<<"0 ";
}

void Cellule::modifCase(int x,int y)          //etape intermediare
{int voisinV=verifieVoisin(x,y,this->m,this->n);
 
  if (voisinV<=1)
    {tabJeu[y][x]=2*Vivante(x,y);}
 else if(voisinV>3)
    {tabJeu[y][x]=2*Vivante(x,y);}
 else if(voisinV==3)
   {tabJeu[y][x]=(-2)*Vivante(x,y)+3;}
}

void Cellule::modifCase2(int x,int y)      //modification
{if (tabJeu[y][x]==2)
    {tabJeu[y][x]=0;}
  else if(tabJeu[y][x]==3)
    {tabJeu[y][x]=1;}
}

//ajout des fonctions du prof

void Cellule::nettoie(std::string &s) 
 {
  int beg=0,end=s.size()-1;
  while(beg<end+1 && s[beg]==' ') beg++;
  while(end>beg-1 && s[end]==' ')end--;
  s=s.substr(beg,end-beg+1);
  int pos=s.find_first_of("#");
  s=s.substr(0,pos);
 } 

Cellule::Cellule(char *fichier) : bord(0),contin(1),m(0),n(0)
{ if(fichier!=NULL)
  { int i,indi,indj,cptTailleTabFiI,cptTailleTabFiJ; //les deux indices pour ce reperer dans le fichier et le compteur de #dans le fichier
    string c;
    indi=0;
    indj=0;
    i=0;
    cptTailleTabFiI=0; 
    cptTailleTabFiJ=0; //compte le nombre de case qui sont dans le fichier qui ont comme valeur 0 ou 1;
    
    ifstream fichLecture;
    fichLecture.open(fichier,ios::in|ios::binary);
    
    if(fichLecture.is_open()) 
    {
      cout<<"fichier ouvertttt"<<endl;
      /*while(getline(fichLecture,c,'\n'))
        { i=0;
          while(c[i]!='#')
          {cout<<c[i];
           i++;
          }
          cout<<endl;
        }
      */ 
      //on cherche la taille du tableau rentrer en parametre fichier
      while(getline(fichLecture,c,'\n'))
      { 
        if(m==0)
        { 
          while(c[i]!='#')
          { 
            if((c[i]=='0') or (c[i]=='1'))
             cptTailleTabFiJ++;
            
            i++;
          }
        }
       this->m=cptTailleTabFiJ;
      
       cptTailleTabFiI++;
      }
     
      this->n=cptTailleTabFiI;
      

      cout<<"m : "<<this->m<<endl;
      cout<<"n : "<<this->n<<endl;    

      //tabJeu=new int*[this->n];
      
      for(int k=0;k<this->n;k++)
      {
        tabJeu[k]=new int[this->m];
      } 
      //on stock ligne par ligne du fichier dans c pour copier les valeur dans tabJeu;
      i=0;
      fichLecture.clear();
      fichLecture.seekg(0,ios::beg);
      /* exemple qui marche!
       while(getline(fichLecture,c,'\n'))
       {
        while(c[i]!='#')
         {
           cout<<c[i];
           i++;
         }
         i=0;
         cout<<endl;
       } fin exemple qui marche!
       */ 
      while(getline(fichLecture,c,'\n'))
      { 
         while(c[i]!='#')
        { 
          if (c[i]=='1')
          {tabJeu[indi][indj]=1;
            i++;

            indj++;
            
          }
          else if (c[i]=='0')
          { 
            tabJeu[indi][indj]=0;
            i++;
            indj++;
           
          }
          else if (c[i]=='|')
          { 
            i++;
            
          }
        }

       indi++;
       indj=0; 
       i=0;
      }
    }
    
    for(int i=0;i<this->n;i++)
      {
        for (int j=0;j<this->m;j++)
        {
         afficheTab2(j,i);
        }
        cout<<endl;
      }
   
   //on recopie le fonctionnement deja existent;
      //AGRADISSEMENT DU TERRAIN
   /*for(int ii=0;ii<this->n;ii++)
    for(int jj=0;jj<this->m;jj++)
     {
       if((tabJeu[0][jj]==1)||(tabJeu[ii][0]==1)||(tabJeu[n-1][jj]==1)||(tabJeu[ii][m-1]))
          {
            bord=1;
          }
      }
    */
   if(bord)
    { cout<<"extension du tableau activé"<<endl;
     tabJeuExt=new int*[2*n];
     for (int i=0;i<2*n;i++)
     {
       tabJeuExt[i]=new int[2*m];
     }
     int mdiv2=m/2;
     int ndiv2=n/2;    
     //ON COPIE LE TABLEAU DE BASE VER LE NOUVEAU TABLEAU AGRANDIE
     for (int i=0;i<n;i++)
      {for (int j=0;j<m;j++)
        {
         if (tabJeu[i][j]==1)
          {
           tabJeuExt[i+ndiv2][j+mdiv2]=1;
          }
        }
      }
     delete [] tabJeu;
     this->m*=2;
     this->n*=2;
     tabJeu=tabJeuExt;     
    }
    system("clear"); 
    for(int i=0;i<n;i++)
    {
      for (int j=0;j<m;j++)
      {
       afficheTab2(j,i);
      }
     cout<<endl;
    }
    
    while(true)
   {
     //system("sleep 1");
    cout<<"rentrer 1 pour continuer, 2 pour sauvegarder, 3 pour saisir des cellules, 4 pour effacer :"<<endl;
    cin>>contin;
     
     if(contin==1)
     {
       system("clear"); 
       for(int i=0;i<n;i++) 
        {for (int j=0;j<m;j++)
         {
           modifCase(j,i);
         }
        }

       for(int i=0;i<n;i++) 
        {for (int j=0;j<m;j++)
         {
           modifCase2(j,i);
         }
        }
       
       for(int i=0;i<n;i++)
       {
        for (int j=0;j<m;j++)
        {
         afficheTab2(j,i);
        }
        cout<<endl;
       }
      }
      
     else if (contin==2) //proces de sauvegarde
     { int i,j;
       i=j=0;
       cout<<"sauvegarde en cour"<<endl;
       ofstream fichierEcriture;
       fichierEcriture.open("fichierSauvegarde.txt");
       
       while(i<n)
       { 
         while(j<m)
         {
           if (tabJeu[i][j]==1)
           {
            fichierEcriture<<"1|";
            j++;
           }
           else if(tabJeu[i][j]==0)
           {
             fichierEcriture<<"0|";
             j++;
           }
         }
         j=0;
         i++;
         fichierEcriture<<"#\n";
       }

       
       system("clear"); 
          for(int i=0;i<n;i++)
           {
            for (int j=0;j<m;j++)
             {
              afficheTab2(j,i);
             }
             cout<<endl;
            }
       cout<<"sauvegarde effectué"<<endl;
       fichierEcriture.close();
     }

     else if(contin==3) //proces de saisie
     {while(true)
        { 
         cout<<"rentrer x:"<<endl;
         cin>>x;
      
         cout<<"rentrer y:"<<endl;
         cin>>y;
         if(x>=0&&y>=0)
          iniCel(x,y);
      
         else    
         {system("clear"); 
          for(int i=0;i<n;i++)
           {
            for (int j=0;j<m;j++)
             {
              afficheTab2(j,i);
             }
             cout<<endl;
            }
          break;
         }
        }
      }

      else if(contin==4)
      { while(true)
        {cout<<"rentrer x à effacer :"<<endl;
         cin>>x;
         cout<<"rentrer y à effacer :"<<endl;
         cin>>y;

         if(x>=0&&y>=0)
          tabJeu[y][x]=0;
         else    
         { system("clear"); 
          for(int i=0;i<n;i++)
           {
            for (int j=0;j<m;j++)
             {
              afficheTab2(j,i);
             }
             cout<<endl;
           }
          break;
         }
        } 
      }




     else 
     break;
    }


    fichLecture.close();
  }






//SI ON A PAS DE FICHIER EN PARAMETRE
//SI ON A PAS DE FICHIER EN PARAMETRE
//SI ON A PAS DE FICHIER EN PARAMETRE
//SI ON A PAS DE FICHIER EN PARAMETRE
//SI ON A PAS DE FICHIER EN PARAMETRE
//SI ON A PAS DE FICHIER EN PARAMETRE
//SI ON A PAS DE FICHIER EN PARAMETRE

 else if(fichier==NULL)
  {
   cout<<"fichier egale null, quelle sera la taille des abscisse du tableau?"<<endl;
   cin>>this->m;
  
   cout<<"quelle sera la taille des ordonnes du tableau?"<<endl;
   cin>>this->n;

   tabJeu= new int*[n];

   for(int i=0;i<n;i++)
    {tabJeu[i]=new int[m];}
 
   cout<<"m="<<m<<" n="<<n<<endl;; 
 
   //initialisation de cellule
   cout<<"donner les coordonnees des cellules à initialiser puis entrer un numero < 0 pour terminer la saisie"<<endl;
   while(true)
   {
     cout<<"x=";
     cin>>x;
     cout<<"y=";
     cin>>y;
     if((x<m)&&(y<n)&&(x>=0)&&(y>=0))
     { 
       if(x==0||x==m-1||y==0||y==n-1)
         {bord=1;}
       iniCel(x,y);
      }
     else if ((x<0)||(y<0))
	   {
       cout<<"saise terminé"<<endl;
       break;
     }
    }
    
    
   //ON AGRANDIE LE TABLEAU tabJeu  EN FONCTION DE BORD;
  
   if(bord)
    {
     tabJeuExt=new int*[2*n];
     for (int i=0;i<2*n;i++)
     {
       tabJeuExt[i]=new int[2*m];
     }
     int mdiv2=m/2;
     int ndiv2=n/2;

     //ON COPIE LE TABLEAU DE BASE VER LE NOUVEAU TABLEAU AGRANDIE
     for (int i=0;i<n;i++)
      {for (int j=0;j<m;j++)
        {
         if (tabJeu[i][j]==1)
          {
           tabJeuExt[i+ndiv2][j+mdiv2]=1;
          }
        }
      }
     delete [] tabJeu;
     this->m*=2;
     this->n*=2;
     tabJeu=tabJeuExt; 
     cout<<"cellule au bord detecté, doublement de taille du tableau effectué"<<endl;
     system("sleep 2");    
    }
   system("clear"); 
   for(int i=0;i<n;i++)
    {
      for (int j=0;j<m;j++)
      {
       afficheTab2(j,i);
      }
     cout<<endl;
    }
   

   while(true)
   {//ON SAUVEGARDE SI CONTIN ==2
     //system("sleep 1");
     cout<<"rentrer 1 pour continuer, 2 pour sauvegarder, 3 pour saisir des cellules, 4 pour effacer :"<<endl;
     cin>>contin;
     
     if(contin==1)
     {
       system("clear"); 
       for(int i=0;i<n;i++) 
        {for (int j=0;j<m;j++)
         {
	         modifCase(j,i);
         }
        }

       for(int i=0;i<n;i++) 
        {for (int j=0;j<m;j++)
         {
           modifCase2(j,i);
         }
        }
       
       for(int i=0;i<n;i++)
       {
        for (int j=0;j<m;j++)
        {
         afficheTab2(j,i);
        }
        cout<<endl;
       }
      }

     else if (contin==2) //proces de sauvegarde
    { int i,j;
      i=j=0;
      cout<<"sauvegarde en cour"<<endl;
      ofstream fichierEcriture;
      fichierEcriture.open("fichierSauvegarde.txt");
      while(i<n)
      { while(j<m)
        {
          if (tabJeu[i][j]==1)
          {
            fichierEcriture<<"1|";
            j++;
          }
          else if(tabJeu[i][j]==0)
          {
             fichierEcriture<<"0|";
             j++;
          }
        }
        j=0;
        i++;
        fichierEcriture<<"#\n";
      }
      system("clear"); 
      for(int i=0;i<n;i++)
        {
         for (int j=0;j<m;j++)
          {
           afficheTab2(j,i);
          }
         cout<<endl;
        }
      cout<<"sauvegarde effectué"<<endl;
      fichierEcriture.close();
    }

    else if(contin==3) //proces de saisie
    {while(true)
      { 
        cout<<"rentrer x:"<<endl;
        cin>>x;
      
        cout<<"rentrer y:"<<endl;
        cin>>y;
        if(x>=0&&y>=0)
         iniCel(x,y);
      
        else    
        {system("clear"); 
         for(int i=0;i<n;i++)
          {
           for (int j=0;j<m;j++)
            {
             afficheTab2(j,i);
            }
            cout<<endl;
           }
         break;
        }
      }
    }

      else if(contin==4)
      { while(true)
        {cout<<"rentrer x à effacer :"<<endl;
         cin>>x;
         cout<<"rentrer y à effacer :"<<endl;
         cin>>y;

         if(x>=0&&y>=0)
          tabJeu[y][x]=0;
         else    
         { system("clear"); 
          for(int i=0;i<n;i++)
           {
            for (int j=0;j<m;j++)
             {
              afficheTab2(j,i);
             }
             cout<<endl;
           }
          break;
         }
        } 
      }
    }
  }  
}
//fin constructeur !!


int main(int argc, char** argv) 
{

  Cellule c(argv[1]);

  return 0;

}
