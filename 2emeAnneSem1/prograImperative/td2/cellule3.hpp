#include <iostream>

class Cellule{
	      public:
	      enum Couleur {
		NOIR,
                BLEU,
                VERT,
                ROUGE,
                JAUNE,
                NB_COULEURS
	      };
 

private:
  bool vivante;
  unsigned int x,y;
 
public:
  Cellule();
  Cellule(bool etat,unsigned int x, unsigned inty);

  bool estVivante() const;
  unsigned int getX() const;
  unsigned int getY() const;
  
  void Vivante(bool etat);
  void setX(unsigned int x);
  void setY(unsigned int y);
  
  //autre methode
  bool estVoisine(const Cellule &c) const;
};
