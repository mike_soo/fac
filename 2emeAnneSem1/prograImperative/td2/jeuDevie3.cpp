#include <iostream>

#include "cellule2.hpp"

using namespace std;

Cellule::Cellule(): vivante(false), x(0),y(0) {}

Cellule::Cellule(bool etat, unsigned int _x, unsigned _y):
  vivante(etat), x(_x), y(_y)
{}
bool Cellule::estVivante()const {
  return vivante;
}
unsigned int Cellule::getX()const {
    return x;
  }
unsigned int Cellule::getY()const{return y;}
  //accesseurs en écriture
  void Cellule::Vivante(bool etat)
  {vivante=etat;}

  void Cellule::setX(unsigned int x) 
  {this->x=x;}

  void Cellule::setY(unsigned int y)
  {this->y=y;}
 
bool Cellule::estVoisine(const Cellule &c) const {
  return c.vivante &&
    ((x - c.x) * (x-c.x) + (y- c.y)*(y-c.y)<=2);
}


void test_cell(const Cellule &c){
  cout << "La cellule (à l'adresse mémoire " << &c << ") = {"

       << (c.estVivante() ? "vivante" : "morte")
       << "," << c.getX() << "x" << c.getY() << "}"
       << endl;
}

#define PrintCell(c) \
  cout << "L'objet " #c " est à l'adresse mémoire " \
  << &c <<endl


#define PrintVoisines(c1,c2)  \
  cout << "La cellule " #c1 " "   \
  << (c1.estVoisine(c2) ? "est" : "n'est pas")   \
  << " voisine de " #c2 "." <<endl

int main(int argc, char** argv) {
  Cellule c1(true, 1, 2), c2(false, 1, 3);
  PrintCell(c1); PrintCell(c2);
  test_cell(c1); test_cell(c2);
  PrintVoisines(c1, c2); PrintVoisines(c2, c1);
  return 0;
}
