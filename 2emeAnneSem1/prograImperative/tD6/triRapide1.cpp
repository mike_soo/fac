#include <iostream>

using namespace std;

int TriRapide(int *tab,int deb,int fin)
{ int pvt =(deb+fin)/2;
  int inter;

 while(!((deb==pvt)&&(pvt==fin))) 
 { 
  cout<<endl;
  cout<<"pvt="<<pvt<<endl;
  cout<<"deb="<<deb<<endl;
  cout<<"fin="<<fin<<endl;
  while (deb<pvt)
  {
   if (tab[deb]<=tab[pvt])
    {
     deb++;
    }
   else break;
  }

  while(pvt<fin)
  {
   if (tab[pvt]<=tab[fin])
    {
     fin--;
    }
   else break;
  }
  
  inter=tab[deb];
  tab[deb]=tab[fin];
  tab[fin]=inter;
  

  if(deb==pvt)
   {pvt=fin;}
 
  else if(fin==pvt)
   {pvt=deb;}
  }
 return pvt;
}

void recuTriRapide(int *tab,int deb,int fin)
{ 
  
  if ((deb==fin)||(deb>fin))
   return;

  else 
  { int pvt=TriRapide(tab,deb,fin);
    recuTriRapide(tab,deb,pvt);
    recuTriRapide(tab,pvt+1,fin);
  }

}
int main()
{int tab[40]={9,1,2,5,4,2,8,7,3,0,0,2,5,2,3,1,2,5,2,1,4,5,5,7,8,9,9,6,6,2,1,1,2,3,3,6,5,2,1,2};
 recuTriRapide(tab,0,39);

 cout<<"tableau final:"<<endl;
 for(int i=0;i<39;i++)
 {
  cout<<" :"<<tab[i];
 
 }
  cout<<endl;
  return 0;
}