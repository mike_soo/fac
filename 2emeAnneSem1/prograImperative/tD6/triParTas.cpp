#include <iostream>
#include <math.h>
#include "tri.h"



int triParTas::sag(int v)
{int resInd=2*v+1;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resInd;
 
}

int triParTas::sad(int v)
{int resInd=2*v+2;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resInd;

}

int triParTas::pere(int v)
{
 int resInd=(v-1)/2;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resInd;
}

bool triParTas::estFeuille(int v)
{
	int resInd=(n-1)/2;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return v>=resInd;
}

int triParTas::profondeurA(int v)
{
	int resProf=log2(v+1);
  
 if(v<0||v>=n)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resProf;
}
void triParTas::entasser(int v) //v etant la racine d'un tableau ou tout est entasser sauf la racine
{ int vMax=v;
  
  int inter;
  if(v>=n)
  {
  	return ;
  }

  if (tab[sag(v)]>tab[vMax])
  {
  	vMax = sag(v);
  }
  if(tab[sad(v)]>tab[vMax])
  { 
  	vMax=sad(v);
  }
  if(v!=vMax)
  {
  	inter = tab[vMax];
  	tab[vMax]=tab[v];
  	tab[v]=inter;
  	entasser(vMax);
  }
}

bool triParTas::estTasse(int v)
{   
	if (estFeuille(v))
		return true;

	else if(tab[sag(v)]>tab[v]||tab[sad(v)]>tab[v])
		return false;
	
	else 
		return estTasse(sag(v))&&estTasse(sad(v));
}

/*void initTas(TableauCellule &T)
{
	for(size_t i=T.size()/2-1;i!=(size_t)-1;i--)
	{
		entasser(T,i,T.size());
	}
}*/ //corection du prof initTas

void triParTas::verificateurArbre(int v)
{
	if(estFeuille(v))
		return ;
	else 
	{
		cout<<"sag de "<<v<<" est "<<sag(v)<<endl;
		cout<<"sad de "<<v<<" est "<<sad(v)<<endl;
		verificateurArbre(sag(v));
		verificateurArbre(sad(v));

	}	
}

void triParTas::initTasVmike()
{
	while(!estTasse(0))
 {entasser(0);}
}

void triParTas::trier()
{   int nCop=n;
   int inter;
	while (n>1)
	{	
	initTasVmike();
	inter=tab[n-1];
	tab[n-1]=tab[0];
	tab[0]=inter;
	n--;
	}
 n=nCop;	
}

triParTas::triParTas(int *tabParm,int nParm)
{ this->n=nParm;
	this->tab=new int [n];
	for(int i=0;i<n;i++)
	{
	 tab[i]=tabParm[i];
	}
 
 trier();// verificateurArbre(0);
cout<<"le tableau est tassé?";estTasse(0)?cout<<"oui"<<endl : cout<<"non"<<endl;
 cout<<"j'affiche tableau finale"<<endl;
 for(int i=0;i<n;i++)
 	cout<<tab[i]<<" ";
 cout<<endl;

}


int main()
{int Tab[15]={2,9,8,7,6,8,5,2,1,4,2,3,6,5,2};
 triParTas tri(Tab,15);
 
 return 0;
}