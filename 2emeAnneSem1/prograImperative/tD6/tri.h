#ifndef TRI_H
#define TRI_H
#include <iostream>

using namespace std;

class triParTas
{ public:
	int *tab;
 	int n;
 	triParTas(int *tabParm,int nParm);
    void entasser(int v);
    int sag(int v);
    int sad(int v);
    int pere(int v);
    bool estFeuille(int v);
    int profondeurA(int v);
    void verificateurArbre(int v);
    bool estTasse(int v);
    void trier();
    void initTasVmike();
    ~triParTas()
    {
 	 delete [] tab;
    }
};

#endif