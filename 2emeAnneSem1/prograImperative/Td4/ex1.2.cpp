#include <iostream>
#include <fstream>

using namespace std;

int int main(int argc, char** argv){

if(argc != 2) {
	cerr<<"usage: "<<argv[0]<< " <Fichier>"<<endl;
	return 1;
}
ofstream fich(argv[1]);

if(!fich.is_open()) {
	cerr<<"le fichier"<<argv[1]<< " n'a pas pu etre ouvert."<<endl;
	return 2;
}
while (!cin.eof()){

	char c;
	cin.get(c);
	if(!cin.eof()){
		fich << c;
		cout << c;
	}
}
fich.close();
std::cout<<"bien terminé"<<std::endl;
return 0;

}

 


