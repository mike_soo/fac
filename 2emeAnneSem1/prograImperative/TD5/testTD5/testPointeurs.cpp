#include <iostream>

using namespace std;

void creeTab(int*& ptr)
{ 
 ptr=new int [5];
}

int main()
{
 int *p=new int[5];
 int *q;
 
 p[0]=2;
 p[1]=3;
 
 for(int i=0; i<5; i++) //affiche P
   {
     cout<<p[i];
   }
   cout<<endl;
   q=p;
  
 for(int i=0; i<5; i++) //affiche Q
     cout<<q[i];
 
     cout<<endl;

     p=new int[5];  //redeclare  *p = new..

 for(int i=0; i<5; i++) //on regarde si Q change
     cout<<q[i];
 
 cout<<endl;

 for(int i=0; i<5; i++) //on regarde si P change
   {
     cout<<p[i];
   }


//---------------------------------------------------------
 //int *p;
 /*creeTab(p);
 
 for(int i=0; i<5; i++)
    {
     cout<<p[i];
    }
  cout<<endl;*/   //marche
	return 0;
 
}