#ifndef tableauInt_H
#define tableauInt_H
#include <iostream>

using namespace std;


class TableauInt 
{

 public:
    int *Tab;
    int n; //taille du tableau
	
	TableauInt(int n);
    int &at(int i);
    void extend(int m);
};

void write(ostream& c,TableauInt T);

#endif
