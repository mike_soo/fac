#include "point.h"


class Polygone
{
 private:
 	int nbrCotes;
 	Point *tabPts;

 public:
 	Polygone(): nbrCotes(0), tabPts(NULL) { }
 	Polygone(int n) : nbrCotes(n), tabPts(new Point[n]){ }
 	void saisie() 
 	{
     if (tabPts==NULL)
     	{
     	 cin>>nbrCotes;
 		 tabPts= new Point[nbrCotes];
 		}
 	 
 	 for(int i =0; i<nbrCotes; i++)
 		{double x,y;
 		 cin>>x>>y;
 		 tabPts[i].setX(x);
 		 tabPts[i].setY(y);
 		}
 	}
 	
 	Polygone(const Polygone &p) : nbrCotes(p.nbrCotes), tabPts(new Point[p.nbrCotes])
 	{
 	 for(size_t i = 0 ; i<nbrCotes ; i++)
 	 	tabPts[i]=p.tabPts[i];
 	}


 	~Polygone()
 	{
 	 if(tabPts!= NULL)
 	 	delete[] tabPts;
 	}
};