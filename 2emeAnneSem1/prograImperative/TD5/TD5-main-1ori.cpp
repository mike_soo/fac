#include <iostream>
#include <cstdlib>
#include "tableau-int.h"

using namespace std;

TableauInt::TableauInt(int n)
{
 Tab=new int (n);
 this->n=n;
}

int &TableauInt::at(int i)
{ 
 return Tab[i];
}

void write(ostream& c ,TableauInt T)
{
  for (int i=0;i<T.n;i++)
  {
  	c<<T.Tab[i]<<" : ";
  }
  cout<<endl;
}

void TableauInt::extend(int m)
{int i=0;
 int *Text=new int [2*this->n]; //creation du tableau 2x plus grand que le tableau de base
                               
 for (i;i<this->n;i++)  //copie du tableau de base vers le nouveau tableau
  { 
    Text[i]=this->Tab[i]; 
  }
  Text[i]=m;
  this->n*=2;
  
  delete [] this->Tab; //on efface le tableau de base pointé par Tab
  
  Tab=Text; //Tab pointe vers le nouveau tableau
 
}
  

int main(int argc, char** argv)
{ int rep=1; //reponse
  int m=0;
  if (argc !=2) {cerr<<"Usage: "<<argv[0]<<" [tab dim]"<<endl; return 1;}

  size_t n = atoi(argv[1]);
  TableauInt T(n);
  for(size_t i=0;i<n;i++){
    T.at(i)=i+1;
  }
  write(cout,T);
  
  
  
  while (rep)
  {
   cout<<" voulez vous ajouter un entier a la fin du tableau et le tableau est remplie? 1 : oui -- 0 : non"<<endl;
   cin>>rep;
   if (rep)
   { cout<<"qu'elle est l'entier a ajouter?"<<endl;
     cin>>m;
     T.extend(m);
     cout<<"le nouveau tableau ressemble a : "<<endl;
     for (int i=0;i<T.n;i++)
     {
       cout<<T.Tab[i];
     }
   }
   
   else 
   {
     cout<<"taille maintenue"<<endl;
   } 
  }
  
 return 0;
}

