#include <iostream>

class Cellule{
private:
  bool vivante;
  unsigned int x,y;
 
public:
  Cellule();
  Cellule(bool etat,unsigned int x, unsigned int y);
  
  bool estVivante() const;
  unsigned int getX() const;
  unsigned int getY() const;
  void iniCel();
  void Vivante(bool etat);
  void setX(unsigned int x);
  void setY(unsigned int y);
  /*void vCpt(int x,int y,Cellule **tab); //voisine compteur: 
                           renvois le nombre                                                               de voisine de la                                                                cellule (x,y)*/
  //autre methode
  bool estVoisine(const Cellule &c) const;
};
