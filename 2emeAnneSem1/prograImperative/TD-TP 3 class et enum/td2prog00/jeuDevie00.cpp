#include <iostream>

#include "cellule2.hpp"

using namespace std;

Cellule::Cellule(): vivante(false), x(0),y(0) {}

Cellule::Cellule(bool etat, unsigned int _x, unsigned _y):
  vivante(etat), x(_x), y(_y)
{}

bool Cellule::estVivante()const 
{
  return vivante;
}

void Cellule::iniCel()
{this->vivante=true;}

unsigned int Cellule::getX()const 
{
    return x;
}

unsigned int Cellule::getY()const{return y;}
  //accesseurs en écriture

void Cellule::Vivante(bool etat)
  {vivante=etat;}

  void Cellule::setX(unsigned int x) 
  {this->x=x;}

  void Cellule::setY(unsigned int y)
  {this->y=y;}
 
/*
bool Cellule::estVoisine(const Cellule &c) const {
  return c.vivante &&
    ((x - c.x) * (x-c.x) + (y- c.y)*(y-c.y)<=2);
}
*/

void test_cell(const Cellule &c){
  cout << "La cellule (à l'adresse mémoire " << &c << ") = {"

       << (c.estVivante() ? "vivante" : "morte")
       << "," << c.getX() << "x" << c.getY() << "}"
       << endl;
}

/*
#define PrintCell(c)				    \
  cout << "L'objet " #c " est à l'adresse mémoire " \
  << &c <<endl
*/

/*
#define PrintVoisines(c1,c2)  \
  cout << "La cellule " #c1 " "   \
  << (c1.estVoisine(c2) ? "est" : "n'est pas")   \
  << " voisine de " #c2 "." <<endl
*/

void afficheTab2(Cellule **tab,int m,int n)
 {
  for (int i=0;i<n;i++)
    {for (int j=0;j<m;j++)
	{ if((i==5)&&(j==5))
	    {cout<<"2 ";}
          else if (tab[i][j].estVivante())
           cout<<"1 ";
          
          else 
           cout<<"0 ";
        }
      cout<<endl;
    }
 }

/*
void Cellule::vCpt(int x, int y, Cellule **tab)
{
  int cpt=0;
  int i=y-1; //ordonné
  int j=x-1; //abscisse

  for(i;i<=i+2;i++)
    {for (j;j<=j+2;j++) 
	{
	  if((i>=0)&&(j>=0))
            {((tab[i][j].estVivante())&&(i!=y)&&(j!=x)) ? cpt++:cpt+=0;}
        }
    }
  cout<<"la cellule ("<<x<<','<<y<<','<<") posséde "<<cpt<<" voisin"<<endl;

}
*/
int main(int argc, char** argv) 
{

  Cellule **tabJeu;
  char a='o';
  int m,n,x,y;
  x=0; y=0;
  cout<<"quelle sera la taille des abscisse du tableau?"<<endl;
  cin>>m;
  
  cout<<"quelle sera la taille des ordonnes du tableau?"<<endl;
  cin>>n;
  
  tabJeu= new Cellule*[n];

  for(int i=0;i<n;i++)
    tabJeu[i]=new Cellule[m];

  /*while(true)
    {cout<<"donner les coordonnees des cellules à initialiser"<<endl;
      cout<<"x=";
      cin>>x;
      cout<<"y=";
      cin>>y;
      if((x!=99)&&(y!=99))
        { tabJeu[x][y].iniCel();
          break;
        }
     } */

  tabJeu[4][5].iniCel();
  tabJeu[5][4].iniCel();
  tabJeu[6][5].iniCel();
 

  afficheTab2(tabJeu,m,n);
  
  x=5;
  y=5;
 int cpt=0;
  int i=y-1; //ordonné
  int j=x-1; //abscisse
  int i2=i+2;
  int j2=j+2;
  
  while(i<=i2)
    {
      while (j<=j2) 
	{
	  if(tabJeu[i][j].estVivante())
            {
              
	      cout<<"coucou"<<endl;
             
		cout<<"la cellule ("<<i<<','<<j<<")"<<"est = "<<tabJeu[i][j].estVivante()<<endl;
            cpt++;
             
            }
          j++;
        }
      j-=3;i++;
      cout<<endl;
    }
  cout<<"la cellule ("<<x<<','<<y<<") posséde "<<cpt<<" voisin"<<endl;




  return 0;
}
