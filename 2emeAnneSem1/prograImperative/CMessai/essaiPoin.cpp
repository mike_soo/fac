#include <iostream>

void pointeur (int *a, int *b)
{
  std::cout<<"*a="<<*a<<std::endl;
}

int main()
{int a=9; 
  int b=5;
  int *q;
  int *p=&a;
  q=&b;
  pointeur(&b,&a);
  return 0;
}
