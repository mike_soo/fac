#ifndef CURSOR_H
#define CURSOR_H
#include <iostream>
#include <string>


class Cursor{
 public:
  Cursor(); // créé un curseur à la position (0,0)
  Cursor(size_t x, size_t y); // créé un curseur à la position (x,y) 
  void left(size_t i) ; // recule le curseur de i position
  void right(size_t i); // avance le curseur de i position
  void up(size_t i)   ; // monte le curseur de i position
  void down(size_t i) ; // descend le curseur de i position
  void move_to(size_t x, size_t y); // bouge le curseur à la position (x,y)
  void to_origin(); // remet le curseur à son origine
  void write(std::string s); // écrit la chaine s à la position courante du curseur
  void clear();              // Efface l'écran et remet le curseur à son origine
  void endl();               // fait un retour chariot à la position courante du curseur

 private:
  size_t _x,_y;
  const size_t _xo,_yo;
};

#endif