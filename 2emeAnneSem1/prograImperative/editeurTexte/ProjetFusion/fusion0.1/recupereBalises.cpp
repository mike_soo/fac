#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <string.h>
#include "recupereB.hpp"
#include "boite.h"
#include "cursor.h"

using namespace std;

void recupFichier::getTaille()
{
 ifstream fichLecture;
 fichLecture.open(fichier,ios::in|ios::binary);
 fichLecture.seekg(0,ios::end);
 fTaille=fichLecture.tellg();
 fichLecture.seekg(0,ios::beg);

 ecriTexteB<<"tailleFichier="<<fTaille<<'\n';
 fichLecture.close();

}

void recupFichier::chargeFichier()
{ cptBal=0;
  ifstream fichLecture;
  unsigned char c;
  fichLecture.open(fichier,ios::in|ios::binary);
  if(fichLecture.is_open())
  {
   tabFichierSN=new unsigned char[fTaille];
   size_t i=0;
   while(i<fTaille)
   {
    c=fichLecture.get();
    if(i>=0)
      {
       if(c=='<')
        {
          cptBal++;
        }
      }
     tabFichierSN[i]=c;
     i++;
   }
  }
  ecriTexteB<<tabFichierSN;
 fichLecture.close(); 
}



void recupFichier::chargeDico()
{ unsigned char c;
  size_t j=0;
  size_t i=0;
  while(i<24)
  {
    while(j<9)
    { 
      tabDico[i][j]='\0';
      j++;
    }
        
   j=0;
   i++;
  }
  ;
  ifstream fDico;
  fDico.open("fDico.txt",ios::in|ios::binary);
  if(fDico.is_open())
    { c=fDico.get();
      i=j=0;

       while(c!='#')
      {
        while((c!='\n')&&(c!='#'))
        {
         tabDico[i][j]=c;
         j++;
           
         c=fDico.get();
        }
      
       j=0;
       i++;
       if(c!='#')
        c=fDico.get();  
      }
  
      ecriTexteB<<"i="<<i<<"j="<<j<<'\n';
      ecriTexteB<<"ca marche"<<'\n';
      ecriTexteB<<"tableauF"<<'\n';
      i=j=0;
      while(i<24)
      {
        while(j<9)
        { if(tabDico[i][j]=='\0')
            {
             ecriTexteB<<'0';
            }
          else 
          ecriTexteB<<tabDico[i][j];
          j++;
          
        }
        ecriTexteB<<'\n';
        j=0;
        i++;
      }
      ecriTexteB<<'\n';
    }
  else
     {ecriTexteB<<"erreur a l'ouverture de fichier fDico"<<'\n';}
 
 fDico.close(); 
}



void recupFichier::fNettoie() //charge le fichier passé en parametre dans la ram 
{unsigned char c;
 size_t nbEspace=0;
 size_t nbEntre=0;
 size_t i=0;
 ifstream fichLecture;
 fichLecture.open(fichier,ios::in|ios::binary);
 size_t posCurseur=0;
 unsigned iSN=0;

 if(fichLecture.is_open())
 	{   	 
 	 tabFichier = new unsigned char[fTaille];   //ON NETOIE TOUT LES ESPACES ET RETOUR A LA LIGNE EN PLUS
 	 ecriTexteB<<"---------------------------"<<'\n';
 	 ecriTexteB<<"c=";
 	 //fichLecture.read(tabFichier,fTaille);
 	 c=tabFichierSN[iSN];
   iSN++;
 	 ecriTexteB<<c;
 	 while(posCurseur<fTaille)
 	    {
 	     if((c==' ')||(c=='\n')||(c=='\t')) //SI ON TROUVE UN ESPACE OU ENTRER OU TAB
 	        {
 	        
		     if((nbEspace==0)&&(c==' '))
		    {
		     tabFichier[i]=' ';
		     i++;
		     nbEspace++;
		    }	 
		    else if((nbEntre==1)&&(c=='\n')) 
		    {
		     tabFichier[i-1]='\n';
		     nbEntre++;
		     //ecriTexteB<<"je boucle en entre ecrit";
		    } 
		    else if((nbEntre==0)&&(c=='\n')&&(nbEspace==0))
		    {
		     tabFichier[i]=' ';
		     nbEspace++;
		     nbEntre++;
		     i++;
		     //ecriTexteB<<"j'ecrit espace";
		    }  
		    else if((nbEntre==0)&&(c=='\n'))
		    {//ecriTexteB<<"je boucle en entre";
		     nbEspace++;
		    }
		    else if((c=='\t')&&(nbEspace==0))
		    {
		     tabFichier[i]=' ';
		     i++;
		     nbEspace++;
		    }
		    else if(nbEntre>1)
		    {
		    	nbEntre++;
		    }

		   }
		 
		 else 
		  {
		  	tabFichier[i]=c;
		  	nbEntre=0;
		  	nbEspace=0;
		  	i++;
          } 
		 iSN++;
		 c=tabFichierSN[iSN];
		 ecriTexteB<<c;
		 posCurseur++;

		}
		ecriTexteB<<'\n';
 	}
 else 
  {
   ecriTexteB<<"erreur d'ouverture de chargement du fichier"<<'\n';}
   ecriTexteB<<'\n';
   //ecriTexteB<<"tabFichier"<<'\n';
   //ecriTexteB<<tabFichier<<'\n';
  ecriTexteB<<"<===============tableau tabFichier================>"<<'\n';	
  printf("%s\n",tabFichier);
  fichLecture.close();
  delete [] tabFichierSN;
  }





void recupFichier::verifieBalises()
{ size_t i=0;
  size_t erreurB=0;
  size_t nbEspace=0;
  size_t nbEntre=0;
  int coulCod=0;
  size_t indDi=0; //INDICE DU TABLEAU DICO
  size_t indDj=0; //INDICE DU TABLEAU DICO
  int match=0;  //MATCH DES BALISES OUVRANTE
  int matchf=0; //MATCH DES BALISES FERMANTE
  size_t cptLigne=1; //compte les lignes du fichier original en fonction du caractere entre
  //compte le nombre de balises ouvrante9
  size_t indPi=0;
  size_t indPj=0;
  int rappelfgbg;
  int indParPilOu=0;
  int indParPilFe=0;
  int asciV;
  int balMinOuv=0;
  int detecPreChar=0;

  string sortieE="";
  
  Boite B;
  B.init();

  pile=new int*[cptBal];
  ecriTexteB<<"cptBal= "<<cptBal<<'\n';
  for(indPi=0;indPi<cptBal;indPi++)
  { 
    pile[indPi]=new int[4];
  }

  indPi=0;
  for(indPi=0;indPi<cptBal;indPi++)//ON INITIALISE LE TABLEAU DE PILE AVEC QUE DES -1;
  {
    for(indPj=0;indPj<4;indPj++)
    { 
      pile[indPi][indPj]=-1;
    }
  }

  ecriTexteB<<"piledeb:"<<'\n';
       //j'affice la pile
       ecriTexteB<<"indPi="<<indPi<<'\n';
       for(size_t ip=0;ip<cptBal;ip++)
       {for(size_t jp=0;jp<4;jp++)
        {
         ecriTexteB<<pile[ip][jp]<<" ";
        }
        ecriTexteB<<'\n';
       }
 indPi=0;
 indPj=0;  

  ecriTexteB<<"fTaille="<<fTaille<<'\n';
 while(i<fTaille)
  { 
     

   if(tabFichierSN[i]=='<') //CAS ON TROUVE UNE BALISE
 	  {    
 	   i++;
 	   if(tabFichierSN[i]=='/')//CAS OU ON TROUVE UNE BALISE FERMANTE
 	    { ecriTexteB<<"pilef:"<<'\n';
        ecriTexteB<<"indPi="<<indPi<<'\n';
       //j'affiche la pile
       for(size_t ip=0;ip<cptBal;ip++)
       {for(size_t jp=0;jp<4;jp++)
        {
         ecriTexteB<<pile[ip][jp]<<" ";
        }
        ecriTexteB<<'\n';
       }
        i++;
       ecriTexteB<<"balise fermante<-------------------------------------------"<<'\n';
       

       while((indDi<23)&&(matchf==0))//ON ARRETE SI ON MATCH OU SI ON DEPASSE LA TAILLE DU DICO
       {
        ecriTexteB<<" indDj="<<indDj<<" indDi="<<indDi<<" i="<<i<<" tabFichier= "<<tabFichierSN[i]<<" tabDico= "<<tabDico[indDi][indDj]<<'\n';
         //ecriTexteB<<" indDj="<<indDj<<" indDi="<<indDi<<" i="<<i<<" tabFichier= "<<tabFichierSN[i]<<" tabDico= "<<tabDico[indDi][indDj]<<'\n';
         
         //ON ARETE SI ON EST A LA FIN DUNE
         //LIGNE DANS LE DICO ON CHERCHE LIGNE PAR LIGNE                                            
         //PUIS ON CHERCHE A LA LIGNE SUIVANTE
                     
         if(((tabDico[indDi][indDj]=='\0')&&(tabFichierSN[i]=='>'))||((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]=='>'))) //CAS DE MATCH AVEC CHEVRONS
         {
           ecriTexteB<<"matchf"<<'\n';
           matchf=1;
           pile[indPi][0]=indDi; pile[indPi][3]=cptLigne;

              //j'affice la pile
           ecriTexteB<<"pile"<<'\n';
           
           for(size_t ip=0;ip<cptBal;ip++)
            {for(size_t jp=0;jp<4;jp++)
              {
               ecriTexteB<<pile[ip][jp]<<" ";
              }
              ecriTexteB<<'\n';
            }
        
             //ON VERIFIE LA PILE DE TEL SORTE QUE SI (pile[indPi-1]==pile[indPi]) LA BALISE
             //CORRESPOND BIEN A LA BALISES OUVRANTE TROUVÉE JUST AVANT LA BALISE FERMANTE ACUTELLEMENT
             //TRAITÉ
            
            ecriTexteB<<"indPi="<<indPi<<'\n';
           if(indPi==0)
            {
              sortieE+="fatal erreur : premiere balises est une balises fermante, abandon total\n";
              i=fTaille;
              erreurB=1;
              break;
            }
           

           else if(pile[indPi-1][0]==pile[indPi][0])
            {//ICI JE SUIS SENSÉ ECRIRE TOUT LE TEXTE BRUTE CUMULE DANS UN STRING EN FUNCTION DE PILE
             
             
             texteB+="\e[0m";

             //ON AFFICE EN FONCTION DE LA BALISES D'AFFICHAGE TROUVÉ

              if (pile[indPi][0]==8)
              { cout<<texteB;
                B.afficheG(texteB);
                texteB="";
              }
              
              else if (pile[indPi][0]==9)
              {cout<<texteB;
                B.afficheD(texteB);
                texteB="";
              }

              else if (pile[indPi][0]==10)
              {cout<<texteB;
                B.afficheC(texteB);
                texteB="";
              }
              //cout<<texteB;
             //ecriTexteB<<texteB;
             //B.afficheG(texteB);
              //ecriTexteB<<"Col="<<B.getcol()<<"\n";
              //ecriTexteB<<"Lig="<<B.getlig()<<"\n";
             //texteB="";
             
             indParPilFe=0;

             ecriTexteB<<"balises ouvrante coincide avec fermante"<<'\n';
         
             pile[indPi-1][0]=-1; pile[indPi-1][1]=-1; pile[indPi-1][2]=-1; pile[indPi-1][3]=-1; 

             pile[indPi][0]=-1; pile[indPi][1]=-1; pile[indPi][2]=-1; pile[indPi][3]=-1; 
         
             indPi--;
             indParPilOu--;
             i++;

             while(indParPilFe<indParPilOu) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
              {
               if(pile[indParPilFe][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
               { 
                 if(pile[indParPilFe][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
                  {
                   texteB+="\e[3"+to_string(pile[indParPilFe][1])+"m";
                   ecriTexteB<<"dans </>"<<pile[indParPilFe][1]<<'\n';
                   ecriTexteB<<"indPilFe="<<indParPilFe<<'\n';
                  }  
                
                 if(pile[indParPilFe][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
                  {
                   texteB+="\e[4"+to_string(pile[indParPilFe][2])+"m";
                   ecriTexteB<<"dans </>"<<pile[indParPilFe][2]<<'\n';
                  }
                   indParPilFe++;
                }


               else if(pile[indParPilFe][0]==16)
               {
                 texteB+="\e[3m";
                 indParPilFe++;
               }

             
               else if(pile[indParPilFe][0]==17)
               {
                 texteB+="\e[4m";
                 indParPilFe++;
               }
             

               else if(pile[indParPilFe][0]==15)
               {
                 texteB+="\e[1m";
                 indParPilFe++;                
               } 
               
               else
               {
                indParPilFe++;
               }
             } 

             break;
            }
           
           else 
            { erreurB=1;
              ecriTexteB<<"Balise fermante qui ne conside pas avec une balise ouvrante correctement a la ligne: " \
                  <<cptLigne<<'\n';
              sortieE+="Balise fermante qui ne conside pas avec une balise ouvrante correctement a la ligne: " \
                   +to_string(cptLigne)+'\n';
              i=fTaille;
              break;
            }
          }
           
         else if(tabDico[indDi][indDj]==tabFichierSN[i]) //CAS OU ON COMPARE CHAQUE LETRE DU DICO AVEC LE FICHIER DE TEXTE A BALISE
         { 
           indDj++;
           i++;
         }


         else if(tabDico[indDi][indDj]!=tabFichierSN[i])
         { i-=indDj;
           indDi++;
           indDj=0;
         } 
        }

       if(matchf==0)//CAS OU IL Y A PAS MATCHf AVIS D'EUREUR !
        { 
         ecriTexteB<<"Erreur a la "<<cptLigne<<"eme ligne du fichier "<<fichier<<'\n';
         sortieE+="Erreur a la "+to_string(cptLigne);
         sortieE+=" eme ligne du fichier ";
         erreurB=1;
         i=fTaille+1;
         break;
        }

 	    }
     
     else if(tabFichierSN[i]!='/') //CAS OU ON TROUVE UNE BALISE OUVRANTE
 	    { ecriTexteB<<"BOpile:"<<'\n';
        //j'affice la pile
       for(size_t ip=0;ip<cptBal;ip++)
       {for(size_t jp=0;jp<4;jp++)
        {
         ecriTexteB<<pile[ip][jp]<<" ";
        }
        ecriTexteB<<'\n';
       }

        ecriTexteB<<"balise ouvrante<-------------------------------------------"<<'\n';
 	     while((indDi<23)&&(match==0))//ON ARRETE SI ON MATCH OU SI ON DEPASSE LA TAILLE DU DICO !
 	     {
 	       
         //ON ARETE SI ON ATEsize_t LA FIN DUNE
 	       //LIGNE DANS LE DICO ON CHERCHE LIGNE PAR LIGNE										                        
 	       //PUIS ON CHERCHE A LA LIGNE SUIVANTE
         	         
         if((tabDico[indDi][indDj]=='\0')&&(tabFichierSN[i]=='>')) //CAS DE MATCH AVEC CHEVRONS
 	       {
           ecriTexteB<<"Match"<<'\n';
 	 	     	 match=1;
           pile[indPi][0]=indDi; 
           pile[indPi][3]=cptLigne;
           indPi++;
           i++;

          // while(pile[indParPilOu][0]!=-1) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
          // {
             if(pile[indParPilOu][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
             { if(pile[indParPilOu][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
               {
                texteB+="\e[3"+to_string(pile[indParPilOu][1])+"m";
                ecriTexteB<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<'\n';
                ecriTexteB<<texteB<<'\n';
               }
 
               if(pile[indParPilOu][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
               {
                texteB+="\e[4"+to_string(pile[indParPilOu][2])+"m";
                ecriTexteB<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<'\n';
               }
               ecriTexteB<<texteB<<'\n';
               indParPilOu++;
             //else if()
             }


             else if(pile[indParPilOu][0]==16)
              {
               texteB+="\e[3m";
               indParPilOu++;

              }

             
             else if(pile[indParPilOu][0]==17)
              {
               texteB+="\e[4m";
               indParPilOu++;
               //ecriTexteB<<texteB<<'\n';
              }
             

             else if(pile[indParPilOu][0]==15)
             {
              texteB+="\e[1m";
              indParPilOu++;                
             } 

            else if(pile[indParPilOu][0]==22)  // DANS LE CAS OU ON TROUVE <p>
              {
                texteB+="\n\n";
                indParPilOu++;
              }

             else
             {
              indParPilOu++;
             }
             ecriTexteB<<"indParPilOu="<<indParPilOu<<'\n';
          //} 
           //indParPilOu=0;
 	       }
    	  	 
         else if(tabDico[indDi][indDj]==tabFichierSN[i]) //CAS OU ON COMPARE CHAQUE LETRE DU DICO AVEC LE FICHIER DE TEXTE A BALISE
  	     { ecriTexteB<<" indDj="<<indDj<<" indDi="<<indDi<<" i="<<i<<" tabFichier= "<<tabFichierSN[i]<<" tabDico= "<<tabDico[indDi][indDj]<<'\n';

 	    	   indDj++;
 	    	   i++;
           

           if((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]==' ')) //CAS A LA POSITION '_' où <color_fg=..> 
            {
             indDi=0;
             indDj=0;
             i++;
            }
           

           else if((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]=='>')) //CAS <color>
            {ecriTexteB<<"Warning: balises a la ligne "<<cptLigne<<" sans parametre"<<'\n';
             sortieE+="Warning: balises a la ligne "+to_string(cptLigne)+" sans parametre\n";
             i++;
             pile[indPi][0]=18; pile[indPi][3]=cptLigne;
             indPi++;
             indParPilOu++;
             match=1;
            }
           

           else if((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]!='>')) //CAS DE LA POSITION '_ ' OÙ <color fg=_..
            {rappelfgbg=indDi-19; //ON SE RAPPEL POUR LA SUITE 
             indDi=0;             //DE L'OPTION (FG OU BG) DANS LAQUELLE ON SE TROUVE
             indDj=0;             //POUR APRES QUAND ON CONNAITRA LA COULEUR, POUVOIR L'ATRIBUÉ AU BON PARAM FG OU BG
             ecriTexteB<<"rappel dans 1er param= "<<rappelfgbg<<'\n';
            }

           
           else if((tabDico[indDi][indDj]=='-')&&(tabFichierSN[i]==' ')) //<color fg=bleu_bg=red  où _ est le curseur sur ' '
            { 
             
             coulCod=indDi;
             ecriTexteB<<"de couleurs "<<coulCod<<" rappelfgbg="<<rappelfgbg<<'\n';
             pile[indPi][0]=18; pile[indPi][rappelfgbg]=indDi; pile[indPi][3]=cptLigne;
             
             i++;
              indDi=0;
              indDj=0;  

            }

           else if((tabDico[indDi][indDj]=='-')&&(tabFichierSN[i]=='>')) //<color fg=bleu_   où _ est le curseur sur >
            { 
             match=1;
             coulCod=indDi;
             ecriTexteB<<"de couleur "<<coulCod<<'\n';
             pile[indPi][0]=18; pile[indPi][rappelfgbg]=indDi; pile[indPi][3]=cptLigne;
             indPi++;
             i++;
           //while(pile[indParPilOu][0]!=-1) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
           //{
             
             if(pile[indParPilOu][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
             { if(pile[indParPilOu][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
                {
                 texteB+="\e[3"+to_string(pile[indParPilOu][1])+"m";
                 ecriTexteB<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<'\n';
                 ecriTexteB<<texteB<<'\n';
                }
 
               if(pile[indParPilOu][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
               {
                 texteB+="\e[4"+to_string(pile[indParPilOu][2])+"m";
                 ecriTexteB<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<'\n';
                }
                ecriTexteB<<texteB<<'\n';
               indParPilOu++;
             }
             else
              {indParPilOu++;}
            } 
           
           else if((tabDico[indDi][indDj]=='\0')&&(tabFichierSN[i]==' ')) //CAS OU BALISES OUVRANTE SANS CHEVRON FERMANT <p
            { balMinOuv++;
              match=1;
              pile[indPi][0]=indDi; 
              pile[indPi][3]=cptLigne;
              indPi++;  
              i++;
              if(pile[indParPilOu][0]==18)
              { if(pile[indParPilOu][1]!=-1) 
                {
                 texteB+="\e[3"+to_string(pile[indParPilOu][1])+"m";
                 ecriTexteB<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<'\n';
                 ecriTexteB<<texteB<<'\n';
                }
 
                if(pile[indParPilOu][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
                {
                 texteB+="\e[4"+to_string(pile[indParPilOu][2])+"m";
                 ecriTexteB<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<'\n';
                }
               ecriTexteB<<texteB<<'\n';
               indParPilOu++;
                //else if()
              }

             else if((!texteB.empty())&&(pile[indParPilOu][0]==8 || pile[indParPilOu][0]==9 || pile[indParPilOu][0]==10 || pile[indParPilOu][0]==11)) 
             {
               //cout<<texteB;
                texteB+="\e[0m";
                B.afficheG(texteB);
                ecriTexteB<<"Par d'affaut affichage de texteB:"<<'\n'<<texteB;

                texteB="";
               

               

               for(int iAff=0;iAff<indParPilOu;iAff++)
               {
                 if(pile[iAff][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
                 { 
                   if(pile[iAff][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
                   {
                     texteB+="\e[3"+to_string(pile[iAff][1])+"m";
                     ecriTexteB<<"dans </>"<<pile[iAff][1]<<'\n';
                     ecriTexteB<<"indPilFe="<<iAff<<'\n';
                   }  
                
                   if(pile[iAff][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
                   {
                     texteB+="\e[4"+to_string(pile[iAff][2])+"m";
                     ecriTexteB<<"dans </>"<<pile[iAff][2]<<'\n';
                    }
                   }
    
                 else if(pile[iAff][0]==16)
                 {
                   texteB+="\e[3m";
                 }
 
                 else if(pile[iAff][0]==17)
                 {
                  texteB+="\e[4m";
                 }
             

                 else if(pile[iAff][0]==15)
                 {
                   texteB+="\e[1m";
                 } 
               }
              indParPilOu++;
              }
            

             else if(pile[indParPilOu][0]==16)
              {
               texteB+="\e[3m";
               indParPilOu++;
              }
             

             else if(pile[indParPilOu][0]==17)
              {
               texteB+="\e[4m";
               indParPilOu++;
               //ecriTexteB<<texteB<<'\n';
              }
             
              else if(pile[indParPilOu][0]==22)  // DANS LE CAS OU ON TROUVE <p>
              {
                texteB+="\n\n";
                indParPilOu++;
              }


             else if(pile[indParPilOu][0]==15)
             {
               texteB+="\e[1m";
               indParPilOu++;                
             } 


             else
              {
               indParPilOu++;
              }
            }
         }


 	       else if(tabDico[indDi][indDj]!=tabFichierSN[i])
 	    	 { i-=indDj;
 	    	 	 indDi++;
           indDj=0;
 	       } 
   	   }
       if(match==0)//CAS OU IL Y A PAS MATCH AVIS D'EUREUR !
     	  { 
     	   ecriTexteB<<"Erreur a la "<<cptLigne<<"eme ligne du fichier "<<fichier<<'\n';
         erreurB=1;
         while(tabFichierSN[i]!='>')
         {
          i++;
          if(i>=fTaille||tabFichierSN[i]=='<')
          { i=fTaille;
            ecriTexteB<<"A la ligne"<<cptLigne<<" Fatal error balise ouvrante no fermé====================!=!=!=!=!=!=!=!=!=!=!=!!!"<<'\n';
          }
          break;
         }
         i++;
     	  }
 	    }
     indDj=0;
     indDi=0;
     match=0;
     matchf=0;
 	  }	//FIN DE CAS QUAND ON TROUVE UNE BALISE,

    else if((tabFichierSN[i]=='/')&&(i<fTaille-1)&&(tabFichierSN[i+1]=='>')) //ON SE TROUVE DANS LE CAS OU _>. EST LE CURSEUR PLACÉ SUR /
    {  
     i++;
     if(balMinOuv<=0)
      {
       sortieE+="mini balise fermante presente sans mini balise ouvrante a la ligne :"+to_string(cptLigne)+'\n';
       erreurB=1;
       break;
      }
       
     else if(pile[indPi-1][0]==18)
      {
       sortieE+="l'usage de  <color /> n'est pas accepté\n";
       erreurB=1;
       break;
      }

     else if(pile[indPi-1][0]!=18)
      {indParPilFe=0;
       ecriTexteB<<"pile apres Bal Mini fermante Trouveé donc apres /> et avant de modifié la pile"<<'\n';
       ecriTexteB<<"indPi="<<indPi<<'\n';
       for(size_t ip=0;ip<cptBal;ip++)
        {for(size_t jp=0;jp<4;jp++)
          {
           ecriTexteB<<pile[ip][jp]<<" ";
          }
         ecriTexteB<<'\n';
        }

       if (pile[indPi][0]==8)
       { cout<<texteB;
         B.afficheG(texteB);
         texteB="";
       }
       
       else if (pile[indPi][0]==9)
       { cout<<texteB;
         B.afficheD(texteB);
         texteB="";
       }

       else if (pile[indPi][0]==10)
       {cout<<texteB;
         B.afficheC(texteB);
         texteB="";
       }      


       ecriTexteB<<" mini balise ouvrante coincide avec fermante"<<'\n';
       pile[indPi-1][0]=-1; pile[indPi-1][1]=-1; pile[indPi-1][2]=-1; pile[indPi-1][3]=-1;
         
       indPi--;
       indParPilOu--;
       i++;
       balMinOuv--;

       texteB+="\e[0m";


       while(indParPilFe<indParPilOu) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
        { 
         if(pile[indParPilFe][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
         { 
           if(pile[indParPilFe][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
           {
             texteB+="\e[3"+to_string(pile[indParPilFe][1])+"m";
             ecriTexteB<<"dans </>"<<pile[indParPilFe][1]<<'\n';
             ecriTexteB<<"indPilFe="<<indParPilFe<<'\n';
           }  
               
           if(pile[indParPilFe][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
            {
             texteB+="\e[4"+to_string(pile[indParPilFe][2])+"m";
             ecriTexteB<<"dans </>"<<pile[indParPilFe][2]<<'\n';
            }
           indParPilFe++;
         }


         else if(pile[indParPilFe][0]==16)
          {
           texteB+="\e[3m";
           indParPilFe++;
          }

             
         else if(pile[indParPilFe][0]==17)
          {
           texteB+="\e[4m";
           indParPilFe++;
          }
             

         else if(pile[indParPilFe][0]==15)
          {
           texteB+="\e[1m";
           indParPilFe++;                
          } 

         /*else if(pile[indParPilFe][0]==22)
          {
            texteB+="\n\n";
            indParPilFe++;
          }  */  

         else
          {
           indParPilFe++;
          }
        } 
      }
    }


   /*ON SE TROUVE PAR LA SUITE DANS LE CAS OU ON TROUVE DU TEXTE BRUTE QU'ON VIENDRA SAUVEGARDÉ
     DANS UNE VARIABLE STRING EN FONCTION DES ESPACES QU'ON TROUVE OU PAS (PLUS D'UN ESPACE LUE A LA 
     SUITE L'UN DE L'AUTRE IGNORÉ, UN ENTRE = ESPACE ; DEUX ENTRÉ = ENTRÉ)*/
   

   else  //SI ON TOMBE SUR DU TEXTE BRUTE 
   { ecriTexteB<<"texte brutee"<<'\n';

    asciV=tabFichierSN[i];
     if((tabFichierSN[i]!=' ' || tabFichierSN[i]!='\n' || tabFichierSN[i]!='\b') && detecPreChar==0)
     {
      detecPreChar=1;
     }

     if(tabFichierSN[i]=='\n')
      {
       cptLigne++; 
      } 
     //texteB+=tabFichierSN;
     ecriTexteB<<'\n';
     
     if( detecPreChar==0 &&(tabFichierSN[i]==' ' || tabFichierSN[i]=='\n' || tabFichierSN[i]=='\b') && cptBal==0 )
      {
        i++;
      }        
      
     else if((tabFichierSN[i]==' ')||(tabFichierSN[i]=='\n')||(tabFichierSN[i]=='\t')) //SI ON TROUVE UN ESPACE OU ENTRER OU TAB
      {  
       if((nbEspace==0)&&(tabFichierSN[i]==' '))
        {
         ecriTexteB<<"espaceIn";
         ecriTexteB<<" nbEspace="<<nbEspace<<'\n';
         texteB+=' ';
         i++;
         nbEspace++;
        }  
      
        else if((nbEntre==1)&&(tabFichierSN[i]=='\n')&&(texteB.back()==' ')) 
        {
         //tabFichier[i-1]='\n';
         
          texteB.pop_back();
          texteB+='\n';
          nbEntre++;
          i++;
          nbEspace--;
         //ecriTexteB<<"je boucle en entre ecrit";
        } 
      
        else if((nbEntre==0)&&(tabFichierSN[i]=='\n')&&(nbEspace==0))
        {
         texteB+=' ';
         nbEspace++;
         nbEntre++;
         i++;
         //ecriTexteB<<"j'ecrit espace";
        }  
      
        else if((nbEntre==0)&&(tabFichierSN[i]=='\n')&&(nbEspace==0))
        {//ecriTexteB<<"je boucle en entre";
         texteB+=' ';
         nbEspace++;
         i++;
        }
      
        else if((tabFichierSN[i]=='\t')&&(nbEspace==0))
        {
         texteB+=' ';
         i++;
         nbEspace++;
        }
      
        else
        {
         i++;
        }

      }
     
     else 
      {
        texteB+=tabFichierSN[i];
        nbEntre=0;
        nbEspace=0;
        i++;
      } 
   }
   asciV=tabFichierSN[i];
   ecriTexteB<<" i="<<i<<" indDj="<<indDj<<" indDi="<<indDi<<"tabFichierbl="<<tabFichierSN[i]<<" tabF[i] en ascii="<<asciV<<'\n'<<"i+2="<<tabFichierSN[i+2]; 
   
  
  }
 // ecriTexteB<<"le texte brute donne donc ="<<texteB<<"<---"<<'\n';
 // texteB.pop_back();
 // ecriTexteB<<"le texte brute donne donc ="<<texteB<<"<---"<<'\n';
 
//GESTION D'AFFICHAGE DES ERREUR A PARTIR DE CETTE LIGNE
 if(!erreurB)
  { 
   ecriTexteB<<"les balices coincides parfaitement"<<'\n';
  }
 
 else
  {
   ecriTexteB<<"erreur votre fichier text contient des balises non recconue"<<'\n';
  }

 if(cptBal>0)
 {
   if(pile[0][0]!=-1)
   { 
     system("clear");
     cout<<sortieE<<endl;
     size_t iErr=0;
     cout<<"erreur fatal la balises ";
 
     while((pile[iErr][0]!=-1)&&(iErr<cptBal))
     { iErr++;}
       
     iErr--;
     cout<<'<'<<tabDico[pile[iErr][0]]<<'>'<<" ";
     cout<<" ligne: ";
     cout<<pile[iErr][3]<<" ";
     cout<<endl;
     cout<<"ne possede pas de balise fermante associé."<<endl;;
     erreurB=1;
    } 
   ecriTexteB<<"au total il y a "<<cptBal<<"balises"<<'\n';
   
 }
 if(!erreurB)
   { 
    if(!texteB.empty())
      { cout<<texteB;
        B.afficheG(texteB);}
   // cout<<texteB;
    ecriTexteB<<texteB;
    cout<<sortieE;
   }
 else if (erreurB)
   {
    //B.fin();
    cout<<endl;
    cout<<sortieE;
   }
}
void recupFichier::afficheConsole()
{ size_t tailleduTextB;
  size_t itB=0;
  unsigned char ch;
  string texteBaff="";
  
  ifstream liFichiereB; //on lit le fichier dans lequel on a ecrit le texteB
  liFichiereB.open("ecriTexteB.txt",ios::in|ios::binary);

  liFichiereB.seekg(0,ios::end);
  tailleduTextB=liFichiereB.tellg();
  liFichiereB.seekg(0,ios::beg);  

  if(liFichiereB.is_open())
  {
    while(itB<tailleduTextB)
    {
      ch=liFichiereB.get();
     // ecriTexteB<<ch<<'\n';
      texteBaff+=ch;
      itB++;
    }

    ecriTexteB<<"la tailleB="<<tailleduTextB<<'\n';
    ecriTexteB<<"voici le fichier texte coloré"<<'\n';
    ecriTexteB<<texteBaff<<'\n'; 
    ecriTexteB<<"boombitch"<<'\n';
  }
 
}


recupFichier::recupFichier(char* fichierParm)
{ char man[]="--man";
  if(strcmp(man,fichierParm)==0)
  {
    cout<<"Vous avez deux moyen d'afficher un texte: "<<endl;
    cout<<"Soit vous appellé l'executable avec votre fichier en parametre  "<<endl;
    cout<<"  ex: ./balTexte fichiertexte.txt"<<endl;
    cout<<"Ou alors a l'interieur du main, vous declarez une variable de type recupFichier sans parametre"<<endl;
    cout<<"et puis a la suite vous rempliser une variable de type string avec votre texte à afficher"<<endl;
    cout<<"puis vous inserz votre string dans la variable de type recupFichier grace a l'operateur <<"<<endl;
    cout<<"  ex: recupFichier F"<<endl;
    cout<<"      string S=texte a afficher;"<<endl;
    cout<<"      F<<S;"<<endl;
    cout<<"Voice donc nos deux option d''affichage disponible"<<endl;
  }
  else if(fichierParm!=NULL)
  {
   fichier=fichierParm;
   ecriTexteB.open("ecriTexteB.txt",ios::out|ios::binary);
   getTaille(); //OBTIENT LA TAILLE DU FICHIER EN OCTET
   chargeDico(); //CHARGE LE DICTIONAIRE DE BALISES EN MEMORIE DANS UN TAB STATIQUE
   chargeFichier();//CHARGE LE FICHIER TEXTE AVEC BALISES AVEC ESPACES; 
	 verifieBalises(); //VERIFIE LES BALISES ET ATRIBUE LES MODIFICATION AU TEXTE A BALISES
   ecriTexteB.close();
   //afficheConsole(); //AFFICHE LE FICHIER COLORE DANS LA CONSOLE
   //fNettoie(); //NETTOIE LE FICHIER DE TEXTE A BALISE EN MEMOIRE;
   //cout<<endl;
  }
}
recupFichier::recupFichier()
{

}
 

int main(int argc, char** argv)
{  
  recupFichier F(argv[1]);
  
  /*string s="";
  s+="aaaaadd        <color fg=yellow bg=red> salut <u  ceci est juste du texte/> inutile<b> un texte qui ne sert vraiment a rien <reverse> juste pour teste  é è à ù       éé                      éé é é équoi <hr> voila toujours un simple test   <color bg=blue fg=yellow>lalalala </color></hr>  puu<p> donc je continue a ecrire sans aucun sans , en vrai le projet avance mais sa galere un peu quand meme </p><p nempeche que ca va /> ooo</reverse> </b> </color><color bg=white></color><color fg=red bg=blue> </color> toujour       du texte <color bg=yellow fg=green>pour trouver la belle erreur </color> quil  me semble quelle se trouve dans les balises fermante what the ";

  F<<s;*/

  //Boite C;
  //C.init();

  //C.afficheG("ta fonction fait de la merde avec les accent :(il affiche despace entre les é qui devrait pas) et puis ta toujour pas reglé le probleme d'une chaine de charactere a la suite qui depace le tableau aa é é ébb cc  aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa si la chaine de charactere est trop long il chevauche avec le tableau");
  
  return 0;
}
