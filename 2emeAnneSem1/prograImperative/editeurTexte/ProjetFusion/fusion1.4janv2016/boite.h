#ifndef BOITE_H
#define BOITE_H

#include "cursor.h"
#include <iostream>
#include <string>
#include <sstream>

#include <string.h>

using namespace std;

class Boite {
  private:
    size_t _width, _height;
	  Cursor cur;
	  size_t col, lig;
	size_t nbLigLibre;
    int cptR; //compteur de retour a la ligne pour les  affiche
    unsigned int asciCode;
  public:
    Boite(size_t , size_t);	//construit une Boite de w x h
    Boite();
    //~Boite();

    size_t getWidth() const;
    size_t getHeight() const;
    void afficherBoite();
    void LigneSeparateur();
    void afficheText(std::string S, size_t x, size_t y);
    void origine();
    void fin();
    void afficheG(std::string T);
    void afficheD(std::string T);
    void afficheC(std::string T);
    void afficheJ(std::string T);
    void init();
    size_t getcol();
    size_t getlig();
    void clearA();

    void tailleCons();
    void initDel();
    void afficheMoi(string S);
    void reset();
    void afficheEsp(unsigned int j);
    bool espaceFin(string S);
    bool espaceDeb(string S);
    size_t cptEspace(string S);
    void coutJustify(string S,size_t COUNT);
};

#endif
