#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include <fstream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include "boite.h"
#include "cursor.h"

using namespace std;


//~Boite();

size_t Boite::getWidth() const{
  return _width;
}

size_t Boite::getHeight() const{
  return _height;
}

void Boite::afficherBoite(){
	
  //Cursor cur;
  cur.clear();
  cur.write("\0");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("\0");
  }
  cur.write("\0");
  cur.endl();
  for(size_t i=1;i<this->getHeight()-5;i++){
    cur.write("\0");
    cur.endl();
  }
  //cur.endl();
  cur.move_to(this->getWidth()-1,1);
  for(size_t i=1;i<this->getHeight()-4;i++){
    cur.write("\0");
    cur.endl();
    cur.move_to(this->getWidth()-1,i);

  }
  cur.move_to(0,this->getHeight()-5);
  cur.write("\0");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("\0");
  }
  cur.write("\0");

	
  cur.endl();	

}

void Boite::LigneSeparateur(){
  cur.move_to(0,lig);
  cur.write("C");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("d");
  }
  cur.write("D");
  cur.endl();
  cur.right(1);
  cur.move_to(0,this->getHeight()+1);
  lig++;
  
}

void Boite::afficheText(std::string S, size_t x, size_t y){
  cur.move_to(y,x);
  cur.write(S);
  cur.move_to(0,this->getHeight());

}

void Boite::origine(){
  cur.move_to(0,0);

}

void Boite::fin(){
  cur.move_to(0,getHeight()+1);
}

void Boite::tailleCons()
{
   struct winsize w;
   ioctl(0,TIOCGWINSZ, &w);
   _width= w.ws_col;
   _height=w.ws_row;
}




void Boite::init(){
  struct winsize w;
  
  ioctl(0,TIOCGWINSZ, &w);
  _width= w.ws_col;
  _height=w.ws_row;
  afficherBoite();

  
   
  lig=2;
  nbLigLibre=getHeight()-5;
}


void Boite::afficheMoi(string S)
{  ofstream fichCoord;
   fichCoord.open("traceCord.txt",ios::out|ios::binary|ios::app);
   if(fichCoord.is_open())
   {
   	fichCoord<<"jaffiche avec mot :"<<'\n'<<S<<"\n\n";
   }
   fichCoord.close();
 
 //cout<<' ';
 cout<<S;

 //cout<<endl;
}

void Boite::initDel()
{  for (size_t i=1;i<_width;i++) //DELIMITEUR HAUT
  {

    cur.deplaceVer(1,i);
    if(i==1)
  	cout<<"*";
   
   cout<<"-";
  }
  cout<<'*';


 for(size_t i=2;i<_height-5;i++) //DELIMITEUR COTÉ
   {
     cur.deplaceVer(i,1);
     cout<<'|';
	 cur.deplaceVer(i,_width);
	 cout<<'|';
    }
 cur.deplaceVer(_height-5,0);
 cout<<'*';
 
 for(size_t i=0;i<_width-2;i++) //DELIMITEUR BAS
    {
  	 cout<<"-";
    }
 cout<<'*';
 //cur.deplaceVer(_height,1);
  
  //cur.deplaceVer(2,1);
    //cout<<'J';
 
 //cout<<endl;
}

void Boite::reset()
{
  cur.deplaceVer(_height,1);
}

//AfficheG OK-----
void Boite::afficheG(string T)
{ 
   //on cpteur de retour a la ligne utilisé;
  ofstream fichierE;
  fichierE.open("traceG.txt",ios::out|ios::binary);
  fichierE<<"getHeight="<<_height<<" getWidth="<<_width<<'\n';
  int pageS;
  size_t plEsp=0;
  //size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(cptR==1)
   {fichierE<<"j'affiche un retour a la ligne car cptR==1"<<'\n';
   	cout<<'\n';
   	cptR=0;
   }
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.length();i++)
	 {
	  fichierE<<"T["<<i<<"]="<<T[i]<<endl;
	  fichierE<<"nbLigLibre="<<nbLigLibre<<endl;
	  
	  if(nbLigLibre==0)
	  { 
	  	cout<<"\e[0m";
	  	fichierE<<"je boucle dans change pages"<<'\n';
	  	initDel();
	    afficheMoi("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?");
	    std::cin>>pageS;
	    cur.clear();
	    cout<<"\n";
	    afficheMoi(style);
	    nbLigLibre=_height-7;
	  }
 
	  
	    
	     if(T[i]=='\e' )
		    {fichierE<<"j'incremente escp";
		     escape++;
	        }
      
	     if((i!=0) && T[i-1]=='m' && escape && ((T[i-3]=='[')||(T[i-4]=='[')))
	     	{
	     	 fichierE<<"je decremente escp"<<"T[i-3]="<<T[i-3]<<" T[i-4]="<<T[i-4]<<endl;
	     	 escape--;
	     	}
      
	     if(escape)
		  {  fichierE<<"je suis dans echappe"<<endl;
	         mot+=T[i];
		     style+=T[i];
	      }
	    
      fichierE<<"escape="<<escape<<endl;
	  if((!escape))
	    {
	     asciCode=T[i];
	     fichierE<<"asciCode="<<asciCode<<endl;
	     if(asciCode==195 || asciCode==197 || asciCode==4294967235 || asciCode==4294967237)
	     	{
	     	 fichierE<<"je boucle dans accent"<<endl;
	     	 COUNT--; //ON GERE LES CARACTERE ACCENTUÉ 
	     	}

	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape )
	  { if(!espaceDeb(mot))
	  	cout<<' ';

	    afficheMoi(mot);
	    //cout<<endl;
	    mot="";
	    
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	  

	  plEsp=0;
    while(true && !escape)
      {
       if((T[i+plEsp+1]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp+1]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp+1]==' '||T[i+plEsp+1]=='\0')
          {break;}       
       
       else if (T[i+plEsp+1]=='\e')
          {break;}
       
       else
        {plEsp++;}
     } 
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>_width-3&&!escape)
	    { 
	     fichierE<<"j'afficheText ligne="<<lig<<" colone="<<2<<"\n\n";
	      if(!espaceDeb(mot))
	         cout<<" ";
	      afficheMoi(mot);
	      cout<<endl;
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      
	      //nbColLibre==getWidth()-3;
	      plEsp=0;
        }

   
   
	 }
	  if(!mot.empty())
	  { 
	  	if(!espaceDeb(mot))
	         cout<<" ";
	  	
        afficheMoi(mot);
    	lig++;
    	nbLigLibre--;
    	cout<<endl;
       }
	      
      
        
      fichierE.close();
      
    }
}





void Boite::afficheD(std::string T)
{ ofstream fichierE;
  fichierE.open("traceD.txt",ios::out|ios::binary|ios::app);
  int pageS;
  size_t plEsp=0;
 // size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t i;
  size_t COUNT=0;
  string mot="";
  string style="";

  if(cptR==1)
   {fichierE<<"j'affiche un retour a la ligne car cptR==1"<<'\n';
   	cout<<'\n';
   	cptR=0;
   }

  if(fichierE.is_open())
    { fichierE<<"heigth="<<_height<<" width="<<_width<<endl;
     for(i=0;i<T.length();i++)
	    {
	     
	     if(nbLigLibre==0) 
	       { cout<<"\e[0m";
	       	 fichierE<<"je boucle dans change pages"<<'\n';
	  		 initDel();
	         afficheMoi("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?");
	         std::cin>>pageS;
	         cur.clear();
	         cout<<"\n";
	         
	         afficheMoi(style);
	         nbLigLibre=_height-7;
	        }
   
         
         
	      if(T[i]=='\e' )
		    {
		     fichierE<<"j'incremente escp";
		     escape++;
	        }
      
	        if((i!=0) && T[i-1]=='m' && escape && ((T[i-3]=='[')||(T[i-4]=='[')))
	     	{
	     	 fichierE<<"je decremente escp"<<"T[i-3]="<<T[i-3]<<" T[i-4]="<<T[i-4]<<endl;
	     	 escape--;
	     	}
      
	        if(escape)
		   { 
		   	 fichierE<<"je suis dans echappe"<<endl;
	         mot+=T[i];
		     style+=T[i];
	       }
	    
	        
         

         if((!escape))
	       { asciCode=T[i];
	  	     
	    	 if(asciCode==195 || asciCode==197 || asciCode==4294967235 || asciCode==4294967237)
	     	  {
	     	    fichierE<<"je boucle dans accent"<<endl;
	     	    COUNT--; //ON GERE LES CARACTERE ACCENTUÉ 
	     	  }
 			 COUNT++;
	         mot+=T[i];
	        }

	        //J'AFFICHE SI JE TROUVE UUNE ENTRE DANS LE STRING T
         if((T[i]=='\n')&&!escape )
     	    {
     	     fichierE<<"j'afficheText dans entree ligne="<<lig<<" colone="<<_width-COUNT<<'\n';
	 	     afficheEsp(_width-COUNT);
	         afficheMoi(mot);
	         mot="";
	     	 
	         lig++;
	         nbLigLibre--;
	         COUNT=0;
	         
	    	}

        
	     plEsp=0;
         while(true && !escape)
            { 

         	 if((T[i+plEsp+1]==' '))
            	{
             	 fichierE<<"je marche avec espace"<<'\n';
                }

         	 else if(((T[i+plEsp+1]=='\e')))
                {
                 fichierE<<"je marche avec e"<<'\n';
                }
  
 	         if(T[i+plEsp+1]==' '||T[i+plEsp+1]=='\0')
             {fichierE<<"je break"<<'\n';
             	break;}       
       
             else if (T[i+plEsp+1]=='\e')
             {break;}
       
             else
             {plEsp++;}
           } 
    
	     fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n'; 
		 fichierE<<"mot="<<mot<<"\n\n";
		 fichierE<<"T["<<i<<"]="<<T[i]<<' ';
		 fichierE<<"asciCode="<<asciCode<<endl;
         
        /* if((mot[i]=='\0') && (i>0))
 			{
 			 fichierE<<"je decremente COUNT car j'ai trouvé un null"<<endl;
 		     COUNT--;
 		    }
        */
		 if(COUNT+plEsp>_width-3&&!escape)
	  	   { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<_width-COUNT<<'\n';
	 	     
	 	     afficheEsp(_width-COUNT);
	 	     afficheMoi(mot);
	 	     COUNT=0;
	  	     lig++;
	  	     nbLigLibre--;
	   	     mot="";
	   	     
	         // nbColLibre==getWidth()-3;
	         plEsp=0;
           }
      
	   }
	   
     if(!mot.empty())
       { fichierE<<"j'affiche le mot dans la derniere boucle avec mot:"<<endl;
    	 fichierE<<mot<<endl;
    	 fichierE<<"a la col="<<_width-COUNT;
    	 
       	
       		if(!espaceFin(mot))
    	   	{
       		 COUNT++;
       		}

       	afficheEsp(_width-COUNT);
       	afficheMoi(mot);
       	lig++;
       	nbLigLibre--;
       	cout<<endl;
       }
	 //fin();
     
   
     fichierE.close();
    }
}

void Boite::afficheC(string T)
{ ofstream fichierE;
  fichierE.open("traceC.txt",ios::out|ios::binary);
  int pageS;
  size_t plEsp=0;
 // size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t i;
  size_t COUNT=0;
  string mot="";
  string style="";
  fichierE<<"heigth="<<_height<<" width="<<_width<<endl;
  if(cptR==1)
   {
   	fichierE<<"j'affiche un retour a la ligne car cptR==1"<<'\n';
   	cout<<'\n';
   	cptR=0;
   }
  
  if(fichierE.is_open())
    { 
      for(i=0;i<=T.length();i++)
	 { 
	  fichierE<<"T["<<i<<"]="<<T[i]<<endl;
	  fichierE<<"nbLigLibre="<<nbLigLibre<<endl;
	  
	  if(nbLigLibre==0)
	    {
	   	 cout<<"\e[0m";
	     initDel();
	     afficheMoi("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?");
	     std::cin>>pageS;
	     cur.clear();
	     cout<<"\n";
 		 afficheMoi(style);
	     nbLigLibre=_height-7;
	    }

      if(T[i]=='\e' )
	   {
	     escape++;
	    }
      
	  if((i!=0) && T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
	   {
	     fichierE<<"je decremente escp"<<"T[i-3]="<<T[i-3]<<" T[i-4]="<<T[i-4]<<endl;
	     escape--;
	   }
      
	   	
     
	  if(escape)
	  	{
	  	 fichierE<<"je suis dans echappe"<<endl;
	 	 mot+=T[i];
		 style+=T[i];
	    }
	    
      fichierE<<"escape="<<escape<<endl;
	  if((!escape))
	    { 
	      asciCode=T[i];
	      fichierE<<"asciCode="<<asciCode<<endl;
	      if(asciCode==195 || asciCode==197 || asciCode==4294967235 || asciCode==4294967237)
	     	  {
	     	    fichierE<<"je boucle dans accent"<<endl;
	     	    COUNT--; //ON GERE LES CARACTERE ACCENTUÉ 
	     	  }
	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape )
	  {
	  	fichierE<<"j'afficheText dans entree ligne="<<lig<<" colone="<<(size_t)(_width-COUNT)/2<<'\n';
	    afficheEsp((size_t)((_width-COUNT)/2));
	    afficheMoi(mot);
	    mot="";
	    cout<<endl;
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	 plEsp=0;
     while(true && !escape)
      {

       if((T[i+plEsp+1]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp+1]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp+1]==' '||T[i+plEsp+1]=='\0')
          {fichierE<<"je break"<<'\n';
          	break;}       
       
       else if (T[i+plEsp+1]=='\e')
          {break;}
       
       else
        {plEsp++;}
      } 
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';
	  fichierE<<"mot="<<mot<<"\n\n";
	  fichierE<<"T["<<i<<"]="<<T[i]<<' ';
	  fichierE<<"asciCode="<<asciCode<<endl;
	 

	  if(COUNT+plEsp>_width-3&&!escape)
	    { 
	      fichierE<<"j'afficheText ligne="<<lig<<" colone="<<(size_t)(_width-COUNT)/2<<'\n';
	      //afficheText(mot.substr(0,mot.size()-1),lig,(getWidth()-COUNT)/2+1);
	      afficheEsp((size_t)(_width-COUNT)/2);
	      afficheMoi(mot);
	      cout<<endl;
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      plEsp=0;
     
	    }
     } //Boucle for FIN
 	 
 	 if(!mot.empty())
       { fichierE<<"j'affiche le mot dans la derniere boucle avec mot:"<<endl;
    	 fichierE<<mot<<endl;
    	 fichierE<<"a la col="<<((size_t)(_width-COUNT)/2);

       	 if(mot.back()!=' ')
           {/* if(mot.back()=='m')
              {
          	   	fichierE<<"mot[mot.length()-1]="<<mot[mot.length()-1]<<" ";
          		if(((mot[mot.length()-3]=='[')||(mot[mot.length()-4]=='[')) && ((mot[mot.length()-4]=='\e')||(mot[mot.length()-5]=='\e')))
          		{
              
          		}
       		  }*/
             
       	  	 //else
         	 //{
       	  	 fichierE<<"motBack="<<mot.back();
       	  	 COUNT++;
       	 	// }
       		}

       	afficheEsp((size_t)(_width-COUNT)/2);
       	afficheMoi(mot);
       	lig++;
       	nbLigLibre--;
       	cout<<endl;
       }
      	     
      
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}

void Boite::afficheJ(string T)
{ 
 int pageS; 
 ofstream fichierE;
 fichierE.open("traceJ.txt",ios::out|ios::binary);
 fichierE<<"getHeight="<<_height<<" getWidth="<<_width<<'\n';
  
  size_t plEsp=0;
  //size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  fichierE<<"Je suis dans justify"<<endl;
  
  if(cptR==1)
   {
   	fichierE<<"j'affiche un retour a la ligne car cptR==1"<<'\n';
   	cout<<'\n';
   	cptR=0;
   
   }
  
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.length();i++)
	 {
	  fichierE<<"T["<<i<<"]="<<T[i]<<endl;
	  fichierE<<"nbLigLibre="<<nbLigLibre<<endl;
	  
	  if(nbLigLibre==0)
	    { 
	  	 cout<<"\e[0m";
	  	 fichierE<<"je boucle dans change pages"<<'\n';
	  	 initDel();
	     afficheMoi("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?");
	     std::cin>>pageS;
	     cur.clear();
	     cout<<"\n";
	     afficheMoi(style);
	     nbLigLibre=_height-7;
	   }
 
	  
	    
	 if(T[i]=='\e' )
	    {fichierE<<"j'incremente escp";
	     escape++;
	    }
     
	 if((i!=0) && T[i-1]=='m' && escape && ((T[i-3]=='[')||(T[i-4]=='[')))
	  	{
	  	 fichierE<<"je decremente escp"<<"T[i-3]="<<T[i-3]<<" T[i-4]="<<T[i-4]<<endl;
	  	 escape--;
	  	}
      
	 if(escape)
	   {  fichierE<<"je suis dans echappe"<<endl;
	     mot+=T[i];
	     style+=T[i];
	    }
	    
     fichierE<<"escape="<<escape<<endl;
	 if((!escape))
	    {
	     asciCode=T[i];
	     fichierE<<"asciCode="<<asciCode<<endl;
	     if(asciCode==195 || asciCode==197 || asciCode==4294967235 || asciCode==4294967237)
	     	{
	     	 fichierE<<"je boucle dans accent"<<endl;
	     	 COUNT--; //ON GERE LES CARACTERE ACCENTUÉ 
	     	}

	      COUNT++;
	      mot+=T[i];
	    }
	 
    
	 if((T[i]=='\n')&&!escape )
	    { 
	  	
	     fichierE<<"j'affiche car j'ai trouvé une espace"<<endl;
	   	 if(!espaceDeb(mot))
	  	 cout<<' ';
	     afficheMoi(mot);
         //cout<<endl;
	     mot="";
	     lig++;
	     nbLigLibre--;
	     COUNT=0;
	    }


	  

	  plEsp=0;
     while(true && !escape)
      {
       if((T[i+plEsp+1]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp+1]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp+1]==' '||T[i+plEsp+1]=='\0')
          {break;}       
       
       else if (T[i+plEsp+1]=='\e')
          {break;}
       
       else
        {plEsp++;}
     } 
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';
	  fichierE<<"mot= "<<mot<<"\n\n";
	  
	 // cout<<"je boucle"<<endl;
	  if(COUNT+plEsp>_width-3&&!escape)
	    {
	     coutJustify(mot,COUNT);
	     
	     lig++;
	     nbLigLibre--;
	     COUNT=0;
 		 mot="";
	    } //fin if(_-width-COUNT>3 ......

	}//fin for loop
	  if(!mot.empty())
	  { 
	  	if(!espaceDeb(mot))
	         cout<<" ";
	  	
        afficheMoi(mot);
    	lig++;
    	nbLigLibre--;
    	cout<<endl;
       }
	      
      
        
      fichierE.close();
      
    }

}


size_t Boite::cptEspace(string S)
{ ofstream fCptEspace;
  
  fCptEspace.open("traceEspaceCpt.txt",ios::out|ios::binary);
  if(!fCptEspace.is_open())
 	{
     return 0;
    }

  else 
  {
  	fCptEspace<<"fichier correctement ouvert"<<endl;
  }

  size_t cptE=0;
  for(size_t i=0;i<S.length();i++)
	{
	 if(S[i]==' ')
	  {
	  	cptE++;
	  }
	}

  fCptEspace.close();
  return cptE;
  

}

bool Boite::espaceDeb(string S)
{
  ofstream fEspace;
  fEspace.open("traceEspaceD.txt",ios::out|ios::binary);
  if(!fEspace.is_open())
  {
  	return false;
  }

  else 
  {
  	fEspace<<"fichier correctement ouvert"<<endl;
  }
 size_t i=0;
 string inter;
 inter="";
 while(i<S.length())
 	{if(S[i]=='\e')
 		{
 			while(S[i]!='m')
 			{
 				i++;
 			}
 		 i++;
 		}
 	 
 	 else
 	  { fEspace<<"j'ajoute un caractere="<<S[i]<<endl;
 	  	inter+=S[i];
 	  	i++;
 	  	
 	  }

 	}
 fEspace<<"inter="<<inter<<endl;

 if(inter[0]==' ')
 	{	
 	 fEspace<<"il y a bien un espace au deb du mot"<<endl;
 	 return true;
 	}

 else 
 	{
 	 fEspace<<"il y a pas d'espaces au deb du mot "<<endl;
 	 return false;
 	}
}




bool Boite::espaceFin(string S)
{ ofstream fEspace;
  fEspace.open("traceEspaceF.txt",ios::out|ios::binary);
  if(!fEspace.is_open())
  {
  	return false;
  }

  else 
  {
  	fEspace<<"fichier correctement ouvert"<<endl;
  }
 size_t i=0;
 string inter;
 inter="";
 while(i<S.length())
 	{if(S[i]=='\e')
 		{
 			while(S[i]!='m')
 			{
 				i++;
 			}
 		 i++;
 		}
 	 
 	 else
 	  { fEspace<<"j'ajoute un caractere="<<S[i]<<endl;
 	  	inter+=S[i];
 	  	i++;
 	  	
 	  }

 	}
 fEspace<<"inter="<<inter<<endl;

 if(inter.back()==' ')
 	{	fEspace<<"il y a bien un espace a la fin du mot"<<endl;
 		return true;
 	}
 else 
 	{
 	 fEspace<<"il y a pas d'espaces a la fin du mot "<<endl;
 	 return false;
 	}
}



void Boite::clearA()
{
  system("clear");
}

//POR SI ACASO

void SetCursorPos(int XPos, int YPos)
{
 printf("\033[%d;%dH", YPos+1, XPos+1);
}

void Boite::afficheEsp(unsigned int j)
{
  for( size_t i=0;i<j;i++)
  {
  	cout<<' ';
  }
}

void  Boite::coutJustify(string mot,size_t COUNT)
{
 size_t resteEsp;
 size_t espTrop; //on stock les espaces a en trop en fonction de _width 
 size_t cptE=0;
 size_t espAttr;
 string interm="";
 srand (time(NULL));
 int random;
 ofstream fichierT;
 fichierT.open("tracecoutJ.txt",ios::out|ios::binary);
 if(!fichierT.is_open())
 {cout<<"erreur de fichier d'ouvertir dans coutJustify";}
  
 cptE=cptEspace(mot);
 fichierT<<"cptE="<<cptE<<endl;
 espTrop=_width-COUNT;
 fichierT<<"espEnTrop="<<espTrop<<endl;
 //random=rand()%2;
 //fichierE<<"random="<<random<<endl;
 espAttr=espTrop/cptE; //nombre d'espaces minimum a ecrire entre chaque mot
 fichierT<<"espAttr="<<espAttr<<endl;
 fichierT<<"--------------------"<<endl;
 
 if(!espaceDeb(mot))
  	cout<<' ';
 if(!espaceFin(mot))
 	mot+=' ';

 

 
 
 if(espAttr==0)
   { 
     size_t j=0;
  	 while(j<mot.length())
   	   { fichierT<<"interm="<<interm<<endl;
   	 	 fichierT<<"cptE="<<cptE<<endl;
		 fichierT<<"espEnTrop="<<espTrop<<endl;
		 fichierT<<"espAttr="<<espAttr<<endl;
   	   	 fichierT<<"j="<<j<<endl;
   	   	 fichierT<<endl;
   	     if(mot[j]==' ')
  	        {
   	         
   	         interm+=' ';
  	         cout<<interm;
   	         interm="";
	         random=rand()%2; 	
	         fichierT<<"random="<<random<<endl;     
	         if(espTrop>0 && random==1)
	            { fichierT<<"j'affiche un espace car estTrop>0 && random==1"<<endl;
	             afficheEsp(1);
	     	     espTrop--;
	     	     cptE--;
	     	     j++;
	            }
	         else if(espTrop>0 && cptE==espTrop)
	         	{fichierT<<"j'affiche un espace car cptE==espTrop "<<endl;
	         	 afficheEsp(1);
	     	     espTrop--;
	     	     cptE--;
	     	     j++;	     	      	
	            }

	         else
	        	{
	         	 cptE--;
	             j++;
	       	    }
	        }
	     	
   	     else
   	   		{
   	   	     interm+=mot[j];
       	     j++;
   		    }
   	    }
    }

 else if(espAttr>0)
   { resteEsp=espTrop%cptE;
     size_t j=0;
 
     while(j<mot.length())
	 	{
	   	 if(mot[j]==' ')
	        {
	         interm+=' ';
	         if(!espaceDeb(interm))
	     	  cout<<' ';
	         
	         random=rand()%9;
	         cout<<interm;
	         interm="";
	     	     
	         if(espTrop>0 && random>=4 && resteEsp>0) //on ajoute espace atribue plus reste
	            {
	             afficheEsp(espAttr+1);
                 espTrop-=espAttr+1;
	             cptE--;
	             j++;
	             resteEsp--;
	            }
	     	     
	         else if(espTrop>0) //on ajoute espace atribue 
	            {
	             afficheEsp(espAttr);

	         	 espTrop-=espAttr;
	             cptE--;
	             j++;
	             resteEsp--;
	            }

	         else if(espTrop>0 && cptE==1) /*si on el sur le dernier mot -dernierMot____| tel que - est le curseur placé sur un espace*/ \
	     	     								//et | le bord de  console droit avec _ representant un espace en trop
	           {afficheEsp(espTrop+resteEsp);
	            espTrop=0;
	            cptE--;
	            j++;
	            resteEsp=0;	     	      	
	           }

	         else
	            {
	         	 cptE--;
	             j++;
	            }
	        } 
	     	
  	     else
  	       {
     	     interm+=mot[j];
	     	 j++;
	       }
	  	}
    }
	    cout<<"mot="<<mot<<endl; 
}

Boite::Boite(size_t w, size_t h) : _width(w), _height(h), cur(0,0) {}

Boite::Boite():cur(0,0)
{	cur.clear();
	lig=2;col=2;
	//std::cout << "\033[2J\033[1;1f";
	tailleCons();
	cptR=1;
	nbLigLibre=_height-7;
	//initDel();
//cout<<"je marche"<<endl;
	//afficheMoi("salut ceci est une facon de fking afficher un texte",3,3);
}