#ifndef RECUPEREB_H
#define RECUPEREB_H

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>

using namespace std;

class recupFichier
{
	public:
	 recupFichier(char* fichierParm);
	 recupFichier();
	 void chargeDico();
	 void chargeFichier();
	 void fNettoie();
	 void verifieBalises(); //verifie que les balises dans le fichier texte possede bien une sintaxe valiee
	 void verifiePile();//verifie que chaque balise ovrante possede bien une balise fermante
	 void getTaille();
	 void afficheConsole();
	 void operator<<(string& S);
	 ~recupFichier()
	 {
	 	
	 }
	private:
	 unsigned char* tabFichierSN; //tableau sans netoyé 
	 unsigned char* tabFichier;  //tableau ou on stock le fichier texte a balises sans espace
	 unsigned char tabDico[24][9];
	 int** pile;
	 size_t cptBal;
	 unsigned long long int fTaille;
	 char* fichier;
	 string texteB;
	 ofstream ecriTexteB;
	 

};

#endif