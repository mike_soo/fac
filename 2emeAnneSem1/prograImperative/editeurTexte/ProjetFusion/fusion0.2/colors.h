#ifndef _COLORS_
#define _COLORS_
#include <iostream>
#include <string>
#include <sstream>

std::string writeCo(std::string s, std::string FB, std::string C, short int option);

enum Couleur {
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,
    //reset
};

/*class Colorizer{
  private:
    Couleur col;  
  public:
    Colorizer(Couleur c);  
    void write(std::string s, std::string FB, std::string C);    
    void setcolor(Couleur c);
};*/

//#define RST  "\x1B[0m"

/* FOREGROUND COLOR */
/*#define BLK  "\x1B[30m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define YEL  "\x1B[33m"
#define BLU  "\x1B[34m"
#define MAG  "\x1B[35m"
#define CYN  "\x1B[36m"
#define WHT  "\x1B[37m"

#define FBLK(x) BLK x RST
#define FRED(x) RED x RST
#define FGRN(x) GRN x RST
#define FYEL(x) YEL x RST
#define FBLU(x) BLU x RST
#define FMAG(x) MAG x RST
#define FCYN(x) CYN x RST
#define FWHT(x) WHT x RST
*/
/* BACKGROUND COLOR*/
/*#define _BLK  "\x1B[40m"
#define _RED  "\x1B[41m"
#define _GRN  "\x1B[42m"
#define _YEL  "\x1B[43m"
#define _BLU  "\x1B[44m"
#define _MAG  "\x1B[45m"
#define _CYN  "\x1B[46m"
#define _WHT  "\x1B[47m"

#define BBLK(x) _BLK x RST
#define BRED(x) _RED x RST
#define BGRN(x) _GRN x RST
#define BYEL(x) _YEL x RST
#define BBLU(x) _BLU x RST
#define BMAG(x) _MAG x RST
#define BCYN(x) _CYN x RST
#define BWHT(x) _WHT x RST
*/
/* AUTRES */
/*#define BOLD(x) "\x1B[1m" x RST
#define ITAL(x) "\x1B[3m" x RST //fonctionne pas
#define UNDL(x) "\x1B[4m" x RST
#define BLINK(x) "\x1B[5m" x RST //fonctionne pas
#define REVE(x) "\x1B[7m" x RST 

*/
#endif 
