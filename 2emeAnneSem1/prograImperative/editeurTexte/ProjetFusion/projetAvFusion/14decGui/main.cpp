#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include "boite.h"
#include "cursor.h"
#include "colors.h"

using namespace std;


void disp(string T,Boite B, size_t *nbColLibre, size_t *nbLigLibre, size_t *col, size_t *lig)
{
  bool inBalise=0;
  size_t COUNT=0;

  string mot="";
  //size_t col=2; size_t lig=3;
  size_t nbEsp=0;
  for(size_t i=0;i<=T.size();i++){
    if(i!=0){
      if(T[i]=='<' && T[i-1]!='\\')
	inBalise=1;
      if(T[i-1]=='>' && T[i-1]!='\\')
	inBalise=0;
    }
    else{
      if(T[i]=='<')	
	inBalise=1;
    }
    if(!inBalise){
      COUNT++;
      mot+=T[i];
    }

    if(T[i]=='\n' && !inBalise){
      B.afficheText(mot,*lig,*col);
      mot="";
      *col=2;
      *lig++;
      *nbColLibre=B.getWidth()-3;
      COUNT=0;
    }

    if(T[i]=='*'){
      B.vider();
      *col=2;
      *lig=3;
    }

    /*if(nbEsp==1){
      col=col-10;
      nbColLibre-=10;
      }*/

    if(T[i]==' ' || i==T.size()){
      if(*nbColLibre<COUNT){
	*lig++;
	*col=2;
	B.afficheText(mot,*lig,*col);
	mot="";
	*col+=COUNT;
	*nbColLibre=B.getWidth()-3-COUNT;
	COUNT=0;
	nbEsp++;
      }
      else{
	B.afficheText(mot,*lig,*col);
	nbColLibre-=COUNT;
	mot="";
	*col+=COUNT;
	COUNT=0;
	nbEsp++;
      }
    }
    //B.origine();
    B.fin();
  }
}

int main(int argc, char** argv){
  struct winsize w;
  ioctl(0,TIOCGWINSZ, &w);
  Boite B(w.ws_col,w.ws_row);
  B.afficherBoite();
  //B.LigneSeparateur(4);

  bool gras=0;
  bool ita=0;
  bool soulign=0;
  bool blink=0;
  bool reverse=0;
  
  short int option=1*gras+2*ita+4*soulign+8*blink+16*reverse;
  
  string var="dee dfg";

  /*string text=writeCo(var,"9","6",option);
    string text1=writeCo("hehe","1","2",option);
    B.afficheText(text1,10,10);
    B.origine();
    B.afficheText(text,11,11);
    B.origine();
    B.afficheText(text1,12,10);
    B.origine();
    B.fin();*/

  //char* tt=new char[10];
  //tt="azertyu";
  //char* ttt=new char[10];
  //strcat(ttt,"iop");
  //cout<<ttt;

  //size_t ftaille=160;
  //char* T= new char[ftaille];
  std::string k="Je teste mon<ignore> afficheur\nde texte avec cette phrase qui ne sert vraiment a rien et qui n'a aucun sens. C'est trop bien ce truc de merde pour du texte, je k  crois que ca fonctionne!";
  //T="hjze zfer zer zaere  dzf zf zae fze fzaef zjhf g r ";
  size_t nbColLibre=B.getWidth()-3;
  size_t nbLigLibre=B.getHeight()-3;
  string T1=writeCo(var,"7","2",option,&nbColLibre);
  string T2=writeCo(var,"2","1",option,&nbColLibre);
  string T=k;

  size_t col=2;
  size_t lig=3;
  disp(k,B,&nbColLibre,&nbLigLibre,&col,&lig);

  return 0;
}
