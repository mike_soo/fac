#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include "boite.h"
#include "cursor.h"

using namespace std;

Boite::Boite(size_t w, size_t h) : _width(w), _height(h), cur(0,0) {}

//~Boite();

size_t Boite::getWidth() const{
	return _width;
}

size_t Boite::getHeight() const{
	return _height;
}

void Boite::afficherBoite(){
	
	//Cursor cur;
	cur.clear();
	cur.write("A");
	for(size_t i=1;i<this->getWidth()-1;i++){
		cur.write("c");
	}
	cur.write("B");
	cur.endl();
	for(size_t i=1;i<this->getHeight()-2;i++){
		cur.write("a");
		cur.endl();
	}
	//cur.endl();
	cur.move_to(this->getWidth()-1,1);
	for(size_t i=1;i<this->getHeight()-1;i++){
		cur.write("f");
		cur.endl();
		cur.move_to(this->getWidth()-1,i);

	}
	cur.move_to(0,this->getHeight()-2);
	cur.write("E");
	for(size_t i=1;i<this->getWidth()-1;i++){
		cur.write("e");
	}
	cur.write("F");

	
	cur.endl();	

}

void Boite::LigneSeparateur(size_t ligne){
  cur.move_to(0,ligne+1);
  cur.write("C");
  for(size_t i=1;i<this->getWidth()-1;i++){
  	cur.write("d");
  }
  cur.write("D");
  cur.endl();
  cur.right(1);
  cur.move_to(0,this->getHeight()+1);
}

void Boite::afficheText(std::string S, size_t x, size_t y){
	cur.move_to(y,x);
	cur.write(S);
	cur.move_to(0,this->getHeight());

}

void Boite::origine(){
	cur.move_to(0,0);

}

void Boite::fin(){
	cur.move_to(0,getHeight()+1);
}

void Boite::vider(){
	for(int W=2;W<_width-2;W++){
		for(int H=3;H<_height-3;H++)
			Boite::afficheText(" ",H,W);
	}
}

/*int main(){
	struct winsize w;
	ioctl(0,TIOCGWINSZ, &w);
	Boite B(w.ws_col,w.ws_row);
	//cout<<"la boite va bien";
	B.afficherBoite();
	B.LigneSeparateur(4);
	return 0;
}*/