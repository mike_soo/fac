#ifndef BOITE_H
#define BOITE_H

#include "cursor.h"
#include <iostream>
#include <string>
#include <sstream>

class Boite {
  private:
    size_t _width, _height;
	  Cursor cur;

  public:
    Boite(size_t , size_t);	//construit une Boite de w x h
    Boite();
    //~Boite();

    size_t getWidth() const;
    size_t getHeight() const;
    void afficherBoite();
    void LigneSeparateur(size_t ligne);
    void afficheText(std::string S, size_t x, size_t y);
    void origine();
    void fin();
    void vider();
    void afficheG(std::string T);
};

#endif
