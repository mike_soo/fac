#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "recupereB.hpp"

using namespace std;

void recupFichier::getTaille()
{
 ifstream fichLecture;
 fichLecture.open(fichier,ios::in|ios::binary);
 fichLecture.seekg(0,ios::end);
 fTaille=fichLecture.tellg();
 fichLecture.seekg(0,ios::beg);

 cout<<"tailleFichier="<<fTaille<<endl;
 fichLecture.close();

}

void recupFichier::chargeFichier()
{ cptBal=0;
  ifstream fichLecture;
  unsigned char c;
  fichLecture.open(fichier,ios::in|ios::binary);
  if(fichLecture.is_open())
  {
   tabFichierSN=new unsigned char[fTaille];
   size_t i=0;
   while(i<fTaille)
   {
    c=fichLecture.get();
    if(i>=1)
      {
       if((c=='<')&&(tabFichierSN[i-1]!='<'))
        {
          cptBal++;
        }
      }
     tabFichierSN[i]=c;
     i++;
   }
  }
  cout<<tabFichierSN;
 fichLecture.close(); 
}



void recupFichier::chargeDico()
{ unsigned char c;
  size_t j=0;
  size_t i=0;
  while(i<24)
  {
    while(j<9)
    { 
      tabDico[i][j]='\0';
      j++;
    }
        
   j=0;
   i++;
  }
  ;
  ifstream fDico;
  fDico.open("fDico.txt",ios::in|ios::binary);
  if(fDico.is_open())
    { c=fDico.get();
      i=j=0;

       while(c!='#')
      {
        while((c!='\n')&&(c!='#'))
        {
         tabDico[i][j]=c;
         j++;
           
         c=fDico.get();
        }
      
       j=0;
       i++;
       if(c!='#')
        c=fDico.get();  
      }
  
      cout<<"i="<<i<<"j="<<j<<endl;
      cout<<"ca marche"<<endl;
      cout<<"tableauF"<<endl;
      i=j=0;
      while(i<24)
      {
        while(j<9)
        { if(tabDico[i][j]=='\0')
            {
             cout<<'0';
            }
          else 
          cout<<tabDico[i][j];
          j++;
          
        }
        cout<<endl;
        j=0;
        i++;
      }
      cout<<endl;
    }
  else
     {cout<<"erreur a l'ouverture de fichier fDico"<<endl;}
 
 fDico.close(); 
}



void recupFichier::fNettoie() //charge le fichier passé en parametre dans la ram 
{unsigned char c;
 size_t nbEspace=0;
 size_t nbEntre=0;
 size_t i=0;
 ifstream fichLecture;
 fichLecture.open(fichier,ios::in|ios::binary);
 size_t posCurseur=0;
 unsigned iSN=0;

 if(fichLecture.is_open())
 	{   	 
 	 tabFichier = new unsigned char[fTaille];   //ON NETOIE TOUT LES ESPACES ET RETOUR A LA LIGNE EN PLUS
 	 cout<<"---------------------------"<<endl;
 	 cout<<"c=";
 	 //fichLecture.read(tabFichier,fTaille);
 	 c=tabFichierSN[iSN];
   iSN++;
 	 cout<<c;
 	 while(posCurseur<fTaille)
 	    {
 	     if((c==' ')||(c=='\n')||(c=='\t')) //SI ON TROUVE UN ESPACE OU ENTRER OU TAB
 	        {
 	        
		     if((nbEspace==0)&&(c==' '))
		    {
		     tabFichier[i]=' ';
		     i++;
		     nbEspace++;
		    }	 
		    else if((nbEntre==1)&&(c=='\n')) 
		    {
		     tabFichier[i-1]='\n';
		     nbEntre++;
		     //cout<<"je boucle en entre ecrit";
		    } 
		    else if((nbEntre==0)&&(c=='\n')&&(nbEspace==0))
		    {
		     tabFichier[i]=' ';
		     nbEspace++;
		     nbEntre++;
		     i++;
		     //cout<<"j'ecrit espace";
		    }  
		    else if((nbEntre==0)&&(c=='\n'))
		    {//cout<<"je boucle en entre";
		     nbEspace++;
		    }
		    else if((c=='\t')&&(nbEspace==0))
		    {
		     tabFichier[i]=' ';
		     i++;
		     nbEspace++;
		    }
		    else if(nbEntre>1)
		    {
		    	nbEntre++;
		    }

		   }
		 
		 else 
		  {
		  	tabFichier[i]=c;
		  	nbEntre=0;
		  	nbEspace=0;
		  	i++;
          } 
		 iSN++;
		 c=tabFichierSN[iSN];
		 cout<<c;
		 posCurseur++;

		}
		cout<<endl;
 	}
 else 
  {
   cout<<"erreur d'ouverture de chargement du fichier"<<endl;}
   cout<<endl;
   //cout<<"tabFichier"<<endl;
   //cout<<tabFichier<<endl;
  cout<<"<===============tableau tabFichier================>"<<endl;	
  printf("%s\n",tabFichier);
  fichLecture.close();
  delete [] tabFichierSN;
  }





void recupFichier::verifieBalises()
{ size_t i=0;
  size_t erreurB=0;
  size_t nbEspace=0;
  size_t nbEntre=0;
  int coulCod=0;
  size_t indDi=0; //INDICE DU TABLEAU DICO
  size_t indDj=0; //INDICE DU TABLEAU DICO
  int match=0;  //MATCH DES BALISES OUVRANTE
  int matchf=0; //MATCH DES BALISES FERMANTE
  size_t cptLigne=1; //compte les lignes du fichier original en fonction du caractere entre
  //compte le nombre de balises ouvrante
  size_t indPi=0;
  size_t indPj=0;
  int rappelfgbg;
  int indParPilOu=0;
  int indParPilFe=0;
  

  pile=new int*[cptBal];
  cout<<"cptBal= "<<cptBal<<endl;
  for(indPi=0;indPi<cptBal;indPi++)
  { 
    pile[indPi]=new int[4];
  }

  indPi=0;
  for(indPi=0;indPi<cptBal;indPi++)//ON INITIALISE LE TABLEAU DE PILE AVEC QUE DES -1;
  {
    for(indPj=0;indPj<4;indPj++)
    { 
      pile[indPi][indPj]=-1;
    }
  }

  cout<<"piledeb:"<<endl;
       //j'affice la pile
       cout<<"indPi="<<indPi<<endl;
       for(size_t ip=0;ip<cptBal;ip++)
       {for(size_t jp=0;jp<4;jp++)
        {
         cout<<pile[ip][jp]<<" ";
        }
        cout<<endl;
       }

 indPi=0;
 indPj=0;  

  cout<<"fTaille="<<fTaille<<endl;
 while(i<fTaille)
  { 
     

   if(tabFichierSN[i]=='<') //CAS ON TROUVE UNE BALISE
 	  {    
 	   i++;
 	   if(tabFichierSN[i]=='/')//CAS OU ON TROUVE UNE BALISE FERMANTE
 	    { cout<<"pilef:"<<endl;
        cout<<"indPi="<<indPi<<endl;
       //j'affiche la pile
       for(size_t ip=0;ip<cptBal;ip++)
       {for(size_t jp=0;jp<4;jp++)
        {
         cout<<pile[ip][jp]<<" ";
        }
        cout<<endl;
       }
        i++;
       cout<<"balise fermante<-------------------------------------------"<<endl;
       while((indDi<23)&&(matchf==0))//ON ARRETE SI ON MATCH OU SI ON DEPASSE LA TAILLE DU DICO
       {cout<<" indDj="<<indDj<<" indDi="<<indDi<<" i="<<i<<" tabFichier= "<<tabFichierSN[i]<<" tabDico= "<<tabDico[indDi][indDj]<<endl;
         //cout<<" indDj="<<indDj<<" indDi="<<indDi<<" i="<<i<<" tabFichier= "<<tabFichierSN[i]<<" tabDico= "<<tabDico[indDi][indDj]<<endl;
         
         //ON ARETE SI ON EST A LA FIN DUNE
         //LIGNE DANS LE DICO ON CHERCHE LIGNE PAR LIGNE                                            
         //PUIS ON CHERCHE A LA LIGNE SUIVANTE
                     
         if(((tabDico[indDi][indDj]=='\0')&&(tabFichierSN[i]=='>'))||((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]=='>'))) //CAS DE MATCH AVEC CHEVRONS
         {
           cout<<"matchf"<<endl;
           matchf=1;
           pile[indPi][0]=indDi; pile[indPi][3]=cptLigne;

              //j'affice la pile
           cout<<"pile"<<endl;
           cout<<"indPi="<<indPi<<endl;
           for(size_t ip=0;ip<cptBal;ip++)
            {for(size_t jp=0;jp<4;jp++)
              {
               cout<<pile[ip][jp]<<" ";
              }
              cout<<endl;
            }
           
             //ON VERIFIE LA PILE DE TEL SORTE QUE SI (pile[indPi-1]==pile[indPi]) LA BALISE
             //CORRESPOND BIEN A LA BALISES OUVRANTE TROUVÉE JUST AVANT LA BALISE FERMANTE ACUTELLEMENT
             //TRAITÉ
           if((pile[indPi-1][0])<0)
            {
              cout<<"fatal error : premiere balises est une balises fermante, abandon total"<<endl;
              i=fTaille;
              break;
            }
           
           else if(pile[indPi-1][0]==pile[indPi][0])
            {//ICI JE SUIS SENSÉ ECRIRE TOUT LE TEXTE BRUTE CUMULE DANS UN STRING EN FUNCTION DE PILE
             texteB+="\e[0m";
             ecriTexteB<<texteB;
             texteB="";
             
             while(indParPilFe<indParPilOu) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
              {
               if(pile[indParPilFe][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
               { 
                 if(pile[indParPilFe][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
                  {
                   texteB+="\\e[3"+to_string(pile[indParPilFe][1])+"m";
                   cout<<"dans </>"<<pile[indParPilFe][1]<<endl;
                   cout<<"indPilFe="<<indParPilFe<<endl;
                  }  
                
                 if(pile[indParPilFe][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
                  {
                   texteB+="\\e[4"+to_string(pile[indParPilFe][2])+"m";
                   cout<<"dans </>"<<pile[indParPilFe][2]<<endl;
                  }
                   indParPilFe++;
                }
               indParPilFe++;
              } 
  
             indParPilFe=0;

             cout<<"balises ouvrante coincide avec fermante"<<endl;
         
             pile[indPi-1][0]=-1; pile[indPi-1][1]=-1; pile[indPi-1][2]=-1; pile[indPi-1][3]=-1; 

             pile[indPi][0]=-1; pile[indPi][1]=-1; pile[indPi][2]=-1; pile[indPi][3]=-1; 
         
             indPi--;
             indParPilOu--;
             i++;

             break;
            }
           
           else 
            { erreurB=1;
              cout<<"Balise fermante qui ne conside pas avec une balise ouvrante correctement a la ligne: " \
                  <<cptLigne<<endl;
              i=fTaille;
              break;
            }
          }
           
         else if(tabDico[indDi][indDj]==tabFichierSN[i]) //CAS OU ON COMPARE CHAQUE LETRE DU DICO AVEC LE FICHIER DE TEXTE A BALISE
         { 
           indDj++;
           i++;
         }


         else if(tabDico[indDi][indDj]!=tabFichierSN[i])
         { i-=indDj;
           indDi++;
           indDj=0;
         } 
        }
       if(matchf==0)//CAS OU IL Y A PAS MATCHf AVIS D'EUREUR !
        { 
         cout<<"Erreur a la "<<cptLigne<<"eme ligne du fichier "<<fichier<<endl;
         erreurB=1;
         while(tabFichierSN[i]!='>')
         {
           i++;
           if(i>=fTaille||tabFichierSN[i]=='<')
            { i=fTaille;
             cout<<"A la ligne: "<<cptLigne<<" Fatal error balise ouvrante no fermé====================!=!=!=!=!=!=!=!=!=!=!=!-!!"<<endl;
            }
           break;
         }
         i++;
        }

 	    }
     
     else if(tabFichierSN[i]!='/') //CAS OU ON TROUVE UNE BALISE OUVRANTE
 	    { cout<<"pile:"<<endl;
        //j'affice la pile
       for(size_t ip=0;ip<cptBal;ip++)
       {for(size_t jp=0;jp<4;jp++)
        {
         cout<<pile[ip][jp]<<" ";
        }
        cout<<endl;
       }

        cout<<"balise ouvrante<-------------------------------------------"<<endl;
 	     while((indDi<23)&&(match==0))//ON ARRETE SI ON MATCH OU SI ON DEPASSE LA TAILLE DU DICO !
 	     {
 	       
         //ON ARETE SI ON ATEsize_t LA FIN DUNE
 	       //LIGNE DANS LE DICO ON CHERCHE LIGNE PAR LIGNE										                        
 	       //PUIS ON CHERCHE A LA LIGNE SUIVANTE
         	         
         if((tabDico[indDi][indDj]=='\0')&&(tabFichierSN[i]=='>')) //CAS DE MATCH AVEC CHEVRONS
 	       {
           cout<<"Match"<<endl;
 	 	     	 match=1;
           pile[indPi][0]=indDi; 
           pile[indPi][3]=cptLigne;
           indPi++;
           i++;

           while(pile[indParPilOu][0]!=-1) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
           {
             if(pile[indParPilOu][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
             { if(pile[indParPilOu][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
               {
                texteB+="\\e[3"+to_string(pile[indParPilOu][1])+"m";
                cout<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<endl;
                cout<<texteB<<endl;
               }
 
               if(pile[indParPilOu][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
               {
                texteB+="\\e[4"+to_string(pile[indParPilOu][2])+"m";
                cout<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<endl;
               }
               cout<<texteB<<endl;
               indParPilOu++;
             }
             indParPilOu++;
           } 
           //indParPilOu=0;
 	       }
    	  	 
         else if(tabDico[indDi][indDj]==tabFichierSN[i]) //CAS OU ON COMPARE CHAQUE LETRE DU DICO AVEC LE FICHIER DE TEXTE A BALISE
  	     { cout<<" indDj="<<indDj<<" indDi="<<indDi<<" i="<<i<<" tabFichier= "<<tabFichierSN[i]<<" tabDico= "<<tabDico[indDi][indDj]<<endl;

 	    	   indDj++;
 	    	   i++;
           if((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]==' ')) //CAS A LA POSITION '_' où <color_fg=..> 
            {
             indDi=0;
             indDj=0;
             i++;
            }
           else if((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]=='>')) //CAS <color>
            {cout<<"Warning: balises a la ligne "<<cptLigne<<" sans parametre"<<endl;
             i++;
             pile[indPi][0]=18; pile[indPi][3]=cptLigne;
             indPi++;
             indParPilOu++;
            }
           else if((tabDico[indDi][indDj]=='~')&&(tabFichierSN[i]!='>')) //CAS DE LA POSITION '_ ' OÙ <color fg=_..
            {rappelfgbg=indDi-19; //ON SE RAPPEL POUR LA SUITE 
             indDi=0;             //DE L'OPTION (FG OU BG) DANS LAQUELLE ON SE TROUVE
             indDj=0;             //POUR APRES QUAND ON CONNAITRA LA COULEUR, POUVOIR L'ATRIBUÉ AU BON PARAM FG OU BG
             cout<<"rappel dans 1er param= "<<rappelfgbg<<endl;
            }

            else if((tabDico[indDi][indDj]=='-')&&(tabFichierSN[i]==' ')) //<color fg=bleu_bg=red  où _ est le curseur sur ' '
            { 
             
             coulCod=indDi;
             cout<<"de couleurs "<<coulCod<<" rappelfgbg="<<rappelfgbg<<endl;
             pile[indPi][0]=18; pile[indPi][rappelfgbg]=indDi; pile[indPi][3]=cptLigne;
             
             i++;
              indDi=0;
              indDj=0;  

            }

           else if((tabDico[indDi][indDj]=='-')&&(tabFichierSN[i]=='>')) //<color fg=bleu_   où _ est le curseur sur >
            { 
             match=1;
             coulCod=indDi;
             cout<<"de couleur "<<coulCod<<endl;
             pile[indPi][0]=18; pile[indPi][rappelfgbg]=indDi; pile[indPi][3]=cptLigne;
             indPi++;
             i++;
              while(pile[indParPilOu][0]!=-1) //ON AJOUTE TOUTE LES CHARACTERE D'ECHAPPEMENT DANS LE STRING DE CHARACTERE
             {
              if(pile[indParPilOu][0]==18) //SI ON TROUVE UN BALISE OUVRANTE COULEUR DANS LA PILE
              { if(pile[indParPilOu][1]!=-1) //ON GERE LA COULEUR DE FOREGROUND
                {
                 texteB+="\\e[3"+to_string(pile[indParPilOu][1])+"m";
                 cout<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<endl;
                 cout<<texteB<<endl;
                }
 
               if(pile[indParPilOu][2]!=-1) //ON GERE LA COULEUR DE BACKGROUND
               {
                 texteB+="\\e[4"+to_string(pile[indParPilOu][2])+"m";
                 cout<<"je boucle dans char ECHAPPEMENT=x=x=x=x=x=x=x=x=s=s=s=s=s=s=d=d=d=d=d=+D+D+D+D+D+D"<<endl;
                }
                cout<<texteB<<endl;
               indParPilOu++;
              }
             
             indParPilOu++;
             } 
            }
         }


 	       else if(tabDico[indDi][indDj]!=tabFichierSN[i])
 	    	 { i-=indDj;
 	    	 	 indDi++;
           indDj=0;
 	       } 
   	   }
       if(match==0)//CAS OU IL Y A PAS MATCH AVIS D'EUREUR !
     	  { 
     	   cout<<"Erreur a la "<<cptLigne<<"eme ligne du fichier "<<fichier<<endl;
         erreurB=1;
         while(tabFichierSN[i]!='>')
         {
          i++;
          if(i>=fTaille||tabFichierSN[i]=='<')
          { i=fTaille;
            cout<<"A la ligne"<<cptLigne<<" Fatal error balise ouvrante no fermé====================!=!=!=!=!=!=!=!=!=!=!=!!!"<<endl;
          }
          break;
         }
         i++;
     	  }
 	    }
     indDj=0;
     indDi=0;
     match=0;
     matchf=0;
 	  }	//FIN DE CAS QUAND ON TROUVE UNE BALISE,

   /*ON SE TROUVE PAR LA SUITE DANS LE CAS OU ON TROUVE DU TEXTE BRUTE QU'ON VIENDRA SAUVEGARDÉ
     DANS UNE VARIABLE STRING EN FONCTION DES ESPACES QU'ON TROUVE OU PAS (PLUS D'UN ESPACE LUE A LA 
     SUITE L'UN DE L'AUTRE IGNORÉ, UN ENTRE = ESPACE ; DEUX ENTRÉ = ENTRÉ)*/
   

   else  //SI ON TOMBE SUR DU TEXTE BRUTE 
   { 
     if(tabFichierSN[i]=='\n')
      {
       
       cptLigne++; 
      } 
     //texteB+=tabFichierSN;
      cout<<endl;
    
     if((tabFichierSN[i]==' ')||(tabFichierSN[i]=='\n')||(tabFichierSN[i]=='\t')) //SI ON TROUVE UN ESPACE OU ENTRER OU TAB
      {  
          
         if((nbEspace==0)&&(tabFichierSN[i]==' '))
        {
         texteB+=' ';
         i++;
         nbEspace++;
        }  
        else if((nbEntre==1)&&(tabFichierSN[i]=='\n')) 
        {
         //tabFichier[i-1]='\n';
         texteB.pop_back();
         texteB+='\n';
         nbEntre++;
         i++;
         nbEspace--;
         //cout<<"je boucle en entre ecrit";
        } 
        else if((nbEntre==0)&&(tabFichierSN[i]=='\n')&&(nbEspace==0))
        {
         texteB+=' ';
         nbEspace++;
         nbEntre++;
         i++;
         //cout<<"j'ecrit espace";
        }  
        else if((nbEntre==0)&&(tabFichierSN[i]=='\n')&&(nbEspace==0))
        {//cout<<"je boucle en entre";
         texteB+=' ';
         nbEspace++;
         i++;
        }
        else if((tabFichierSN[i]=='\t')&&(nbEspace==0))
        {
         texteB+=' ';
         i++;
         nbEspace++;
        }
        else
        {
         i++;
        }

      }
     
     else 
      {
        texteB+=tabFichierSN[i];
        nbEntre=0;
        nbEspace=0;
        i++;
      } 
   }
  
   cout<<" i="<<i<<" indDj="<<indDj<<" indDi="<<indDi<<"tabFichier="<<tabFichierSN[i]<<endl;
  }
 // cout<<"le texte brute donne donc ="<<texteB<<"<---"<<endl;
 // texteB.pop_back();
 // cout<<"le texte brute donne donc ="<<texteB<<"<---"<<endl;
 !erreurB ? cout<<"les balices coincides parfaitement"<<endl :cout<<"erreur votre fichier text contient des balises non recconue"<<endl;
  
  if (pile[0][0]!=-1)
  { size_t iErr=0;
    cout<<"erreur fatal la/les balises ";
    while((pile[iErr][0]!=-1)&&(iErr<cptBal))
    {
      cout<<'<'<<tabDico[pile[iErr][0]]<<'>'<<" ";
      
  
     cout<<" ligne: ";
    
    
      cout<<pile[iErr][3]<<" ";
      iErr++;
      cout<<endl;
    }
    cout<<"ne possede pas de balise fermante associé."<<endl;

  }
  cout<<"au total il y a "<<cptBal<<"balises"<<endl;
  

}

void recupFichier::afficheConsole()
{  size_t tailleduTextB;
   size_t itB=0;
  unsigned char ch;
  string texteBaff="";
  
  
  
  ifstream liFichiereB; //on lit le fichier dans lequel on a ecrit le texteB
  liFichiereB.open("ecritureB.txt",ios::in|ios::binary);

  liFichiereB.seekg(0,ios::end);
  tailleduTextB=liFichiereB.tellg();
  liFichiereB.seekg(0,ios::beg);  

  if(liFichiereB.is_open())
  {
    while(itB<tailleduTextB)
    {
      ch=liFichiereB.get();
     // cout<<ch<<endl;
      texteBaff+=ch;
      itB++;
    }
    cout<<"la tailleB="<<tailleduTextB<<endl;
    cout<<"voici le fichier texte coloré"<<endl;
    cout<<texteBaff<<endl; 
    cout<<"boombitch"<<endl;
  }
}


recupFichier::recupFichier(char* fichierParm)
{ fichier=fichierParm;
  ecriTexteB.open("ecritureB.txt",ios::out|ios::binary);
	getTaille(); //OBTIENT LA TAILLE DU FICHIER EN OCTET
	chargeDico(); //CHARGE LE DICTIONAIRE DE BALISES EN MEMORIE DANS UN TAB STATIQUE
  chargeFichier();//CHARGE LE FICHIER TEXTE AVEC BALISES AVEC ESPACES;
	verifieBalises(); 
  ecriTexteB.close();
  afficheConsole(); //AFFICHE LE FICHIER COLORE DANS LA CONSOLE
  //fNettoie(); //NETTOIE LE FICHIER DE TEXTE A BALISE EN MEMOIRE;
  
 
}

int main(int argc, char** argv)
{ cout<<"sizeof(uchar)="<<sizeof(unsigned char)<<endl;
  
  recupFichier F(argv[1]);

  return 0;
}
