#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include <stdlib.h> 
#include <fstream>
#include "boite.h"
#include "cursor.h"

using namespace std;


//~Boite();

size_t Boite::getWidth() const{
  return _width;
}

size_t Boite::getHeight() const{
  return _height;
}

void Boite::afficherBoite(){
	
  //Cursor cur;
  cur.clear();
  cur.write("\0");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("\0");
  }
  cur.write("\0");
  cur.endl();
  for(size_t i=1;i<this->getHeight()-5;i++){
    cur.write("\0");
    cur.endl();
  }
  //cur.endl();
  cur.move_to(this->getWidth()-1,1);
  for(size_t i=1;i<this->getHeight()-4;i++){
    cur.write("\0");
    cur.endl();
    cur.move_to(this->getWidth()-1,i);

  }
  cur.move_to(0,this->getHeight()-5);
  cur.write("\0");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("\0");
  }
  cur.write("\0");

	
  cur.endl();	

}

void Boite::LigneSeparateur(){
  cur.move_to(0,lig);
  cur.write("C");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("d");
  }
  cur.write("D");
  cur.endl();
  cur.right(1);
  cur.move_to(0,this->getHeight()+1);
  lig++;
  
}

void Boite::afficheText(std::string S, size_t x, size_t y){
  cur.move_to(y,x);
  cur.write(S);
  cur.move_to(0,this->getHeight());

}

void Boite::origine(){
  cur.move_to(0,0);

}

void Boite::fin(){
  cur.move_to(0,getHeight()+1);
}

void Boite::tailleCons()
{
   struct winsize w;
   ioctl(0,TIOCGWINSZ, &w);
   _width= w.ws_col;
   _height=w.ws_row;
}




void Boite::init(){
  struct winsize w;
  
  ioctl(0,TIOCGWINSZ, &w);
  _width= w.ws_col;
  _height=w.ws_row;
  afficherBoite();

  
   
  lig=2;
  nbLigLibre=getHeight()-5;
}


void Boite::afficheMoi(string S)
{  ofstream fichCoord;
   fichCoord.open("traceCord.txt",ios::out|ios::binary|ios::app);
   if(fichCoord.is_open())
   {
   	fichCoord<<"jaffiche avec mot :"<<'\n'<<S<<"\n\n";
   }
   fichCoord.close();
 
 //cout<<' ';
 cout<<S;

 //cout<<endl;
}

void Boite::initDel()
{  for (size_t i=1;i<_width;i++) //DELIMITEUR HAUT
  {

    cur.deplaceVer(1,i);
    if(i==1)
  	cout<<"*";
   
   cout<<"-";
  }
  cout<<'*';


 for(size_t i=2;i<_height-5;i++) //DELIMITEUR COTÉ
   {
     cur.deplaceVer(i,1);
     cout<<'|';
	 cur.deplaceVer(i,_width);
	 cout<<'|';
    }
 cur.deplaceVer(_height-5,0);
 cout<<'*';
 
 for(size_t i=0;i<_width-2;i++) //DELIMITEUR BAS
    {
  	 cout<<"-";
    }
 cout<<'*';
 //cur.deplaceVer(_height,1);
  
  //cur.deplaceVer(2,1);
    //cout<<'J';
 
 //cout<<endl;
}

void Boite::reset()
{
  cur.deplaceVer(_height,1);
}

//AfficheG OK-----
void Boite::afficheG(string T)
{ lig=4;
   //on cpteur de retour a la ligne utilisé;
  ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary|ios::app);
  fichierE<<"getHeight="<<_height<<" getWidth="<<_width<<'\n';
  int pageS;
  size_t plEsp=0;
  //size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(cptR==1)
   {fichierE<<"j'affiche un retour a la ligne car cptR==1"<<'\n';
   	cout<<'\n';
   	cptR=0;
   }
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.length();i++)
	 {
	  fichierE<<"T["<<i<<"]="<<T[i]<<endl;
	  fichierE<<"nbLigLibre="<<nbLigLibre<<endl;
	  
	  if(nbLigLibre==0)
	  { cout<<"\e[0m";
	  	fichierE<<"je boucle dans change pages"<<'\n';
	  	initDel();
	  	
	    afficheMoi("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?");
	    
	    std::cin>>pageS;
	    //init();
	    cur.clear();
	    cout<<"\n";
	    //cout<<" ";
	    afficheMoi(style);
	    nbLigLibre=_height-7;
	  }
 
	  if(i!=0)
	    {
	     if(T[i]=='\e' )
		    {
		     escape++;
	        }
      
	     if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
	     	escape--;
      
	     if(escape)
		  {
	         mot+=T[i];
		     style+=T[i];
	      }
	    }
      
	  if((!escape))
	    {
	     
	     asciCode=T[i];
	     
	     fichierE<<"asciCode="<<asciCode<<endl;
	     if(asciCode==195 || asciCode==197 || asciCode==4294967235 || asciCode==4294967237)
	     	{fichierE<<"je boucle dans accent"<<endl;
	     	 COUNT--; //ON GERE LES CARACTERE ACCENTUÉ 
	     	}

	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape)
	  { cout<<' ';

	    afficheMoi(mot);
	    //cout<<endl;
	    mot="";
	    
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	  

	  plEsp=0;
    while(true && !escape)
      {
       if((T[i+plEsp+1]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp+1]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp+1]==' '||T[i+plEsp+1]=='\0')
          {break;}       
       
       else if (T[i+plEsp+1]=='\e')
          {break;}
       
       else
        {plEsp++;}
     } 
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>_width-3&&!escape)
	    { 
	     fichierE<<"j'afficheText ligne="<<lig<<" colone="<<2<<'\n';
	      cout<<" ";
	      afficheMoi(mot);
	      cout<<endl;
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      
	      //nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	 }
	  if(!mot.empty())
	  {  
	  	cout<<" ";
        afficheMoi(mot);
    	lig++;
    	nbLigLibre--;
    	cout<<endl;
       }
	      
      
        
      fichierE.close();
      
    }
}





void Boite::afficheD(std::string T)
{ ofstream fichierE;
  fichierE.open("traceD.txt",ios::out|ios::binary|ios::app);
  int pageS;
  size_t plEsp=0;
 // size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t i;
  size_t COUNT=0;
  string mot="";
  string style="";

  if(cptR==1)
   {fichierE<<"j'affiche un retour a la ligne car cptR==1"<<'\n';
   	cout<<'\n';
   	cptR=0;
   }

  if(fichierE.is_open())
    { fichierE<<"heigth="<<_height<<" width="<<_width<<endl;
     for(i=0;i<T.length();i++)
	    {
	     
	     if(nbLigLibre==0)
	       { cout<<"\e[0m";
	       	 fichierE<<"je boucle dans change pages"<<'\n';
	  		 initDel();
	         afficheMoi("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?");
	         std::cin>>pageS;
	         cur.clear();
	         cout<<"\n";
	         
	         afficheMoi(style);
	         nbLigLibre=_height-7;
	        }
   
         
         if(i!=0)
	        {
	         if(T[i]=='\e' )
	 		    {
			     escape++;
			    }
      
	         if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		      	escape--;
      
	         if(escape)
			   {
			     mot+=T[i];
			     style+=T[i];
			    }
	        }
         

         if((!escape))
	       { asciCode=T[i];
	  	     
	    	 if(asciCode==195 || asciCode==197 || asciCode==4294967235 || asciCode==4294967237)
	     	  {
	     	    fichierE<<"je boucle dans accent"<<endl;
	     	    COUNT--; //ON GERE LES CARACTERE ACCENTUÉ 
	     	  }
 			 COUNT++;
	         mot+=T[i];
	        }

	        //J'AFFICHE SI JE TROUVE UUNE ENTRE DANS LE STRING T
         if((T[i]=='\n')&&!escape)
     	    {
     	     fichierE<<"j'afficheText dans entree ligne="<<lig<<" colone="<<_width-COUNT<<'\n';
	 	     
     	     afficheEsp(_width-COUNT);
	         afficheMoi(mot);
	         mot="";
	     	 
	         lig++;
	         nbLigLibre--;
	         COUNT=0;
	         
	    	}

         /*plEsp=0;
	     while((T[i+plEsp+1+1]!=' ')&&(T[i+plEsp+1+1]!='\e'))
	     { plEsp++;}*/

	     plEsp=0;
         while(true && !escape)
            { 

         	 if((T[i+plEsp+1]==' '))
            	{
             	 fichierE<<"je marche avec espace"<<'\n';
                }

         	 else if(((T[i+plEsp+1]=='\e')))
                {
                 fichierE<<"je marche avec e"<<'\n';
                }
  
 	         if(T[i+plEsp+1]==' '||T[i+plEsp+1]=='\0')
             {fichierE<<"je break"<<'\n';
             	break;}       
       
             else if (T[i+plEsp+1]=='\e')
             {break;}
       
             else
             {plEsp++;}
           } 
    
	     fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n'; 
		 fichierE<<"mot="<<mot<<"\n\n";
		 fichierE<<"T["<<i<<"]="<<T[i]<<' ';
		 
		 fichierE<<"asciCode="<<asciCode<<endl;
         
        /* if((mot[i]=='\0') && (i>0))
 			{
 			 fichierE<<"je decremente COUNT car j'ai trouvé un null"<<endl;
 		     COUNT--;
 		    }
        */
		 if(COUNT+plEsp>_width-3&&!escape)
	  	   { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<_width-COUNT<<'\n';
	 	     
	 	     afficheEsp(_width-COUNT);
	 	     afficheMoi(mot);
	 	     COUNT=0;
	  	     lig++;
	  	     nbLigLibre--;
	   	     mot="";
	   	     
	         // nbColLibre==getWidth()-3;
	         plEsp=0;
           }
      
	   }
	   
     if(!mot.empty())
       { fichierE<<"j'affiche le mot dans la derniere boucle avec mot:"<<endl;
    	 fichierE<<mot<<endl;
    	 fichierE<<"a la col="<<_width-COUNT;

       	if(mot.back()!=' ')
       	{
       	  fichierE<<"motBack="<<mot.back();
       	  COUNT++;
       	}

       	afficheEsp(_width-COUNT);
       	afficheMoi(mot);
       	lig++;
       	nbLigLibre--;
       	cout<<endl;
       }
	 //fin();
     
   
     fichierE.close();
    }
}

void Boite::afficheC(string T)
{ ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary);
 
  size_t plEsp=0;
 // size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.size();i++)
	{
	  if(nbLigLibre==3){
	    afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
	    int pageS;
	    std::cin>>pageS;
	    nbLigLibre=getHeight()-5;
	    init();
	    afficheText(style,lig,col);

	  }

   
	  if(i!=0)
	    {
	      if(T[i]=='\e' )
		{
		  escape++;
		}
      
	      if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		escape--;
      
	      if(escape)
		{
		  mot+=T[i];
		  style+=T[i];
		}
	    }
      
	  if((!escape))
	    {
	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape){
	    afficheText(mot.substr(0,mot.size()-1),lig,(getWidth()-COUNT)/2+1);
	    mot="";
	    
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	  /* plEsp=0;
	  while((T[i+plEsp+1+1]!=' ')&&(T[i+plEsp+1+1]!='\e'))
	  { plEsp++;}*/
	  plEsp=0;
    while(true && !escape)
      {/* fichierE<<"T[i+plEsp+1]="<<T[i+plEsp+1];
       if(i>3)
        {
          fichierE<<" T[i+plEsp+1-3]="<<T[i+plEsp+1-3]<<'\n';
        }
       else
        {fichierE<<'\n';}*/

       if((T[i+plEsp+1]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp+1]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp+1]==' ')
          {break;}       
       
       else if (T[i+plEsp+1]=='\e')
          {break;}
       
       else
        {plEsp++;}
     } 
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>getWidth()-3&&(T[i+plEsp+1-2]!='['||T[i+plEsp+1-3]!='['))
	    { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<col<<'\n';
	      afficheText(mot.substr(0,mot.size()-1),lig,(getWidth()-COUNT)/2+1);
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      
	      //nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	}

      	      afficheText(mot.substr(0,mot.size()-1),lig,(getWidth()-COUNT)/2+1);

	      //fin();
      
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}

void Boite::afficheJ(string T)
{ ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary);
  size_t plEsp=0;
  //size_t nbColLibre=getWidth()-3;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  size_t nbMot=0;
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.size();i++)
	{
	  if(nbLigLibre==3){
	    afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
	    int pageS;
	    std::cin>>pageS;
	    nbLigLibre=getHeight()-5;
	    init();
	    afficheText(style,lig,col);

	  }

   
	  if(i!=0)
	    {
	      if(T[i]=='\e' )
		{
		  escape++;
		}
      
	      if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		escape--;
      
	      if(escape)
		{
		  mot+=T[i];
		  style+=T[i];
		}
	    }
      
	  if((!escape))
	    {
	      COUNT++;
	      mot+=T[i];
	    }


	  if((T[i]=='\n')&&!escape){
	    afficheText(mot,lig,2);
	    mot="";
	    
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	    nbMot=0;
	  }
     
     
	  /*plEsp=0;
	  while((T[i+plEsp+1+1]!=' ')&&(T[i+plEsp+1+1]!='\e'))
	  { plEsp++;}*/
	  plEsp=0;
    while(!escape)
      {/* fichierE<<"T[i+plEsp+1]="<<T[i+plEsp+1];
       if(i>3)
        {
          fichierE<<" T[i+plEsp+1-3]="<<T[i+plEsp+1-3]<<'\n';
        }
       else
        {fichierE<<'\n';}*/

       if((T[i+plEsp+1]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp+1]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp+1]==' ')
          {break;}       
       
       else if (T[i+plEsp+1]=='\e')
          {break;}
       
       else
        {plEsp++;}
     } 

	  
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>getWidth()-3&&(T[i+plEsp+1-2]!='['||T[i+plEsp+1-3]!='['))
	    { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<col<<'\n';

	      size_t nbEsp=getWidth()-3-COUNT;
	      nbMot=0;
	      
	      for(size_t j=0;j<getWidth();j++)
		{
	 
		  if(T[j]==' ')
		    nbMot++;
		}

	      string NouvMot="";
	      string esp=" ";

	      float nbMotLigne=0;
	      for(size_t k=0;k<getWidth();k++)
		{
		  if(mot[k]==' ')
		    nbMotLigne++;
		}

   
	        for(size_t k=0;k<getWidth()-3;k++)
		{
		  if(mot[k]!=' ')
		    NouvMot.push_back(mot[k]);
		  else{
		    for(int l=0;l<(nbMotLigne/nbEsp)-1;l++){
		      NouvMot.push_back(' ');
		      plEsp++;
		    }
		  }
		}
		nbMotLigne=0;
	      
	      afficheText(NouvMot,lig,2);
	      COUNT=0;
	      nbMot=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      
	     // nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	}
      afficheText(mot,lig,2);

	      //fin();
      
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}



void Boite::clearA()
{
  system("clear");
}

//POR SI ACASO

void SetCursorPos(int XPos, int YPos)
{
 printf("\033[%d;%dH", YPos+1, XPos+1);
}

void Boite::afficheEsp(unsigned int j)
{
  for( size_t i=0;i<j;i++)
  {
  	cout<<' ';
  }
}


Boite::Boite(size_t w, size_t h) : _width(w), _height(h), cur(0,0) {}

Boite::Boite():cur(0,0)
{	cur.clear();
	lig=3;col=3;
	//std::cout << "\033[2J\033[1;1f";
	tailleCons();
	cptR=1;
	nbLigLibre=_height-7;
	//initDel();
//cout<<"je marche"<<endl;
	//afficheMoi("salut ceci est une facon de fking afficher un texte",3,3);
}