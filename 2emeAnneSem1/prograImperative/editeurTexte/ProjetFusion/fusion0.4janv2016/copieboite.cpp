#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include <stdlib.h> 
#include "boite.h"
#include "cursor.h"

using namespace std;

Boite::Boite(size_t w, size_t h) : _width(w), _height(h), cur(0,0) {}
Boite::Boite():cur(0,0){}
//~Boite();

size_t Boite::getWidth() const{
  return _width;
}

size_t Boite::getHeight() const{
  return _height;
}

void Boite::afficherBoite(){
	
  //Cursor cur;
  cur.clear();
  cur.write("A");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("c");
  }
  cur.write("B");
  cur.endl();
  for(size_t i=1;i<this->getHeight()-5;i++){
    cur.write("a");
    cur.endl();
  }
  //cur.endl();
  cur.move_to(this->getWidth()-1,1);
  for(size_t i=1;i<this->getHeight()-4;i++){
    cur.write("f");
    cur.endl();
    cur.move_to(this->getWidth()-1,i);

  }
  cur.move_to(0,this->getHeight()-5);
  cur.write("E");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("e");
  }
  cur.write("F");

	
  cur.endl();	

}

void Boite::LigneSeparateur(){
  cur.move_to(0,lig);
  cur.write("C");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("d");
  }
  cur.write("D");
  cur.endl();
  cur.right(1);
  cur.move_to(0,this->getHeight()+1);
  lig++;
  col=2;
}

void Boite::afficheText(std::string S, size_t x, size_t y){
  cur.move_to(y,x);
  cur.write(S);
  cur.move_to(0,this->getHeight());

}

void Boite::origine(){
  cur.move_to(0,0);

}

void Boite::fin(){
  cur.move_to(0,getHeight()+1);
}





void Boite::init(){
  struct winsize w;
  ioctl(0,TIOCGWINSZ, &w);
  _width= w.ws_col;
  _height=w.ws_row;
  afficherBoite();

  
  col=2; 
  lig=3;

}


//AfficheG OK-----
void Boite::afficheG(string T)
{
  size_t nbColLibre=getWidth()-3;
  size_t nbLigLibre=getHeight()-5;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  for(size_t i=0;i<=T.size();i++){
    if(nbLigLibre==3){
      afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
      int pageS;
      std::cin>>pageS;
      nbLigLibre=getHeight()-5;
      init();
      afficheText(style,0,0);

    }

   
    if(i!=0){
      if(T[i]=='\e' )
	{
	  escape++;
	}
      
      if(T[i-1]=='m' && escape)
	escape--;
      
      if(escape){
	mot+=T[i];
	style+=T[i];
      }
   
    }
      
    if((!escape)){
      COUNT++;
      mot+=T[i];
    }

    if((T[i]=='\n')&&!escape){
      afficheText(mot,lig,col);
      mot="";
      col=2;
      lig++;
      nbLigLibre--;
      nbColLibre=getWidth()-3;
      COUNT=0;
    }


   
    if((T[i]==' ' || i==T.size())){
      if(nbColLibre<COUNT){
	lig++;
	nbLigLibre--;
	col=2;
	afficheText(mot,lig,col);
	mot="";
	col+=COUNT;
	nbColLibre=getWidth()-3-COUNT;
	COUNT=0;
      }
      else{
	afficheText(mot,lig,col);
	nbColLibre-=COUNT;
	mot="";
	col+=COUNT;
	COUNT=0;
      }
    }

    fin();
  }
  col=2;
  lig++;
  nbLigLibre--;

}


size_t Boite::decale(string T, size_t nblig)
{
  
  for(size_t i=0;i<T.size();i++){
    size_t pos=getWidth()-3;
    while(T[pos]!=' ')
      pos--;
  
    
    return getWidth()-pos;
  }
  return 0;
}


void Boite::afficheD(std::string T)
{
  size_t nbColLibre=getWidth()-3-decale(T,lig);
  size_t nbLigLibre=getHeight()-5;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  col=2+decale(T,lig);
  
  for(size_t i=0;i<=T.size();i++){
    if(nbLigLibre==3){
      afficheText("\e[0m plus de place,  passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
      //afficheText(style,0,0);
      int pageS;
      std::cin>>pageS;
      nbLigLibre=getHeight()-5;
      init();
      afficheText(style,0,0);

    }

    if(i!=0){
      if(T[i]=='\e' )
	{
	  escape++;
	}
      
      if(T[i-1]=='m' && escape)
	escape--;
      
      if(escape){
	mot+=T[i];
	style+=T[i];
      }
   
    }
      
    if((!escape)){
      COUNT++;
      mot+=T[i];
    }

    if((T[i]=='\n')&&!escape){
      afficheText(mot,lig,col);
      mot="";
      col=2+decale(T,lig);
      lig++;
      nbLigLibre--;
      nbColLibre=getWidth()-3;
      COUNT=0;
    }



   
    

    fin();
  }
  col=2;
  lig++;
  nbLigLibre--;

}




void Boite::clearA()
{
  system("clear");
}
