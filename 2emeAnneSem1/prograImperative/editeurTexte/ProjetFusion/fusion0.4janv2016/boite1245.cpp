#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include <stdlib.h> 
#include <fstream>
#include "boite.h"
#include "cursor.h"

using namespace std;

Boite::Boite(size_t w, size_t h) : _width(w), _height(h), cur(0,0) {}
Boite::Boite():cur(0,0){}
//~Boite();

size_t Boite::getWidth() const{
  return _width;
}

size_t Boite::getHeight() const{
  return _height;
}

void Boite::afficherBoite(){
	
  //Cursor cur;
  cur.clear();
  cur.write("A");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("c");
  }
  cur.write("B");
  cur.endl();
  for(size_t i=1;i<this->getHeight()-5;i++){
    cur.write("|");
    cur.endl();
  }
  //cur.endl();
  cur.move_to(this->getWidth()-1,1);
  for(size_t i=1;i<this->getHeight()-4;i++){
    cur.write("f");
    cur.endl();
    cur.move_to(this->getWidth()-1,i);

  }
  cur.move_to(0,this->getHeight()-5);
  cur.write("E");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("e");
  }
  cur.write("F");

	
  cur.endl();	

}

void Boite::LigneSeparateur(){
  cur.move_to(0,lig);
  cur.write("C");
  for(size_t i=1;i<this->getWidth()-1;i++){
    cur.write("d");
  }
  cur.write("D");
  cur.endl();
  cur.right(1);
  cur.move_to(0,this->getHeight()+1);
  lig++;
  col=2;
}

void Boite::afficheText(std::string S, size_t x, size_t y){
  cur.move_to(y,x);
  cur.write(S);
  cur.move_to(0,this->getHeight());

}

void Boite::origine(){
  cur.move_to(0,0);

}

void Boite::fin(){
  cur.move_to(0,getHeight()+1);
}





void Boite::init(){
  struct winsize w;
  ioctl(0,TIOCGWINSZ, &w);
  _width= w.ws_col;
  _height=w.ws_row;
  afficherBoite();

  
  col=2; 
  lig=2;

}


//AfficheG OK-----
void Boite::afficheG(string T)
{ ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary);
 
  size_t plEsp=0;
  size_t nbColLibre=getWidth()-3;
  size_t nbLigLibre=getHeight()-5;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.size();i++)
	{
	  if(nbLigLibre==3){
	    afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
	    int pageS;
	    std::cin>>pageS;
	    nbLigLibre=getHeight()-5;
	    init();
	    afficheText(style,lig,col);

	  }

   
	  if(i!=0)
	    {
	      if(T[i]=='\e' )
		{
		  escape++;
		}
      
	      if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		escape--;
      
	      if(escape)
		{
		  mot+=T[i];
		  style+=T[i];
		}
	    }
      
	  if((!escape))
	    {
	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape){
	    afficheText(mot,lig,2);
	    mot="";
	    col=2;
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	  plEsp=0;
	  while((T[i+plEsp+1]!=' ')&&(T[i+plEsp+1]!='\e'))
	    { plEsp++;}
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>getWidth()-3&&(T[i+plEsp-2]!='['||T[i+plEsp-3]!='['))
	    { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<col<<'\n';
	      afficheText(mot,lig,2);
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      col=2;
	      nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	}
      fin();
      col=2;
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}





void Boite::afficheD(std::string T)
{ ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary);
 
  size_t plEsp=0;
  size_t nbColLibre=getWidth()-3;
  size_t nbLigLibre=getHeight()-5;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.size();i++)
	{
	  if(nbLigLibre==3){
	    afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
	    int pageS;
	    std::cin>>pageS;
	    nbLigLibre=getHeight()-5;
	    init();
	    afficheText(style,lig,col);

	  }

   
	  if(i!=0)
	    {
	      if(T[i]=='\e' )
		{
		  escape++;
		}
      
	      if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		escape--;
      
	      if(escape)
		{
		  mot+=T[i];
		  style+=T[i];
		}
	    }
      
	  if((!escape))
	    {
	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape){
	    afficheText(mot.substr(0,mot.size()-1),lig,getWidth()-COUNT);
	    mot="";
	    col=2;
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	  plEsp=0;
	  while((T[i+plEsp+1]!=' ')&&(T[i+plEsp+1]!='\e'))
	    { plEsp++;}
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>getWidth()-3&&(T[i+plEsp-2]!='['||T[i+plEsp-3]!='['))
	    { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<col<<'\n';
	      afficheText(mot.substr(0,mot.size()-1),lig,getWidth()-COUNT);
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      col=2;
	      nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	}
      fin();
      col=2;
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}

void Boite::afficheC(string T)
{ ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary);
 
  size_t plEsp=0;
  size_t nbColLibre=getWidth()-3;
  size_t nbLigLibre=getHeight()-5;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.size();i++)
	{
	  if(nbLigLibre==3){
	    afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
	    int pageS;
	    std::cin>>pageS;
	    nbLigLibre=getHeight()-5;
	    init();
	    afficheText(style,lig,col);

	  }

   
	  if(i!=0)
	    {
	      if(T[i]=='\e' )
		{
		  escape++;
		}
      
	      if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		escape--;
      
	      if(escape)
		{
		  mot+=T[i];
		  style+=T[i];
		}
	    }
      
	  if((!escape))
	    {
	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape){
	    afficheText(mot.substr(0,mot.size()-1),lig,(getWidth()-COUNT)/2+1);
	    mot="";
	    col=2;
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }


	  plEsp=0;
	  while((T[i+plEsp+1]!=' ')&&(T[i+plEsp+1]!='\e'))
	    { plEsp++;}
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>getWidth()-3&&(T[i+plEsp-2]!='['||T[i+plEsp-3]!='['))
	    { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<col<<'\n';
	      afficheText(mot.substr(0,mot.size()-1),lig,(getWidth()-COUNT)/2+1);
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      col=2;
	      nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	}
      fin();
      col=2;
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}

void Boite::afficheJ(string T)
{ ofstream fichierE;
  fichierE.open("traceBoite.txt",ios::out|ios::binary);
 
  size_t plEsp=0;
  size_t nbColLibre=getWidth()-3;
  size_t nbLigLibre=getHeight()-5;
  int escape=0;
  size_t COUNT=0;
  string mot="";
  string style="";
  
  if(fichierE.is_open())
    { 
      for(size_t i=0;i<=T.size();i++)
	{
	  if(nbLigLibre==3){
	    afficheText("\e[0m plus de place, passer a la page suivante (1 + \"entrée\") ?",getHeight()-1,0);
	    int pageS;
	    std::cin>>pageS;
	    nbLigLibre=getHeight()-5;
	    init();
	    afficheText(style,lig,col);

	  }

   
	  if(i!=0)
	    {
	      if(T[i]=='\e' )
		{
		  escape++;
		}
      
	      if(T[i-1]=='m' && escape && ((T[i-3]=='[')|(T[i-4]=='[')))
		escape--;
      
	      if(escape)
		{
		  mot+=T[i];
		  style+=T[i];
		}
	    }
      
	  if((!escape))
	    {
	      COUNT++;
	      mot+=T[i];
	    }

    
	  if((T[i]=='\n')&&!escape){
	    afficheText(mot,lig,2);
	    mot="";
	    col=2;
	    lig++;
	    nbLigLibre--;
	    COUNT=0;
	  }
     
     
	  /*  plEsp=0;
	  while((T[i+plEsp+1]!=' ')&&(T[i+plEsp+1]!='\e'))
	  { plEsp++;}*/
	  plEsp=1;
    while(true && !escape)
      {/* fichierE<<"T[i+plEsp]="<<T[i+plEsp];
       if(i>3)
        {
          fichierE<<" T[i+plEsp-3]="<<T[i+plEsp-3]<<'\n';
        }
       else
        {fichierE<<'\n';}*/

       if((T[i+plEsp]==' '))
       {
        fichierE<<"je marche avec espace"<<'\n';
       }

       else if(((T[i+plEsp]=='\e')))
       {
        fichierE<<"je marche avec e"<<'\n';
       }



       if(T[i+plEsp]==' ')
          {break;}       
       
       else if (T[i+plEsp]=='\e')
          {break;}
       
       else
        {plEsp++;}
     } 
    
	  fichierE<<"plEsp="<<plEsp<<" _width="<<_width<<" COUNT="<<COUNT<<'\n';

	  fichierE<<"mot= "<<mot<<"\n\n";
	  if(COUNT+plEsp>getWidth()-3&&(T[i+plEsp-2]!='['||T[i+plEsp-3]!='['))
	    { fichierE<<"j'afficheText ligne="<<lig<<" colone="<<col<<'\n';

	      size_t nbEsp=getWidth()-3-COUNT;
	      
	      afficheText(mot,lig,2);
	      COUNT=0;
	      lig++;
	      nbLigLibre--;
	      mot="";
	      col=2;
	      nbColLibre==getWidth()-3;
	      plEsp=0;
     
	    }

   
   
	}
      fin();
      col=2;
      lig++;
      nbLigLibre--;
      fichierE.close();
    }
}


void Boite::clearA()
{
  system("clear");
}
