#include <iostream>
#include <string>
#include <sstream>


std::string to_string(int i){
  std::ostringstream os;
  os<<i;
  return os.str();
}

#include "cursor.h"

Cursor::Cursor(): _x(0),_y(0), _xo(0),_yo(0) {}

Cursor::Cursor(size_t x, size_t y): _x(x),_y(y),_xo(x),_yo(y) {right(x);down(y);}

void Cursor::left(size_t i) {if (i>0){_x-=i;std::cout<<std::string("\33[")+to_string(i)+std::string("D");}}

void Cursor::right(size_t i){if (i>0){_x+=i;std::cout<<std::string("\33[")+to_string(i)+std::string("C");}}

void Cursor::up(size_t i)   {if (i>0){_y-=i;std::cout<<std::string("\33[")+to_string(i)+std::string("A");}}

void Cursor::down(size_t i) {if (i>0){_y+=i;std::cout<<std::string("\33[")+to_string(i)+std::string("B");}}

void Cursor::move_to(size_t x, size_t y){
  if (x>_x) right(x-_x); else left(_x-x);
  if (y>_y) down(y-_y); else up(_y-y); 
}

void Cursor::to_origin(){ move_to(_xo,_yo);}
void Cursor::write(std::string s){_x+=s.size();}
void Cursor::endl(){
  move_to(_xo,_y+1);
}
void Cursor::clear() { std::cout << "\033[2J\033[1;1f"; }
