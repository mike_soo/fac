#ifndef BOITE_H
#define BOITE_H

#include <iostream>
#include <string>
#include <sstream>

class Boite {
  private:
    size_t _width, _height;

  public:
    Boite(size_t , size_t);	//construit une Boite de w x h
    //~Boite();
    size_t getWidth() const;
    size_t getHeight() const;
    void afficherBoite(Boite B);
    void LigneSeparateur();
};

#endif