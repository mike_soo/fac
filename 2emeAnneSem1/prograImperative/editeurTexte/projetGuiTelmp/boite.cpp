#include <iostream>
#include <string>
#include <sstream>
#include <sys/ioctl.h>
#include "boite.h"
#include "cursor.h"

using namespace std;

Boite::Boite(size_t w, size_t h){
	_width=w;
	_height=h;
}

//~Boite();

size_t Boite::getWidth() const{
	return _width;
}

size_t Boite::getHeight() const{
	return _height;
}

/*void ecrirePremiereLigne(Boite B){
	cout<<"A";
	size_t width=B.getWidth();
	size_t height=B.getHeight();
	for(size_t i=1;i<width-1;i++)
	{
		cout<<"c";
	}
	cout<<"B"<<endl;
}

void ecrirePremiereColonne(Boite B){
	size_t height=B.getHeight();
	for(size_t i=1;i<height-1;i++)
	{
		cout<<"a"<<endl;
	}
	cout<<"E"<<endl;
}*/

void afficherBoite(Boite B){
	Cursor cur();
	write("A");
	for(size_t i=1;i<_width;i++){
		write("c");
		right(1);
	}
	write("B");

}

void LigneSeparateur(){

}


int main(){
	struct winsize w;
	ioctl(0,TIOCGWINSZ, &w);
	Boite B(w.ws_row,w.ws_col);
	afficherBoite(B);
	return 0;
}