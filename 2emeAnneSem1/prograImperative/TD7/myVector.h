#ifndef myvector_h
#define myvector_h
#include <iostream>

using namespace std;

template<typename T>
class myVector 
{	private:
    	T* tab;
    	int n;
    	int indV;

	public:
		myVector();
		myVector(int taille);
		void extend();
		void pushBack(T elem);
		T &at(int i);
		void push_back(T x);
		int size();
		int find (T x);
		void erase(int i);
		void eraseElem (T elem);
		
		//il faut faire un operateur =;
		//et une methode at
    
};
//template<typename T>
//stream& operator<<(ostream & os,const myVector<T>* tabAff);


template<typename T>
void write (ostream &os ,myVector<T>* tabAff);

#include "myVector.tcc"


#endif