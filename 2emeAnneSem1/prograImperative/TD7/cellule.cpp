#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "cellule.h"

using namespace std;

int cellule::getx()
{
 return x;
}

int cellule::gety()
{
 return y;
}

bool cellule::estVivante()
{
  return vivante;
}

bool cellule::estVoisine(const cellule &c) const 
{
  return vivante &&
    ((x - c.x) * (x - c.x) + (y - c.y) * (y - c.y) <= 2);
}


bool cellule::estAvant(const cellule &c) const 
{
  return (x < c.x) || ((x == c.x) && (y < c.y));
}


bool cellule::estApres(const cellule &c) const 
{
  return c.estAvant(*this);
}


bool cellule::estEquivalente(const cellule &c) const 
{
  return !estAvant(c) && !estApres(c);
}


bool cellule::estDifferente(const cellule &c) const 
{
  return !estEquivalente(c);
}


bool cellule::estAvantOuEquivalente(const cellule &c) const 
{
  return estAvant(c) || estEquivalente(c);
}


bool cellule::estApresOuEquivalente(const cellule &c) const 
{
  return estApres(c) || estEquivalente(c);
}

void cellule::print() const {
  std::cout<<"("<<x<<","<<y<<")"<<endl;
}

void write (ostream& os,cellule &c1)
{
  os<<"c.x="<<c1.getx()<<" c.y="<<c1.gety()<<endl;
}

ostream& operator<<(ostream& os,cellule& c1)
    {
      write (os,c1);
      return os;
    }


cellule::cellule(int ab,int ord)
{
 srand(time(NULL));
 x=rand()% ab;
 y=rand()% ord;
 vivante =1;
 
}

cellule::cellule()
{int ab,ord;
 cout<<"Quelles est l'absice max"<<endl;
 cin>>ab;
 cout<<"Quelle est l'ordonees max"<<endl;
 cin>>ord;
 srand(time(NULL));
 x=rand()% ab;
 y=rand()% ord;
 vivante =1;

}


