#ifndef __CELLULE_H
#define __CELLULE_H
#include <iostream>
#include <string>

class Cellule {
 public:

  enum Couleur {
    NOIR,
    BLEU,
    VERT,
    ROUGE,
    JAUNE,
    NB_COULEURS
  };

 private:
  size_t age;
  unsigned int x, y;
  Couleur couleur;

 public:

  // Constructeurs
  Cellule(); // morte par défaut
  Cellule(bool etat, unsigned int x, unsigned int y);

  // Accesseurs en lecture
  bool estVivante() const;
  unsigned int getX() const;
  unsigned int getY() const;
  Couleur getCouleur() const;

  // Accesseurs en écriture
  void setX(unsigned int x);
  void setY(unsigned int y);

  // renvoie vrai si la cellule courante est vivante et est voisine de c
  bool estVoisine(const Cellule &c) const;
  // comparaison de cellules
  bool estAvant(const Cellule &c) const;
  bool estApres(const Cellule &c) const;
  bool estEquivalente(const Cellule &c) const;
  bool estDifferente(const Cellule &c) const;
  bool estAvantOuEquivalente(const Cellule &c) const;
  bool estApresOuEquivalente(const Cellule &c) const;
  // affiche la cellule
  void print() const;
  // spécifie si une cellule est vivante à ce tour-ci
  void Vivante(bool etat);

  // spécifie si une cellule sera vivante au trou d'après
  void vaVivre(bool etat);
};

// Renvoie vrai si la cellule est de la couleur passée en paramètre, faux sinon.
bool CelluleEstDeLaCouleur(const Cellule &cellule, Cellule::Couleur couleur);

// Retourne la chaîne correspondant à la couleur passée en paramètre 
std::string Couleur2String(Cellule::Couleur c);

void write_cell(std::ostream&, const Cellule&);
void read_cell(std::istream&, Cellule&);

#include <ostream>
std::ostream& operator<<(std::ostream& os, const Cellule& C);
std::istream& operator>>(std::istream& is, Cellule& C);


#endif
