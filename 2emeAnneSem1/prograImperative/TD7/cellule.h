#ifndef cellule_h
#define cellule_h
#include <iostream>

using namespace std;

class cellule {
	private:
		int x;
		int y;
		bool vivante;
	public:
		int getx();
		int gety();
		bool estVivante();
		void print() const;
		bool estVoisine(const cellule &c) const;
		bool estAvant(const cellule &c) const;
		bool estApres(const cellule &c) const;
		bool estEquivalente(const cellule &c) const;
		bool estDifferente(const cellule &c) const;
		bool estAvantOuEquivalente(const cellule &c) const;
		bool estApresOuEquivalente(const cellule &c) const;
	    cellule();
		cellule(int ab,int ord);
		

		

		bool operator==(cellule& c2)
		 {if ((c2.x==y)&&(c2.y==y)&&(c2.vivante==vivante))
		 	 return true;
		 else 
		 	 return false;
		 };


};
void write (ostream& os,cellule &c1);

ostream& operator<<(ostream& os,cellule c1);

		
#endif