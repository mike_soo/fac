#ifndef CURSOR_H
#define CURSOR_H
#include <iostream>

enum Couleur {
    NOIR,
    BLEU,
    VERT,
    ROUGE,
    JAUNE,
    NB_COULEURS
};

class Cursor{
 public:
  Cursor(); // créé un curseur à la position (0,0)
  Cursor(size_t x, size_t y); // créé un curseur à la position (x,y) 
  void left(size_t i) ; // recule le curseur de i position
  void right(size_t i); // avance le curseur de i position
  void up(size_t i)   ; // monte le curseur de i position
  void down(size_t i) ; // descend le curseur de i position
  void move_to(size_t x, size_t y); // bouge le curseur à la position (x,y)
  void to_origin(); // remet le curseur à son origine
  void setcolor(Couleur c);  // change la couleur d'affichage
  void write(std::string s); // écrit la chaine s à la position courante du curseur
  void clear();              // Efface l'écran et remet le curseur à son origine
  void endl();               // fait un retour chariot à la position courante du curseur

  // LA PARTIE CI DESSOUS NE DOIT PAS VOUS INTERESSÉ
 private:
  class Colorizer{
  private:
    Couleur col;  
  public:
    Colorizer();
    Colorizer(Couleur c);  
    void write(std::string s);    
    void setcolor(Couleur c);
  };
  size_t _x,_y;
  const size_t _xo,_yo;
  Colorizer _out;  
};

#endif
