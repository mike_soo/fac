#include "carre.h"

Carre::Carre(): _x(0), _y(0), _size(0){}
Carre::Carre(size_t x, size_t y, size_t s, Couleur c) 
  : _x(x), _y(y), _size(s), _col(c){}

void Carre::affiche(Cursor& cur) const{
  cur.setcolor(_col);
  for(size_t i=0; i<_size;i++){
    cur.move_to(_x,_y+i);
    for(size_t j=0; j<_size;j++){
      cur.write("X");
    }
  }
}
