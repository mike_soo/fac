#include <iostream>
#include "pilecarre.h"
#include <unistd.h>
PileCarre::PileCarre() : _end(-1), _n(0) {}

void PileCarre::empile(const Carre& C){
  if(_n<NMAX){
    _end= _end+1;
    data[_end]=C;
    sleep(2);
    _n++;
  }
  else{
    std::cerr<<"La pile est pleine"<<std::endl;
  }
}

Carre PileCarre::depile(){
  if(_n!=0){
    _end--;
    _n--;
    return data[_end+1];
  }
  else{
    std::cerr<<"La pile est vide"<<std::endl;
    return Carre();
  }
}

bool PileCarre::estVide()const{return _n==0;}
