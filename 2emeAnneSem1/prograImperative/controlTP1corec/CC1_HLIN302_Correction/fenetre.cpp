#include "fenetre.h"
//#include <stdlib.h>
#include <unistd.h>

Fenetre::Fenetre(size_t w, size_t h)
  : _width(w), _height(h), cur(1,1) {init();}

void Fenetre::drawSquare(const Carre& C){
  data.empile(C);
  C.affiche(cur);
  flush();
}

void Fenetre::back(){
  data.depile();
  init();
  PileCarre copy;
  while(!data.estVide()){
    copy.empile(data.depile());
  }
  while(!copy.estVide()){
    Carre tmp(copy.depile());
    drawSquare(tmp);
    data.empile(tmp);
  }
}

void Fenetre::init(){
  cur.to_origin();
  cur.setcolor(BLEU);
  cur.clear();

  for(size_t i=0;i<_width+2;i++)
    cur.write("O");
  cur.endl();
  cur.down(_height);
  for(size_t i=0;i<_width+2;i++)
    cur.write("O");
  cur.endl();
  cur.to_origin();
  cur.down(1);
  for(size_t i=0; i<_height;i++){
    cur.write("O");
    cur.endl();
  }
  cur.to_origin();
  cur.down(1);
  cur.right(_width+1);
  for(size_t i=0; i<_height; i++){
    cur.write("O");
    cur.down(1);
    cur.left(1);
  }
  flush();
}

void Fenetre::flush(){
  cur.move_to(_width+2, _height+4);
  cur.endl();
}
