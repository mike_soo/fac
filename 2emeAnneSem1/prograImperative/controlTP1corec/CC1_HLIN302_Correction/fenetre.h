#ifndef FENETRE_H
#define FENETRE_H

#include "cursor.h"
#include "carre.h"
#include "pilecarre.h"

class Fenetre{
 private:
  size_t _width, _height;
  PileCarre data;
  Cursor cur;

 public:
  Fenetre(size_t w, size_t h); //Construit une fenêtre de w*h
  void drawSquare(const Carre& C); // Affiche le Carré C dans la fenêtre
  void back(); // Défait la dernière action (enlève le dernier carré)
  void init(); // Initialise la fenêtre
  void flush(); // Positionne le curseur de la fenêtre après celle-ci
};

#endif
