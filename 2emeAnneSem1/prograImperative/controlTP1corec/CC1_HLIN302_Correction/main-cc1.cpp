#include "fenetre.h"

int main(int argc, char** argv) {
  Fenetre F(100,40);
  F.drawSquare(Carre(10,10,16,ROUGE));
  F.drawSquare(Carre(10,10,10,VERT));
  F.drawSquare(Carre(20,20,20,BLEU));
  F.drawSquare(Carre(30,20,7,JAUNE));
  F.back();
  F.back();
  return 0;
}
