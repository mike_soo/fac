#ifndef CARRE_H
#define CARRE_H
#include "cursor.h"

class Carre{
 private:
  size_t _x, _y;
  size_t _size;
  Couleur _col;

 public:
  Carre(); // Construit un carré vide
  //Construit un carré à la position (x,y) de taille s et de couleur c
  Carre(size_t x, size_t y, size_t s, Couleur c);

  //Affiche le carré via le curseur cur
  void affiche(Cursor& cur) const;
};

#endif
