#ifndef PILECARRE_H
#define PILECARRE_H
#include "carre.h"
#define NMAX 10

class PileCarre{
 private:
  Carre data[NMAX];
  int _end;
  size_t _n;

 public: 

  PileCarre();
  void empile(const Carre& C); // Ajoute le carré C à la pile
  Carre depile(); // Enlève le carré en haut de la pile et le renvoi
  bool estVide()const; // Teste sur la pile est vide

};

#endif
