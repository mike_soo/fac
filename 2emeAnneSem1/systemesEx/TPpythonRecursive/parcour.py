#!/usr/bin/env python3
import os,sys,re
extension={}

extension['sansExt']=0
def parcour(repertoire, prof):
	cpt=0
	print(' '*prof,repertoire)
	liste=os.listdir(repertoire)
	for fichier in liste:
		cpt+=1    	
		print(' '*(prof+3),fichier)
		
		if os.path.isdir(repertoire+"/"+fichier):
			parcour(repertoire+"/"+fichier,prof+1)
		else :
			print (' '*(prof+4),"cmtpeur",cpt) 
			res=re.search(".*\.(.+)",fichier)
			if res:
				print(' '*(prof+4),res.group(1))
				if res.group(1) in extension:
					extension[res.group(1)]+=1
				else :
					extension[res.group(1)]=1
			else :
				extension['sansExt']+=1

parcour(sys.argv[1],0)
for ext in extension :
	print("il y a",extension[ext],"extension de type ",ext)

