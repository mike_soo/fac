 #include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct noeud{
  float f;
  int pere,fg,fd;
}NOEUD;


void afficheTab(NOEUD *arbre)
{ 
  //printf("lettres\n");
  for(int i=0;i<=255;i++)
    {
      if(arbre[i].f!=0) printf("%i %c %f %i %i %i\n",i,i, arbre[i].f,arbre[i].pere,arbre[i].fg,arbre[i].fd);

    }

  //printf("noeud\n");
  for(int i=256;i<=511;i++)
    {
      if(arbre[i].f!=0)
  printf("%i %f %i %i %i\n",i,arbre[i].f,arbre[i].pere,arbre[i].fg,arbre[i].fd);
     
    }

  
}

long double* calculFrequences (char* fichier)
{
  long double* frequences=malloc(256*sizeof(long double)); //création d'un tableau de 256 float, pour chaque caractères ASCII
  int i=0;
  unsigned long long int nbCar=0; //compteur de caractères total
  long double freqTotal=0;
  unsigned long long int occurenTot = 0;
  for(i=0;i<256;i++)
    frequences[i]=0; //initialisation de chaque cases du tableau a 0

  FILE* fd;
  if((fd=fopen(fichier,"r"))!=NULL)
  {
    unsigned int c;
    /*
      lectures car par car, incrementation de la case qui corespond et du nb total
    */

	    while((c=fgetc(fd))!=EOF)
	    {
	      frequences[c]++;
	      nbCar++;
	      // printf("%c \n", c);
	    }

    printf("effectif total=%Li\n",nbCar);
    for(i=0;i<256;i++)
      if(frequences[i]!=0)
	     {occurenTot+=frequences[i];
        frequences[i]=(frequences[i])/(1.0*nbCar);
        freqTotal+=frequences[i];
        //printf("%i %f",i)
       // printf("")
       }

    //on converti les occurences en frequences
   printf ("occurences totalt dans caculFrequences=%Li\n",occurenTot);
   printf("frequence total dans calculFrequences=%Le\n",freqTotal);
    fclose(fd);
    return frequences;
  }
  else{printf("Erreur d'ouverture\n");
    exit(1);}
}



void rechercheMins(int *ind,NOEUD* arbre,unsigned int racine,float* Mins)
{
  //int *ind=malloc(2*sizeof(int));
  ind[0]=0;
  ind[1]=1;
  Mins[0]=1;
  Mins[1]=1;
  int i;
  for(i=0;i<512;i++)
  {
    if((arbre[i].f!=0)&&(arbre[i].f<=Mins[0])&&(arbre[i].pere==-1))
    {
      Mins[1]=Mins[0];
      Mins[0]=arbre[i].f;
      ind[1]=ind[0];
      ind[0]=i;
     // printf("a l'indice i : %i",i );
     // printf(" la frequence est :%f\n",arbre[i].f);
    }
    
    else if((arbre[i].f!=0)&&(arbre[i].f>Mins[0])&&(arbre[i].f<=Mins[1])&&(arbre[i].pere==-1))
    { 
      Mins[1]=arbre[i].f;
      ind[1]=i;
    }
  }
 

  //return ind;
}


int constArbre(long double* tab,NOEUD* arbre)
{

  
  int *ind=malloc(2*sizeof(int));
  int *Tabmin=malloc(2*sizeof(float));
  Tabmin[0]=0;
  Tabmin[1]=0;
  float Mins[2];
  unsigned int i;
  unsigned int racine=256;
  for(i=0;i<256;i++)
  {
    arbre[i].f=tab[i];
    arbre[i].fg=-1;
    arbre[i].fd=-1;
    arbre[i].pere=-1;
  }

  for(i=256;i<512;i++)
  {
    arbre[i].f=0;
    arbre[i].fd=-1;
    arbre[i].fg=-1;
    arbre[i].pere=-1;
  }
  
  
  while((arbre[ind[0]].f+arbre[ind[1]].f)<1)
    {
      
      rechercheMins(ind,arbre,racine,Mins);
      
      /*for (int i=0;i<2;i++)
      {
       Tabmin[i]=ind[i];
      }
      //printf("Tabmin= %i %i\n",Tabmin[0],Tabmin[1]);
      */

      arbre[racine].fd=ind[0];
      arbre[racine].fg=ind[1];
      arbre[racine].f=arbre[ind[0]].f+arbre[ind[1]].f;
      arbre[ind[0]].pere=racine;
      arbre[ind[1]].pere=racine; 
      racine++;
     
     
      //printf("racine : %i\n",racine );
      //printf("%i %i\n",Tabmin[0],Tabmin[1]);

    }

    
    
    free(Tabmin);
    free(ind);
  return racine;

}



void ConstruireSuiteBin(NOEUD *arbre,int num_noeud,char* suite,char** TableCar)
{  
  char suiteD[256];
  char suiteG[256];
  for(int i=0;i<256;i++){
    suiteG[i]=suiteD[i]=suite[i];
    
  }
  if(arbre[num_noeud].fg==-1)
    { 
      strcat(TableCar[num_noeud],suite);

    }
  
  else{
    ConstruireSuiteBin(arbre,arbre[num_noeud].fd,strcat(suiteD,"1"),TableCar);
    ConstruireSuiteBin(arbre,arbre[num_noeud].fg,strcat(suiteG,"0"),TableCar);
    
    
  }
}




void afficheCodeBin(char** TableCar, unsigned int ni)
{
  int i=0;
  while(TableCar[ni][i]!='\0')
    {
      printf("%c",TableCar[ni][i]);
      i++;
    }
}

char* retourneCodeBin(char** TableCar, unsigned ni, char* retour)
{
  retour[0]='a';
  int nbCh=1;
  int i=0;
  
  while(TableCar[ni][i]!='\0')
    {
      retour[i]=TableCar[ni][i];
      retour=realloc(retour,nbCh*sizeof(char));
      i++;
      nbCh++;
      retour[i-1]=TableCar[ni][i-1];
      retour[i]='\0';
    }
  if(retour[0]=='a')
    retour[0]='\0';
  return retour;
}


void ecrireFichierComp(char* fichier,char**TableCar)
{  
  char* buffer=malloc(264*sizeof(char));
  for(int i=0;i<264;i++)
    {
      buffer[i]='\0';
    }
  int nbBitBuffer=0;
  int car;
  int i;
  FILE* fichier_or;
  fichier_or=fopen(fichier,"r");
  FILE* fichierComp;
  fichierComp=fopen(strcat(fichier,".huf"),"w+b");
  char* retour1=malloc(sizeof(char));
  char* retour2=malloc(sizeof(char));
  char lettre;


  char c;
  c=fgetc(fichier_or);
  while(c!=EOF)
  {
	 if(nbBitBuffer>7)
    {
            
     car=0;
     for(int i=0;i<8;i++)
      {
	       car*=2;
	       car+=(buffer[i]-48);//equivalent atoi
	       //printf("--> %c",car);
	
      }
      //printf("Le car obtenu est donc %i \n",car);
      
      lettre=car;
      fwrite(&lettre,1,1,fichierComp);
      i=0;
     
       
     while(buffer[i+8]!='\0')
     {
       buffer[i]=buffer[i+8];
  	   i++;
	   }
     nbBitBuffer-=8;
     for(int j=0;j<8;j++)
     {
	     buffer[i]='\0';
	     i++;
      }
      
    }
    
     
	 strcpy(retour2,retourneCodeBin(TableCar,c,retour1));
	 strcpy(retour1,retour2);
   strcat(buffer,retour2);
	 i=0;
	 while(retour2[i]!='\0')
	  {
     i++;
     nbBitBuffer++;
	  }
   c=fgetc(fichier_or);
  }
  
  if((c==EOF)&&(nbBitBuffer<8))
  {
    //printf("Ecriture dernier elettre");
    int diff;
    diff=8-nbBitBuffer;
    for(int i=0;i<8;i++)
      {
	     if(buffer[i]=='\0')
	     buffer[i]='0';
      }


    
    car=0;
    for(int i=0;i<8;i++)
    {
      car*=2;
      car+=(buffer[i]-48);//equivalent atoi
      //printf("--> %c",car);
      
    }
    lettre=car;
    fwrite(&lettre,1,1,fichierComp);

    
  }

    fclose(fichierComp);
    free(retour2);
    free(retour1);

}

void stats(NOEUD* arbre)
{int cpt=0;
   long double freqTot=0;
  for(int j=0;j<512;j++)
 {
    
   
   if(j<256)
    { if(arbre[j].f!=0)
        cpt++;

      printf("%c ",j);
    }
   else if(j>256)
     {printf(" ");}
  
   printf("%i %f %i %i %i \n",j,arbre[j].f,arbre[j].fg,arbre[j].fd,arbre[j].pere);
   
  }



  for(int j=0;j<256;j++)
    {freqTot+=arbre[j].f;}
  
   printf("frequences total =%Le\n",freqTot);
   printf("cpt : %i \n",cpt);
}

int main(int argc, char** argv)
{
  long double* freq=calculFrequences(argv[1]);
  NOEUD arbre[512];
  int racine=constArbre(freq, arbre);

  racine --;
  //afficheTab(arbre);
  //printf("La racine est %d\n",racine);

  
  char** TableCar=malloc(512*sizeof(char*));
  for (int i=0;i<512;i++)
    {TableCar[i]=malloc(512*sizeof(char));}
  
  for(int i=0;i<512;i++)
    {
      for(int j=0;j<512;j++)
			{
	  		TableCar[i][j]='\0';
			}
    }
  
  char* suite=malloc(512*sizeof(char));
  for(int i=0;i<512;i++)
    {
      suite[i]='\0';
    }
  ConstruireSuiteBin(arbre,racine,suite,TableCar);


  //int ni='c';  
  
  /*printf("a=");
  afficheCodeBin(TableCar,'a');
   printf("\n");
  //code de a a f
   printf("b=");
  afficheCodeBin(TableCar,'b');
  printf("\n");
   printf("c=");
  afficheCodeBin(TableCar,'c');
  printf("\n");
   printf("d=");
  afficheCodeBin(TableCar,'d');
  printf("\n");
   printf("e=");
  afficheCodeBin(TableCar,'e');
  printf("\n");
   printf("f=");
  afficheCodeBin(TableCar,'f');
  printf("\n");
  */
 // char* retour=malloc(sizeof(char));
  //retour=retourneCodeBin(TableCar,ni,retour);
  
  // printf("retour ds main-- >%s",retour);
    
   
  
  
 
  ecrireFichierComp(argv[1],TableCar);

  stats(arbre);
  
  return 0;
}
