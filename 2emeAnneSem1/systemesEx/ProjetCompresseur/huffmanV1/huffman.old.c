#include <stdio.h>
#include <stdlib.h>
#include <string.h>

float* calculFrequences (char* fichier){
  float* frequences=(float*) malloc(256*sizeof(float)); //création d'un tableau de 256 float, pour chaque caractères ASCII
  int i=0;
  int nbCar=0; //compteur de caractères total

  for(i=0;i<256;i++)
    frequences[i]=0; //initialisation de chaque cases du tableau a 0

  FILE* fd;
  if((fd=fopen(fichier,"r"))!=NULL){
    unsigned int c;
    /*
     lectures car par car, incrementation de la case qui corespond et du nb total
     */

    while((c=fgetc(fd))!=EOF){
      frequences[c]++;
      nbCar++;
      // printf("%c \n", c);
    }

    for(i=0;i<256;i++)
      if(frequences[i]!=0)
      frequences[i]=frequences[i]/nbCar;
    //on converti les occurences en frequences

    fclose(fd);
    return frequences;
  }
  else{printf("Erreur d'ouverture\n");
    exit(1);}
}

typedef struct noeud{
  float f;
  int pere,fg,fd;
}NOEUD;


int* rechercheMins(NOEUD* arbre, int racine,float* Mins){
  int *ind=malloc(2*sizeof(int));
  ind[0]=0;
  ind[1]=1;
  Mins[0]=1;
  Mins[1]=1;
  int i;
  for(i=0;i<racine;i++){
    if((arbre[i].f!=0)&&(arbre[i].f<Mins[0])&&(arbre[i].pere==-1)){
      Mins[1]=Mins[0];
      Mins[0]=arbre[i].f;
      ind[1]=ind[0];
      ind[0]=i;
    }
    
    else if((arbre[i].f!=0)&&(arbre[i].f>Mins[0])&&(arbre[i].f<Mins[1])&&(arbre[i].pere==-1)){
      Mins[1]=arbre[i].f;
      ind[1]=i;
   
    }

  }


  /* Mins[0]=ind[0];
  Mins[1]=ind[1];
  free(ind);*/
  return ind;
}


int constArbre(float* tab,NOEUD* arbre){
  //NOEUD arbre[511];

  int *Tabmin=malloc(2*sizeof(float));
   Tabmin[0]=0;
   Tabmin[1]=0;
  float Mins[2];
  int i;
  int racine=256;
  for(i=0;i<256;i++){
    arbre[i].f=tab[i];
    arbre[i].fg=-1;
    arbre[i].fd=-1;
    arbre[i].pere=-1;
  }

  for(i=256;i<511;i++){
    arbre[i].f=0;
    arbre[i].fd=-1;
    arbre[i].fg=-1;
    arbre[i].pere=-1;
  }

  while((arbre[Tabmin[0]].f+arbre[Tabmin[1]].f)<1)
  {
   
    Tabmin=rechercheMins(arbre,racine,Mins);
    //printf("Tabmin= %i %i\n",Tabmin[0],Tabmin[1]);
    arbre[racine].fd=Tabmin[0];
    arbre[racine].fg=Tabmin[1];
    arbre[racine].f=arbre[Tabmin[0]].f+arbre[Tabmin[1]].f;
    arbre[Tabmin[0]].pere=racine;
    arbre[Tabmin[1]].pere=racine; 
    racine++;
    
    //printf("%i %i\n",Tabmin[0],Tabmin[1]);

  }
  return racine;

}

void afficheTab(NOEUD *arbre)
{ //int deb,fin;
  //printf("rentrer deb et fin");
  //  scanf("%i",&deb);
  //scanf("%i",&fin);
  
  printf("lettres\n");
for(int i=0;i<=255;i++)
 {
   if(arbre[i].f!=0)
     printf("%i %f %i %i %i\n",i, arbre[i].f,arbre[i].pere,arbre[i].fg,arbre[i].fd);

 }

 printf("noeud\n");
 for(int i=256;i<=510;i++)
   {
   if(arbre[i].f!=0)
     printf("%i %f %i %i %i\n",i,arbre[i].f,arbre[i].pere,arbre[i].fg,arbre[i].fd);
     
}

}


int main(int argc, char** argv){


  float* freq=calculFrequences(argv[1]);

  
  for(int i=0;i<256;i++){
    if(freq[i]!=0)
      printf("%c %i %f \n", i, i, freq[i]);

  }


  NOEUD arbre[511];
  int racine=constArbre(freq, arbre);
  racine --;
  afficheTab(arbre);
  printf("La racine est %d\n",racine);
  return 0;
}
