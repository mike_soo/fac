/* Creation des relations */
/* Attention ce fichier ne doit etre lance qu'une seule fois */
DROP TABLE vol;
DROP TABLE Avion;
DROP TABLE pilote;

CREATE TABLE PILOTE (
plnum INTEGER, 
plnom VARCHAR(10) NOT NULL, 
pladr VARCHAR(10),
plsal INTEGER,
CONSTRAINT cp_plnum PRIMARY KEY (plnum)
);

CREATE TABLE AVION (
avnum INTEGER, 
avnom VARCHAR(10) NOT NULL, 
avloc VARCHAR(10),
avcap NUMERIC (3,0),
CONSTRAINT cp_avnum PRIMARY KEY (avnum)
);


CREATE TABLE VOL (
volnum  INTEGER,  
numpl INTEGER,
numav INTEGER,
vd VARCHAR(10),
va VARCHAR(10),
hd INTEGER,
ha INTEGER,
CONSTRAINT cp_vol PRIMARY KEY (volnum),
CONSTRAINT ce1_vol FOREIGN KEY (numpl) REFERENCES PILOTE(plnum),
CONSTRAINT ce2_vol FOREIGN KEY (numav) REFERENCES AVION(avnum)
);

