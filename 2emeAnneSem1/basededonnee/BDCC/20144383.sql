--question 3: 

select  pladr
from pilote
where plnum=2;

--question 4:

select plnum,plnom
from pilote
where pilote.plnom like '%E%';

--question 5:

select avnum,avcap
from avion
where avloc='TOULOUSE'
order by avcap DESC;

--question 6:

select plnum as NumeroPilote, plnom as NomPilote, pladr as AdressePilote, plsal as SalairePilote
from pilote;


--question 7:


select plnom,numav
from pilote,vol
where plnum=numpl and numav=112
order by plnom;
--aucun des vol contient l'avion 112

--qestion 8:

select plnom,avnom
from pilote,vol,avion
where plnum=numpl and avnum=numav and avnom='A330'
order by plnom;

--question 9:
select plnom,plnum
from pilote
where plnum not in (select numpl
                    from vol
                    );

--question 10:
select plnum, plnom, avnom as avionUtilise
from pilote,vol,avion
where plnum=numpl and avnum=numav;

--question 11:

select plnum, plnom ,count(plnum) 
from pilote,vol
where plnum=numpl
group by plnum, plnom;

--question 12:

select pladr, count (pladr) as nombredePiloteAlaVille
from pilote 
group by pladr
order by count(pladr);

--question 13
select plnom,count(plnom) as nombredavionutil
from pilote,vol
where plnum=numpl 
group by plnom;

--question 14
select avnom,count(avnom) as nombreDeVol
from avion,vol
where avnum=numav and avnom='B747'
group by avnom;

--question 15
select plnum,plnom,plsal
from pilote
where plsal>=all(select plsal
 				 from pilote				
 				);
--question 16
select plnum,plnom,avnom
from pilote,avion,vol
where plnum=numpl and avnum=numav and avnom like 'B%';

--question 17
select plnum,plnom
from pilote
where pladr=(select pladr
			 from pilote
			 where plnum=4 
			)
and plnum<>4;

--question 18
