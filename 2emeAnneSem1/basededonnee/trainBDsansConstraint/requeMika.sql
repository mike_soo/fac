
----------------------un count simple
select film.idRealisateur, count(film.idRealisateur) cptFilmm
from film
group by film.idRealisateur;

----------------------personne ayante realisé le plus de film
select f1.idRealisateur, count(f1.idRealisateur) f1cptFilm ,nomR,prenomR
from film f1, realisateur
where f1.idRealisateur=idr
group by f1.idRealisateur,nomR,prenomR
having count(f1.idRealisateur)>=ALL (select count(film.idRealisateur) cptFilmm
                                     from film
                                     group by film.idRealisateur
                                    );
-----------------------personne ayant realisé le plus de film version avec nom et preno
select nomR,prenomR,count(*)
from realisateur,film
where realisateur.idr=film.idRealisateur
group by realisateur.idr ,realisateur.nomR , realisateur.prenomR
having count(*)>=ALL(select count(*)
					 from film
					 group by film.idRealisateur
				    );
---------------------personne avec le plus grand salaire
select nomA,prenomA,salaire
from acteur,jouer
where idA=idActeur and salaire >= (select max(salaire)
								   from jouer
								   );
--------------------realisateur dont tous les films depassent la freq moyenne
select nomR,prenomR
from  realisateur,film
where idR=idRealisateur and idR not in (
										select idRealisateur
										from film
										where nbspectateurs<(select avg(nbspectateurs)
															 from film
															)		
										);
-------------------le plus grand succes de chaque acteur designé par son nom et prenom
select a1.nomA,a1.PrenomA,f1.titre
from film f1,jouer j1,acteur a1
where a1.idA=j1.idActeur and f1.idF=j1.idFilm and f1.nbspectateurs>=all(select nbspectateurs
 													                    from film,jouer
 													                    where a1.idA=idActeur and idF=idFilm
 														               );
----------------les fils ayant rapporte le plus d'argent pour chaque acteur designé par nom prenom
select f1.titre as titrePlusdArgentRaporte, a1.nomA, a1.prenomA
from film f1, acteur a1, jouer j1
where f1.idf=j1.idFilm and a1.ida=j1.idActeur and j1.salaire>=all(select salaire
															      from jouer
															      where a1.ida=idActeur
															     ) 
--select *
--from film;
