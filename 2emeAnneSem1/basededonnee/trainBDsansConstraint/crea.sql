DROP TABLE film;
DROP TABLE acteur;
DROP TABLE realisateur;
DROP TABLE genre;
DROP TABLE jouer;

CREATE TABLE acteur
(
idA NUMERIC(5) PRIMARY KEY,
nomA CHARACTER(40) NOT NULL,
prenomA CHARACTER(40),
nationalite CHARACTER(40)
);

CREATE TABLE realisateur
(
 idR NUMERIC (5) PRIMARY KEY,
 nomR CHARACTER(50) NOT NULL,
 prenomR CHARACTER(50),
 nationalite CHARACTER(50)
);

CREATE TABLE genre 
(
 idG NUMERIC (5) PRIMARY KEY,
 description CHARACTER(20)
);

CREATE TABLE film
(
 idF NUMERIC(5) PRIMARY KEY,
 titre CHARACTER(50) NOT NULL,
 annee NUMERIC(4),
 pays CHARACTER(20),
 nbspectateurs NUMERIC(4),
 idRealisateur NUMERIC(5),
 idGenre NUMERIC(5)
 --CONSTRAINT FK_FILM_R FOREIGN KEY (idRealisateur) REFERENCES realisateur (idR),
 --CONSTRAINT FK_FILM_G FOREIGN KEY (idGenre) REFERENCES genre (idG)
);

CREATE TABLE jouer
( 
 idActeur NUMERIC(5),
 idFilm NUMERIC(5),
 salaire INT CHECK(salaire > 0)
 --PRIMARY KEY (idActeur,idFilm)
);
