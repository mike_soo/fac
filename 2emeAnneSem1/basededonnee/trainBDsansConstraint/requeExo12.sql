
/*le plus grand succes de chaque acteur designe par son nom et prenom*/
select distinct nomA as new1,prenomA,titre,nbspectateurs
from jouer,film,acteur
where idA=idActeur and idF=idFilm and nbspectateurs=(select max(F1.nbspectateurs)
													from film F1,jouer J1
													where idF=idFilm and J1.idActeur=idA);

/*les films ayant rapporté leplus d'argent pour chaque acteur designé par son nom et prenom*/

select nomA, prenomA, titre,salaire
from acteur,jouer,film
where idActeur=idA and idF=idFilm and salaire = (
													select max(J1.salaire)
													from jouer J1
													where idA=J1.idActeur						
												);
/*le plus grand succes en nombre de spectateurs de chaque realisateur designe par son nom et prenom*/

select nomR,prenomR,titre,nbspectateurs
from realisateur,film
where idRealisateur=idR and nbspectateurs=(
	                                        select max(F1.nbspectateurs)
											from film F1
											where idR=F1.idRealisateur
										  );