/*acteur et realisateur qui on fait ensemble 2 ou plus de film*/

select nomA,prenomA,nomR
from film,jouer,acteur,realisateur
where idR=idRealisateur and idA = idActeur and idF=idFilm
group by nomA,prenomA,nomR
having count(*)>1;

/*homonymes d'acteurs*/
select nomA,prenomA
from acteur
group by nomA,prenomA
having count(*)>1;

/*pour chasue realisateur le nombre 
moyen et maximum de spectateurs qui on vue ses films*/

--and idR in [select idRealisateur
--									from realisateur
--									where 
--									]

select nomR,AVG(nbspectateurs), MAX(nbspectateurs),count(nomR),MIN(nbspectateurs)
from realisateur,film
where idRealisateur=idR
group by nomR;

/*nombre d'acteur par nationalité*/

select nationalite as nationaliteActeur,count(*)
from acteur
group by nationalite;

/*nombre d'acteur total*/

select count(idA) as nbDacteurs
from acteur;

/*nombre de films enregistres par genre*/

select description,count(*)
from genre,film
where idG=idGenre 
group by description;

/*liste des acteurs qui on un salaiare moyen de plus de 1 mil d'euros*/
select nomA,prenomA as AvecSalaireMoysup15000,avg(salaire)
from acteur,jouer
where idActeur=idA
group by nomA,prenomA
having avg(salaire)>15000;


/*nombre de films jouer par chaque acteur*/
select nomA,prenomA,count(*)
from jouer,acteur
where idA=idActeur
group by nomA,prenomA 

/*nombre de films realisés par chaque acteur anglais*/
select nomR,prenomR,count(*)
from realisateur,film
where idR=idRealisateur and nationalite='E.U.'
group by nomR,prenomR

/*acteur qui a joué dans plus de 4 films comique*/

select nomA,prenomA,count(*) as nombreFilsComedie
from acteur,jouer,film,genre
where idA=idActeur and idFilm=idF and idG=idGenre and description='Comedie'
group by nomA,prenomA;

--liste des acteurs par ordre alphabetique qui ont un salaire 
--moyen inf a 10000 euros et qui ont joué dans des films policier

select nomA as JoueDansDesFilmPolicer,prenomA,avg(salaire)
from acteur,jouer,film,genre
where idA=idActeur and idF=idFilm and idG=idGenre and description='Comedie'
group by nomA,prenomA
having avg(salaire)<15000
order by nomA DESC;

select nomR,description,avg(nbspectateurs)
from realisateur,film,genre
where idR=idRealisateur and idG=idGenre
group by nomR,description
order by description;

