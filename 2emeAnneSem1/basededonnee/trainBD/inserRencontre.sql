DROP TABLE rencontre;

CREATE TABLE rencontre
(#idRencontre NUMERIC(5),
 idActeur1 NUMERIC(5),
 idActeur2 NUMERIC(5),
 idFilmRencontre NUMERIC(5),
 #CONSTRAINT cp_idRencontre PRIMARY KEY (idRencontre),
 CONSTRAINT ce1_rencontre FOREIGN KEY (idFilmRencontre) REFERENCES film(idF),
 CONSTRAINT ce2_rencontre FOREIGN KEY (idActeur1) REFERENCES acteur(idA),
 CONSTRAINT ce3_rencontre FOREIGN KEY (idActeur2) REFERENCES acteur(idA),
 PRIMARY KEY(idActeur1,idActeur2)
 

);

INSERT INTO rencontre (idActeur1,idActeur2,idFilmRencontre)
	VALUES ('00001','00009','00001');