/*le plus grand succes de chaque acteur desgine par son nom et prenom*/
select A1.nomA,A1.prenomA,F1.titre 
from acteur A1,jouer J1,film F1 
where A1.idA=J1.idActeur and J1.idFilm = F1.idF and F1.nbspectateurs = (select max(F2.nbspectateurs) 
																	    from film F2,jouer J2
																	    where J2.idFilm=F2.idF and A1.idA=J2.idActeur);

/*les fils ayant rapporté le plus d'argent pour chaque acteur desgine par son nom et prenom*/
select titre,nomA,prenomA,salaire
from acteur ,jouer,film
where idA=idActeur and idFilm=idF and salaire = (
								  select max(J1.salaire)
								  from jouer J1
								  where J1.idActeur=idA
								 );

/*le plus grand succes en nombre de spectateurs de chaque realisateur designe par son nom et prenom*/

select nomR,prenomR,titre
from realisateur,film
where idR=idRealisateur and nbspectateurs = (select max(F1.nbspectateurs)
											 from film F1
											 where F1.idRealisateur=idR);

/*Les genres jamais utilisés*/

select idG
from genre
where idG not in (select idGenre
				  from film
				  where idGenre	
				  );

/*les homonymes des acteurs*/

select nomA,prenomA,count(*)
from acteur
group by nomA,prenomA
having count(*)>1;

/*homonymes version corelative*/

select nomA , prenomA
from acteur
where idA in (select idA
			from acteur A1
			where A1.nomA=nomA
			group by A1.nomA
			having count(*)>1
		    );




/*acteurs qui ont eu au moins un salaire superieur au salaire moyen*/

select distinct nomA,prenomA
from acteur ,jouer 
where idA=idActeur and salaire >=(
								  select avg(salaire)
								  from jouer,acteur
								  where idActeur=idA

								  );


/*Films qui ont eu une frequentation deux fois superieure a la moyenne*/

select titre,nbspectateurs as nbSpec
from film
where nbspectateurs> 2*(
					  select avg(nbspectateurs)
					  from film
					 );


select 2*avg(nbspectateurs)
from film;

/*realisateur ayant realise tous leurs films dans le style 'policier'*/

select nomR, prenomR
from realisateur
where idR not in (select idR
				  from film,genre
				  where idGenre=idG and description <>'Comedie'

				 );

/*realisateur ayant realise le plus de films*/

select nomR,prenomR,count(*)
from realisateur,film
where idRealisateur=idR
group by idRealisateur
having count(*)>= all(select count(idRealisateur)
				  from film
				  group by idRealisateur
					);

/*Acterus ayant touché le plus d'argent pour un film*/

select nomA,prenomA,salaire
from jouer, acteur
where idActeur=idA and salaire= (select max(salaire)
								 from jouer

								);
/*realisateur dont tous les films depasset la freq moyenne*/

select nomR,prenomR,nbspectateurs
from realisateur, film
where idR=idRealisateur and idF not in  (select idF
 											from film
 											where nbspectateurs<=(select avg(nbspectateurs)
					  					    						from film
					  					    					 )

										);

select avg (nbspectateurs)
from film;

/*acteur dont le salaire est toujours superieur a la moyenne*/
select avg(salaire)
from jouer;

select nomA,prenomA,salaire
from acteur,jouer
where idA=idActeur and idA not in  (select idA
                  					from acteur,jouer
                 					 where idA=idActeur and salaire <=(select avg(salaire)
				 	                                from jouer))
