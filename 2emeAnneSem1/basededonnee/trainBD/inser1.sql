
INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00001','Portman','Natalie','E.U.');

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00002','Blunt','Emily','Angleterre');

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00003','Monagan','Michelle','E.U.');

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00004','Johansson','Scarlet','EU');

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00005','Brewster','Jordana','Panama'); #FAST AND FURIOUS

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00006','Jolie','Angelina','E.U.'); #MAD MAX

INSERT INTO acteur(idA,nomA,prenomA,nationalite) #star wars
	VALUES ('00007','Fisher','Carrie','E.U.');

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00008','Lawrence','Jenifer','E.U.'); #hunger games

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00009','Bond','James','E.U.');

INSERT INTO acteur(idA,nomA,prenomA,nationalite)
	VALUES ('00010','Blunt','Emily','E.U.');


INSERT INTO genre(idG,description)
	VALUES ('00001','Drame');
INSERT INTO genre(idG,description)
	VALUES ('00002','ScienceFiction');
INSERT INTO genre(idG,description)
	VALUES ('00003','Comedie');
INSERT INTO genre(idG,description)
 	VALUES ('00004','Horreur');
INSERT INTO genre(idG,description)
	VALUES ('00005','Amour');
INSERT INTO genre(idG,description)
	VALUES ('00006','Action');



INSERT INTO realisateur(idR,nomR,prenomR,nationalite) 
	VALUES ('00001','Aronofsky','Darren','E.U.');

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00002','Liman','Doug','E.U.');

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00003','Jones','Duncan','E.U.');

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00004','Whedon','Joss','E.U.');	

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00005','Darroussin','Jean-Pierre','France');

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00006','Wan','James','Malasia');	 #fast and furious

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00007','Abhrams','J.J.','E.U.');	 #star wars

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00008','Miller','George','Australie'); #mad max

INSERT INTO realisateur(idR,nomR,prenomR,nationalite)
	VALUES ('00009','Lawrence','Francis','Autriche'); #the hunger games 3



INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00001','Black Swam','2010','USA','50','00001','00001');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00002','Edge Of tommorow','2010','USA','45','00002','00002');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00003','Code Source','2011','USA','56','00003','00002');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00004','The Avengers 2','2012','USA','62','00004','00002');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00005','A l''attaque','1999','France','54','00005','00003');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00006','La Ville est tranquille','2000','France','150','00005','00001');

INSERT INTO film (idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00007','C''est le bouquet!','2001','France','47','00005','00004');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)
	VALUES ('00008','Une affaire privée','2002','France','57','00005','00001');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00009','Fast and Furious 5','2015','USA','67','00006','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00010','Mad Max','2015','USA','40','00008','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00011','Star Wars VII','2015','USA','155','00007','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00012','Hunger Games 3 part 2','2015','USA','60','00009','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00013','Le coeur des hommes','2002','France','12','00005','00003');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00014','Mon pere est ingenieur','2003','France','55','00005','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00015','Cause toujours!','2004','France','75','00005','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00016','Le septieme jure','2004','France','42','00005','00004');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00017','Le voyage en armenie','2006','France','95','00005','00005');

INSERT INTO film(idF,titre,annee,pays,nbspectateurs,idRealisateur,idGenre)	
	VALUES ('00018','Cause toujours!','2004','France','75','00005','00002');



INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00001','00001','15000');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00002','00002','30000');

INSERT INTO jouer(idActeur,idFilm,salaire)
    VALUES ('00003','00003','25000');

INSERT INTO jouer(idActeur,idFilm,salaire)
    VALUES ('00004','00004','26000');

INSERT INTO jouer(idActeur,idFilm,salaire) 
	VALUES ('00005','00009','15000'); #Fast and furious

INSERT INTO jouer(idActeur,idFilm,salaire) 
	VALUES ('00006','00010','14000'); #mad max

INSERT INTO jouer(idActeur,idFilm,salaire) 
	VALUES ('00007','00011','17000');  #star wars

INSERT INTO jouer(idActeur,idFilm,salaire) 
	VALUES ('00008','00012','15500'); #hunger games

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00001','12400');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00002','13500');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00003','15500');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00004','17500');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00005','8000');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00006','26000');

INSERT INTO jouer(idActeur,idFilm,salaire)
	VALUES ('00009','00007','30000');