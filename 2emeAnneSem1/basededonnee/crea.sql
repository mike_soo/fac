DROP TABLE film;
DROP TABLE acteur;
DROP TABLE realisateur;
DROP TABLE genre;
DROP TABLE jouer;

CREATE TABLE acteur
(
idA NUMERIC(5) ,
nomA CHARACTER(40) NOT NULL,
prenomA CHARACTER(40),
nationalite CHARACTER(40)
CONSTRAINT cp_idA PRIMARY KEY (idA)
);

CREATE TABLE realisateur
(
 idR NUMERIC (5) ,
 nomR CHARACTER(50) NOT NULL,
 prenomR CHARACTER(50),
 nationalite CHARACTER(50)
 CONSTRAINT cp_idR PRIMARY KEY (idR)
);

CREATE TABLE genre 
(
 idG NUMERIC (5) ,
 description CHARACTER(20)
 CONSTRAINT cp_idG PRIMARY KEY (idG)
);

CREATE TABLE film
(
 idF NUMERIC(5),
 titre CHARACTER(50) NOT NULL,
 annee NUMERIC(4),
 pays CHARACTER(20),
 nbspectateurs NUMERIC(4),
 idRealisateur NUMERIC(5),
 idGenre NUMERIC(5)
 CONSTRAINT cp_idF PRIMARY KEY (idF)
 CONSTRAINT ce1_film FOREIGN KEY (idRealisateur) REFERENCES realisateur(idR),
 CONSTRAINT ce2_film FOREIGN KEY (idGenre) REFERENCES genre(idG),

);

CREATE TABLE jouer
( 
 idActeur NUMERIC(5),
 idFilm NUMERIC(5),
 salaire INT CHECK(salaire > 0),
 PRIMARY KEY (idActeur,idFilm)
);
