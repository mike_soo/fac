#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "progListeSC.h"
using namespace std;


ListeSC dernierLSC(ListeSC L){
  assert(L!=NULL);
  while ((L->succ) != NULL){
    L=(L->succ);}
  return L;
}

bool estTrieeLSC(ListeSC L){
  /*   Res : Renvoie 1 si L est une ListeSC tri�e, 0 sinon */
  /*   version r�cursive  */

  if (estVideLSC(L) || estVideLSC(L->succ))
    return true;
  else
    if (L->info < (L->succ)->info)
      return estTrieeLSC(L->succ);
    else return false;
}



bool estListeIntervalle(ListeSC L){
  /*   Res : renvoie 1 si L est une Liste intervalle, renvoie 0 sinon */
  while(L->succ!=NULL)
    {if (((L->succ)->info)==((L->info)+1))
        {L=L->succ;}
     else
     {
      return false;
     } 
    } 

  return true;
}

ListeSC consListeIntervalle1(int l, int p){
  /*     Donn�e : l>0 */
  /*     Res : renvoie une liste intervalle de longueur l et dont le premier �l�ment a pour valeur p */
  /*     Complexit� : O(l)=l^2  */
  int i; ListeSC L;
  assert(l>0);
  L=NULL;
  for(i=0;i<l;i++){
    insererFinLSC(L,p+i);}
  return L;
}

ListeSC consListeIntervalle2(int l, int p){
  /*     Donn�e : l>0 */
  /*     Res : renvoie une liste intervalle de longueur l et dont le premier �l�ment a pour valeur p */
  /*     Complexit� : ???  */
  //assert(l>0);
  int i;
  ListeSC L;
 L=creerLSC(p,NULL);

    ListeSC Q;
  Q=L;
  
  for(i=0;i<l;i++)
  { p++;
    insererApresLSC(Q,L,p);
    
     L=L->succ;
  }
return Q;
}

ListeSC consListeIntervalle3(int l, int p){
  /*     Donn�e : l>0 */
  /*     Res : renvoie une liste intervalle de longueur l et dont le premier �l�ment a pour valeur p */
  /*     Complexit� : ???  */
  assert(l>0);

  /*
  ListeSC L;
  
  if (l==0)
    return insererDebutLSC(L,p);
    
  else 
  {
    return consListeIntervalle3(l-1,p-1); 
  }

  */
  return NULL;
}

void transfListeIntervalle(ListeSC L){
  /* Donn�e : L est une liste tri�e non vide  */
  /* Res : modifie L en y inserant des �l�ments de sorte qu'elle soit une Liste Intervalle */
  assert((L!=NULL));
  assert(estTrieeLSC(L));

  return;
}

ListeSC intersectionListesIntervalles(ListeSC l1, ListeSC l2){
  assert(estListeIntervalle(l1));
  assert(estListeIntervalle(l2));

  return NULL  ;
}
  


/*****************************************************************************/
/*                                                                           */
/*                              main                                         */
/*                                                                           */
/*****************************************************************************/
int main(int argc, char *argv[]){
  ListeSC l1,l2,l3;
  int q, lg, prem;
  clock_t t1,t2,t3, t4;

  cout << "Numero de la question traitee (1/2/3/4/5) ? ";
  cin >> q ;
    switch (q){
    case 1 :
      l1=lireLSC();
      if (estListeIntervalle(l1))
	cout << "Cette liste est une liste intervalle \n";
      else
	cout << "Cette liste n'est pas une liste intervalle \n";
      break;
      
    case 2 :
      cout << "Donnez 2 entiers strictement positifs (longueur et premier element de la liste intervalle) : ";
      cin >> lg >> prem;
      l1=consListeIntervalle1(lg,prem);
      afficherLSC(l1);
      l2=consListeIntervalle2(lg,prem);
      afficherLSC(l2);
      break;
    case 3 :
      cout << "Donnez 2 entiers strictement positifs (longueur et premier element de la liste intervalle) : ";
      cin >> lg >> prem;
      t1=clock();
      l1=consListeIntervalle1(lg,prem);
      t2=clock();
      l2=consListeIntervalle2(lg,prem);
      t3=clock();
      l3=consListeIntervalle3(lg,prem);
      t4=clock();
      
      cout << " Construction d'une Liste de taille " <<   lg 
	   << "\n  version 1 "  <<(double) (t2-t1)/CLOCKS_PER_SEC 
	   << " sec\n  version 2 " <<(double) (t3-t2)/CLOCKS_PER_SEC 
	   << " sec\n version 3 "<<(double) (t4-t3)/CLOCKS_PER_SEC<< " sec\n";
      break;
    case 4 : // Transformation d'une liste triee en liste Intervalle 
      cout << "Entrez une Liste Triee : ";
      l1=lireLSC();
      transfListeIntervalle(l1);
      cout << "Liste Intervalle construite ";
      afficherLSC(l1);
      break;
    case 5 : // intersection de 2 listes intervalle
      cout << "Liste intervalle : ";
      l1=lireLSC();
      cout << "Liste intervalle : ";
      l2=lireLSC();
      l3=intersectionListesIntervalles(l1,l2);
      cout << "Intersection : \n";
      afficherLSC(l3);
    }
    return 0;
}




