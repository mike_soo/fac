#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "outilsTab.h"

int* genTab(int n){
    int* t; int i;
    t=malloc(n*sizeof(int));
    for (i=0;i<n;i++) t[i]=-100+rand()%200;
    return t;
}

void afficheTab(int* T, int taille){
    int i;
    printf("\n");
    for (i=0;i<taille;i++) printf("%d ;   ",T[i]);
    printf("\n");
    }

void fichierTemps(char* nomFic, int tMaxTab, int pas, int (*f)(int*, int)){
    int taille;
    FILE* fichier = NULL;
    int* Tab;
    clock_t t1, t2;

    fichier = fopen(nomFic, "w");
    if (fichier != NULL)
    {
        for (taille=pas; taille<=tMaxTab; taille=taille+pas){
            Tab=genTab(taille);
            t1=clock();
            (*f)(Tab,taille);
            t2=clock();
            fprintf(fichier,"%d %f \n", taille,  (double)(t2-t1)/ CLOCKS_PER_SEC);
        }
        fclose(fichier);
    }

    return ;
}

int ssTabSomMax1(int* T, int taille){
    int somMax, som, i, j, k;
    somMax=0;
    for (i=0;i<taille;i++){
        for (j=i; j<taille; j++){
            som=0;
            for (k=i; k<=j; k++) {
                som = som + T[k];
            }
            if (som > somMax) somMax = som;
        }
    }
    return somMax;
}

int ssTabSomMax2(int* T, int taille){
    int somMax, som, i, j;
    somMax=0;
    for (i=0;i<taille;i++){
        som=0;
        for (j=i; j<taille; j++){
            som = som + T[j];
            if (som > somMax) somMax = som;
        }
    }
    return somMax;
}

int sTSM3(int* T,int g,int d){
    int m, som, i, smgd, smdg, smm, smg, smd;
    assert(g<=d);
    if (g==d){
        if (T[g]>0) return T[g]; else return 0;
    }
    else {
        m = (d+g)/2;
        smg=sTSM3(T,g,m);
        smd=sTSM3(T,m+1,d);
        smgd=0; som=0;
        for (i=m;i>=g;i--){
            som=som+T[i];
            if (som > smgd) smgd=som;
        }
        smdg=0; som=0;
        for(i=m+1;i<=d;i++){
            som=som+T[i];
            if (som>smdg) smdg=som;
        }
        smm=smgd+smdg;
        if ((smg>=smd) && (smg>=smm)) return smg;
        else {
            if (smd>=smm) return smd;
            else return smm;
        }
    }
}

int ssTabSomMax3(int* T, int taille){
    return sTSM3(T,0,taille-1);
}

int ssTabSomMax4(int* T, int taille){
  int i,max;
  int *m=malloc(taille*sizeof(int));
    max=T[0];
    m[0]=T[0];
    for (i=2;i<(taille-1);i++)
      {if (m[i-1]>0)
	  {m[i]=m[i-1]+T[i];}
	else 
          {m[i]=T[i];}
	if (m[i]>max)
	  {max=m[i];}
      }    
  
 
    return max;
}

struct triplet indSsTabSomMax(int* T, int taille){
/* A COMPLETER */
    struct triplet  res;
    res.somMax=0; res.deb=0; res.fin=0;
    return res;
}


void rangerElemNeg(int* T,int taille){
/* A COMPLETER */
   return;

}
