#include <iostream>
#include <stdlib.h>
#include <tgmath.h>
#include <stdio.h>
#include "triTas.hpp"
using namespace std;

int triTas::sag(int v)
{int resInd=2*v+1;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resInd;
 
}

int triTas::sad(int v)
{int resInd=2*v+2;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resInd;

}

int triTas::pere(int v)
{
 int resInd=(v-1)/2;
  
 if(v<0||v>=n||resInd>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return resInd;
}

bool triTas::estFeuille(int v)
{
	int resInd=(n-1)/2;
  
 if(v<0||v>=n||resInd<0)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
 	return v>resInd;
}

/*int triTas::profondeurA(int v)
{
	
  
 if(v<0||v>=n)
	{
		//cout<<"la celulle n'est pas dans le tab"<<endl;
		return -1;
	}
 else 
	{
	 int resProf=log2(v+1);	
 	 return resProf;
    }
}*/

void triTas::echanger(int v1,int v2)
{
	int inter;
	inter=Tab[v1];
	Tab[v1]=Tab[v2];
	Tab[v2]=inter;	
}

void triTas::triHaut(int v)
{
    if(v==0)
    {
     	return;
    }

    else if(Tab[pere(v)]<Tab[v])
    {
    	echanger(pere(v),v);
 		triHaut(pere(v));
 		return;
    }

    else 
    {
    	triHaut(pere(v));
    	return;
    }

}

void triTas::triTabRec(int v,int r)
{cout<<"r="<<r<<endl;
 cout<<"v="<<v<<endl<<endl;
 

 if(estFeuille(r)||v==-1||r==-1)
	{//cout<<"finito"<<endl;
	 return;
	}

 /*if(v==n-1)
	{ cout<<"je suis dans v==n-1"<<endl;
 	  //cout<<"r="<<r<<endl;
	 if(Tab[v]>Tab[r])
		{   
			echanger(v,r);
			//cout<<"j'echange dans 1 "<<Tab[v]<<"par"<<Tab[r]<<endl;
		 	//cout<<" r="<<r<<endl;
		 	triTabRec(sag(r),sag(r));
		 	
		 	triTabRec(sad(r),sad(r));
		 	return ;
		}
	 else
	 	{
	 		;//cout<<" r="<<r<<endl;
		 	triTabRec(sag(r),sag(r));
		 	triTabRec(sad(r),sad(r));
		 	return ;
	 	}
	}*/
 
 else if(estFeuille(v))
 	 {
		if(Tab[v]>Tab[r])
 		{
 		  echanger(v,r);
 		  triTabRec(sag(r),sag(r));
		  triTabRec(sad(r),sad(r));
	  	  return;
	  	}
	  	else 
	    { 
	      triTabRec(sag(r),sag(r));
		  triTabRec(sad(r),sad(r));
	      return;
	    }
 	}

 else if(Tab[v]>Tab[r])
	{	
		echanger(v,r);
		//cout<<"j'echange dans 2 "<<Tab[v]<<"par"<<Tab[r]<<endl;
		triTabRec(sag(v),r);
		triTabRec(sad(v),r);
		return ;
	}

	else
	{   
		
		triTabRec(sag(v),r);
		triTabRec(sad(v),r);
		return;
	}
}

void triTas::triTabTasse(int v)
{
 if(estFeuille(v))
 	{
 	 return ;
 	}

 else 
  	{
  	 int vSup;
  	 if((Tab[sag(v)]>Tab[v]))
  	 	{
  	 	 echanger(sag(v),v);
  	 	 vSup=sag(v);
  	 	}
  	 if((Tab[sad(v)]>Tab[v]))
  	 	{
  	 	 echanger(sad(v),v);
  	 	 vSup=sad(v);
  	 	}
  	 triTabTasse(vSup);
  	 return ;
 	}
}

void triTas::afficheTab(int _n)
{for(int i=0;i<_n;i++)
   	{
 		cout<<" "<<Tab[i];
   	}
 cout<<endl;
}

triTas::triTas(int *T,int _n)
{   n=_n;
	Tab=new int[n];
	for(int i=0;i<n;i++)
	{
		Tab[i]=T[i];
	}

   
	triTabRec(0,0);
	cout<<"j'affiche TabTrié1:"<<endl;
 	
    afficheTab(_n);
    cout<<endl;
	
	/*while(n>0)
    {cout<<"jeboucle"<<endl;
     echanger(Tab[n-1],Tab[0]);
     afficheTab(_n);
     n--;
     triTabTasse(0);
    }

	
	cout<<"j'affiche TabTriéFinal:"<<endl;
 
    afficheTab(_n);*/
}



int main(int argc,char**argv)
{ 
 if(argc==2 && argv[1][0]=='r')
	{
 	 int taille;
     srand (time(NULL));
     cout<<"taille?"<<endl;
     cin>>taille; 

	 int *tabMain=new int[taille];

 	 for(int i=0;i<taille;i++)
 	 {
 		tabMain[i]=rand()%(taille+30);
 	 }
 	 cout<<"j'affiche TabMain:"<<endl;
 	
 	 for(int i=0;i<taille;i++)
 	 {
 		cout<<" "<<tabMain[i];
  	 }
          	
     cout<<endl;
     triTas t(tabMain,taille);
   }
 
 else if(argc==2 && argv[1][0]=='c')
   { int e;
   	 int taille;
	 cout<<"taille?"<<endl;
	 cin>>taille; 
   	 int *tabMain=new int[taille];
   	 for(int i=0;i<taille;i++)
   		{
   			cin>>e;
   			tabMain[i]=e;
   		}
   	 cout<<"j'affiche TabMain:"<<endl;
 	
 	 for(int i=0;i<taille;i++)
 	 {
 		cout<<" "<<tabMain[i];
  	 }
          	
     cout<<endl;
     triTas t(tabMain,taille);

   }

  else if(argc==1)
  	{ 
  	 int tabMain[4]={28,3,11,9};
  	 cout<<"j'affiche TabMain:"<<endl;
 	
 	 for(int i=0;i<4;i++)
 	 {
 		cout<<" "<<tabMain[i];
  	 }
  	 cout<<endl;
  	 triTas t(tabMain,4);

  	}


	return 0;
}