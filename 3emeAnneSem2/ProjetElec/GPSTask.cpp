/*
 * GPSTask.cpp
 *
 *  Created on: 9 f�vr. 2013
 *	Author: Benjamin Navarro
 */

#include "tasks.h"

// Global variables
// Task handle and GPS data
xQueueHandle 	GPSQueue;
nmeaINFO 		GPSInfo;
nmeaPOS			GPSPos;

void USART_Configuration();

/**
  * @brief  GPS Task. Initialize the USART and the NMEA parser then wait for incomming messages to parse them
  * @param  None
  * @retval None
  */
void GPSTask(void * pvArg) {

	// Buffer to hold a sentence received from the GPS
	char sentence[NMEA_SENTENCE_LENGTH];

	// Create a parser data structure
	nmeaPARSER parser;

	// Set all fileds of GPSInfo to 0
	nmea_zero_INFO(&GPSInfo);
	// Intit the parser
	nmea_parser_init(&parser);
	// Configure the USART
	USART_Configuration();

	while(1) {
		// While there are some sentences to decode
		while(uxQueueMessagesWaiting(GPSQueue)) {
			// Get the first sentence in the queue
			xQueueReceive(GPSQueue, sentence, 0);
			// Parse it
			nmea_parse(&parser, sentence, NMEA_SENTENCE_LENGTH, &GPSInfo);
			// Get the position from it
			nmea_info2pos(&GPSInfo, &GPSPos);

		}
		vTaskDelay(100);
	}
}

/**
  * @brief  Initialize the USART3 at 9600bauds 8N1
  * @param  None
  * @retval None
  */
void USART_Configuration() {
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3, ENABLE);

	/*
	 * 	STM32 <-> GPS USART
	 * 	USART3_TX -> PD8 , USART3_RX ->	PD9
	 */

	// Set the alternate function of PD8 and PD9 to the USART
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_USART3);

	// Configure USART Tx as alternate function
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	// Configure USART Rx as alternate function
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	// Configure the USART
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;

	// Init the USART with the previous configuration
	USART_Init(USART3, &USART_InitStructure);

	// Enable the USART
	USART_Cmd(USART3, ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure;

	// Enable the USART3 RXNE Interrupt (non empty data received)
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);

}

