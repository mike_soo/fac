/*
 * vTaskSend.c
 *
 *  Created on: 6 mai 2016
 *      Author: jouvencel
 */
#include "vTaskSend.h"


SemaphoreHandle_t xReceived_semaphore; //jamais utiliser dans ce fichier!

void Init_DMA_Send0(void) { /*
 *********************************************************
 * initialisation USART1
 *  Tx pour la transmission : source --> TX
 *  RX pour la réception : Rx --> destination
 *********************************************************
 */
	// création d'une queue pour TX
		xQueueG = xQueueCreate(10,sizeof (struct Message *));
	// horloge bus et gpio
	USART_Cmd(USART1, DISABLE);
	USART_DeInit(USART1);
	DMA_Cmd(DMA1_Channel4, DISABLE);
	DMA_DeInit(DMA1_Channel4);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

	// iniitialisation de la structure GPIO_InitStructUSART
	GPIO_InitTypeDef GPIO_InitStructUSART;
//USART 1 dispo PC4 TX et PC5 RX  compatible avec gyro
// 			dispo PC4 et PC5
	GPIO_InitStructUSART.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStructUSART.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructUSART.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructUSART.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructUSART.GPIO_PuPd = GPIO_PuPd_UP;

	// initialisation  registres du ARM 
	GPIO_Init(GPIOC, &GPIO_InitStructUSART);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource4, GPIO_AF_7);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource5, GPIO_AF_7);

	// initiailisation USART
	USART_InitTypeDef USART_InitStruct; // déclaration structure 

	USART_InitStruct.USART_BaudRate = 57600;   // variable initialisée 
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	//Activation
	USART_Init(USART1, &USART_InitStruct);
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);  //on valide IT réception
	USART_Cmd(USART1, ENABLE);
	// à voir si ligne suivante est utile
	USART_ClearFlag(USART1, USART_FLAG_TC);
}

/*
 * seconde partie init dma
 *
 */
void Init_DMA_Send(uint32_t Long, void * AdrMes) {
//	/**********************************************************
//	 * Initialisation DMA1 canal 4 tx
//	***********************************************************/

			// Horloge DMA1
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	// DMA structure
	DMA_InitTypeDef DMA_InitStructure;

	//reset DMA1 canal 4 valeurs par défaut ; utilisé mémoire -->TX
	DMA_DeInit(DMA1_Channel4);
	//transfert  mémoire périphérique donc M2M disable
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	//mode  normal et non circulaire
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	//priorité medium (j'essaierai DMA_Priority_VeryHigh/DMA_Priority_High/DMA_Priority_Low
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	//source and destination data size word=32bits
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	//incrémentation automatique memoire source mais pas pour la destination
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	//le périphérique est la destination
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	//data à transmettre
	DMA_InitStructure.DMA_BufferSize = Long;
	//adresse de départ source et adresse de destination
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART1->TDR); // 0x40004828;//&USART3->TDR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) AdrMes;
	//send structure aux registres du  DMA
	DMA_Init(DMA1_Channel4, &DMA_InitStructure);
	// init IT DMA1 Channel pour Transfer Complet
	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
   //DMA_Cmd(DMA1_Channel4, ENABLE);
	NVIC_InitTypeDef NVIC_InitStructure;
	//Enable DMA1 channel IRQ Channel */
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0xF;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0xF;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}
/*********************************************************************************
 * vTaskSendœ
 */
 
//
void vTask_Send(void * pArg) {

	struct Message Tampon, *pxMess;
	uint32_t Long;
	vSemaphoreCreateBinary(xDMATX_semaphore);
	Long = sizeof(struct Message);
	//init 1 fois le DMA avec une adresse d'un buffer tempon et une longueur Long
	Init_DMA_Send0();
	for (;;) 
	{

		xQueueReceive(xQueueG, (void * ) &pxMess, portMAX_DELAY); 
		// si la queue est vide blocage
		// copie du message dans le tampon

		Tampon.Mes[0] = pxMess->Mes[0];
		Tampon.Mes[1] = pxMess->Mes[1];
		Tampon.Mes[2] = pxMess->Mes[2];
		Tampon.buffer[0] = pxMess->buffer[0];
		Tampon.buffer[1] = pxMess->buffer[1];
		Tampon.buffer[2] = pxMess->buffer[2];
		Tampon.buffer[3] = pxMess->buffer[3];
		//init dma
		Init_DMA_Send(Long, &Tampon);
		// activation DMA
		DMA_Cmd(DMA1_Channel4, ENABLE);
		//attente fin de transfert, indiqué par le SB de DMA1_Channel4_IRQHandler()
		xSemaphoreTake(xDMATX_semaphore, portMAX_DELAY);
		
		//on arrête le DMA
		// on attend que TC4 passe à 1
		//pour test
		//int flag;
		//flag =DMA_GetFlagStatus(DMA1_FLAG_TC4);  // Attention attendre que TC4 passe à 1
		while (DMA_GetFlagStatus(DMA1_FLAG_TC4)) 
		{}
		DMA_ClearFlag(DMA1_IT_TC4);
		DMA_Cmd(DMA1_Channel4, DISABLE);
	}
}

void DMA1_Channel4_IRQHandler(void) {
	static signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	DMA_ClearITPendingBit(DMA1_IT_GL4);
	STM_EVAL_LEDToggle(LED9);
	xSemaphoreGiveFromISR(xDMATX_semaphore, &xHigherPriorityTaskWoken);
	if (xHigherPriorityTaskWoken == pdTRUE) {
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}
