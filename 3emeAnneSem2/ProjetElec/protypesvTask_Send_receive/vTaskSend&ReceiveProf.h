/*
 * vTaskSend.h
 *
 *  Created on: 6 mai 2016
 *      Author: jouvencel
 */

#ifndef INCLUDE_VTASKSEND_H_
#define INCLUDE_VTASKSEND_H_
 


xSemaphoreHandle xDMARX_semaphore;
xSemaphoreHandle xDMATX_semaphore;

//uint8_t Mes[ARRAYSIZE_MES];

void vTask_Send(void * pArg);
void Init_DMA_Send(uint32_t Long, void * AdrMes);
//void vTask_Receive(void * pArg);
struct Message  //envoyés vers le PC
{
 char  Mes[3];  //message du type Mx:   x=A acc; G pqr; m mag; E encoder; B batterie; I courant
 float buffer[4];
};

QueueHandle_t xQueueG;
#endif /* INCLUDE_VTASKSEND_H_ */
