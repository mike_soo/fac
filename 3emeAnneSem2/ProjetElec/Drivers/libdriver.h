/*
 * libdriver.h
 *
 *  Created on: 31 déc. 2016
 *      Author: jouvencel
 */

#ifndef LIBDRIVER_H_
#define LIBDRIVER_H_
/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"

#include "stm32f3_discovery.h"
#include "stm32f30x_exti.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_adc.h"
#include "stm32f30x_can.h"
#include "stm32f30x_crc.h"
#include "stm32f30x_comp.h"
#include "stm32f30x_dac.h"
#include "stm32f30x_dbgmcu.h"
#include "stm32f30x_dma.h"
#include "stm32f30x_exti.h"
#include "stm32f30x_flash.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_syscfg.h"
#include "stm32f30x_i2c.h"
#include "stm32f30x_iwdg.h"
#include "stm32f30x_opamp.h"
#include "stm32f30x_pwr.h"
#include "stm32f30x_rcc.h"
#include "stm32f30x_rtc.h"
#include "stm32f30x_spi.h"
#include "stm32f30x_tim.h"
#include "stm32f30x_usart.h"
#include "stm32f30x_wwdg.h"
#include "stm32f30x_misc.h"

//#include "FreeRTOSConfig.h"

#include "FreeRTOS.h"
#include "FreeRTOS_Hooks.h"
#include "task.h"
#include "list.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro.h"


/*****************************************************/
/*****************************************************
 * Mesure signal PWM
 * données dans frequence et rapport
 */
void Task_Mesure1(void*pArg);
extern float frequence, rapport;
/*****************************************************/



/****************************************************************
 * sortie d'un sinus sur PA4, frequence réglable 100 échantillons
 * on appel une fois cette fonction ce n'est pas une tâche
 * les circuits programmés pour générer un sinus fonctionnent
 * indépendamment de l'UC
 */
void Config_DAC(uint16_t frequence);
/*************************************************************/



/*************************************************************
 * génération pwm sur PD3
 * freq 100 et 500 Hz prec 50 100 valeur 10 1000
 * l'initilisation n'a lieu qu'une fois
 **/
void TPWM_Init(uint32_t freq, uint16_t valeur);
void cTask_TPWM( void *pArg );
extern uint16_t pwm;
/**********************************************************/

/**********************************************************
 * ADC
 *
 */
extern uint16_t Value_ADCConVoltage,Value_ADCConVoltage2;
/*************************************
Acquisition continue
	résolution  12 bits
	entrée pc4 vers canal 5 indépendant de l'ADC2
	période échantillonnage 181 cycles
	horloge 36MHz
	canal 5 ADC2
	acquisition toutes les xTicksToDelay ms
*/
void Init_TADC_C(const TickType_t Delay);
void sTask_TADC_C(void * pArg );
//valeur convertie dans
extern uint16_t Value_ADCConVoltage;


/*************************************************/
/* acquistion en discontinu
 * sur PC4
 * horloge 36MHz
 * tps conversion  181Cycles5
 * résolution 12 bits
 */
void Init_TADC_D(const  TickType_t xTicksToDelay);
/*
 * prototype de fonction
 */
//recopier à partir de la lligne ci=dessous
// void sTask_TADC_D(void * pArg )
//{	Init_TADC_D();
//	  for(;;)
//
//	  {	 ADC_StartConversion(ADC);
//	  	  while(ADC_GetFlagStatus(ADC, ADC_FLAG_EOC) == RESET);
//	  	Value_ADCConvertedValue =ADC_GetConversionValue(ADC);
//	  	Value_ADCConVoltage = (Value_ADCConvertedValue *3300)/0xFFF;
//		vTaskDelay(500);
//	  }
//	}
//fin de la fonction prototype

/* *********************************************************************
 * acquisition sur PC2 et PC4 ADC2 canal 5 et 8 gestion par DMA1 canal 1
 *
 ***********************************************************************/
void sTask_ADCDMA(void * pArg );
//les valeurs sont dans
extern uint16_t Value_ADCConVoltage, Value_ADCConVoltage2;

/***********************************************************************
 * send sur USART1
 **********************************************************************/

void vTask_Send(void * pArg);
//la tâche attend sur xQueueG
extern QueueHandle_t xQueueG;
// passer dans la queue l'adresse d'une structure
// déclarer une variable struct Message MonMessage
// déclarer un pointeur struct Message pMonMessage
// mettez les valeurs à envoyer dans MonMessage
// pMonMessage = &MonMessage
// solliciter la xQueueG
/*
 *  if (xQueueSendToBack(xQueueG, ( void * ) &pMonMessage, portMAX_DELAY) != pdPASS )
	        		{}
 */
extern struct Message
{
 char  Mes[3];
 float buffer[4];
};



/****************************************************
 * encodeur
 * canal A pc6 et pc7
 * canal 2 pd12 et pd13
 * TIM3 et TIM4
**************************************************** */
void sTask_TQencoder (void * pArg);
extern float speedG, speedD;

#endif /* LIBDRIVER_H_ */
