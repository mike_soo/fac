fonctions utilisées recherches:
//====================>in stm32f30x_gpio.c<====================//
/*
(#) Peripherals alternate function:
        (++) For ADC, DAC and comparators, configure the desired pin in 
             analog mode using GPIO_InitStruct->GPIO_Mode = GPIO_Mode_AN
        (++) For other peripherals (TIM, USART...):
             (+++) Connect the pin to the desired peripherals' Alternate 
                   Function (AF) using GPIO_PinAFConfig() function.
             (+++) Configure the desired pin in alternate function mode using
                   GPIO_InitStruct->GPIO_Mode = GPIO_Mode_AF
             (+++) Select the type, pull-up/pull-down and output speed via 
                   GPIO_PuPd, GPIO_OType and GPIO_Speed members.
             (+++) Call GPIO_Init() function.
    (#) To get the level of a pin configured in input mode use GPIO_ReadInputDataBit()
    (#) To set/reset the level of a pin configured in output mode use
        GPIO_SetBits()/GPIO_ResetBits()
    (#) During and just after reset, the alternate functions are not active 
        and the GPIO pins are configured in input floating mode (except JTAG pins).
    (#) The LSE oscillator pins OSC32_IN and OSC32_OUT can be used as 
        general-purpose (PC14 and PC15, respectively) when the LSE
        oscillator is off. The LSE has priority over the GPIO function.
    (#) The HSE oscillator pins OSC_IN/OSC_OUT can be used as general-purpose 
        (PF0 and PF1 respectively) when the HSE oscillator is off. The HSE has 
        the priority over the GPIO function.  
/**
  * @brief  Initializes the GPIOx peripheral according to the specified 
  *         parameters in the GPIO_InitStruct.
  * @param  GPIOx: where x can be (A, B, C, D, E or F) to select the GPIO peripheral.
  * @param  GPIO_InitStruct: pointer to a GPIO_InitTypeDef structure that 
  *         contains the configuration information for the specified GPIO
  *         peripheral.
  * @note   GPIO_Pin: selects the pin to be configured:
  *         GPIO_Pin_0->GPIO_Pin_15 for GPIOA, GPIOB, GPIOC, GPIOD and GPIOE;
  *         GPIO_Pin_0->GPIO_Pin_2, GPIO_Pin_4, GPIO_Pin_6, GPIO_Pin_9 
  *                       and GPIO_Pin_10 for GPIOF.
  * @retval None
  */
void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_InitStruct);



/**
  * @brief  Writes data to the specified GPIO data port.
  * @param  GPIOx: where x can be (A, B, C, D, E or F) to select the GPIO peripheral.
  * @param  GPIO_PinSource: specifies the pin for the Alternate function.
  *   This parameter can be GPIO_PinSourcex where x can be (0..15).
  * @param  GPIO_AF: selects the pin to be used as Alternate function.  
  *   This parameter can be one of the following value:
  *     @arg GPIO_AF_0:  JTCK-SWCLK, JTDI, JTDO/TRACESW0, JTMS-SWDAT, MCO, NJTRST, 
  *                      TRACED, TRACECK.
  *     @arg GPIO_AF_1:  OUT, TIM2, TIM15, TIM16, TIM17.
  *     @arg GPIO_AF_2:  COMP1_OUT, TIM1, TIM2, TIM3, TIM4, TIM8, TIM15.
  *     @arg GPIO_AF_3:  COMP7_OUT, TIM8, TIM15, Touch.
  *     @arg GPIO_AF_4:  I2C1, I2C2, TIM1, TIM8, TIM16, TIM17.
  *     @arg GPIO_AF_5:  IR_OUT, I2S2, I2S3, SPI1, SPI2, TIM8, USART4, USART5
  *     @arg GPIO_AF_6:  IR_OUT, I2S2, I2S3, SPI2, SPI3, TIM1, TIM8
  *     @arg GPIO_AF_7:  AOP2_OUT, CAN, COMP3_OUT, COMP5_OUT, COMP6_OUT, USART1, 
  *                      USART2, USART3.
  *     @arg GPIO_AF_8:  COMP1_OUT, COMP2_OUT, COMP3_OUT, COMP4_OUT, COMP5_OUT, 
  *                      COMP6_OUT.
  *     @arg GPIO_AF_9:  AOP4_OUT, CAN, TIM1, TIM8, TIM15.
  *     @arg GPIO_AF_10: AOP1_OUT, AOP3_OUT, TIM2, TIM3, TIM4, TIM8, TIM17. 
  *     @arg GPIO_AF_11: TIM1, TIM8.
  *     @arg GPIO_AF_12: TIM1.
  *     @arg GPIO_AF_14: USBDM, USBDP.
  *     @arg GPIO_AF_15: OUT.             
  * @note  The pin should already been configured in Alternate Function mode(AF)
  *        using GPIO_InitStruct->GPIO_Mode = GPIO_Mode_AF
  * @note  Refer to the Alternate function mapping table in the device datasheet 
  *        for the detailed mapping of the system and peripherals alternate 
  *        function I/O pins.
  * @retval None
  */
void GPIO_PinAFConfig(GPIO_TypeDef* GPIOx, uint16_t GPIO_PinSource, uint8_t GPIO_AF)


//====================>in stm32f30x_usart.c<====================//



/**
  * @brief  Checks whether the specified USART interrupt has occurred or not.
  * @param  USARTx: Select the USART peripheral. This parameter can be one of the 
  *         following values: USART1 or USART2 or USART3 or UART4 or UART5.
  * @param  USART_IT: specifies the USART interrupt source to check.
  *         This parameter can be one of the following values:
  *         @arg USART_IT_WU:  Wake up interrupt.
  *         @arg USART_IT_CM:  Character match interrupt.
  *         @arg USART_IT_EOB:  End of block interrupt.
  *         @arg USART_IT_RTO:  Receive time out interrupt.
  *         @arg USART_IT_CTS:  CTS change interrupt.
  *         @arg USART_IT_LBD:  LIN Break detection interrupt.
  *         @arg USART_IT_TXE:  Tansmit Data Register empty interrupt.
  *         @arg USART_IT_TC:  Transmission complete interrupt.
  *         @arg USART_IT_RXNE:  Receive Data register not empty interrupt.
  *         @arg USART_IT_IDLE:  Idle line detection interrupt.
  *         @arg USART_IT_ORE:  OverRun Error interrupt.
  *         @arg USART_IT_NE:  Noise Error interrupt.
  *         @arg USART_IT_FE:  Framing Error interrupt.
  *         @arg USART_IT_PE:  Parity Error interrupt.
  * @retval The new state of USART_IT (SET or RESET).
  */
ITStatus USART_GetITStatus(USART_TypeDef* USARTx, uint32_t USART_IT);