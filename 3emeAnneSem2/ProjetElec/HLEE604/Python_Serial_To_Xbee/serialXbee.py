#!/usr/bin/python
#coding=utf-8

import serial 
import time
import sys
class SerialCommunicator():

	def __init__(self,pathToSerial,baudrate=9600):
		self.ser = serial.Serial(pathToSerial,baudrate, timeout=3, writeTimeout=3)
		time.sleep(3);

	#Fonction assurant l'envoi d'une donnée
	def send(self,c):
		try:
			self.ser.write(c)
			r=self.ser.read()
			# print "wrote: " + c
			# print "read: " + r
		except :
			print "SerialCommunicator.send() : exception !\n"

	def recv(self):
		incomingChar="-"
		try:
			incomingChar=self.ser.read()
			if incomingChar != "-" : 
				#print "SerialCommunicator.recv() : received " + incomingChar +"\n"
				return incomingChar
			else:
				return "-"
		except:
			print "SerialCommunicator.recv() :  error\n"
			return "-"
	
	def listen(self):
		while 1:
			incomingByte = self.recv();
			print "recv: "+incomingByte

	def speak(self):
		c='a'
		inc='1'
		while 1:
			self.send(c)
			c=chr(ord(c) +1)
			if c>122 :
				c='a'
			
			while inc!='0': 
				inc = self.recv()
				if inc != '-':
					print "SerialCommunicator.speak() : send and recv " +inc + " succeeded\n"
				else:
					print "SerialCommunicator.speak() : send and recv " + inc +" failed\n"

if __name__ == '__main__':
	pathToSerial="/dev/ttyUSB0"
	baudrate=57600
	serial = serial.Serial(pathToSerial,baudrate, timeout=3, writeTimeout=3)
	time.sleep(2);
	serial.write("+++")
	msgr=serial.read()
	
	print msgr
	msge=''
	while 1: 
		
		try:
			msge=raw_input(">")
		except:
			serial.close()
			break

		
		try: 
			serial.write(msge + '\r')
		
		except Exception as e :
			print e

		
		try:
			msgr=serial.read()
			print msgr
		
		except Exception as e :

			print "exc pendant read"
			print e
			print "msge: " + msge 
			print "msgr: " + msgr
			print "fin exc"

	
	#rôle receveur
	# if role = r:
	# 	serial.write("+++")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATRE")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATID1111")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATMY0")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATDL1")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATIR")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATIT")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATIU")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATIA")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATD0")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATP0")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATD1")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATD1")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATWR")
	# 	recv=serial.read()
	# 	print recv

	# 	serial.write("ATCN")


		# emet	recep
# +++ 			
# ATRE 					Restaure les paramètres par défaut
# ATID 	1111 	1111 	Adresse du réseau
# ATMY 	1 		0 		Adresse du module dans le réseau
# ATDL 	0 		1 		Adresse du destinataire dans le réseau
# ATIR 	14 		- 		Taux d'échantillonnage 20ms (14 en hexadecimal) (p.43 du manuel)
# ATIT 	5 		- 		Nombre d'échantillons à effectuer avant l'envoi des données
# ATIU 	1 		- 		I/O output enabled : autoriser émission des I/O sans passer par l'UART
# ATIA 	- 		1 		I/O input from address 1
# ATD0 	2 		- 		POTENTIOMETRE : D0 pour pin20 (DIO0, AD0) et 2 pour ADC (p.12, p.39)
# ATP0 	- 		2 		LED : P0 pour PWM 0 et 2 pour PWM mode (p.31)
# ATD1 	3 				BOUTON : D1 pour pin19 (DIO1, AD1) et 3 pour Digital Input
# ATD1 			4 		LED : D1 pour pin19 (DIO1, AD1) et 4 pour Digital Out Low Support
# ATWR 					Écrit la nouvelle configuration dans la mémoire flash du module
# ATCN 	- 		- 		Sort du mode configuration