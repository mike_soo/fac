/**
  ******************************************************************************
  * @file    stm32f3xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"
#include "stm32f3xx.h"
#ifdef USE_RTOS_SYSTICK
#include <cmsis_os.h>
#endif
#include "stm32f3xx_it.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            	  	    Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles SysTick Handler, but only if no RTOS defines it.
  * @param  None
  * @retval None
  */


int freq2HZ = 0;
char freq10HZ = 0;
char f = 0;

int intervalcnt = 0;
char firstclick = 0;


void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();

	if (firstclick == 1) {
		if (intervalcnt >= 1000) {
			firstclick = 0;
			intervalcnt = 0;
		}
		else
			intervalcnt++;

	}
	if (f == 0) {
		if (freq2HZ == 499) { // 2HZ
			BSP_LED_Toggle (LED4);
			freq2HZ = 0;
		}
		else
			freq2HZ++;
	}
	else {
		if (freq10HZ == 99) { // 10HZ
			BSP_LED_Toggle (LED4);
			freq10HZ = 0;
		}
		else
			freq10HZ++;


	}
#ifdef USE_RTOS_SYSTICK
	osSystickHandler();
#endif
}



void EXTI0_IRQHandler(void) {


	if (firstclick == 0) {

		firstclick = 1;

	}
	else  {
		if (intervalcnt < 1000) { // counts until 1s at maximum
			// second click
			f = (f + 1) % 2;
		}
		intervalcnt = 0;
		firstclick = 0;

	}

	LL_EXTI_ClearFlag_0_31 (LL_EXTI_LINE_0);
	/* mandatory function to clear the Interrupt request */
}

