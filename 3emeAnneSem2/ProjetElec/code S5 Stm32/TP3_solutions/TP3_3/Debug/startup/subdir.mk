################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../startup/startup_stm32f303xc.s 

OBJS += \
./startup/startup_stm32f303xc.o 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo %cd%
	arm-none-eabi-as -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib" -I"C:/Users/bosio/workspace/TP3_3/inc" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/CMSIS/core" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/CMSIS/device" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/HAL_Driver/Inc/Legacy" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/HAL_Driver/Inc" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/Utilities/Components/Common" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/Utilities/Components/l3gd20" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/Utilities/Components/lsm303dlhc" -I"C:/Users/bosio/workspace/stm32f3discovery_hal_lib/Utilities/STM32F3-Discovery" -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


