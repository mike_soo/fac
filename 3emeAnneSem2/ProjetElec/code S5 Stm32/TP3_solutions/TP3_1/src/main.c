/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"
#include "stm32f3_discovery.h"
			

int main(void)
{

	BSP_LED_Init (LED7);

	BSP_PB_Init (BUTTON_USER, BUTTON_MODE_EXTI);


	while (1);

}
