/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"
#include "stm32f3_discovery.h"
			

int main(void)
{

	BSP_LED_Init (LED3);
	BSP_LED_Init (LED4);
	BSP_LED_Init (LED6);
	BSP_LED_Init (LED10);
	BSP_LED_Init (LED9);
	BSP_LED_Init (LED7);
	BSP_LED_Init (LED5);
	BSP_LED_Init (LED8);

	if (SysTick_Config(SystemCoreClock / 2)) {
		/* Capture error */
		BSP_LED_On (LED3);
	}


	while (1);

}
