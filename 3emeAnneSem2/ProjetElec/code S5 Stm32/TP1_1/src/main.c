/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"
#include "stm32f3_discovery.h"
			
#include <string.h>

int main(void)
{
	char mot[30] = "otto";

	int i,j, palindrome;

	i = 0;
	j = strlen (mot) - 1;
	palindrome = 1;
	while (i <= j) {


		if (mot[i] != mot[j])
			palindrome = 0;
		i++;
		j--;
	}

	BSP_LED_Init (LED7);
	BSP_LED_Init (LED4);
	if (palindrome)
		BSP_LED_On (LED4);
	else
		BSP_LED_On (LED7);

	for(;;);
}
