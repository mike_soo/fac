/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"
#include "stm32f3_discovery.h"



int main(void)
{


	BSP_LED_Init (LED4);
	BSP_LED_Init (LED3);

	BSP_PB_Init (BUTTON_USER, BUTTON_MODE_EXTI);

	if (SysTick_Config(SystemCoreClock / 1000)) { // 1 tick each millisecond
		/* Capture error */
		BSP_LED_On (LED3);
	}
	else
		BSP_LED_On (LED4);



	while (1);

}
