/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"
#include "stm32f3_discovery.h"
#include "stm32f3xx_it.h"


uint32_t Address = 0;
uint32_t PageError = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

/*Variable used for Erase procedure*/
FLASH_EraseInitTypeDef EraseInitStruct;



char save_db = 0;

int RecPeriod = 0;


char counterDB = 0;

int intervalcnt = 0;
char firstclick = 0;


int main(void)
{


	BSP_LED_Init (LED4);
	BSP_LED_Init (LED5);


	Address = FLASH_USER_START_ADDR;
	data32 = *(__IO uint32_t*)Address;



	BSP_PB_Init (BUTTON_USER, BUTTON_MODE_EXTI);

	if (SysTick_Config(SystemCoreClock / 1000)) { // 1 tick each millisecond
		/* Capture error */
		BSP_LED_On (LED5);
	}
	else {

		if (data32 == DATA_32) {   // data previously written

			save_db = 1;
		}
		else
			BSP_LED_On (LED4);

	}

	while (1);

}
