/**
  ******************************************************************************
  * @file    stm32f3xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
//#include "stm32f3xx_hal.h"
#include "stm32f3xx.h"
#ifdef USE_RTOS_SYSTICK
#include <cmsis_os.h>
#endif
#include "stm32f3xx_it.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            	  	    Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles SysTick Handler, but only if no RTOS defines it.
  * @param  None
  * @retval None
  */


void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();

	if (RecPeriod < 30000) { // 30 seconds

		RecPeriod++;

		// double click

		if (firstclick == 1) {
			if (intervalcnt >= 1000) { // 1 second
				firstclick = 0;
				intervalcnt = 0;
			}
			else
				intervalcnt++;

		}


		// end double click


	}
	else {
		if (save_db == 0) {
			BSP_LED_Off (LED4);

			// save DBcounter in flash

			/* Unlock the Flash to enable the flash control register access *************/
			HAL_FLASH_Unlock();

			/* Erase the user Flash area
			  (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

			/* Fill EraseInit structure*/
			EraseInitStruct.TypeErase = TYPEERASE_PAGES;
			EraseInitStruct.PageAddress = FLASH_USER_START_ADDR;
			EraseInitStruct.NbPages = (FLASH_USER_END_ADDR - FLASH_USER_START_ADDR)/FLASH_PAGE_SIZE;

			HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);


			/* Program the user Flash area word by word
				(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

			Address = FLASH_USER_START_ADDR;


			HAL_FLASH_Program(TYPEPROGRAM_WORD, Address, DATA_32 );
			Address = Address + 4;
			HAL_FLASH_Program(TYPEPROGRAM_WORD, Address, counterDB );




			/* Lock the Flash to disable the flash control register access (recommended
				 to protect the FLASH memory against possible unwanted operation) *********/
			HAL_FLASH_Lock();

			save_db = 1;

			// end flash
		}
	}


#ifdef USE_RTOS_SYSTICK
	osSystickHandler();
#endif

}


void EXTI0_IRQHandler(void) {


	if (firstclick == 0) {

		firstclick = 1;

	}
	else  {
		if (intervalcnt < 1000) { // counts until 1s at maximum
			// second click

			if (save_db == 0)
				counterDB++;
			else  {
				// read value from flash
				Address = FLASH_USER_START_ADDR + 4;
				data32 = *(__IO uint32_t*)Address;

				BSP_LED_Init(LED3);
				BSP_LED_Init(LED7);
				if (data32 < 5)
					BSP_LED_On(LED3);
				else
					BSP_LED_On(LED7);
			}

		}
		intervalcnt = 0;
		firstclick = 0;

	}

	LL_EXTI_ClearFlag_0_31 (LL_EXTI_LINE_0);
	/* mandatory function to clear the Interrupt request */
}



