#ifndef suiteArith_h
#define suiteArith_h
#include "Suite.h"

class SuiteArith : : public virtual Suite
{
	private:
		float u0;
		float r;
	public:
		SuiteArith(float u0,float r);
		~SuiteArith();
		virtual float operator()(int n);
		virtual float somme(int n);
		virtual float raison();
		virtual void affiche(ostream&  os);
		virtual void saisie(istream& is);

		/* data */
};


#endif