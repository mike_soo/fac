#include "SuiteArith.h"

SuiteArith::SuiteArith(float u0,float r)
{
	this->u0=u0;
	this->r=r;
}

float SuiteArith::raison()
{
	return r;
}

float SuiteArith::operator()(int n)
{
	return u0+n*r;
}

float SuiteArith::somme(int n)
{
	return (n*u0) + (n*(n-1))*(r/2);
}

void SuiteArith::affiche(ostream& os)
{
	os <<"u0="<<u0<<" r="<<r;
}

void SuiteArith::saisie(istream& is)
{
	cout<<"rentrez uO"<<endl;
	is>>u0;
	cout<<"rentrez r"<<endl;
	is>>r;
}