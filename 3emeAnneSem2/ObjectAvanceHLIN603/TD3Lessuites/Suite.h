#ifndef Suite_h
#define Suite_h

#include <iostream>
#include <string>
using namespace std;
class Suite
{
	public:
		Suite();
		~Suite();
		virtual float operator()(int n)=0;
		virtual float somme(int n)=0;
		virtual void affiche(ostream&  os)=0;
		virtual void saisie(istream& is)=0;

		/* data */
};
#endif