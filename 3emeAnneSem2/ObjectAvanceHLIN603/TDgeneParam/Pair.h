#ifndef pair_h
#define pair_h
#include <iostream>
#include <string>
using namespace std;

template<typename TypeCle, typename TypeValeur>

class Pair 
{
	private: 
		TypeCle cle;
		TypeValeur val; 

	public: 
		virtual TypeCle getCle();
		virtual TypeValeur getVal();
		Pair(TypeCle cle,TypeValeur val);



};


#endif