#ifndef dico_h
#define dico_h
#include "Pair.h"
#include <iostream>
#include <string>
using namespace std;

template<typename TypeCle, typename TypeValeur>


class Dico 
{
	private: 
		Pair <TypeCle,TypeValeur> *tabPair;
		int taille;
		int indAjout;
	public: 
		Dico();
		void ajouterPair(TypeCle cle,TypeValeur val);
		//TypeValeur getVal(TypeCle &cle);
		TypeValeur chercherCle(TypeCle &cle);


};


#endif