
#include "Pair.h"

template<typename TypeCle, typename TypeValeur>
Pair<TypeCle,TypeValeur>::Pair(TypeCle cle,TypeValeur val)
{
	this.cle=cle;
	this.val=val;
}

template<typename TypeCle, typename TypeValeur>

TypeCle Pair<TypeCle,TypeValeur>::getCle()
{
	return this.cle;
}

template<typename TypeCle, typename TypeValeur>

TypeValeur Pair<TypeCle,TypeValeur>::getVal()
{
	return this.val;
}


