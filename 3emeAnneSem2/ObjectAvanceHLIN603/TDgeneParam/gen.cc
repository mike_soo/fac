#include <iostream>

using namespace std;
template<typename T>
void echange(T & e1, T & e2)
{
	T aux = e1;
	e1=e2;
	e2=aux;
}

template<typename T,int taille>
void triBulles(T t[])
{
	int i = taille-2,j; bool ech=true;
	while(i>=0 && ech)
	{
		ech=false;
		for(j=0;j<=i;j++)
		{
			if (t[j]>t[j+1])
			{
				echange(t[j],t[j+1]);
				ech=true;
			}
		}
		i--;
	}

}


template<typename T,int taille>
void affiche(T t[])
{
	for(int i=0;i<taille;i++)
	{ 
		cout<<t[i]<<" ";

	}
	cout<<endl;
}


int main()
{
	int taillee=5;
	int t[taillee]={5,4,2,7,9};

	affiche<int,5>(t);
	triBulles<int,5>(t);
	affiche<int,5>(t);

	return 0;
}