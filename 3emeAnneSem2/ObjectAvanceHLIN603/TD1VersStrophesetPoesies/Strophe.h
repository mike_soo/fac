#ifndef Strophe_h
#define Strophe_h

#include <iostream>
#include <string>

class Strophe
{
	private:
		Vers ** suiteVers;

		int nbVers;
	public:
		Strophe();
		virtual ~Strophe();
		virtual void saisie(istream& is);
		virtual Vers* vers (int i) const;
		virtual void affiche (ostream& os) const;
};
#endif