#include "objLig.h"

objLig::objLig()
{
	this->donateur="Aucun";
	this->anneDon=-1;
}

objLig::objLig(string desc, int ref, int anneDon ,string donateur) : objMusee(desc,ref)
{
	this->donateur=donateur;
	this->anneDon=anneDon;
}

string objLig::getDonateur()
{
	return donateur;
}

void objLig::setDonateur(string donateur)
{
	this->donateur=donateur;
}

int objLig::getAnneDon()
{
	return anneDon;
}

void objLig::setAnneDon(int anneDon)
{
	this->anneDon=anneDon;
}



void objLig::saisie(istream& is)
{	
	objMusee::saisie(is);
	cout<<"saisir donateur"<<endl;
	is>>donateur>>anneDon;
}

void objLig::affiche(ostream& os) const
{
	objMusee::affiche(os);
	os<<anneDon<<" "<<donateur;
}