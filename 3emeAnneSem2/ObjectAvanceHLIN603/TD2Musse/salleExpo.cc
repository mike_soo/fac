
#include "salleExpo.h"

salleExpo::salleExpo()
{
	this->cap=10;
	this->remp=0;
	t=new objMusee* [10];

}


bool salleExpo::dansTab(int ind)
{
	return ind<this->cap;
}

bool salleExpo::dansRemp(int ind)
{
	return ind<this->remp;
}

salleExpo::salleExpo(int cap)
{

	if(cap>this->cap)
	{
		this->cap=cap;
		objMusee **tempt=new objMusee* [cap];

		for(int i=0;i<this->remp;i++)
		{
			tempt[i]=t[i];

		}
		t=tempt;
	}
}
salleExpo::~salleExpo()
{
	for(int i=0;i<this->remp;i++)
	{
		delete t[i];
	}
	delete t;
}
bool salleExpo::restePlace()
{
	return this->remp<this->cap;
}
void salleExpo::ajouter(objMusee *om)
{
	if(restePlace())
	{
		t[this->remp]=om;
		this->remp++;
	}
	else
	{
		cout<<"Erreur om non ajouté"<<endl;
	}
}
void salleExpo::enlever(int ind)
{
	if(ind < this->remp)
	{
		delete t[ind];
		t[ind]=NULL;

		for(int i=ind; i<this->remp -1;i++)
		{
			t[i]=t[i+1];
		}

		this->remp--;
		for(int i=this->remp;i<this->cap;i++)
		{
			t[i]=NULL;
		}


	}
	else
	{
		cout<<"ind :"<<ind<<" superieur à cap : "<<cap<<endl;
	}
}
void salleExpo::afficher(ostream& os)
{
	for(int i=0 ; i<this->remp;i++)
	{
		t[i]->affiche(os);
	}
}
objMusee* salleExpo::getObjet(int ind)
{
	if(dansRemp(ind))
	{
		return t[ind];
	}
	else
	{
		return NULL;
	}
}