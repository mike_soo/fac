#include "objSig.h"

objSig::objSig()
{
	this->auteur="Aucune";
}

objSig::objSig(string desc, int ref, string auteur) : objMusee(desc,ref)
{
	this->auteur=auteur;
}

string objSig::getAuteur()
{
	return auteur;
}

void objSig::setAuteur(string auteur)
{
	this->auteur=auteur;
}

void objSig::saisie(istream& is)
{
	objMusee::saisie(is);
	cout<<"Saisir auteur"<<endl;
	is>>auteur;
}

void objSig::affiche(ostream& os) const
{
	objMusee::affiche(os);
	os<<" "<<auteur;
}

ostream& operator<<(ostream& os,const objSig& obs)
{
	obs.affiche(os);
	return os;
}
/*
istream& operator>>(istream& is,const objSig& obs)
{
	obs.saisie(is);
	return is;
}
*/
