#ifndef objS_h
#define objS_h

#include "objMusee.h"

class objSig : public virtual objMusee
{
	private: 
		string auteur;
		
	public:
		objSig();
		objSig(string desc,int ref,string auteur);
		virtual string getAuteur();
		
		virtual void setAuteur(string auteur);
		
		void virtual saisie(istream& is);
		void virtual affiche(ostream& os) const;
};

ostream& operator<<(ostream&,const objSig& obs);
//istream& operator>>(istream&,const objSig& obs);
#endif