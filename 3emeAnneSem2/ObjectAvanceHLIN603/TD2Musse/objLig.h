#ifndef objL_h
#define objL_h

#include "objMusee.h"

class objLig : public virtual objMusee
{
	private: 
		string donateur;
		int anneDon;
	public:
		objLig();
		objLig(string desc,int ref,int anneDon,string donateur);
		virtual string getDonateur();
		virtual int getAnneDon();

		virtual void setDonateur(string donateur);
		virtual void setAnneDon(int anneDon);

		void virtual saisie(istream& is);
		void virtual affiche(ostream& os) const;
};
#endif