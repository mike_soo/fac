#ifndef salleExpo_h
#define salleExpo_h

#include "objMusee.h"
#include <string>
#include <iostream>

using namespace std;


class salleExpo
{
	private : 
		int cap;
		int remp;
		objMusee ** t;
	public : 
		salleExpo();
		salleExpo(int cap);
		virtual ~salleExpo();
		virtual void ajouter(objMusee *om);
		virtual void enlever(int ind);
		virtual void afficher(ostream& os);
		virtual objMusee* getObjet(int ind);
		virtual bool restePlace();
		virtual bool dansRemp(int ind);
		virtual bool dansTab(int ind);
};
#endif