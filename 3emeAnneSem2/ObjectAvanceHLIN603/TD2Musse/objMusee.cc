#include "objMusee.h"

objMusee::objMusee()
{
	this->desc="Aucune";
	this->ref=-1;
}

objMusee::objMusee(string desc,int ref)
{
	this->desc=desc;
	this->ref=ref;
}

string objMusee::getDesc()
{
	return desc;
}

int objMusee::getRef()
{
	return ref;
}

void objMusee::setDesc(string desc)
{
	this->desc=desc;
}

void objMusee::setRef(int ref)
{
	this->ref=ref;
}

void objMusee::saisie(istream& is)
{
	cout<<"saisir ref:"<<endl;
	is>>ref;

	cout<<"saisir desc:"<<endl;
	is>>desc;
}

void objMusee::affiche(ostream& os) const
{
	os<<ref<<" "<<desc;
}

ostream& operator<<(ostream& os,const objMusee& ob)
{
	ob.affiche(os);
	return os;
}