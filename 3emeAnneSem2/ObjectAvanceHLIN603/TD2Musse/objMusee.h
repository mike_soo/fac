#ifndef objM_h
#define objM_h

#include <iostream>
#include <string>
using namespace std;
class objMusee
{
	private: 
		string desc;
		int ref;
	public:
		objMusee();
		objMusee(string desc,int ref);
		virtual string getDesc();
		virtual int getRef();
		virtual void setDesc(string desc);
		virtual void setRef(int ref);
		virtual void saisie(istream& is);
		virtual void affiche(ostream& os) const;
};

ostream& operator<<(ostream&,const objMusee& ob);
#endif