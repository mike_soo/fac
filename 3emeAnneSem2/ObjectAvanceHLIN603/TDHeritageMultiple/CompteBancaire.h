#ifndef compteBancaire_h
#define compteBancaire_h

#include <string>
#include <iostream>

using namespace std;


class CompteBancaire
{
	public:
		float solde;
		CompteBancaire(float solde);
		virtual float getSolde();
		virtual ~CompteBancaire();
		virtual void Deposer(float depot);
};

#endif
