#ifndef compteRemunere_h
#define compteRemunere_h

#include "CompteBancaire.h"
#include <string>
#include <iostream>

using namespace std;


class CompteRemunere : virtual public CompteBancaire
{
	
		

	public:
		float interet;
		CompteRemunere(float depot);
		virtual ~CompteRemunere();
		virtual void Deposer(float depot);
};

#endif