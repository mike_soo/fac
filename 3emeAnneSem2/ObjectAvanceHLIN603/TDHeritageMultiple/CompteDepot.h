#ifndef compteDepot_h
#define compteDepot_h

#include "CompteBancaire.h"
#include <string>
#include <iostream>
using namespace std;

class CompteDepot : virtual public CompteBancaire
{
	//private:
		

	public:
		CompteDepot(float solde);
		virtual void Deposer(float depot);
		virtual ~CompteDepot();
};

#endif
