#ifndef compteDepotRemunere_h
#define compteDepotRemunere_h
#include "CompteDepot.h"
#include "CompteRemunere.h"


#include <string>
#include <iostream>
using namespace std;
class CompteDepotRemunere : virtual public CompteDepot, virtual public CompteRemunere
{
	//private:
		

	public:
		CompteDepotRemunere(float solde);
		
		virtual ~CompteDepotRemunere();
};

#endif
