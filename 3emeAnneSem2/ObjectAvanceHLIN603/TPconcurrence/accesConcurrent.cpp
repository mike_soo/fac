#include "accesConcurrent.h"

int total=0;
mutex verrou; 

void carre(int n)
{
	verrou.lock();
	total+=n*n;
	verrou.unlock();
}