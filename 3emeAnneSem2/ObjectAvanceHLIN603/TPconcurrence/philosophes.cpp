#include <thread>
#include <iostream>
#include <mutex>
#include <functional>
#include <condition_variable>
#include <ctime>
using namespace std;

#define NBPHILOS 5
int neMangePas=1;

class Baguette{

  private:
    
    bool bagPrise;
    int priseParPlace;
  public:
    Baguette()
    {
      bagPrise = false;
      priseParPlace=-1;
    }
    void prendreB(int place)
    {
      if(!bagPrise)
      {
        bagPrise=true;
        this->priseParPlace=place;
      }
      else
      {
        cout<<"Baguette.prendreB() : erreur baguette prise par :"<<priseParPlace<<\
        "impossible de la reprendre par :"<<place<<endl;
      }
    }

    void deposerB()
    {
      if(bagPrise)
      { 
        bagPrise=false;
        priseParPlace=-1;
      }
      else 
      {
        //cout<<" baguette non prise => imposible donc de la deposer"<<endl;
      }
    }

    bool estPrise()
    {
      return bagPrise;
    }
    bool estPrisePar(int place)
    {
      return place == this->priseParPlace;
    }

};

class Philo{
public:
  void operator()(Baguette &baguetteG, Baguette &baguetteD, int place, condition_variable &cond ,mutex &m,int &pariteMange) const
  {
    int t1=0,t2=0;
    int pariteCourante=0;
    while(true)
    {
      //this_thread::sleep_for(chrono::milliseconds(1));//je pense
      
        //Pour savoir qui mange une seul fois par changement de parité.
      
      {
        unique_lock <mutex> ul(m);
        if(pariteCourante!=pariteMange)      
        {
      
          // 
          // 
          //  cout<< "Philisophe :"<<place<<" pense"<<endl;    
          // 
        
          cond.wait(ul, 
            [&t1,&t2,&pariteCourante,&baguetteG,&baguetteD,&place,&pariteMange]()
            {
              if(((place % 2) == pariteMange) && (place != neMangePas))
                return true;
              
              if(baguetteG.estPrisePar(place) || baguetteD.estPrisePar(place))
              {
                baguetteG.deposerB();
                baguetteD.deposerB();
                t2=clock();

                cout<< "Philisophe :"<<place<<" à mangé pendant :"<<" t :"<<t2-t1<<"clocks"<<endl;
              }
              return false;
               
              //&& (!baguetteG.estPrise() && !baguetteD.estPrise());
              
            });
          
          //philosophe qui demande ces baguettes
          cond.wait(ul, 
            [&baguetteG, &baguetteD,&place]()
            {
              if(!baguetteG.estPrise()) baguetteG.prendreB(place);
              if(!baguetteD.estPrise()) baguetteD.prendreB(place);
              
              return baguetteG.estPrisePar(place) && baguetteD.estPrisePar(place);
              //Pour savoir qui mange une seul fois par changement de parité.
            });  
          
          t1=clock();
          cout<< "Philisophe :"<<place<<" mange :"<<"pariteMange :"<<pariteMange<<" ne mange pas :"<<neMangePas<<endl; 
          pariteCourante=pariteMange;
        }
      }
    }
      
    
      
      
      
    //this_thread::sleep_for(chrono::milliseconds(place));//je mange  
    
  }
};



int main(){
  
  thread tphilos[5];
  Philo philos[5];
  mutex m;
  Baguette baguettes[5];
  int pariteMange=1; //si paritMange == 0 => philo assis sinon 
  condition_variable cond;


  for(int i=0; i < 4; i++)
  {
    tphilos[i] = thread(philos[i], ref(baguettes[i+1]), ref(baguettes[i]), i+1, ref(cond), ref(m),ref(pariteMange));
    //Possible amélioration pour éviter une situation ou tous les philosophes mangent en même temp.
    //avec le delay suivant on provoque le fait que le philosophe mange pendant que celui à ça
    //gauche mange.
     
    //chrono::milliseconds(1);
  }  
  tphilos[4] = thread(philos[4], ref(baguettes[0]), ref(baguettes[4]), 5, ref(cond), ref(m),ref(pariteMange));

  while(true)
  {  
    
    m.lock();
    pariteMange == 0 ? pariteMange =1 : pariteMange =0;
    
    neMangePas++;
    neMangePas%=6;
    if(neMangePas ==0 )neMangePas=1;
    
    m.unlock();
    
    cond.notify_all();
    //les philosophes manges et penses en fonction de leur 
    //nombre de place tel que seul les philosophes de la
    //même parité (pair ou impair) soit ils réfléchissent soit ils mangent.
    this_thread::sleep_for(chrono::milliseconds(1));  
  }


  for(int i=0; i < 5; i++)
    tphilos[i].join();
  return 0;

}

