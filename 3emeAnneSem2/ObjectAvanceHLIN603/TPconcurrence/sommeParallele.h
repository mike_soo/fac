#ifndef SOMMEPARALLELE_h
#define SOMMEPARALLELE_H
#include <iostream>
#include <future>
#include <math.h>       /* ceil */
#include <vector>
using namespace std;
class SommeParallele
{
	private:
		int* tabI;
		int n;
	public:	
		SommeParallele(int * tabI, int n);
		~SommeParallele();
		void sommeTabI(int deb, int fin,int* tabRes,int itRes);
		int sommeTabIfuture(int deb, int fin);
};

#endif