#include <thread>
#include <iostream>
#include <mutex>
#include <functional>

using namespace std;
class Philo{
public:
  void operator()(std::mutex & baguetteG, std::mutex & baguetteD,mutex &mutexAff,int place) const{
    
    while(true){
      {
        unique_lock<mutex> ulockAffichage(mutexAff);
        std::cout<< "Philisophe "<<place<<" pense"<<std::endl;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(1));//je pense
      
      {
        unique_lock<mutex> ulockG(baguetteG,defer_lock);
        unique_lock<mutex> ulockD(baguetteD,defer_lock);
        lock(ulockG,ulockD);
        { 
          unique_lock<mutex> ulockAffichage(mutexAff);
          std::cout<< "Philisophe "<<place<<" mange"<<std::endl;
        }
        
        std::this_thread::sleep_for(std::chrono::milliseconds(1));//je mange  
        
      
      }
    }
  }
};



int main(){
  
  thread tphilos[5];
  Philo philos[5];
  mutex baguettes[5];
  mutex mutexAff;


  for(int i=0; i < 4; i++)
    tphilos[i] = std::thread(philos[i], std::ref(baguettes[i+1]),  std::ref(baguettes[i]),ref(mutexAff), i+1);
  tphilos[4] = std::thread(philos[4], std::ref(baguettes[0]),  std::ref(baguettes[4]),ref(mutexAff), 5);

  for(int i=0; i < 5; i++)
    tphilos[i].join();
  return 0;

}

