#include "concurrence.h"
#include "sommeParallele.h"
#include "accesConcurrent.h"

#include <string>
using namespace std;




int main(int argc, char ** argv)
{
	// thread t1(afficheO);
	// thread t2(afficheK);
	// t1.join();
	// t2.join();
	
	if(argc==4) //params: char c1,char c2, int n
	{
		
		threadfcts *tA= new threadfcts(argv[1][0],argv[2][0],atoi(&argv[3][0]));
	
		tA->threading();	
	}
	
	else if (argc ==1)
	{


		threadfcts t_a;

		char a,b;
		a='a';
		b='b';


		thread t1(&threadfcts::afficheC,&t_a,a,100);
		thread t2(&threadfcts::afficheC,&t_a,b,100);
		
		t1.join();
		t2.join();

	}

	/*
	*	GESTION DE LA SOMME D'UN TABLEAU. 
	*	GRACE À L'UTILSATION DES THREADS ON VIENT 
	*	EFFECTUER LA SOMME DES QUATRES SOUS TABLEAUX
	*	
	*/
	else if ((argc == 2) && (argv[1][0]=='s'))
	{

		#define T 100
		
		int tabRes[4];
		
		int tabI[T]={
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
						1,2,3,4,5,6,7,8,9,10,
					};
		//nombre de cases à parcourir dans chaque thread
		
		int nCase=ceil((double)T/4);
		
		int lastnCase=T-(nCase*3);
		int resultatSomme=0;
		SommeParallele* sp=new SommeParallele (tabI,T);
		thread t[3];

		/*
		* 	SOMME DU TABLEAU AVEC L'UTILISATION DES THREADS
		*	SANS L'UTILISATION DE FUTURE
		*/

		for(int i=0;i<4;i++)
		{
			
			if(i == 3)
			{
				t[i]=thread(&SommeParallele::sommeTabI,sp,i*nCase,((i*nCase)+lastnCase) - 1 , tabRes, i);	
			}
			else
			{
				t[i]=thread(&SommeParallele::sommeTabI,sp,i*nCase,((i*nCase)+nCase) - 1 , tabRes, i);
			}
		}

		for(int i=0;i<4;i++)
		{
			t[i].join();
		}
		cout<<"tabRes"<<endl;
		for(int i=0;i<4;i++)
		{
			resultatSomme+=tabRes[i];
			cout<<tabRes[i]<<" " ;
		}

		cout<<"la somme du tableau  ="<<resultatSomme<<endl;
		
		cout<<"je boucle "<<endl;


		/*
		* 	SOMME DU TABLEAU AVEC L'UTILISATION DES THREADS
		*	AVEC L'UTILISATION DE FUTURE
		*/
		
		//La déclaration suivante renvois une jolie erreur.
		//future<int> tabResf[4];

		vector<future<int> > tabResf(4);//=async(&SommeParallele::sommeTabIfuture,sp,0*nCase,((0*nCase)+nCase) - 1);;

		for(int i=0;i<4;i++)
		{
			cout<<"je boucle i="<<i<<endl;
			if(i == 3)
			{
				tabResf[i]=async(&SommeParallele::sommeTabIfuture,sp,i*nCase,((i*nCase)+lastnCase) - 1);	
			}
			else
			{
				tabResf[i]=async(&SommeParallele::sommeTabIfuture,sp,i*nCase,((i*nCase)+nCase) - 1);
			}
		}

		cout<<"tabResf"<<endl;
		resultatSomme=0;
		for(int i=0;i<4;i++)
		{
		 	resultatSomme+=tabResf[i].get();
		 	
		}
		cout<<endl;
		cout<<"la somme du tableau avec future ="<<resultatSomme<<endl;
		

	}
	/*
	*	4.Accès concurrent : problème et solution
	*	
	*	ON TESTE L'ACCÉS CONCURENT D'UNE VARIABLE GLOBALE
	*	DEFINIE DANS accesConcurent.h et on constate que 
	*	la variable global n'est pas augmenté de facon correcte,
	*	pour le code si dessous on s'attend à avoir total == 2870
	*	et on a en realité total == 2868 ~ 2870 (apres avoir lance l'exec 1000 fois)
	*	avec: 
	*	for i in {1..40} ; do ./concurrence -a; done 
	*	dans le terminal => le résultat n'est pas assuré.
	*	Puis avec l'utilisation du mutex verrou définie dans accesConcurrent.h
	*	on obtient, à tous les coups 2870 => le résultat est assuré.
	*/

	else if (argc ==2 && argv[1][0]=='a')//&& (string(argv[2]).compare(string("ac"))==0))
	{
	
		thread tac[20];
		for(int i=0;i<20;i++)
		{
			tac[i]=thread(&carre,i+1);
		}
		for(int i=0;i<20;i++)
		{
			tac[i].join();
		}
		cout<<"total="<<total<<endl;
		
	}


	cout<<endl;
	

	return 0;
}