// compiler avec les options : -Wall -std=c++11
// Exercice 6,  
#include <thread>
#include <iostream>
#include <queue>
#include <future>
#include <condition_variable>
#include <mutex>

using namespace std;

class Producteur{
public:
  void operator()(queue<int> & buffer, bool & fini,mutex &verrou,condition_variable &cond) const{

    for (int i = 1; i <= 100; ++i) 
    {
      {
        lock_guard<mutex> guard(verrou);
        buffer.push(i);
        cout<<"je push"<<endl;
      }
      
      cond.notify_all();
      
      
    }
    
    
    {
      //quand le producteur finis de remplir le buffer, fini passe à true et
      //cond.notify_all() "reveille" tout les threads mis en attente par 
      //cond.wait() 
      lock_guard<mutex> lg(verrou);
      fini = true;
      //cond.notify_all();

    }
    
    cout<<"j'ai finis\n"<<endl;
  }
};

class Consommateur{
public:
  int operator()(queue<int> & buffer, bool & fini,mutex &verrou,condition_variable &cond) const{

    int summ = 0;
    int finiCons=0;
    while (finiCons!=1) 
    {
      {
        //une fois que le producteur est terminé, fini passe à vrai, 
        //sinon le mutex verrou est libéré et la thread passé en attente
        //grace à cond.wait(ul)
        unique_lock <mutex> ul(verrou);
        
        if(fini && buffer.empty())
          finiCons=1;
        else
        {
          cout<<"j'attend"<<endl;
          cond.wait(ul,[&buffer,&fini]()
          {
            //buffer.empty() => condition d'attente
            return !buffer.empty() ;
          });
      
          cout<<"je consomme apres avoir attendu"<<endl;
          
          {
            summ+=buffer.front(); 
            buffer.pop();
          }
        }
      }
      
      
    }
  
    return summ;
  }
};



int main(){
  
  Producteur prod;
  Consommateur cons;

  queue<int> buffer;
  bool fin = false;
  condition_variable cond;
  mutex verrou;

  thread t1(prod,  ref(buffer), ref(fin),ref(verrou),ref(cond));
  future<int> resultat = async(launch::async,cons, ref(buffer), ref(fin),ref(verrou),ref(cond));
  t1.join();

  int res = resultat.get();
  cout << "Somme : " << res << endl;
  return 0;

}

