#include <thread>
#include <iostream>
#include <queue>
#include <future>
#include <condition_variable>
#include <mutex>
#include <vector>
using namespace std;


class Pizzeria
{
    public:
        int tempsPrepPizza=60;

        void operator()(queue<int> &stockPizza,condition_variable &cond,mutex &m)
        {
            while(true)
            {
                {
                    unique_lock<mutex> ul(m);
                    cond.wait(ul , [&stockPizza]()
                    {
                        return stockPizza.size()<35;
                    });
                    stockPizza.push(1);
                }
                cond.notify_all();
                this_thread::sleep_for(chrono::milliseconds(60));
            }
        }
};


class Donatelo
{
    

    public:
        queue <int> stockPizza;

        void operator()(condition_variable &cond, mutex &m) 
        {
    
            while(true)
            {
                {
                    unique_lock<mutex> ul(m);
                    
                    cond.wait(ul,
                    [this]() 
                    {return !stockPizza.empty();});

                    stockPizza.pop();
                    cout<<"je mange"<<endl;
                    
                    for(unsigned int i=0;i<stockPizza.size();i++)
                        cout<<"p";
                    cout<<"Fin"<<endl;
                    
                    
                }
                //Affiche les pizzas dans stockPizza
                cond.notify_all();
                this_thread::sleep_for(chrono::milliseconds(10));
            }

        }

};






int main()
{   
    mutex m;
    condition_variable cond;
    Pizzeria pizzerias[9];
    Donatelo don;
    thread tp[9];
    
    thread td(ref(don),ref(cond),ref(m));

    for(int i=0;i<5;i++)
    {
        tp[i]=thread(ref(pizzerias[i]),ref(don.stockPizza),ref(cond),ref(m));
    }

    td.join();
    for(int i=0;i<9;i++)
    {
        tp[i].join();
    }

    return 0;
}