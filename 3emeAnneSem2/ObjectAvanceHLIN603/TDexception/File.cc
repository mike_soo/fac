#include "File.h"

template<typename T>
void File::inserer(T* elem)
{
	this->file->push_back(elem);
}


template<typename T>
T* File::retirer()
{
	if(!this->vide())
	{
		T* elemR=this->file->front();
		this->file->pop_back();
		return elemR;
	}
}


template<typename T>
bool File::vide()
{
	return this->file->empty();
}