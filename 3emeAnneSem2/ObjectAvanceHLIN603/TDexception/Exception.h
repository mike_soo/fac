#ifndef exception_h
#define exception_h

#include <string>
#include <iostream>
#include <exception>
using namespace std;


class Exception : public exception
{
	public:
		
		Exception();
		virtual ~Exception();
		
};

#endif
