#ifndef file_h
#define file_h

#include <string>
#include <iostream>
#include <exception>
#include <vector>
using namespace std;
template <typename T>
class File
{
	private : 
		vector<*T> pile;


	public : 
		File();
		~File();	
		void inserer(T* elem);
		*T retirer();
		bool vide();

};

#endif

