/*

L3 HLIN611
	
	Bonjour,
	Ce fichier contient toutes les réponses aux exercices 2 sous forme
	d'un code accompagné de texte au fond du fichier.
	Voici donc m'a proposition de programme de recherche d'adresse IP pour un host 
	données.
	Veuillez utiliser make pour compiler et pouvoir executer les 2 differents 
	comportements disponible.

	./IpAvecAddrinfo nomDuHost 
		Fournie l'adresse IP + détails de nomDuHost fourni.

	./IpAvecAddrinfo -s
		Active une entrée continue où l'on pourra saisir des noms de
		host suivis d'entré pour avoir un retour sur les détails de ce même 
		host de façon continue. Saisir STOP pour arrêter le flux.

*/


#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h> //HOST_NAME_MAX
#include <arpa/inet.h>
#include <string.h>

void afficheIp(char * hostname)
{

    struct hostent *he;
   // char ip[100];
    struct in_addr **addr_list;
    
  	
  	he=gethostbyname(hostname);

  	if(he)
  	{
  		printf("nom officiel=%s\n",he->h_name);
	   	addr_list= (struct in_addr **) he->h_addr_list;
	   	
	   	int i=0;
	   	while(he->h_aliases[i]!=NULL)
	   	{
	   		printf("aliase %i : %s\n",i,he->h_aliases[i]);
	   	}

	   	printf("type d'adresse : %i\n",he->h_addrtype);
	   	printf("taille d'adresse : %i\n",he->h_length);

	   	i=0;
		while (addr_list[i]!=NULL)
		{
			/*
				inet_ntoa permet  de convertir adresse obtenue
				en hexadécimal vers des caractères
			*/
			printf("adresse ip=%s\n",inet_ntoa(*addr_list[i]));
			i++;
		} 	
	}
	else
	{
		printf("\n recherche de %s sans sucés\n",hostname);
	}
}

int main(int argc, char *argv[])
{	



	if(argc ==1)
	{

	    char hostname[HOST_NAME_MAX + 1];
	    if (gethostname(hostname, HOST_NAME_MAX) == 0)
	    {
	    	afficheIp(hostname);
	    }
	    else
	        perror("gethostname");
	}

	else if (argc ==2)
	{
		if(strcmp(argv[1],"-s")==0)
		{
			char lu[1024];
			lu[1023]='\0';
			printf("Saisir des hostname:\n" );
			while( scanf("%1023s",lu)>0   &&   strcmp(lu,"STOP")!=0  )
			{
				afficheIp(lu);
			}
		}
		
		else
		{
			afficheIp(argv[1]);
		}
	}
    return 0;
}


/*
	6.
	$ ./hostbyname saisie
		Saisir des hostname:
			hotmail.com
			nom officiel=hotmail.com
			adresse ip=65.55.65.188
			adresse ip=65.55.65.172
			o9
			nom officiel=o9.etu.info-ufr.univ-montp2.fr
			adresse ip=10.6.15.9
			9gag.com
			nom officiel=9gag.com
			adresse ip=52.58.245.91
			adresse ip=35.156.9.200


	7.

	$ getent hosts hotmail.com
		65.55.65.188    hotmail.com
		65.55.65.172    hotmail.com

	$ getent hosts o9
		10.6.15.9       o9.etu.info-ufr.univ-montp2.fr

	$ getent hosts 9gag.com
		52.58.245.91    9gag.com
		35.156.9.200    9gag.com

	On constate qu'on a bien les mêmes résultats lors de l'usage de notre 
	programme créé et getent.


	8. L'appel de fonction inet_ntoa pourra nous rendre service pour venir 
	automatiser la fonction de transformation des nombre hexadécimal en
	caractères. Effectivement cette fonction facilite la création du programme
	car on n'aura pas a pas à implémenter un algorithme pour réussir cela.
	
	10. 
		char *h_name
			Cette variable correspond au nom officiel du host

		char **h_alisases
			Ceci correspond à un tableau de noms alternative représentant le
			même host.

		int h_addrtype : 
			En pratique ça valeur varie entre AF_INET ou AF_INET6 avec le dernier
			utilisé pour host en IPv6. On ajoute aussi qu'un autre type d'adresse pourrait
			être représenté dans h_addrtype si celle-ci se présente.
		
		int h_length : 
			Cette variable représente la taille de l'adresse en octet.

		char **h_addr_list : 
			Pointant sur le tableau contenant tout les adresse associé au host.
			Il est possible d'avoir multiples adresses trouvées lorsque le host possède plusieurs 
			réseau associés. Ce tableau est délimité par le pointeur NULL.

		char *h_addr
			Ceci étant synonyme de h_addr_list[0], correspond à la première adresse
			relié au host.

		source : http://www.gnu.org/software/libc/manual/html_node/Host-Names.html

	Exercice 3
		1. nslookup lancé sans paramétrés permet de lancer le programme
			en mode interactive.  Il permet d’effectuer des
			requêtes vers un nom de domaine fourni pour obtenir de informations 
			concernant le nom, les adresses associés et autres concernant ce même domaine. 
		2. et 3.
			$ nslookup
				> hotmail.com
				Server:		10.6.200.93
				Address:	10.6.200.93#53

				Non-authoritative answer:
				Name:	hotmail.com
				Address: 65.55.65.172
				Name:	hotmail.com
				Address: 65.55.65.188
				

				> 9gag.com
				Server:		10.6.200.93
				Address:	10.6.200.93#53

				Non-authoritative answer:
				Name:	9gag.com
				Address: 35.156.9.200
				Name:	9gag.com
				Address: 52.58.245.91
				

				> o9 //machine courante
				Server:		10.6.200.93
				Address:	10.6.200.93#53

				Name:	o9.etu.info-ufr.univ-montp2.fr
				Address: 10.6.15.9
		4. host et nslookup ont des fonctionnalité presque équivalentes avec la 
		différence que host et conçu pour des systems Unix et que nslookup conçu
		pour windows et unix, est considéré comme ancien ou "deprecated" terme en anglais définissante 
		une fonction que ne devrai plus être utilisé.
		Source entre autres:
		http://unix.stackexchange.com/questions/93808/dig-vs-nslookup

		D'autre part, dig pourrai être considéré comme le plus complet des différents
		outils mentionnés à cause de ça flexibilité et sont large contenu de retour
		d'information. Il s’avère que la plupart des gestionnaire de DNS utilise dig
		pour le débogage des fonctionnalités implémenté en réseau.
		Source:
		https://linux.die.net/man/1/dig
		
*/		

	

