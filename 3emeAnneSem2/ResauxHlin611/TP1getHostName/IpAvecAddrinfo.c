/*

L3 HLIN611
    
    Bonjour,
    Ce fichier contient toutes les réponses aux exercices 2 sous forme
    d'un code accompagné de texte au fond du fichier.
    Voici donc m'a proposition de programme de recherche d'adresse IP pour un host 
    donné.
    Veuillez utiliser make pour compiler et pouvoir exécuter les 2 différents 
    comportements disponible.

    ./IpAvecAddrinfo nomDuHost 
        Fournie toutes les adresses IP + nom canonique.

    ./IpAvecAddrinfo -s
        Active une entrée continue où l'on pourra saisir des noms de
        host suivis d'entré pour avoir un retour sur les détails de ce même 
        host de façon continue. Saisir STOP pour arrêter le flux.
    
    Sachez que inet_addr est une fonction obsolète, c'est pour cela que 
    vous ne la verrez pas apparaitre tout au long du code, on choisira plutôt
    des alternative pour réaliser le fonctionnement demandé grâce à getnameinfo().

*/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>

void afficheIp(char* hostname)
{
    /*
        Le code suivant utilise getaddrinfo() pour trouver la 
        liste des adresses reliés au nom de domaine fourni en paramètre où chaque 
        adresse IP retrouvé est associé à son nom canonique grâce à l'appel de getnameinfo() 
        sur chaque adresse retrouvé.
    */

    struct addrinfo hints,*res;
    /*
        hints sera la structure contenant des attribut définis par la suite qui permettrons
        d'orienter le fonctionnement de getaddrinfo()
    */
    memset(&hints,0,sizeof(hints)); //tous les attribut de la struc hints initialisés à 0.
    /*
        La valeur AF_UNSPEC  indique que getaddrinfo() doit renvoyer les adresses de socket
        de n'importe quelle type de famille d'adresses (par exemple, IPv4 ou IPv6).
        Autrement dit AF_UNSPEC permettra d'accepter tout type d'adresse.
    */
    hints.ai_family = AF_UNSPEC; 
    
    /*
        La valeur de socktype décrit le type de socket désiré lors de la
        demande des structures addrinfo ici on précise que l'on cherche à utiliser le 
        protocole en Diagramme.
    */
    hints.ai_socktype = SOCK_DGRAM;


    /*
        getaddrinfo() retourne une structure de type addrinfo qui sera stockée dans res
        qui contient (entre autres) une ou plusieurs adresses Internet obtenues en combinant 
        les fonctionnalités de gethostbyname() et getservbyname().  
    */
    if(getaddrinfo(hostname,NULL,&hints,&res)!= 0)
    {
       printf("Erreur hostname non trouvé, avez vous saisi une adresse valide?\n" );
        return ;
    }

    char host[512];
    char serv[512];
    
    /*
        Par la suite on effectue un parcours de structure chainé afin de 
        pouvoir afficher toutes les adresses récupérer dans res, en affichant 
        le nom canonique de chacune de celles-ci.
    */

    while(res!=NULL)
    {
        
        getnameinfo(res->ai_addr,res->ai_addrlen,host,sizeof(host),NULL,0,0);
        printf("Nom Canonique: %s\n",host);        
        
        /*
            Ici l'ajout du paramètre NI_NUMERICHOST nous permet de changer le comportement 
            de getnameinfo(), ce qui permet de stocker cette fois-ci dans host l'adresse IP 
            courante.
        */
        getnameinfo(res->ai_addr,res->ai_addrlen,host,sizeof(host),serv,sizeof(serv),NI_NUMERICHOST);
        printf("IP:            %s\n",host);   


        res=res->ai_next;
    }
    
    


    freeaddrinfo(res);
}



int main(int argc,char *argv[])
{
    
    /*
        Si on a deux arguments, on vérifie si le deuxième argument correspond 
        à -s, puis si c'est le cas on boucle indéfiniment jusqu'à ce que l'utilisateur
        saisi STOP. Chaque saisi de l'utilisateur correspond à une recherche
        d'adresses IP en fonction d'un nom de hôte.
    */
    if ((argc ==2) && (strcmp(argv[1],"-s")==0))
    {
    
        char lu[1024];
        lu[1023]='\0';
        printf("Saisir des hostname puis STOP pour arrêter:\n");

        while( scanf("%1023s",lu)>0   &&   strcmp(lu,"STOP")!=0  )
        {
            afficheIp(lu);
            printf("\nSaisissez nom de domaine suivant:\n");
        }
    }
    else
    {
        afficheIp(argv[1]);
    }
    

    
    
    return 0;
    
}



/*  
    En essayant de rester équivalent avec le sujet originel de tp, 
    au lieu de détailler l'entité hostent, on étudiera par la suite la 
    structure addrinfo. Avant de rentrer dans les détails de chaque attribut, 
    je me permet de faire une brève introduction sur l'utilité de cette structure.
    Cette structure est employé en premier temps lors de l'utilisation de getaddrinfo() 
    pour déterminer le retour obtenu. Celle-ci permet de filtrer les informations à trouver
    offrant un contrôle sur le type de données à analyser (adresses en IPv4 ou IPv6, entre autres
    détallés par la suite) lors de la recherche d'information en fonction d'un nom de domaine donnée. 
    Puis dans un deuxième temps cette même structure stockera les informations trouvés 
    par getaddrinfo() pour ensuite pouvoir entre autres les analyser grâce à l'aide de getnameinfo() 
    (que vous avez pus rencontrer plus haut dans le code) pour extraire les adresses pointé par ai_addr
    ainsi que les noms officiels associés à chaque adresse IP. 
    Ces explications s'orientent plutôt vers l’utilité qu'on a donnée aux structures et aux fonctions 
    lors de la réalisation du tp et n'abordent pas forcement toutes les fonctionnalités possible réalisable. 

    struct addrinfo 
    {
       int              ai_flags;
       int              ai_family;
       int              ai_socktype;
       int              ai_protocol;
       socklen_t        ai_addrlen;
       struct sockaddr *ai_addr;
       char            *ai_canonname;
       struct addrinfo *ai_next;
    };
    

    ai_family : Cette attribut décrit le type d'adresse 
    souhaité lors de l'emploi de hints comme paramètre de la fonction
    getaddrinfo(). Autrement dit le type d'adresse retourné lors de l'appel 
    de cette même fonction dépend de l'attribue ai_family.
    Des attribues valides pour cette attribut sont (entre autres):
    -AF_INET pour un retour d'adresses en IPv4.
    -AF_INET6 "    "    "     "    "  en IPv6.
    -AF_UNSPEC désigne que le type d'adresse souhaité en retour n'est pas spécifié 
                donc permet de recevoir tout type d'adresse.
     
    ai_socktype : Cette attribut permet de spécifier le type de socket 
    souhaité. On peut par exemple préciser que l'on souhaite avoir de retour
    un socket de type stream avec SOCK_STREAM ou SOCK_DGRAM pour obtenir un 
    socket de type diagramme.
    
    ai_protocol : Désigne le protocole à utiliser lors de la communication entre
    sockets. En désignant cette attribut à 0 on permet l'utilisation de n'importe 
    quel protocole.

    ai_flags : Cette attribut permet de préciser plus d'options par la superposition de
    bits avec "|". Dans notre programme on laissera cette attribut à 0.
    
    Source: http://man7.org/linux/man-pages/man3/getaddrinfo.3.html
    
    Ensuite je vous montre mes résultats des tests effectuer avec hostent.

    $ getent hosts hotmail.com
        65.55.65.188    hotmail.com
        65.55.65.172    hotmail.com

    $ getent hosts o9
        10.6.15.9       o9.etu.info-ufr.univ-montp2.fr

    $ getent hosts 9gag.com
        52.58.245.91    9gag.com
        35.156.9.200    9gag.com

    On constate qu'on a bien des résultats similaires lors de l'usage de notre 
    programme créé et getent.
*/