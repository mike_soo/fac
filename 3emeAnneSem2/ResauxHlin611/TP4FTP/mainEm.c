#include "clientTCP.h"



#define FBUFFERSIZE 256

int main(int argc,char **argv)
{
	if (argc==4)
	{
		char* IP=argv[1];
		
		short int PORT=atoi(argv[2]);
		
		int dSock = socket(PF_INET,SOCK_STREAM,0);
		struct sockaddr_in aS;
		socklen_t sizeOfaS=sizeof(aS);
		
		/*
			Pas d'erreur à envoyer par htons() d’après la documentation de cette même
			fonction
		*/
		

		if(creerClientTCP(IP,PORT,dSock,&aS)>=0)
		{
			
			envoyerFichierTCP(dSock,argv[3]);
			//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
			

		}
		else
		{
			perror("erreur creation du client\n");
		}
		close(dSock);
	}


	else
	{
		perror("rentrez le bon nombre d'arguments\n");
	}
}