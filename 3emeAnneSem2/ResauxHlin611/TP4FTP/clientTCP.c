#include "clientTCP.h"
#define FBUFFERSIZE 256

void afficheFileBuffer(char * fileBuffer , int n)
{
	int i=0;
	printf("\t\e[1;36m\n");
	for(i=0;i<n;i++)
	{
		fileBuffer[i]=='\0'? printf("NULL") : printf("%c",fileBuffer[i]);
	}
	
	printf("\e[0m\n");
}

int creerClientTCP(char* IP,int PORT,int dSock,struct sockaddr_in* aS)
{   
    int res;
	// int dSock = socket(PF_INET,SOCK_DGRAM,0);
	// if (dSock<0)
	// {	
	// 	perror("erreur dans creation de socket!\n");	
	// 	return -1;
	// }
	
    memset((char *) aS, 0, sizeof(aS));
	aS->sin_family = AF_INET;
	aS->sin_port=htons((short)PORT);
	/*
		Pas d'erreur à envoyer par htons() d’après la documentation de cette même
		fonction
	*/
    

    res=inet_pton(AF_INET,IP,&(aS->sin_addr));
    if(res<0)
    {
        perror("inet_pton erreur");
        return -1;
    }
    
   
    if(connect(dSock,(struct sockaddr *) aS,sizeof(struct sockaddr_in))<0)
    {
		perror("client non connecté, erreur dans connect()");
    	return -1;
    }    

    return 1;
}



int envoyerTCP(int dSock,char *msg ,off_t tOctet)
{
	off_t octenvTot;
	int er=0,res;
	
	
	
	octenvTot=0;			

	while (octenvTot<tOctet && er==0)
	{
		res=send(dSock,&msg[octenvTot],tOctet-octenvTot,0);
		
		if(res<0)
		{
			perror("\e[0;31mErreur dans envoi du message\e[0m\n");
			er=1;
		}
		else if(res==0)
		{
			printf("res==0\n");
			break;
		}
		else
		{
			octenvTot+=res;
			printf("\t\e[1;33m nbOctectActuellement envoyé :%i\e[0m\n",res);
			printf("message act envoyé : \n");			
			//afficheFileBuffer(fileBuffer,fbuffersize);
		}
		
	}

	return er? -1 : octenvTot;
}

void envoyerFichierTCP(int dSock, char * fichier)
{	
	

	FILE* fd;
	
	int  clu;
	char fileBuffer[FBUFFERSIZE];
	memset(fileBuffer,'\0',FBUFFERSIZE);
	off_t tailleFichierEnvoyer;
	struct stat fileStat;
	
	if(stat(fichier,&fileStat) < 0)    
	{
		perror("erreur dans la recuperation des information du fichier");
		exit(-1);
	}
	
	if((tailleFichierEnvoyer=fileStat.st_size) <=0)
	{
		printf("erreur : taille de fichier <=0 : %li oct\n",tailleFichierEnvoyer);
		exit(-1);
	}

	printf("\e[1;37m taille de fichier à envoyer => %li oct <=\e[0m\n",tailleFichierEnvoyer);
		

	if((fd=fopen(fichier,"r"))!=NULL)
	{
		
	}
	else
	{
		perror("Erreur dans ouverture du fichier.");
		exit(-1);
	}

	printf("client TCP Crée!\n");
	int recu,octenvTot,res,octenvAct;

	short unsigned int er=0;
	octenvTot=0;


	char * tailleFichierEnvoyerc=(char *)&tailleFichierEnvoyer;

	if(0>envoyerTCP(dSock,tailleFichierEnvoyerc,sizeof(off_t)))
	{
		printf("Erreur dans envoi de la taille du fichier\n");
	}

	octenvTot=0;
	octenvAct=0;			
	memset(fileBuffer,'a',FBUFFERSIZE);
	//ToDo si aucun charactere lu dans fichier renvoyer warning? À revoir
	if(0 >= (clu=fread(fileBuffer,sizeof(char),FBUFFERSIZE,fd)))
	{
		printf("Warning : clu = %i \n",clu);
		er=1;
	}
	
	printf("chars lu = %i\n",clu);
	//afficheFileBuffer(fileBuffer,FBUFFERSIZE);
	while(octenvTot<tailleFichierEnvoyer && er==0)
	{
		if(0>envoyerTCP(dSock,fileBuffer,clu))
		{
			printf("Erreur dans envois continue de données du fichier\n");
			er=1;
		}

		octenvTot+=clu;
		// 		octenvTot+=res;
		// 		printf("\t\e[1;33m nbOctectActuellement envoyé :%i\e[0m\n",res);
		// 		printf("message act envoyé : \n");
		


		memset(fileBuffer,'\0',FBUFFERSIZE);
		if(0 > (clu=fread(fileBuffer,sizeof(char),FBUFFERSIZE,fd)))
		{
			printf("erreur lors de lecture de fichier pendant le transfert\n");
			er=1;	
		}
		
		printf("chars lu = %i\n",clu);
	}


	
	char finRecois;
	
	res=recv(dSock,&finRecois,sizeof(finRecois),0);
	res ? printf("finRecv\n") : printf("error : not finRecv\n");
	
	printf("\t\e[1;31m msg completement envoyé :%i\n", octenvTot);
	er ? printf("\terreur lors de l'envoi\n") : printf("\tsucces lors de l'envoi\n");
	printf("\e[0m\n");
	fclose(fd);

}