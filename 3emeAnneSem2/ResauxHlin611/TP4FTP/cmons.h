#ifndef CMONS_H
#define CMONS_H
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void extraireIP(char *IPatrib,struct sockaddr_in * sourcaSock);

#endif