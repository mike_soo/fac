#ifndef CLIENT_H
#define CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
 
void afficheFileBuffer(char * fileBuffer , int n);
int creerClientTCP(char* IP,int PORT,int dSock,struct sockaddr_in* aS);
void envoyerFichierTCP(int dSock, char * fichier);
int envoyerTCP(int dSock,char *msg ,off_t tOctet);

#endif