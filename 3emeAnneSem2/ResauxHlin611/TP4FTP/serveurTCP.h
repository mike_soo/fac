#ifndef SERVEURTCP_H
#define SERVEURTCP_H
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>



int creerServeurTCP(char* IP,int PORT,int dSock);

void afficheFileBuffer(char * fileBuffer,int n);

int recevoirTCP(int sourcdSock,char *msg ,off_t tOctet);

void recevoirFichierTCP(int dSock);

void recevoirFichierTCPconcurrent(int dSock);

void chatTCPmultiplex(int dSock);

#endif