#include "serveurTCP.h"
#include <stdint.h>
#include "cmons.h"

void afficheFileBuffer(char * fileBuffer,int n)
{
	int i=0;
	printf("\t\e[1;36m\n");
	for(i=0;i<n;i++)
	{
		fileBuffer[i]=='\0'? printf("NULL") : printf("%c",fileBuffer[i]);
	}
	
	printf("\e[0m\n");
}


int creerServeurTCP(char* IP,int PORT,int dSock)
{   
    int res;
	
	if (dSock<0)
	{	
		perror("erreur dans création de socket!\n");	
		return -1;
	}
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	

    if(0!=strcmp(IP,"0"))
    {
		res=inet_pton(AF_INET,IP,&ad.sin_addr);
        if(res<=0)
        {
            perror("inet_pton erreur");
            return -1;
        } 
    }
	else
	{
		ad.sin_addr.s_addr= INADDR_ANY;
	}

	/*
		Pas d'erreur à envoyer par htons() d’après la documentation de cette même
		fonction
	*/
	ad.sin_port=htons((short)PORT);
	
    if(bind(dSock,(struct sockaddr*)&ad,sizeof(ad))<0 )
	{
		perror ("erreur lors de l'utilisation de bind");
		return -1;

	}

	else
	{
		socklen_t sizeofad=sizeof(ad);
		if (getsockname(dSock, (struct sockaddr *)&ad,&sizeofad) == -1)
		{
    		perror("getsockname erreur");
            return -1;
        }
	    
	    char *IPatrib;
        char IPatrib_ntop[1024];
        printf("IP atribé avec ntoa : %s\n",(IPatrib = inet_ntoa(ad.sin_addr)));

        inet_ntop(AF_INET,&(ad.sin_addr),IPatrib_ntop,1024);

        printf("IP atribué avec ntop : %s\n",IPatrib);
        printf("PORT :  %i\n", ntohs(ad.sin_port)); 

        if(listen(dSock,7)<0)
        {
        	perror("Erreur dans listen\n");
        }
        
        
    	   
	}


    return dSock;
}

/*
	@params :
	dSock : socket à partir de laquelle on recoit un msg
	msg : buffer on l'on vient stocker le message
	tOctet : taille du buffer msg
	tOctet : nombre d'octet du message à recevoir.
*/
int recevoirTCP(int sourcdSock,char *msg ,off_t tOctet)
{	
	off_t octrecuTot;
	
	int res,er=0;
	
	octrecuTot=0;
	

	while (octrecuTot<tOctet && er==0)
	{
		//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
		res=recv(sourcdSock, &msg[octrecuTot], tOctet - octrecuTot,0);
		if(res<0)
		{
			perror("recevoirTCP() : Erreur dans reçois du message \n");
			printf("octrecuTot = %lu\n",octrecuTot);
			er=1;
		}
		else if(res==0)
		{
			printf("recevoirTCP() : res == 0\n");
			break;
			er=1;
			
		}
		else
		{ 
			octrecuTot+=res;
			//printf("recevoirTCP() : octets tot reçu: %lu\n",octrecuTot);
		}

	
	}
	return er? -1 : octrecuTot;
}


void recevoirFichierTCP(int dSock)   
{
	#define FBUFFERSIZE 256
	
	struct sockaddr_in sourcaSock;
	int sizeOfsourcaSock=sizeof(sourcaSock);
	
	int sourcdSock,res;	
	off_t octrecuTot, octrecuAct, tailleFichierRecevoir, octrecuManque;
			 
	char fileBuffer[FBUFFERSIZE];
	
	printf("serveur Crée!\n");
		
	
	
	int cecrit;
	

	FILE *fp;
	short unsigned int er;
	char * tailleFichierRecevoirc=(char *)&tailleFichierRecevoir;
	
	char nomFichier[20]={"fichierRecu\0"};
	char c[6]={"a.txt\0"};
	char nomFichierR[30];
    
    char IPatrib[INET_ADDRSTRLEN];

	while(1)
	{
		printf("j’attends une connexion\n");	
		memset(nomFichierR,'\0',30);
		
		/*Remplir le fileBuffer de a pour des raison d'observation
		  du remplissage de ce même buffer*/
		memset(fileBuffer,'a',FBUFFERSIZE);
		er=0;
		octrecuTot=0;
		strcat(nomFichierR,nomFichier);
		strcat(nomFichierR,c);
		//c[0]++;
		sourcdSock=accept(dSock,(struct sockaddr*) &sourcaSock, (socklen_t*) &sizeOfsourcaSock);
		


		/*
			Après accept, on cherche à identifier l'IP du client en s'inspirant de 
			comment on fait pour printé l'IP de serveur qu'on crée.
		*/

		extraireIP(IPatrib,&sourcaSock);

		printf("nomFichier : %s\n",nomFichierR); 	
		if((fp = fopen(nomFichierR,"w"))==NULL)
		{
			perror("erreur dans la creation du nouveau fichier recu");
			er=1;
		}

		printf("j’attends la taille du message\n");
		
		/*On recoit un entier: la taille du fichier à recevoir*/
		while (octrecuTot<sizeof(off_t) && er==0)
		{
			//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
			res=recevoirTCP(sourcdSock,tailleFichierRecevoirc ,sizeof(off_t));
			if(res<0)
			{
				printf("RecevoirFichierTCP() : erreur pendant le reçois de la taille du fichier \n");
				er=1;
			}	
			else if(res == 0)
			{
				printf("RecevoirFichierTCP() : Warning res == 0");
			}
			else
			{
				octrecuTot+=res;
			}
		}
			
		printf("\e[1;37m Taille du fichier à recevoir => %li <=\e[0m\n",tailleFichierRecevoir);

		
		octrecuTot=0;
		octrecuAct=0;
		
		/*
			En fonction de la taille du fichier a recevoir, on lit 
			tout les octets reçue jusqu’à ce qu'on est reçue 
			tailleFichierRecevoir octets
		*/		
		 	
		while (octrecuTot<tailleFichierRecevoir && er==0)
		{
			octrecuManque =tailleFichierRecevoir - octrecuTot;
			//printf("octrecuTot=%lu\n",octrecuTot);
			//printf("il manque = %lu\n",octrecuManque);
			
			//inutile car la socket du client est relié au desc de fichier sourcdSock
			//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
			
			// printf("fileBuffer avant reçois de message\n");
			// afficheFileBuffer(fileBuffer,FBUFFERSIZE);
			res=recevoirTCP(sourcdSock, fileBuffer , FBUFFERSIZE > octrecuManque ? octrecuManque : FBUFFERSIZE );
			printf("\t coucou\n");
			if(res<0)
			{
				printf("recevoirFichierTCP() : erreur lors de recoit\n octrecuTotal = %lu ",octrecuTot);
				er=1;
			}

			else if (res==0)
			{
				printf("recevoirFichierTCP() : ress==0 lors de recoit\n octrecuTotal = %lu ",octrecuTot);
			}
			else
			{
				octrecuTot+=res;
				octrecuAct+=res;
			}

			if((octrecuTot >= tailleFichierRecevoir) )
			{	
				/*
					Tous les octets sont reçus, écriture dans le
					fichier en fonction des octets actuellement
					reçus. octrecuAct correspond au nombre de octets
					reçus avant d'attendre la taille maximal du buffer
					du fichier. Quand la taille max est atteinte, 
					octrecuAct repasse à 0.
				*/
				//printf("ecriture final dans fichier! \n");
				cecrit=fwrite(fileBuffer , sizeof(char), octrecuAct , fp);
				printf("oct ecrit dans fichier : %i\n",cecrit);
				fclose(fp);
				
				break;
			}

			else if(FBUFFERSIZE == octrecuAct)
			{
				/*
					Toujours en cours de reçois d'octets, le buffer
					doit être vider dans le fichier car il est 
					remplit.
				*/
				//printf("ecriture dans fichier\n ");
				
				cecrit=fwrite(fileBuffer , sizeof(char), octrecuAct , fp);
				//printf("oct ecrit dans fichier : %i\n",cecrit);
				octrecuAct=0;
				memset(fileBuffer,'a',FBUFFERSIZE);
			}
		}

		char finiRecois;
		res=send(sourcdSock,&finiRecois,sizeof(finiRecois),0);


		printf("\t>>>>EOT<<<<<\n");
		printf(":Bilan Transmission:\n");
		if(octrecuTot==tailleFichierRecevoir && er==0)
		{
			printf("Message correctement reçu \n");
		}
		else
		{

			printf("octets reçu: %lu\n",octrecuTot);
			printf("octets censé recevoir: %li \n ", tailleFichierRecevoir);
		}
		printf("\n\n");		     				
 		

 		// printf("test: ");
 		// (&fileBuffer[0]==fileBuffer) ? printf ("vrai\n") : printf("faux\n");
	} 	


}



void recevoirFichierTCPconcurrent(int dSock)
{
	#define FBUFFERSIZE 256
	#define TNOMFICHIER 50
	struct sockaddr_in sourcaSock;
	int sizeOfsourcaSock=sizeof(sourcaSock);
	
	int sourcdSock,res,end=0;	
	off_t octrecuTot, octrecuAct, tailleFichierRecevoir, octrecuManque;
			 
	char fileBuffer[FBUFFERSIZE];
	
	printf("serveur Crée!\n");
		
	
	
	int cecrit;
	

	FILE *fp;
	short unsigned int er;
	char * tailleFichierRecevoirc=(char *)&tailleFichierRecevoir;
	
	char nomFichierR[TNOMFICHIER];
    
    char IPatrib[INET_ADDRSTRLEN];

    char nomFichierFinal[TNOMFICHIER + INET_ADDRSTRLEN];
	
	while(!end)
	{
		memset(IPatrib,'\0',INET_ADDRSTRLEN);
		memset(nomFichierR,'\0',TNOMFICHIER);
		printf("j’attends une connexion\n");	
		
		/*Remplir le fileBuffer de a pour des raison d'observation
		  du remplissage de ce même buffer*/
		memset(fileBuffer,'a',FBUFFERSIZE);
		er=0;
		octrecuTot=0;
	
		//c[0]++;
		sourcdSock=accept(dSock,(struct sockaddr*) &sourcaSock, (socklen_t*) &sizeOfsourcaSock);
		

		pid_t pid;
		if(0 == (pid=fork()))
		{
			close(dSock);
			/*
				Après accept, on cherche à identifier l'IP du client en s'inspirant de 
				comment on fait pour printé l'IP de serveur qu'on crée.
			*/

			extraireIP(IPatrib,&sourcaSock);

			/*On recoit le nom du fichier à recevoir*/
			while (octrecuTot<TNOMFICHIER && er==0)
			{
				//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
				res=recevoirTCP(sourcdSock, nomFichierR,TNOMFICHIER);
				if(res<0)
				{
					printf("RecevoirFichierTCP() : erreur pendant le reçois de la taille du fichier \n");
					er=1;
				}	
				else if(res == 0)
				{
					printf("RecevoirFichierTCP() : Warning res == 0");
					er=1;
				}
				else
				{
					octrecuTot+=res;
				}
			}


			 	
			strcat(nomFichierFinal,IPatrib);
			strcat(nomFichierFinal,"--");
			strcat(nomFichierFinal,nomFichierR);
			if((fp = fopen(nomFichierFinal,"w"))==NULL)
			{
				perror("erreur dans la création du nouveau fichier reçu");
				er=1;
			}

			printf("nomFichierFinal : %s\n",nomFichierFinal);
			
			octrecuTot=0;
			/*On recoit un entier: la taille du fichier à recevoir*/
			while (octrecuTot<sizeof(off_t) && er==0)
			{
				//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
				res=recevoirTCP(sourcdSock,tailleFichierRecevoirc ,sizeof(off_t));
				if(res<0)
				{
					printf("RecevoirFichierTCP() : erreur pendant le reçois de la taille du fichier \n");
					er=1;
				}	
				else if(res == 0)
				{
					printf("RecevoirFichierTCP() : Warning res == 0");
					er=1;
				}
				else
				{
					octrecuTot+=res;
				}
			}
				
			printf("\e[1;37m Taille du fichier à recevoir => %li <=\e[0m\n",tailleFichierRecevoir);

			
			octrecuTot=0;
			octrecuAct=0;
			
			/*
				En fonction de la taille du fichier a recevoir, on lit 
				tout les octets reçue jusqu’à ce qu'on est reçue 
				tailleFichierRecevoir octets
			*/		
			 	
			while (octrecuTot<tailleFichierRecevoir && er==0)
			{
				octrecuManque =tailleFichierRecevoir - octrecuTot;
				//printf("octrecuTot=%lu\n",octrecuTot);
				//printf("il manque = %lu\n",octrecuManque);
				
				//inutile car la socket du client est relié au desc de fichier sourcdSock
				//res=recvfrom(sourcdSock,&fileBuffer,sizeof(fileBuffer),0,(struct sockaddr*) sourcaSock,sizeof(sourcaSock));
				
				// printf("fileBuffer avant reçois de message\n");
				// afficheFileBuffer(fileBuffer,FBUFFERSIZE);
				res=recevoirTCP(sourcdSock, fileBuffer , FBUFFERSIZE > octrecuManque ? octrecuManque : FBUFFERSIZE );
				printf("\t coucou\n");
				if(res<0)
				{
					printf("recevoirFichierTCP() : erreur lors de recoit\n octrecuTotal = %lu ",octrecuTot);
					er=1;
				}

				else if (res==0)
				{
					printf("recevoirFichierTCP() : ress==0 lors de recoit\n octrecuTotal = %lu ",octrecuTot);
					er=1;
				}
				else
				{
					octrecuTot+=res;
					octrecuAct+=res;
				}

				if((octrecuTot >= tailleFichierRecevoir) )
				{	
					/*
						Tous les octets sont reçus, écriture dans le
						fichier en fonction des octets actuellement
						reçus. octrecuAct correspond au nombre de octets
						reçus avant d'attendre la taille maximal du buffer
						du fichier. Quand la taille max est atteinte, 
						octrecuAct repasse à 0.
					*/
					//printf("ecriture final dans fichier! \n");
					cecrit=fwrite(fileBuffer , sizeof(char), octrecuAct , fp);
					printf("oct ecrit dans fichier : %i\n",cecrit);
					
					
					break;
				}

				else if(FBUFFERSIZE == octrecuAct)
				{
					/*
						Toujours en cours de reçois d'octets, le buffer
						doit être vider dans le fichier car il est 
						remplit.
					*/
					//printf("ecriture dans fichier\n ");
					
					cecrit=fwrite(fileBuffer , sizeof(char), octrecuAct , fp);
					//printf("oct ecrit dans fichier : %i\n",cecrit);
					octrecuAct=0;
					memset(fileBuffer,'a',FBUFFERSIZE);
					printf("oct ecrit dans fichier : %i\n",cecrit);
				}
			}

			char finiRecois;
			res=send(sourcdSock,&finiRecois,sizeof(finiRecois),0);


			printf("\t>>>>EOT<<<<<\n");
			printf(":Bilan Transmission:\n");
			if(octrecuTot==tailleFichierRecevoir && er==0)
			{
				printf("Message correctement reçu \n");
			}
			else
			{

				printf("octets reçu: %lu\n",octrecuTot);
				printf("octets censé recevoir: %li \n ", tailleFichierRecevoir);
			}
			printf("end\n\n");		     				
			
			fclose(fp); 		
			close(sourcdSock);
	 		// printf("test: ");
	 		// (&fileBuffer[0]==fileBuffer) ? printf ("vrai\n") : printf("faux\n");
			end=1;
		} 	

		else
		{
			close(sourcdSock);

		}
	}

}



void chatTCPmultiplex(int dSock)
{	
	int dSC;
	fd_set set,settmp;
	FD_ZERO(&set);FD_SET(dSock,&set);
	int max = dSock;
	int df;
	while(1)
	{
		settmp=set;
		select(max +1, &settmp,NULL,NULL,NULL);
		for(df=3; df<= max; df++)
		{
			if(!FD_ISSET(df,&settmp)) continue;
			if(df ==dSock)
			{
				dSC= accept(dSock,NULL,NULL);
				FD_SET(dSC,&set);
				if(max<dSC) max= dSC;
				continue;

			}
			//struct requete req;
			
			// if(recv(df,&req,sizeof(req),0)<=0)
			// {
			// 	FD_CLR(df,&set);
			// 	close(df);
			// 	continue;
			// }
			//int rep=taiter(req);
			// send(df,&rep,sizeof(int),0);
		}
	}
}





