#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>


#ifndef TAILLEHOTE
#define TAILLEHOTE 30
#endif

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 30
#endif

int main(int argc, char** argv){
  
  // Test du nombre d'arguments
  if(argc!=2){ 
    
    printf("erreur de paramètre");
    
  }else{
    
	// Récupération du nom d'hôte passé en paramètre
    char* nom = argv[1]; 
    int res;
    struct addrinfo *resultat; 
    
	// Execution de getaddrinfo sur le nom d'hôte récupéré, 'résultat' récupère les structures addrinfo renvoyées
    res = getaddrinfo(nom, NULL, NULL, &resultat); 

	// Si getaddrinfo remonte une erreur
    if(res !=0){ 
      
	  // gai_strerror() traduit les codes d'erreur pour qu'ils soient compréhensibles
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(res));

	  // On sort en erreur.	
      exit(EXIT_FAILURE);
    
	// Sinon on continue les traitements	
    }else{

      /****** Affichage des adresses IP  *****/
      
      struct addrinfo *info;
      int i=1;
      
      printf("\n Liste des adresses: \n \n");
      
	  // Récupération et parcourt des structures addrinfo récupérées dans 'résultat'
      for(info = resultat; info != NULL; info = info->ai_next){ 

		// Cas IPV4
		if(info->ai_family = AF_INET){ 
		  
		  struct sockaddr_in *sock;
		  
		  // Récupération de la struct sockaddr de la structure de resultat en court pour afficher l'adresses ipv4.
		  sock = (struct sockaddr_in *) info->ai_addr; 
		  
		  // Affichage
		  printf("addr n°%i IPV4: %s \n",i,inet_ntoa(sock->sin_addr));
		
		// Cas IPV6
		}else{
		  if(info->ai_family = AF_INET6){
			
			char straddr[INET6_ADDRSTRLEN];
			struct sockaddr_in6 *sock6;
			
			// Récupération de la struct sockaddr de la structure de resultat en court pour afficher l'adresses ipv6.
			sock6 = (struct sockaddr_in6 *) info->ai_addr;
		    
			// Affichage
			printf("addr n°%i IPV6: %s \n",i,inet_ntop(AF_INET6, &sock6->sin6_addr,straddr, sizeof(straddr)));
		  }
		  
		}
		i++;

      }

	   /****** Affichage du nom officiel  *****/
      
      int name;
      socklen_t socklen;
      struct addrinfo *nom;
      char hote[TAILLEHOTE];

      nom=resultat;
	  
	  // Exécution de getnameinfo sur l'adresse ip récupérée dans 'nom'. 
	  // Le nom officiel de l'hote est retourné via le paramètre 'hote'
      name = getnameinfo(nom->ai_addr,nom->ai_addrlen,hote,sizeof(hote),NULL,0,0);

	  // Si getnameinfo remonte une erreur
      if(name !=0){
	    
		// Affichage
		// gai_strerror() traduit les codes d'erreur pour qu'ils soient compréhensibles
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(res));
		
		// On sort en erreur
		exit(EXIT_FAILURE);
	  
	  // Sinon on continue les traitement
      }else{
		
		// Affichage du nom officiel.
		printf("\n nom officiel: %s\n \n",hote);
	    
      }

    }
    
	// Libèration de la mémoire allouée par getaddrinfo()
    freeaddrinfo(resultat); 

  }  

  // Fin des traitements
  return 0;
}
