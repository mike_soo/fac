/**
 * \file host.h
 * \author Elodie Savajols
 * \version 0.1
 * \date 10 Fevrier 2017
 *
 * Travaux pratique HLIN611 - Reseaux
 *
 */

#ifndef __HOST_H__
#define __HOST_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

void findIP (const char*);
char* findAndReturnIP (const char*);
void findOfficialName (const char*);

#endif
