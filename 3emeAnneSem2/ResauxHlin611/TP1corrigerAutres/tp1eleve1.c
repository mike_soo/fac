#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <iostream>

using namespace std;

int main (int argc, char *argv[]) {

  struct addrinfo hints, *res, *p;
  void *addr;
  int status;
  char ipstr[INET6_ADDRSTRLEN], ipver;

  if (argc != 2) {
    cout << "Erreur : il manque le hostname." << endl;
    return 1;
  }

  memset(&hints, 0, sizeof hints);
  hints.ai_socktype = SOCK_STREAM; // Une seule famille de socket
  hints.ai_flags = AI_CANONNAME;

  if ((status = getaddrinfo(argv[1], NULL, &hints, &res)) != 0) {
    cout << "getaddrinfo : " << gai_strerror(status);
    return 2;
  }

  cout << "Adresse IP pour " << argv[1] << endl;

  p = res;
  
  while (p != NULL) {

    // Identification de l'adresse courante
    if (p->ai_family == AF_INET) { // IPv4
      struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
      addr = &(ipv4->sin_addr);
      ipver = '4';

    }
    else { // IPv6
      struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
      addr = &(ipv6->sin6_addr);
      ipver = '6';

    }

    // Conversion de l'adresse IP en une chaîne de caractères
    inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
    cout << "IPv" << ipver << " " << ipstr << "\ncanonname : " << p->ai_canonname << endl;

    // Adresse suivante
    p = p->ai_next;
  }

  struct sockaddr *sa = res->ai_addr;
  socklen_t len = 100;
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

  if (getnameinfo(sa, len, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)
  {
    printf("host=%s, serv=%s\n", hbuf, sbuf);
  }

  // Libération de la mémoire occupée par les enregistrements
  freeaddrinfo(res);


  // Remarque : le programme termine tout seul sans soucis.
  return 0;
}