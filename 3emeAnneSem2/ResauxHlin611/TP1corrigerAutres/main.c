/**
 * \file main.c
 * \author Elodie Savajols
 * \version 0.1
 * \date 10 Fevrier 2017
 *
 * Travaux pratique HLIN611 - Reseaux
 *
 */

#include "host.h"

int main(int argc, char* argv[]){
	printf("###############################################\n");
	printf("###############TRAVAUX PRATIQUE################\n");
	printf("###############################################\n");
  
	/*Test distint des fonctions : *
   
	printf("Veuillez indiquer le nom de l'hote :\n");
	char* hostname;
	char* ip;
	scanf("%ms", &hostname);
	findIP(hostname);
	printf("Veuillez indiquer une adresse ip :\n");
	scanf("%ms", &ip); 
	findOfficialName(ip);
	//*/

	/*
	*Version parametre
	*/
	char* ip = findAndReturnIP(argv[1]);
	findOfficialName(ip);
}
