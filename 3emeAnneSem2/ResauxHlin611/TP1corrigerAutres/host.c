/**
 * \file host.c
 * \brief Liste de fonctions permettant de retrouver une adresse IP  * en fonction d'un nom d'hote et inversement.
 * \author Elodie Savajols
 * \version 0.1
 * \date 10 Fevrier 2017
 *
 * Travaux pratique HLIN611 - Reseaux
 *
 */

#include "host.h"

/**
 * \fn void findIP (const char* hostname)
 * \brief Fonction qui affiche une adresse ip en fonction d'un nom d'hote.
 *
 * \param hostname, une chaine de caratere indiquant le nom d'hote.
 */
 
void findIP (const char* hostname){

	struct addrinfo hints;
	struct addrinfo  *res, *p;
	char ipstr[INET_ADDRSTRLEN];
	int err;

	//Initialisation de hints
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = 0;
	hints.ai_family = AF_INET;
	hints.ai_socktype = 0;
	hints.ai_protocol = 0;
	hints.ai_addrlen = 0;
	hints.ai_addr = NULL;
	hints.ai_canonname = NULL;
	hints.ai_next = NULL;
  
	//Recuperation du resultat d'execution de getaddrinfo(...)
	err=getaddrinfo(hostname, NULL, &hints, &res);
	//Si erreur :
	if (err!=0){
		printf("Erreur: %d\n", err); //Affiche le code erreur
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err)); //Affiche la raison de l'erreur
	}
	//Si aucune erreur :
	else{
		printf("Adresses IP de %s:\n", hostname);
		p = res;
		while (p != NULL) {
			//Creation d'une structure sockaddr_in pour stocker l'adresse ip
			struct sockaddr_in *addrIp = (struct sockaddr_in *)p->ai_addr;
			//Utilisation de inet_ntop pour transformer l'ip en chaine de caractere.
			inet_ntop(p->ai_family, &(addrIp->sin_addr), ipstr, sizeof ipstr);
			printf(" IP: %s\n", ipstr);
			p = p->ai_next;
		}
		freeaddrinfo(res);
	}
}

/**
 * \fn void findAndReturnIP (const char* hostname)
 * \brief Fonction qui affiche une adresse ip en fonction d'un nom d'hote et renvoie l'adresse sous forme de chaine de caractere.
 *
 * \param hostname, une chaine de caratere indiquant le nom d'hote.
 */
 
char* findAndReturnIP (const char* hostname){

	struct addrinfo hints;
	struct addrinfo  *res, *p;
	char* result;
	char ipstr[INET_ADDRSTRLEN];
	int err;

	//Initialisation de hints
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = 0;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_addrlen = 0;
	hints.ai_addr = NULL;
	hints.ai_canonname = NULL;
	hints.ai_next = NULL;
	
	//Recuperation du resultat d'execution de getaddrinfo(...)
	err=getaddrinfo(hostname, NULL, &hints, &res);
	//Si erreur :
	if (err!=0){
		printf("Erreur: %d\n", err); //Affiche le code erreur
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err)); //Affiche la raison de l'erreur
		result="N";
		exit(EXIT_FAILURE);
	}
	//Si aucune erreur :
	else{
		printf("Adresses IP de %s:\n", hostname);
		p = res;
		while (p != NULL) {
		//Creation d'une structure sockaddr_in pour stocker l'adresse ip
		struct sockaddr_in *addrIp = (struct sockaddr_in *)p->ai_addr;
		//Utilisation de inet_ntop pour transformer l'ip en chaine de caractere.
		inet_ntop(p->ai_family, &(addrIp->sin_addr), ipstr, sizeof ipstr);
		printf(" IP: %s\n", ipstr);
		p = p->ai_next;
		}
	result=ipstr;
	freeaddrinfo(res);
	}	
	return result;
}

/**
 * \fn void findOfficialName (const char* ip)
 * \brief Fonction qui affiche le nom de l'hote a partir d'une adresse ip passee en parametre.
 *
 * \param ip, une chaine de caratere indiquant l'adresse ip.
 */
 
void findOfficialName(const char* ip){

	struct sockaddr_in sa;
	char host[NI_MAXHOST];
	int res;
	if(ip[0]=='N')
	  	printf("Erreur");
	else{
		//On stocke l'ip dans sa
		sa.sin_family=AF_INET;
		inet_pton(AF_INET, ip, &(sa.sin_addr));
		//Recuperation du resultat d'execution de getnameinfo(...)
		res= getnameinfo((struct sockaddr*)&sa, sizeof sa, host, sizeof host, NULL, 0, NI_NAMEREQD);
		//Si aucune erreur:
		if (res) {
        		printf("error: %d\n", res);
        		printf("%s\n", gai_strerror(res));
    		}
		//Sinon :
		else{
			printf("Hote de l'adresse %s :\n", ip);
			printf(" %s\n", host);
		}
	 }
}
