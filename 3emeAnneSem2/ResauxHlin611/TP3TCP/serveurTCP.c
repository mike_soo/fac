#include "serveurTCP.h"

int creerServeurTCP(char* IP,int PORT,int dSock)
{   
    int res;
	
	if (dSock<0)
	{	
		perror("erreur dans creation de socket!\n");	
		return -1;
	}
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	

    if(0!=strcmp(IP,"0"))
    {
		res=inet_pton(AF_INET,IP,&ad.sin_addr);
        if(res<=0)
        {
            perror("inet_pton erreur");
            return -1;
        } 
    }
	else
	{
		ad.sin_addr.s_addr= INADDR_ANY;
	}

	/*
		Pas d'erreur à envoyer par htons() d’après la documentation de cette même
		fonction
	*/
	ad.sin_port=htons((short)PORT);
	
    if(bind(dSock,(struct sockaddr*)&ad,sizeof(ad))<0 )
	{
		perror ("erreur lors de l'utilisation de bind");
		return -1;

	}

	else
	{
		socklen_t sizeofad=sizeof(ad);
		if (getsockname(dSock, (struct sockaddr *)&ad,&sizeofad) == -1)
   		//perror("getsockname erreur");
        {
    		perror("getsockname erreur");
            return -1;
        }
	    
	    char *IPatrib;
        char IPatrib_ntop[1024];
        printf("IP atribé avec ntoa : %s\n",(IPatrib = inet_ntoa(ad.sin_addr)));

        inet_ntop(AF_INET,&(ad.sin_addr),IPatrib_ntop,1024);

        printf("IP atribué avec ntop : %s\n",IPatrib);
        printf("PORT :  %i\n", ntohs(ad.sin_port)); 

        if(listen(dSock,7)<0)
        {
        	perror("Erreur dans listen\n");
        }
        
        
    	   
	}

    

    return dSock;
}