#include "clientTCP.h"

int main(int argc, char ** argv)
{
		
	if (argc>=3)
	{
		char* IP=argv[1];
		
		short int PORT=atoi(argv[2]);
		int res;
		int dSock = socket(PF_INET,SOCK_STREAM,0);
		struct sockaddr_in ad;
	
		
		/*
			Pas d'erreur à envoyer par htons() d’après la documentation de cette même
			fonction
		*/
	
		if(0<creerClientTCP(IP,PORT,dSock,&ad))
		{	    
			printf("client Crée!\n");
			char msg[512];
			memset(msg,'a',512);
			unsigned int octenvTot=0;
			printf("saisir un message puis entrer pour envoyer\n");
			//scanf("%s",msg);
			while (octenvTot<512)
			{
				
				
				printf("envoi : %s\n",msg);

				res=send(dSock,&msg,sizeof(msg),0);
				
				if(res<0)
				{
					perror("Erreur dans envoi du message\n");
				}
				else if (res ==0)
				{
					printf("res == 0\n");
					break;
				}
				else
				{
					octenvTot+=res;
				}

			}
		}

		close(dSock);
			
	}

	else
	{
		perror("Erreur de la creation du client\n");
	}
		
		
		
	

	
	return 0;	

} 