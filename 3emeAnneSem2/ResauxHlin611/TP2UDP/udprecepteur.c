#include "udprecepteur.h"

void getIp(char* hostname,char* hostIP)
{
    /*
        Le code suivant utilise getaddrinfo() pour trouver la 
        liste des adresses reliés au nom de domaine fourni en paramètre où chaque 
        adresse IP retrouvé est associé à son nom canonique grâce à l'appel de getnameinfo() 
        sur chaque adresse retrouvé.
    */

    struct addrinfo hints,*res;
    /*
        hints sera la structure contenant des attribut définis par la suite qui permettrons
        d'orienter le fonctionnement de getaddrinfo()
    */
    memset(&hints,0,sizeof(hints)); //tous les attribut de la struc hints initialisés à 0.
    /*
        La valeur AF_UNSPEC  indique que getaddrinfo() doit renvoyer les adresses de socket
        de n'importe quelle type de famille d'adresses (par exemple, IPv4 ou IPv6).
        Autrement dit AF_UNSPEC permettra d'accepter tout type d'adresse.
    */
    hints.ai_family = AF_UNSPEC; 
    
    /*
        La valeur de socktype décrit le type de socket désiré lors de la
        demande des structures addrinfo ici on précise que l'on cherche à utiliser le 
        protocole en Diagramme.
    */
    hints.ai_socktype = SOCK_DGRAM;


    /*
        getaddrinfo() retourne une structure de type addrinfo qui sera stockée dans res
        qui contient (entre autres) une ou plusieurs adresses Internet obtenues en combinant 
        les fonctionnalités de gethostbyname() et getservbyname().  
    */
    if(getaddrinfo(hostname,NULL,&hints,&res)!= 0)
    {
       printf("Erreur hostname non trouvé, avez vous saisi une adresse valide?\n" );
        return;
    }

    
    char serv[512];
    
     
    /*
        Ici l'ajout du paramètre NI_NUMERICHOST nous permet de changer le comportement 
        de getnameinfo(), ce qui permet de stocker cette fois-ci dans hostIP l'adresse IP 
        courante.
    */
    getnameinfo(res->ai_addr,res->ai_addrlen,hostIP,sizeof(hostIP),serv,sizeof(serv),NI_NUMERICHOST);
    
    

    freeaddrinfo(res);

    

}



int creerServeurUDP(char* IP,int PORT,int dSock)
{   
    int res;
	
	if (dSock<0)
	{	
		perror("erreur dans creation de socket!\n");	
		return -1;
	}
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	

    if(0!=strcmp(IP,"0"))
    {
		res=inet_pton(AF_INET,IP,&ad.sin_addr);
        if(res<=0)
        {
            perror("inet_pton erreur");
            return -1;
        } 
    }
	else
	{
		ad.sin_addr.s_addr= INADDR_ANY;
	}

	/*
		Pas d'erreur à envoyer par htons() d’après la documentation de cette même
		fonction
	*/
	ad.sin_port=htons((short)PORT);
	
    if(bind(dSock,(struct sockaddr*)&ad,sizeof(ad))<0 )
	{
		perror ("erreur lors de l'utilisation de bind");
		return -1;

	}

	else
	{
		socklen_t sizeofad=sizeof(ad);
		if (getsockname(dSock, (struct sockaddr *)&ad,&sizeofad) == -1)
   		//perror("getsockname erreur");
        {
    		perror("getsockname erreur");
            return -1;
        }
    	else
        {
            char *IPatrib;
            char IPatrib_ntop[1024];
            printf("IP atribé avec ntoa : %s\n",(IPatrib = inet_ntoa(ad.sin_addr)));

            inet_ntop(AF_INET,&(ad.sin_addr),IPatrib_ntop,1024);

            printf("IP atribué avec ntop : %s\n",IPatrib);
            printf("PORT :  %i\n", ntohs(ad.sin_port)); 

        }
        
    	   
	}

    

    return dSock;
}