#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "udprecepteur.h"
int main(int argc, char ** argv)
{
		
	if (argc>=3)
	{
		char* IP=argv[1];
		char * PORT=argv[2];
		int res;
		int dSock = socket(PF_INET,SOCK_DGRAM,0);
		
		if((creerServeurUDP(IP,atoi(PORT),dSock))>0)
		{

			int msg=0;
			printf("serveur Crée!\n");
			
			if(argc==3)
			{	
				printf("j’attends un message\n");
				while (-1!=msg)
				{
					res=recvfrom(dSock,&msg,sizeof(msg),0,NULL,NULL);
					if(res<0)
					{
						perror("Erreur dans reçois du message\n");
					}
					else
					{
						printf ("je recois %i\n",msg);

					}
				}
			}

			if(argc==4)
			{	
				/*
					Dans le cas où le 3eme arguement argv[3] soit 
					-som, on commence la somme proposé dans l'exercice 2.
				*/

				if(0==strcmp(argv[3],"-som"))
				{
					int somme=0;
					printf("j'attend des entiers pour en faire une somme\n");
					while (-1!=msg)
					{

						res=recvfrom(dSock,&msg,sizeof(msg),0,NULL,NULL);
						if(res<0)
						{
							perror("Erreur dans recois du message");
						}
						else
						{
							somme+=msg;
							printf("Somme Courante : %i\n",somme);	
						}
					}

				}

				/*
					Limites des paquets
			
						–> le programme émetteur envoie successivement deux entiers saisis au clavier (il fait donc 2 envois).
						–> le programme récepteur demande à recevoir une suite d’octets dont la taille est passée en paramètre, et affiche le
						nombre d’octets reçus.

				*/

				else if(0==strcmp(argv[3],"-lim"))
				{
					int n;
					printf("J’attends un paquet de taille n octets, ");
					printf("saisir n:\n");
					scanf ("%i",&n);

					res=recvfrom(dSock,&msg,n*8,0,NULL,NULL);
					if(res<0)
					{
						perror("Erreur dans reçois du message\n");
					}
					else
					{
						printf("Nombre d'octets reçus : %i\n",res);	
						printf("Entier reçu %i",n)
					}
				}


				else if(0==strcmp(argv[3],"-limp"))
				{
					int n;
					printf("J’attends un paquet de taille n octets, ");
					printf("saisir n:\n");
					scanf ("%i",&n);

					res=recvfrom(dSock,&msg,n*8,0,NULL,NULL);
					if(res<0)
					{
						perror("Erreur dans reçois du message\n");
					}
					else
					{
						printf("Nombre d'octets reçus : %i\n",res);	
						printf("Entier reçu %i",n)
					}
				}
							
			}


			printf("fermer serveur");			
				close(dSock);
			
		}
		else
		{
			perror("Erreur dans création de serveurUDP\n");
		}

	}


	return 0;	

}