#include "udpemeteur.h"

int main(int argc, char ** argv)
{
		
	if (argc>=3)
	{
		char* IP=argv[1];
		
		short int PORT=atoi(argv[2]);
		int res;
		int dSock = socket(PF_INET,SOCK_DGRAM,0);
		struct sockaddr_in ad;
		socklen_t sizeOfad=sizeof(ad);
		
		/*
			Pas d'erreur à envoyer par htons() d’après la documentation de cette même
			fonction
		*/
	
		if(0<creerClientUDP(IP,PORT,dSock,&ad))
		{	    
			printf("client Crée!\n");
			int msg=-1;

			if(argc==3)
			{
				printf("saisir un numéro puis entrer pour envoyer\n");
				while (0!=msg)
				{
					scanf("%i",&msg);
					res=sendto(dSock,&msg,sizeof(msg),0,(struct sockaddr*)&ad,sizeOfad);
					
					if(res<0)
					{
						perror("Erreur dans envoi du message\n");
					}	
				}
			}

			else if(argc==4)
			{
				if(0==strcmp(argv[3],"-som"))
				{
					int somme=0;
					int N;
					printf("Envois d'une suite d'entiers afin de comparer la somme coté client et serveur\n");
					scanf("%i",&N);
					printf("Envoi des entiers de 1 à %i",N);
					for (int i=0;i<=N;i++)
					{
						msg=i;
						res=sendto(dSock,&msg,sizeof(msg),0,(struct sockaddr*)&ad,sizeOfad);
						
						if(res<0)
						{
							perror("Erreur dans envoi du message\n");
						}
						else
						{
							somme+=i;
							printf("Somme Courante : %i\n",somme);	
						}	
					}
				}

				else if(0==strcmp(argv[3],"-lim"))
				{

					int n1,n2;
					printf("Envois de deux entiers consecutifs:\n");
					
					printf("Saisir entier 1 : \n");
					scanf("%i",&n1);
					/*
						Envois du premier entier.
					*/
					res=sendto(dSock,&n1,sizeof(msg),0,(struct sockaddr*)&ad,sizeOfad);
					
					if(res<0)
					{
						perror("Erreur dans envoi du message\n");
					}
					else
					{
						printf("n1 envoyé\n");	
					}


					/*
						Envois du deuxieme entier.
					*/
					
					printf("Saisir entier 2 : \n");
					scanf("%i",&n2);
													
					res=sendto(dSock,&n2,sizeof(msg),0,(struct sockaddr*)&ad,sizeOfad);
					
					if(res<0)
					{
						perror("Erreur dans envoi du message\n");
					}
					else
					{
						printf("n2 envoyé\n");	
					}	

				}

				else if(0==strcmp(argv[3],"-limp"))
				{
					int n;
					printf("J’envoi un paquet de taille n octets, ");
					printf("saisir n:\n");
					scanf ("%i",&n);

					for(int i=0;i<n;i++)
					{
						
					}
					res=sendto(dSock,&n1,sizeof(msg),0,(struct sockaddr*)&ad,sizeOfad);
					
					if(res<0)
					{
						perror("Erreur dans envoi du message\n");
					}
					else
					{
						printf("n1 envoyé\n");	
					}
				}
						
				/*
					Dernier message pour dire au serveur que la somme est terminé.
				*/
				// msg=-1;
				// res=sendto(dSock,&msg,sizeof(msg),0,(struct sockaddr*)&ad,sizeOfad);
					
				// if(res<0)
				// {
				// 	perror("Erreur dans envoi du message\n");
				// }
				// else
				// {
					
				// 	printf("Fin de transmition\n");	
				// }	

			}
		}

		else
		{
			perror("Erreur de la creation du client\n");
		}
		close(dSock);
		
		
		
	

	}
	return 0;	

} 