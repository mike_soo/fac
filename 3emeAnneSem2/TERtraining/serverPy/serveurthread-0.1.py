# coding=utf-8
import socket
import threading
import SocketServer
import logging

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data="";

        while data!="EOT":            
            
            """
            data = self.request.recv(1024)
            cur_thread = threading.current_thread()
            #print "je suis la thread ",cur_thread.name," qui repond!"
            
            response = "ouaich bien recu {}: {}".format(cur_thread.name, data)
            self.request.sendall(response)
            """
            
            data = self.request.recv(1024)
            cur_thread = threading.current_thread()
            response = "{} : {} Bonjour, rentrez votre nom ".format(cur_thread.name, data)
            self.request.sendall(response)

            data = self.request.recv(1024)
            response = "{} : Votre nom est : {}".format(cur_thread.name, data)
            print response
            self.request.sendall(response)

            data = self.request.recv(1024)
        #print "activeCount=",threading.activeCount() #Returns the number of thread objects that are active.

        #print "currentThread=",threading.currentThread() #Returns the number of thread objects in the caller's thread control.

        #print "je suis la thread ",cur_thread.name," qui finis!"
        self.finish()
        #print "threading.enumerate=",threading.enumerate() #Returns a list of all thread objects that are currently active.

    def finish(self):
        
        return SocketServer.BaseRequestHandler.finish(self)




class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
   
    def __init__(self, server_address, handler_class=ThreadedTCPRequestHandler):
        
        
        SocketServer.TCPServer.__init__(self, server_address, handler_class)
        return
    """    
    def serve_forever(self):
        #ThreadedTCPRequestHandler.handler_class.handle()
        
        while self.serving:
            r,w,e = select.select([self.socket], [], [], self.pause)
            if r:
                self.handle_request()
    """    

    

if __name__ == "__main__":
    # Port 0 means to select an arbitrary unused port
    HOST, PORT = "192.168.0.25", 10000  
    #pour HOST utiliser l'adresse local 
    #attribué par le router 

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()

    print "Server loop running in thread:", server_thread.name
    #server_thread.join()

    line=""
    
    while line!="stop":
       print "dans thread",server_thread.name
       line = raw_input('Saisir stop pour arreter serveur \n')


    server.shutdown()
    server.server_close()