import logging
import sys

import socket
import threading

#create an INET, STREAMing socket
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#bind the socket to a public host,
# and a well-known port
serversocket.bind(('192.168.0.25', 10000))
#become a server socket
serversocket.listen(5)
while 1:
    #accept connections from outside
    (clientsocket, address) = serversocket.accept()
    #now do something with the clientsocket
    #in this case, we'll pretend this is a threaded server
    print "oui conection"
    #ct = client_thread(clientsocket)
    #ct.run()