#!/usr/bin/env python3
import os,sys,re,SocketServer
from naoqi import ALProxy
 
class hwRequestHandler( SocketServer.StreamRequestHandler ):
  def handle( self ):
    self.wfile.write("Hello World!\n")


server = SocketServer.TCPServer( ("", 2525), hwRequestHandler )
server.serve_forever()