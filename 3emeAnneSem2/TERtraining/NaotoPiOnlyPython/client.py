import logging
if __name__ == '__main__':
    import socket
    import threading

    ip, port = ('127.168.0.25', 10000)
    logger = logging.getLogger('client')
    

    # Connect to the server
    logger.debug('creating socket')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logger.debug('connecting to server')
    s.connect((ip, port))
    
    ## Send the data
    #message = 'Hello, world'
    #logger.debug('sending data: "%s"', message)
    #len_sent = s.send(message)

    ## Receive a response
    #logger.debug('waiting for response')
    #response = s.recv(len_sent)
    #logger.debug('response from server: "%s"', response)

    # Clean up
    logger.debug('closing socket')
    s.close()
    logger.debug('done')
    