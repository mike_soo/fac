clc;
clear;

% Identification

%1. zpk = mod�le z�ro, pole, gain
h = zpk([],[-4 -5 -1],100)

%2. Trac�s
subplot(211);
grid;
impulse(h);
subplot(212);
step(h);

%3. Point d'inflexion
% On cherche le max de la r�ponse impulsionnelle
% La d�riv�e de la r�ponse indicielle est la r�ponse impulsionnelle.
% Equation de la tangente au point a:
%   y=f'(a)(x-a)+f(a)
%   a = 0.716s
%   f(a)=1.27
%   f'(a)=2.87
%   donc
%   y = 2.87 (x-0.716)+1.27
%   y = 2.87 x - 0.785
hold on
t=[0:0.029:12];
y=2.87*t-0.785;
plot(t,y);


% 5
%   Tu = 0.27s
%   Ta = 2 - 0.27 = 1.73s
%   Tu/Ta=0.156    donc n=2 d'apr�s le tableau
%   Si on prend n=2  Ta/tau=2.7183  donc tau=Ta/2.7183 = 1.73/2.7183 = 0.64
%   On a Tu'/tau =0.2817 donc Tu'=0.2817*tau = 0.18
%   r = Tu - Tu' =0.27 - 0.18 = 0.09
%   K = H(0)=5
%
%   T(p)= (K/(tau*tau))e(-rp) / ((p+1/tau)(p+1/tau))

figure
step(h);
hold on;
h2 = zpk([],[-1/0.64 -1/0.64],5/(0.64*0.64),'iodelaymatrix',0.09);
step(h2,'r');

