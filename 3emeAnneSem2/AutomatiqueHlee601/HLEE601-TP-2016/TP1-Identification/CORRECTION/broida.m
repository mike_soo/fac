clc;
clear;

% Identification Broida

%1. zpk = mod�le z�ro, pole, gain
h = zpk([],[-4 -5 -1],100)

%2. Trac�s
step(h);

%3. Calcul de tau et r
% Valeur finale = 5
% 28% de la valeur finale = 0,28 * 5 = 1,4
%    t1 = 0.764s
% 40% de la valeur finale = 0,40 * 5 = 2
%   t2 = 0.982
% tau = 5.5(t2-t1)= 5.5(0.982-0.764)= 1,2
% r = 2.8*0.764 - 1.8*0.982 = 0.37

%   K = H(0)=5
%
%   T(p)= (K/tau)*e(-rp) / (p+1/tau)

figure
step(h);
hold on;
h2 = zpk([],[-1/1.2],5/1.2,'iodelaymatrix',0.37);
step(h2,'r');

