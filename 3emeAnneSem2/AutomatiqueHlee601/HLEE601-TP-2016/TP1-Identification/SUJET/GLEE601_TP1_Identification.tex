\documentclass[a4paper,12pt]{exam}

\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[dvips]{graphicx}
\usepackage{color}
\usepackage{colortbl}
\usepackage{array}
\usepackage{amsfonts}
\date{\today}


\newcommand{\fw}[1]{\fullwidth{#1}}
\newcommand{\ud}{\mathrm{d}}
\newcommand{\attention}{ \mbox{\textcolor{red}{\boldmath{$\bigtriangleup$}}}\raisebox{0.1em}{\hspace{-.63em}\begin{scriptsize}!~~\end{scriptsize} }}
\pagestyle{head}

\begin{document}
\header{\small GLEE601}{}{\small 2015}
\begin{center}
\begin{Large}\textbf{TP1}\end{Large}
\end{center}


\section{Identification d'un système}
\subsection{Méthode de Strejc}
La méthode de Strejc permet d'approcher le comportement d'un système par une fonction d'ordre n avec un retard pur.
$$ T(p)=\frac{K e^{-rp}}{(1+\tau p)^n}$$

Soit le système linéaire défini par la fonction de transfert suivante:
$$H(p) = \frac{100}{(p+4)(p+5)(p+1)}$$
Nous allons approcher cette fonction par une fonction T(p) avec la méthode de Strejc.
\begin{questions}
\question Définir dans Matlab, le modèle \textit{zpk} du système représenté par H(p).
\question Tracer les réponses impulsionnelle et indicielle du système.
\question Déterminer l'abscisse du point d'inflexion i de la réponse impulsionnelle.
\question Tracer sur la réponse indicielle, la tangente au point i.
\question Déterminer les valeurs de Strejc (n, $\tau$ et r) pour identifier le système et calculer T(p).
\question Tracer la réponse indicielle de T(p) et comparer avec la réponse indicielle de H(p).
\end{questions}
Voir en annexe le rappel sur la méthode de Strejc.

\subsection{Méthode de Broida}
La méthode de Broida permet d'approcher le comportement d'un système par un premier ordre avec un retard pur.
$$ T(p)=\frac{K e^{-rp}}{1+\tau p}$$
Nous allons approcher la fonction H(p) précédente par une fonction T(p) avec la méthode de Broida.\\
Pour cela, nous allons tracer la réponse indicielle de H(p) et calculer t1 le temps au bout duquel la réponse indicielle de H(p) est égale à 28\% de sa valeur finale et t2 le temps au bout duquel la réponse indicielle de H(p) est égale à 40\% de sa valeur finale.\\
Alors:
$$ \tau = 5,5(t2 -t1)$$
$$ r = 2,8t1 - 1,8t2$$

\begin{questions}
\question Déterminer les valeurs de $\tau$ et r par la méthode de Broida.
\question Tracer la réponse indicielle de T(p) et comparer avec la réponse indicielle de H(p).
\end{questions}

\newpage
\section{Correcteur PID}
\subsection{Méthode de Ziegler Nichols en boucle ouverte}
Nous allons corriger la réponse indicielle de notre système H(p) en ajoutant un correcteur de type PID dans la boucle fermée.\\

Sous Matlab, la mise en série de systèmes s'effectue simplement. Soit \textit{H(p)} un système écrit sous la forme \textit{h=zpk(..)} et un correcteur \textit{C(p)} écrit \textit{c=zpk(..)}. Le système constitué des 2 fonctions de transfert en série s'obtient par \textit{bo=c*h}.\\
Pour effectuer un retour de fonction de transfert \textit{R(p)}, on utilise la fonction \textit{feedback} dont la syntaxe est \textit{bf=feedback(bo,R)}, bo est le système en boucle ouverte, bf est le système en boucle fermée.\\\\

A partir de la réponse indicielle du système H(p) en boucle ouverte, on calcule Tu, Ta et K.\\
La méthode de Ziegler-Nichols permet alors de calculer un correcteur de type PID de la forme:
$$ C(p)= K_r * (1 + \frac{1}{\tau_i p} + \tau_d p) $$

avec
	\begin {tabular}{|p{4cm}|p{1,5cm}|p{1,5cm}|p{1,5cm}|} \hline
	Type de Correcteur  & $K_r$ & $\tau_i$ & $\tau_d$	   \\ \hline
	P  & $\frac{Ta}{Tu*K}$ &  & 	   \\ \hline
	PI  & 0.9$\frac{Ta}{Tu*K}$ & 3,3*Tu & 	   \\ \hline
	PID  & 1.2$\frac{Ta}{Tu*K}$ & 2*Tu & 	0,5*Tu   \\ \hline
	\end {tabular}\\

\begin{questions}
\question Pour le système \textbf{H(p)} précédent, calculer les valeurs de Tu, Ta, et K le gain statique (Voir calculs méthode Strejc).
\end{questions}


Vous tracerez les 4 réponses indicielles (sans correcteur, avec correcteur P, avec correcteur PI et avec correcteur PID) sur la même figure.

\underline{Correcteur P}
On ajoute un correcteur de type P dans la boucle.
\begin{questions}
\question Calculer le correcteur proportionnel que l'on appellera Cp. 
\question Dessiner le schéma du système en boucle fermé avec le correcteur Cp. 
\question Tracer la réponse indicielle du système en boucle fermée. Interpréter.
\end{questions}

\underline{Correcteur PI}
On ajoute un correcteur de type PI dans la boucle.
\begin{questions}
\question Calculer le correcteur PI que l'on appellera Cpi. 
\question Dessiner le schéma du système en boucle fermé avec le correcteur Cpi. 
\question Tracer la réponse indicielle du système en boucle fermée. Interpréter.
\end{questions}

\underline{Correcteur PID}
On ajoute un correcteur de type PID dans la boucle.
\begin{questions}
\question Calculer le correcteur proportionnel que l'on appellera Cpid. 
\question Dessiner le schéma du système en boucle fermé avec le correcteur Cpid.  
\question Tracer la réponse indicielle du système en boucle fermée. Interpréter.
\end{questions}


\subsection{Méthode de Ziegler Nichols en boucle fermée}
A partir de la réponse indicielle du système H(p) en boucle fermée, on cherche à amener le système en \textit{limite de pompage}. Pour cela, on place un correcteur proportionnel de valeur K dans la boucle fermée et on augmente K jusqu'à obtenir des oscillations entretenues (pompage). On note alors ce gain $K_0$ et on mesure la période $T_0$ des oscillations. Les paramètres pour calculer le correcteur PID sont donnés dans le tableau ci-dessous.

$$ C(p)= K_r * (1 + \frac{1}{\tau_i p} + \tau_d p) $$

avec
	\begin {tabular}{|p{4cm}|p{1,5cm}|p{1,5cm}|p{1,5cm}|} \hline
	Type de Correcteur  & $K_r$ & $\tau_i$ & $\tau_d$	   \\ \hline
	P  & $0.5K_0$ &  & 	   \\ \hline
	PI  & $0.45K_0$ & $0.83T_0$ & 	   \\ \hline
	PID  & $0.6K_0$ & $0.5T_0$ & 	$0.125T_0$   \\ \hline
	\end {tabular}\\

\begin{questions}
\question Calculer le correcteur PID. 
\question Dessiner le schéma du système en boucle fermé avec le correcteur.  
\question Tracer la réponse indicielle du système en boucle fermée. Interpréter.
\end{questions}

\newpage
\section{Annexe: Rappels sur la méthode de Strejc}
Le modèle de Strejc s'écrit:
$$ T(p)=\frac{K e^{-rp}}{(1+\tau p)^n}$$
La réponse indicielle doit être sans dépassement comme sur la figure ci-dessous:

	\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth,page=1]{Figures.pdf}
	\caption{Réponse indicielle en boucle ouverte.}
	\end{figure}

	\begin {tabular}{|p{1cm}|p{1,5cm}|p{1,5cm}|p{1,5cm}|} \hline
	n  & $\frac{Ta}{\tau}$ & $\frac{Tu}{\tau}$ & $\frac{Tu}{Ta}$	   \\ \hline
	2  & 2.7183 & 0.2817 & 0.1036	   \\ \hline
	3  & 3.6945 & 0.8055 & 0.218	   \\ \hline
	4  & 4.4635 & 1.4254 & 0.3194	   \\ \hline
	5  & 5.1186 & 2.102 & 0.4103	   \\ \hline
	6  & 5.6991 & 2.8113 & 0.4933	   \\ \hline
	7  & 6.2256 & 3.5489 & 0.57	   \\ \hline
	8  & 6.7113 & 4.3069 & 0.6417	   \\ \hline
	9  & 7.164 & 5.081 & 0.7092	   \\ \hline
	10  & 7.5898 & 5.8685 & 0.7732	   \\ \hline
	11  & 7.993 & 6.6673 & 0.8341	   \\ \hline
	\end {tabular}\\\\

Pour résumer, la méthode d'identification à partir de la réponse indicielle du système s'appuie sur les étapes suivantes:
\begin{enumerate}
\item On recherche le point d'inflexion sur le relevé de la réponse indicielle.
\item On trace la tangente à la courbe en ce point.
\item On relève les valeurs Tu et Ta pour établir le rapport $\frac{Tu}{Ta}$.
\item Dans le tableau, on détermine l'ordre n du système en fonction du rapport calculé $\frac{Tu}{Ta}$. Il est rare que la valeur du rapport $\frac{Tu}{Ta}$ donne une valeur exacte du tableau. On prend alors la valeur immédiatement inférieure dans le tableau.
\item On calcule $\tau$ avec le rapport $\frac{Ta}{\tau}$.
\item Comme le rapport $\frac{Tu}{Ta}$ n'est pas dans le tableau, on calcule un nouveau Tu' avec le rapport $\frac{Tu}{Ta}$ du tableau et le retard r est égal à Tu - Tu'.
\end{enumerate}

\end{document}
