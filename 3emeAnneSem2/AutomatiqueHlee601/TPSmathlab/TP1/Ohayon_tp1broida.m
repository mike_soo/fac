clc;
clear;
%identification Broida
%1. zpk=modèle zéro, pole, gain
h=zpk([],[-4 -5 -1],100)
%tracés
figure(1)
impulse(h);
grid
figure(2);
step(h);
grid
% 3. Calcul de tau et r
% Valeur finale =5
% 28% de la valeur finale = 0,28*5 = 1,4
% t1 = 0.764s
% 40 % de la valeur finale = 0,40*5 = 2
%t2=0.982
%tau =5,5(t2-t1) = 5.5 (0.982 -0.764) = 1.2
% r = 2.8*0.764 - 1.8*0.982 = 0.37
% K= H(0)=5
%
% T(p) = (K/tau)* e(-rp) /(p+1)/tau

figure(3);
step(h);
hold on;
h2=zpk([],-1/1.2,5/1.2,'iodelaymatrix',0.37);
%h3=zpk([],[-1/1.2],5/1.2,'iodelaymatrix',0.37)
step(h2,'r');
%step(h3,'g');
grid;