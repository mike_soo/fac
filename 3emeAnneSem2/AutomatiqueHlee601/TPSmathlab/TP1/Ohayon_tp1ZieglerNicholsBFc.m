clc;
clear all;
close all;
%Identification Ziegler Nichols PID boucle ouverte

%1. zpk = modèle zéro , pole, gain
h = zpk([],[-4 -5 -1],100);


%2. bo et bf 

bo = h;

figure(1);
step(bo,'r');
t=(0 : 0.029 :12);

y=2.87 *t-0.785;
plot(t,y);

bf = feedback(bo,1);

figure(2);
step(bf,'y');

%PID 
Tu =0.27;
Ta = 1.73;
K=5;


%CORRECTEUR P
Kr =Ta/(Tu*K);

cp = Kr;

bfp=feedback(bo*cp,1);

hold on;
step(bfp,'g');

%CORRECTEUR PI (annulle l'erreur statique)
Kr= (0.9*Ta)/(Tu*K);
ti=3.3 * Tu;
%cpi = Kr + Kr/(ti*p) = (Kr*ti*p  + Kr)/(ti * p)
cpi = tf([Kr*ti Kr],[ti 0]);
bfpi = feedback(bo*cpi,1);
hold on;
step(bfpi,'r');


%CORRECTEUR PID
Kr=(1.2*Ta)/(Tu*K);
ti = 2 *Tu;
td = 0.5*Tu;
%cpid = Kr + Kr/(ti*p) + Kr * td * p =(Kr*td*p² + Kr* ti * p + Kr)/ (ti *p)



cpid = tf([Kr*td*ti Kr*ti Kr],[ti 0]);
bfpid=feedback(bo*cpid,1);
hold on
step(bfpid,'y');
title('green = Correcteur P, red = Correcteur PI, yellow = Correcteur PID')

%Ziegler - Nichols en boucle fermée
%On règle KO = 2.7 et on mesure T0
% T0 = 1.16s
K=2.7;
bf = feedback(bo,K);
figure(3);
step(bf);

%T0=1.17s
%Kr = 0.6* K0 = 1.62
%ti = 0.5*T0 =0.58s
%td=0.125*T0 = 0.146s
%cpid = (0.136*p² + 0.94*p +1.62) / (0.58*p)
figure (4);
cpid= tf([0.136 0.94 1.62 ] , [0.58 0 ]);
bfpid = feedback(bo*cpid,1);
step(bfpid , 'k');

return 