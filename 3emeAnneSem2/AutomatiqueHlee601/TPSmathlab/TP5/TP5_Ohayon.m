n=[1]; % definition du numerateur
d=[ 1 1 0]; % definition du d'enominateur
sys=tf(n,d); % fonction de transfert continue
Te=1; % temps d'echantillonage
sysd = c2d (sys,Te) % fonction de transfert discr\`{e}te


%Zdan
%Comportement du 2eme ordre souhaité
n=[4];
d=[1 2.8 4]
g=tf(n,d);
gz=c2d(g,1)

syms Y0 X1 X0;
Equ1=Y0-1;
Equ2=0.37*X0-2*Y0+0.076;
Equ3=Y0+0.37*X1-0.06;
Sxy=solve(Equ1,Equ2,Equ3,X0,X1,Y0)
X0=double(Sxy.X0)
X1=double(Sxy.X1)
Y0=double(Sxy.Y0)

%        X                                5.2 + -2.54z^-1
% Cc = -----  ; avec X = X0+X1z^-1 => Cc= ---------------
%        Y    ;      Y = Y0                     1

%Reponse plate

%On commence par developer = (L⁺D⁻Y) + N⁻X 
% =(1 - z^-1)^2(Y0 + Y1*z^-1) + 0.37z^-1 (X0+X1z^-1)(1+0.7z^-1)
% avec Y=Y0+Y1*z^-1 et X=X0 + X1^-1

syms Y0 Y1 X0 X1;
Equ1=Y0-1;
Equ2=0.37*X0+Y1-2*Y0;
Equ3=0.26*X0+0.37*X1-2*Y1+Y0;
Equ4=0.26*X1 + Y1;
Sxy=solve(Equ1,Equ2,Equ3,Equ4,X0,X1,Y0,Y1)
X0=double(Sxy.X0)
X1=double(Sxy.X1)
Y0=double(Sxy.Y0)
Y1=double(Sxy.Y1)

% ===>  
%           D⁺X    (1-0.3679z^-1) (3.8297 - 2.2424*z^-1)
%   C(z) = ----= ----------------------------------------
%           L⁺Y      (1-z^-1)² (1 + 0.5830*z^-1)
%
%