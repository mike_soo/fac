clc;
clear;
close all;
%Exercice1
g=tf([2 1],[1 2 1])

gc=c2d(g,0.1)
figure(1)
step(g)
hold on
step(gc)
grid on

%Exercice2

%1 Utilisation de la fonction tf pour ecrire hd
Hdtf=tf([0.047 0.046],[1 -1.81 +0.9],0.1)

%2 Utilisation de la fonction zpk pour écrire Hd
Hdzpk= zpk([(-0.046/0.047)],[(1.81-0.5691*i)/2 (1.81+0.5691*i)/2 ], 0.047 , 0.1)

%3Visualisation des poles avec pzmap
figure(3)
pzmap(Hdtf)
grid on

%4. Les poles de Hd avec la fonction pole
pole(Hdtf)

%Exercice3

%1 Condition de stabilité système échantillonné linéaire
% ssi les poles ont tous un module inférieur à 1
% cad se trouvent tous à l'intereur du cercle de centre (0,0) et de rayon
% unité du plan z.
% On constate que le denominateur est de degré 2 => deux poles : 0.4 et 0.8
%2 On a donc deux poles de modules < 1 => système stable.

%3 Apres calculs sur feuille, on trouve la fonction de transfert en boucle
%                           k                        k
%    fermé Hbf(z)= ------------------------ = -----------------------
%                  (z-0.4)(z - 0.8) + k         z² - 1.2z + 0.32 +k
%
% 
%
%4 et 5



k1=0.67
Hz1=tf([k1],[1 -1.2 0.32],0.1)
figure(4)

Hz1bf=feedback(Hz1,+1)
step(Hz1bf)

k2=0.68
Hz2=tf([k2],[1 -1.2 0.32],0.1)
figure(5)

Hz2bf=feedback(Hz2,+1)
step(Hz2bf)

%dans la demande de saisi de rltool, G prend la valeur de Hz1 puis de
%Hz2 , decomentez pour tester :
%rltool

%On constate visuelement que k<0.68 est une condition pour que le
% systeme soit stable car grace à rtool on constate que les poles, lorsque 
% k = 0.68, sortent legerement du cercle de rayon unité du plan z.
% Puis la reponse indicielle losque k=0.68 est graphiquement instable .

%Exercice4

Hdz= tf([0.9 0],[1  -0.9],0.1)
Hdzbf=feedback(Hdz,1)
figure(6)
step(Hdzbf)
% La valeur final de la commande souhaité est de 1, or la valeur final 
% atteinte par lecture graphique est de 0.9 => Le systeme possede une
% erreur à 10%

%Exercice 5
H = tf([10] , [0.1 1])


figure(7)

Hd1 = feedback(c2d(H,0.1),1)
step(Hd1,'g')
hold on

Hd2 = feedback(c2d(H,0.01),1)
step(Hd2,'r')


