#include <iostream>
#include <cmath>

struct poly{
  int degre;
  float *list;
};
 
poly cree()
{ 
  poly p;
  std::cout<<"donnez le degre du polynome \n";
  std::cin>>p.degre;
  p.liste= new float [p.degre+1];
  return p;
}

void init (poly* p)
{for (int i=0;i<=p->degre;i++)
    { 
      std::cout<<"donnez le coefficient du terme de degre "<<i<<"\n";
      std::cin>>p->liste[i];
    }
}

void affiche (poly *p)
{for (int i=p->degre;i>0;i--)
    {
     std::cout<<p->liste[i]<<"x^"<<i<<" + ";
    }
  std::cout<<p->liste[0]<<"\n";
}

poly derivee(poly p)
{
  poly res ;
  res.degre=p.degre-1;
  res.liste=new float [res.degre+1];
  for(int i=0;i<=res.degre;i++)
    res.liste[i]=(i+1)*p.liste[i+1];
  return res;
} 

poly primitive(poly p)
{poly res;
  res.degre=p.degre+1; 
  res.liste=new float[res.degre+1];
  res.liste[0]=0;
  for(int i=1;i<=res.degre;i++)
    res.liste[i]=p.liste[i-1]/i;
  return res;
}

float evalue(poly p,float x)
{float res=0;
  for(int i=0;i<=p.degre;i++)
    res+=p.liste[i]*pow(x,i);
  return res;
}
int main()
{

  return 0;
}
