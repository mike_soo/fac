#include <iostream>
#include "affiche.h"

int *extract(int *T,int a,int b,int n)
{
 int x=b-a+1;
 int *Tex=new int[x];
 
 for(int i=0;i<x;i++)
   {
    Tex[i]=T[a];
    a+=1;
   }
 return Tex;

}

int main()
{int a,b,n;
 
  std::cout<<"entrez la valeur de l'intervalle des indices a et b"<<std::endl;
  std::cin>>a;
  std::cin>>b;
  std::cout<<"entrez la taille du tableaux d'entrée"<<std::endl;
  std::cin>>n;
  int *T=new int[n];
  for(int i=0;i<n;i++)
    {T[i]=2*i+1;}
  
  affiche(extract(T,a,b,n),b-a+1); 
 
  return 0;
}
