#include <iostream>
void affiche(int *t,int taille)
{for(int i=0;i<taille;i++)
    {std::cout<<t[i]<<" ";
     std::cout<<std::endl;}
}
void minmax(int *T,int n,int *min, int *max)
{*min=*max=*T;//equivalent a : *min =*max=T[0]
  for (int i=1;i<n;i++)
    {
      if (T[i]>*max) *max=T[i];
      if(T[i]<*min) *min=T[i];
    }
}
int main()
{int T[7]={10,11,34,2,45,49,67};
  int min,max;
  minmax(T,7,&min,&max);

  std::cout<<"le minimu et le maximum de ";
  affiche(T,7);
  std::cout<<" sont: "<<min<<" et "<<max<<std::endl;
  return 0;
}
