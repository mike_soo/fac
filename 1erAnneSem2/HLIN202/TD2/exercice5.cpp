#include <iostream>
#include <cmath>

int main()
{float a;
  std::cin>>a;
  if (a<0)
    {std::cout<<"un nombre negatif n'a pas de racine reel"<<std::endl;
      return 0;}
  else if (a==0)
    {std::cout<<"la racine de 0 n'est pas calculable"<<std::endl;
      return 0;}
  else 
    {std::cout<<"la racine de "<<a<<" est ";
      a=sqrt(a);
      std::cout<<a<<std::endl;
      return 0;}
  
  return 0;
}
