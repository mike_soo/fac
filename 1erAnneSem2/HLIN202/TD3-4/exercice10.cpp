#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int C(int p,int n)
{
  if (p==0)
    {return 1;}
  if (p==n)
    {return 1;}
  
    { return C(p,n-1)+C(p-1,n-1);}
 }

int C2(int p,int n)
{if (p==0)
    {return 1;}
 if (p==n)
    {return 1;}
   
    {return (n/p)*C2(p-1,n-1);}
}

void triangle(int n)
{int c=0;
  
  for (int i=0;i<=n;i++)
      {for (int j=0;j<=i;j++) 
	{
	  c=C(j,i);
          cout<<setw(3)<<c;
          
        }
	cout<<endl;
          
    } 
  
}
 
 int main()
 {cout<<C(4,8)<<endl;
   cout<<C2(4,8)<<endl; 
   triangle(8);
   return 0;
 }
  
